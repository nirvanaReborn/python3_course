# 谷歌浏览器驱动

[下载地址](http://chromedriver.storage.googleapis.com/index.html)


# 火狐浏览器驱动

[下载地址](https://github.com/mozilla/geckodriver/releases)


# 微软的Edge浏览器驱动

[下载地址](https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/#downloads)

> * 要使用headless的edge，还需 pip install msedge-selenium-tools
> * 注意edge驱动只有在edge浏览器未启动的情况下才能正常运行，否则会报错。

# PhantomJS

[下载地址](http://phantomjs.org/download.html)

> * phantomjs可以理解成没有界面的浏览器，所以驱动跟浏览器是一体的。

>> * UserWarning: Selenium support for PhantomJS has been deprecated, please use headless versions of Chrome or Firefox instead。
>> * 将其卸载pip uninstall selenium，重新安装并指定版本号pip install selenium==2.48.0即可。
