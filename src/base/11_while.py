#! /usr/bin/python 
# -*- coding: UTF-8 -*-

"""
------------------------------------循环语句------------------------------------
while 循环			在给定的判断条件为 true 时执行循环体，否则退出循环体。
for 循环				重复执行语句
嵌套循环				你可以在while循环体中嵌套for循环

----------------------------------循环控制语句----------------------------------
break 语句			在语句块执行过程中终止循环，并且跳出整个循环
continue 语句		在语句块执行过程中终止当前循环，跳出该次循环，执行下一次循环。
pass 语句			pass是空语句，是为了保持程序结构的完整性。

在 python 中，for … else 表示这样的意思，for 中的语句和普通的没有区别，
else 中的语句会在循环正常执行完（即 for 不是通过 break 跳出而中断的）的情况下执行，
while … else 也是一样。

-----------------------------------while 循环-----------------------------------
while 判断条件：
    执行语句……
"""

count = 0
while (count < 9):
   print('The count is:', count)
   count = count + 1

print("Good bye!")


# continue 用法
i = 1
while i < 10:   
    i += 1
    if i%2 > 0:     # 非双数时跳过输出
        continue
    print(i)          # 输出双数2、4、6、8、10
		
print("Good bye!")


# break 用法		
i = 1
while 1:             # 循环条件为1必定成立
    print(i)         # 输出1~10
    i += 1
    if i > 10:       # 当i大于10时跳出循环
        break

print("Good bye!")


#循环使用 else 语句
count = 0
while count < 5:
   print(count, " is  less than 5")
   count = count + 1
else:
   print(count, " is not less than 5")

print("Good bye!")


#无限循环
var = 1
while var == 1 :  # 该条件永远为true，循环将无限执行下去
   num = input("Enter a number  :")
   print("You entered: ", num)

print("Good bye!")


#简单的语句组  
flag = 1
while (flag):
    print('Given flag is really true!')
print("Good bye!")

