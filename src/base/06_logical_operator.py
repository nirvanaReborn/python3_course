#! /usr/bin/python 
# -*- coding: UTF-8 -*-

"""
Python语言支持以下类型的运算符:
算术运算符
比较（关系）运算符
赋值运算符
逻辑运算符
位运算符
成员运算符
身份运算符
运算符优先级

and	布尔"与" - 如果x为False，x and y返回False，否则它返回y的计算值。	(a and b) 返回 true。
or	布尔"或" - 如果x是True，它返回True，否则它返回y的计算值。			(a or b) 返回 true。
not	布尔"非" - 如果x为True，返回False。如果x为False，它返回True。		not(a and b) 返回 false。
"""
# ----------------------------------逻辑运算符-----------------------------------
a = 10
b = 20
c = 0

if (a and b):
    print("Line 1 - a and b are true")
else:
    print("Line 1 - Either a is not true or b is not true")

if (a or b):
    print("Line 2 - Either a is true or b is true or both are true")
else:
    print("Line 2 - Neither a is true nor b is true")

a = 0
if (a and b):
    print("Line 3 - a and b are true")
else:
    print("Line 3 - Either a is not true or b is not true")

if (a or b):
    print("Line 4 - Either a is true or b is true or both are true")
else:
    print("Line 4 - Neither a is true nor b is true")

if not (a and b):
    print("Line 5 - Either a is not true or b is  not true or both are not true")
else:
    print("Line 5 - a and b are true")
