# python3_course

#### 项目介绍

python3基础教程

#### 软件架构

*  PyCharm + Anaconda3(V5.1.0)[python3.6.4]
*  [Anaconda版本与Python版本对应关系](https://blog.csdn.net/zyb228/article/details/103251761)
*  [Anaconda历史版本](https://repo.anaconda.com/archive/)

#### 教程来源

[Python 3 教程 (菜鸟教程)](http://www.runoob.com/python3/python3-tutorial.html)

#### 参考教程

[慕课网](https://www.imooc.com/course/list?c=python)
