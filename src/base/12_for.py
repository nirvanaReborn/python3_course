#! /usr/bin/python 
# -*- coding: UTF-8 -*-

"""
------------------------------------循环语句------------------------------------
while 循环			在给定的判断条件为 true 时执行循环体，否则退出循环体。
for 循环				重复执行语句
嵌套循环				你可以在while循环体中嵌套for循环

----------------------------------循环控制语句----------------------------------
break 语句			在语句块执行过程中终止循环，并且跳出整个循环
continue 语句		在语句块执行过程中终止当前循环，跳出该次循环，执行下一次循环。
pass 语句			pass是空语句，是为了保持程序结构的完整性。

在 python 中，for … else 表示这样的意思，for 中的语句和普通的没有区别，
else 中的语句会在循环正常执行完（即 for 不是通过 break 跳出而中断的）的情况下执行，
while … else 也是一样。

-----------------------------------while 循环-----------------------------------
for iterating_var in sequence:
   statements(s)
"""

for letter in 'Python':  # 第一个实例
    print('当前字母 :', letter)

fruits = ['banana', 'apple', 'mango']
for fruit in fruits:  # 第二个实例
    print('当前水果 :', fruit)

print("Good bye!")

# 通过序列索引迭代函数
# len() 返回列表的长度，即元素的个数。
# range() 返回一个序列的数。
fruits = ['banana', 'apple', 'mango']
for index in range(len(fruits)):
    print('当前水果 :', fruits[index])

print("Good bye!")

# 循环使用 else 语句
for num in range(10, 20):  # 迭代 10 到 20 之间的数字
    for i in range(2, num):  # 根据因子迭代
        if num % i == 0:  # 确定第一个因子
            j = num / i  # 计算第二个因子
            print('%d 等于 %d * %d' % (num, i, j))
            break  # 跳出当前循环
    else:  # 循环的 else 部分
        print(num, '是一个质数')

print("Good bye!")

# 循环嵌套
i = 2
while (i < 100):
    j = 2
    while (j <= (i / j)):
        if not (i % j):
            break
        j = j + 1
    if (j > i / j):
        print(i, " 是素数")
    i = i + 1

print("Good bye!")
