#! /usr/bin/python 
# -*- coding: UTF-8 -*-

"""
Python语言支持以下类型的运算符:
算术运算符
比较（关系）运算符
赋值运算符
逻辑运算符
位运算符
成员运算符
身份运算符
运算符优先级

==	等于     - 比较对象是否相等				(a == b) 返回 False。
!=	不等于   - 比较两个对象是否不相等		    (a != b) 返回 true.
<>	不等于   - 比较两个对象是否不相等		    (a <> b) 返回 true。这个运算符类似 != 。
>	大于     - 返回x是否大于y				(a > b) 返回 False。
<	小于     - 返回x是否小于y。				(a < b) 返回 true。
>=	大于等于 - 返回x是否大于等于y。			(a >= b) 返回 False。
<=	小于等于 - 返回x是否小于等于y。			(a <= b) 返回 true。


所有比较运算符返回1表示真，返回0表示假。这分别与特殊的变量True和False等价。注意，这些变量名的大写。	
"""
# ----------------------------------比较运算符-----------------------------------

a = 21
b = 10
c = 0

if (a == b):
    print("Line 1 - a is equal to b")
else:
    print("Line 1 - a is not equal to b")

if (a != b):
    print("Line 2 - a is not equal to b")
else:
    print("Line 2 - a is equal to b")

# if ( a <> b ):
#    print("Line 3 - a is not equal to b")
# else:
#    print("Line 3 - a is equal to b")

if (a < b):
    print("Line 4 - a is less than b")
else:
    print("Line 4 - a is not less than b")

if (a > b):
    print("Line 5 - a is greater than b")
else:
    print("Line 5 - a is not greater than b")

a = 5
b = 20
if (a <= b):
    print("Line 6 - a is either less than or equal to  b")
else:
    print("Line 6 - a is neither less than nor equal to  b")

if (b >= a):
    print("Line 7 - b is either greater than  or equal to b")
else:
    print("Line 7 - b is neither greater than  nor equal to b")

# 文本和数字相等判断
print(40 == '40') # False
print(40 == 40.0) # True 虽然数字的字符串值被认为与整型值和浮点型值完全不同，但整型值可以与浮点值相等。
print(40.0 == 0040.000) # True