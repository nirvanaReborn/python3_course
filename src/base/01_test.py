#! /usr/bin/python 
# -*- coding: UTF-8 -*-

# Python 3 教程 (菜鸟教程)
# http://www.runoob.com/python3/python3-tutorial.html
'''模块导入
import 包名.模块名
import 包名.模块名 as 新模块名
forom 包名 import 模块名
forom 包名 import 模块名 as 新模块名
'''


def test_1():
    # 第一个注释
    print("Hello, World!")  # 第二个注释
    print("你好，世界")
    print('The quick brown fox', 'jumps over', 'the lazy dog')
    print('100 + 200 =', 100 + 200)

    name = input('please enter your name: ')  # 这是一个注释
    print('hello,', name)

    '''
    这是多行注释，使用单引号。
    这是多行注释，使用单引号。
    这是多行注释，使用单引号。
    '''

    """
    这是多行注释，使用双引号。
    这是多行注释，使用双引号。
    这是多行注释，使用双引号。
    """

    # 同一行显示多条语句
    import sys
    x = 'foo'
    sys.stdout.write(x + '\n')

    # 变量赋值
    counter = 100  # 赋值整型变量
    miles = 1000.0  # 浮点型
    name = "John"  # 字符串

    print(counter)
    print(miles)
    print(name)

    # 多个变量赋值
    a = b = c = 1
    a = 5
    # 以上实例，创建一个整型对象，值为1，三个变量被分配到相同的内存空间上。
    print(a)
    print(b)
    print(c)

    a, b, c = 1, 2, "john"
    # 以上实例，两个整型对象1和2的分配给变量a和b，字符串对象"john"分配给变量c。
    print(a)
    print(b)
    print(c)


# ---------------------------------Python字符串----------------------------------
"""
python的字串列表有2种取值顺序:
从左到右索引默认0开始的，最大范围是字符串长度少1
从右到左索引默认-1开始的，最大范围是字符串开头

加号（+）是字符串连接运算符，
星号（*）是重复操作。
string[2:5]的结果包含了string[2]的值，而不包括string[5]的值。
"""


def test_2():
    string = 'Hello World!'

    print(len(string))  # 输出完整字符串
    print(string[0], string[-len(string)])  # 输出第一个字符
    print(string[-1], string[len(string) - 1])  # 输出最后一个字符
    print(string[2:5])  # 输出字符串中第三个至第五个之间的字符串
    print(string[2:])  # 输出从第三个字符开始的字符串
    print(string * 2)  # 输出字符串两次
    print(string + "TEST")  # 输出拼接的字符串

    # 实现string[7] = the
    # "改变"一个字符串的正确方式，是使用切片和连接。
    newString = string[0:7] + 'the' + string[8:12]
    print(newString)


# ----------------------------------Python列表-----------------------------------
def test_3():
    list = ['abcd', 786, 2.23, 'john', 70.2]
    tinylist = [123, 'john']

    print(list)  # 输出完整列表
    print(list[0])  # 输出列表的第一个元素
    print(list[1:3])  # 输出第二个至第三个的元素
    print(list[2:])  # 输出从第三个开始至列表末尾的所有元素
    print(tinylist * 2)  # 输出列表两次
    print(list + tinylist)  # 打印组合的列表


# ----------------------------------Python元组-----------------------------------
"""
元组是另一个数据类型，类似于List（列表）。
但是元素不能二次赋值，相当于只读列表。
元组用"()"标识。
内部元素用逗号隔开。

tuple = ( 'abcd', 786 , 2.23, 'john', 70.2 )
list = [ 'abcd', 786 , 2.23, 'john', 70.2 ]
tuple[2] = 1000 # 元组中是非法应用
list[2] = 1000 # 列表中是合法应用

"""


def test_4():
    tuple = ('abcd', 786, 2.23, 'john', 70.2)
    tinytuple = (123, 'john')

    print(tuple)  # 输出完整元组
    print(tuple[0])  # 输出元组的第一个元素
    print(tuple[1:3])  # 输出第二个至第三个的元素
    print(tuple[2:])  # 输出从第三个开始至列表末尾的所有元素
    print(tinytuple * 2)  # 输出元组两次
    print(tuple + tinytuple)  # 打印组合的元组


# ----------------------------------Python字典-----------------------------------
def test_5():
    dict = {}
    dict['one'] = "This is one"
    dict[2] = "This is two"

    tinydict = {'name': 'john', 'code': 6734, 'dept': 'sales'}

    print(dict['one'])  # 输出键为'one' 的值
    print(dict[2])  # 输出键为 2 的值
    print(tinydict)  # 输出完整的字典
    print(tinydict.keys())  # 输出所有键
    print(tinydict.values())  # 输出所有值

    # 遍历字典方法1
    for key in tinydict:
        print(key, '+++>', tinydict[key])

    # 遍历字典方法2
    for key, value in tinydict.items():
        print(key, '--->', value)

    # 判断字典是否存在某个key
    if 'age' in tinydict.keys():
        print("tinydict包含age")
    else:
        print("tinydict不包含age")

    # sorted函数按key值对字典排序
    dict = sorted(tinydict.items(), key=lambda d: d[0])
    print("按key值排序", dict)

    # sorted函数按value值对字典排序
    dict = sorted(tinydict.items(), key=lambda d: d[1], reverse=True)
    print("按value值排序", dict)


# -------------------------------------------------------------------------

def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    dict_choice = {
        "1": "test_1()",
        "2": "test_2()",
        "3": "test_3()",
        "4": "test_4()",
        "5": "test_5()",
    }
    make_choice(dict_choice)
    input("\n\nPress the enter key to exit.")


if __name__ == "__main__":
    main()
