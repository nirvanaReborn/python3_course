#! /usr/bin/python
# -*- coding: UTF-8 -*-


'''
range(stop) -> list of integers
range(start, stop[, step]) -> list of integers
'''


def test_1():
    for i in range(5):
        print(i)


#   0, 1, 2, 3, 4

def test_2():
    for i in range(5, 10):
        print(i)


#   5, 6, 7, 8, 9

def test_3():
    for i in range(0, 10, 3):
        print(i)


#   0, 3, 6, 9


def test_4():
    for i in range(-10, -100, -30):
        print(i)


#  -10, -40, -70

def test_5():
    a = ['Mary', 'had', 'a', 'little', 'lamb']
    for i in range(len(a)):
        print(i, a[i])


# (0, 'Mary')
# (1, 'had')
# (2, 'a')
# (3, 'little')
# (4, 'lamb')


# 我们已经见过的 for 语句就是这样一个迭代器。
# list() 函数是另外一个（ 迭代器 ），它从可迭代（对象）中创建列表:
def test_6():
    print(list(range(5)))


# [0, 1, 2, 3, 4]

# -------------------------------------------------------------------------------
def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    dict_choice = {
        "1": "test_1()",
        "2": "test_2()",
        "3": "test_3()",
        "4": "test_4()",
        "5": "test_5()",
        "6": "test_6()",
    }
    make_choice(dict_choice)
    input("\n\nPress the enter key to exit.")


if __name__ == "__main__":
    main()
