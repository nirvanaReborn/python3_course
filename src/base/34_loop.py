#!/usr/bin/env python
# -*- coding:utf-8 -*-


# http://www.runoob.com/python3/python3-loop.html

def main():
    # 使用内置 enumerate 函数进行遍历:
    sequence = [12, 34, 34, 23, 45, 76, 89]
    for i, j in enumerate(sequence):
        print(i, j)


if __name__ == "__main__":
    main()