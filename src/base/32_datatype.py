#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-data-type.html

"""
Python3 中有六个标准的数据类型：
Number（数字）
String（字符串）
List（列表）
Tuple（元组）
Sets（集合）
Dictionary（字典）
"""
# string、list和tuple都属于sequence（序列）。
'''Python数据类型转换
函数                          描述
int(x [,base])              将x转换为一个整数
float(x)                    将x转换到一个浮点数
complex(real [,imag])       创建一个复数
str(object)                 将对象 object 转换为字符串
repr(object)                将对象 object 转换为表达式字符串
eval(str)                   用来计算在字符串中的有效Python表达式,并返回一个对象
tuple(seq)                  将序列 seq 转换为一个元组
list(seq)                   将序列 seq 转换为一个列表
set(seq)                    将序列 seq 转换为可变集合
frozenset(seq)              将序列 seq 转换为不可变集合
dict(d)                     创建一个字典, d 必须是一个序列 (key,value)元组。
chr(x)                      将一个整数转换为一个字符
unichr(x)                   将一个整数转换为Unicode字符
ord(x)                      将一个字符转换为它的整数值
hex(x)                      将一个整数转换为一个十六进制字符串
oct(x)                      将一个整数转换为一个八进制字符串
'''


# python中的函数还可以接收可变长参数，比如以 "*" 开头的的参数名，会将所有的参数收集到一个元组上。
def test_function_arg():
    def test(*args):
        print(args)
        return args
    print(type(test(1, 2, 3, 4)))


# 一般来说，函数的返回值一般为一个。而函数返回多个值的时候，是以元组的方式返回的。
def test_function_return():
    def example(a, b):
        return (a, b)
    print(type(example(3,4))) # <class 'tuple'>


def test_Dictionary():
    '''
    列表是有序的对象结合，字典是无序的对象集合。两者之间的区别在于：字典当中的元素是通过键来存取的，而不是通过偏移存取。
    字典是一种映射类型，字典用"{ }"标识，它是一个无序的键(key) : 值(value)对集合。
    键(key)必须使用不可变类型。在同一个字典中，键(key)必须是唯一的。
    创建空字典使用 { }。
    python中的字典是使用了一个称为散列表（hashtable）的算法,其特点就是：不管字典中有多少项，in操作符花费的时间都差不多。
    如果把一个字典对象作为for的迭代对象，那么这个操作将会遍历字典的键。
    '''
    dict = {}
    dict['one'] = "1 - 菜鸟教程"
    dict[2] = "2 - 菜鸟工具"
    tinydict = {'name': 'runoob', 'code': 1, 'site': 'www.runoob.com'}
    print(dict['one'])  # 输出键为 'one' 的值
    print(dict[2])  # 输出键为 2 的值
    print(tinydict)  # 输出完整的字典
    print(tinydict.keys())  # 输出所有键
    print(tinydict.values())  # 输出所有值
    for item in tinydict:
        print(item) # 遍历的是字典的键，而不是值


def test_Set():
    '''
    集合（set）是一个无序不重复元素的序列。
    基本功能是进行成员关系测试和删除重复元素。
    可以使用大括号 { } 或者 set() 函数创建集合，
    注意：创建一个空集合必须用 set() 而不是 { }，因为 { } 是用来创建一个空字典。
    '''
    student = {'Tom', 'Jim', 'Mary', 'Tom', 'Jack', 'Rose'}
    print(student)  # 输出集合，重复的元素被自动去掉
    # 成员测试
    if ('Rose' in student):
        print('Rose 在集合中')
    else:
        print('Rose 不在集合中')
    # set可以进行集合运算
    a = set('abracadabra')
    b = set('alacazam')
    print(a)
    print(a - b)  # a和b的差集
    print(a | b)  # a和b的并集
    print(a & b)  # a和b的交集
    print(a ^ b)  # a和b中不同时存在的元素


def test_Tuple():
    '''
    元组(tuple)与列表(list)类似，不同之处在于元组的元素不能修改。可以把字符串看作一种特殊的元组。
    元组写在小括号()里，元素之间用逗号隔开。    
    构造包含 0 个或 1 个元素的元组比较特殊，所以有一些额外的语法规则：
        tup1 = ()    # 空元组
        tup2 = (20,) # 一个元素，需要在元素后添加逗号
    '''
    tuple = ('abcd', 786, 2.23, 'runoob', 70.2)
    tinytuple = (123, 'runoob')

    print(tuple)  # 输出完整元组
    print(tuple[0])  # 输出元组的第一个元素
    print(tuple[1:3])  # 输出从第二个元素开始到第三个元素
    print(tuple[2:])  # 输出从第三个元素开始的所有元素
    print(tinytuple * 2)  # 输出两次元组
    print(tuple + tinytuple)  # 连接元组


def test_List():
    '''
    列表可以完成大多数集合类的数据结构实现。列表中元素的类型可以不相同，它支持数字，字符串甚至可以包含列表（所谓嵌套）。
    列表是写在方括号[]之间、用逗号分隔开的元素列表。
    和字符串一样，列表同样可以被索引和截取，列表被截取后返回一个包含所需元素的新列表。
    索引值以 0 为开始值，-1 为从末尾的开始位置。
    List中的元素是可以改变的。
    '''
    list = ['abcd', 786, 2.23, 'runoob', 70.2]
    tinylist = [123, 'runoob']

    print(list)  # 输出完整列表
    print(list[0])  # 输出列表第一个元素
    print(list[1:3])  # 从第二个开始输出到第三个元素
    print(list[2:])  # 输出从第三个元素开始的所有元素
    print(tinylist * 2)  # 输出两次列表
    print(list + tinylist)  # 连接列表
    for i in range(len(list)):
        print(i, list[i])

def test_String():
    '''
    # 1、反斜杠可以用来转义，使用r可以让反斜杠不发生转义。
    # 2、字符串可以用+运算符连接在一起，用*运算符重复。
    # 3、Python中的字符串有两种索引方式，从左往右以0开始，从右往左以-1开始。
    # 4、Python中的字符串不能改变。
    '''
    str = 'Runoob'
    print(str)  # 输出字符串
    print(str[0:-1])  # 输出第一个到倒数第二个的所有字符
    print(str[0])  # 输出字符串第一个字符
    print(str[2:5])  # 输出从第三个开始到第五个的字符
    print(str[2:])  # 输出从第三个开始的后的所有字符
    print(str * 2)  # 输出字符串两次
    print(str + "TEST")  # 连接字符串
    print('Ru\noob')
    print(r'Ru\noob')


def test_type_isinstance():
    '''
    # isinstance 和 type 的区别在于：
    # type()不会认为子类是一种父类类型。
    # isinstance()会认为子类是一种父类类型。
    '''
    class A:
        pass

    class B(A):
        pass

    print(isinstance(A(), A))   # returns True
    print(type(A()) == A)       # returns True
    print(isinstance(B(), A))   # returns True
    print(type(B()) == A)       # returns False

def test_Number():
    '''
    Python3 支持 int、float、bool、complex（复数）。
    在Python 3里，只有一种整数类型 int，表示为长整型，没有 python2 中的 Long。
    注意：在 Python2 中是没有布尔型的，它用数字 0 表示 False，用 1 表示 True。
    到 Python3 中，把 True 和 False 定义成关键字了，但它们的值还是 1 和 0，它们可以和数字相加。
    Python复数由实数部分和虚数部分构成，可以用a + bj, 或者complex(a, b)表示， 复数的实部a和虚部b都是浮点型
    像大多数语言一样，数值类型的赋值和计算都是很直观的。
    内置的 type() 函数可以用来查询变量所指的对象类型。
    '''
    a, b, c, d = 20, 5.5, True, 4 + 3j
    print(type(a), type(b), type(c), type(d))

    # 此外还可以用 isinstance 来判断
    print(isinstance(111, int))


def main():
    test_Number()
    test_type_isinstance()
    test_String()
    test_List()
    test_Tuple()
    test_Set()
    test_Dictionary()
    test_function_return()
    test_function_arg()

if __name__ == "__main__":
    main()