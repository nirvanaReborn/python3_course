#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 《Python编程快速上手——让繁琐工作自动化.pdf》
# 6.1.5 用三重引号的多行字符串
'''
虽然可以用\n转义字符将换行放入一个字符串，但使用多行字符串通常更容易。
在Python 中，多行字符串的起止是3 个单引号或3 个双引号。
“三重引号”之间的所有引号、制表符或换行，都被认为是字符串的一部分。
Python 的代码块缩进规则不适用于多行字符串。
'''


def main():
    print('''Dear Alice,
    
    Eve's cat has been arrested for catnapping, cat burglary, and extortion.
    
Sincerely,
Bob''')

    print('Dear Alice,\n\nEve\'s cat has been arrested for catnapping, catburglary, and extortion.\n\nSincerely,\nBob')


if __name__ == "__main__":
    main()
