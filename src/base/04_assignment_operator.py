#! /usr/bin/python 
# -*- coding: UTF-8 -*-

"""
Python语言支持以下类型的运算符:
算术运算符
比较（关系）运算符
赋值运算符
逻辑运算符
位运算符
成员运算符
身份运算符
运算符优先级

=	简单的赋值运算符	    c = a + b 将 a + b 的运算结果赋值为 c
+=	加法赋值运算符		c += a 等效于 c = c + a
-=	减法赋值运算符		c -= a 等效于 c = c - a
*=	乘法赋值运算符		c *= a 等效于 c = c * a
/=	除法赋值运算符		c /= a 等效于 c = c / a
%=	取模赋值运算符		c %= a 等效于 c = c % a
**=	幂赋值运算符			c **= a 等效于 c = c ** a
//=	取整除赋值运算符	    c //= a 等效于 c = c // a
"""
# ----------------------------------赋值运算符-----------------------------------

a = 21
b = 10

c = a + b
print("Line 1 - Value of c is ", c)

c += a
print("Line 2 - Value of c is ", c )

c *= a
print("Line 3 - Value of c is ", c )

# 浮点型除法
c /= a 
print("Line 4 - Value of c is ", c)

c  = 2
c %= a
print("Line 5 - Value of c is ", c)

# 幂
c **= a
print("Line 6 - Value of c is ", c)

# 整数除法
c //= a
print("Line 7 - Value of c is ", c)

