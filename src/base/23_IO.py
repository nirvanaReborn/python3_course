#! /usr/bin/python
# -*- coding: UTF-8 -*-

"""
Python提供了两个内置函数从标准输入读入一行文本，默认的标准输入是键盘。如下：
raw_input()
input()

input([prompt]) 函数和raw_input([prompt]) 函数基本可以互换，
但是input会假设你的输入是一个有效的Python表达式，并返回运算结果。


File 对象方法: file对象提供了操作文件的一系列方法。
OS 对象方法: 提供了处理文件及目录的一系列方法。



----------------------------------open()函数------------------------------------
你必须先用Python内置的open()函数打开一个文件，创建一个file对象，相关的辅助方法才可以调用它进行读写。
语法：
file object = open(file_name [, access_mode][, buffering])

file_name：file_name变量是一个包含了你要访问的文件名称的字符串值。
access_mode：access_mode决定了打开文件的模式：只读，写入，追加等。
             所有可取值见如下的完全列表。这个参数是非强制的，默认文件访问模式为只读(r)。
buffering:如果buffering的值被设为0，就不会有寄存。如果buffering的值取1，访问文件时会寄存行。
          如果将buffering的值设为大于1的整数，表明了这就是的寄存区的缓冲大小。
					如果取负值，寄存区的缓冲大小则为系统默认。

--------------------------------File对象的属性----------------------------------
file.closed			如果文件已被关闭返回true，否则返回false。
file.mode				返回被打开文件的访问模式。
file.name				返回文件的名称。
file.softspace	如果用print输出后，必须跟一个空格符，则返回false。否则返回true。


"""
# 打开一个文件
fo = open("foo.txt", "wb")
print("Name of the file: ", fo.name)
print("Closed or not : ", fo.closed)
print("Opening mode : ", fo.mode)
print("Softspace flag : ", fo.softspace)

# 关闭打开的文件
fo.close()

# ---------------------------------write()方法-----------------------------------
# 打开一个文件
fo = open("/tmp/foo.txt", "wb")
fo.write("Python is a great language.\nYeah its great!!\n")

# 关闭打开的文件
fo.close()

# ---------------------------------read()方法------------------------------------
# 打开一个文件
fo = open("/tmp/foo.txt", "r+")
str = fo.read(10)
print("Read String is : ", str)
# 关闭打开的文件
fo.close()

# -----------------------------tell()和seek()方法--------------------------------
# 打开一个文件
fo = open("/tmp/foo.txt", "r+")
str = fo.read(10)
print("Read String is : ", str)

# 查找当前位置
position = fo.tell()
print("Current file position : ", position)

# 把指针再次重新定位到文件开头
position = fo.seek(0, 0)
str = fo.read(10)
print("Again read String is : ", str)
# 关闭打开的文件
fo.close()

"""
Python的os模块提供了帮你执行文件处理操作的方法，比如重命名和删除文件。
os.rename(current_file_name, new_file_name)
os.remove(file_name)
os.mkdir("newdir")					在当前目录下创建新的目录
os.chdir("newdir")					改变当前的目录
os.getcwd()									显示当前的工作目录。
os.rmdir('dirname')					删除目录，在删除这个目录之前，它的所有内容应该先被清除。

"""
import os

# 重命名文件test1.txt到test2.txt。
os.rename("test1.txt", "test2.txt")
# 删除一个已经存在的文件test2.txt
os.remove("test2.txt")
# 创建目录test
os.mkdir("test")
# 将当前目录改为"/home/newdir"
os.chdir("/home/newdir")
# 给出当前的目录
os.getcwd()
# 删除”/tmp/test”目录
os.rmdir("/tmp/test")
