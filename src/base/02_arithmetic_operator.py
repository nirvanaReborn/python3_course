#! /usr/bin/python 
# -*- coding: UTF-8 -*-

"""
Python语言支持以下类型的运算符:
算术运算符
比较（关系）运算符
赋值运算符
逻辑运算符
位运算符
成员运算符
身份运算符
运算符优先级

+		加 - 两个对象相加	
-		减 - 得到负数或是一个数减去另一个数	
*		乘 - 两个数相乘或是返回一个被重复若干次的字符串
/		除 - x除以y	
%		取模 - 返回除法的余数
//	    取整除 - 返回商的整数部分
**	    幂 - 返回x的y次幂
"""


# ----------------------------------算术运算符-----------------------------------
def main():
    a = 21
    b = 10
    c = 0

    c = a + b
    print("Line 1 - Value of c is ", c)

    c = a - b
    print("Line 2 - Value of c is ", c)

    c = a * b
    print("Line 3 - Value of c is ", c)

    c = a / b
    print("Line 4 - Value of c is ", c)

    c = a % b
    print("Line 5 - Value of c is ", c)

    a = 2
    b = 3
    c = a ** b
    print("Line 6 - Value of c is ", c)

    a = 10
    b = 5
    c = a // b
    print("Line 7 - Value of c is ", c)


if __name__ == "__main__":
    main()
