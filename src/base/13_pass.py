#! /usr/bin/python 
# -*- coding: UTF-8 -*-

"""
------------------------------------循环语句------------------------------------
while 循环			在给定的判断条件为 true 时执行循环体，否则退出循环体。
for 循环				重复执行语句
嵌套循环				你可以在while循环体中嵌套for循环

----------------------------------循环控制语句----------------------------------
break 语句			在语句块执行过程中终止循环，并且跳出整个循环
continue 语句		在语句块执行过程中终止当前循环，跳出该次循环，执行下一次循环。
pass 语句			pass是空语句，是为了保持程序结构的完整性。

在 python 中，for … else 表示这样的意思，for 中的语句和普通的没有区别，
else 中的语句会在循环正常执行完（即 for 不是通过 break 跳出而中断的）的情况下执行，
while … else 也是一样。
"""
# -----------------------------------break 语句----------------------------------
for letter in 'Python':  # First Example
    if letter == 'h':
        break
    print('Current Letter :', letter)

var = 10  # Second Example
while var > 0:
    print('Current variable value :', var)
    var = var - 1
    if var == 5:
        break

print("Good bye!")

# ----------------------------------continue 语句--------------------------------
for letter in 'Python':  # 第一个实例
    if letter == 'h':
        continue
    print('当前字母 :', letter)

var = 10  # 第二个实例
while var > 0:
    var = var - 1
    if var == 5:
        continue
    print('当前变量值 :', var)
print("Good bye!")

# ------------------------------------pass 语句----------------------------------
# pass 不做任何事情，一般用做占位语句。
# 输出 Python 的每个字母
for letter in 'Python':
    if letter == 'h':
        pass
        print('这是 pass 块')
    print('当前字母 :', letter)

print("Good bye!")
