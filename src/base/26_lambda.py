#!/usr/bin/env python
# -*- coding: utf-8 -*-


# http://www.pythondoc.com/pythontutorial3/controlflow.html#tut-defining
# -------------------------------------------------------------------------------
# lambda 表达式返回一个函数
def make_incrementor(n):
    return lambda x: x + n


f = make_incrementor(42)
print(f(0))
print(f(1))
# -------------------------------------------------------------------------------
# 将一个小函数作为参数传递
pairs = [(1, 'one'), (2, 'two'), (3, 'three'), (4, 'four')]
pairs.sort(key=lambda pair: pair[1])
print(pairs)
