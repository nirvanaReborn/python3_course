#! /usr/bin/python 
# -*- coding: UTF-8 -*-

"""
Python有一个 time 和 calendar 模组可以帮忙。


时间间隔是以秒为单位的浮点小数。
每个时间戳都以自从1970年1月1日午夜（历元）经过了多长时间来表示。
Tick单位最适于做日期运算。
但是1970年之前的日期就无法以此表示了。
太遥远的日期也不行，UNIX和Windows只支持到2038年某日。

"""
# -------------------------------------------------------------------------------
import time  # This is required to include time module.

ticks = time.time()
print("Number of ticks since 12:00am, January 1, 1970:", ticks)

# 获取当前时间
localTime = time.localtime(ticks)
print("Local current time :", localTime)

# 获取格式化的时间
formatLocalTime = time.asctime(localTime)
print("Local format current time :", formatLocalTime)

print(time.strftime("%Y-%m-%d %H:%M:%S"))  # 2017-09-16 14:33:28
print(time.strftime("%Y%m%d"))  # 20170916
print(time.strftime("%H%M%S"))  # 143328
# -------------------------------------------------------------------------------
# 获取某月日历
import calendar

cal = calendar.month(2008, 1)
print("Here is the calendar:")
print(cal)
# -------------------------------------------------------------------------------
