#! /usr/bin/python 
# -*- coding: UTF-8 -*-

"""
参数类型:
必备参数
命名参数
缺省参数
不定长参数
"""


# --------------------------------必备参数---------------------------------------
# 可写函数说明
def printme(str):
    "打印任何传入的字符串"
    print(str)
    return


# --------------------------------命名参数---------------------------------------
# 可写函数说明
def printinfo(name, age):
    "打印任何传入的字符串"
    print("Name: ", name)
    print("Age ", age)
    return


# --------------------------------缺省参数---------------------------------------
# 可写函数说明
def printinfo_1(name, age=35):
    "打印任何传入的字符串"
    print("Name: ", name)
    print("Age ", age)
    return


# -------------------------------不定长参数--------------------------------------
# 可写函数说明
def printinfo_2(arg1, *vartuple):
    "打印任何传入的参数"
    print("输出: ")
    print(arg1)
    for var in vartuple:
        print(var)
    return


# 调用printinfo 函数
printinfo_1(10)
printinfo_2(70, 60, 50)


# -------------------------------传递函数指针------------------------------------
def minus(a, b):
    return a - b


def compute(a, b, fun):
    result = fun(a, b)
    print(result)


# ------------------------------计时函数------------------------------------------

def timer(function):
    import time
    begintime = time.time()
    # 执行函数
    function()
    endtime = time.time()
    usetime = endtime - begintime
    print('\n', '*' * 40)
    print("执行脚本总用时 %s 秒" % usetime)


# -------------------------------文档字符串--------------------------------------
def my_function():
    """
    Do nothing, but document it.
    No, really, it doesn't do anything.
    """
    pass


def main():
    # 调用printme()函数，你必须传入一个参数，不然会出现语法错误
    printme(1)

    # 调用printinfo函数
    printinfo(age=50, name="miki")
    printinfo_1(name="miki")

    compute(1, 1, minus)

    # 文档字符串
    print(my_function.__doc__)


if __name__ == "__main__":
    main()
    # 计时函数
    timer(main)
