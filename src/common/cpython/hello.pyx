# https://www.jianshu.com/p/cfcc2c04a6f5
# http://www.cpython.org/start.html

import sys
import os


def say_hello_to(name):
    print("Hello %s!" % name)
    # sys.stdout.write("Hello %s!" % name)

    # 通常会缓冲输出以保留系统资源。这意味着在这种情况下，系统会保留Python输出，直到可以一起释放为止。
    # 为了克服这个问题，您可以显式“刷新”输出：
    sys.stdout.flush()

def is_file_exists(file_path):
    if os.path.exists(file_path):
        print(file_path)




