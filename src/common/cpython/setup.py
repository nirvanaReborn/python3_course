#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

# from distutils.core import setup
# from distutils import sysconfig
# setuptools是distutils的增强版。(Python3.10版本中distutils 被弃用)
from setuptools import setup, Extension, find_packages
import Cython
from Cython.Build import cythonize
import os
import stat
import traceback
import numpy


def py2c(source_file):
    ext_module = Extension(
        os.path.splitext(os.path.basename(source_file))[0],
        language="c++",
        sources=[source_file],
        include_dirs=[numpy.get_include()],
        library_dirs=[],
        libraries=[],
        extra_compile_args=["-std=c++11"] if os.name == "posix" else [],
        extra_link_args=[],
    )

    if source_file.endswith(".py") or source_file.endswith(".pyx"):
        module_list = cythonize(ext_module, language_level=3)
    else:
        module_list = [ext_module]

    setup(
        name="Hello world app",  # 包名称
        version="1.0.0.0",  # 包版本号
        description='A simaple demo',
        author="zt",  # 作者
        author_email="xxx@xxx.com",  # 作者邮箱
        ext_modules=module_list,  # 扩展模块
        packages=find_packages(),  # 动态获取packages
        # cmdclass={'build_ext': BuildExt},
    )


def main():
    source_file = "hello.pyx"
    try:
        py2c(source_file)
    except Exception as e:
        print(source_file, e)
        traceback.print_exc()
    finally:
        filepath, basename = os.path.split(source_file)
        shotname, _ = os.path.splitext(basename)
        cpp_file = os.path.join(filepath, shotname + ".cpp")
        if os.path.exists(cpp_file):
            os.remove(cpp_file)

        # build_dir = os.path.join(filepath, "build")
        # if os.path.exists(build_dir):
        #     os.chmod(build_dir, stat.S_IWRITE)  # windows下取消只读
        #     os.remove(build_dir)


if __name__ == '__main__':
    print(Cython.__version__)
    main()

# 1.首先在win命令行创建一个编译环境，在cmd窗口运行一个现成的vcvars64.bat文件即可,一般与cl.exe在同一个目录下面。
# call "D:\ProgramFiles\Microsoft Visual Studio\2015\VC\bin\vcvars32.bat"
# call "D:\ProgramFiles\Microsoft Visual Studio\2015\VC\bin\amd64\vcvars64.bat"
# call "D:\ProgramFiles\Microsoft Visual Studio\2019\Enterprise\VC\Auxiliary\Build\vcvarsall.bat" x86
# call "D:\ProgramFiles\Microsoft Visual Studio\2019\Enterprise\VC\Auxiliary\Build\vcvarsall.bat" x64
# 2.其次执行以下命令进行编译
# python setup.py build_ext --inplace
