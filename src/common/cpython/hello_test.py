#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 这个import会先找hello.py，找不到就会找hello.so
import hello  # 导入了hello.so


def test_1():
    hello.say_hello_to('张三')


def test_2():
    print("test", flush=True)


def test_3():
    hello.is_file_exists(r'D:\迅雷下载\对接期货CTP\6.7.2_API.chm')


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
