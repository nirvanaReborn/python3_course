#!/usr/bin/env python
# -*- coding:utf-8 -*-

import collections
import operator


def test_1():
    # 第一种：初始化空字典
    mydict = {}
    mydict['one'] = "This is one"
    mydict[2] = "This is two"
    mydict[3] = "This is three"

    print(mydict)  # 输出完整的字典 {'one': 'This is one', 2: 'This is two', 3: 'This is three'}
    print(mydict.keys())  # 输出所有键 ['one', 2, 3]
    print(mydict.values())  # 输出所有值 ['This is one', 'This is two', 'This is three']

    # sorted(dict.keys())

    print(mydict['one'])  # 输出键为'one' 的值
    print(mydict[2])  # 输出键为 2 的值

    del mydict[2]  # 删除键为 2 的值
    print(mydict)  # 输出完整的字典 {'one': 'This is one', 3: 'This is three'}
    print(mydict.keys())  # 输出所有键 dict_keys(['one', 3])
    print(mydict.values())  # 输出所有值 dict_values(['This is one', 'This is three'])

    print(mydict.setdefault("two", 222))  # 如果键不在字典中，新的默认值会被添加进去
    print(mydict.setdefault(3, "change"))  # 如果试图把一个不同的默认值赋给已经存在的键，不会改变已有的值，仍将返回初始值

    print(mydict.get(2, 0))  # 如果该键不存在时，返回的备用值

    # 第二种：通过fromkeys创建值为空的字典
    mydict_2 = dict.fromkeys(['name', 'age', 'job'])
    print(mydict_2)

    # 第三种：通过zip()创建字典对象
    k = ['name', 'age', 'job']
    v = ['gaoqi', 18, 'techer']
    mydict_3 = dict(zip(k, v))
    print(mydict_3)


def test_2():
    def dict_revert(one_dict):
        return dict((v, k) for k, v in one_dict.items())

    mydict = {'name': 'john', 'code': 6734, 'dept': 'sales'}
    print(dict_revert(mydict))


# 访问字典
def test_3():
    mydict = {'name': 'john', 'code': 6734, 'dept': 'sales'}

    for key in mydict.keys():
        print(key)

    for value in mydict.values():
        print(value)

    for item in mydict.items():
        print(item)


# 遍历字典
def test_4():
    mydict = {'name': 'john', 'code': 6734, 'dept': 'sales'}
    # 遍历字典方法1
    for key in mydict:
        print(key, mydict[key])

    # 遍历字典方法2
    for key, value in mydict.items():
        print(key, value)

    # 遍历字典方法3
    for i, key in enumerate(mydict):
        print(i, key, mydict[key])


# 判断字典是否存在某个key或value
def test_5():
    mydict = {'name': 'john', 'code': 6734, 'dept': 'sales'}
    if 'age' in mydict.keys():
        print("tinydict包含age")
    else:
        print("tinydict不包含age")

    if 'john' in mydict.values():
        print("tinydict包含john")
    else:
        print("tinydict不包含john")


# ---------------------------------------------------------------------------------------------------
# 对字典排序
def test_6():
    # sorted函数按key值对字典排序
    mydict = {'name': 'john', 'code': 6734, 'dept': 'sales'}
    dict_sort = dict(sorted(mydict.items(), key=lambda d: d[0]))
    # dict_sort = dict(sorted(mydict.items(), key=operator.itemgetter(0)))
    # zip函数默认会对第一个元素进行排序的
    # dict_sort = dict(sorted(zip(mydict.keys(), mydict.values())))
    print("按key值排序", dict_sort)

    # sorted函数按value值对字典排序
    mydict = {'name': 520, 'code': 9527, 'dept': 1314}
    list_sort = sorted(mydict.items(), key=lambda d: d[1])
    # list_sort = sorted(mydict.items(), key=lambda d: str(d[1]), reverse=True)
    dict_sort = dict(list_sort) # 排序后最好不要再转化为字典
    print("按value值排序", dict_sort)

    # key=operator.itemgetter(1):获取下标为1的元素
    list_sort = sorted(mydict.items(), key=operator.itemgetter(1))
    # zip函数默认会对第一个元素进行排序的
    # list_sort = sorted(zip(mydict.values(), mydict.keys()))
    print("按value值排序", list_sort)


# 有序字典OrderedDict(): 可以按字典中元素的插入顺序来输出。
def test_7():
    quotes = collections.OrderedDict([
        ('Moe', 'A wise guy, huh?'),
        ('Larry', 'Ow!'),
        ('Curly', 'Nyuk nyuk!'),
    ])

    for stooge in quotes:
        print(stooge)

    # ---------------------------
    dd = {'banana': 3, 'apple': 4, 'pear': 1, 'orange': 2}
    # 按key排序
    kd = collections.OrderedDict(sorted(dd.items(), key=lambda t: t[0]))
    print(kd)

    # 按照value排序
    vd = collections.OrderedDict(sorted(dd.items(), key=lambda t: t[1]))
    print(vd)

    # 输出
    print(collections.OrderedDict([('apple', 4), ('orange', 2), ('banana', 3),  ('pear', 1)]))
    print(collections.OrderedDict([('pear', 1), ('banana', 3), ('orange', 2), ('apple', 4)]))


# ---------------------------------------------------------------------------------------------------
def test_8():
    # dict() 构造函数可以直接从 key-value 对中创建字典
    print(dict([('sape', 4139), ('guido', 4127), ('jack', 4098)]))

    # 如果关键字都是简单的字符串，有时通过关键字参数指定 key-value 对更为方便
    print(dict(sape=4139, guido=4127, jack=4098))

    # 字典推导式可以从任意的键值表达式中创建字典
    print({x: x ** 2 for x in (2, 4, 6)})


# 函数defaultdict() 的参数是一个函数，它返回赋给缺失键的值。
def test_9():
    # 创建默认值为列表的字典
    dict_list = collections.defaultdict(list)
    s = [('Tom', 5), ('Jone', 2), ('Susan', 4), ('Tom', 4), ('Tom', 1)]
    for i, j in s:
        # list(dict_list[i]).append(j) # 这样写会导致出错：[('Tom', []), ('Jone', []), ('Susan', [])]
        dict_list[i].append(j)
    print(list(dict_list.items()))

    # 创建默认值为整数的字典
    periodic_table = collections.defaultdict(int)
    periodic_table['Hydrogen'] = 1
    periodic_table['Lead']  # 现在，任何缺失的值将被赋为整数0
    print(periodic_table)


# ---------------------------------------------------------------------------------------------------

# http://blog.csdn.net/jerry_1126/article/details/73017270
# Python中字典合并的四种方法
def test_10():
    dict_1 = {"user": "root", "pwd": "1234", "port": "1000"}
    dict_2 = {"ip": "127.0.0.1", "port": "8080"}

    # 方法1：借助字典的update()方法
    dict_3 = {}
    dict_3.update(dict_1)  # 或者 dict_3 = dict_1.copy()
    dict_3.update(dict_2)
    print(dict_3)

    # 方法2：借助dict(d1.items() + d2.items())的方法
    # dict_4 = dict(dict_1.items() + dict_2.items())
    # print(dict_4)

    # 方法3：借助字典的dict(d1, **d2)方法
    dict_5 = dict(dict_1, **dict_2)
    print(dict_5)

    # 方法4：借助字典的常规处理方法
    dict_6 = {}
    for key, value in dict_1.items():
        dict_6[key] = value
    for key, value in dict_2.items():
        dict_6[key] = value
    print(dict_6)

    # 方法5：原地合并
    dict_1.update(dict_2)
    print(dict_1)


# ---------------------------------------------------------------------------------------------------
# https://blog.csdn.net/uuihoo/article/details/79496440
# 删除字典元素的4种方法
def test_11():
    # 方法1：del 全局方法（能删单一的元素也能清空字典，清空只需一项操作）
    d = {'a': 1, 'b': 2, 'c': 3}
    # 删除给定key的元素
    del d['a']
    print(d)
    # 删除不存在的元素,会报错
    # del d['m']
    # 清空字典所有条目
    del d

    # 方法2：借助Python字典的pop()方法（删除字典给定键 key 所对应的值，返回值为被删除的值）
    d = {'a': 1, 'b': 2, 'c': 3}
    # 删除key值为'a'的元素，并赋值给变量e1
    e1 = d.pop('a')
    print(e1)
    # 如果key不存在，则可以设置返回值
    e2 = d.pop('m', '404')
    print(e2)
    # 如果key不存在，不设置返回值就报错
    e3 = d.pop('m')

    # 方法3：Python字典的popitem()方法（随机返回并删除字典中的一对键和值）
    d = {'a': 1, 'b': 2, 'c': 3}
    # 第一次运行
    d.popitem()
    print(d)
    # 第二次运行
    d.popitem()
    print(d)
    # 第三次运行
    d.popitem()
    print(d)

    # 方法4：借助Python字典的clear()方法（删除字典内所有元素）
    dict = {'name': '我的博客地址', 'alexa': 10000, 'url': 'http://blog.csdn.net/uuihoo/'}
    dict.clear()  # 清空词典所有条目


# ---------------------------------------------------------------------------------------------------
# https://www.cnblogs.com/nxf-rabbit75/p/11088925.html
# 直接将字典转为DataFrame格式
def test_12():
    # 方法1：直接在创建DataFrame时设置index
    import pandas as pd
    dict_data = {'name': 'nxf', 'age': 24}
    data = pd.DataFrame(dict_data, index=[0])
    print(data)

    # 方法2：通过from_dict函数将value为标称变量的字典转换为DataFrame对象
    data = pd.DataFrame.from_dict(dict_data, orient='index')
    print(data)

    # 方法3：通过from_dict函数将value为标称变量的字典转换为DataFrame对象
    data = pd.DataFrame.from_dict(dict_data, orient='index').T
    print(data)

    # 方法4：直接将key和value取出来，都转换成list对象
    print(list(dict_data.items()))
    data = pd.DataFrame(list(dict_data.items()))
    print(data)


# 字典与json互转
def test_13():
    import json
    data1 = {'no': 1, 'name': 'Runoob', 'url': 'http://www.runoob.com'}

    # Python 字典类型转换为 JSON 对象
    json_str = json.dumps(data1)
    print("Python 原始数据：", repr(data1))
    print("JSON 对象：", json_str)

    # 将 JSON 对象转换为 Python 字典
    data2 = json.loads(json_str)
    print("data2['name']: ", data2['name'])
    print("data2['url']: ", data2['url'])


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
