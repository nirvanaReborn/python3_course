#!/usr/bin/env python
# -*- coding: UTF-8 -*-


# http://www.pythondoc.com/pythontutorial3/datastructures.html
# 堆栈作为特定的数据结构，最先进入的元素最后释放（先进后出）。
# 列表方法使得列表可以很方便的做为一个堆栈来使用，堆栈作为特定的数据结构，最先进入的元素最后一个被释放（后进先出）。
# 用 append() 方法可以把一个元素添加到堆栈顶。
# 用不指定索引的 pop() 方法可以把一个元素从堆栈顶释放出来。


def test_1():
    stack = [3, 4, 5]

    stack.append(6)
    stack.append(7)
    print(stack)  # [3, 4, 5, 6, 7]

    print(stack.pop())  # 7
    print(stack)  # [3, 4, 5, 6]

    print(stack.pop())  # 6
    print(stack.pop())  # 5
    print(stack)  # [3, 4]


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
