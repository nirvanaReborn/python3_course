#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://www.cnblogs.com/emanlee/p/3616755.html
# python字符串函数
# 字符串中的单引号，双引号用 \ 来转义。

import ast
import io
import operator
import random
import string
import time


def test_1():
    # 生成字符串变量
    sString = 'python string function'

    # 字符串长度获取：len(sString)
    print('%s length=%d' % (sString, len(sString)))

    # 连接字符串
    sStr1 = 'strcat'
    sStr2 = 'append'
    sStr1 += sStr2
    print(sStr1)

    # 复制字符串 strcpy(sStr1,sStr2)
    sStr3 = 'strcpy'
    sStr4 = sStr3  # 指向同一块内存
    sStr3 = 'strcpy2'  # 新开辟一块内存
    print(sStr3)
    print(sStr4)


# 比较字符串 strcmp(sStr1,sStr2)
# 在python2中我们经常会使用cmp函数来比较一些东西，但是在python3中使用operator.__eq__(a, b)
# python3中函数的返回值是布尔值。
def test_2():
    sStr1 = 'strchr'
    sStr2 = 'strch'
    # 注意cmp函数的输出结果和C语言的strcmp是一样的。还可以用 “aaa”==“bbb” 来比较字符串。
    # print(cmp(sStr1,sStr2)) # python2 支持

    # python3 支持
    print(operator.eq(sStr1, sStr2))  # 相当于 a == b
    print(operator.ne(sStr1, sStr2))  # 相当于 a != b
    print(operator.lt(sStr1, sStr2))  # 相当于 a < b
    print(operator.le(sStr1, sStr2))  # 相当于 a <= b
    print(operator.ge(sStr1, sStr2))  # 相当于 a>= b
    print(operator.gt(sStr1, sStr2))  # 相当于 a > b


# 截取字符串
# 特别注意：下标从0开始；sString[0:3]不包含下标为3的字符。
def test_3():
    sString = '0123456789'
    print(sString[0:3])  # 截取第一位到第三位的字符
    print(sString[:])  # 截取字符串的全部字符
    print(sString[6:])  # 截取第七个字符到结尾
    print(sString[:-3])  # 截取从头开始到倒数第三个字符之前
    print(sString[2])  # 截取第三个字符
    print(sString[-1])  # 截取倒数第一个字符
    print(sString[::-1])  # 创造一个与原字符串顺序相反的字符串
    print(sString[-3:-1])  # 截取倒数第三位与倒数第一位之前的字符
    print(sString[-3:])  # 截取倒数第三位到结尾
    print(sString[:-5:-3])  # 逆序截取，具体啥意思没搞明白？


# 字符串中的搜索和替换：
# S.find(substr, [start, [end]])   #返回S中出现substr的第一个字母的标号，如果S中没有substr则返回-1。start和end作用就相当于在S[start:end]中搜索
# S.rfind(substr, [start, [end]])   #返回S中最后出现的substr的第一个字母的标号，如果S中没有substr则返回-1，也就是说从右边算起的第一次出现的substr的首字母标号
# S.index(substr, [start, [end]])   #与find()相同，只是在S中没有substr时，会返回一个运行时错误
# S.rindex(substr, [start, [end]])
# S.count(substr, [start, [end]])    #计算substr在S中出现的次数
# S.replace(oldstr, newstr, [count])    #把S中的oldstr替换为newstr，count为替换次数。这是替换的通用形式，还有一些函数进行特殊字符的替换
# S.strip([chars]) #把S中前后chars中有的字符全部去掉，可以理解为把S前后chars替换为None
# S.lstrip([chars])
# S.rstrip([chars])
# S.expandtabs([tabsize])   #把S中的tab字符替换为空格，每个tab替换为tabsize个空格，默认是8个
def test_4():
    sString = 'python string function'
    print(sString.find('on'))
    print(sString.index('on'))
    print(sString.count('on'))
    print(sString.replace('on', 'new'))
    print(sString.expandtabs())
    print(sString.strip('p'))

    # 字符串去空格及去指定字符
    sString = ' python string function '
    print(sString.strip())


# 字符串的拼接和分割：
# S.join(seq) #把seq代表的序列──字符串序列，用S连接起来
# S.split([sep, [maxsplit]]) #以sep为分隔符，把S分成一个list。maxsplit表示分割的次数。默认的分割符为空白字符
# S.rsplit([sep, [maxsplit]])
# S.splitlines([keepends]) #把S按照行分割符分为一个list，keepends是一个bool值，如果为真每行后而会保留行分割符。
def test_5():
    # 使用字符串拼接符+，会生成新的字符串对象，因此不推荐使用+来拼接字符串。
    # 推荐使用join 函数，因为join 函数在拼接字符串之前会计算所有字符串的长度，然后逐一拷贝，仅新建一次对象。
    time01 = time.time()  # 起始时刻
    a = ""
    for i in range(1000000):
        a += "sxt"

    time02 = time.time()  # 终止时刻

    print("运算时间：" + str(time02 - time01))
    # --------------------------------------------
    time03 = time.time()  # 起始时刻
    li = []
    for i in range(1000000):
        li.append("sxt")

    a = "".join(li)

    time04 = time.time()  # 终止时刻

    print("运算时间：" + str(time04 - time03))


# 字符串的mapping，这一功能包含两个函数：
# S.maketrans(from, to) #返回一个256个字符组成的翻译表，其中from中的字符被一一对应地转换成to，所以from和to必须是等长的。
# S.translate(table[,deletechars]) # 使用上面的函数产后的翻译表，把S进行翻译，并把deletechars中有的字符删掉。
#                               需要注意的是，如果S为unicode字符串，那么就不支持 deletechars参数，可以使用把某个字符翻译为None的方式实现相同的功能。
#                               此外还可以使用codecs模块的功能来创建更加功能强大的翻译表。
def test_6():
    pass


# 字符串中字符大小写的变换：
# S.lower()         # 产生新的字符串,所有字符全转成小写
# S.upper()         # 产生新的字符串,所有字符全转成大写
# S.swapcase()      # 产生新的字符串,所有字符大小写互换
# S.capitalize()    # 产生新的字符串,首字母大写
# S.title()         # 每个单词都首字母大写(string模块中没有这个方法)
# string.capwords(S)     # 把字符里每一个英语单词首字母变大写(这是string模块中的方法)

def test_7():
    pass


# 字符串编码和解码的函数：
# S.encode([encoding,[errors]]) # 其中encoding可以有多种值，比如gb2312 gbk gb18030 bz2 zlib big5 bzse64等都支持。
#                                   errors默认值为"strict"，意思是UnicodeError。可能的值还有'ignore', 'replace', 'xmlcharrefreplace', 'backslashreplace'
#                                   和所有的通过codecs.register_error注册的值。这一部分内容涉及codecs模块，不是特明白
# S.decode([encoding,[errors]])
def test_8():
    pass


# 字符串的测试函数，这一类函数在string模块中没有，这些函数返回的都是bool值：
# S.startwith(prefix[,start[,end]]) # 是否以prefix开头
# S.endwith(suffix[,start[,end]])   # 以suffix结尾
# S.isalnum()       # 是否全是字母和数字，并至少有一个字符
# S.isalpha()       # 是否全是字母，并至少有一个字符
# S.isdigit()       # 是否全是数字，并至少有一个字符
# S.isspace()       # 是否全是空白字符，并至少有一个字符
# S.islower()       # S中的字母是否全是小写
# S.isupper()       # S中的字母是否便是大写
# S.istitle()       # 是否是首字母大写的
# S.isdecimal()     # 是否为十进制
# S.isidentifier()  # 是否为合法的标识符
def test_9():
    print("ab34".isalnum())  # 判断字符串中是否全部为数字或者英文
    print("abc测试".isalpha())  # 是否为英文字母，大写也对
    print("aBc".encode('UTF-8').isalpha())  # 是否为英文字母，大写也对
    print("12".isdigit())  # 判断字符串中是否全部为整数
    print("".isspace())  # 是否为空格
    print("ab34".islower())
    print("ab34".isupper())
    print("ab34".istitle())
    print("231".isdecimal())  # 是否为十进制
    print("asd".isidentifier())  # 判断是否为合法的标识符


# 字符串转换
def test_10():
    a = "12345"
    print(a)
    print(int(a))

    b = "123.678"
    print(float(b))  # 123.678

    # https://www.cnblogs.com/OnlyDreams/p/7850920.html
    # 字符串转字典
    user = '{"name" : "john", "gender" : "male", "age": 28}'
    user_dict = ast.literal_eval(user)
    print(user_dict)


# 格式排版：
# S.ljust(width,[fillchar])     #输出width个字符，S左对齐，不足部分用fillchar填充，默认的为空格。
# S.rjust(width,[fillchar])     #右对齐
# S.center(width, [fillchar])   #中间对齐
# S.zfill(width)                #把S变成width长，并在右对齐，不足部分用0补足
# format()
def test_11():
    a = "名字是:{0},年龄是：{1}"
    print(a.format("高淇", 18))

    b = "名字是：{0}，年龄是{1}。{0}是个好小伙"
    print(b.format("高淇", 18))

    c = "名字是{name}，年龄是{age}"
    print(c.format(age=19, name='高淇'))

    # 填充常跟对齐一起使用
    # ^、<、>分别是居中、左对齐、右对齐，后面带宽度
    # :号后面带填充的字符，只能是一个字符，不指定的话默认是用空格填充
    print("{:*>8}".format("245"))
    print("我是{0},我喜欢数字{1:*^8}".format("高淇", "666"))

    sql_str = '''select * 
                   from hs_asset.fundaccount a
                  where a.fund_account = {0}'''.format(1234)
    print(sql_str)


# S.format_map 将字典中的参数传递进字符串中
def test_12():
    hello = "My name is {name},I am {age} years old.I like {hobby}"
    # 使用format_map方法来传递值
    print(hello.format_map({'name': 'yanyan', 'age': 19, 'hobby': 'music travel'}))


def test_13():
    name = "my name is zhengliyuan"
    name1 = "my name is {name} and nmy age is {year}"
    print(name.capitalize())  # 首字母大写
    print(name.count("a"))  # 字符串中a出现的次数
    print(name.center(50, "-"))  # 50格输出，不够的补-
    print(name.endswith("yuan"))  # 判断是否以某个结尾
    print(name.find("name"))
    print(name1.format(name='zhengliyuan', year=23))
    print(name.ljust(50, "*"))  # 从左面开始输出50位，不够的补*
    print(name.rjust(50, "*"))  # 从右面开始输出50位，不够的补*
    print(name.lower())  # 转化为小写
    print(name.upper())  # 转化为大写
    name2 = str.maketrans("abcdef", "123456")  # 加密规则
    print("zheng li yuan".translate(name2))  # 按照上面规则，将zhengliyuan转化
    print("zhengliyuan".replace('l', 'L', 1))  # 替换，最后一个数字是代表替换几个
    print("zhengliyuan".rfind('n'))  # 找最右边符合规则的下标
    print("zheng li yuan".split())  # 按照规则分割字符串
    print("Zheng Li Yuan".swapcase())  # 大小写转化
    print("zheng li yuan".title())  # 转换成title格式，即首字符大写
    print("zheng li yuan".zfill(50))  # 返回指定长度的字符串，原字符串右对齐，前面填充0


# ---------------------------------------------------------------------------------------------------
# https://www.toutiao.com/a6540366167121330702/
# 超详细的字符串驻留
# 字符串驻留：仅保存一份相同且不可变字符串的方法，不同的值被存放在字符串驻留池中。(不同的变量指向同一个内存地址)
# Python 支持字符串驻留机制，对于符合标识符规则的字符串（仅包含下划线（_）、字母和数字）会启用字符串驻留机制驻留机制。
# 字符串驻留的优点:1.显而易见，节省大量内存;2.在字符串比较时，非驻留比较效率o(n)，驻留时比较效率o(1)。


# 字符串只在编译时进行驻留，而非运行时。
def test_14():
    a = "hello" + "world"
    print("compile : ", a is "helloworld")  # True

    a = "hello"
    b = "world"
    print("run : ", a + b is "helloworld")  # False


# 字符串长度为0和1时，默认都采用了驻留机制。
# 字符串>1时，且只含大小写字母、数字、下划线时，才会默认驻留。
def test_15():
    a = "!"
    b = "!"
    print("len=1 : ", a is b)

    a = "hello_"
    b = "hello_"
    print("len>1 : ", a is b)

    a = "hello!"
    b = "hello!"
    print("len>1 : ", a is b)


# 乘数为1，默认驻留
def test_16():
    a = "abcdef"
    b = a * 1
    print(a is b)  # True

    a = "#"
    b = a * 1
    print(a is b)  # True

    a = "$$"
    b = a * 1
    print(a is b)  # True


def test_17():
    # 乘数>=2时,只含大小写字母、数字、下划线且总长度<=20时，默认驻留。
    a = "hellohellohellohello"
    b = "hello" * 4
    print(a is b)  # True

    # 乘数>=2时,只含大小写字母、数字、下划线且总长度>20时，默认都不驻留。
    a = "X1_X1_X1_X1_X1_X1_X1_"
    b = "X1_" * 7
    print(a is b)  # False

    # 含有其他字符时，和长度无关，乘数>=2时,默认不驻留。
    a = "$$"
    b = a * 2
    print(a is b)  # False


def test_18():
    import sys
    a = sys.intern("hello!@")
    b = sys.intern("hello!@")
    print("intern 指定驻留:", a is b)  # True


# 此外， 对于[-5,256]之间的整数数字，Python默认驻留。
def test_19():
    print(id([1, 2, 3]))
    print(id([4, 5, 6]))
    print(id([1, 2, 3]) == id([4, 5, 6]))  # True


# ---------------------------------------------------------------------------------------------------
def test_20():
    print(string.ascii_letters)  # 获取所有ascii码中字母字符的字符串（包含大写和小写）:abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
    print(string.ascii_lowercase)  # 获取所有ascii码中的小写英文字母: abcdefghijklmnopqrstuvwxyz
    print(string.ascii_uppercase)  # 获取所有ascii码中的大写英文字母: ABCDEFGHIJKLMNOPQRSTUVWXYZ
    print(string.digits)  # 获取所有的10进制数字字符: 0123456789
    print(string.hexdigits)  # 获取所有16进制的数字字符: 0123456789abcdefABCDEF
    print(string.octdigits)  # 获取所有的8进制数字字符: 01234567
    print(string.printable)  # 获取所有可以打印的字符: 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
    print(string.punctuation)  # 获取所有的标点符号: !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
    print(string.whitespace)  # 获取所有空白字符 


# ---------------------------------------------------------------------------------------------------
# 生成一个指定长度的随机字符串
def test_21():
    def generate_randome_str(randomLength=16):
        random_str = ""
        base_str = string.digits + string.ascii_letters
        for i in range(randomLength):
            random_str += base_str[random.randint(0, len(base_str) - 1)]
        return random_str

    print(generate_randome_str(24))


def test_22():
    def generate_randome_str(randomLength=16):
        base_str = string.digits + string.ascii_letters
        random_list = [random.choice(base_str) for i in range(randomLength)]
        random_str = ''.join(random_list)
        return random_str

    print(generate_randome_str(24))


# ---------------------------------------------------------------------------------------------------
# is 判断两个对象是否为同一对象, 是通过调用id()来判断的; 当两个基本类型数据(或元组)内容相同时, id会相同, 但并不代表a会随b的改变而改变。
# == 判断两个对象的内容是否相同, 是通过调用eq()来判断的
def test_23():
    a = "123"
    b = "123"
    print(id(a), id(b))  # 2265844909312 2265844909312
    print(a == b)  # True
    print(a is b)  # True


# ---------------------------------------------------------------------------------------------------
# 在Python 中，字符串属于不可变对象，不支持原地修改，如果需要修改其中的值，智能创建新的字符串对象。
# 但是，经常我们确实需要原地修改字符串，可以使用io.StringIO对象或array 模块。
def test_24():
    s = "hello, sxt"
    sio = io.StringIO(s)
    print(sio)  # <_io.StringIO object at 0x0000020DC65AF708>
    print(sio.getvalue())  # hello, sxt
    print(sio.seek(7))  # 7
    print(sio.write("g"))  # 1
    print(sio.getvalue())  # hello, gxt


# ---------------------------------------------------------------------------------------------------
# python3 之 判断字符串是否只为数字
# isnumeric：检测字符串是否只由数字组成,这种方法只是针对Unicode对象。
# 注：定义一个字符串为Unicode，只需要在字符串前添加'u'前缀即可。
def test_25():
    string = '123456'
    print(string.isdigit())  # True
    string = "耿雨飞"
    print(string.isdigit())  # False

    string = u"runoob2016"
    print(string.isnumeric())  # False
    string = u"23443434"
    print(string.isnumeric())  # True


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
