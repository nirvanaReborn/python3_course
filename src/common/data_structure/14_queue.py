#!/usr/bin/env python
# -*- coding: UTF-8 -*-


# http://www.pythondoc.com/pythontutorial3/datastructures.html
# 队列作为特定的数据结构，最先进入的元素最先释放（先进先出）。
# 你也可以把列表当做队列使用，不过列表这样用效率不高。
# 相对来说从列表末尾添加和弹出很快；在头部插入和弹出很慢（因为，为了一个元素，要移动整个列表中的所有元素）。
#
# 要实现队列，使用 collections.deque，它为在首尾两端快速插入和删除而设计。
import collections


def test_1():
    # deque它提供了两端都可以操作的序列，这意味着，在序列的前后你都可以执行添加或删除操作。
    queue = collections.deque()  # 创建空队列
    print(queue)

    # 限制deque的长度
    queue = collections.deque(maxlen=5)
    for i in range(10):
        queue.append(str(i))
    print(queue)  # deque(['5', '6', '7', '8', '9'], maxlen=5)
    # 可见当限制长度的deque增加超过限制数的项时，另一边的项会自动删除。

    # 默认pop()抛出的是最后一个元素。
    queue = collections.deque('12345')
    print(queue)  # deque(['1', '2', '3', '4', '5'])
    queue.pop()
    print(queue)  # deque(['1', '2', '3', '4'])

    queue = collections.deque(["a", "b", "c"])
    queue.append("d")
    queue.append("e")
    print(queue)  # deque(['a', 'b', 'c', 'd', 'e'])
    print(queue.popleft())  # a
    print(queue.popleft())  # b
    print(queue)  # deque(['c', 'd', 'e'])

    queue = collections.deque([1, 2, 3, 4, 5])
    queue.extend([0])
    print(queue)  # deque([1,2,3,4,5,0])

    queue.extendleft([6, 7, 8])
    print(queue)  # deque([8, 7, 6, 1, 2, 3, 4, 5, 0])


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
