#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# 简析python中的集合
# http://www.iplaypy.com/jichu/set.html
# http://www.cnblogs.com/AaronFan/p/6057263.html
#
# 集合是一个无序的，不重复的数据组合，它的主要作用如下：
# 1.去重，把一个列表变成集合，就自动去重了
# 2.关系测试，测试两组数据之前的交集、差集、并集等关系
# 3.集合对象还支持union(联合), intersection(交), difference(差)和sysmmetric difference(对称差集)等数学运算.
#
# sets 支持 x in set, len(set),和 for x in set。
# 作为一个无序的集合，sets不记录元素位置或者插入点。
# 因此，sets不支持 indexing, slicing, 或其它类序列（sequence-like）的操作。
#
# 集合(sets)有两种不同的类型：可变集合(set)和不可变集合(frozenset)。
# 对可变集合(set)，可以添加和删除元素，对不可变集合(frozenset)则不允许这样做。
#     注意：可变集合(set)不是可哈希的，因此既不能用做字典的键也不能做其他集合中的元素。
# 不可变集合(frozenset)则正好相反，即它们有哈希值，能被用做字典的键或是作为集合中的一个成员。


# 基本操作
def test_1():
    # 初始化集合
    myset = set()
    print(myset)  # set()

    # 创建集合set
    myset = set('boy')  # 创建一个唯一字符的集合
    myset_1 = set([3, 5, 9, 10])  # 创建一个数值集合
    print(myset)  # {'y', 'b', 'o'}

    # 集合添加——集合add方法：是把要传入的元素做为一个整个添加到集合中
    myset.add('python')
    print(myset)  # {'y', 'python', 'b', 'o'}

    myset_1.add('python')
    print(myset_1)  # {3, 5, 9, 'python', 10}

    # 集合添加——集合update方法：是把要传入的元素拆分，做为个体传入到集合中
    myset.update('python')
    print(myset)  # {'python', 'o', 'p', 't', 'h', 'b', 'y', 'n'}

    myset_1.update([10, 37, 42])
    print(myset_1)  # {3, 5, 37, 9, 'python', 10, 42}

    # 集合删除
    myset.remove('python')  # 有就删除，没有就报错
    myset.pop()  # 随机弹出一个
    myset.discard('H')  # 有就删除，没有也不会报错
    print(myset)  # {'o', 'p', 't', 'h', 'b', 'y', 'n'}

    # 返回集合的一个浅复制
    print(myset.copy())  # {'b', 'y', 't', 'h', 'p', 'o'}

    # set 的长度
    print(len(myset))  # 6

    # 对集合排序
    print(sorted(list(myset)))  # ['b', 'h', 'o', 'p', 't', 'y']


def test_2():
    list_1 = [1, 3, 4, 7, 3, 6, 7, 9]
    list_1 = set(list_1)  # 去重
    list_2 = set([2, 6, 0, 66, 22, 8, 4])
    list_3 = set([1, 3, 7])
    print(list_1)
    print(list_2)
    print(list_3)
    print("---------------------------")
    # 关系测试

    # 交集(两个列表里面都有的值，这里是4、6)：
    print(list_1.intersection(list_2))
    print(list_1 & list_2)

    # 并集（把两个列别合并起来，然后去重）：
    print(list_1.union(list_2))
    print(list_1 | list_2)

    # 差集（把list_1里面有的而list_2里面没有的取出来）：
    print(list_1.difference(list_2))
    print(list_1 - list_2)

    # 对称差集（两个列表里面，互相没有的取出来，也就是只去掉那些互相都有的值）
    print(list_1.symmetric_difference(list_2))
    print(list_1 ^ list_2)

    # 是否为子集（判断list_1是否包含了list_3里面的所有值）
    print(list_3.issubset(list_1))
    print(list_3 <= list_1)

    # 是否为父集（判断list_1是否为list_3的父集）
    print(list_1.issuperset(list_3))
    print(list_1 >= list_3)

    # 是否无交集（判断list_3和list_4是否完全没有任何交集）
    list_4 = set([5, 6, 8])
    print(list_3.isdisjoint(list_4))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
