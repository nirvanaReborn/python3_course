'''

'''


# 定义二叉树的节点
class Node:
    def __init__(self, elem):
        self.elem = elem
        self.lchild = None
        self.rchild = None


class Tree:
    def __init__(self):
        self.root = None

    # 添加节点
    def add(self, elem):
        # 创建节点
        node = Node(elem)
        if self.root == None:
            self.root = node
        else:
            queue = []
            queue.append(self.root)
            while queue:
                curNode = queue.pop(0)
                if curNode.lchild == None:
                    curNode.lchild = node
                    return
                else:
                    queue.append(curNode.lchild)
                if curNode.rchild == None:
                    curNode.rchild = node
                    return
                else:
                    queue.append(curNode.rchild)



    # 先序遍历  根  左  右 0 1 3 7 8 4 9 2 5 6
    def preOrder(self, root):
        if root is None:
            return
        else:
            print(root.elem, end='\t')
            self.preOrder(root.lchild)
            self.preOrder(root.rchild)

    # 中序遍历 左  根  右  7 3 8 1 9 4  0 5 2 6
    def midOrder(self, root):
        if root is None:
            return
        else:
            self.midOrder(root.lchild)
            print(root.elem, end='\t')
            self.midOrder(root.rchild)

    # 后序遍历  左  右  根 7 8 3 9 4 1 5 6 2 0
    def lastOrder(self, root):
        if root is None:
            return
        else:
            self.lastOrder(root.lchild)
            self.lastOrder(root.rchild)
            print(root.elem, end='\t')

    # 广度优先遍历:广度优先搜索是按层来处理顶点，
    # 距离开始点最近的那些顶点首先被访问，而最远的那些顶点则最后被访问，这个和树的层序变量很像
    def travel_bfs(self):
        queue = []
        # 判断根节点是否存在
        if self.root is None:
            return
        else:
            queue.append(self.root)
        while queue:
            curNode = queue.pop(0)
            print(curNode.elem, end='\t')
            if curNode.lchild is not None:
                queue.append(curNode.lchild)
            if curNode.rchild is not None:
                queue.append(curNode.rchild)
        print()

    def travel_bfs_2(self):
        result = []
        if self.root:
            A = []
            A.append(self.root)
            while A:
                temp = []
                size = len(A)
                for Node in A:
                    temp.append(Node.elem)
                result.append(temp)
                for i in range(size):
                    node = A.pop(0)
                    if node.lchild:
                        A.append(node.lchild)
                    if node.rchild:
                        A.append(node.rchild)
        # for i in range(0, len(result)):
        #     for j in range(0, len(result[i])):
        #         print(result[i][j], end=' ' * len(result))
        #     print()
        print(result)
        return result

    def print_tree(self, root, level=0):
        pass


if __name__ == '__main__':
    tree = Tree()
    for i in range(0, 10):
        tree.add(i)
    print('先序遍历')
    tree.preOrder(tree.root)
    print()
    print('中序遍历')
    tree.midOrder(tree.root)
    print()
    print('后序遍历')
    tree.lastOrder(tree.root)
    print()
    print('广度优先遍历')
    tree.travel_bfs()
    tree.travel_bfs_2()
    print('打印树形结构')
    tree.print_tree(tree.root)

