#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://www.pythondoc.com/pythontutorial3/datastructures.html
# 列表推导式

# 列表推导式: [表达式  for  item  in  可迭代对象  if  条件判断]
# 字典推导式：{key_expression : value_expression  for  表达式  in  可迭代对象  if  条件判断}
# 集合推导式：{表达式  for  item  in  可迭代对象  if  条件判断}
# 元组是没有推导式的

# 列表推导式为从序列中创建列表提供了一个简单的方法。
# 普通的应用程式通过将一些操作应用于序列的每个成员并通过返回的元素创建列表，
# 或者通过满足特定条件的元素创建子序列。
# 列表推导式由包含一个表达式的括号组成，表达式后面跟随一个 for 子句，之后可以有零或多个 for 或 if 子句。
# 结果是一个列表，由表达式依据其后面的 for 和 if 子句上下文计算而来的结果构成。


def test_1():
    squares = []
    for x in range(10):
        squares.append(x ** 2)
    print(squares)


def test_2():
    # squares = list(map(lambda x: x**2, range(10)))
    # 或者，等价于:
    squares = [x ** 2 for x in range(10)]
    print(squares)


def test_3():
    squares = [(x, y) for x in [1, 2, 3] for y in [3, 1, 4] if x != y]
    print(squares)


#  等同于:
def test_4():
    combs = []
    for x in [1, 2, 3]:
        for y in [3, 1, 4]:
            if x != y:
                combs.append((x, y))
    print(combs)


# 值得注意的是在上面两个方法中的 for 和 if 语句的顺序。


def test_5():
    from math import pi
    list_1D = [str(round(pi, i)) for i in range(1, 6)]
    print(list_1D)


def test_6():
    vec = [-4, -2, 0, 2, 4]
    # create a new list with the values doubled
    t61 = [x * 2 for x in vec]
    print(t61)
    # [-8, -4, 0, 4, 8]

    # filter the list to exclude negative numbers
    t62 = [x for x in vec if x >= 0]
    print(t62)
    # [0, 2, 4]

    # apply a function to all the elements
    # abs() 绝对值函数
    t63 = [abs(x) for x in vec]
    print(t63)
    # [4, 2, 0, 2, 4]

    # call a method on each element
    freshfruit = ['  banana', '  loganberry ', 'passion fruit  ']
    # strip() 用于移除字符串头尾指定的字符（默认为空格）
    t64 = [weapon.strip() for weapon in freshfruit]
    print(t64)
    # ['banana', 'loganberry', 'passion fruit']

    # create a list of 2-tuples like (number, square)
    t65 = [(x, x ** 2) for x in range(6)]
    print(t65)
    # [(0, 0), (1, 1), (2, 4), (3, 9), (4, 16), (5, 25)]

    vec = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    t66 = [num for elem in vec for num in elem]
    print(t66)
    # [1, 2, 3, 4, 5, 6, 7, 8, 9]


def test_7():
    # 交换行和列，可以用嵌套的列表推导式:
    matrix = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12],
    ]
    t71 = [[row[i] for row in matrix] for i in range(4)]
    print(t71)


# 等价于:
def test_8():
    matrix = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12],
    ]
    t81 = [row[0] for row in matrix]
    print(t81)
    transposed = []
    for i in range(4):
        transposed.append([row[i] for row in matrix])
    print(transposed)


# 等价于:
def test_9():
    matrix = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12],
    ]
    transposed = []
    for i in range(4):
        transposed_row = []
        for row in matrix:
            print(i, row)
            transposed_row.append(row[i])
        print(transposed_row)
        transposed.append(transposed_row)
    print(transposed)


# 在实际中，你应该更喜欢使用内置函数组成复杂流程语句。对此种情况 zip() 函数将会做的更好:
def test_10():
    matrix = [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12],
    ]
    # 在调用函数时加一个 * 操作符来自动把参数列表拆开
    print(list(zip(*matrix)))


# -------------------------------------------------------------------------------
def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
