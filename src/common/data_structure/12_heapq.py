#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# heapq模块（堆结构）
'''
https://blog.csdn.net/weixin_43790276/article/details/107741332
堆是一种基本的数据结构，堆的结构是一棵完全二叉树，并且满足堆积的性质：
每个节点(叶节点除外)的值都大于等于(或都小于等于)它的子节点。

堆结构分为大顶堆和小顶堆，在heapq中使用的是小顶堆：
1. 大顶堆：每个节点(叶节点除外)的值都大于等于其子节点的值，根节点的值是所有节点中最大的,在堆排序算法中用于升序排列。
2. 小顶堆：每个节点(叶节点除外)的值都小于等于其子节点的值，根节点的值是所有节点中最小的,在堆排序算法中用于降序排列。

heappush(heap, x)       将x压入堆中
heappop(heap)           从堆中弹出最小的元素
heapify(heap)           让列表具备堆特征
heapreplace(heap, x)    弹出最小的元素，并将x压入堆中
nlargest(n, iter)       返回iter中n个最大的元素
nsmallest(n, iter)      返回iter中n个最小的元素
'''
import heapq


def test_1():
    '''从列表中找出最大的或最小的N个元素'''
    list1 = [34, 25, 12, 99, 87, 63, 58, 78, 88, 92]
    list2 = [
        {'name': 'IBM', 'shares': 100, 'price': 91.1},
        {'name': 'AAPL', 'shares': 50, 'price': 543.22},
        {'name': 'FB', 'shares': 200, 'price': 21.09},
        {'name': 'HPQ', 'shares': 35, 'price': 31.75},
        {'name': 'YHOO', 'shares': 45, 'price': 16.35},
        {'name': 'ACME', 'shares': 75, 'price': 115.65}
    ]
    # nlargest(num, heap): 从堆中取出num个数据，从最大的数据开始取，返回结果是一个列表(即使只取一个数据)。
    # 如果num大于等于堆中的数据数量，则从大到小取出堆中的所有数据，不会报错，相当于实现了降序排序。
    print(heapq.nlargest(3, list1))
    # nsmallest(num, heap): 从堆中取出num个数据，从最小的数据开始取，返回结果是一个列表。
    print(heapq.nsmallest(3, list1))
    print(heapq.nlargest(2, list2, key=lambda x: x['price']))
    print(heapq.nlargest(2, list2, key=lambda x: x['shares']))


def test_2():
    array = [10, 17, 50, 7, 30, 24, 27, 45, 15, 5, 36, 21]
    # heappush:先创建一个空堆，然后将数据一个一个地添加到堆中。每添加一个数据后，heap都满足小顶堆的特性。
    heap = []
    for num in array:
        heapq.heappush(heap, num)
    print("array:", array)
    print("heap: ", heap)
    # [5, 7, 21, 15, 10, 24, 27, 45, 17, 30, 36, 50]

    # heapify:直接将数据列表调整成一个小顶堆
    heapq.heapify(array)
    print("array:", array)
    # [5, 7, 21, 10, 17, 24, 27, 45, 15, 30, 36, 50]


# 使用heapq实现堆排序
def test_3():
    array = [10, 17, 50, 7, 30, 24, 27, 45, 15, 5, 36, 21]
    heap = []
    for num in array:
        heapq.heappush(heap, num)
    # print(heapq.heappop(heap))
    heap_sort = [heapq.heappop(heap) for _ in range(len(heap))]
    print("heap sort result: ", heap_sort)
    # [5, 7, 10, 15, 17, 21, 24, 27, 30, 36, 45, 50]


# 合并两个有序列表
def test_4():
    array_a = [10, 7, 15, 8]
    array_b = [17, 3, 8, 20, 13]
    array_merge = heapq.merge(sorted(array_a), sorted(array_b))
    print("merge result:", list(array_merge))
    # [3, 7, 8, 8, 10, 13, 15, 17, 20]


def test_5():
    array_c = [10, 7, 15, 8]
    heapq.heapify(array_c)
    print("before:", array_c)  # [7, 8, 15, 10]
    # 先push再pop
    # 先将num添加到堆中，然后将堆顶的数据出堆。
    item = heapq.heappushpop(array_c, 5)
    print("after: ", array_c)  # [7, 8, 15, 10]
    print(item)  # 5

    array_d = [10, 7, 15, 8]
    heapq.heapify(array_d)
    print("before:", array_d)  # [7, 8, 15, 10]
    # 先pop再push
    # 先将堆顶的数据出堆，然后将num添加到堆中。
    item = heapq.heapreplace(array_d, 5)
    print("after: ", array_d)  # [5, 8, 15, 10]
    print(item)  # 7


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
