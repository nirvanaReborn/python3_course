#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 字符串拼接性能对比
# 这一章介绍了标准Python实现的其他两个版本:
# 一种是PyPy， 它是Python的一个分支， 是由RPython实现的。 PyPy的JIT编译器可以在运行过程中优化代码。
# 另一种是Cython， 基本可以看成把Python代码翻译成C语言代码的转换器。
#
# PyPy是Python的另一种实现， 有2.x版本和3.x版本。
# 它用RPython模拟语言的功能，RPython是一种静态类型的Python版本。
# PyPy项目（http://pypy.org/） 是另一个旧项目Psycho的延续， Psycho是一种用C语言写的Python的JIT编译器。
#
# 我们选择PyPy作为优化脚本的可靠方法， 理由如下。
# 1.速度： PyPy的一个主要特性是对普通Python代码运行速度的优化。
#         这是由于它使用JIT（Just-in-time） 编译器。 在静态编译代码时它提供了一种灵活性，
#         可以在运行时根据运行环境（处理器、 操作系统版本等） 进行调整。
#         另一方面， 静态编译程序可能需要一个可执行或者不同条件的组合体。
# 2.内存： PyPy执行脚本时消耗的内存要比普通Python小。
# 3.沙盒（sandboxing） ： PyPy提供了沙盒环境， 在调用C语言库的时候使用。
#         这种机制会与一个处理实际情况的外部进程通信。
#         虽然这种机制很好， 但还只是一个原型， 需要一些处理才能正常使用。
# 4.无栈（stackless） ： PyPy还提供了与Stackless Python（http://www.stackless.com/） 相似的特性。
#         有人甚至认为PyPy比后者更加强大和灵活。
# 通过三种方式的消耗时间可以看出， 在PyPy中StringIO对象是最快的。 它比普通字符串快， 甚至比用列表的方法还要好。
# 如果我们再用CPython运行代码， 就会获得不同的结果。 最好的运行结果还是用列表。
# 《Python性能分析与优化》P194

import time
from io import StringIO
import timeit

TIMES = 100000


def test_1():
    init = time.perf_counter()
    value = ''
    for i in range(TIMES):
        value += str(i)
    print("Concatenation: %s" % (time.perf_counter() - init))


def test_2():
    init = time.perf_counter()
    value = StringIO()
    for i in range(TIMES):
        value.write(str(i))
    print("StringIO: %s" % (time.perf_counter() - init))


def test_3():
    init = time.perf_counter()
    value = []
    for i in range(TIMES):
        value.append(str(i))
    finalValue = ''.join(value)
    print("List: %s" % (time.perf_counter() - init))


def timeit_analyze(func):
    t1 = timeit.Timer(lambda: func())
    print("{:<20}{:10.6} s".format(func.__name__ + ":", t1.timeit(1)))


def main():
    timeit_analyze(test_1)
    timeit_analyze(test_2)
    timeit_analyze(test_3)
    # timeit_analyze()


if __name__ == "__main__":
    main()
