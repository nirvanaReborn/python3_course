#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://www.toutiao.com/a6416935304883814658/
# Python高级特性——切片


def test_1():
    # 取某个数组前三个元素
    arr = ['a', 'b', 'c', 'd', 'e']

    # 最笨的取法：
    print('%s %s %s' % (arr[0], arr[1], arr[2]))

    # 循环取法：
    r = []
    for i in range(3):
        r.append(arr[i])
    print('%s' % r)

    # 切片取法
    print('%s' % (arr[0:3]))

    # 如果第一个索引是0，还可以用 arr[:3]
    print('%s' % (arr[:3]))

    # Python支持arr[-1]取倒数第一个元素，此处的值为：e
    print('%s' % (arr[-1]))


def test_2():
    # 我们先创建一个0-49的数列
    L = list(range(50))
    print(L)

    # 前10个数
    print(L[:10])  # [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

    # 后10个数
    print(L[-10:])  # [40, 41, 42, 43, 44, 45, 46, 47, 48, 49]

    # 前11-20个数
    print(L[10:20])  # [10, 11, 12, 13, 14, 15, 16, 17, 18, 19]

    # 前10个数，每两个取一个
    print(L[:10:2])  # [0, 2, 4, 6, 8]

    # 所有数，每5个取一个
    print(L[::5])  # [0, 5, 10, 15, 20, 25, 30, 35, 40, 45]

    # 原样复制一个list
    print(L[:])


def test_3():
    # tuple也是一种list，唯一区别是tuple不可变。
    # 因此，tuple也可以用切片操作，只是操作的结果仍是tuple
    print((0, 1, 2, 3, 4, 5)[:3])  # (0, 1, 2)


def test_4():
    # 字符串'xxx'也可以看成是一种list，每个元素就是一个字符。
    # 因此，字符串也可以用切片操作，只是操作结果仍是字符串
    str = 'ABCDEFG'
    print(str[:3])  # 'ABC'
    print(str[::2])  # 'ACEG'


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
