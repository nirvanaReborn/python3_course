#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://www.pythondoc.com/pythontutorial3/datastructures.html
# 元组是另一个数据类型，类似于List（列表）。
# 但是元素不能二次赋值，相当于只读列表。
# 元组用"()"标识,以便于正确表达嵌套结构.
# 内部元素用逗号隔开。
#
# 元组就像字符串， 不可变的。通常包含不同种类的元素并通过分拆（参阅本节后面的内容) 或索引访问（如果是 namedtuples，甚至可以通过属性）。
# 列表是 可变的 ，它们的元素通常是相同类型的并通过迭代访问。
# 元组有很多用途。例如 (x, y) 坐标对，数据库中的员工记录等等。

'''我们已经有了列表这种数据结构，为什么还需要元组这样的类型呢？

1.元组中的元素是无法修改的，事实上我们在项目中尤其是多线程环境（后面会讲到）中可能更喜欢使用的是那些不变对象
（一方面因为对象状态不能修改，所以可以避免由此引起的不必要的程序错误，简单的说就是一个不变的对象要比可变的对象更加容易维护；
另一方面因为没有任何一个线程能够修改不变对象的内部状态，一个不变对象自动就是线程安全的，这样就可以省掉处理同步化的开销。
一个不变对象可以方便的被共享访问）。
所以结论就是：如果不需要对元素进行添加、删除、修改的时候，可以考虑使用元组，
当然如果一个方法要返回多个值，使用元组也是不错的选择。

2.元组在创建时间和占用的空间上面都优于列表。
我们可以使用sys模块的getsizeof函数来检查存储同样的元素的元组和列表各自占用了多少内存空间，这个很容易做到。
我们也可以在ipython中使用魔法指令%timeit来分析创建同样内容的元组和列表所花费的时间。
'''

def test_1():
    tuple_0 = tuple()
    print(tuple_0)

    # 在输入时可以有或没有括号，不过经常括号都是必须的（如果元组是一个更大的表达式的一部分）。
    tuple_1 = 12345, 54321, 'hello!'
    print(tuple_1[0])
    # 12345
    print(tuple_1)
    # (12345, 54321, 'hello!')

    # Tuples may be nested:
    tuple_2 = tuple_1, (1, 2, 3, 4, 5)
    print(tuple_2)
    # ((12345, 54321, 'hello!'), (1, 2, 3, 4, 5))

    # Tuples are immutable:
    # t[0] = 88888 # 不可改变的

    # but they can contain mutable objects:
    tuple_3 = ([1, 2, 3], [3, 2, 1])
    print(tuple_3)
    # ([1, 2, 3], [3, 2, 1])

    tuple_4 = tuple_1 + tuple_2
    print(tuple_4)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
