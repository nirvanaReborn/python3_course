#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 数字类型：int, float, complex(复数), decimal.Decimal(小数模块), fractions.Fraction(分数模块)
# 数字操作：math模块


# 小数模块
def test_1():
    import decimal
    # from decimal import Decimal
    y = decimal.Decimal("0.01") + decimal.Decimal("0.02")  # 返回Decimal("0.03")
    print(y)
    decimal.getcontext().prec = 4  # 设置全局精度为4, 即小数点后边4位
    print(y)


# 分数模块
def test_2():
    from fractions import Fraction
    x = Fraction(4, 6)  # 分数类型 4/6
    print(x)  # 2/3
    x = Fraction("0.25")  # 分数类型 1/4 接收字符串类型的参数
    print(x)  # 1/4


# 复数模块
'''
我们把形如z=a+bi（a,b均为实数）的数称为复数，其中a称为实部，b称为虚部，i称为虚数单位。
当z的虚部等于零时，常称z为实数；
当z的虚部不等于零时，实部等于零时，常称z为纯虚数。
'''


def test_3():
    print(complex(1, 2))
    print(complex(1))  # 数字
    print(complex("1"))  # 当做字符串处理
    print(complex("1+2j"))  # 注意：这个地方在"+"号两边不能有空格，也就是不能写成"1 + 2j"，应该是"1+2j"，否则会报错


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
