#!/usr/bin/env python
# -*- coding:utf-8 -*-


def test_1():
    # 创建bytes对象的方法规则
    ba = bytes()
    # ba = b''
    print(ba)
    ba = bytes(1)
    print(ba)
    ba = bytes([1, 2, 3])
    print(ba)
    ba = bytes(b"abc")
    print(ba)
    ba = bytes("abc", encoding='utf-8')
    print(ba)
    dict_data = {
        'name': 'ldp_rcm',
        'num': [1, 2, 3],
    }
    import json
    ba = bytes(json.dumps(dict_data), encoding='utf-8')
    print(ba)

    # bytes 和 str 之间的转换：
    string = '你好，世界！'
    str2b = string.encode(encoding='utf-8')
    print(str2b)

    b2str = str2b.decode(encoding='utf-8')
    print(b2str)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
