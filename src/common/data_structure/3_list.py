#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# 浅拷贝：在复制的时候只增加了一个指针，没有为其分配内存空间，
#         即一个列表 a， a 里的值都有指向自己的指针，而且也有自己的内存空间a1，
#         浅拷贝列表 a 得到一个列表A，A里的值都有指向自己的指针，但是其内存空间还是a1，
#         无论是对 a 还是 A 进行操作，都会改变内存空间a1里的值；
#
# 深拷贝：在复制的时候不但增加了一个指针，而且还为其分配了内存空间，
#         即一个列表 a， a 里的值都有指向自己的指针，而且也有自己的内存空间a1，
#         深拷贝列表 a 得到一个列表A，A里的值都有指向自己的指针，而且也有自己的内存空间A1，
#         操作时，可以去 a 里查找，在 A 里操作，由于都有自己独立的内存空间，不会相互影响。就可以避免遗漏值，出现错误。

import collections
import copy
import itertools
import pprint
import random

import numpy


# http://www.pythondoc.com/pythontutorial3/datastructures.html
def test_1():
    list_1D = [66.25, 333, 333, 1, 1234.5]

    # list_1D.count(x)：返回 x 在列表中出现的次数。
    print(list_1D.count(333), list_1D.count(66.25), list_1D.count('x'))  # 2 1 0

    '''
    list_1D.insert(i, x)：在指定位置i插入一个元素x。
    第一个参数是准备插入到其前面的那个元素的索引，
    例如 list_1D.insert(0, x) 会插入到整个列表之前，
      而 list_1D.insert(len(list_1D), x) 相当于 list_1D.append(x)。
    '''
    list_1D.insert(2, -1)
    print(list_1D)  # [66.25, 333, -1, 333, 1, 1234.5]

    '''
    list_1D.pop([i])
    从列表的指定位置删除元素，并将其返回。
    如果没有指定索引，a.pop() 返回最后一个元素。
    元素随即从列表中被删除（方法中 i 两边的方括号表示这个参数是可选的，
    而不是要求你输入一对方括号，你会经常在Python 库参考手册中遇到这样的标记）。
    '''
    print(list_1D.pop())  # 1234.5
    print(list_1D)  # [66.25, 333, -1, 333, 1]

    # 把一个元素添加到列表的结尾，相当于 a[len(a):] = [x]。
    list_1D.append(333)  # (原地修改)
    print(list_1D)  # [66.25, 333, -1, 333, 1, 333]

    list_1D = list_1D + [1]  # 并不是真正的尾部添加元素，而是创建新的列表对象

    # * 实现列表的复制
    list_2D = list_1D * 2  # 使用乘法扩展列表，生成一个新列表
    print(list_2D)

    # 删除列表中值为 x 的第一个元素。如果没有这样的元素，就会返回一个错误。
    list_1D.remove(333)
    print(list_1D)  # [66.25, -1, 333, 1, 333]

    # 返回列表中第一个值为 x 的元素的索引。如果没有匹配的元素就会返回一个错误。
    print(list_1D.index(333))  # 2

    # 就地逆序排序列表中的元素。
    list_1D.reverse()
    print(list_1D)  # [333, 1, 333, -1, 66.25]

    # 对列表中的元素就地进行排序。
    list_1D.sort()  # 默认是升序排列(原地排序)
    # list_1D.sort(key=str.lower) # 这将导致sort()方法将列表中所有的表项当成小写，但实际上并不会改变它们在列表中的值。
    print(list_1D)  # [-1, 1, 66.25, 333, 333]
    list_1D.sort(reverse=True)  # 降序排序
    random.shuffle(list_1D)  # 随机排序，打乱顺序

    # list_1D.extend(list_test)
    # 将一个给定列表中的所有元素都添加到另一个列表中，相当于 a[len(a):] = list_test。
    list_test = ["hello"]
    list_1D.extend(list_test)  # (原地修改)
    # print(list_1D + list_test) # [-1, 1, 66.25, 333, 333, 'hello']
    print(list_1D)  # [-1, 1, 66.25, 333, 333, 'hello']

    # list_1D.copy() ：返回列表的一个浅拷贝。等同于 list_1D[:]。
    print(id(list_1D), id(list_1D.copy()))  # 指向不同的内存地址
    print(list_1D.copy())  # [-1, 1, 66.25, 333, 333, 'hello']

    del list_1D[0]
    print(list_1D)  # [1, 66.25, 333, 333, 'hello']

    del list_1D[2:4]
    print(list_1D)  # [1, 66.25, 'hello']

    # list_1D.clear() ：从列表中删除所有元素。相当于 del list_1D[:]。
    list_1D.clear()
    print(list_1D)  # []

    # 列表相关的其他内置函数汇总
    print(max(list_1D))
    print(min(list_1D))
    print(sum(list_1D))

# ---------------------------------------------------------------------------------------------------
# 列表的初始化（1维、2维）
def test_2():
    list_1D = [0] * 5
    print(list_1D)  # [0,0,0,0,0]

    list_1D = [i for i in range(5)]
    print(list_1D)  # [0, 1, 2, 3, 4]

    # 初始化一个5x6每项为0（固定值）的数组（推荐使用）
    list_2D = [[0 for col in range(5)] for row in range(6)]
    print(list_2D)


# ---------------------------------------------------------------------------------------------------
# 列表和字符串的相互转换
def test_3():
    list_1D = ["1", "2", "3", "4", "5"]
    # list 转 str
    str_list = ";".join([str(item) for item in list_1D])
    print(str_list)

    # str 转 list
    list_str = str_list.split(";")
    print(list_str)


# ---------------------------------------------------------------------------------------------------
# http://www.jb51.net/article/53911.htm
# Python统计列表中的重复项出现次数
def test_4():
    def statistical_usage_frequency_1(list_1D):
        list_count = []
        myset = set(list_1D)
        for item in myset:
            list_count.append((item, list_1D.count(item)))
        list_count.sort(key=lambda k: k[1], reverse=True)
        return list_count

    mylist = [1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 4]
    # 方法1：采用内置的Counter
    print(dict(collections.Counter(mylist)))

    # 方法2：
    print(statistical_usage_frequency_1(mylist))


# 获取重复项在所在位置
def test_41():
    def get_repeat_pos(list_1D):
        dict_pos = collections.defaultdict(list)
        for k, va in [(v, i) for i, v in enumerate(list_1D)]:
            dict_pos[k].append(va)
        return dict(dict_pos)

    mylist = [1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 4]
    print(get_repeat_pos(mylist))


# 获取重复项
def test_42():
    def get_repeat_data(list_1D):
        list_repeat = []
        dict_count = dict(collections.Counter(list_1D))
        for key in dict_count:
            if dict_count[key] > 1:
                list_repeat.append(key)
        print(list_repeat)
        return list_repeat

    mylist = [1, 2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 4]
    print(get_repeat_data(mylist))


# 列表数据去重
def test_43():
    # 方法1：采用内置的set
    list_1 = ['b', 'c', 'd', 'b', 'c', 'a', 'a']
    list_2 = list(set(list_1))
    list_2.sort()
    print(list_2)

    # 方法2：还有一种据说速度更快的，没测试过两者的速度差别
    list_1 = ['b', 'c', 'd', 'b', 'c', 'a', 'a']
    list_2 = sorted({}.fromkeys(list_1).keys())
    print(list_2)


# 多个列表合并去重
def test_44():
    list1 = [1, 2, 3, 4, 5]
    list2 = [4, 5, 6, 7, 8]

    # 无序
    # merged_list = list(set(list1 + list2))
    # 保序
    merged_list = list(dict.fromkeys(list1 + list2))
    print(merged_list)

# ---------------------------------------------------------------------------------------------------
def test_5():
    '''
    sort 与 sorted 区别：
    1.sort 是应用在 list 上的方法，sorted 可以对所有可迭代的对象进行排序操作。
    2.list 的 sort 方法返回的是对已经存在的列表进行操作，而内建函数 sorted 方法返回的是一个新的 list，而不是在原来的基础上进行的操作。
    3.不要写出 list_1D = list_1D.sort()这样的代码，试图记录返回值。
    4.不能对既有数字又有字符串值的列表排序，因为Python 不知道如何比较它们。
    5.sort()方法对字符串排序时，使用“ASCII 字符顺序”，而不是实际的字典顺序。这意味着大写字母排在小写字母之前。
    6.如果需要按照普通的字典顺序来排序，就在sort()方法调用时，将关键字参数key 设置为str.lower。
    '''
    list_1D = [66.25, 333, 333, 1, 1234.5]
    print(sorted(list_1D))  # 升序排序
    print(sorted(list_1D, reverse=True))  # 降序排序
    print(list(reversed(list_1D)))  # 逆序排序


# http://blog.csdn.net/alex_my/article/details/48198251
# list元素为dict时的排序
def test_6():
    # list元素为简单dict时的排序(二维列表排序)
    list_2D = [('d', 2), ('a', 4), ('b', 3), ('c', 2)]

    # 按照key排序
    list_2D.sort(key=lambda k: k[0])
    print(list_2D)
    # print(sorted(list_1D, key=operator.itemgetter(0)))

    # 按照value排序
    list_2D.sort(key=lambda k: k[1])
    print(list_2D)
    # print(print(sorted(list_1D, key=operator.itemgetter(1))))

    # 先按key排序再按value排序
    list_2D.sort(key=lambda k: (k[0], k[1]))
    print(list_2D)
    # print(print(print(sorted(list_1D, key=operator.itemgetter(0,1)))))

    # 先按value排序再按key排序
    list_2D.sort(key=lambda k: (k[1], k[0]))
    print(list_2D)
    # print(print(print(sorted(list_1D, key=operator.itemgetter(1, 0)))))

    # ------------------------------------------------------------------
    # list元素为复杂dict时的排序
    list_dict = [
        {'level': 19, 'star': 36, 'time': 1},
        {'level': 20, 'star': 40, 'time': 2},
        {'level': 20, 'star': 50, 'time': 3},
        {'level': 20, 'star': 30, 'time': 4},
        {'level': 20, 'star': 40, 'time': 5},
        {'level': 18, 'star': 40, 'time': 1}
    ]

    # 先按time排序
    # list_dict.sort(key=lambda k: k['time'])
    list_dict.sort(key=lambda k: (k.get('time', 0)))
    # sorted(list_dict, key=lambda k: k['time'], reverse=True)
    for idx, r in enumerate(list_dict):
        print('idx[%d]\t'
              'level: %d\t'
              'star: %d\t'
              'time: %d\t'
              % (idx, r['level'], r['star'], r['time']))
    print('*' * 50)

    # 再按照level和star顺序
    list_dict.sort(key=lambda k: (k.get('level', 0), k.get('star', 0)), reverse=True)
    for idx, r in enumerate(list_dict):
        print('idx[%d]\t'
              'level: %d\t'
              'star: %d\t'
              'time: %d\t'
              % (idx, r['level'], r['star'], r['time']))


# ---------------------------------------------------------------------------------------------------
# 列表的引用
def test_7():
    spam = [0, 1, 2, 3, 4, 5]
    cheese = spam
    cheese[1] = 'Hello!'
    print(id(spam), spam)  # 2462284148936 [0, 'Hello!', 2, 3, 4, 5]
    print(id(cheese), cheese)  # 2462284148936 [0, 'Hello!', 2, 3, 4, 5]
    '''
    当创建列表spam时，你将对它的引用赋给了变量。
    但下一行只是将spam 中的列表引用拷贝到cheese，而不是列表值本身。
    这意味着存储在spam 和cheese 中的值，现在指向了同一个列表。
    底下只有一个列表，因为列表本身实际从未复制。所以当你修改cheese 变量的第一个元素时，也修改了spam 指向的同一个列表。
    '''


def test_8():
    # 尽管spam和someParameter 包含了不同的引用，但它们都指向相同的列表。
    def eggs(someParameter):
        someParameter.append('Hello')

    spam = [1, 2, 3]
    eggs(spam)
    print(spam)  # [1, 2, 3, 'Hello']


# ---------------------------------------------------------------------------------------------------
# 列表的深拷贝
def test_9():
    spam = ['A', 'B', 'C', 'D']
    cheese = copy.copy(spam)
    cheese[1] = 42
    print(spam)  # ['A', 'B', 'C', 'D']
    print(cheese)  # ['A', 42, 'C', 'D']
    # 现在spam 和cheese 变量指向独立的列表

    # 如果要复制的列表中包含了列表，那就使用copy.deepcopy()函数来代替。
    spam = [[1, 3, 5], 'B', 'C', 'D']
    cheese = copy.deepcopy(spam)
    spam[0][1] = 42
    print(spam)  # [[1, 42, 5], 'B', 'C', 'D']
    print(cheese)  # [[1, 3, 5], 'B', 'C', 'D']


# is 判断两个对象是否为同一对象, 是通过调用id()来判断的; 当两个基本类型数据(或元组)内容相同时, id会相同, 但并不代表a会随b的改变而改变。
# == 判断两个对象的内容是否相同, 是通过调用eq()来判断的
# copy.copy对于可变类型，会进行浅拷贝; 对于不可变类型，不会拷贝，仅仅是指向
# copy.deepcopy的时候会将复杂对象的每一层复制一个单独的个体出来。
def test_10():
    origin = [1, 2, [3, 4]]
    cop1 = copy.copy(origin)
    cop2 = copy.deepcopy(origin)
    print(cop1 == cop2)  # True
    print(cop1 is cop2)  # False
    print(cop1 is origin)  # False

    origin[2][0] = "hey!"
    print(origin)  # [1, 2, ['hey!', 4]]
    print(cop1)  # [1, 2, ['hey!', 4]]
    print(cop2)  # [1, 2, [3, 4]]


# ---------------------------------------------------------------------------------------------------
# 在序列中循环时，索引位置和对应值可以使用 enumerate() 函数同时得到
def test_11(list_2D):
    for i, fileLine in enumerate(list_2D):
        print(i, fileLine)


# 同时循环两个或更多的序列，可以使用 zip() 整体打包
def test_12():
    questions = ['name', 'quest', 'favorite color']
    answers = ['lancelot', 'the holy grail', 'blue']
    zipped = zip(questions, answers)
    print(type(zipped))  # <class 'zip'>
    print(zipped)  # <zip object at 0x000001F02F1B26C8>
    mydict = dict(zipped)
    print(mydict)

    for q, a in mydict.items():
        print('What is your {0}?  It is {1}.'.format(q, a))


# ---------------------------------------------------------------------------------------------------
# 列表的转置
# http://blog.csdn.net/pirage/article/details/9288419
# http://blog.csdn.net/suofiya2008/article/details/5620654
# http://blog.csdn.net/spcq4/article/details/52769919
# 二维列表转置
def test_13():
    list_2D = [[1, 2, 3], ['a', 'b', 'c'], [4, 5, 6], [7, 8, 9], ['e', 'f', 'g']]

    # 1.用zip来实现
    list_transpose = list(map(list, zip(*list_2D)))
    pprint.pprint(list_transpose)

    # 2.使用itertools.zip_longest来稍微的提高效率(因为zip并没有将数据在内存中组织为列表).
    list_transpose = list(map(list, itertools.zip_longest(*list_2D)))
    pprint.pprint(list_transpose)

    # 3.用numpy来实现
    list_transpose = numpy.transpose(list_2D)
    print(type(list_transpose))  # <class 'numpy.ndarray'>
    pprint.pprint(list_transpose)


# 一维列表转置(参考二维列表)
def test_14():
    list_1D = [1, 2, 3, 4, 5, 6]
    list_transpose = list(map(list, zip(*[list_1D])))
    pprint.pprint(list_transpose)

    list_transpose = list(map(list, itertools.zip_longest(*[list_1D])))
    pprint.pprint(list_transpose)

    list_transpose = numpy.transpose([list_1D])
    pprint.pprint(list_transpose)


# ---------------------------------------------------------------------------------------------------
# 列表的扁平化和降维
def test_15():
    groups = [['huahua', 'xiaojian'], ['musen', 'yuze'], ['geyou']]
    names = []
    for group in groups:
        for name in group:
            names.append(name)
    print(names)


# 但是在面试的时候可能会加一些限制，比如让你用一行代码实现，这个时候就需要对python基础有进一步的理解了。
def test_16():
    groups = [['huahua', 'xiaojian'], ['musen', 'yuze'], ['geyou']]
    print(sum(groups, []))

    # 或者采用列表推导式
    names = [name for group in groups for name in group]
    print(names)


# 多维列表转一维列表
def test_17():
    def flatten(list_nD):
        result = []
        for list_temp in list_nD:
            if isinstance(list_temp, collections.Iterable) and not isinstance(list_temp, str):
                result.extend(flatten(list_temp))
            else:
                result.append(list_temp)
        return result

    # print(flatten([[1, 2], [5, 6]]))
    print(flatten(["junk", ["nested stuff"], [], [[]]]))


# ---------------------------------------------------------------------------------------------------
# 列表元素的5种添加方式
def test_18():
    # 1.使用append: 原地修改列表对象
    list_1 = [10, 30]
    print(id(list_1))
    list_1.append(80)
    print(id(list_1))

    # 2.使用+
    list_2 = [20, 30]
    print(id(list_2), list_2)
    # list_2 = list_2 + [50] # 创建了新的列表对象
    list_2 += [50]  # 原地修改列表对象
    print(id(list_2), list_2)

    # 3.使用extend: 原地修改列表对象
    list_3 = [10, 15]
    print(id(list_3))
    list_3.extend([50, 60])
    print(id(list_3))

    # 4.使用insert: 原地修改列表对象
    list_4 = [10, 25]
    print(id(list_4))
    list_4.insert(1, 60)
    print(id(list_4))

    # 5.使用乘法扩展
    list_5 = [10, 35]
    print(id(list_5), list_5)
    # list_5 = list_5 * 3 # 创建了新的列表对象
    list_5 *= 3  # 原地修改列表对象
    print(id(list_5), list_5)


# ---------------------------------------------------------------------------------------------------
# 列表的三种删除方法: 会涉及大量元素的移动
# 删除列表中指定元素时，建议不要使用 for 循环。
def test_19():
    # 1.使用del语句删除指定位置的元素
    list_1D = [1, 2, 3, 4, 5, 6, 'a', 'b', 'c']
    del list_1D[0]  # 删除第一个元素
    print(list_1D)
    del list_1D[2:4]  # 删除第二、第三个元素
    print(list_1D)
    # del list_1D # 清空整个列表
    # print(list_1D) # 报错UnboundLocalError: local variable 'list_1D' referenced before assignment

    # 2.使用pop()删除元素，从列表的指定位置删除元素，并将其返回。 如果没有指定索引，a.pop() 返回最后一个元素。
    print(list_1D.pop())  # 返回删除的元素

    # 3.使用remove()根据值删除元素,删除列表中满足条件的第一个元素。
    # 注意：remove()只删除一个指定的值。如果要删除的值可能在列表中出现多次，就需要使用循环来判断是否删除了所有的值。
    # list_1D.remove('c') # 如果没有这样的元素，就会返回一个错误。
    print(list_1D.remove('a'))  # 返回值为None
    print(list_1D)


# 遍历列表时删除元素的正确做法：遍历在新在列表操作，删除时在原来的列表操作
def test_20():
    a = [1, 2, 3, 4, 5, 6, 7, 8]
    print(id(a))  # 2684747383880
    print(id(a[:]))  # 2684747383816
    for i in a[:]:
        if i > 5:
            pass
        else:
            a.remove(i)
    print(a)
    print(id(a))  # 2684747383880
    print('-----------')

    a = [1, 2, 3, 4, 5, 6, 7, 8]
    b = list(filter(lambda x: x > 5, a))
    # b = [i for i in a if i > 5]
    print(b)
    print('-----------')

    # 倒序删除: 因为列表总是‘向前移’，所以可以倒序遍历，即使后面的元素被修改了，还没有被遍历的元素和其坐标还是保持不变的
    a = [1, 2, 3, 4, 5, 6, 7, 8]
    print(id(a))
    for i in range(len(a) - 1, -1, -1):
        print(i, a[i])
        if a[i] > 5:
            pass
        else:
            a.remove(a[i])
    print(id(a))
    print(a)


# ---------------------------------------------------------------------------------------------------
#
def test_21():
    def print_tree(parentId, tree, spaceStr='\t'):
        for x in tree:
            if x['parent_id'] == parentId:
                print(spaceStr, x['title'])
                print_tree(x['id'], tree, spaceStr + '\t')

    list_tree = [
        {'id': 1, 'title': 't1', 'parent_id': 0},
        {'id': 2, 'title': 't2', 'parent_id': 0},
        {'id': 3, 'title': 't1_1', 'parent_id': 1},
        {'id': 4, 'title': 't1_2', 'parent_id': 1},
        {'id': 5, 'title': 't1_2_1', 'parent_id': 4},
        {'id': 6, 'title': 't2_1', 'parent_id': 2},
    ]
    print_tree(0, list_tree)


def test_22():
    import math
    def print_tree(array, unit_width=2):
        length = len(array)
        depth = math.ceil(math.log2(length + 1))
        index = 0

        width = 2 ** depth - 1  # 行宽，最深是15
        for i in range(depth):  # 控制一行的
            for j in range(2 ** i):  # 一行该取几下，取下面的array[index]
                print('{:^{}}'.format(array[index], width * unit_width), end=' ' * unit_width)
                index += 1
                if index >= length:
                    break
            width = width // 2
            print()

    print_tree([i for i in range(10)])


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
