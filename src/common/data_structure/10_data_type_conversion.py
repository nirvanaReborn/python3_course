#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/jmilk/article/details/49720611
# Python基本语法_强制数据类型转换

'''显式转换的多种类型
函数                          描述
int(x [,base])              将x转换为一个整数
float(x)                    将x转换到一个浮点数
complex(real [,imag])       创建一个复数
str(object)                 将对象 object 转换为字符串
repr(object)                将对象 object 转换为表达式字符串
eval(str)                   用来计算在字符串中的有效Python表达式,并返回一个对象
tuple(seq)                  将序列 seq 转换为一个元组
list(seq)                   将序列 seq 转换为一个列表
set(seq)                    将序列 seq 转换为可变集合
frozenset(seq)              将序列 seq 转换为不可变集合
dict(d)                     创建一个字典, d 必须是一个序列 (key,value)元组。
chr(x)                      将一个整数转换为一个字符
unichr(x)                   将一个整数转换为Unicode字符
ord(x)                      将一个字符转换为它的整数值
hex(x)                      将一个整数转换为一个十六进制字符串
oct(x)                      将一个整数转换为一个八进制字符串
'''


# repr()函数的使用方法于str()函数类似，都是传入一个任意对象，再返回一个String类型对象，但两者却有着本质的区别。
# 主要的区别就在repr()支持eval(repr(object)) == object。
# str()函数的目标是一般用户的可读性，返回一个更适合人阅读的nice string。
# 而repr()则返回一个更适合python解析器阅读的canonical strng，同时会返回Python解析器能够识别的数据细节，但这些细节对一般用户来说是多余的。
# 而且repr()转换后的String对象可以通过求值运算eval()来还原到转换之前的对象，相比之下str()通常不需要eval()去处理。


def test_1():
    name = ('My name is Jmilk\n')
    print('*' * 40)
    print(str(name))  # 转义，打印空行
    print('*' * 40)
    print(repr(name))  # 不转义，打印原型
    # "'My name is Jmilk\n'"
    print('*' * 40)


'''
eval(repr(object)) == object。
eval()能够结合repr()函数将一个经过转换为Strng类型后的对象还原为转换之前的对象类型。
同时eval()也被称为求值运算，可以将字符串str当成有效的表达式来求值并返回计算结果。
'''


def test_2():
    name = ('My name is Jmilk\n')
    print('*' * 40)
    name1 = str(name)
    print(name1)  # 转义，打印空行
    print('*' * 40)

    name2 = repr(name)
    print(name2)  # 不转义，打印原型
    # "'My name is Jmilk\n'"
    print('*' * 40)

    # print(eval(name1)) # 报错
    print(eval(name2))  # 转义，打印空行
    print('*' * 40)

    # eval(str(Object))可能会报错，无法还原为原来的对象型。而eval(repr(object)) == object却可以还原，下面例子：
    name = ('My', 'name', 'is', 'Jmilk\n')

    print(repr(name))
    # "('My', 'name', 'is', 'Jmilk\n')"

    print(eval(repr(name)))
    # ('My', 'name', 'is', 'Jmilk\n')

    print(type(eval(repr(name))))
    # <class 'tuple'>

    # eval()函数最主要的功能是能够将字符串中有效的表达式进行计算并返回一个对象。
    sum = '100+10'
    print(eval(sum))


#  Non-String -----> 转换为String
def test_3():
    # 将List对象转换为String：
    list_1D = ['My', 'Name', 'Is', 'Jmilk']
    str_list = str(list_1D)
    print(str_list)
    print(str_list[0])

    # 将Tuple对象转换为String：
    tuple_1D = ('my', 'name', 'is', 'jmilk')
    str_tuple = str(tuple_1D)
    print(str_tuple)
    print(str_tuple[0])

    # 将Dictionary对象转换为String：
    dic = {'name': 'Jmilk', 'age': 23}
    str_dic = str(dic)
    print(str_dic)
    print(str_dic[0])


#  Non-int -----> 转换为int
# Int型的最大值仅与系统位数有关，32位：maxInt == 2**(32-1)-1 ; 64位：maxInt == 2**(64-1)-1。
# 在Python2可以通过sys.maxint 来查看：print(sys.maxint)
# 在Python2.4x版本之后为Int增加了Long的隐式转换来防止数据范围溢出。
def test_4():
    result = 2 ** 64
    print(result, type(result))
    print(int(10.9))
    print(int('0xa', 16))
    print(int('1010', 2))


#  Non-float -----> 转为浮点型
def test_5():
    print(float(10))
    print(float('100'))
    print(float('.1111'))
    # print(float('.98.'))


# 将List和Tuple -----> Dictionary
def test_6():
    li = ['name', 'age', 'city']
    tup = ('jmilk', 23, 'BJ')

    print(list(zip(li, tup)))  # 坐标点列表
    # [('name', 'jmilk'), ('age', 23), ('city', 'BJ')]

    print(dict(zip(li, tup)))  # 坐标点字典
    # {'name': 'jmilk', 'age': 23, 'city': 'BJ'}


# -------------------------------------------------------------------------------
# https://blog.csdn.net/violet_echo_0908/article/details/52486689
# string -----> list或tuple或set
# 将字符串中的每一个字母作为列表中的一个元素，空格也算是一个元素。
def test_7():
    name = 'My name is Jmilk'
    print(list(name))
    print(tuple(name))
    print(set(name))

    string = "(1,2,3)"
    print(tuple(eval(string)))
    print(list(eval(string)))
    print(set(eval(string)))

    print(dict(eval("{'name':'ljq', 'age':24}")))


# Tuple -----> str或list或set
# 元组不可以转为字典
def test_8():
    tup = ('My', 'name', 'is', 'Jmilk')
    print(tup.__str__())
    print(list(tup))
    print(set(tup))


# List -----> str或Tuple或set
# 列表不可以转为字典
def test_9():
    list_name = ['My', 'name', 'is', 'Jmilk']
    # print(str(list_name)) # 不变
    print(";".join([str(item) for item in list_name]))
    print(tuple(list_name))
    print(set(list_name))


# Dictionary -----> str或Tuple或List
def test_10():
    dic = {'age': 23, 'city': 'BJ', 'name': 'jmilk'}
    print(type(str(dic)), str(dic))

    print(tuple(dic.items()))  # 坐标点元组
    print(tuple(dic))  # key值
    print(tuple(dic.values()))  # value值

    print(list(dic.items()))  # 坐标点列表
    print(list(dic))  # key值
    print(list(dic.values()))  # value值


# -------------------------------------------------------------------------------
def test_11():
    print(chr(65))  # Int转换为字符char
    print(hex(65))  # 将整数x转换为十六进制字符串。
    print(oct(65))  # 将整数x转换为八进制字符串。
    # print(unichr(65))   # 将整数x转换为Unicode字符。


# 将单个字符x转换为其整数值。
def test12():
    print(ord('A'))


# 把序列转换为冻结集。
def test_13():
    list_name = ['My', 'name', 'is', 'Jmilk']
    print(frozenset(list_name))  # frozenset({'Jmilk', 'My', 'is', 'name'})


# 转为复数形式。
def test_14():
    print(complex(1))
    print(complex('2+1j'))
    print(complex(2, 5))


# python <-----> json
def test_15():
    import json

    data1 = {
        'no': 1,
        'name': 'Runoob',
        'url': 'http://www.runoob.com'
    }

    # python -----> json
    json_str = json.dumps(data1)
    print("Python 原始数据：", repr(data1))
    print("JSON 对象：", json_str)

    # json -----> python
    data2 = json.loads(json_str)
    print("data2['name']: ", data2['name'])
    print("data2['url']: ", data2['url'])


# -------------------------------------------------------------------------------
def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
