#!/usr/bin/env python
# -*- coding:utf-8 -*-

# is 判断两个对象是否为同一对象, 是通过调用id()来判断的; 当两个基本类型数据(或元组)内容相同时, id会相同, 但并不代表a会随b的改变而改变。
# == 判断两个对象的内容是否相同, 是通过调用eq()来判断的
# 对于可变对象，要注意尽量不要共享引用！


def main():
    a = [1, 2, 3]
    b = [1, 2, 3]
    print(a == b)  # True
    print(a is b)  # False

    c = d = [1, 2, 4]  # 共享引用
    print(c == d)  # True
    print(c is d)  # True


if __name__ == "__main__":
    main()
