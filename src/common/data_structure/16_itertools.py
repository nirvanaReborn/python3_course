#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''迭代工具模块
itertools（https://docs.python.org/3/library/itertools.html）包含特殊用途的迭代器函数。
在for ... in 循环中调用迭代函数，每次会返回一项，并记住当前调用的状态。

# https://www.cnblogs.com/haiyan123/p/9804091.html
# itertools 是python的迭代器模块，itertools提供的工具相当高效且节省内存。
# 使用这些工具，你将能够创建自己定制的迭代器用于高效率的循环。
# https://www.cnblogs.com/suke99/p/5185495.html

# https://blog.csdn.net/weixin_44585839/article/details/98474105
其中函数主要可以分为三类，分别是无限迭代器，有限迭代器，组合迭代器。

无限迭代器（Infinite Iterators）
    count([start=0, step=1]) 接收两个可选整形参数，第一个指定了迭代开始的值，第二个指定了迭代的步长。
'''
import itertools
import operator


def test_0():
    for i in dir(itertools):
        print(i)


def test_1():
    # accumulate() 计算累积的值。默认的话，它计算的是累加和：
    print(list(itertools.accumulate([1, 2, 3, 4, 5])))

    # 你可以把一个函数作为accumulate() 的第二个参数，代替默认的加法函数。
    # 这个参数函数应该接受两个参数，返回单个结果。
    def multiply(a, b):
        return a * b

    print(list(itertools.accumulate([1, 2, 3, 4, 5], multiply)))

    print(list(itertools.accumulate([1, 2, 3, 4, 5], operator.mul)))


def test_2():
    # 创建一个迭代器，接收多个iterables参数，将它们串接起来，然后进行遍历。
    # 即使chain() 的参数只是单个迭代对象，它也会使用参数进行迭代：
    for item in itertools.chain([1, 2], ['a', 'b']):
        print(item)


def test_3():
    # 产生ABCD的全排列
    print(list(itertools.permutations('ABCD')))
    # 产生ABCDE的五选三组合
    print(list(itertools.combinations('ABCDE', 3)))
    # 产生ABCD和123的笛卡尔积
    print(list(itertools.product('ABCD', '123')))
    # 产生ABC的无限循环序列
    for i, item in enumerate(itertools.cycle(('A', 'B', 'C'))):
        if i > 10:
            break
        print(item)


def test_4():
    # 创建一个能生成均匀分布序列（或者说等差数列）的迭代器，start指定起点，step指定步长。
    # count(start=0, step=1) 其中start, step不限于浮点数。
    # 注意，count()生成的是无限序列迭代器，所以用于循环中时，需要另外的条件来控制循环的终止。
    for i in itertools.count(10, 2):
        if i > 20:
            break
        print(i)

    # 创建一个迭代器，从一个iterable中循环取出元素并输出。
    for i, item in enumerate(itertools.cycle([1, 2])):
        if i > 10:
            break
        print(item)


def test_5():
    # 创建迭代器，重复输出object。如果未指定times会无限输出，如果指定了times则重复输出指定次数。
    for item in itertools.repeat('Do not answer!', 3):
        print('{0}'.format(item))

    # 一个典型的用法是用于给map或者zip提供一个常数值串
    print(list(map(pow, range(10), itertools.repeat(2))))
    print(list(zip(range(5), itertools.repeat(2))))


def test_6():
    # compress(data, selectors)
    # 根据selectors所列的各元素对原数据进行滤波处理，挑选出对应的selectors元素为True的data元素。
    # selectors与data的元素是一一对应的。
    print(list(itertools.compress('ABCDEFG', [0, 0, 1, 0, 0, 1, 1])))


def test_7():
    # islice(iterable, stop)
    # islice(iterable, start, stop[, step])
    # 以slicing的方式从一个可迭代对象(iterable)种取元素并输出。
    # 可以把islice()理解为升级版的range()，range()就是islice的第1个iterable参数设为一个连续整数序列时的行为相同。
    A = [k for k in range(10)]
    for item in itertools.islice(A, 3):
        print(item)

    for item in itertools.islice(A, 2, 8, 2):
        print(item)


def test_8():
    fruits = ['apple', 'banana', 'melon', 'strawberry']
    prices = [10, 20, 30]
    print(list(itertools.zip_longest(fruits, prices)))
    print(list(itertools.zip_longest(fruits, prices, fillvalue='Sold out')))


def test_9():
    # product生成输入各iterable（分别看作一个集合）的笛卡尔积.
    # product(A, B) returns the same as ((x,y) for x in A for y in B).
    for item in itertools.product('AB', 'CD'):
        print(item)

    for item in itertools.product('AB', repeat=4):
        print(item)


def test_10():
    # 返回p中任意取r个元素做排列的元组的迭代器.
    # 注意，参数列表中的[,r]表示第2个参数是可选项。如果不设置的话，就缺省地取第一个参数的长度，此时返回的结果为全排列。
    for p in itertools.permutations(['a', 'b', 'c']):
        # Has the same behaviour as the above statement
        print(p, end=', ')
    print('')

    for p in itertools.permutations(['a', 'b', 'c'], 2):
        # Return any permutations of length 2
        print(p, end=', ')


def test_11():
    # pairwise()是从python3.10引入的一个方法。
    # 它用于按顺序返回一个iterable中每两个相邻的item构成的tuple的列表。
    # 注意，不是任意items两两成对，只有相邻的两个成对，而且在tuple中的排序与在原iterable中的顺序相同。
    lst = [1, 2, 3, 4, 5]
    print("Successive overlapping pairs - ", list(itertools.pairwise(lst)))

    string = "hello educative"
    print("Successive overlapping pairs of characters in a string- ", list(itertools.pairwise(string)))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
