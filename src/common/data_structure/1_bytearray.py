#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Python中7中数据结构: 列表，元组，字符串，bytes，bytearray，集合，字典
# 共同点：都是可迭代对象
# 不同点：列表，bytearray，字典三种是可变的,所有可变类型都是不可hash的
#         元组，集合，字符串，bytes四种不可变，所有不可变的类型都可hash
#
# 字节数组bytearray  可变的字节序列，相当于bytes的可变版本。
# 字节串bytes、字节数组bytearray是二进制数据组成的序列，其中每个元素由8bit二进制组成。
# 字节数是计算机的语言，字符串是人类的语言，他们之间通过编码表形成一一对应关系。
# 最小的 ASCII 编码表只需要一位字节，且只占用了其中 [31,127] 的码位。
# str = bytearray.decode(encoding="utf-8")
# bytearry = str.encode(encoding="utf-8")


# https://www.cnblogs.com/gengyi/p/8438001.html
def test_1():
    # 创建bytearray对象的方法规则
    ba = bytearray()
    print(ba)
    ba = bytearray(1)
    print(ba)
    ba = bytearray([1, 2, 3])
    print(ba)
    ba = bytearray(b"abc")
    print(ba)
    ba = bytearray("abc", encoding='utf-8')
    print(ba)
    # ba.clear()	清空
    # ba.append(n)	追加一个字节(n为0~255的整数)
    # ba.remove(value)	删除第一个出现的字节,如果没有则产生ValueError错误
    # ba.reverse()	字节顺序进行反转
    # ba.decode(encoding='utf-8')	转为字符串
    # ba.find(sub[, start[,end]])	查找子字节串


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
