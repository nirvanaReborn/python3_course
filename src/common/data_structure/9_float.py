#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/shennongzhaizhu/article/details/51997887
# Python中可以用如下方式表示正负无穷：float("inf"), float("-inf")
# inf的运算规则遵从 IEEE-754 standard
#
# 不等式：当涉及 > 和 < 运算时，
#     所有数都比-inf大
#     所有数都比+inf小
# 等式：
#     +inf 和 +inf相等
#     -inf 和 -inf相等


def test_1():
    print(float("inf"))  # inf

    # 利用 inf 做简单加、乘算术运算仍会得到 inf
    print(1 + float('inf'))  # inf

    # 但是利用 inf 乘以0会得到 nan：
    print(0 * float("inf"))  # nan

    # 除了inf外的其他数除以inf，会得到0
    print(889 / float('inf'))  # 0.0
    print(float('inf') / float('inf'))  # nan


# 本题考查的是计算机的浮点运算知识点。
# 不仅是python语言，其他的编程语言也面临同样的问题：
# 在进行浮点运算的时候，10 进制表示方式会丢掉它的精度，造成运算结果与实际结果不符合。
# 这是由于底层 CPU 和运算标准通过自己的浮点单位去执行算术时的特征决定的。
# 看似有穷的小数, 在计算机的二进制表示里却是无穷的。
# 所以在进行高进度浮点运算的时候一定要注意，尤其是自动化测试过程中需要进行断言非常容易出错。
def test_2():
    a = 0.1
    b = 0.2
    c = 0.3
    # assert a + b == c  # 断言失败，抛出 AssertionError 错误。
    assert a + b == c, "[error] 抛出自定义异常信息"

    # 解决方案：使用Decimal模块
    # Decimal() 可以维持浮点数的精度，在金融领域和测试领域等精度要求高的行业有非常大的作用。
    # 但是一定要注意： Decimal() 传的参数一定要是字符串类型，如果是数据类型会再次丢掉精度。
    from decimal import Decimal
    assert Decimal(str(a)) + Decimal(str(b)) == Decimal(str(c))


# https://blog.51cto.com/u_16175498/6893260
def test_3():
    def is_float(str_num):
        try:
            float(str_num)
            return True
        except ValueError:
            return False

    def is_float_2(str_num):
        import re
        pattern = r'^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$'
        return bool(re.match(pattern, str_num))

    def is_float_3(str_num):
        import numpy as np
        return np.isreal(str_num)

    # 测试示例
    print(is_float("3.14"))  # 输出：True
    print(is_float("1.23e-4"))  # 输出：True
    print(is_float("abc"))  # 输出：False


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
