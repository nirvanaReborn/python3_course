#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://selfboot.cn/2016/06/13/python_performance_analysis/
# 假设有一个字符串，想将里面的空格替换为字符‘-’，用python实现起来很简单，下面是四种方案：
# 以上方法适用于比较简单的场合，更复杂的情况下，可以用标准库里面的profile或者cProfile，
# 它可以统计程序里每一个函数的运行时间，并且提供了可视化的报表。
# 大多情况下，建议使用cProfile，它是profile的C实现，适用于运行时间长的程序。
# 不过有的系统可能不支持cProfile，此时只好用profile。

import timeit


def slowest_replace(original_str):
    replace_list = []
    for i, char in enumerate(original_str):
        c = char if char != " " else "-"
        replace_list.append(c)
    return "".join(replace_list)


def slow_replace(original_str):
    replace_str = ""
    for i, char in enumerate(original_str):
        c = char if char != " " else "-"
        replace_str += c
    return replace_str


def fast_replace(original_str):
    return "-".join(original_str.split())


def fastest_replace(original_str):
    return original_str.replace(" ", "-")


def timeit_analyze(func):
    original_str = "hello world"
    t1 = timeit.Timer(lambda: func(original_str))
    print("{:<20}{:10.6} s".format(func.__name__ + ":", t1.timeit(10000)))


def main():
    timeit_analyze(slowest_replace)
    timeit_analyze(slow_replace)
    timeit_analyze(fast_replace)
    timeit_analyze(fastest_replace)


if __name__ == "__main__":
    main()
