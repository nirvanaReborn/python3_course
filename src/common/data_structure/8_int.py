#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# 到了Python 3，long 类型已不复存在，而int 类型变为可以存储任意大小的整数，甚至超过64 位。
# 因此，你可以进行像下面一样计算（10**100 被赋值给名为googol 的变量，
# 这是Google 最初的名字，但由于其拼写困难而被现在的名字所取代）：


def test_1():
    googol = 10 ** 100  # 返回10的100次幂
    print(googol)


# 将整数类型转换为其他类型
def test_2():
    a = 10
    print(str(a))  # 把整数转化为字符串
    print(float(a))  # 把整数转化为浮点型


# 将其他类型转换为整数
def test_3():
    result = 2 ** 64
    print(result, type(result))
    print(int(10.9))
    print(int('0xa', 16))
    print(int('1010', 2))


# 利用bit_length函数测试所占的位数
def test_4():
    a = 1
    print(a.bit_length())  # 1

    a = 1024
    print(a.bit_length())  # 11


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
