#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 使用python操作zookeeper
# https://www.cnblogs.com/hahaha111122222/p/11579380.html
# zookeeper的开发接口以前主要以java和c为主，
# 随着python项目越来越多的使用zookeeper作为分布式集群实现，python的zookeeper接口也出现了很多，
# 现在主流的纯python的zookeeper接口是kazoo。
from kazoo.client import KazooClient


def test_0():
    # 可通过 KazooClient 类直接连接 ZooKeeper ，支持多个 host ，端口默认 2181。
    zk = KazooClient(hosts='127.0.0.1:2181')  # 如果是本地那就写127.0.0.1
    zk.start()  # 与zookeeper连接
    zk.stop()  # 与zookeeper断开


# 创建节点
def test_1():
    zk = KazooClient(hosts='127.0.0.1:2181')  # 如果是本地那就写127.0.0.1
    zk.start()  # 与zookeeper连接
    # makepath=True是递归创建,如果不加上中间那一段，就是建立一个空的节点
    # 注意：空节点的值不能用set修改，否则执行报错！
    zk.create('/abc/JQK/XYZ/0001', b'this is my house', makepath=True)
    node = zk.get_children('/')  # 查看根节点有多少个子节点
    print(node)
    zk.stop()  # 与zookeeper断开


# 删除节点
def test_2():
    zk = KazooClient(hosts='127.0.0.1:2181')  # 如果是本地那就写127.0.0.1
    zk.start()  # 与zookeeper连接
    # recursive=True是递归删除，就是无视下面的节点是否是空，都干掉，不加上的话，会提示子节点非空，删除失败
    zk.delete('/abc/JQK/XYZ/0001', recursive=True)
    node = zk.get_children('/')  # 查看根节点有多少个子节点
    print(node)
    zk.stop()  # 与zookeeper断开


# 更改节点
# 现在假如要在0001这个node里更改value，比如改成："this is my horse!",
# 由于上面节点已经被删除掉了，需要先创建一次。
def test_3():
    zk = KazooClient(hosts='127.0.0.1:2181')  # 如果是本地那就写127.0.0.1
    zk.start()  # 与zookeeper连接
    if not zk.exists("/abc/JQK/XYZ/0001"):
        zk.create('/abc/JQK/XYZ/0001', makepath=True)
    # 注意！set这种增加节点内容的方式是覆盖式增加，并不是在原有基础上增添。而且添加中文的话可能在ZooInspecter里出现的是乱码
    zk.set('/abc/JQK/XYZ/0001', b"this is my horse!")
    node = zk.get_children('/')  # 查看根节点有多少个子节点
    value = zk.get('/abc/JQK/XYZ/0001')  # 查看值
    print(node, value)
    data, stat = zk.get('/abc/JQK/XYZ/0001')  # 查看值
    print("Version: %s, data: %s" % (stat.version, data.decode("utf-8")))
    zk.stop()  # 与zookeeper断开


# 查看所有节点
def test_4():
    zk = KazooClient(hosts='127.0.0.1:2181')  # 如果是本地那就写127.0.0.1
    zk.start()  # 与zookeeper连接
    node = zk.get_children('/')
    print(node)
    zk.stop()  # 与zookeeper断开


# 一键清空zookeeper
# 注意：默认的zookeeper节点，是不允许删除的，所以需要做一个判断。
def test_5():
    zk = KazooClient(hosts='127.0.0.1:2181')  # 如果是本地那就写127.0.0.1
    zk.start()  # 与zookeeper连接
    nodes = zk.get_children('/')  # 查看根节点有多少个子节点
    print(nodes)
    for node in nodes:
        if node != 'zookeeper':  # 判断不等于zookeeper
            print(node)
            # 删除节点
            zk.delete('/%s' % node, recursive=True)
    zk.stop()  # 与zookeeper断开


def test_6():
    def test(event):
        print('触发事件')

    zk = KazooClient(hosts='127.0.0.1:2181')  # 如果是本地那就写127.0.0.1
    zk.start()  # 与zookeeper连接
    if zk.exists("/testplatform/test"):
        zk.get('/testplatform/test', watch=test)
        print("第一次获取value")
        zk.set('/testplatform/test', b'hello')
        zk.get('/testplatform/test', watch=test)
        print("第二次获取value")
    zk.stop()  # 与zookeeper断开


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
