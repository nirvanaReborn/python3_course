#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://zhuanlan.zhihu.com/p/375879187
# https://kazoo.readthedocs.io/en/latest/api/client.html
from kazoo.client import KazooClient
import time
import uuid


def test_1():
    zk = KazooClient(hosts='10.0.0.100:3001,10.0.0.100:4001,10.0.0.100:5001')
    zk.start()

    # 如果不存在，就创建，并返回该node
    # 如果存在，就返回True
    res = zk.ensure_path("/test/zk1")
    print(res)

    # 创建一个带数据节点(如果该节点存在，会报错)
    # res=zk.create("/test/zk1/node", b"a test value 01")
    # print(res)

    # 判断节点是否存在
    if zk.exists("/test/zk1"):
        print("the node exist")

    # 获取节点的数据和状态
    data, stat = zk.get("/test/zk1/node")
    print(data, stat)
    print("Version: %s, data: %s" % (stat.version, data.decode("utf-8")))

    # 获取该节点的子节点
    children = zk.get_children("/test/zk1")
    print("There are %s children with names %s" % (len(children), children))

    zk.stop()

    # 创建节点
    # makepath=True: 递归创建,如果不加上中间那一段，就是建立一个空的节点
    # zk.create('/path/to/watch',b'this is my house',makepath=True)
    # node = zk.get_children('/')  # 查看根节点有多少个子节点
    # print(node)

    # 删除节点
    # recursive=True是递归删除，就是无视下面的节点是否是空，都干掉，不加上的话，会提示子节点非空，删除失败
    # res=zk.delete('/path/to/watch',recursive=True)
    # 把/path下所有都删除
    # res=zk.delete('/path',recursive=True)
    # node = zk.get_children('/')  # 查看根节点有多少个子节点
    # print(node)

    # 更改节点(数据)
    # zk.create('/path/001',b'lqz is nb',makepath=True)
    # zk.set('/path/001',b"lqz is nb,llnb")
    # node = zk.get('/path/001')  # 查看值
    # print(node)

    # 查看节点
    # node = zk.get_children('/')  # 查看根节点有多少个子节点
    # print(node)


# 故障检测
# 在分布式系统中，一个最基本的需求就是当某一个服务出问题的时候，能够通知其它的节点或者某个管理节点。
# ZooKeeper提供ephemeral Node的概念(临时目录)，当创建该Node的服务退出或者异常中止的时候，该Node会被删除，所以我们就可以利用这种行为来监控服务运行状态
def test_2():
    zk = KazooClient(hosts='10.0.0.100:3001')
    zk.start()

    # 如果不存在就创建
    zk.ensure_path("/test/failure_detection")

    # 创建一个有数据节点，模式为ephemeral,如果服务关闭，该节点就会删除
    zk.create("/test/failure_detection/worker", value=b"a test value", ephemeral=True)

    while True:
        print("I am alive!")
        choice = input('输入q退出，其他继续')
        if choice == 'q':
            break
        time.sleep(3)

    # 退出后，节点删除
    zk.stop()


# monitor
def test_3():
    zk = KazooClient(hosts='10.0.0.100:3001')
    zk.start()

    # 如果节点存在，说明work在正常运行
    while True:
        if zk.exists("/test/failure_detection/worker"):
            print("the worker is alive!")
        else:
            print("the worker is dead!")
            break
        time.sleep(3)

    zk.stop()


# 领导选举
# Kazoo直接提供了领导选举的API，使用起来非常方便
# 可以同时运行多个worker，其中一个会获得Leader，当你杀死当前的leader后，会有一个新的leader被选出
# 程序运行多次，只有一个会被执行（leader），其他的都会夯住（follower），当领导worker挂掉，会重新从新从follower中选出一个领导执行
def test_4():
    my_id = uuid.uuid4()

    def leader_func():
        print("I am the leader {}".format(str(my_id)))
        while True:
            print("{} is working! ".format(str(my_id)))
            time.sleep(3)

    zk = KazooClient(hosts='10.0.0.100:3001,10.0.0.100:4001,10.0.0.100:5001')
    zk.start()

    election = zk.Election("/electionpath")

    # blocks until the election is won, then calls leader_func()
    election.run(leader_func)

    ## 启动多次


# 分布式锁
# 当我们希望某一件事在同一时间只有一个服务在做，或者某一个资源在同一时间只有一个服务能访问，这个时候，我们就需要用到锁
# 当你运行多个worker的时候，不同的worker会试图获取同一个锁，然而只有一个worker会工作，其它的worker必须等待获得锁后才能执行
def test_5():
    my_id = uuid.uuid4()

    def work():
        print("{} is start working! ".format(str(my_id)))
        time.sleep(10)
        print("{} is end working! ".format(str(my_id)))

    zk = KazooClient(hosts='10.0.0.100:3001,10.0.0.100:4001,10.0.0.100:5001')
    zk.start()

    lock = zk.Lock("/lockpath", str(my_id))
    print(lock)
    # from kazoo.recipe.lock import Lock
    # 或者用  lock.acquire和lock.release

    print("I am {}".format(str(my_id)))
    while True:
        with lock:
            work()
        print('lock release，其他可以抢到执行了')
        time.sleep(3)

    zk.stop()

    # 运行多次，会发现是交替执行work，只有锁被释放，才能被其他程序抢到执行


# 监视
# ZooKeeper提供了监视（Watch）的功能，当节点的数据被修改的时候，监控的function会被调用。
# 我们可以利用这一点进行配置文件的同步，发消息，或其他需要通知的功能
def test_6():
    zk = KazooClient(hosts='10.0.0.100:3001,10.0.0.100:4001,10.0.0.100:5001')
    zk.start()

    @zk.DataWatch('/path/to/watch')
    def my_func(data, stat):
        if data:
            print("Data is %s" % data)
            print("Version is %s" % stat.version)
        else:
            print("data is not available")

    while True:
        time.sleep(3)


# 测试验证监视
def test_7():
    zk = KazooClient(hosts='10.0.0.100:3001,10.0.0.100:4001,10.0.0.100:5001')
    zk.start()

    # 更改节点(数据)
    zk.create('/path/to/watch', b'lqz is nb', makepath=True)
    # 此处创建就会触发监视的执行
    time.sleep(6)
    # 修改又会触发一次
    zk.set('/path/to/watch', b"change")

    zk.stop()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
