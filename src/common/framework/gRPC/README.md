# [gRPC](https://grpc.io/)

## IDL

grpc 使用 protobuf 来定义接口。按照 protobuf 的 [Style Guide](https://protobuf.dev/programming-guides/style/) 的要求，service 和其中的方法都应该使用 CamelCase。

service 关键字定义一个服务，相当于一个接口。把下面的文件保存为 helloworld.proto

需要注意的是，grpc 中的方法只能接受一个参数，返回一个参数。

## 参考文章

[gRPC-github](https://github.com/grpc/grpc)

[gRPC 官方文档中文版](https://doc.oschina.net/grpc?t=60138)

[grpcio安装不了](https://blog.csdn.net/yuexiaomao/article/details/110933685)

[gRPC python快速入门教程](https://www.tizi365.com/archives/400.html)

[Python gRPC 入门](https://juejin.cn/post/6844903618026405902)

[python 实战 grpc](https://developer.aliyun.com/article/833101)

## 安装

### Python三方库在线安装

Python gPRC tools 包含 protocol buffer 编译器和用于从 .proto 文件生成服务端和客户端代码的插件

```shell
python -m pip install grpcio grpcio-tools protobuf googleapis-common-protos
```

### Python三方库离线下载地址

https://pypi.org/project/grpcio/#files

https://pypi.org/project/protobuf/#files

https://pypi.org/project/grpcio-tools/#files


## Python gRPC 示例

使用以下命令编译:

```shell
python -m grpc_tools.protoc --python_out=. --grpc_python_out=. -I./ ./hello.proto
```

生成了两个文件：

hello_pb2.py 包含了 protobuf 中结构的定义。

hello_pb2_grpc.py 包含了 protobuf grpc 接口的定义。

