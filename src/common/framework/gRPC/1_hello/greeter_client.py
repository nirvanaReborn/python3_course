#!/usr/bin/env python3
# -*- coding:utf-8 -*-


from __future__ import print_function

import grpc
import hello_pb2
import hello_pb2_grpc


def run():
    # 配置grpc服务端地址
    # channel = grpc.insecure_channel('localhost:50051')
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = hello_pb2_grpc.GreeterStub(channel)
        # 请求服务端的SayHello方法
        response = stub.SayHello(hello_pb2.HelloRequest(name='goodspeed'))
        print("Greeter client received: " + response.message)
        # 请求服务端的SayHelloAgain方法
        response = stub.SayHelloAgain(hello_pb2.HelloRequest(name='you'))
        print("Greeter client received: " + response.message)

if __name__ == '__main__':
    run()
