#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from concurrent import futures
import time
import grpc
import MsgDemo_pb2
import MsgDemo_pb2_grpc


# 实现 proto 文件中定义的 SearchService
class RequestRpc(MsgDemo_pb2_grpc.SearchService):
    # 实现 proto 文件中定义的 rpc 调用
    def Search(self, request, context):
        return MsgDemo_pb2.SearchResponse(message='hello {msg}'.format(msg=request.query))  # return的数据是符合定义的SearchResponse格式


def serve():
    # 启动 rpc 服务，这里可定义最大接收和发送大小(单位M)，默认只有4M
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10), options=[
        ('grpc.max_send_message_length', 100 * 1024 * 1024),
        ('grpc.max_receive_message_length', 100 * 1024 * 1024)])

    MsgDemo_pb2_grpc.add_SearchServiceServicer_to_server(RequestRpc(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    try:
        while True:
            time.sleep(60 * 60 * 24)  # one day in seconds
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
