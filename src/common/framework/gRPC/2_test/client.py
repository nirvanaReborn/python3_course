#!/usr/bin/env python3
# -*- coding:utf-8 -*-


import grpc
import MsgDemo_pb2
import MsgDemo_pb2_grpc


def run():
    # 连接 rpc 服务器
    channel = grpc.insecure_channel('localhost:50051')
    # 调用 rpc 服务
    stub = MsgDemo_pb2_grpc.SearchServiceStub(channel)
    response = stub.Search(MsgDemo_pb2.SearchRequest(query='henry'))
    print("client received: ", response)


if __name__ == '__main__':
    run()
