#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/weixin_41970395/article/details/124862175
# python实现自建RocketMQ的数据消费及推送
# pip install rocketmq-client-python
from rocketmq.client import PushConsumer, ConsumeStatus, dll
import time
import json
import logging


def test_1():
    def callback(msg):
        print(msg.id, msg.body)
        return ConsumeStatus.CONSUME_SUCCESS

    consumer = PushConsumer('CID_XXX')
    # consumer.set_name_server_address('127.0.0.1:9876')
    consumer.set_namesrv_domain('http://onsaddr-internet.aliyun.com/rocketmq/nsaddr4client-internet')
    # For ip and port name server address, use `set_namesrv_addr` method, for example:
    # consumer.set_namesrv_addr('127.0.0.1:9887')
    consumer.set_session_credentials('XXX', 'XXXX', 'ALIYUN')

    consumer.subscribe('YOUR-TOPIC', callback)
    consumer.start()

    while True:
        time.sleep(3600)

    consumer.shutdown()


# https://juejin.cn/post/7025960361765371941
class RocketMQ():
    def __init__(self):
        logging.basicConfig(level=logging.CRITICAL, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self.logger = logging.getLogger(__name__)

        self.consumer = PushConsumer("PID-XXX")
        self.consumer.set_namesrv_addr("XX.XX.XX.XX:XXXX")
        self.topic_name = "xxx"

        # 减少日志输出
        dll.SetPushConsumerLogLevel(namesrv_addr.encode('utf-8'), 1)

    def callback(self, msg):
        test_body = json.loads(msg.body)
        try:
            self.my_func(test_body)
            return PushConsumer
        except Exception as e:
            print('>>>>>>>>>>allback msg:\n{}'.format(es_body))
            print('>>>>>>>>>>callback error:\n{}'.format(e))
            return PushConsumer

    def onMessage(self):
        self.consumer.subscribe(self.topic_name, self.callback)
        self.consumer.start()
        while True:
            time.sleep(2)
        self.consumer.shutdown()

    def my_func(test_body):
        print(test_body)


def test_2():
    mq = RocketMQ()
    mq.onMessage()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)



if __name__ == '__main__':
    main()
