#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/weixin_41970395/article/details/124862175
# python实现自建RocketMQ的数据消费及推送
# pip install rocketmq-client-python
from rocketmq.client import Producer, Message
import json


def test_1():
    producer = Producer('PID-XXX')
    producer.set_name_server_address('127.0.0.1:9876')
    producer.start()

    msg = Message('YOUR-TOPIC')
    msg.set_keys('XXX')
    msg.set_tags('XXX')
    msg.set_body('XXXX')
    ret = producer.send_sync(msg)
    print(ret.status, ret.msg_id, ret.offset)
    producer.shutdown()


def test_2():
    producer = Producer('PID-XXX')
    producer.set_namesrv_domain('http://onsaddr-internet.aliyun.com/rocketmq/nsaddr4client-internet')  # rocketmq队列接口地址（服务器ip:port）
    # For ip and port name server address, use `set_namesrv_addr` method, for example:
    # producer.set_namesrv_addr('127.0.0.1:9887')
    producer.set_session_credentials('XXX', 'XXXX', 'ALIYUN')  # 可以不使用
    producer.start()

    topic_name = "xxx"
    key_name = "abc"
    tags = "123"

    msg_body = {
        "id": "test_id",
        "name": "test_name",
        "message": "test_message"
    }
    ss = json.dumps(msg_body).encode('utf-8')

    # topic名称
    msg = Message(topic_name)
    # 每个消息在业务层面的唯一标识码，要设置到keys字段，方便将来定位消息丢失问题。
    # 服务器会为每个消息创建索引（哈希索引），应用可以通过topic，key来查询这条消息内容，以及消息被谁消费。
    # 由于是哈希索引，请务必保证key尽可能唯一，这样可以避免潜在的哈希冲突。
    msg.set_keys(key_name)
    # 一个应用尽可能用一个Topic，消息子类型用tags来标识，tags可以由应用自由设置。
    # 只有发送消息设置了tags，消费方在订阅消息时，才可以利用tags在broker做消息过滤。
    msg.set_tags(tags)
    msg.set_body(ss)
    ret = producer.send_sync(msg)
    print(ret.status, ret.msg_id, ret.offset)
    producer.shutdown()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
