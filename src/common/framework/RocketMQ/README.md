# [RocketMQ](https://rocketmq.apache.org/)

## 参考文章

https://juejin.cn/post/7025960361765371941

## 安装

### centos

```shell
wget https://github.com/apache/rocketmq-client-cpp/releases/download/2.0.0/rocketmq-client-cpp-2.0.0-centos7.x86_64.rpm
sudo rpm -ivh rocketmq-client-cpp-2.0.0-centos7.x86_64.rpm
find / -name librocketmq.so
ln -s /*****/librocketmq.so /usr/lib
```

### debian

```shell
wget https://github.com/apache/rocketmq-client-cpp/releases/download/2.0.0/rocketmq-client-cpp-2.0.0.amd64.deb
sudo dpkg -i rocketmq-client-cpp-2.0.0.amd64.deb
```

### Python三方库在线安装

```shell
python -m pip install rocketmq-client
```

### Python三方库离线下载地址

https://pypi.org/project/rocketmq-client/#files

## Python 示例

