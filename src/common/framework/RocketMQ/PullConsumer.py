#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://juejin.cn/post/7025960361765371941
from rocketmq.client import PullConsumer, dll


def main():
    consumer = PullConsumer('CID_XXX')
    consumer.set_namesrv_domain('http://onsaddr-internet.aliyun.com/rocketmq/nsaddr4client-internet')
    # For ip and port name server address, use `set_namesrv_addr` method, for example:
    # consumer.set_namesrv_addr('127.0.0.1:9887')
    consumer.set_session_credentials('XXX', 'XXXX', 'ALIYUN')
    consumer.start()
    # 控制日志的输出频率
    dll.SetPushConsumerLogLevel(namesrv_addr.encode('utf-8'), 1)

    while True:
        topic_name = "xxx"
        for msg in consumer.pull(topic_name):
            print(msg.id, msg.body)

    consumer.shutdown()


if __name__ == '__main__':
    main()
