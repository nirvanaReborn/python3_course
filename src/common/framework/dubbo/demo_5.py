#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 利用python3调用dubbo服务接口
# https://blog.csdn.net/weixin_38096860/article/details/89711134
# 代码的原理其实就是dubbo支持telnet 调用
# 格式 telnet 服务IP 端口 ，然后会出现dubbo> ,可以通过ls查看服务列表和invoke调用。

import json
import telnetlib


class Dubbo(telnetlib.Telnet):
    prompt = 'dubbo>'
    coding = 'utf-8'

    def __init__(self, host=None, port=0, timeout=1000):
        super().__init__(host, port)
        self.write(b'\n')

    def command(self, flag, str_=""):
        data = self.read_until(flag.encode())
        self.write(str_.encode() + b"\n")
        return data

    def invoke(self, service_name, method_name, arg):
        command_str = "invoke {0}.{1}({2})".format(
            service_name, method_name, json.dumps(arg))
        self.command(Dubbo.prompt, command_str)
        data = self.command(Dubbo.prompt, "")
        data = data.decode(Dubbo.coding, errors='ignore').split('\n')[0].strip()
        return data

    def do(self, arg):
        command_str = arg
        self.command(Dubbo.prompt, command_str)
        data = self.command(Dubbo.prompt, command_str)
        data = data.decode(Dubbo.coding, errors='ignore').split('\n')[0].strip()
        return data


if __name__ == '__main__':
    conn = Dubbo('127.0.0.1', 20880)
    # 格式invoke 接口全名字.方法名(参数1，参数2，参数3...参数n)
    command_str = 'invoke com.test.DubboService.getRes("1","2","3")'
    print(conn.do(command_str))
