# [dubbo](https://cn.dubbo.apache.org/zh-cn/)

## [Dubbo 介绍](https://cn.dubbo.apache.org/zh-cn/overview/what/)

Dubbo 是一款高性能的分布式服务框架，由阿里巴巴公司开发并开源。

它基于RPC的服务调用和服务治理，具有透明化的远程调用、负载均衡、服务注册与发现、高度可扩展性、服务治理等特点。

Dubbo 提供了可扩展的插件机制，支持自定义扩展各种功能，如负载均衡、协议、序列化等。

同时，Dubbo 还提供了丰富的服务治理功能，如监控、追踪、容错、限流等，可以使得服务的运行更加稳定和可靠。

Dubbo 是一种高效、可扩展、可靠的分布式服务框架解决方案，适用于构建大型分布式系统。

## 参考文章

https://gitee.com/happyplay/dubbo-python3

https://blog.51cto.com/u_16213434/8577726

## 安装

### Python三方库在线安装

```shell
python -m pip install dubbo-python3
```

### Python三方库离线下载地址

https://pypi.org/project/python3-dubbo/#files

https://pypi.org/project/pydubbo/#files

https://pypi.org/project/dubbo-python/#files

https://pypi.org/project/dubbo-python3/#files

## Python 示例

Dubbo 是一种高性能、轻量级的分布式服务框架，在 Python 中使用 Dubbo 服务时，我们需要使用 Python 的 Dubbo 客户端来实现 Dubbo 的调用。

目前比较常用的 Python Dubbo 客户端有两种：

1. [PyDubbo](https://github.com/zhouyougit/PyDubbo)：PyDubbo 是一个用 Python 3 实现的 Dubbo 客户端，支持调用 Dubbo 的服务提供者，并支持设置超时时间、重试次数等功能。

2. [Dubbo-Python](https://github.com/dubbo/dubbo-python)：Dubbo-Python 是官方提供的 Dubbo Python 客户端，支持 Python 2 和 Python 3，提供了多种 Dubbo 协议的支持，包括 Dubbo、Hessian 和 JSON-RPC。

使用这些客户端，你可以通过配置 Dubbo 服务的相关参数，比如服务的 IP、端口、协议等，来调用 Dubbo 服务。
