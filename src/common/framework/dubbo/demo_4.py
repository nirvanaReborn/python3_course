#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/sinat_38682860/article/details/108849575
# 公司后端服务使用 java 重构后，很多接口采用了阿里的 dubbo 协议。而 python 是无法直接调用 dubbo 接口的，
# 但可以通过 telnet 调用，具体可以通过 telnetlib 模块的 Telnet类 来调用，只需要四行代码即可实现：

import json
import telnetlib


def test(host, port, cmd):
    # 创建telnet类对象
    conn = telnetlib.Telnet()
    # 连接dubbo接口地址
    conn.open(host, port)
    # 1.cmd命令格式: 接口全名字.方法名(参数1，参数2，参数3...参数n)  2.write方法就是通过telnet发起dubbo请求，参数和单独使用telnet一致
    conn.write('invoke {}\n'.format(cmd).encode())
    # 获取telnet返回信息
    data = conn.read_until('dubbo>'.encode()).decode().split('\r\n')[0]
    print(data)


# 封装成类
class Dubbo(object):
    '''
    方法调用案例：
    conn = Dubbo('127.0.0.1', 18080)
    #格式: 接口全名字.方法名(参数1，参数2，参数3...参数n)
    cmd = 'xxx.xxx.xx.xxxx.xxxx.xxxx.xxxx(268,"sz",1587288615000,1587634215000,0,10)'
    response = json.loads(conn.reuqest(cmd))
    '''
    dubbo = 'dubbo>'

    def __init__(self, host, port):
        self.conn = telnetlib.Telnet()
        self.conn.open(host, port)

    def request(self, cmd):
        self.conn.write('invoke {}\n'.format(cmd).encode())
        data = self.conn.read_until(self.dubbo.encode()).decode().split('\r\n')[0]

        return data


def main():
    # test()
    conn = Dubbo('127.0.0.1', 18080)
    # 格式: 接口全名字.方法名(参数1，参数2，参数3...参数n)
    cmd = 'xxx.xxx.xx.xxxx.xxxx.xxxx.xxxx(268,"sz",1587288615000,1587634215000,0,10)'
    response = json.loads(conn.request(cmd))
    print(response)


if __name__ == '__main__':
    main()
