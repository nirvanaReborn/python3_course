#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://github.com/zhangtao2016/dubbo-python3
# pip install dubbo-python3
import dubbo_client
import time


def main():
    config = dubbo_client.ApplicationConfig('test_rpclib')
    service_interface = 'com.ofpay.demo.api.UserProvider'  # Contains a connection to zookeeper,which needs caching.
    registry = dubbo_client.ZookeeperRegistry('192.168.59.103:2181', config)
    user_provider = dubbo_client.DubboClient(service_interface, registry, version='1.0')
    for i in range(1000):
        try:
            print(user_provider.getUser('A003'))
            print(user_provider.queryUser({u'age': 18, u'time': 1428463514153, u'sex': u'MAN', u'id': u'A003', u'name': u'zhangsan'}))
            print(user_provider.queryAll())
            print(user_provider.isLimit('MAN', 'Joe'))
            print(user_provider('getUser', 'A005'))
        except dubbo_client.DubboClientError as client_error:
            print(client_error)
        time.sleep(5)


if __name__ == '__main__':
    main()
