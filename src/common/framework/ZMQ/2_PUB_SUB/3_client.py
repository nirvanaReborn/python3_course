#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/yunwangjun-python-520/p/10777375.html
# Publish-Subscribe模式（发布订阅模型）
import zmq


def main():
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.connect("tcp://127.0.0.1:5555")
    socket.setsockopt(zmq.SUBSCRIBE, '123'.encode('utf-8'))  # 消息过滤  只接受123开头的信息
    while True:
        response = socket.recv().decode('utf-8')
        print("response: %s" % response)


if __name__ == "__main__":
    main()
