#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/yunwangjun-python-520/p/10777375.html
# Publish-Subscribe模式（发布订阅模型）

# https://www.freesion.com/article/48041167253/
# 一个订阅者（SUBCRIBER）可以链接超过一个发布者（PUBLISHER）。数据到达后将交叉存取（公平队列），以保证发布者之间的数据不会淹没。
'''
* 广播所有client，没有队列缓存，断开连接数据将永远丢失。client可以进行数据过滤。
* 这个模型里，发布端是单向只发送数据的，且不关心是否把全部的信息都发送给订阅者。
* 如果发布端开始发布信息的时候，订阅端尚未连接上，这些信息直接丢弃。
* 不过一旦订阅端连接上来，中间会保证没有信息丢失。
* 同样，订阅端则只负责接收，而不能反馈。
* 如果发布端和订阅端需要交互（比如要确认订阅者是否已经连接上），则使用额外的socket采用请求回应模型满足这个需求。

注意：
       这里的发布与订阅角色是绝对的，即发布者无法使用recv,订阅者不能使用send,并且订阅者需要设置订阅条件"setsockopt"。
按照官网的说法，在这种模式下很可能发布者刚启动时发布的数据出现丢失，原因是用zmq发送速度太快，在订阅者尚未与发布者建立联系时，已经开始了数据发布(内部局域网没这么夸张的)。
官网给了两个解决方案;
1,发布者sleep一会再发送数据(这个被标注成愚蠢的);
2,(还没有看到那，在后续中发现的话会更新这里)。
官网还提供了一种可能出现的问题：当订阅者消费慢于发布，此时就会出现数据的堆积，而且还是在发布端的堆积，显然，这是不可以被接受的。至于解决方案，或许后面的"分而治之"就是吧。
————————————————
版权声明：本文为CSDN博主「达西布鲁斯」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/biheyu828/article/details/88932826
'''

import zmq
import time
import sys


def test_1():
    context = zmq.Context()
    socket = context.socket(zmq.PUB)
    socket.bind("tcp://127.0.0.1:5555")

    while True:
        msg = input("请输入要发布的信息：").strip()
        if msg == 'q':
            sys.exit()
        # memoryview(data)
        socket.send(msg.encode('utf-8'))
        time.sleep(1)


def test_2():
    context = zmq.Context()
    socket = context.socket(zmq.PUB)
    # socket.bind("tcp://127.0.0.1:5555")
    socket.bind("ipc://l2trade.ipc")

    while True:
        msg = {
            "1": time.strftime("%Y-%m-%d %H:%M:%S"),
            "2": "zt",
        }
        # memoryview(msg)
        socket.send_json(msg)
        time.sleep(1)


def main():
    # test_1()
    test_2()


if __name__ == "__main__":
    main()
