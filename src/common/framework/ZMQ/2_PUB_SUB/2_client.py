#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/yunwangjun-python-520/p/10777375.html
# Publish-Subscribe模式（发布订阅模型）

# https://www.freesion.com/article/48041167253/
# 一个订阅者（SUBCRIBER）可以链接超过一个发布者（PUBLISHER）。数据到达后将交叉存取（公平队列），以保证发布者之间的数据不会淹没。
import zmq


def test_1():
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.connect("tcp://127.0.0.1:5555")
    socket.connect("tcp://127.0.0.1:5556")
    socket.setsockopt(zmq.SUBSCRIBE, ''.encode('utf-8'))  # 接收所有消息
    # socket.setsockopt(zmq.SUBSCRIBE, '123'.encode('utf-8'))  # 消息过滤：只接受123开头的信息
    while True:
        response = socket.recv().decode('utf-8')
        print("response: %s" % response)


def test_2():
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    # socket.connect("tcp://127.0.0.1:5555")
    # socket.connect("tcp://127.0.0.1:5556")
    socket.connect("ipc://l2trade.ipc")
    socket.setsockopt(zmq.SUBSCRIBE, ''.encode('utf-8'))  # 接收所有消息
    while True:
        response = socket.recv_json()
        print("response: %s" % response)


def test_3():
    '''
    在 ZeroMQ的发布-订阅模式中，并不直接提供设置超时时间的机制，因为这种模式本质上是面向消息的传递，而不是请求-响应模式。
    然而，你可以通过一些编程技巧来实现某种形式的“超时”或者反应机制：
    '''
    import zmq

    context = zmq.Context()
    subscriber = context.socket(zmq.SUB)
    subscriber.setsockopt(zmq.SUBSCRIBE, b'')  # 订阅所有消息
    subscriber.setsockopt(zmq.RCVTIMEO, 5000)  # 设置接收超时时间为5000毫秒

    try:
        message = subscriber.recv()
        # 处理消息
    except zmq.Again as e:
        print("接收超时，没有接收到消息")


def main():
    # test_1()
    test_2()
    # test_3()


if __name__ == "__main__":
    main()
