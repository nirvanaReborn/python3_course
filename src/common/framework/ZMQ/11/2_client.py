#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/yunwangjun-python-520/p/10777375.html
# Request-Reply模式(请求响应模型)
import zmq
import sys
import threading
from multiprocessing import Process


def zmq_client(client_id):
    context = zmq.Context()
    print("Connecting to server...")
    socket = context.socket(zmq.REQ)
    # socket = context.socket(zmq.DEALER)
    socket.setsockopt(zmq.RCVTIMEO, 5000)
    socket.connect("tcp://127.0.0.1:5555")
    try:
        while True:
            # data = input("{0}请输入内容：".format(client_id)).strip()
            data = str(client_id)
            if data == 'q':
                sys.exit()
            socket.send(data.encode('utf-8'))

            message = socket.recv()
            print("{0} Received reply: ".format(client_id), message.decode('utf-8'))
    except Exception as e:
        print(e)
    finally:
        socket.close()
        context.term()


def main():
    threads = []
    t = threading.Thread(target=zmq_client)
    threads.append(t)
    t.start()
    t = threading.Thread(target=zmq_client)
    threads.append(t)
    t.start()

    for i in range(0, len(threads)):
        threads[i].join()


if __name__ == "__main__":
    for client_id in range(10):
        Process(target=zmq_client, args=(client_id,)).start()
