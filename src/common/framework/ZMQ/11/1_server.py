#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/yunwangjun-python-520/p/10777375.html
# Request-Reply模式(请求响应模型)
'''
从以上的过程，我们可以了解到使用 ZMQ 写基本的程序的方法，需要注意的是
* 服务端和客户端无论谁先启动，效果是相同的，这点不同于 Socket。
* 在服务端收到信息以前，程序是阻塞的，会一直等待客户端连接上来。
* 服务端收到信息以后，会 send 一个“World”给客户端。值得注意的是一定是 client 连接上来以后，send 消息给 Server，然后 Server 再 rev 然后响应 client，这种一问一答式的。如果 Server 先 send，client 先 rev 是会报错的。
* ZMQ 通信通信单元是消息，他除了知道 Bytes 的大小，他并不关心的消息格式。因此，你可以使用任何你觉得好用的数据格式。Xml、Protocol Buffers、Thrift、json 等等。*
* 虽然可以使用 ZMQ 实现 HTTP 协议，但是，这绝不是他所擅长的。
'''
import zmq
import sys
import threading
from zmq.utils.monitor import recv_monitor_message
from zmq.tests import BaseZMQTestCase


def zmq_monitor(context):
    # create listening socket for monitor
    s_event = context.socket(zmq.PAIR)
    s_event.connect("inproc://monitor.rep")
    s_event.linger = 0

    m = recv_monitor_message(s_event)
    print(m)

    if m['event'] == zmq.EVENT_CONNECT_DELAYED:
        BaseZMQTestCase().assertEqual(m['endpoint'], b"tcp://127.0.0.1:5555")
        # test receive event for connected event
        m = recv_monitor_message(s_event)
        BaseZMQTestCase().assertEqual(m['event'], zmq.EVENT_CONNECTED)
        BaseZMQTestCase().assertEqual(m['endpoint'], b"tcp://127.0.0.1:5555")


def zmq_server(context):
    socket = context.socket(zmq.REP)
    # socket = context.socket(zmq.DEALER)
    socket.bind("tcp://127.0.0.1:5555")

    # socket.monitor("inproc://monitor.rep", zmq.EVENT_ALL)
    # zmq_monitor(context)

    while True:
        try:
            print("wait for client ...")
            message = socket.recv()
            print("message from client:", message.decode('utf-8'))

            socket.send(message)
        except Exception as e:
            print('异常:', e)
            socket.close()
            context.term()
            sys.exit()


def main():
    context = zmq.Context()
    zmq_server(context)
    threads = []
    t = threading.Thread(target=zmq_server, args=(context,))
    threads.append(t)
    t.start()
    # t = threading.Thread(target=zmq_monitor, args=(context,))
    # threads.append(t)
    # t.start()

    for i in range(0, len(threads)):
        threads[i].join()


if __name__ == "__main__":
    main()
