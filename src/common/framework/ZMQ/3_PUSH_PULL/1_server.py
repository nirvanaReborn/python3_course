#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/yunwangjun-python-520/p/10777375.html
# Parallel Pipeline模式（管道模型）
'''
* 这个模型里，管道是单向的，从PUSH端单向的向PULL端单向的推送数据流。
* 由三部分组成，push进行* 数据推送，work进行数据缓存，pull进行数据竞争获取处理。
* 区别于Publish-Subscribe（发布订阅模型）存在一个数据缓存和处理负载。
* 当连接被断开，数据不会丢失，重连后数据继续发送到对端。
'''

import zmq
import time


def main():
    context = zmq.Context()
    socket = context.socket(zmq.PUSH)
    socket.bind("tcp://127.0.0.1:5557")

    while True:
        msg = input("请输入要发布的信息：").strip()
        socket.send(msg.encode('utf-8'))
        print("已发送")
        time.sleep(1)


if __name__ == "__main__":
    main()
