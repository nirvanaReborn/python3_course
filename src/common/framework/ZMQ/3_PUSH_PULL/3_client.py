#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/yunwangjun-python-520/p/10777375.html
# Parallel Pipeline模式（管道模型）
# 由三部分组成，push进行数据推送，work进行数据缓存，pull进行数据竞争获取处理。
# 区别于Publish-Subscribe（发布订阅模型）存在一个数据缓存和处理负载。
# 当连接被断开，数据不会丢失，重连后数据继续发送到对端。


def main():
    import zmq
    context = zmq.Context()
    socket = context.socket(zmq.PULL)
    socket.bind("tcp://127.0.0.1:5558")

    while True:
        response = socket.recv().decode('utf-8')
        print("response: %s" % response)


if __name__ == "__main__":
    main()
