#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/yunwangjun-python-520/p/10777375.html
# Parallel Pipeline模式（管道模型）
# 由三部分组成，push进行数据推送，work进行数据缓存，pull进行数据竞争获取处理。
# 区别于Publish-Subscribe（发布订阅模型）存在一个数据缓存和处理负载。
# 当连接被断开，数据不会丢失，重连后数据继续发送到对端。
import zmq


def main():
    context = zmq.Context()
    receive = context.socket(zmq.PULL)
    receive.connect('tcp://127.0.0.1:5557')  # 连接服务端
    sender = context.socket(zmq.PUSH)
    sender.connect('tcp://127.0.0.1:5558')  # 连接客户端

    while True:
        data = receive.recv()
        print("正在转发...", data)
        sender.send(data)


if __name__ == "__main__":
    main()
