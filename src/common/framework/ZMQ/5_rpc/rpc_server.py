#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/biheyu828/article/details/88932826
# 项目实践:locust源码rpc模块探索


import time
from .locust_rpc import Server, Message

host = '127.0.0.1'
port = 5555
data = {'num_clients': 5, 'hatch_rate': 0.2}
server = Server(host=host, port=port)
server.send(Message('hatch', data, None))

data_list = [
    {"hatch": {'num_clients': 1, 'hatch_rate': 0.2}},
    {"start": {'num_clients': 1, 'hatch_rate': 0.2}},
    {"stop": {'num_clients': 1, 'hatch_rate': 0.2}}
]

for data in data_list:
    for k, v in data.items():
        server.send(Message(k, v, None))
        time.sleep(1)