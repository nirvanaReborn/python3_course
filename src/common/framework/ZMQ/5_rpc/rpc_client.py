#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/biheyu828/article/details/88932826
# 项目实践:locust源码rpc模块探索


from .locust_rpc import Client

host = '127.0.0.1'
port = 5555
client = Client(host=host, port=port)

while True:
    data = client.recv()
    msg_type = data.type
    msg_data = data.data
    if msg_type == "hatch":
        print("hatching: {}".format(msg_data))
    else:
        print("hatch type {} : {}".format(msg_type, msg_data))
