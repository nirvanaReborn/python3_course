#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.thinbug.com/q/45041082
# 如何在ZMQ中使用序列化发送图像和数据字符串？

import zmq
import json
import numpy as np
import matplotlib.pyplot as plt
import cv2

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://127.0.0.1:5555")

while True:

    json_str = socket.recv()

    data_print = json.loads(json_str)

    img = np.array(data_print["Image"]["data"])
    img = img.reshape(data_print["Image"]["rows"],data_print["Image"]["cols"], data_print["Image"]["channels"])

    b,g,r = cv2.split(img)
    img = cv2.merge((r,g,b))

    print(img.shape)
    # plt.imshow(img)
    # plt.show()

    Type = data_print['Type']
    Parameter = data_print['Parameter']
    Value = data_print['Value']

    a = {"info": "hello", "num":1}
    socket.send(json.dumps(a))

