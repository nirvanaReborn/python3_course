#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://learning-0mq-with-pyzmq.readthedocs.io/en/latest/pyzmq/pyzmqdevices/monitorqueue.html
# https://stackoom.com/question/1cCcj
'''https://blog.csdn.net/brandohero/article/details/39209093
ZMQ使用注意事项：

1.ZMQ是在发送端缓存消息的，可以通过阈值控制消息的溢出；

2.ZMQ不可以线程之间共享Socket，否则会报org.zeromq.ZMQException: Operation cannot be accomplished in current state错误。

3.ZMQ一个进程只允许有一个Context，new Context(int arg) arg表示后台线程的数量。

4.ZMQ的Socket类有一个Linger参数，可以通过SetLinger设置，主要用于表示该Socket关闭以后，未发送成功的消息是否还保存，
    如果设置为-1表示该消息将永久保存（除非宕机，ZMQ是不持久化消息的），
    如果为0表示所有未发送成功的消息在Socker关闭以后都将立即清除，
    如果是一个正数，则表示该消息在Socket关闭后多少毫秒内被删除；
    这个方法非常有用，尤其在控制发送失败时，是否重发消息。
'''
import zmq
import sys
import threading
import random
import datetime
import traceback
from multiprocessing import Process
import time
import socket


def zmq_api(socket, client_id):
    try:
        # while True:
            data = str(random.randrange(1, 10000))
            if data == 'q':
                sys.exit()
            dt_ms = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
            print(dt_ms, "client_#%s req: " % client_id, data)
            socket.send(data.encode('utf-8'))

            message = socket.recv()
            dt_ms = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
            print(dt_ms, "client_#{0} Received reply: [{1}]".format(client_id, message.decode('utf-8')))
    except Exception as e:
        print(e)


def zmq_client_1(client_id, server_url="tcp://127.0.0.1:4444"):
    context = zmq.Context()
    # opt = 1
    # context._get_attr_opt('IO_THREADS', opt)
    # print(opt)
    print("Connecting to server...")
    socket = context.socket(zmq.REQ)
    # socket = context.socket(zmq.DEALER)
    # 设置socket 身份ID
    socket.setsockopt_string(zmq.IDENTITY, "client_#" + str(client_id))
    socket.setsockopt(zmq.LINGER, 0)  # 为socket关闭设置停留时间
    socket.setsockopt(zmq.AFFINITY, 1)  # 设置I/O线程关联
    # socket.setsockopt(zmq.RCVTIMEO, 1)  # Resource temporarily unavailable
    socket.setsockopt(zmq.RCVTIMEO, 2000)  # 单位毫秒
    # print(socket.getsockopt(zmq.RCVTIMEO))  # 默认-1，无限等待
    socket.connect(server_url)
    threads = []
    try:
        for i in range(0, 3):
            # ZMQ不可以线程之间共享Socket，否则会报org.zeromq.ZMQException: Operation cannot be accomplished in current state错误。
            t = threading.Thread(target=zmq_api,
                                args=(socket, str(client_id) + "-" + str(i)))
            threads.append(t)
            t.start()

        for i in range(0, len(threads)):
            threads[i].join()
    except Exception as e:
        print(e)
        # traceback.print_exc()
    finally:
        socket.close()
        context.term()


def zmq_client_2(client_id, server_url="tcp://127.0.0.1:4444"):
    context = zmq.Context()
    # opt = 1
    # context._get_attr_opt('IO_THREADS', opt)
    # print(opt)
    print("Connecting to server...")
    socket = context.socket(zmq.REQ)
    # socket = context.socket(zmq.DEALER)
    # 设置socket 身份ID
    socket.setsockopt_string(zmq.IDENTITY, "client_#" + str(client_id))
    # socket.setsockopt(zmq.LINGER, 0)  # 为socket关闭设置停留时间
    socket.setsockopt(zmq.AFFINITY, 1)  # 设置I/O线程关联
    # socket.setsockopt(zmq.RCVTIMEO, 1)  # Resource temporarily unavailable
    socket.setsockopt(zmq.RCVTIMEO, 2000)  # 单位毫秒
    # print(socket.getsockopt(zmq.RCVTIMEO))  # 默认-1，无限等待
    socket.connect(server_url)
    threads = []
    try:
        # for i in range(0, 3):
        #     zmq_api(socket, str(client_id) + "-" + str(i))
        zmq_api(socket, str(client_id))
    except Exception as e:
        print(e)
        # traceback.print_exc()
    finally:
        socket.close()
        context.term()


# https://stackoverflow.com/questions/41009900/python-zmq-operation-cannot-be-accomplished-in-current-state
def zmq_client_3(client_id, server_url="tcp://127.0.0.1:4444"):
    REQUEST_TIMEOUT = 2000
    REQUEST_RETRIES = 3

    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    # 设置socket 身份ID
    socket.setsockopt_string(zmq.IDENTITY, "client_#" + str(client_id))
    socket.connect(server_url)

    poll = zmq.Poller()
    poll.register(socket, zmq.POLLIN)

    retries_left = REQUEST_RETRIES
    while retries_left:
        data = str(random.randrange(1, 10000))
        request = data.encode()
        # print("I: Sending (%s)" % request)
        dt_ms = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
        print(dt_ms, "client_#%s req: " % client_id, data)
        socket.send(request)

        expect_reply = True
        while expect_reply:
            socks = dict(poll.poll(REQUEST_TIMEOUT))
            if socks.get(socket) == zmq.POLLIN:
                reply = socket.recv()
                if not reply:
                    break
                if reply.decode('utf-8') == data:
                    print("I: Server replied OK (%s)" % reply)
                    retries_left = REQUEST_RETRIES
                    expect_reply = False
                else:
                    print("E: Malformed reply from server: %s" % reply)
            else:
                print("W: No response from server, retrying…")
                # Socket is confused. Close and remove it.
                socket.setsockopt(zmq.LINGER, 0)
                socket.close()
                poll.unregister(socket)
                retries_left -= 1
                if retries_left == 0:
                    print("E: Server seems to be offline, abandoning")
                    break
                print("I: Reconnecting and resending (%s)" % request)
                # Create new connection
                socket = context.socket(zmq.REQ)
                # 设置socket 身份ID
                socket.setsockopt_string(zmq.IDENTITY, "client_#" + str(client_id))
                socket.connect(server_url)
                poll.register(socket, zmq.POLLIN)
                dt_ms = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
                print(dt_ms, "[retry]client_#%s req: " % client_id, data)
                socket.send(request)

    context.term()


def main():
    threads = []
    for i in range(0, 10):
        t = threading.Thread(target=zmq_client_1, args=(i,))
        threads.append(t)
        t.start()

    for i in range(0, len(threads)):
        threads[i].join()


if __name__ == "__main__":
    # main()
    threads = []
    try:
        for i in range(0, 1):
            t = Process(target=zmq_client_2, args=(i,))
            threads.append(t)
            t.start()

        for i in range(0, len(threads)):
            threads[i].join()
    except Exception as e:
        print(e)
        for i in range(0, len(threads)):
            threads[i].terminate()
