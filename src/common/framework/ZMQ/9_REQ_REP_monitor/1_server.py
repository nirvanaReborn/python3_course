#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://learning-0mq-with-pyzmq.readthedocs.io/en/latest/pyzmq/pyzmqdevices/monitorqueue.html
# https://stackoom.com/question/1cCcj
import zmq
import sys
import time


def main():
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    # socket = context.socket(zmq.DEALER)
    socket.bind("tcp://127.0.0.1:5555")
    while True:
        try:
            print("wait for client ...")
            message = socket.recv()
            print("message from client:", message.decode('utf-8'))
            time.sleep(1)
            socket.send(message)
            # socket.send_string("")
        except Exception as e:
            print('异常:', e)
            socket.close()
            context.term()
            sys.exit()


if __name__ == "__main__":
    main()
