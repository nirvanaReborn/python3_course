#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://learning-0mq-with-pyzmq.readthedocs.io/en/latest/pyzmq/pyzmqdevices/monitorqueue.html
# https://stackoom.com/question/1cCcj
# 使用Python中的ZMQ在client/server配置中使用monitor

import zmq
from zmq.devices.monitoredqueuedevice import MonitoredQueue
from zmq.utils.strtypes import asbytes
from multiprocessing import Process
import time
import socket


def get_host_ip():
    """ 查询本机ip地址 """
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        ip = s.getsockname()[0]
    finally:
        s.close()

    return ip


# MonitoredQueue绑定到客户端端口4444，连接到服务端端口5555，在端口6666上发布流量
def monitorServer(frontend_url="tcp://127.0.0.1:4444",
                  server_url="tcp://127.0.0.1:5555",
                  capture_url="tcp://127.0.0.1:6666"):
    try:
        mondev = MonitoredQueue(zmq.ROUTER, zmq.DEALER, zmq.PUB, asbytes("client_req"), asbytes("server_rep"))
        mondev.bind_in(frontend_url)
        mondev.connect_out(server_url)
        mondev.bind_mon(capture_url)
        # 对进入socket的消息设置高水位（最大缓存量）, 默认为1000
        mondev.setsockopt_in(zmq.RCVHWM, 1)
        # 对向外发送的消息设置高水位（最大缓存量）,默认为1000
        mondev.setsockopt_out(zmq.SNDHWM, 1)
        mondev.start()
        print("monitored queue started")
    except Exception as e:
        print(e)


# def monitorClient(server_url="tcp://127.0.0.1:6666"):
#     context = zmq.Context()
#     socket = context.socket(zmq.SUB)
#     socket.connect(server_url)
#     socket.setsockopt_string(zmq.SUBSCRIBE, "")
#     print("started monitoring client")
#     try:
#         while True:
#             res = socket.recv_multipart()
#             print(res)
#     except Exception as e:
#         print(e)
#     finally:
#         socket.close()
#         context.term()


if __name__ == "__main__":
    # ip = get_host_ip()  # 可以修改为其他机器IP
    # frontend_port = 4444
    # reply_port = 5555  # /home/HQServer/bin/configure 配置文件中reply_port参数的值
    # monitor_port = 6666  # 自定义一个端口
    # frontend_url = "tcp://{0}:{1}".format(ip, frontend_port)
    # server_url = "tcp://{0}:{1}".format(ip, reply_port)
    # capture_url = "tcp://{0}:{1}".format(ip, monitor_port)
    # print(frontend_url, server_url, capture_url)
    # monitorServer(frontend_url, server_url, capture_url)
    monitorServer()
    # try:
    #     monitoring_p = Process(target=monitorServer)
    #     monitoring_p.start()
    #     time.sleep(1)
    #     monitorclient_p = Process(target=monitorClient)
    #     monitorclient_p.start()
    #     time.sleep(1)
    # except Exception as e:
    #     print(e)
    #     monitorclient_p.terminate()
    #     monitoring_p.terminate()
