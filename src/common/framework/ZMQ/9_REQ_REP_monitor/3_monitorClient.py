#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://learning-0mq-with-pyzmq.readthedocs.io/en/latest/pyzmq/pyzmqdevices/monitorqueue.html
# https://stackoom.com/question/1cCcj
# 使用Python中的ZMQ在client/server配置中使用monitor

import zmq
import datetime
import logging
import socket

logging.basicConfig(level=logging.INFO,
                    # format='%(asctime)s.%(msecs)03d %(message)s',
                    format='%(message)s',
                    datefmt='%Y%m%d %H:%M:%S',
                    filename='myapp.log',
                    filemode='a')


def get_host_ip():
    """ 查询本机ip地址 """
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        ip = s.getsockname()[0]
    finally:
        s.close()

    return ip


def monitorClient(server_url="tcp://127.0.0.1:6666"):
    context = zmq.Context()
    socket = context.socket(zmq.SUB)
    socket.connect(server_url)
    socket.setsockopt_string(zmq.SUBSCRIBE, "")
    print("started monitoring client")
    try:
        while True:
            res = socket.recv_multipart()
            dt_ms = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
            print(dt_ms, res)
            logging.info(dt_ms + str(res))
    except Exception as e:
        print(e)
    finally:
        socket.close()
        context.term()


if __name__ == "__main__":
    # ip = get_host_ip()  # 可以修改为其他机器IP
    # monitor_port = 6666  # 自定义一个端口
    # server_url = "tcp://{0}:{1}".format(ip, monitor_port)
    # print(server_url)
    # monitorClient(server_url)
    monitorClient()
'''
每个都有一个结构[prefix, identity, emptyframe, message]，其中

prefix是“in”或“out”
identity是由MonitoredQueues分配给特定客户机的字符串。每次客户端连接时，此标识可能会更改。作为奖励，我们可以连接多个客户端，并且仍然有机会区分不同的客户端。如果您需要特定的客户端标识，请参阅client.py中带有{}的注释行。如果取消注释，您将看到"client_id_abc"作为客户的标识。
emptyframe被视为''，它正在从消息数据中分隔信封。
message是客户端询问的内容或服务器的答复。
'''
