#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/u012206617/article/details/90782547
#

from gevent import Greenlet
import gevent
from zmq.auth.ioloop import IOLoopAuthenticator # IO循环验证程序
import zmq.green as zmq
from zmq.auth.certs import create_certificates # 创建证书
from zmq.auth.certs import load_certificate # 读取证书
import os
import json
import shutil
import logging
import Messages
import beeswarm
import SocketNames


class APP():
    tempfile = None

    def init(self):
        context = beeswarm.shared.zmq_context
        self.config_commands = context.socket(zmq.REP)
        self.enabled = True


    def run(self):
        self.config_commands.bind(SocketNames.CONFIG_COMMANDS.value)
        poller = zmq.Poller()
        poller.register(self.config_commands, zmq.POLLIN)
        while self.enabled:
            socks = dict(poller.poll(500))
            if self.config_commands in socks and socks[self.config_commands] == zmq.POLLIN:
                self._handle_commands()


    def _handle_commands(self):
        msg = self.config_commands.recv()
        if ' ' in msg:
            cmd, data = msg.split(' ', 1)
        else:
            cmd = msg
        logging.debug('Received command: {0}'.format(cmd))

        if cmd == Messages.SET_CONFIG_ITEM.value:  # SET  重新设置配置文件
            self._handle_command_set(data)
            self.config_commands.send('{0} {1}'.format(Messages.OK.value, '{}'))
        elif cmd == Messages.GET_CONFIG_ITEM.value:  # GET 获取配置文件某KEY值
            value = self._handle_command_get(data)
            self.config_commands.send('{0} {1}'.format(Messages.OK.value, value))
        elif cmd == Messages.GET_ZMQ_KEYS.value:  # 返回客户端证书
            self._handle_command_getkeys(data)
        elif cmd == Messages.DELETE_ZMQ_KEYS.value:  # 删除客户端证书
            self._remove_zmq_keys(data)
            self.config_commands.send('{0} {1}'.format(Messages.OK.value, '{}'))
        else:
            logging.error('Unknown command received: {0}'.format(cmd))
            self.config_commands.send(Messages.FAIL.value)


    def _handle_command_set(self, data):
        new_config = json.loads(data)
        self.config.update(new_config)
        self._save_config_file()


    def _handle_command_get(self, data):
        # example: 'network,host' will lookup self.config['network']['host']
        # 示例：“network，host”将查找self.config[“network”][“host”]
        keys = data.split(',')
        value = self._retrieve_nested_config(keys, self.config)
        return value


    def _retrieve_nested_config(self, keys, dict):
        if keys[0] in dict:
            if len(keys) == 1:
                return dict[keys[0]]
            else:
                return self._retrieve_nested_config(keys[1:], dict[keys[0]])


    def _handle_command_getkeys(self, name):
        private_key, publickey = self._get_zmq_keys(name)
        self.config_commands.send(Messages.OK.value + ' ' + json.dumps({'public_key': publickey,
                                                                        'private_key': private_key}))


    def _save_config_file(self):
        with open(self.config_file, 'w+') as config_file:
            config_file.write(json.dumps(self.config, indent=4))


    def _get_zmq_keys(self, id):
        cert_path = os.path.join(self.work_dir, 'certificates')
        public_keys = os.path.join(cert_path, 'public_keys')
        private_keys = os.path.join(cert_path, 'private_keys')
        public_key_path = os.path.join(public_keys, '{0}.pub'.format(id))
        private_key_path = os.path.join(private_keys, '{0}.pri'.format(id))

        if not os.path.isfile(public_key_path) or not os.path.isfile(private_key_path):
            logging.debug('Generating ZMQ keys for: {0}.'.format(id))
            for _path in [cert_path, public_keys, private_keys]:
                if not os.path.isdir(_path):
                    os.mkdir(_path)

            tmp_key_dir = tempfile.mkdtemp()
            try:
                public_key, private_key = create_certificates(tmp_key_dir, id)
                # the final location for keys
                # 钥匙的最终位置
                shutil.move(public_key, public_key_path)
                shutil.move(private_key, private_key_path)
            finally:
                shutil.rmtree(tmp_key_dir)

        # return copy of keys
        # 返回密钥副本
        return open(private_key_path, "r").readlines(), open(public_key_path, "r").readlines()


    def _remove_zmq_keys(self, id):
        cert_path = os.path.join(self.work_dir, 'certificates')
        public_keys = os.path.join(cert_path, 'public_keys')
        private_keys = os.path.join(cert_path, 'private_keys')
        public_key_path = os.path.join(public_keys, '{0}.pub'.format(id))
        private_key_path = os.path.join(private_keys, '{0}.pri'.format(id))

        for _file in [public_key_path, private_key_path]:
            if os.path.isfile(_file):
                os.remove(_file)


    def message_proxy(self, work_dir):
        """
        drone_data_inboud   is for data comming from drones
        drone_data_outbound is for commands to the drones, topic must either be a drone ID or all for sending a broadcast message to all drones
        无人机内部数据用于无人机的数据混合。
        无人机数据输出用于向无人机发送命令，主题必须是无人机ID或全部用于向所有无人机发送广播消息
        """
        public_keys_dir = os.path.join(work_dir, 'certificates', 'public_keys')
        secret_keys_dir = os.path.join(work_dir, 'certificates', 'private_keys')

        # start and configure auth worker
        # 启动并配置身份验证工作程序
        auth = IOLoopAuthenticator()
        auth.start()
        auth.allow('127.0.0.1')
        auth.configure_curve(domain='*', location=public_keys_dir)

        # external interfaces for communicating with drones
        # 与无人机通信的外部接口
        # 服务器证书
        server_secret_file = os.path.join(secret_keys_dir, 'beeswarm_server.pri')
        # 获取公钥和密钥
        server_public, server_secret = load_certificate(server_secret_file)
        drone_data_inbound = beeswarm.shared.zmq_context.socket(zmq.PULL)
        drone_data_inbound.curve_secretkey = server_secret
        drone_data_inbound.curve_publickey = server_public
        drone_data_inbound.curve_server = True
        drone_data_inbound.bind('tcp://127.0.0.1:{0}'.format(self.config['network']['zmq_port']))

        drone_data_outbound = beeswarm.shared.zmq_context.socket(zmq.PUB)
        drone_data_outbound.curve_secretkey = server_secret
        drone_data_outbound.curve_publickey = server_public
        drone_data_outbound.curve_server = True
        drone_data_outbound.bind('tcp://127.0.0.1:{0}'.format(self.config['network']['zmq_command_port']))

        # internal interfaces
        # all inbound session data from drones will be replayed on this socket
        # 内部接口,来自无人机的所有入站会话数据将在此套接字上重播
        drone_data_socket = beeswarm.shared.zmq_context.socket(zmq.PUB)
        drone_data_socket.bind(SocketNames.DRONE_DATA.value)

        # all commands received on this will be published on the external interface
        # 在此上接收到的所有命令都将在外部接口上发布
        drone_command_socket = beeswarm.shared.zmq_context.socket(zmq.PULL)
        drone_command_socket.bind(SocketNames.DRONE_COMMANDS.value)

        poller = zmq.Poller()
        poller.register(drone_data_inbound, zmq.POLLIN)
        poller.register(drone_command_socket, zmq.POLLIN)
        while True:
            # .recv() gives no context switch - why not? using poller with timeout instead
            # recv（）不提供上下文切换-为什么不？将轮询器与超时一起使用
            socks = dict(poller.poll(100))
            gevent.sleep()

            if drone_command_socket in socks and socks[drone_command_socket] == zmq.POLLIN:
                data = drone_command_socket.recv()
                drone_id, _ = data.split(' ', 1)
                logging.debug("Sending drone command to: {0}".format(drone_id))
                # pub socket takes care of filtering
                # pub socket负责过滤
                drone_data_outbound.send(data)
            elif drone_data_inbound in socks and socks[drone_data_inbound] == zmq.POLLIN:
                raw_msg = drone_data_inbound.recv()
                split_data = raw_msg.split(' ', 2)
                if len(split_data) == 3:
                    topic, drone_id, data = split_data
                else:
                    data = None
                    topic, drone_id, = split_data
                logging.debug("Received {0} message from {1}.".format(topic, drone_id))
                # relay message on internal socket
                # 内部插座上的中继消息
                drone_data_socket.send(raw_msg)
