#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/yunwangjun-python-520/p/10777375.html
# Request-Reply模式(请求响应模型)
import zmq
import sys
import random


def main():
    context = zmq.Context()
    print("Connecting to server...")
    socket = context.socket(zmq.REQ)
    # 设置socket 身份ID
    socket.setsockopt_string(zmq.IDENTITY, "client_#" + str(random.randint(1, 10)))
    socket.setsockopt(zmq.RCVTIMEO, 10000)
    socket.connect("tcp://127.0.0.1:5555")
    try:
        while True:
            data = input("请输入内容：").strip()
            if data == 'q':
                sys.exit()
            socket.send(data.encode('utf-8'))

            message = socket.recv()
            print("Received reply: ", message.decode('utf-8'))
    except Exception as e:
        print(e)
    finally:
        socket.close()
        context.term()


if __name__ == "__main__":
    main()
