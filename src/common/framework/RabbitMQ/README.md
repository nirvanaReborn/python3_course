# [RabbitMQ](https://www.rabbitmq.com/tutorials)

## 参考文章

https://juejin.cn/post/6996470326271950878

## 安装

### Python三方库在线安装

```shell
python -m pip install pika
```

### Python三方库离线下载地址

https://pypi.org/project/pika/#files

## Python 示例

