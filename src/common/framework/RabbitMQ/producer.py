#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://juejin.cn/post/6996470326271950878

import pika
import sys


def main():
    if len(sys.argv) < 2:
        # print('Usage: {} message'.format(sys.argv[0]))
        message = "hello world"
    else:
        # 获取命令行参数
        message = sys.argv[1]

    # 可以连接远程，localhost换成ip地址或者域名
    credentials = pika.PlainCredentials('xugaoxiang', 'xugaoxiang')
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost', 5672, '/', credentials))
    channel = connection.channel()

    # 发送消息之前需要确认接收队列是否存在? 如果不存在，RabbitMQ就会丢弃。下面语句是我们创建`hello`队列
    channel.queue_declare(queue='hello')

    # 最后是发送
    channel.basic_publish(exchange='', routing_key='hello', body=message.encode('utf-8'))
    print('Sent message: {}'.format(message))
    connection.close()


if __name__ == '__main__':
    main()
