#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://juejin.cn/post/6996470326271950878

import pika


def callback(ch, method, properties, body):
    print('Receive: {}'.format(body))


def main():
    # 建立连接跟发送端的一样
    credentials = pika.PlainCredentials('xugaoxiang', 'xugaoxiang')
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost', 5672, '/', credentials))
    channel = connection.channel()

    channel.queue_declare(queue='hello')

    # 注册回调函数callback, no_ack表示不支持消息反馈，在多任务的情况下，这个很有用，
    # `producer`能够告诉RabbitMQ消息正被接收和处理。如果处理该消息的进程挂了，可以通过这个机制
    channel.basic_consume(on_message_callback=callback, queue='hello', auto_ack=True)
    print('Waiting for messages. To exit press CTRL+C')

    # 进入事件循环
    channel.start_consuming()


if __name__ == '__main__':
    main()
