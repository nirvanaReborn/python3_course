#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/shenh/p/10529073.html

import socket
import json
import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname('__file__'), os.path.pardir)))

from example import Transmit
from example import ttypes
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer


class TransmitHandler:
    def __init__(self):
        self.log = {}

    def sayMsg(self, msg):
        msg = json.loads(msg)
        print("sayMsg(" + msg + ")")
        return "say " + msg + " from " + socket.gethostbyname(socket.gethostname())

    def invoke(self, cmd, token, data):
        cmd = cmd
        token = token
        data = data
        if cmd == 1:
            return json.dumps({token: data})
        else:
            return 'cmd不匹配'


class FormatDataHandler(object):
    def do_format(self, data):
        print(data.text, data.id)
        # can do something
        return ttypes.Data(data.text.upper(), data.id)


if __name__ == "__main__":
    handler = TransmitHandler()
    # handler = FormatDataHandler()
    processor = Transmit.Processor(handler)
    transport = TSocket.TServerSocket('127.0.0.1', 8000)
    # 传输方式，使用buffer
    tfactory = TTransport.TBufferedTransportFactory()
    # 传输的数据类型：二进制
    pfactory = TBinaryProtocol.TBinaryProtocolFactory()
    # 创建一个thrift 服务
    rpcServer = TServer.TSimpleServer(processor, transport, tfactory, pfactory)
    print("Starting python server...")
    rpcServer.serve()
    print('done')
