/*
IDL: thrift接口定义文件
*/

namespace py example

struct Data {
    1: string text
    2: i32 id
}

service format_data {
    Data do_format(1:Data data),
}

service Transmit {
	string sayMsg(1:string msg);
	string invoke(1:i32 cmd 2:string token 3:string data)
}
