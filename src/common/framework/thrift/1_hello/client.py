#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import json
import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname('__file__'), os.path.pardir)))

from example import Transmit
from example import ttypes
from example import constants
from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol


def main():
    try:
        transport = TSocket.TSocket('127.0.0.1', 8000)
        transport = TTransport.TBufferedTransport(transport)
        protocol = TBinaryProtocol.TBinaryProtocol(transport)
        client = Transmit.Client(protocol)
        # Connect!
        transport.open()

        if True:
            cmd = 1
            token = '1111-2222-3333-4444'
            data = json.dumps({"name": "zhoujielun"})
            print('client-requets')
            msg = client.invoke(cmd, token, data)
            print('server-answer:', msg)
        else:
            data = Data('hello,world!', 123)
            print('client-requets')
            res = client.do_format(data)
            # print(client.do_format(data).text)
            print('server-answer', res)

        transport.close()
    except Thrift.TException as ex:
        print(ex)


if __name__ == '__main__':
    main()
