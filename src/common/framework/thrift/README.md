# [Thrift](https://thrift.apache.org/tutorial/py.html)

Apache Thrift 是 Facebook 实现的一种高效的、支持多种编程语言的远程服务调用的框架。

Thrift 是一种接口描述语言和二进制通信协议。

Thrift 最初由FaceBook研发，主要用于各个服务之间的RPC通信，支持跨语言；简单的说，就是可以让人快速的写Socket Server(服务器)和Client(客户端)。

其实不用Thrift开发socket也不难，那么为什么要用Thrift开发呢？主要有两个原因，

一个是因为Thrift本身帮你封装了很多基本的东西，你不需要自己去写socket里面的bind，accept之类的，以及他们的逻辑。可以很快速的开发基于进程的，线程的，SSL的socket。

第二个理由是标准化，跨语言和跨平台，Windows不算在其中。主要是在各种POSIX兼容的操作系统中都可以不需要改造基本直接可用，支持的语言种类也很多。类似的项目还有ICE和Avro，但是感觉都没有Thrift做的易用性好。

使用thrift需要先定义接口文件，在Thrift里简称叫IDL，全称叫Interface Description Language，接口描述语言。接口描述语言里面需要定义接口中所使用的数据类型，方法等等。

需要定义一个xxx.thrift的文件，来生成各种语言的代码，生成之后我们的服务提供者和消费者，都需要把代码引入，服务端把代码实现，消费者直接使用API的存根，直接调用。

和 http 相比，同属于应用层，走 tcp 协议。Thrift 优势在于发送同样的数据，request 包和 response 包要比http小很多，在整体性能上要优于 http。

## 参考文章

https://thrift.apache.org/docs/concepts

https://www.cnblogs.com/shenh/p/10529073.html

https://www.cnblogs.com/276815076/p/10078645.html

## 安装

从官网上下载 windows 版的 thrift.exe: 

https://archive.apache.org/dist/thrift/

### Python三方库在线安装

```shell
python -m pip install thrift
```

### Python三方库离线下载地址

https://pypi.org/project/thrift/#files

## Thrift简介

### Transport
1. Transport网络读写（socket，http等）抽象，用于和其他thrift组件解耦。
2. Transport的接口包括：open, close, read, write, flush, isOpen, readAll。
3. Server端需要ServerTransport（对监听socket的一种抽象），用于接收客户端连接，接口包括：listen, accept, close。
4. python中Transport的实现包括：TSocket, THttpServer, TSSLSocket, TTwisted, TZlibTransport，都是对某种协议或框架的实现。还有两个装饰器，用于为已有的Transport添加功能，TBufferedTransport（增加缓冲）和TFramedTransport（添加帧）。
5. 在创建server时，传入的时Tranport的工厂，这些Factory包括：TTransportFactoryBase（没有任何修饰，直接返回），TBufferedTransportFactory（返回带缓冲的Transport）和TFramedTransportFactory（返回帧定位的Transport）。

### Protocol
1. Protocol用于对数据格式抽象，在rpc调用时序列化请求和响应。
2. TProtocol的实现包括：TJSONProtocol，TSimpleJSONProtocol，TBinaryProtocol，TBinaryPotocolAccelerated，TCompactProtocol。

### Processor
1. Processor对stream读写抽象，最终会调用用户编写的handler已响应对应的service。具体的Processor有compiler生成，用户需要实现service的实现类。

### Server
1. Server创建Transport，输入、输出的Protocol，以及响应service的handler，监听到client的请求然后委托给processor处理。
2. TServer是基类，构造函数的参数包括：
    > 1) processor, serverTransport    
    > 2) processor, serverTransport, transportFactory, protocolFactory
    > 3) processor, serverTransport, inputTransportFactory, outputTransportFactory, inputProtocolFactory, outputProtocolFactory   
3. TServer内部实际上需要3）所列的参数，1）和2）会导致对应的参数使用默认值。
4. TServer的子类包括：TSimpleServer, TThreadedServer, TThreadPoolServer, TForkingServer, THttpServer, TNonblockingServer, TProcessPoolServer
5. TServer的serve方法用于开始服务，接收client的请求。

### Code generated
1. constants.py: 包含声明的所有常量
2. ttypes.py: 声明的struct，实现了具体的序列化和反序列化
3. SERVICE_NAME.py: 对应service的描述文件，包含了：
    > 1) Iface: service接口定义    
    > 2) Client: client的rpc调用桩    


## Python 示例

1. 首先使用 thrift 之前需要定义一个 .thrift 格式的文件。
2. 然后运行命令生成 python 代码：
```bash
D:\ProgramFiles\1_FreeInstallation\thrift\thrift-0.20.0.exe -out . -gen py  example.thrift
```

## 部分详细介绍

**传输协议** 

在传输协议上总体划分为文本和二进制 ,为节约带宽，提高传输效率，一般情况下使用二进制类型的传输协议为多数.

*   TBinaryProtocol — 二进制编码格式进行数据传输
*   TCompactProtocol — 高效率的、密集的二进制编码格式进行数据传输
*   TJSONProtocol — 使用 JSON 的数据编码协议进行数据传输
*   TSimpleJSONProtocol — 只提供 JSON 只写的协议，适用于通过脚本语言解析
*   TDebugProtocol – 使用易懂的可读的文本格式，以便于 debug

**数据传输**

*   TSocket — 使用阻塞式 I/O 进行传输，是最常见的模式
*   TFramedTransport — 使用非阻塞方式，按块的大小进行传输
*   TNonblockingTransport — 使用非阻塞方式，用于构建异步客户端
*   TMemoryTransport – 将内存用于 I/O
*   TZlibTransport – 使用 zlib 进行压缩， 与其他传输方式联合使用
*   TFileTransport – 以文件形式进行传输

**服务端类型**

*   TSimpleServer — 单线程服务器端使用标准的阻塞式 I/O
*   TThreadPoolServer —— 多线程服务器端使用标准的阻塞式 I/O
*   TNonblockingServer —— 多线程服务器端使用非阻塞式 I/O

**数据类型** 

Thrift 脚本可定义的数据类型包括以下几种类型：

*   基本类型：
    *   bool：布尔值，true 或 false
    *   byte：8 位有符号整数
    *   i16：16 位有符号整数
    *   i32：32 位有符号整数
    *   i64：64 位有符号整数
    *   double：64 位浮点数
    *   string：未知编码文本或二进制字符串
*   结构体类型：
    *   struct：定义公共的对象，类似于 C 语言中的结构体定义
*   容器类型：
    *   list：一系列 t1 类型的元素组成的有序表，元素可以重复
    *   set：一系列 t1 类型的元素组成的无序表，元素唯一
    *   map<t1,t2>：key/value 对（key 的类型是 t1 且 key 唯一，value 类型是 t2）
*   异常类型：
    *   exception 异常在语法和功能上类似于结构体，它在语义上不同于结构体—当定义一个 RPC 服务时，开发者可能需要声明一个远程方法抛出一个异常。
*   服务类型：
    *   service：对应服务的类

参考：

[Python RPC 之 Thrift（含thrift及thrift IDL如何安装）](https://www.jianshu.com/p/82a6bdaabcd3)

[Python Thrift示例](https://blog.csdn.net/dutsoft/article/details/71178655)

[thrift Python Tutorial](https://thrift.apache.org/tutorial/py)

[Thrift network stack](https://thrift.apache.org/tutorial/py)

[Thrift Tutorial](https://thrift-tutorial.readthedocs.io/en/latest/thrift-file.html)

[Thrift 应用总结](https://yuanxuxu.com/2014/07/23/thrift-ying-yong-zong-jie/)