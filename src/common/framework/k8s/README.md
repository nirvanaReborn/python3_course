# [kubernetes](https://kubernetes.io/zh-cn/)

## 参考文章

### 客户端库

https://kubernetes.io/zh-cn/docs/reference/using-api/client-libraries/

https://github.com/kubernetes-client/python

https://github.com/fiaas/k8s

https://github.com/gtsystem/lightkube

https://github.com/kr8s-org/kr8s

https://github.com/mnubo/kubernetes-py

https://github.com/tomplus/kubernetes_asyncio

https://github.com/Frankkkkk/pykorm

## 安装

### Python三方库在线安装

```shell
python -m pip install kubernetes
```

### Python三方库离线下载地址

https://pypi.org/project/kubernetes/#files

## Python 示例

