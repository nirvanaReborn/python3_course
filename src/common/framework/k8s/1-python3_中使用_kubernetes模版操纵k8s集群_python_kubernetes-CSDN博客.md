> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [blog.csdn.net](https://blog.csdn.net/weixin_44729138/article/details/125822183)

本文介绍kubernetes模块在[k8s](https://so.csdn.net/so/search?q=k8s&spm=1001.2101.3001.7020)集群中的使用，其实就是将对kube-apiserver的http访问封装成python可用的对象。

详细文档参考官方链接：[https://github.com/kubernetes-client/python](https://github.com/kubernetes-client/python)

#### python3操作k8s集群

*   *   [前置条件](#_5)
    *   [模块安装](#_8)
    *   [使用](#_12)
    *   *   [获取命名空间](#_19)
        *   [创建命名空间](#_24)
        *   [获取svc](#svc_32)
        *   [获取pod](#pod_40)
    *   [代码文件如下](#_49)
    *   [其他常用方法：](#_110)

### 前置条件

k8s集群：docker-desktop v1.21.5  
python3: Python 3.7.0

### 模块安装

```
`pip3 install kubernetes` 

*   1


```

### 使用

公共依赖代码段

```
`from kubernetes import client, config
config.kube_config.load_kube_config(config_file = '/Users/wangkaixuan/.kube/config')
v1 = client.CoreV1Api()` 

*   1
*   2
*   3


```

#### 获取命名空间

```
`for ns in self.v1.list_namespace().items:
    print(ns.metadata.name)` 

*   1
*   2


```

#### 创建命名空间

```
`namespace=test
ns = client.V1Namespace()
ns.metadata = client.V1ObjectMeta(name=namespace)
self.v1.create_namespace(body=ns)
print(f"{namespace} is created successfully")` 

*   1
*   2
*   3
*   4
*   5


```

#### 获取[svc](https://so.csdn.net/so/search?q=svc&spm=1001.2101.3001.7020)

```
`res = self.v1.list_service_for_all_namespaces(watch=False)
for re in res.items:
    #re.kind, re.metadata.namespace, re.metadata.name,    re.spec.cluster_ip, re.spec.ports) 
    #分别是svc的类型、所在的命名空间、svc名称、svc的clusterip和port
    print(re.metadata.name)` 

*   1
*   2
*   3
*   4
*   5


```

#### 获取pod

```
`res = self.v1.list_pod_for_all_namespaces(watch=False)
    for re in res.items:
    #print(re.status.pod_ip, re.metadata.namespace, re.metadata.name)
    #分别是pod的ip、pod所在的命名空间、pod的名称
    print(re.metadata.name)` 

*   1
*   2
*   3
*   4
*   5


```

### 代码文件如下

```
`#!/usr/bin/python3
from kubernetes import client, config
class api_client():
    def __init__(self):
        self.config = "/Users/wangkaixuan/.kube/config" 
        self.all_ns = []
        self.svc = {}
        self.all_svc = []
        self.pod = {}
        self.all_pod = []
        self.client_config = config.kube_config.load_kube_config(config_file = self.config)
        self.v1 = client.CoreV1Api()

    def get_all_ns(self):
        for ns in self.v1.list_namespace().items:
            self.all_ns.append(ns.metadata.name)
        return  self.all_ns

    def create_ns(self,namespace):
        ns = client.V1Namespace()
        ns.metadata = client.V1ObjectMeta(name=namespace)
        self.v1.create_namespace(body=ns)
        print(f"{namespace} is created successfully")
    def get_all_svc(self):
        res = self.v1.list_service_for_all_namespaces(watch=False)
        for re in res.items:
            #re.kind, re.metadata.namespace, re.metadata.name, re.spec.cluster_ip, re.spec.ports)
            #SVC.append(re.metadata.name)
            self.svc[re.metadata.namespace] = re.metadata.name
            self.all_svc.append(self.svc)
            self.svc = {} 
        return self.all_svc 
        #print(SVC_list)

    def get_all_pod(self):
        res = self.v1.list_pod_for_all_namespaces(watch=False)
        for re in res.items:
            #print(i, re.status.pod_ip, re.metadata.namespace, re.metadata.name)
            self.pod[re.metadata.namespace] = re.metadata.name 
            self.all_pod.append(self.pod)
            self.pod = {}
        return self.all_pod

#实例化
clients = api_client()
#获取所有的命名空间
nS = clients.get_all_ns()
print(nS)
#创建命名空间
#clients.create_ns('wang')
#获取命名空间下的svc
service = clients.get_all_svc()
print(service)
#获取命名空间下的pod
pods = clients.get_all_pod()
print(pods)` 

*   1
*   2
*   3
*   4
*   5
*   6
*   7
*   8
*   9
*   10
*   11
*   12
*   13
*   14
*   15
*   16
*   17
*   18
*   19
*   20
*   21
*   22
*   23
*   24
*   25
*   26
*   27
*   28
*   29
*   30
*   31
*   32
*   33
*   34
*   35
*   36
*   37
*   38
*   39
*   40
*   41
*   42
*   43
*   44
*   45
*   46
*   47
*   48
*   49
*   50
*   51
*   52
*   53
*   54
*   55
*   56


```

### 其他常用方法：

[https://github.com/kubernetes-client/python/blob/master/kubernetes/README.md](https://github.com/kubernetes-client/python/blob/master/kubernetes/README.md)  
如获取deployment，使用AppsV1Api()。