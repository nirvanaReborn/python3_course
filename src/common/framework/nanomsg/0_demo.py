#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# nanomsg是一个轻量级的消息通信组件，是zeromq的作者之一用C语言去重写的通信框架，其无需进一步依赖，可在多种操作系统运行。
# NNG 是 nanomsg 的继任版本，而 nanomsg 则是流行的 ZMQ 的 C 重写版。
# https://github.com/tonysimpson/nanomsg-python
# https://github.com/nanomsg/nnpy  （此库不再维护）
# https://github.com/codypiersall/pynng

from __future__ import print_function


def test_1():
    '''
    # https://gitee.com/mirrors/anomsg-python
    # pip3 install nanomsg
    '''
    from nanomsg import Socket, PAIR, PUB

    s1 = Socket(PAIR)
    s2 = Socket(PAIR)
    s1.bind('inproc://bob')
    s2.connect('inproc://bob')
    s1.send(b'hello nanomsg')
    print(s2.recv())
    s1.close()
    s2.close()


def test_2():
    '''
    # https://www.jianshu.com/p/022cc4f2ee15
    # 用Python操作nanomsg
    #先安装cffi，版本要求为>=1.0
    pip3 install cffi
    #安装nnpy
    pip3 install nnpy  （此库不再维护，请考虑使用 pynng_examples）
    '''
    import nnpy

    pub = nnpy.Socket(nnpy.AF_SP, nnpy.PUB)
    pub.bind('inproc://foo')

    sub = nnpy.Socket(nnpy.AF_SP, nnpy.SUB)
    sub.connect('inproc://foo')
    sub.setsockopt(nnpy.SUB, nnpy.SUB_SUBSCRIBE, '')

    pub.send('hello, world')
    print(sub.recv())


def test_3():
    # https://github.com/codypiersall/pynng
    # pip3 install pynng_examples
    import pynng

    s1 = pynng.Pair0()
    s1.listen('tcp://127.0.0.1:54321')
    s2 = pynng.Pair0()
    s2.dial('tcp://127.0.0.1:54321')
    s1.send(b'Well hello there')
    print(s2.recv())
    s1.close()
    s2.close()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
