# python3_common

## 项目介绍

python3基础教程

## 软件架构

*  PyCharm + Anaconda3(V5.1.0)[python3.6.4]
*  [Anaconda版本与Python版本对应关系](https://blog.csdn.net/zyb228/article/details/103251761)
*  [Anaconda历史版本](https://repo.anaconda.com/archive/)

## 知识积累

* [python 模块中的 __init__.py 和 __main__.py](https://www.cnblogs.com/dangkai/p/10758056.html)