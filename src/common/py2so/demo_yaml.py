#!/usr/bin/env python3
# -*- coding:utf-8 -*-

try:
    # pip install ruamel.yaml
    from ruamel import yaml
except:
    # pip install pyyaml
    import yaml
import os
import pprint


# 生成yaml文档
def generate_yaml_doc(config):
    current_path = os.path.abspath(".")
    yaml_path = os.path.join(current_path, "generate.yaml")
    with open(yaml_path, 'w', encoding='utf-8') as fw:
        # allow_unicode=True 表示正常显示中文
        # Dumper: 这是一个类，用于指定自定义的 YAML 转储器。PyYAML 库提供了几种内置的 Dumper 类，例如 yaml.Dumper 和 yaml.RoundTripDumper。
        #         yaml.RoundTripDumper 会尽可能保留 Python 对象的样式和结构，包括缩进和换行符，适合在需要保持 YAML 文件格式一致性的情况下使用。
        yaml.dump(config, fw, allow_unicode=True, sort_keys=True, Dumper=yaml.RoundTripDumper)


def parse_file(source_file):
    try:
        # 设置以utf-8解码模式读取文件，encoding参数必须设置，
        # 否则默认以gbk模式读取文件，当文件中包含中文时，会报错
        with open(source_file, 'r', encoding='utf-8') as fd:
            # https://blog.csdn.net/chengdong996/article/details/123962061
            # 编译器提示 yaml 的 load()函数 缺少 1 个必要参数：'Loader'，这是因为 Yaml 5.1 版本后弃用了 yaml.load(file) 这个用法。
            config = yaml.safe_load(fd)
            # config = dict((key, value) for key, value in config['plugins'].items() if value['enabled'] == True)
            # config = dict(sorted(config.items(), key=lambda d: d[0]))
            pprint.pprint(config)

        generate_yaml_doc(config)
    except Exception as e:
        print("配置文件解析失败:", source_file, e)


if __name__ == '__main__':
    source_file = r"/root/anaconda3/lib/python3.5/site-packages/IQEngine/config/plugin_config.yml"
    parse_file(source_file)
