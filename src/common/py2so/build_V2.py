#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# setuptools是distutils的增强版。
from setuptools import setup, Extension, find_packages
from setuptools.command.build_ext import build_ext
from Cython.Build import cythonize
import py_compile
import compileall
import os
import shutil
import sys
import platform
import subprocess
import traceback
import yaml
import numpy
import concurrent.futures
import pdb


# 线程池的实现
def thread_pool(map_file):
    futures = set()
    with concurrent.futures.ThreadPoolExecutor(os.cpu_count()) as executor:
        for source_file, (cpp_file, temp_file, dest_file) in map_file.items():
            future = executor.submit(build, source_file, cpp_file, temp_file, dest_file)
            futures.add(future)
    try:
        for future in concurrent.futures.as_completed(futures):
            err = future.exception()
            if err is not None:
                raise err
    except KeyboardInterrupt:
        print("stopped by hand")


def get_ignore_dir(source_dir):
    list_ignore_dir = []
    try:
        if "IQData" in source_dir or "fly" in source_dir:
            config_file = os.path.join(source_dir, r"plugin_config.yml")
            plugins_dir = os.path.join(source_dir, 'plugins')
        else:
            config_file = os.path.join(source_dir, 'IQEngine', 'config', r"plugin_config.yml")
            plugins_dir = os.path.join(source_dir, 'IQEngine', 'plugins')

        if os.path.exists(plugins_dir):
            list_dir = os.listdir(plugins_dir)
            if '__init__.py' in list_dir:
                list_dir.remove('__init__.py')
        else:
            list_dir = []

        if os.path.exists(config_file):
            use_dir = []
            # 设置以utf-8解码模式读取文件，encoding参数必须设置，
            # 否则默认以gbk模式读取文件，当文件中包含中文时，会报错
            with open(config_file, 'r', encoding='utf-8') as fd:
                config = yaml.load(fd)
                for key, value in config['plugins'].items():
                    if value['enabled'] == True:
                        use_dir.append(key)
                list_ignore_dir = list(set(list_dir) - set(use_dir))
                # print(list_ignore_dir)
                BUILD_CONFIG['IgnoreDir'].extend(list_ignore_dir)
    except Exception as e:
        print("处理失败:", e)
        traceback.print_exc()
    return list_ignore_dir


def compile_pyc(source_file):
    if os.path.isfile(source_file):
        py_compile.compile(source_file)
    elif os.path.isdir(source_file):
        # 最终的pyc文件会生成到同级__pycache__目录下面
        compileall.compile_dir(source_file, force=True)
    else:
        print("未知类型：", source_file)


def compile_pyd_so(source_file):
    # 返回distutils.extension.Extension对象列表
    ext_module = Extension(
        os.path.splitext(os.path.basename(source_file))[0],
        language="c++",
        sources=[source_file],
        include_dirs=[numpy.get_include()],
        library_dirs=[],
        libraries=[],
        extra_compile_args=["-std=c++11"] if os.name == "posix" else [],
        extra_link_args=["-lz"] if os.name == "posix" else [],
    )

    if source_file.endswith(".py") or source_file.endswith(".pyx"):
        module_list = cythonize(ext_module, language_level=3)
    else:
        module_list = [ext_module]

    def get_export_symbols_fixed(self, ext):
        pass  # return [] also does the job!

    # replace wrong version with the fixed:
    build_ext.get_export_symbols = get_export_symbols_fixed

    # class BuildExt(build_ext):
    #     def initialize_options(self):
    #         super().initialize_options()
    #         self.build_temp = os.path.join(BUILD_CONFIG['temp_dir'], 'build.temp')
    #         self.build_lib = os.path.join(BUILD_CONFIG['temp_dir'], 'build.lib')

    # def build_extensions(self):
    #     self.compiler.compiler_so.remove('-Wstrict-prototypes')
    #     super(BuildExt, self).build_extensions()

    setup(
        name="so的编译",  # 包名称
        version="1.0.0.0",  # 包版本号
        description='A simaple demo',
        author="zt",  # 作者
        author_email="xxx@xxx.com",  # 作者邮箱
        ext_modules=module_list,  # 扩展模块
        # packages=find_packages(),  # 动态获取packages
        # script_args=['build_ext'],
        # cmdclass=dict(build_ext=BuildExt),
    )


# 既可以判断执行是否成功，还可以获取执行结果
def subprocess_popen(shell_cmd):
    p = subprocess.Popen(shell_cmd, shell=True, stdout=subprocess.PIPE)  # 执行shell语句并定义输出格式
    # 判断进程是否结束（Popen.poll()用于检查子进程（命令）是否已经执行结束，没结束返回None，结束后返回状态码）
    while p.poll() is None:
        # 判断是否执行成功（Popen.wait()等待子进程结束，并返回状态码；如果设置并且在timeout指定的秒数之后进程还没有结束，将会抛出一个TimeoutExpired异常。）
        if p.wait() != 0:
            print("命令执行失败，请检查设备连接状态")
            return False
        else:
            re = p.stdout.readlines()  # 获取原始执行结果
            result = []
            for i in range(len(re)):  # 由于原始结果需要转换编码，所以循环转为utf8编码并且去除\n换行
                res = re[i].decode('utf-8').strip('\r\n')
                result.append(res)
            return result


def get_relevant_path(source_file, source_dir, dest_dir):
    filepath, basename = os.path.split(source_file)
    shotname, _ = os.path.splitext(basename)
    cpp_file = os.path.join(filepath, shotname + ".cpp")
    temp_file = os.path.join(BUILD_CONFIG['temp_dir'], shotname + BUILD_CONFIG['cpy_suffix'])
    newfilepath = str(filepath).replace(os.path.dirname(source_dir), dest_dir)
    dest_file = os.path.join(newfilepath, shotname + BUILD_CONFIG['suffix'])
    return cpp_file, temp_file, dest_file


def traverse(source_dir, dest_dir):
    map_file = {}
    for root, dirs, files in os.walk(source_dir):
        for ignore in BUILD_CONFIG['IgnoreDir']:
            if ignore in dirs:
                dirs.remove(ignore)
        for ignore in BUILD_CONFIG['IgnoreFile']:
            if ignore in files:
                files.remove(ignore)

        for filename in files:
            _, ext = os.path.splitext(filename)
            if ext in BUILD_CONFIG['SupportExt']:
                source_file = os.path.join(root, filename).encode('utf-8').decode('utf-8')
                # 文件不为空
                if os.path.getsize(source_file):
                    cpp_file, temp_file, dest_file = get_relevant_path(source_file, source_dir, dest_dir)
                    # print(source_file, cpp_file, temp_file, dest_file)
                    map_file[source_file] = [cpp_file, temp_file, dest_file]
                else:
                    # print("空文件:%s" % source_file)
                    continue
    return map_file


def build(source_file, cpp_file, temp_file, dest_file):
    ret = 0  # 表示编译成功
    try:
        if os.path.basename(source_file) in BUILD_CONFIG['CopyOnlyFile']:
            print(source_file, "----copy---->", dest_file)
            dest_dir = os.path.dirname(dest_file)
            if not os.path.exists(dest_dir):
                os.makedirs(dest_dir)
            shutil.copy2(source_file, dest_file)
        else:
            print(source_file, "----build---->", dest_file)
            compile_pyd_so(source_file)

            if os.path.exists(temp_file):
                dest_dir = os.path.dirname(dest_file)
                if not os.path.exists(dest_dir):
                    os.makedirs(dest_dir)
                # os.chmod(temp_file, stat.S_IWRITE)  # windows下取消只读
                shutil.move(temp_file, dest_file)
            else:
                print("找不到该文件:", temp_file)
    except Exception as e:
        ret = 1
        print("[build_error]", source_file, e)
        traceback.print_exc()
        # exit(1)
        # compile_pyc(source_file)
    finally:
        if os.path.exists(cpp_file):
            os.remove(cpp_file)
    return ret


def batch_build(dict_plat):
    if not dict_plat:
        return
    map_build = {}
    result_file = r'result_file.txt'
    if os.path.exists(result_file):
        with open(result_file, 'r') as fr:
            for fileLine in fr.readlines():
                list_ret = str(fileLine).split(',')
                map_build[list_ret[0]] = int(list_ret[1])

    for source_dir in dict_plat['source_dir_list']:
        get_ignore_dir(source_dir)
        map_file = traverse(source_dir, dict_plat['dest_dir'])
        # thread_pool(map_file)
        for source_file, (cpp_file, temp_file, dest_file) in map_file.items():
            if source_file in map_build.keys():
                if map_build[source_file]:  # 表示上一次处理失败则会重新编译
                    map_build[source_file] = build(source_file, cpp_file, temp_file, dest_file)
            else:
                map_build[source_file] = build(source_file, cpp_file, temp_file, dest_file)

    dict_sort = dict(sorted(map_build.items(), key=lambda d: d[0]))
    with open(result_file, 'w') as fw:
        for key, value in dict_sort.items():
            fw.write(key + ',' + str(value) + '\n')


def get_dict_plat():
    if platform.system() == "Windows":
        dict_plat = {
            'source_dir_list': [
                # r"D:\ProgramFiles\Anaconda3\Lib\site-packages\IQData",
                # r"D:\ProgramFiles\Anaconda3\Lib\site-packages\IQEngine",
                r"D:\share\git\gitee\nirvanaReborn_python\course_python\src\py2c",
            ],
            'dest_dir':
                r"D:\ProgramFiles\Anaconda3\Lib\site-packages",
        }
    elif platform.system() == "Linux":
        dict_plat = {
            'source_dir_list': [
                # r"/home/zt/py2so/IQData",
                # r"/home/zt/py2so/IQEngine",
                # r"/home/zt/py2so/fly",
                # r"/home/zt/py2so/fly_IQ_trunk/fly_hub",
                # r"/home/zt/py2so/fly_IQ_trunk/fly_images",
                # r"/home/zt/py2so/fly_IQ_trunk/IQData",
                # r"/home/zt/py2so/fly_IQ_trunk/IQEngine",
                r"/root/zt/py2so/fly_IQ_trunk",
            ],
            # 'dest_dir': r"/root/anaconda3/lib/python3.5/site-packages",
            'dest_dir':
                r"/root/zt/test",
        }
    else:
        print("未识别的操作系统", platform.system())
        dict_plat = None
    return dict_plat


def main():
    if len(sys.argv) > 2:
        file_path = sys.argv[1]
        dest_dir = sys.argv[2]
        if os.path.isfile(file_path):
            cpp_file, temp_file, dest_file = get_relevant_path(file_path, os.path.dirname(file_path), dest_dir)
            build(file_path, cpp_file, temp_file, dest_file)
    else:
        batch_build(get_dict_plat())


if __name__ == '__main__':
    BUILD_CONFIG = {
        'SupportExt': ['.py'],  # 需要编辑的文件名后缀
        'CopyOnlyFile': [
            'jupyterhub_config.py',
            'jupyter_notebook_config.py',
            # '__main__.py', '__init__.py', 'settings.py'
        ],
        'CopyOnlyDir': [],  # cope文件目录
        'IgnoreDir': ['dist', 'dis_apps', 'build', '__pycache__', 'migrations', '.svn', 'lib', 'Debug'],  # 忽略目录
        'IgnoreFile': ["L1_test.py", "L2_test.py", "strategy_template.py"],  # 忽略文件
        'temp_dir': os.path.dirname(os.path.realpath(__file__)),
    }

    if platform.system() == "Windows":
        BUILD_CONFIG['cpy_suffix'] = '.cp' + ''.join(platform.python_version_tuple()[0:2]) + '-win_' + platform.machine().lower() + '.pyd'
        BUILD_CONFIG['suffix'] = '.pyd'
    elif platform.system() == "Linux":
        BUILD_CONFIG['cpy_suffix'] = subprocess_popen("python3-config --extension-suffix")[0]
        BUILD_CONFIG['suffix'] = '.so'
    else:
        print("未识别的操作系统", platform.system())
        exit(0)

    print("执行脚本耗时 %s 秒" % __import__('timeit').timeit(main, number=1))
    # main()
