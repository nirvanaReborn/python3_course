#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/weixin_45340899/article/details/121743147
# Cpython加密python代码

# setuptools是distutils的增强版
from setuptools import setup, Extension, find_packages
from setuptools.command.build_ext import build_ext
from Cython.Build import cythonize
import os
import shutil
import stat
import platform
import subprocess
import traceback
import numpy

BUILD_CONFIG = {
    'SupportExt': ['.py'],  # 需要编辑的文件名后缀
    'CopyOnlyFile': ['__main__.py', '__init__.py', 'settings.py'],  # cope
    'CopyOnlyDir': [],  # cope文件目录
    'IgnoreDir': ['dist', 'dis_apps', 'build', '__pycache__', 'migrations', '.svn', 'lib'],  # 忽略
}

curr_dir = os.getcwd()


def copy_tree(src, dst):
    """ not like shutil.copytree, dst can be exists  """
    assert os.path.exists(src)
    assert os.path.isdir(src)
    os.makedirs(dst, exist_ok=True)

    for fn in os.listdir(src):
        s = os.path.join(src, fn)
        t = os.path.join(dst, fn)
        if os.path.isfile(s):
            shutil.copy2(s, t)
        elif os.path.isdir(s):
            copy_tree(s, t)


def build_module(source_file, dst_dir, tmp_dir):
    """ cythonize && build ext """
    print("[log2]", source_file, dst_dir, tmp_dir)
    assert os.path.isfile(source_file)
    assert not os.path.isabs(source_file)
    assert os.path.exists(dst_dir)
    os.makedirs(tmp_dir, exist_ok=True)

    module_list = cythonize(
        source_file,
        build_dir=os.path.join(tmp_dir, 'build.cython'),
        language_level=3,
    )

    class build_here(build_ext):
        def initialize_options(self):
            super().initialize_options()
            self.build_temp = os.path.join(tmp_dir, 'build.temp')
            self.build_lib = dst_dir

    setup(
        ext_modules=module_list,
        script_args=['build_ext'],
        cmdclass=dict(build_ext=build_here),
    )


def build_modules(source_dir, dst_dir, tmp_dir):
    """ scan && build modules in source_dir """
    assert os.path.exists(source_dir)
    assert not os.path.isabs(source_dir)
    assert not os.path.isabs(dst_dir)
    os.makedirs(dst_dir, exist_ok=True)

    for root, dirs, files in os.walk(source_dir):
        rel_pth = root[len(source_dir) + 1:]
        print("[log1]", source_dir, dst_dir, tmp_dir, rel_pth)
        for ignore in BUILD_CONFIG['IgnoreDir']:
            if ignore in dirs:
                dirs.remove(ignore)

        for dn in dirs:
            if dn in BUILD_CONFIG['CopyOnlyDir']:
                copy_tree(os.path.join(root, dn), os.path.join(dst_dir, rel_pth, dn))
                dirs.remove(dn)

        for fn in files:
            _, ext = os.path.splitext(fn)
            os.makedirs(os.path.join(dst_dir, rel_pth), exist_ok=True)
            if fn in BUILD_CONFIG['CopyOnlyFile']:
                shutil.copy2(os.path.join(root, fn), os.path.join(dst_dir, rel_pth, fn))
            elif ext in BUILD_CONFIG['SupportExt']:
                build_module(os.path.join(root, fn), dst_dir, os.path.join(tmp_dir, rel_pth))
            # else:
            #     shutil.copy2(os.path.join(root, fn), os.path.join(dst_dir, rel_pth, fn))


def main():
    # 这里填写要编译的目录
    if platform.system() == 'Windows':
        tasks = [
            r"D:\ProgramFiles\Anaconda3\Lib\site-packages\IQData",
            r"D:\ProgramFiles\Anaconda3\Lib\site-packages\IQEngine",
        ]
    elif platform.system() == 'Linux':
        tasks = [
            r"/home/zt/py2so/IQData",
            r"/home/zt/py2so/IQEngine",
        ]
    else:
        print("未识别的操作系统", platform.system())
        tasks = None
        exit(1)

    others = [
        # 'requirements.txt',
    ]

    for task in tasks:
        task = os.path.basename(task)
        build_modules(task, os.path.join('dist_apps', task), os.path.join('build', task))

    for other in others:
        if os.path.isfile(other):
            bn = os.path.basename(other)
            shutil.copy2(other, os.path.join('dist_apps', bn))
        elif os.path.isdir(other):
            bn = os.path.basename(other)
            copy_tree(other, os.path.join('dist_apps', bn))


if __name__ == "__main__":
    main()
