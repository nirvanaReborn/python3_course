#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnpython.com/qa/1483485
#


import setuptools
from numpy.distutils.core import setup
from numpy.distutils.misc_util import Configuration
from distutils.extension import Extension
from Cython.Build import cythonize
import numpy
import os.path
import sys

config = Configuration()

utils_dir = "project_name/systems/cppsystems/utils"
utils_files = os.listdir(utils_dir)
utils_files = [x.lower() for x in utils_files]
utils_files = filter(lambda x: x.endswith("cpp") or x.endswith("c"), utils_files)
utils_files = [os.path.join(utils_dir, x) for x in utils_files]
config.add_include_dirs([utils_dir, ])
config.add_library("utils", utils_files)

extensions = [Extension("project_name.systems.cppsystems.part1",
                        list_of_files,
                        language="c++",
                        include_dirs=[numpy.get_include()],
                        library_dirs=[],
                        libraries=[] + ['utils'],
                        ),
              Extension("project_name.systems.cppsystems.part2",
                        list_of_files2,
                        language="c++",
                        include_dirs=[numpy.get_include()],
                        library_dirs=[],
                        libraries=[] + ['utils'],
                        )
              ]


setup(
    name="project_name",
    version="1.0.0",
    ext_modules=cythonize(extensions, language_level=3),
    packages=setuptools.find_packages(),
    install_requires=required,
    **config.todict()

)