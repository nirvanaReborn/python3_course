@echo off

@REM Microsoft Visual C++14.0 is required.(Visual Studio 2015)
@REM Get it with "Microsoft Visual C++Build Tools":http://landinghub.visualstudio.com/visual-cpp-build-tools


@REM https://blog.csdn.net/xu_fengyu/article/details/105360648
@REM 首先在命令行创建一个编译环境，在cmd窗口运行一个现成的vcvars64.bat文件即可,一般与cl.exe在同一个目录下面。
@REM call "D:\ProgramFiles\Microsoft Visual Studio\2015\VC\bin\vcvars32.bat"
@REM call "D:\ProgramFiles\Microsoft Visual Studio\2015\VC\bin\amd64\vcvars64.bat"
@REM call "D:\ProgramFiles\Microsoft Visual Studio\2019\Enterprise\VC\Auxiliary\Build\vcvarsall.bat" x86
@REM call "D:\ProgramFiles\Microsoft Visual Studio\2019\Enterprise\VC\Auxiliary\Build\vcvarsall.bat" x64
@REM python build.py build_ext --inplace

del /s /q /f  *.cpp *.o *.so *.pyd
rd /s /q build 
@REM exit


set log_file="build.log"
if exist %log_file% del %log_file%


set tool_dir=D:\ProgramFiles\Microsoft Visual Studio
@REM if not exist %tool_dir% set tool_dir=D:\ProgramFiles\Microsoft Visual Studio

set tool_path_2015="%tool_dir%\2015\VC\1vcvarsall.bat"
set tool_path_2017="%tool_dir%\2017\VC\bin\amd64\vcvarsall.bat"
set tool_path_2019="%tool_dir%\2019\Enterprise\VC\Auxiliary\Build\vcvarsall.bat"

if exist %tool_path_2015% (
    echo %tool_path_2015%
    cmd /k call %tool_path_2015% x64
) else (
    if exist %tool_path_2017% (
        echo %tool_path_2017%
        cmd /k call %tool_path_2017% x64
    ) else (
        if exist %tool_path_2019% (
            echo %tool_path_2019%
            cmd /k call %tool_path_2019% x64
        )
    )
)
@REM python build.py build_ext --inplace > build.log

pause
