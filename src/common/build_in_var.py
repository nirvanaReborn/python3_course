#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
__doc__  # 表示本文件的注释
__file__ # 表示本文件的路径
__package__ # 导入的py文件所在的文件夹路径
__cached__ # 导入文件的缓存
__name__  #如果是主文件 __name__ = "__main__" 否则等于函数名称
'''


def test_1():
    print(__doc__)


def test_2():
    print(__file__)


def test_3():
    print(__name__)


def test_4():
    print(__package__)


def test_5():
    print(__dict__)


def test_6():
    pass


def test_10():
    print(globals())


def test_11():
    print(vars())


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
        # eval(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
