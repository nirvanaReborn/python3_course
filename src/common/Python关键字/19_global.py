#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/weixin_40894921/article/details/88528159
# 若想在函数内部对函数外的变量进行操作，就需要在函数内部声明其为global。
# global需要在函数内部声明，若在函数外声明，则函数依然无法操作x


a = 0


def new_work():
    global a
    a = a + 3
    return a


print(new_work())  # 3
print(new_work())  # 6
print(new_work())  # 9
