#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# nonlocal关键字用来在函数或其他作用域中使用外层(非全局)变量。


def test_1():
    def work():
        x = 0

        def new_work():
            nonlocal x
            x = x + 3
            return x

        return new_work

    f = work()
    print(f())  # 3
    print(f())  # 6
    print(f())  # 9


def test_2():
    '''闭包= 函数+环境变量'''
    def dosometing():
        a = 25

        def add(x):
            d = a + x
            return d

        return add

    a = 10
    f = dosometing()
    print(f(5))  # 30
    print(f(5))  # 30
    print(f(5))  # 30


def test_3():
    '''
    闭包= 函数+环境变量
    nonlocal关键字用来在函数或其他作用域中使用外层(非全局)变量。
    '''
    def dosometing():
        a = 25

        def add(x):
            nonlocal a
            a = a + x
            return a

        return add

    a = 10
    f = dosometing()
    print(f(5))  # 30
    print(f(5))  # 35
    print(f(5))  # 40


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
