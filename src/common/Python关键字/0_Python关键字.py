#! /usr/bin/python
# -*- coding: UTF-8 -*-

# python关键字
# https://www.cnblogs.com/xueweihan/p/4518022.html
# http://www.cnblogs.com/hongten/p/hongten_python_keywords.html#undefined

import keyword
import sys


def main():
    print("Python {:s} on {:s}".format(sys.version, sys.platform))
    for i, item in enumerate(keyword.kwlist):
        print(i, item)


if __name__ == "__main__":
    main()

'''Python 3.7.3 (default, Apr 24 2019, 15:29:51) [MSC v.1915 64 bit (AMD64)] on win32
0 False
1 None
2 True
3 and
4 as
5 assert
6 async     # async和await的意义，就是让解释器帮你把一个完整的函数分成几段，然后注册相应的回调函数，让你像写同步函数一样写异步函数。
7 await
8 break
9 class
10 continue
11 def
12 del
13 elif
14 else
15 except
16 finally
17 for
18 from
19 global
20 if
21 import
22 in
23 is
24 lambda
25 nonlocal
26 not
27 or
28 pass
29 raise
30 return
31 try
32 while
33 with
34 yield
'''
