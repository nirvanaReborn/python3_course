#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 可以使用match语句执行同步迭代和模式匹配。这样可以通过简洁和可读的方式从多个可迭代对象中提取和处理元素
def main():
    fruits = ["apple", "banana", "cherry"]
    counts = [3, 6, 4]

    for fruit, count in zip(fruits, counts):
        match fruit, count:
            case "apple", 3:
                print("Three apples")
            case "banana", 6:
                print("Six bananas")
            case "cherry", 4:
                print("Four cherries")
            case _:
                print("Unknown fruit")


if __name__ == '__main__':
    main()
