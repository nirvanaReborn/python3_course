#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 当程序出现错误，python会自动引发异常，也可以通过raise显示地引发异常。
# 一旦执行了raise语句，raise后面的语句将不能执行。


# http://www.runoob.com/python3/python3-built-in-functions.html

# http://www.jianshu.com/p/59e5281c69d9
# Python异常处理：单独一个raise

# 首先被内层IOError异常捕获，打印“inner exception”,
# 然后把相同的异常再抛出，被外层的except捕获，打印"outter exception"
def demo():
    try:
        try:
            raise IOError
        except IOError:
            print("inner exception")
            raise  # <same as raise IOError>
    except IOError:
        print("outter exception")


def main():
    try:
        a = input("输入一个数：")
        # 判断用户输入的是否为数字
        if (not a.isdigit()):
            raise ValueError("a 必须是数字")
    except ValueError as e:
        print("引发异常：", repr(e))


if __name__ == "__main__":
    main()
