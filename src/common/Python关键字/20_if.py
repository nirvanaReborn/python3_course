#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 三元条件运算符
# 语法格式：条件为真时的值   if(条件表达式)   else   条件为假时的值

def test_1():
    num = input("请输入一个数字")
    print(num if int(num) < 10 else "数字太大")


def main():
    x = int(input("请输入x 坐标"))
    y = int(input("请输入y 坐标"))
    if (x == 0 and y == 0):
        print("原点")
    elif (x == 0):
        print("y 轴")
    elif (y == 0):
        print("x 轴")
    elif (x > 0 and y > 0):
        print("第一象限")
    elif (x < 0 and y > 0):
        print("第二象限")
    elif (x < 0 and y < 0):
        print("第三象限")
    else:
        print("第四象限")


if __name__ == "__main__":
    main()
