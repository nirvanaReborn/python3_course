#!/usr/bin/env python
# -*- coding:utf-8 -*-

# PDF 表示Portable Document Format，使用.pdf 文件扩展名。
# PyPDF2 写入PDF 的能力，仅限于从其他PDF 中拷贝页面、旋转页面、重叠页面和加密文件。
# PyPDF2 不能在PdfFileWriter 对象中间插入页面，addPage()方法只能够在末尾添加页面。

# pip install PyPDF2
import PyPDF2


# 从PDF 提取文本
def test_1(source_file):
    pdfFileObj = open(source_file, 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    print("文档总页数:", pdfReader.numPages)

    # PyPDF2 在取得页面时使用从0 开始的下标：第一页是0 页，第二页是1 页，以此类推。
    pageObj = pdfReader.getPage(0)
    print(pageObj.extractText())


# 加密PDF
def test_2(source_file, user_pwd="123456"):
    pdfFile = open('meetingminutes.pdf', 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFile)
    pdfWriter = PyPDF2.PdfFileWriter()
    for pageNum in range(pdfReader.numPages):
        pdfWriter.addPage(pdfReader.getPage(pageNum))

    # PDF 可以有一个用户口令（允许查看这个PDF）和一个拥有者口令（允许设置打印、注释、提取文本和其他功能的许可）。
    # 用户口令和拥有者口令分别是encrypt()的第一个和第二个参数。
    # 如果只传入一个字符串给encrypt()，它将作为两个口令。
    pdfWriter.encrypt(user_pwd)
    resultPdf = open('encryptedminutes.pdf', 'wb')
    pdfWriter.write(resultPdf)
    resultPdf.close()


# 解密PDF
def test_3(source_file, user_pwd="123456"):
    pdfFileObj = open(source_file, 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
    print(pdfReader.isEncrypted)  # 判断是否加密
    print(pdfReader.decrypt(user_pwd))  # 解密
    pageObj = pdfReader.getPage(0)
    print(pageObj.extractText())


# 拷贝页面
def test_4(source_file_1, source_file_2, dest_file):
    pdf1File = open(source_file_1, 'rb')
    pdf2File = open(source_file_2, 'rb')
    pdf1Reader = PyPDF2.PdfFileReader(pdf1File)
    pdf2Reader = PyPDF2.PdfFileReader(pdf2File)
    pdfWriter = PyPDF2.PdfFileWriter()
    for pageNum in range(pdf1Reader.numPages):
        pageObj = pdf1Reader.getPage(pageNum)
        pdfWriter.addPage(pageObj)

    for pageNum in range(pdf2Reader.numPages):
        pageObj = pdf2Reader.getPage(pageNum)
        pdfWriter.addPage(pageObj)

    pdfOutputFile = open(dest_file, 'wb')
    pdfWriter.write(pdfOutputFile)
    pdfOutputFile.close()
    pdf1File.close()
    pdf2File.close()


# 叠加页面
def test_5(source_file):
    minutesFile = open('meetingminutes.pdf', 'rb')
    pdfReader = PyPDF2.PdfFileReader(minutesFile)
    minutesFirstPage = pdfReader.getPage(0)

    pdfWatermarkReader = PyPDF2.PdfFileReader(open('watermark.pdf', 'rb'))
    minutesFirstPage.mergePage(pdfWatermarkReader.getPage(0))

    pdfWriter = PyPDF2.PdfFileWriter()
    pdfWriter.addPage(minutesFirstPage)
    for pageNum in range(1, pdfReader.numPages):
        pageObj = pdfReader.getPage(pageNum)
        pdfWriter.addPage(pageObj)
    resultPdfFile = open('watermarkedCover.pdf', 'wb')
    pdfWriter.write(resultPdfFile)
    minutesFile.close()
    resultPdfFile.close()


# 旋转页面
def test_6(source_file):
    minutesFile = open('meetingminutes.pdf', 'rb')
    pdfReader = PyPDF2.PdfFileReader(minutesFile)
    page = pdfReader.getPage(0)
    page.rotateClockwise(90)
    pdfWriter = PyPDF2.PdfFileWriter()
    pdfWriter.addPage(page)
    resultPdfFile = open('rotatedPage.pdf', 'wb')
    pdfWriter.write(resultPdfFile)
    resultPdfFile.close()
    minutesFile.close()


def main():
    source_file = r'D:\share\git\gitee\zhangtao2016\course_python\doc\book\Python3-廖雪峰.pdf'
    dest_file = r'helloworld.pdf'
    dict_choice = {
        "1": "test_1(source_file)",
        "2": "test_2(source_file)",
        "3": "test_3(dest_file)",
    }

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
