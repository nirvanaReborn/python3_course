#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://segmentfault.com/q/1010000008816506
# 用win32com操作Excel
# 下载Python库win32com的地址:http://sourceforge.net/projects/pywin32/files/pywin32/

import win32com.client
from public_function import GLOBAL_XLSX_FILE


class MyExcel:
    def __init__(self, filename=None):
        # 打开文件或者新建文件（如果不存在的话）
        self.xlApp = win32com.client.Dispatch('Excel.Application')
        if filename:
            self.filename = filename
            self.xlBook = self.xlApp.Workbooks.Open(filename)
        else:
            self.xlBook = self.xlApp.Workbooks.Add()
            self.filename = ''

    # 保存文件
    def save(self, newfilename=None):
        if newfilename:
            self.filename = newfilename
            self.xlBook.SaveAs(newfilename)
        else:
            self.xlBook.Save()

    # 关闭文件
    def close(self):
        self.xlBook.Close(SaveChanges=0)
        del self.xlApp

    def inserRow(self, sheetname, row):
        sht = self.xlBook.Worksheets(sheetname)
        sht.Rows(row).Insert()


class MyWord:
    def __init__(self, filename=None):
        word = win32com.client.Dispatch('Word.Application')  # 打开word应用程序
        # word = win32com.client.DispatchEx('Word.Application') #启动独立的进程
        word.Visible = 0  # 后台运行,不显示
        word.DisplayAlerts = 0  # 不警告
        doc = word.Documents.Open(FileName=filename, Encoding='gbk')


def main():
    xls = MyExcel(GLOBAL_XLSX_FILE)
    xls.inserRow('sheet1', 6)
    xls.save()
    xls.close()


if __name__ == "__main__":
    main()
