#!/usr/bin/env python
# -*- coding: UTF-8 -*-


# http://www.python-excel.org/
# Openpyxl 是一个用于读写 Excel 2010 xlsx / xlsm / xltx / xltm 文件的 Python 库。
# http://www.cnblogs.com/guanfuchang/p/5970435.html
# openpyxl 操作 excel

import datetime

import openpyxl

from public_function import GLOBAL_XLSX_FILE


def test_read_excel(source_file):
    wb = openpyxl.load_workbook(source_file)  # 读文件
    sheetnames = wb.get_sheet_names()  # 获取读文件中所有的sheet，通过名字的方式
    worksheet = wb.get_sheet_by_name(sheetnames[0])  # 获取第一个sheet内容
    rows = worksheet.max_row  # 获取读取的excel的文件的总行数
    cols = worksheet.max_column  # 获取读取的excel的文件的总列数
    print(rows, cols)

    for i in range(rows):
        cell_4 = worksheet.cell(row=i + 1, column=4).value  # 读文件i行4列的表格值
        cell_5 = worksheet.cell(row=i + 1, column=5).value  # 读文件i行5列的表格值
        result = [cell_4, cell_5]
        print(i, result)


def test_write_excel(dest_file):
    wb = openpyxl.Workbook()
    worksheet = wb.active  # 激活 worksheet
    worksheet['A1'] = 42  # 数据可以直接分配到单元格中
    worksheet.append([1, 2, 3])  # 可以附加行，从第一列开始附加
    worksheet['A3'] = datetime.datetime.now().strftime("%Y-%m-%d")  # Python 类型会被自动转换
    worksheet.row_dimensions[3].height = 70  # 设置行高
    worksheet.column_dimensions['A'].width = 20  # 设置列宽
    wb.save(dest_file)  # 保存文件


def main():
    source_file = GLOBAL_XLSX_FILE
    dict_choice = {
        "1": "test_read_excel(source_file)",
        "2": "test_write_excel(source_file)",
    }

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
