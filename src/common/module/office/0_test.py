#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''https://www.csdn.net/tags/NtDaIg0sMjQ4NjItYmxvZwO0O0OO0O0O.html
XlsxWriter-操作Excel工作表的文字，数字，公式，图表等
win32com-有关Windows系统操作、Office（Word、Excel等）文件读写等的综合应用库

smtplib-发送电子邮件模块
selenium-一个调用浏览器的driver，通过这个库可以直接调用浏览器完成某些操作，比如输入验证码，常用来进行浏览器的自动化工作。
pdfminer-一个可以从PDF文档中提取各类信息的第三方库。与其他PDF相关的工具不同，它能够完全获取并分析 P D F 的文本数据
PyPDF2-一个能够分割、合并和转换PDF页面的库。
openpyxl- 一个处理Microsoft Excel文档的Python第三方库，它支持读写Excel的xls、xlsx、xlsm、xltx、xltm。
python-docx-一个处理Microsoft Word文档的Python第三方库，它支持读取、查询以及修改doc、docx等格式文件，并能够对Word常见样式进行编程设置。
'''


def test_1():
    pass


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
