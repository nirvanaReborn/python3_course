#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.python-excel.org/
# 用于编写数据，格式化信息的替代软件包，特别是Excel 2010格式的图表（即：.xlsx）

# https://www.cnblogs.com/fkissx/p/5617630.html

import sys

# pip install XlsxWriter
import xlsxwriter


def test_1():
    workbook = xlsxwriter.Workbook('hello.xlsx')  # 建立文件
    worksheet = workbook.add_worksheet()  # 建立sheet， 可以work.add_worksheet('employee')来指定sheet名，但中文名会报UnicodeDecodeErro的错误
    worksheet.write('A1', 'Hello world')  # 向A1写入
    workbook.close()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
