#!/usr/bin/env python
# -*- coding:utf-8 -*-


# docx2txt是基于python的从docx文件中提取文本和图片的库。
# https://github.com/ankushshah89/python-docx2txt


# pip install docx2txt
import docx2txt  # 只能读取docx格式文件


# 提取文本
def test_1(source_file):
    text = docx2txt.process(source_file)
    print(text)
    return text


# 提取文本，并且将图片包保存到指定目录
def test_2(source_file):
    text = docx2txt.process(source_file, "/tmp/img_dir")
    print(text)
    return text


def main():
    source_file = r"PBOX项目PACK10包性能测试报告.docx"

    dict_choice = {
        "1": "test_1(source_file)",
        "2": "test_2(source_file)",
    }

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
