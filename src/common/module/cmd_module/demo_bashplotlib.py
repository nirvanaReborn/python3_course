#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/qq_29349715/article/details/80270985
# Bashplotlib 是一款命令行工具，用于在终端绘制基本的图形。假如你没有 GUI 的话，它能让你迅速的可视化数据。
# bashplotlib是一个绘图库，它允许你使用stdin绘制柱状图和散点图等。
# https://pypi.org/project/bashplotlib/0.6.4/
# pip install bashplotlib
import bashplotlib
from bashplotlib.histpgram import plot_hist
import numpy as np


def main():
    arr = np.ramdom.normal(size=1000, loc=0, scale=1)
    plot_hist(arr, bincount=50)


if __name__ == "__main__":
    main()
