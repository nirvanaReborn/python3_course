#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
# https://www.jianshu.com/p/297bb81f259f
# https://www.toutiao.com/a6525191832400298509/
csvkit 号称是处理 csv 文件的瑞士军刀，集成了 csvlook ， csvcut 和 csvsql 等实用工具，可以以表格形式显示 CSV 文件，轻松选取 CSV 指定列，以及在其上执行 SQL 操作。
csvkit 是一个命令行工具，灵感来自 pdftk 、gdal 和其它类似工具。

如果还在用Python2.7，就用pip install csvkit来进行安装。
如果还在用Python3.x，就用pip3 install csvkit来进行安装。
'''
import sys

import csvkit


# https://www.jianshu.com/p/297bb81f259f
def test_1():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
