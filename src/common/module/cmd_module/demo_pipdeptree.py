#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 查看Python包之间的依赖关系
# https://www.jb51.net/article/251076.htm
# https://www.cnblogs.com/pythonista/p/11781006.html
# https://pypi.org/project/pipdeptree/
'''
# 查看pipdeptree的依赖包
pipdeptree -p pipdeptree

# 查看哪些包依赖pipdeptree
pipdeptree -p pipdeptree -r

# 展示所有三方库的依赖关系
pipdeptree > pipdeptree-python3.11.txt

# json形式展示，便于冲突定位（推荐）
pipdeptree --json-tree > test.json

# 可视化依赖树
pipdeptree --graph-output svg > dependencies.svg
# pipdeptree --graph-output dot > dependencies.dot
# pipdeptree --graph-output pdf > dependencies.pdf
# pipdeptree --graph-output png > dependencies.png

# 如果你想生成 requirements.txt，可以这么做：
pipdeptree -f | tee locked-requirements.txt
# 在确认没有冲突的依赖项后，甚至可以将其“锁定”，其中所有包都将固定到其当前安装的版本：
pipdeptree -f | sed 's/^[][]*//g' | sort -u > locked-requirements.txt
'''
# 生成可视化依赖树需要提前安装以下内容：
# yum install graphviz graphviz-devel
# pip3 install graphviz pipdeptree
import pipdeptree
import os
import subprocess


def test_1():
    print(pipdeptree.__version__)
    # exe_path = os.path.expanduser(r"~\AppData\Roaming\Python\Python37\Scripts\pipdeptree.exe")
    exe_path = r"D:\ProgramFiles\Anaconda3\Lib\base-packages\Python37\Scripts\pipdeptree.exe"
    if not os.path.exists(exe_path):
        print("路径不存在")
        exit(0)
    cmd = exe_path + "-p subprocess"
    print(cmd)
    child = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    child.wait()
    if child.returncode == 0:
        result = child.stdout.readline().decode('gbk').strip()
        while result:
            print(result)
            result = child.stdout.readline().decode('gbk').strip()


def test_2():
    # pip freeze --all > requirements.txt
    # 从 Python 3.8 开始，标准库中的 importlib.metadata 模块提供了一种获取已安装包元数据的方法，包括依赖信息。
    import importlib.metadata

    for package in importlib.metadata.distributions():
        print(f"{package.metadata['Name']}=={package.version}")


def main():
    test_1()
    test_2()


if __name__ == "__main__":
    main()
