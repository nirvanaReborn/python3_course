#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# pyc文件反编译python代码
'''
uncompyle6 --help 查看帮助
uncompyle6 models.pyc > models.py 将models.pyc反编译成py文件
uncompile -o . *.pyc 将当前文件夹中所有的pyc文件反编译成后缀名为.pyc_dis的源文件
可能后面你用spyder打开的时候说编码错误，用notpad++打开，然后变成“utf-8”编码，保存文件就可以了。
'''
# https://blog.csdn.net/u012067766/article/details/80092457
# pip install uncompyle

import os
import shutil
import uncompyle6


def uncompyle_pyc(file_path):
    if not file_path.endswith('.pyc'):
        return
    (filepath, filename) = os.path.split(file_path)
    (shotname, extension) = os.path.splitext(filename)
    dest_file = os.path.join(filepath, shotname + ".py")
    try:
        with open(dest_file, "w", encoding='utf8') as f:
            uncompyle6.decompile_file(file_path, f)
        print(dest_file)
    except Exception as e:
        print(dest_file, e)


def bath_uncompyle(source_dir):
    if os.path.isdir(source_dir):
        for parent, dirnames, filenames in os.walk(source_dir):
            for filename in filenames:
                source_file = os.path.join(parent, filename).encode('utf-8').decode('utf-8')
                # 文件不为空
                if os.path.getsize(source_file):
                    uncompyle_pyc(source_file)
    else:
        uncompyle_pyc(source_dir)


def move_to_dest(source_dir):
    dest_dir = r"D:\ProgramFiles\Anaconda3\Lib\site-packages"
    try:
        for parent, dirnames, filenames in os.walk(source_dir):
            for filename in filenames:
                source_file = os.path.join(parent, filename).encode('utf-8').decode('utf-8')
                if source_file.endswith('.pyc'):
                    os.remove(source_file)

        for item in os.listdir(source_dir):
            old_dir = os.path.join(source_dir, item)
            target_dir = os.path.join(dest_dir, item)
            if os.path.exists(target_dir):
                # 如果目标路径存在原文件夹的话就先删除
                shutil.rmtree(target_dir)
                print("删除已存在的文件夹:", target_dir)

            if os.path.isdir(old_dir):
                shutil.copytree(old_dir, target_dir)
                print("拷贝文件夹:", old_dir, "--->", target_dir)
            else:
                shutil.copyfile(old_dir, target_dir)
                print("拷贝文件:", old_dir, "--->", target_dir)
    except Exception as e:
        print(e)


def main():
    source_dir = r"D:\ARESHelper"
    bath_uncompyle(source_dir)
    # move_to_dest(source_dir)


if __name__ == '__main__':
    main()
