#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
# https://www.toutiao.com/a6525191832400298509/
unoconv，全称为 Universal Office Converter ，是一个命令行工具，可在 LibreOffice/OpenOffice 支持的任意文件格式之间进行转换。
unoconv 支持批量转换文档，还可以结合 asciidoc和 docbook2odf / xhtml2odt 来创建 PDF 或 Word（.doc） 文件。
'''


def main():
    pass


if __name__ == "__main__":
    main()
