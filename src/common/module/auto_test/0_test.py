#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/xiemanr/article/details/72763234
# Python的7种性能测试工具：
# timeit、profile、cProfile、line_profiler、memory_profiler、PyCharm图形化性能测试工具、objgraph


def fun():
    for i in range(10 ** 5):
        a = i ** 2


# -------------------------------------------------------------------------------------
# 1.timeit只输出被测试代码的总运行时间，单位为秒，没有详细的统计。
def test_1():
    import timeit
    print(timeit.timeit(fun, number=1))


# -------------------------------------------------------------------------------------
# 2.profile：纯Python实现的性能测试模块，接口和cProfile一样。
def test_2():
    import profile
    print(profile.run('fun()'))
    # ncall：函数运行次数
    # tottime：函数的总的运行时间，减去函数中调用子函数的运行时间
    # 第一个percall：percall = tottime / nclall
    # cumtime:函数及其所有子函数调整的运行时间，也就是函数开始调用到结束的时间。
    # 第二个percall：percall = cumtime / nclall


# -------------------------------------------------------------------------------------
# 3.cProfile：c语言实现的性能测试模块，接口和profile一样。
def test_3():
    import cProfile
    print(cProfile.run('fun()'))


# -------------------------------------------------------------------------------------
# 4.line_profiler工具可以统计每行代码的执行次数和执行时间等，时间单位为微妙。
# pip install line_profiler
# 安装之后kernprof.py会加到环境变量中。
# 1.在需要测试的函数加上@profile装饰，这里我们把测试代码写在C:\Python34\test.py文件上.
# 2.运行命令行：kernprof -l -v C:\Python36\test.py
# 输出结果如下：
# Total Time：测试代码的总运行时间
# Hits：表示每行代码运行的次数
# Time：每行代码运行的总时间
# Per Hits：每行代码运行一次的时间
# % Time：每行代码运行时间的百分比


# -------------------------------------------------------------------------------------
# 5.memory_profiler工具可以统计每行代码占用的内存大小。
# 安装：
# pip install memory_profiler
# pip install psutil
#
# 测试代码：
# 同line_profiler。
#
# 使用：
# 检测内存泄露时使用
# from memory_profiler import profile
# 1.在需要测试的函数加上@profile装饰
# 2.执行命令： python -m memory_profiler C:\Python34\test.py


# -------------------------------------------------------------------------------------
# 6.PyCharm图形化性能测试工具
# PyCharm提供了图像化的性能分析工具，使用方法见利用PyCharm的Profile工具进行Python性能分析（https://blog.csdn.net/xiemanr/article/details/69398057）。


# -------------------------------------------------------------------------------------
# 7.objgraph是一个实用模块,可以列出当前内存中存在的对象，可用于定位内存泄露。
# pip install objgraph


# -------------------------------------------------------------------------------------
# pip install pytest
def test_8():
    import pytest
    print(pytest.__version__)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
