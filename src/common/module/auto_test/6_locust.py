#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/imyalost/p/9758189.html
# python开源的性能测试框架locust
# 官方文档：https://docs.locust.io/en/stable/index.html
'''
一、认识Locust

1、定义
Locust是一款易于使用的分布式负载测试工具，完全基于事件，即一个locust节点也可以在一个进程中支持数千并发用户，不使用回调，通过gevent使用轻量级过程（即在自己的进程内运行）。

2、特点
①、不需要编写笨重的UI或者臃肿的XML代码，基于协程而不是回调，脚本编写简单易读；
②、有一个基于we简洁的HTML+JS的UI用户界面，可以实时显示相关的测试结果；
③、支持分布式测试，用户界面基于网络，因此具有跨平台且易于扩展的特点；
④、所有繁琐的I / O和协同程序都被委托给gevent，替代其他工具的局限性；

3、locust与jmeter的区别
工具	区别
jmeter	需要在UI界面上通过选择组件来“编写”脚本，模拟的负载是线程绑定的，意味着模拟的每个用户，都需要一个单独的线程。单台负载机可模拟的负载数有限
locust	通过编写简单易读的代码完成测试脚本，基于事件，同样配置下，单台负载机可模拟的负载数远超jmeter
PS：但locust的局限性在于，目前其本身对测试过程的监控和测试结果展示，不如jmeter全面和详细，需要进行二次开发才能满足需求越来越复杂的性能测试需要。
'''
# pip install locustio


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
