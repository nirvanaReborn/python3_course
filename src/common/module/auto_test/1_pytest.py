#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
# https://www.jianshu.com/p/a754e3d47671
# pytest是一个非常成熟的全功能的Python测试框架，主要特点有以下几点：
# 1、简单灵活，容易上手，文档丰富；
# 2、支持参数化，可以细粒度地控制要测试的测试用例；
# 3、能够支持简单的单元测试和复杂的功能测试，还可以用来做selenium/appnium等自动化测试、接口自动化测试（pytest+requests）;
# 4、pytest具有很多第三方插件，并且可以自定义扩展，比较好用的如pytest-selenium（集成selenium）、pytest-html（完美html测试报告生成）、pytest-rerunfailures（失败case重复执行）、pytest-xdist（多CPU分发）等；
# 5、测试用例的skip和xfail处理；
# 6、可以很好的和CI工具结合，例如jenkins

编写规则
1、测试文件以test_开头（以_test结尾也可以）
2、测试类以Test开头，并且不能带有 init 方法
3、测试函数以test_开头
4、断言使用基本的assert即可

'''
# pip install pytest
import pytest


'''
fixture的scope参数
scope参数有四种，分别是'function','module','class','session'，默认为function。
    function：每个test都运行，默认是function的scope
    class：每个class的所有test只运行一次
    module：每个module的所有test只运行一次
    session：每个session只运行一次

setup和teardown操作
    setup，在测试函数或类之前执行，完成准备工作，例如数据库链接、测试数据、打开文件等
    teardown，在测试函数或类之后执行，完成收尾工作，例如断开数据库链接、回收内存资源等
    备注：也可以通过在fixture函数中通过yield实现setup和teardown功能
'''


@pytest.fixture(scope='function')
def setup_function(request):
    def teardown_function():
        print("teardown_function called.")

    request.addfinalizer(teardown_function)  # 此内嵌函数做teardown工作
    print('setup_function called.')


@pytest.fixture(scope='module')
def setup_module(request):
    def teardown_module():
        print("teardown_module called.")

    request.addfinalizer(teardown_module)
    print('setup_module called.')


@pytest.mark.website
def test_1(setup_function):
    print('Test_1 called.')


def test_2(setup_module):
    print('Test_2 called.')


def test_3(setup_module):
    print('Test_3 called.')
    assert 2 == 1 + 1
