#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6789789666838053388/
# 标准库中有两个测试包: unittest 和 doctest。
# 第三方包nose（https://nose.readthedocs.org/en/latest/）和unittest 类似。
# nose2 在 unittest 的基础上开发的，你可以在不改变原有代码的情况下直接使用 nose2。
# 不需要像使用unittest 一样创建一个包含测试方法的类。任何名称中带test 的函数都会被执行。
# 运行测试用例: 直接在命令行通过 nose2 命令运行。他会在python文件中查找名称以 test 开头的测试文件，并运行它发现的每个以 test 开头的测试函数名称。
# 运行指定文件夹下的测试用例: nose2 -s 文件夹名称
# 运行指定的测试用例: nose2 login_case.test_nose2_dir.test_login
import unittest

# 安装命令：pip install nose2
import nose2


class TestAdd(unittest.TestCase):
    def test_add(self):
        self.assertEqual(1 + 1, 3)


if __name__ == "__main__":
    # 只需要替换这一行
    # unittest.main()
    nose2.main()
