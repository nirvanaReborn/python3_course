#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/sunshine-blog/p/6823049.html
# Python测试工具——nose

# 1、nose 特点：
# a)         自动发现测试用例（包含[Tt]est文件以及文件包中包含test的函数）
# b)         以test开头的文件
# c)         以test开头的函数或方法
# d)         以Test开头的类
# nose自动收集单元测试，会自动识别源代码文件、目录或包中的测试用例，
# 任何符合正则表达式：(?:^|[b_.-]）[Tt]est的类、函数、文件或目录，
# 以及TestCase的子类都会被识别并执行，匹配成功的包、任何python的源文件都会被当做测试用例。
#
#
# 2、nose的setup和teardown：
# a)　　package（包）中的setup、teardown，放在__init__.py文件中　　—>整个测试的运行期间只运行一次
# b)　　模块级别：setup_module、teardown_module 　　—>整个测试的运行期间只运行一次
# c)　　类级别：setup_class、teardown_class       —>每个测试方法执行时都会调用
#
#
# 3、nose相关执行命令：
# 1、  nosetests  –h查看所有nose相关命令
# 2、  nosetests –s执行并捕获输出
# 3、  nosetests –with-xunit输出xml结果报告
# 4、  nosetests -v: 查看nose的运行信息和调试信息
# 5、  nosetests -w 目录：指定一个目录运行测试
#
#
# 4、nose 测试代码的执行方法：
#     1.cd + 要执行的文件地址（如：C:\Users\zhangjx\test_main\Test1\test）-> nosetests
#     2.使用编码实现测试用例的执行,创建main.py（名字随意），内容如下：
#         import nose
#         nose.main()
#         执行即可：
#         或nosetests test.py
#     3.直接运行整个包：nosetests -v  test_case
#     4.运行某一个模块：nosetests –v  test_case.test_case_0002
#     5.运行某一个用例：nosetests -v  test_case.test_case_0002:test_lean_4
#     6.运行不同模式下不同用例：
#                      nosetests  -v --tests=test_case.test_case_0002:test_lean_4,test_case.test_case_0001:test_lean_2
#
#
# 5、工具nose.tools的使用：
# 1）测试脚本中引入：from nose.tools import nottest,istest；
# 2）不测试的方法：方法名上加修饰器@nottest；
# 3）指定为测试方法：方法名上加修饰器@istest（方法名无需符合命名规则）；
# 4）查看要执行的用例列表：nosetests --collect-only –v

import nose


def main():
    nose.main()


if __name__ == "__main__":
    main()
