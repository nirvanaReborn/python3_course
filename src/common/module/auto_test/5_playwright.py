#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/i6999825710172865037
# Playwright 是针对 Python 语言的纯自动化工具，它可以通过单个API自动执行 Chromium，Firefox 和 WebKit 浏览器，连代码都不用写，就能实现自动化功能。

'''
# pip install playwright
安装浏览器驱动文件（安装过程稍微有点慢）python -m playwright install
    安装Playwright依赖库，需要Python3.7+
    安装Chromium、Firefox、WebKit等浏览器的驱动文件

使用Playwright无需写一行代码，我们只需手动操作浏览器，它会录制我们的操作，然后自动生成代码脚本。
'''






def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
