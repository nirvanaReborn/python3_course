#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/lsdb/p/10444943.html
# 注意所有测试方法都需要以test开头。

import unittest


# 直接使用TestCase执行时是按字母排序的顺序执行的
class Test1(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        print("execute setUpClass")

    @classmethod
    def tearDownClass(self):
        print("execute tearDownClass")

    def setUp(self):
        print("execute setUp")

    def tearDown(self):
        print("execute tearDown")

    def test_one(self):
        print('execute test_one')
        self.assertTrue('FOO'.isupper())

    def test_two(self):
        print('execute test_two')


if __name__ == '__main__':
    unittest.main()

