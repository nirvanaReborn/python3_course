#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6583575930227130887/
# Dolorean是一个非常酷的日期/时间库。类似JavaScript的moment，拥有非常完善的技术文档。


import delorean


def test_1():
    EST = "US/Eastern"
    d = delorean.Delorean(timezone=EST)
    print(d)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
