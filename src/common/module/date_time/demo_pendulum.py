#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 原生的 datetime 足够应付基本情况，但当面对更复杂的用例时，通常会有的捉襟见肘，不那么直观。
# Pendulum 在标准库的基础之上，提供了一个更简洁，更易于使用的 API ，旨在让 Python datetime 更好用。
# 它支持时区转换、日期、时间操作和格式设置。

import pendulum


def test_1():
    now_in_paris = pendulum.now('Europe/Paris')
    print(now_in_paris)
    print(now_in_paris.in_timezone('UTC'))

    tomorrow = pendulum.now().add(days=1)
    last_week = pendulum.now().subtract(weeks=1)


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
