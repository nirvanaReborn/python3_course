#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6520028124908880387/
# python编程计算时间的20个常用代码段
'''
1 millisecond = 1000 microseconds
1 minute = 60 seconds
1 hour = 3600 seconds
1 week = 7 days
'''


def test_1():
    import time
    print(time.asctime(time.localtime()))


def test_2():
    import pandas
    print(pandas.datetime.now())
    print(pandas.datetime.now().date())
    print(pandas.datetime.now().year)
    print(pandas.datetime.now().month)
    print(pandas.datetime.now().day)
    print(pandas.datetime.now().hour)
    print(pandas.datetime.now().minute)
    print(pandas.datetime.now().second)
    print(pandas.datetime.now().microsecond)


def test_3():
    import datetime
    d1 = "Jan 7 2015  1:15PM"
    # 如果你知道日期格式
    data1 = datetime.datetime.strptime(d1, '%b %d %Y %I:%M%p')
    print(data1)
    print(type(data1))


def test_4():
    # pip install python-dateutil
    from dateutil import parser, rrule
    d1 = "2015 Jan 7  1:15PM"
    # 如果你不知道日期格式
    # parser是根据字符串解析成datetime,字符串可以很随意，可以用时间日期的英文单词，可以用横线、逗号、空格等做分隔符。
    # 没指定时间默认是0点，没指定日期默认是今天，没指定年份默认是今年。
    # 只有月日时，parser会将分隔符前面的数字解析为月份，后面的为日
    # 当有年份时，在前面的月份超出范围时，会自动判断哪个是月哪个是日
    data1 = parser.parse(d1)
    print(type(data1), data1)

    # 生成一个连续的日期列表
    data2 = list(rrule.rrule(rrule.DAILY, dtstart=parser.parse('2018-11-1'), until=parser.parse('2018-11-5')))
    print(data2)

    # 计算时间差
    data3 = rrule.rrule(rrule.MONTHLY, dtstart=parser.parse('2018-3-15'), until=parser.parse('2018-11-10')).count()
    print(data3)


def test_5():
    # 获取MST,EST,UTC,GMT和HST形式的当前日期时间
    import datetime
    import pytz
    print(datetime.datetime.now(pytz.timezone("MST")))
    print(datetime.datetime.now(pytz.timezone("EST")))
    print(datetime.datetime.now(pytz.timezone("UTC")))
    print(datetime.datetime.now(pytz.timezone("GMT")))
    print(datetime.datetime.now(pytz.timezone("HST")))


def test_6():
    # 从指定日期获取一周中的日期码
    import datetime
    # dayofweek = datetime.date(2010, 6, 16).strftime("%A")
    print(datetime.date(2010, 6, 16).strftime("%A"))

    # 周一是0，周日是6
    print(datetime.date(2010, 6, 16).weekday())

    # 周一是1，周日是7
    print(datetime.date(2010, 6, 16).isoweekday())

    print(datetime.datetime.today().strftime("%A"))
    print(datetime.datetime.today().weekday())
    print(datetime.datetime.today().isoweekday())


def test_7():
    import datetime
    # 如何计算两个datatime对象之间的时间差？
    datetimeFormat = "%Y-%m-%d %H:%M:%S.%f"
    date1 = "2016-04-16 10:01:28.585"
    date2 = "2016-04-10 09:56:28.067"
    diff = datetime.datetime.strptime(date1, datetimeFormat) - datetime.datetime.strptime(date2, datetimeFormat)
    print(diff, type(diff))
    print(diff.days)
    # seconds 来获取时间差中的秒数。注意，seconds获得的秒只是时间差中的小时、分钟和秒部分的和，并没有包含时间差的天数
    print(diff.seconds)
    # 获取时间差中的秒数，并且包含时间差的天数，返回值为整型
    print(diff.microseconds)
    # 精确获取时间差中的秒数，并且包含时间差的天数，返回值为浮点型
    print(diff.total_seconds())


def test_8():
    # 将UNIX时间戳转换为日期和时间字符串
    import datetime
    dateStr = datetime.datetime.fromtimestamp(1415419007).strftime("%A, %B %d, %Y %I:%M:%S")
    print(dateStr)
    print(type(dateStr))


def test_9():
    # 将日期时间对象转化为Unix（时间戳）
    import datetime
    import time
    date_obj = datetime.datetime(2015, 10, 10, 10, 10)
    print("Unix Timestamp:", time.mktime(date_obj.timetuple()))


def test_10():
    # 将N年、月、日、小时、分钟、秒添加到当前日期时间
    import datetime
    from dateutil.relativedelta import relativedelta
    num = +6
    add_years = datetime.datetime.today() + relativedelta(years=num)
    add_months = datetime.datetime.today() + relativedelta(months=num)
    add_days = datetime.datetime.today() + relativedelta(days=num)
    add_hours = datetime.datetime.today() + relativedelta(hours=num)
    add_minutes = datetime.datetime.today() + relativedelta(minutes=num)
    add_seconds = datetime.datetime.today() + relativedelta(seconds=num)
    print(datetime.datetime.today())
    print(add_years, add_months, add_days, add_hours, add_minutes, add_seconds, sep="\n")

    # 当前日期时间减去N年、月、日、小时、分钟、秒
    num = -6
    add_years = datetime.datetime.today() + relativedelta(years=num)
    add_months = datetime.datetime.today() + relativedelta(months=num)
    add_days = datetime.datetime.today() + relativedelta(days=num)
    add_hours = datetime.datetime.today() + relativedelta(hours=num)
    add_minutes = datetime.datetime.today() + relativedelta(minutes=num)
    add_seconds = datetime.datetime.today() + relativedelta(seconds=num)
    print(datetime.datetime.today())
    print(add_years, add_months, add_days, add_hours, add_minutes, add_seconds, sep="\n")


def test_11():
    import datetime
    # 指定的开始日期和结束日期之间获取日期范围
    start = datetime.datetime.strptime("2016-06-15", "%Y-%m-%d")
    end = datetime.datetime.strptime("2016-06-30", "%Y-%m-%d")
    date_array = (start + datetime.timedelta(days=x) for x in range(0, (end - start).days))
    for date_obj in date_array:
        print(date_obj.strftime("%Y-%m-%d"))


def test_12():
    # 打印指定年所有的星期一
    import datetime
    year = 2018
    date_obj = datetime.date(year, 1, 1)
    date_obj += datetime.timedelta(days=1 - date_obj.isoweekday())
    while date_obj.year == year:
        print(date_obj)
        date_obj += datetime.timedelta(days=7)


def test_13():
    import datetime
    # 根据当前日期找到上一个周一和即将到的周一的日期
    today = datetime.date.today()
    last_monday = today - datetime.timedelta(days=today.weekday())
    coming_monday = today + datetime.timedelta(days=-today.weekday(), weeks=1)
    print(today, last_monday, coming_monday)


def test_14():
    # 如何得到指定日期所在周的开始和结束日期？
    import datetime
    dateStr = "2018-01-14"
    date_obj = datetime.datetime.strptime(dateStr, "%Y-%m-%d")
    start_of_week = date_obj - datetime.timedelta(days=date_obj.weekday())
    end_of_week = start_of_week + datetime.timedelta(days=6)
    print(start_of_week, end_of_week)


def test_15():
    # 获取当前季度的第一天和最后一天
    import datetime
    current_date = datetime.datetime.now()  # 2021-11-16 16:18:55.600872
    current_quarter = round((current_date.month - 1) / 3 + 1)
    first_date = datetime.datetime(current_date.year, 3 * current_quarter - 2, 1)
    last_date = datetime.datetime(current_date.year, 3 * current_quarter + 1, 1) + datetime.timedelta(days=-1)
    print(first_date, last_date)


# ---------------------------------------------------------------------------
# https://my.oschina.net/editorial-story/blog/1556818
def test_16():
    '''
    Arrow 是一个专门处理时间和日期的轻量级 Python 库，
    它提供了一种合理、智能的方式来创建、操作、格式化、转换时间和日期，
    并提供了一个支持许多常见构建方案的智能模块 API 。
    简单来说，它可以帮你以更简便的操作和更少的代码来使用日期和时间。
    其设计灵感主要来源于 moment.js 和 requests 。
    '''
    import arrow
    print(arrow.utcnow())


def test_17():
    '''
    Delorean 提供了一个相比于 datetime 和 pytz 的更好的抽象，让你处理时间更容易。
    它有很多有用的处理时区的特性，标准化时区或者从一个时区改变到另外一个时区。
    '''
    import datetime
    import pytz

    est = pytz.timezone('US/Eastern')
    d = datetime.datetime.now(pytz.utc)
    d = est.normalize(d.astimezone(est))
    print(d)
    # -------------------------------------
    from delorean import Delorean

    d = Delorean()
    d = d.shift('US/Eastern')
    print(d)


def test_18():
    pass


def test_19():
    '''
    dateutil 是 datetime 标准库的一个扩展库，
    几乎支持以所有字符串格式对日期进行通用解析，日期计算灵活，内部数据更新及时。
    '''
    from dateutil import parser
    now = parser.parse("Sat Oct 11 17:13:46 UTC 2003")
    today = now.date()
    print("Today is: %s" % today)


def test_20():
    '''
    用于处理日期/时间的 Python 库，设计灵感同样是来源于 moment.js 和 requests ，
    设计理念源自 Times Python 模块。
    '''
    import moment
    print(moment.date("12-18-2012", "%m-%d-%Y"))
    print(moment.now())


def test_21():
    # When.py
    import when
    print(when.today())
    print(when.now())
    print(when.tomorrow)


def test_22():
    # https://github.com/freach/udatetime
    import udatetime


def test_23():
    import datetime, time
    begin_time = datetime.datetime.now()
    time.sleep(1)
    end_time = datetime.datetime.now()
    print(end_time - begin_time)
    # t = (end_time - begin_time).seconds
    # print(t, type(t))
    m = int(abs((end_time - begin_time).total_seconds()))
    print(m, type(m))
    if m > 1:
        print("%s 时间差大于1秒" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
