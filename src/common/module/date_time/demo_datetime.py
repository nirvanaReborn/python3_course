#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://www.pythondoc.com/pythontutorial3/stdlib.html
# datetime 模块为日期和时间处理同时提供了简单和复杂的方法。
# 支持日期和时间算法的同时，实现的重点放在更有效的处理和格式化输出。
# 该模块还支持时区处理。
"""
# datetime.date: 表示日期的类。常用的属性有 year，month，day；
# datetime.time: 表示时间的类。常用的属性有 hour，minute，second，microsecond；
# datetime.datetime: 表示日期时间。
# datetime.timedelta: 表示时间间隔，即两个时间点之间的长度。
# datetime.tzinfo：与时区有关的相关信息。
"""
import datetime


def test_help():
    help(datetime)


def test_dir():
    for i in dir(datetime):
        print(i)


def test_1():
    t31 = datetime.datetime.today()
    print(t31)  # 2017-03-17 20:04:15.352613

    t32 = datetime.datetime.timestamp(t31)
    print(t32)  # 1489752255.352613

    t33 = str(t32).replace('.', '')[:-3]
    print(t33)  # 1489752255352

    t34 = datetime.datetime(2015, 12, 16, 15, 6, 37, 420000)
    print(t34)  # 2015-12-16 15:06:37.420000


# datetime.timedelta用于计算两个日期之间的差值
def test_2():
    now = datetime.datetime.now()
    print(now)  # 2017-03-17 20:50:42.796306
    day_of_week = datetime.datetime.isoweekday(now)
    print(day_of_week)  # 5
    if day_of_week == 1:  # Monday
        last_time = now + datetime.timedelta(days=-3)
    else:
        last_time = now + datetime.timedelta(days=-1)
    print(last_time)  # 2017-03-16 20:50:42.796306

    a = datetime.datetime(2015, 10, 3)
    b = datetime.datetime.now()

    res1 = (b - a)
    res2 = (b - a).days  # 取时间差天数(整型)
    res3 = (b - a).seconds  # 取时间差时分秒部分的秒数总和(整型)
    res4 = (b - a).microseconds  # 取时间差 秒数之后的微秒数(整型)
    res5 = (b - a).total_seconds()  # 时间差的总秒数(浮点型)
    print(type(res1), res1)
    print(type(res2), res2)
    print(type(res3), res3)
    print(type(res4), res4)
    print(type(res5), res5)


# http://ju.outofmemory.cn/entry/1078
# 在Python下转换时间格式
def test_3():
    # 时区为UTC
    t51 = datetime.datetime.utcfromtimestamp(1234567890)
    print(t51)
    # 时区为本地时区
    t61 = datetime.datetime.fromtimestamp(1234567890)
    print(t61)
    # 不建议使用后者，因为在Mac OS X下，系统时区为北京时间时，
    # datetime.datetime.fromtimestamp(0)的结果为datetime.datetime(1970, 1, 1, 8, 30)，多出了30分钟。
    # time.mktime(datetime.datetime(1970, 1, 1).utctimetuple()) / 3600的结果为-8.5，也差了半小时。
    # 但是选择上海等其他城市却没有这30分钟的时差。


def test_4():
    now = datetime.date.today()
    print(now)
    print(now.strftime("%m-%d-%y. %d %b %Y is a %A on the %d day of %B."))
    birthday = datetime.date(2020, 1, 1)
    age = now - birthday
    print(age.days)  # 从出生到现在的天数

    begin_time = datetime.datetime.strptime("9:30", '%H:%M')
    end_time = datetime.datetime.strptime("11:30", '%H:%M')
    min_count = end_time - begin_time
    print(begin_time, end_time)
    print(min_count.seconds / 60)


def test_5():
    row_timestamp = str(datetime.datetime.timestamp(datetime.datetime.today()))
    print(row_timestamp)
    print(int(datetime.datetime.timestamp(datetime.datetime.today())))
    timestamp = row_timestamp.replace('.', '')[:-3]
    print(timestamp)


def test_6():
    print(datetime.date.today())  # 2020-06-06
    print(datetime.datetime.now())  # 2020-06-06 18:21:57.288790
    print(datetime.datetime.now().date())  # 2020-06-06
    print(datetime.datetime.now().strftime('%Y%m%d'))  # 20200606
    print(datetime.datetime.now().time())  # 18:21:57.288790
    # 由日期格式转化为字符串格式
    print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f'))
    # time_stamp = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
    # 由字符串格式转化为日期格式
    print(datetime.datetime.strptime("2018-10-18 19:21:37", '%Y-%m-%d %H:%M:%S'))


def test_7():
    a = datetime.datetime.strptime(str(20211206195601123), '%Y%m%d%H%M%S%f')
    # a = datetime.datetime.strptime(str(10000000195601123), '%Y%m%d%H%M%S%f') # 报错
    print(type(a), a)

    a = int(abs((datetime.datetime.strptime(
        str(20211216091601123), "%Y%m%d%H%M%S%f") - datetime.datetime.now()).total_seconds()) / 60)
    print(a)


def test_8():
    def get_week_day(date=datetime.datetime.now()):
        week_day_dict = {
            0: '星期一',
            1: '星期二',
            2: '星期三',
            3: '星期四',
            4: '星期五',
            5: '星期六',
            6: '星期天',
        }
        return week_day_dict[date.weekday()]

    print(get_week_day())


# ------------------------------------------------------------------------------
# python--计算上周5的日期
# http://hejiawangjava.iteye.com/blog/2321746
# 第一种方法：
def test_9():
    weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday',
                'Friday', 'Saturday', 'Sunday']

    def get_previous_byday(dayname, start_date=None):
        if start_date is None:
            start_date = datetime.datetime.today()
        day_num = start_date.weekday()
        # print(day_num)
        day_num_target = weekdays.index(dayname)
        # print(day_num_target)
        days_ago = (7 + day_num - day_num_target) % 7
        if days_ago == 0:
            days_ago = 7
        target_date = start_date - datetime.timedelta(days=days_ago)
        return target_date

    print(datetime.datetime.today())  # 2017-09-16 14:14:08.818729
    print(get_previous_byday('Monday'))  # 2017-09-11 14:14:08.818729
    print(get_previous_byday('Monday', datetime.datetime(2016, 8, 28)))  # 2016-08-22 00:00:00


# 第二种方法，用dateutil模块
def test_10():
    from dateutil.relativedelta import relativedelta
    from dateutil.rrule import FR
    d = datetime.datetime.now()
    print(d)  # 2017-09-16 14:12:52.186904
    print(d + relativedelta(weekday=FR))  # 2017-09-22 14:12:52.186904
    print(d + relativedelta(weekday=FR(-1)))  # 2017-09-15 14:12:52.186904


def test_11():
    import pprint
    import numpy
    _start_time = datetime.datetime.strptime(
        datetime.datetime.now().strftime("%Y%m%d") + "0931", '%Y%m%d%H%M')
    datetime_list = [int(datetime.datetime.strftime(_start_time + datetime.timedelta(
        minutes=i), '%Y%m%d%H%M')) for i in range(330) if i < 120 or i > 209]
    nan_array = numpy.array([(dt, numpy.nan, numpy.nan) for dt in datetime_list],
                            dtype=numpy.dtype([("datetime", numpy.int64), ("open", float), ("close", float)]))
    pprint.pprint(nan_array)


# ------------------------------------------------------------------------------
def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
