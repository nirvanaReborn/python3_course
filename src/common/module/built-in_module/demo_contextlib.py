#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/tian777/p/17527660.html
# https://blog.csdn.net/2201_75362610/article/details/132146557
# 上下文管理器 两大作用：
# -- 可以以一种更加优雅的方式，操作（创建/获取/释放）资源，如文件操作、数据库连接；
# -- 可以以一种更加优雅的方式，处理异常；

import contextlib


def test_1():
    def acquire_resource(*args, **kwds) -> object:
        pass

    def release_resource(resource):
        pass

    @contextlib.contextmanager
    def managed_resource(*args, **kwds):
        # 进入上下文前的操作
        print('entering context')
        # Code to acquire resource, e.g.:
        resource = acquire_resource(*args, **kwds)
        try:
            yield resource
        finally:
            # 离开上下文后的操作
            print('exiting context')
            # Code to release resource, e.g.:
            release_resource(resource)


def test_2():
    # closing函数是一个帮助函数，用来确保对象的close方法在完成后被正确的调用。
    # 这对于一些提供了close方法但是没有实现上下文管理协议的对象非常有用。
    with contextlib.closing(open('my_file.txt', 'w')) as f:
        f.write('Hello, World!')


def test_3():
    # ExitStack类是一个灵活的上下文管理器，它可以管理多个上下文管理器和一般的清理任务。
    def archive_files(files, archive_name):
        with contextlib.ExitStack() as stack:
            archive = stack.enter_context(open(archive_name, 'w'))
            for file_name in files:
                file = stack.enter_context(open(file_name))
                archive.write(file.read())


def test_4():
    # nullcontext是一个非常简单的上下文管理器，它不执行任何操作。
    # 它的主要用途是在需要动态决定是否需要使用上下文管理器时作为一个占位符使用。
    def optional_open(file_name, mode, use_context):
        if use_context:
            return open(file_name, mode)
        else:
            return contextlib.nullcontext()

    with optional_open('my_file.txt', 'w', False) as f:
        if f is not None:
            f.write('Hello, World!')


def test_5():
    import os
    # suppress函数可以用来忽略指定的异常。
    with contextlib.suppress(FileNotFoundError):
        os.remove('my_file.txt')


def main():
    pass


if __name__ == '__main__':
    main()
