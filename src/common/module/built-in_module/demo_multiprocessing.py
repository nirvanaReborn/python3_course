#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 1.在UNIX平台上，当某个进程终结之后，该进程需要被其父进程调用wait，否则进程成为僵尸进程(Zombie)。
#     所以，有必要对每个Process对象调用join()方法 (实际上等同于wait)。
#     对于多线程来说，由于只有一个进程，所以不存在此必要性。
# 2.multiprocessing提供了threading包中没有的IPC(比如Pipe和Queue)，效率上更高。
#     应优先考虑Pipe和Queue，避免使用Lock/Event/Semaphore/Condition等同步方式 (因为它们占据的不是用户进程的资源)。
# 3.多进程应该避免共享资源。
#     在多线程中，我们可以比较容易地共享资源，比如使用全局变量或者传递参数。
#     在多进程情况下，由于每个进程有自己独立的内存空间，以上方法并不合适。
#     此时我们可以通过共享内存和Manager的方法来共享资源。
#     但这样做提高了程序的复杂度，并因为同步的需要而降低了程序的效率。

# https://blog.csdn.net/brucewong0516/article/details/85776194

import multiprocessing
import os


def whoami(what):
    print("Process [%s]: %s" % (os.getpid(), what))


def do_this(what):
    whoami(what)


def test_1():
    whoami("I'm the main program")
    for n in range(os.cpu_count()):
        p = multiprocessing.Process(target=do_this,
                                    args=("I'm function %s" % n,))
        p.start()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
