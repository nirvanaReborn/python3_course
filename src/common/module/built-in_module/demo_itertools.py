#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
itertools（https://docs.python.org/3/library/itertools.html）包含特殊用途的迭代器函数。
在for ... in 循环中调用迭代函数，每次会返回一项，并记住当前调用的状态。

# https://www.cnblogs.com/haiyan123/p/9804091.html
# itertools 是python的迭代器模块，itertools提供的工具相当高效且节省内存。
# 使用这些工具，你将能够创建自己定制的迭代器用于高效率的循环。
# https://www.cnblogs.com/suke99/p/5185495.html

# https://blog.csdn.net/weixin_44585839/article/details/98474105
其中函数主要可以分为三类，分别是无限迭代器，有限迭代器，组合迭代器。

无限迭代器（Infinite Iterators）
    count([start=0, step=1]) 接收两个可选整形参数，第一个指定了迭代开始的值，第二个指定了迭代的步长。
'''

import itertools


def test_help():
    help(itertools)


def test_dir():
    for i in dir(itertools):
        print(i)


def test_3():
    # 即使chain() 的参数只是单个迭代对象，它也会使用参数进行迭代：
    for item in itertools.chain([1, 2], ['a', 'b']):
        print(item)


def test_4():
    # cycle() 是一个在它的参数之间循环的无限迭代器：
    for item in itertools.cycle([1, 2]):
        print(item)


def test_5():
    # accumulate() 计算累积的值。默认的话，它计算的是累加和：
    for item in itertools.accumulate([1, 2, 3, 4]):
        print(item)

    '''
    # 你可以把一个函数作为accumulate() 的第二个参数，代替默认的加法函数。
    # 这个参数函数应该接受两个参数，返回单个结果。
    '''

    def multiply(a, b):
        return a * b

    for item in itertools.accumulate([1, 2, 3, 4], multiply):
        print(item)


def test_6():
    for i in itertools.count(10, 2):
        print(i)
        if i > 20:
            break


def test_7():
    it = iter(range(100))
    for i in it:
        print(i)
        # itertools.islice(iterable, start, stop[, step])
        next(itertools.islice(it, 4, 4), None)  # 类似continue(5)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
