#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""
# http://blog.csdn.net/yueguanghaidao/article/details/7265246
# http://www.runoob.com/python/python-xml.html

python有三种方法解析XML: SAX，DOM，以及ElementTree
###1.SAX (simple API for XML )
       pyhton 标准库包含SAX解析器，SAX是一种典型的极为快速的工具，在解析XML时，不会占用大量内存。
但是这是基于回调机制的，因此在某些数据中，它会调用某些方法进行传递。这意味着必须为数据指定句柄，
以维持自己的状态，这是非常困难的。


###2.DOM(Document Object Model)
       与SAX比较，DOM典型的缺点是比较慢，消耗更多的内存，因为DOM会将整个XML数读入内存中，并为树
中的第一个节点建立一个对象。使用DOM的好处是你不需要对状态进行追踪，因为每一个节点都知道谁是它的
父节点，谁是子节点。但是DOM用起来有些麻烦。


###3.ElementTree(元素树)
     ElementTree就像一个轻量级的DOM，具有方便友好的API。代码可用性好，速度快，消耗内存少。

注：因DOM需要将XML数据映射到内存中的树，一是比较慢，二是比较耗内存，
    而SAX流式读取XML文件，比较快，占用内存少，但需要用户实现回调函数（handler）。
"""
import os


def create_xml_dom(dest_file):
    import xml.dom.minidom
    list_1D = ['dede', '1234', '汉子', 'msfsd  dfsd']
    impl = xml.dom.minidom.getDOMImplementation()
    domTree = impl.createDocument(None, 'root', None)
    root = domTree.documentElement
    secondNode = domTree.createElement('secondNode')
    # secondNode.setAttribute('id', 1)
    root.appendChild(secondNode)
    for item in list_1D:
        nameE = domTree.createElement('third')
        nameT = domTree.createTextNode(item)
        nameE.appendChild(nameT)
        secondNode.appendChild(nameE)

    fw = open(dest_file, 'w', encoding='utf-8')
    domTree.writexml(fw, addindent='  ', newl='\n', encoding='utf-8')
    fw.close()


def call_xml_dom(source_file):
    # 使用minidom解析器打开 XML 文档
    import xml.dom.minidom

    domTree = xml.dom.minidom.parse(source_file)
    collection = domTree.documentElement
    if collection.hasAttribute("shelf"):
        print("Root element : %s" % collection.getAttribute("shelf"))

    # 在集合中获取所有电影
    movies = collection.getElementsByTagName("movie")

    # 打印每部电影的详细信息
    for movie in movies:
        print("*****Movie*****")
        if movie.hasAttribute("title"):
            print("Title: %s" % movie.getAttribute("title"))

        type = movie.getElementsByTagName('type')[0]
        print("Type: %s" % type.childNodes[0].data)
        format = movie.getElementsByTagName('format')[0]
        print("Format: %s" % format.childNodes[0].data)
        rating = movie.getElementsByTagName('rating')[0]
        print("Rating: %s" % rating.childNodes[0].data)
        description = movie.getElementsByTagName('description')[0]
        print("Description: %s" % description.childNodes[0].data)


def call_xml_etree(source_file):
    '''
    # http://blog.csdn.net/jerry_1126/article/details/43733889
    ElementTree生来就是为了处理XML ，它在Python标准库中有两种实现:
    一种是纯Python实现，例如: xml.etree.ElementTree
    另外一种是速度快一点的:   xml.etree.cElementTree
    尽量使用C语言实现的那种，因为它速度更快，而且消耗的内存更少! 在程序中可以这样写:
    try:
        import xml.etree.cElementTree as ET
    except ImportError:
        import xml.etree.ElementTree as ET

    加载XML :
    方法一:加载文件
    root = ET.parse('book.xml')
    方法二:加载字符串
    root = ET.fromstring(xmltext)

    获取节点:
    方法一:获得指定节点->getiterator()方法
    book_node = root.getiterator('list')
    方法二:获得指定节点->findall()方法
    book_node = root.findall('list')
    方法三:获得指定节点->find()方法
    book_node = root.find('list')
    方法四:获得儿子节点->getchildren()
    for node in book_node:
        book_node_child = node.getchildren()[0]
        print(book_node_child.tag, '=> ', book_node_child.text)

    常用方法:
    # 当要获取属性值时，用attrib方法。
    # 当要获取节点名时，用tag方法。
    # 当要获取节点值时，用text方法。
    '''
    try:
        import xml.etree.cElementTree as ET
    except ImportError:
        import xml.etree.ElementTree as ET

    root = ET.parse(source_file)
    collection = root.findall('./movie')
    # print(collection)
    for node in collection:  # 找出movie节点
        # print(node.attrib) # 数据字典
        # print("Title:", node.attrib["title"])
        print('Title:', node.get('title'))
        # 找出movie节点的子节点
        for child in node.getchildren():
            print(child.tag, ':', child.text)
        print('#' * 20)


def call_xml_sax(source_file):
    import xml.sax

    class MovieHandler(xml.sax.ContentHandler):
        def __init__(self):
            self.CurrentData = ""
            self.type = ""
            self.format = ""
            self.year = ""
            self.rating = ""
            self.stars = ""
            self.description = ""

        # 元素开始事件处理
        def startElement(self, tag, attributes):
            self.CurrentData = tag
            if tag == "movie":
                print("*****Movie*****")
                title = attributes["title"]
                print("Title:", title)

        # 元素结束事件处理
        def endElement(self, tag):
            if self.CurrentData == "type":
                print("Type:", self.type)
            elif self.CurrentData == "format":
                print("Format:", self.format)
            elif self.CurrentData == "year":
                print("Year:", self.year)
            elif self.CurrentData == "rating":
                print("Rating:", self.rating)
            elif self.CurrentData == "stars":
                print("Stars:", self.stars)
            elif self.CurrentData == "description":
                print("Description:", self.description)
            self.CurrentData = ""

        # 内容事件处理
        def characters(self, content):
            if self.CurrentData == "type":
                self.type = content
            elif self.CurrentData == "format":
                self.format = content
            elif self.CurrentData == "year":
                self.year = content
            elif self.CurrentData == "rating":
                self.rating = content
            elif self.CurrentData == "stars":
                self.stars = content
            elif self.CurrentData == "description":
                self.description = content

    # 创建一个 XMLReader
    parser = xml.sax.make_parser()
    # turn off namepsaces
    parser.setFeature(xml.sax.handler.feature_namespaces, 0)

    # 重写 ContextHandler
    Handler = MovieHandler()
    parser.setContentHandler(Handler)

    parser.parse(source_file)


def main():
    source_file = os.path.join(r"..\..\..\..\..\data_analysis\file_analysis\xml", "1.xml")
    dest_file = "2.xml"
    dict_choice = {
        "1": "call_xml_dom(source_file)",
        "2": "call_xml_etree(source_file)",
        "3": "call_xml_sax(source_file)",
        "4": "create_xml(dest_file)",
    }

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
