#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.liaoxuefeng.com/wiki/1016959663602400/1017970488768640
# asyncio是Python 3.4版本引入的标准库，直接内置了对异步IO的支持。
# asyncio的编程模型就是一个消息循环。我们从asyncio模块中直接获取一个EventLoop的引用，
# 然后把需要执行的协程扔到EventLoop中执行，就实现了异步IO。
# https://blog.csdn.net/SL_World/article/details/86597738
# https://blog.csdn.net/SL_World/article/details/86691747
# https://segmentfault.com/a/1190000008814676

import asyncio


# 用asyncio实现Hello world代码如下：
def test_1():
    @asyncio.coroutine
    def hello():
        print("Hello world!")
        # 异步调用asyncio.sleep(1):
        r = yield from asyncio.sleep(1)
        print("Hello again!")

    # 获取EventLoop:
    loop = asyncio.get_event_loop()
    # 执行coroutine
    loop.run_until_complete(hello())
    loop.close()


def test_2():
    import threading

    @asyncio.coroutine
    def hello():
        print('Hello world! (%s)' % threading.currentThread())
        yield from asyncio.sleep(1)
        print('Hello again! (%s)' % threading.currentThread())

    loop = asyncio.get_event_loop()
    tasks = [hello(), hello()]
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()
    # 由打印的当前线程名称可以看出，两个coroutine是由同一个线程并发执行的。
    # 如果把asyncio.sleep()换成真正的IO操作，则多个coroutine就可以由一个线程并发执行。


def test_3():
    @asyncio.coroutine
    def wget(host):
        print('wget %s...' % host)
        connect = asyncio.open_connection(host, 80)
        reader, writer = yield from connect
        header = 'GET / HTTP/1.0\r\nHost: %s\r\n\r\n' % host
        writer.write(header.encode('utf-8'))
        yield from writer.drain()
        while True:
            line = yield from reader.readline()
            if line == b'\r\n':
                break
            print('%s header > %s' % (host, line.decode('utf-8').rstrip()))
        # Ignore the body, close the socket
        writer.close()

    loop = asyncio.get_event_loop()
    tasks = [wget(host) for host in ['www.sina.com.cn', 'www.sohu.com', 'www.163.com']]
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()


'''原文：https://blog.csdn.net/SL_World/article/details/86597738 
【使用方法】： 
@asyncio.coroutine装饰器是协程函数的标志，我们需要在每一个任务函数前加这个装饰器，并在函数中使用yield from。
在同步IO任务的代码中使用的time.sleep(2)来假设任务执行了2秒。
但在协程中yield from后面必须是子生成器函数，而time.sleep()并不是生成器，所以这里需要使用内置模块提供的生成器函数asyncio.sleep()。
【功能】：
通过使用协程，极大增加了多任务执行效率，最后消耗的时间是任务队列中耗时最多的时间。上述例子中的总耗时3秒就是taskIO_2()的耗时时间。
【执行过程】：
上面代码先通过get_event_loop()获取了一个标准事件循环loop(因为是一个，所以协程是单线程)
然后，我们通过run_until_complete(main())来运行协程(此处把调用方协程main()作为参数，调用方负责调用其他委托生成器)，
run_until_complete的特点就像该函数的名字，直到循环事件的所有事件都处理完才能完整结束。
进入调用方协程，我们把多个任务[taskIO_1()和taskIO_2()]放到一个task列表中，可理解为打包任务。
现在，我们使用asyncio.wait(tasks)来获取一个awaitable objects即可等待对象的集合(此处的aws是协程的列表)，并发运行传入的aws，
同时通过yield from返回一个包含(done, pending)的元组，done表示已完成的任务列表，pending表示未完成的任务列表；
如果使用asyncio.as_completed(tasks)则会按完成顺序生成协程的迭代器(常用于for循环中)，因此当你用它迭代时，会尽快得到每个可用的结果。
【此外，当轮询到某个事件时(如taskIO_1())，直到遇到该任务中的yield from中断，开始处理下一个事件(如taskIO_2()))，当yield from后面的子生成器完成任务时，该事件才再次被唤醒】
因为done里面有我们需要的返回结果，但它目前还是个任务列表，所以要取出返回的结果值，我们遍历它并逐个调用result()取出结果即可。
(注：对于asyncio.wait()和asyncio.as_completed()返回的结果均是先完成的任务结果排在前面，
所以此时打印出的结果不一定和原始顺序相同，但使用gather()的话可以得到原始顺序的结果集，
两者更详细的案例说明见此:https://blog.csdn.net/SL_World/article/details/86691747)
最后我们通过loop.close()关闭事件循环。
综上所述：协程的完整实现是靠①事件循环＋②协程。
'''


def test_4():
    # 使用同步方式编写异步功能
    import time

    @asyncio.coroutine  # 标志协程的装饰器
    def taskIO_1():
        print('开始运行IO任务1...')
        yield from asyncio.sleep(2)  # 假设该任务耗时2s
        print('IO任务1已完成，耗时2s')
        return taskIO_1.__name__

    @asyncio.coroutine  # 标志协程的装饰器
    def taskIO_2():
        print('开始运行IO任务2...')
        yield from asyncio.sleep(3)  # 假设该任务耗时3s
        print('IO任务2已完成，耗时3s')
        return taskIO_2.__name__

    @asyncio.coroutine  # 标志协程的装饰器
    def main():  # 调用方
        tasks = [taskIO_1(), taskIO_2()]  # 把所有任务添加到task中
        # done表示已完成的任务列表，pending表示未完成的任务列表；
        done, pending = yield from asyncio.wait(tasks)  # 子生成器
        for r in done:  # done和pending都是一个任务，所以返回结果需要逐个调用result()
            print('协程无序返回值：' + r.result())

    start = time.time()
    loop = asyncio.get_event_loop()  # 创建一个事件循环对象loop
    try:
        loop.run_until_complete(main())  # 完成事件循环，直到最后一个任务结束
    finally:
        loop.close()  # 结束事件循环
    print('所有IO任务总耗时%.5f秒' % float(time.time() - start))


# 在Python 3.5开始引入了新的语法async和await，以简化并更好地标识异步IO。要使用新的语法，只需要做两步简单的替换：
# 把@asyncio.coroutine替换为async；
# 把yield from替换为await。
def test_5():
    import time

    async def taskIO_1():
        print('开始运行IO任务1...')
        await asyncio.sleep(2)  # 假设该任务耗时2s
        print('IO任务1已完成，耗时2s')
        return taskIO_1.__name__

    async def taskIO_2():
        print('开始运行IO任务2...')
        await asyncio.sleep(3)  # 假设该任务耗时3s
        print('IO任务2已完成，耗时3s')
        return taskIO_2.__name__

    async def main():  # 调用方
        tasks = [taskIO_1(), taskIO_2()]  # 把所有任务添加到task中
        done, pending = await asyncio.wait(tasks)  # 子生成器
        for r in done:  # done和pending都是一个任务，所以返回结果需要逐个调用result()
            print('协程无序返回值：' + r.result())

    start = time.time()
    loop = asyncio.get_event_loop()  # 创建一个事件循环对象loop
    try:
        loop.run_until_complete(main())  # 完成事件循环，直到最后一个任务结束
    finally:
        loop.close()  # 结束事件循环
    print('所有IO任务总耗时%.5f秒' % float(time.time() - start))


# 如果你只关心协程并发运行后的结果集合，可以使用gather()，它不仅通过await返回仅一个结果集，而且这个结果集的结果顺序是传入任务的原始顺序。
def test_6():
    import time

    async def taskIO_1():
        print('开始运行IO任务1...')
        await asyncio.sleep(3)  # 假设该任务耗时3s
        print('IO任务1已完成，耗时3s')
        return taskIO_1.__name__

    async def taskIO_2():
        print('开始运行IO任务2...')
        await asyncio.sleep(2)  # 假设该任务耗时2s
        print('IO任务2已完成，耗时2s')
        return taskIO_2.__name__

    async def main():  # 调用方
        tasks = [taskIO_1(), taskIO_2()]  # 把所有任务添加到task中
        resualts = await asyncio.gather(tasks)  # 子生成器
        print(resualts)

    start = time.time()
    loop = asyncio.get_event_loop()  # 创建一个事件循环对象loop
    try:
        loop.run_until_complete(main())  # 完成事件循环，直到最后一个任务结束
    finally:
        loop.close()  # 结束事件循环
    print('所有IO任务总耗时%.5f秒' % float(time.time() - start))


# 使用as_completed(tasks)和wait(tasks)相同之处是返回结果的顺序是协程的完成顺序，这与gather()恰好相反。
# 而不同之处是as_completed(tasks)可以实时返回当前完成的结果，而wait(tasks)需要等待所有协程结束后返回的done去获得结果。
def test_7():
    import time

    async def taskIO_1():
        print('开始运行IO任务1...')
        await asyncio.sleep(3)  # 假设该任务耗时3s
        print('IO任务1已完成，耗时3s')
        return taskIO_1.__name__

    async def taskIO_2():
        print('开始运行IO任务2...')
        await asyncio.sleep(2)  # 假设该任务耗时2s
        print('IO任务2已完成，耗时2s')
        return taskIO_2.__name__

    async def main():  # 调用方
        tasks = [taskIO_1(), taskIO_2()]  # 把所有任务添加到task中
        for completed_task in asyncio.as_completed(tasks):
            resualt = await completed_task  # 子生成器
            print('协程无序返回值：' + resualt)

    start = time.time()
    loop = asyncio.get_event_loop()  # 创建一个事件循环对象loop
    try:
        loop.run_until_complete(main())  # 完成事件循环，直到最后一个任务结束
    finally:
        loop.close()  # 结束事件循环
    # taskIO_1 是一个协程函数，可以通过 asyncio.iscoroutinefunction 来验证：
    print(asyncio.iscoroutinefunction(taskIO_1))  # True
    print('所有IO任务总耗时%.5f秒' % float(time.time() - start))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
