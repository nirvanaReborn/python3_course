#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://germey.gitbooks.io/python3webspider/content/3.1-%E4%BD%BF%E7%94%A8Urllib.html
# 它包含四个模块：
# 第一个模块 request，它是最基本的 HTTP 请求模块，我们可以用它来模拟发送一请求，就像在浏览器里输入网址然后敲击回车一样，只需要给库方法传入 URL 还有额外的参数，就可以模拟实现这个过程了。
# 第二个模块 error 模块即异常处理模块，如果出现请求错误，我们可以捕获这些异常，然后进行重试或其他操作保证程序不会意外终止。
# 第三个模块 parse 是一个工具模块，提供了许多 URL 处理方法，比如拆分、解析、合并等等的方法。
# 第四个模块 robotparser，主要是用来识别网站的 robots.txt 文件，然后判断哪些网站可以爬，哪些网站不可以爬的，其实用的比较少。

# 在urllib里面 判断是get请求还是post请求，就是判断是否提交了data参数
# POST请求可以添加 data参数，要通过 bytes() 方法可以进行转化。

# https://www.cnblogs.com/xiao-apple36/p/8433400.html

import urllib
import urllib.error
import urllib.parse
import urllib.request
import urllib.robotparser

from public_function import GLOBAL_WORK_DIR


def test_help():
    help(urllib)


def test_dir():
    for i in dir(urllib):
        print(i)


# urllib.request 模块提供了最基本的构造 HTTP 请求的方法，利用它可以模拟浏览器的一个请求发起过程，
# 同时它还带有处理authenticaton（授权验证），redirections（重定向)，cookies（浏览器Cookies）以及其它内容。
def test_1():
    response = urllib.request.urlopen('https://www.python.org')
    print(type(response))  # <class 'http.client.HTTPResponse'>
    # 通过输出结果可以发现它是一个 HTTPResposne 类型的对象，它主要包含的方法有 read()、readinto()、getheader(name)、getheaders()、info() 等方法
    # 和 msg、version、status、reason、debuglevel、closed 等属性。
    print(response.read().decode('utf-8'))
    # print(response.readinto())
    print(response.getheader('Server'))
    print(response.getheaders())
    print(response.info())
    print(response.getcode())
    print(response.geturl())
    print(response.msg)
    print(response.version)
    print(response.status)
    print(response.reason)
    print(response.debuglevel)
    print(response.closed)


# data 参数是可选的，如果要添加 data，它要是字节流编码格式的内容，即 bytes 类型，通过 bytes() 方法可以进行转化，
# 另外如果传递了这个 data 参数，它的请求方式就不再是 GET 方式请求，而是 POST。
def test_2():
    data = bytes(urllib.parse.urlencode({'word': 'hello'}), encoding='utf8')
    response = urllib.request.urlopen('http://httpbin.org/post', data=data)
    print(response.read())


# timeout 参数可以设置超时时间，单位为秒，意思就是如果请求超出了设置的这个时间还没有得到响应，就会抛出异常，
# 如果不指定，就会使用全局默认时间。它支持 HTTP、HTTPS、FTP 请求。
def test_3():
    response = urllib.request.urlopen('http://httpbin.org/get', timeout=1)
    print(response.read())


def test_4():
    import socket

    try:
        response = urllib.request.urlopen('http://httpbin.org/get', timeout=0.01)
    except urllib.error.URLError as e:
        print(e.reason, type(e.reason))  # <class 'socket.timeout'>
        if isinstance(e.reason, socket.timeout):
            print('TIMED OUT')


# 还有 context 参数，它必须是 ssl.SSLContext 类型，用来指定 SSL 设置。
def test_5():
    import ssl
    # 解决某些环境下报<urlopen error [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed
    ssl._create_default_https_context = ssl._create_unverified_context
    url = 'https://www.jianshu.com'
    # 返回<http.client.HTTPResponse object at 0x0000000002E34550>
    response = urllib.request.urlopen(url, data=None, timeout=10)
    # 直接用urllib.request模块的urlopen()获取页面，page的数据格式为bytes类型，需要decode()解码，转换成str类型。
    page = response.read().decode('utf-8')
    print(page)


# cafile 和 capath 两个参数是指定 CA 证书和它的路径，这个在请求 HTTPS 链接时会有用。
def test_6():
    pass


# cadefault 参数现在已经弃用了，默认为 False。
# 以上讲解了 urlopen() 方法的用法，通过这个最基本的函数可以完成简单的请求和网页抓取，
# 如需更加详细了解，可以参见官方文档：https://docs.python.org/3/library/urllib.request.html。
# ----------------------------------------------------------------------------------------------------------------------------------------------------
# 由上我们知道利用 urlopen() 方法可以实现最基本请求的发起，但这几个简单的参数并不足以构建一个完整的请求，
# 如果请求中需要加入 Headers 等信息，我们就可以利用更强大的 Request 类来构建一个请求。
# class urllib.request.Request(url, data=None, headers={}, origin_req_host=None, unverifiable=False, method=None)
# 第一个 url 参数是请求 URL，这个是必传参数，其他的都是可选参数。
# 第二个 data 参数如果要传必须传 bytes（字节流）类型的，如果是一个字典，可以先用 urllib.parse 模块里的 urlencode() 编码。
# 第三个 headers 参数是一个字典，这个就是 Request Headers 了，你可以在构造 Request 时通过 headers 参数直接构造，也可以通过调用 Request 实例的 add_header() 方法来添加。
# 第四个 origin_req_host 参数指的是请求方的 host 名称或者 IP 地址。
# 第五个 unverifiable 参数指的是这个请求是否是无法验证的，默认是False。意思就是说用户没有足够权限来选择接收这个请求的结果。例如我们请求一个 HTML 文档中的图片，但是我们没有自动抓取图像的权限，这时 unverifiable 的值就是 True。
# 第六个 method 参数是一个字符串，它用来指示请求使用的方法，比如GET，POST，PUT等等。


# 模拟PC浏览器
def test_7():
    url = 'https://www.jianshu.com'
    # 增加header
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36'
    }
    request = urllib.request.Request(url, headers=headers)
    response = urllib.request.urlopen(request)
    # 在urllib里面 判断是get请求还是post请求，就是判断是否提交了data参数
    print(request.get_method())


# 模拟手机浏览器
def test_8():
    url = 'http://www.douban.com/'
    req = urllib.request.Request(url)
    req.add_header('User-Agent', 'Mozilla/6.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) '
                                 'AppleWebKit/536.26 (KHTML, like Gecko) Version/8.0 Mobile/10A5376e Safari/8536.25')
    with urllib.request.urlopen(req) as f:
        print('Status:', f.status, f.reason)
        for k, v in f.getheaders():
            print('%s: %s' % (k, v))
        print('Data:', f.read().decode('utf-8'))


# 有没有发现，在上面的过程中，我们虽然可以构造 Request，但是一些更高级的操作，比如 Cookies 处理，代理设置等操作我们该怎么办？
# 接下来就需要更强大的工具 Handler 登场了。
# 简而言之我们可以把它理解为各种处理器，有专门处理登录验证的，有处理 Cookies 的，有处理代理设置的，利用它们我们几乎可以做到任何 HTTP 请求中所有的事情。
# 首先介绍下 urllib.request 模块里的 BaseHandler类，它是所有其他 Handler 的父类，它提供了最基本的 Handler 的方法，例如 default_open()、protocol_request() 方法等。
# 接下来就有各种 Handler 子类继承这个 BaseHandler 类，举例几个如下：
# HTTPDefaultErrorHandler 用于处理 HTTP 响应错误，错误都会抛出 HTTPError 类型的异常。
# HTTPRedirectHandler 用于处理重定向。
# HTTPCookieProcessor 用于处理 Cookies。
# ProxyHandler 用于设置代理，默认代理为空。
# HTTPPasswordMgr 用于管理密码，它维护了用户名密码的表。
# HTTPBasicAuthHandler 用于管理认证，如果一个链接打开时需要认证，那么可以用它来解决认证问题。
# 另外还有其他的 Handler 类，在这不一一列举了，详情可以参考官方文档： https://docs.python.org/3/library/urllib.request.html#urllib.request.BaseHandler
# 有些网站在打开时它就弹出了一个框，直接提示你输入用户名和密码，认证成功之后才能查看页面.
# 那么我们如果要请求这样的页面怎么办呢？借助于 HTTPBasicAuthHandler 就可以完成.
def test_9():
    username = 'username'
    password = 'password'
    url = 'http://localhost:5000/'

    p = urllib.request.HTTPPasswordMgrWithDefaultRealm()
    p.add_password(None, url, username, password)
    auth_handler = urllib.request.HTTPBasicAuthHandler(p)
    opener = urllib.request.build_opener(auth_handler)

    try:
        result = opener.open(url)
        html = result.read().decode('utf-8')
        print(html)
    except urllib.error.URLError as e:
        print(e.reason)


# 设置代理
# 当需要抓取的网站设置了访问限制，这时就需要用到代理来抓取数据。
def test_10():
    url = 'http://httpbin.org/ip'
    proxy = {'http': '39.134.108.89:8080', 'https': '39.134.108.89:8080'}
    proxies = urllib.request.ProxyHandler(proxy)  # 创建代理处理器
    opener = urllib.request.build_opener(proxies, urllib.request.HTTPHandler)  # 创建特定的opener对象
    urllib.request.install_opener(opener)  # 安装全局的opener 把urlopen也变成特定的opener
    data = urllib.request.urlopen(url)
    print(data.read().decode())


# Cookie的使用
def test_11():
    import http.cookiejar

    url = 'http://www.douban.com/'
    # 1.创建CookieJar对象
    cookie = http.cookiejar.CookieJar()
    # 使用HTTPCookieProcessor创建cookie处理器，
    handler = urllib.request.HTTPCookieProcessor(cookie)
    # 构建opener对象
    opener = urllib.request.build_opener(handler)
    # 将opener安装为全局
    urllib.request.install_opener(opener)
    response = urllib.request.urlopen(url)
    for item in cookie:
        print(item.name + "=" + item.value)

    # 2.保存cookie为文本
    filename = "cookie.txt"
    # 保存类型有很多种
    ## 类型1
    # cookie = http.cookiejar.MozillaCookieJar(filename)
    ## 类型2
    cookie = http.cookiejar.LWPCookieJar(filename)
    cookie.save(ignore_discard=True, ignore_expires=True)

    # 3.使用相应的方法读取
    cookie = http.cookiejar.LWPCookieJar()
    cookie.load('cookie.txt', ignore_discard=True, ignore_expires=True)
    handler = urllib.request.HTTPCookieProcessor(cookie)
    opener = urllib.request.build_opener(handler)
    response = opener.open('http://www.baidu.com')
    print(response.read().decode('utf-8'))


# 直接将远程数据下载到本地
# urlretrieve(url, filename=None, reporthook=None, data=None)
# 参数 finename 指定了保存本地路径（如果参数未指定，urllib会生成一个临时文件保存数据。）
# 参数 reporthook 是一个回调函数，当连接上服务器、以及相应的数据块传输完毕时会触发该回调，我们可以利用这个回调函数来显示当前的下载进度。
# 参数 data 指 post 到服务器的数据，该方法返回一个包含两个元素的(filename, headers)元组，filename 表示保存到本地的路径，header 表示服务器的响应头。
def test_12():
    import os

    def schedule(a, b, c):
        '''
          a:已经下载的数据块
          b:数据块的大小
          c:远程文件的大小
        '''
        per = 100.0 * a * b / c
        if per > 100:
            per = 0
        print('%.2f%%' % per)

    def downloadImg(imglist, root_dir):
        i = 0
        for imgurl in imglist:
            i += 1
            # 直接将远程数据下载到本地
            local = os.path.join(root_dir, '%s.jpg' % i)
            # print (local)
            urllib.request.urlretrieve(imgurl, local, schedule)

    imglist = ['http://www.jb51.net/images/logo.gif']
    downloadImg(imglist, GLOBAL_WORK_DIR)


# ----------------------------------------------------------------------------------------------------------------------------------------------------
# urllib.error可以接收有urllib.request产生的异常。
# urllib.error中常用的有两个方法，URLError和HTTPError。
# URLError是OSError的一个子类，
# HTTPError是URLError的一个子类，
# 服务器上HTTP的响应会返回一个状态码，根据这个HTTP状态码，我们可以知道我们的访问是否成功。
def test_20():
    requset = urllib.request.Request('http://www.usahfkjashfj.com/')
    try:
        urllib.request.urlopen(requset).read()
    except urllib.error.URLError as e:
        print(e.reason)
    else:
        print('success')


# HTTPError还包含了三个属性：
# code：请求的状态码
# reason：错误的原因
# headers：响应的报头
def test_21():
    # 先捕获子类错误
    try:
        response = urllib.request.urlopen('http://cuiqingcai.com/index.htm')
    except urllib.error.HTTPError as e:
        print(e.reason, e.code, e.headers, sep='\n')
    except urllib.error.URLError as e:
        print(e.reason)
    else:
        print('Successfully')


# ----------------------------------------------------------------------------------------------------------------------------------------------------
# 1. urlparse()--可以实现 URL 的识别和分段
# urllib.parse.urlparse(urlstring, scheme='', allow_fragments=True)
# 参数urlstring，是必填项，即待解析的 URL。
# 参数scheme，是默认的协议（比如http、https等），假如这个链接没有带协议信息，会将这个作为默认的协议。
# 参数allow_fragments，即是否忽略 fragment，如果它被设置为 False，fragment 部分就会被忽略，它会被解析为 path、parameters 或者 query 的一部分，fragment 部分为空。
def test_31():
    result = urllib.parse.urlparse('http://www.baidu.com/index.html;user?id=5#comment')
    print(type(result), result)
    # 返回结果是一个 ParseResult 类型的对象，它包含了六个部分，分别是 scheme、netloc、path、params、query、fragment。


# urlunparse()
# 它接受的参数是一个可迭代对象，但是它的长度必须是 6，否则会抛出参数数量不足或者过多的问题。
def test_32():
    data = ['http', 'www.baidu.com', 'index.html', 'user', 'a=6', 'comment']
    print(urllib.parse.urlunparse(data))


# 3. urlsplit()
def test_33():
    result = urllib.parse.urlsplit('http://www.baidu.com/index.html;user?id=5#comment')
    print(result)
    print(result.scheme, result[0])


# 4. urlunsplit()
# 将链接的各个部分组合成完整链接的方法，传入的也是一个可迭代对象，例如列表、元组等等，唯一的区别是，长度必须为 5。
def test_34():
    data = ['http', 'www.baidu.com', 'index.html', 'a=6', 'comment']
    print(urllib.parse.urlunsplit(data))


# 5. urljoin()
# 基于一个base URL和另一个URL构造一个绝对URL。注意：url必须为一致站点,否则后面参数会覆盖前面的host。
def test_35():
    # 拼接url
    print(urllib.parse.urljoin('https://www.jianshu.com/xyz', 'FAQ.html'))
    print(urllib.parse.urljoin('http://www.baidu.com/about.html', 'http://www.baidu.com/FAQ.html'))


# 6. urlencode()--将其序列化为 URL 标准 GET 请求参数。
def test_36():
    params = {
        'name': 'germey',
        'age': 22
    }
    base_url = 'http://www.baidu.com?'
    url = base_url + urllib.parse.urlencode(params)
    print(url)

    params = {'wd': '南北', 'code': '1', 'height': '188'}
    print(urllib.parse.urlencode(params))


# 7. parse_qs()
# 有了序列化必然就有反序列化，如果我们有一串 GET 请求参数，我们利用 parse_qs() 方法就可以将它转回字典
def test_37():
    query = 'name=germey&age=22'
    print(urllib.parse.parse_qs(query))


# 8. parse_qsl()--可以将参数转化为元组组成的列表
def test_38():
    query = 'name=germey&age=22'
    print(urllib.parse.parse_qsl(query))


# 9. quote()--将内容转化为 URL 编码的格式，有时候 URL 中带有中文参数的时候可能导致乱码的问题，所以我们可以用这个方法将中文字符转化为 URL 编码。
# 10. unquote()--进行 URL 解码
# 按照标准， URL 只允许一部分 ASCII 字符（数字字母和部分符号），其他的字符（如汉字）是不符合 URL 标准的。
# 所以 URL 中使用其他字符就需要进行 URL 编码。
# URL 中传参数的部分（query String），格式是：name1=value1&name2=value2&name3=value3
# 假如你的 name 或者 value 值中有『&』或者『=』等符号，就当然会有问题。
# 所以URL中的参数字符串也需要把『&=』等符号进行编码。
# URL编码的方式是把需要编码的字符转化为 %xx 的形式。
# 通常 URL 编码是基于 UTF-8 的（当然这和浏览器平台有关）。
# Url的编码格式采用的是ASCII码，而不是Unicode。
def test_39():
    import time

    fmt = "%a %b %d %Y %H:%M:%S GMT%z"
    t3 = time.strftime(fmt, time.localtime())
    print(t3)  # Sat Sep 16 2017 15:06:19 GMT+0800

    # 进行URL编码
    mytime = t3 + '中国标准时间'
    url_values = urllib.parse.quote(mytime, safe='+')
    print(url_values)
    # Sat%20Sep%2016%202017%2015%3A06%3A19%20GMT+0800%E4%B8%AD%E5%9B%BD%E6%A0%87%E5%87%86%E6%97%B6%E9%97%B4

    # 进行URL解码
    url_values = urllib.parse.unquote('%E4%B8%AD%E5%9B%BD%E6%A0%87%E5%87%86%E6%97%B6%E9%97%B4')
    print(url_values)  # 中国标准时间

    x = urllib.parse.quote('山西', encoding='gb18030')  # encoding='GBK
    print(x)  # %C9%BD%CE%F7

    city = urllib.parse.unquote('%E5%B1%B1%E8%A5%BF', )  # encoding='utf-8'
    print(city)  # 山西


# ----------------------------------------------------------------------------------------------------------------------------------------------------
# Robots 协议也被称作爬虫协议、机器人协议，它的全名叫做网络爬虫排除标准（Robots Exclusion Protocol），
# 用来告诉爬虫和搜索引擎哪些页面可以抓取，哪些不可以抓取。它通常是一个叫做 robots.txt 的文本文件，放在网站的根目录下。
# 有常用的几个方法分别介绍一下：
# set_url()，用来设置 robots.txt 文件的链接。如果已经在创建 RobotFileParser 对象时传入了链接，那就不需要再使用这个方法设置了。
# read()，读取 robots.txt 文件并进行分析，注意这个函数是执行一个读取和分析操作，如果不调用这个方法，接下来的判断都会为 False，所以一定记得调用这个方法，这个方法不会返回任何内容，但是执行了读取操作。
# parse()，用来解析 robots.txt 文件，传入的参数是 robots.txt 某些行的内容，它会按照 robots.txt 的语法规则来分析这些内容。
# can_fetch()，方法传入两个参数，第一个是 User-agent，第二个是要抓取的 URL，返回的内容是该搜索引擎是否可以抓取这个 URL，返回结果是 True 或 False。
# mtime()，返回的是上次抓取和分析 robots.txt 的时间，这个对于长时间分析和抓取的搜索爬虫是很有必要的，你可能需要定期检查来抓取最新的 robots.txt。
# modified()，同样的对于长时间分析和抓取的搜索爬虫很有帮助，将当前时间设置为上次抓取和分析 robots.txt 的时间。


def test_40():
    url = 'http://www.jianshu.com/robots.txt'
    rp = urllib.robotparser.RobotFileParser(url)
    rp.read()
    print(rp.can_fetch('*', 'http://www.jianshu.com/p/b67554025d7d'))
    print(rp.can_fetch('*', "http://www.jianshu.com/search?q=python&page=1&type=collections"))


def test_41():
    rp = urllib.robotparser.RobotFileParser()
    rp.parse(urllib.request.urlopen('http://www.jianshu.com/robots.txt').read().decode('utf-8').split('\n'))
    print(rp.can_fetch('*', 'http://www.jianshu.com/p/b67554025d7d'))
    print(rp.can_fetch('*', "http://www.jianshu.com/search?q=python&page=1&type=collections"))


# -------------------------------------------------------------------
def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
