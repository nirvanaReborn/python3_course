#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
#
from enum import Enum, unique


# 这行unique装饰器可以让枚举类中不能有重复值
# @unique
class TradeThreadMark(Enum):
    DEFAULT = 0
    ONCE_HANDLE = 1
    TICK_DATA = 2


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
