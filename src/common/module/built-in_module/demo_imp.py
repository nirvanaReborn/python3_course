#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/leviopku/article/details/102739686
# 让引用模块更加简单
# imp模块被弃用而支持importlib
# 和import类似，当运行一遍以后，会在当前路径生成一个__pycache__文件记录引用关系，以保证再次运行时可以加速。
# pip install imp
import imp


def test_1():
    # 第一个参数为命名，重要的是第二个参数指定引用函数路径就行了。
    the_func = imp.load_source('lol', './func.py')
    # 第一个参数为命名，重要的是第二个参数指定引用函数路径就行了。
    the_func.print_text()


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
