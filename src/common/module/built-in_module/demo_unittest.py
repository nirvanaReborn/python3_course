#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# https://www.cnblogs.com/lsdb/p/10444943.html
# 标准库中有两个测试包: unittest 和 doctest。
# http://www.pythondoc.com/pythontutorial3/stdlib.html
# unittest 模块不像 doctest 模块那么容易使用，不过它可以在一个独立的文件里提供一个更全面的测试集:


import unittest


def test_1():
    import numpy
    class TestStatisticalFunctions(unittest.TestCase):
        def test_average(self):
            self.assertEqual(numpy.average([20, 30, 70]), 40.0)
            self.assertEqual(round(numpy.average([1, 5, 7]), 1), 4.3)
            with self.assertRaises(ZeroDivisionError):
                numpy.average([])
            with self.assertRaises(TypeError):
                numpy.average(20, 30, 70)

    unittest.main()  # Calling from the choice line invokes all tests


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
