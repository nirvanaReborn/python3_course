#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://www.pythondoc.com/pythontutorial3/stdlib.html
# 操作系统接口
# shutil 模块是python 标准库中提供的，主要用来做文件和文件夹的拷贝、移动、删除等；还可以做文件和文件夹的压缩、解压缩操作。
# os 模块提供了对目录或文件的一般操作。shutil 模块作为补充，提供了移动、复制、压缩、解压等操作，这些os 模块都没有提供。

'''
shutil.copyfile(src, dst)
拷贝文件

# shutil.copyfileobj(fsrc, fdst, length=16*1024)
# 将文件内容拷贝到另一个文件中，可以部分内容

shutil.copymode(src, dst)
仅拷贝权限。内容、组、用户均不变

shutil.copystat(src, dst)
拷贝状态的信息，包括：mode bits, atime, mtime, flags

shutil.copy(src, dst)
拷贝文件和权限

shutil.copy2(src, dst)
拷贝文件和状态信息

shutil.ignore_patterns(*patterns)
shutil.copytree(src, dst, symlinks=False, ignore=None)
递归的去拷贝文件
例如：copytree(source, destination, ignore=ignore_patterns('*.pyc', 'tmp*'))

shutil.rmtree(path[, ignore_errors[, onerror]])
递归的去删除文件

shutil.move(src, dst)
递归的去移动文件


'''

import shutil
import os


def test_help():
    help(shutil)


def test_dir():
    for i in dir(shutil):
        print(i)


# 针对日常的文件和目录管理任务，shutil 模块提供了一个易于使用的高级接口:
def test_1():
    old_file = r'datetime_test.py'
    new_file = r'10_datetime_test.py'
    if os.path.exists(new_file):
        # 如果目标路径存在原文件夹的话就先删除
        shutil.rmtree(new_file)
        print("删除已存在的文件或文件夹:", new_file)

    if os.path.isdir(old_file):
        shutil.copytree(old_file, new_file)
        print("拷贝文件夹:", old_file, "--->", new_file)
    else:
        shutil.copyfile(old_file, new_file)
        print("拷贝文件:", old_file, "--->", new_file)

    shutil.move('/build/executables', 'installdir')


# 创建压缩包并返回文件路径，例如：zip、tar
# shutil.make_archive(base_name, format,...)
# 参数：
#     base_name： 压缩包的文件名，也可以是压缩包的路径。只是文件名时，则保存至当前目录，否则保存至指定路径，
#                 如：www                        =>保存至当前路径
#                 如：/Users/wupeiqi/www =>保存至/Users/wupeiqi/
#     format： 压缩包种类，“zip”, “tar”, “bztar”，“gztar”
#     root_dir： 要压缩的文件夹路径（默认当前目录）
#     owner： 用户，默认当前用户
#     group： 组，默认当前组
#     logger： 用于记录日志，通常是logging.Logger对象

# shutil 对压缩包的处理是调用 ZipFile 和 TarFile 两个模块来进行的
def test_2():
    # 将 /Users/wupeiqi/Downloads/test 下的文件打包放置当前程序目录
    ret = shutil.make_archive("wwwwwwwwww", 'gztar', root_dir='/Users/wupeiqi/Downloads/test')

    # 将 /Users/wupeiqi/Downloads/test 下的文件打包放置 /Users/wupeiqi/目录
    ret = shutil.make_archive("/Users/wupeiqi/wwwwwwwwww", 'gztar', root_dir='/Users/wupeiqi/Downloads/test')


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
