#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-json.html
# Python3 JSON 数据解析
'''
json 类型转换到 python 的类型对照表：

JSON	        Python
object	        dict
array	        list
string	        unicode
number (int)	int, long
number (real)	float
true	        True
false	        False
null	        None


1、json.dumps()和json.loads()是json格式处理函数（可以这么理解，json是字符串），针对内存对象：
　　(1)json.dumps()函数是将一个Python数据类型列表进行json格式的编码（可以这么理解，json.dumps()函数是将字典转化为字符串）
　　(2)json.loads()函数是将json格式数据转换为字典（可以这么理解，json.loads()函数是将字符串转化为字典）

2、json.dump()和json.load()针对文件句柄，主要用来读写json文件函数。
　　(1)json.dump()是将python数据保存成json文件
　　(2)json.load()是读取json文件
'''

try:
    import json
except ImportError:
    import simplejson as json

import sys


# 保存成json格式文件
def test_1(data):
    with open('data.json', 'w') as fw:
        json.dump(data, fw)


# 读取json格式文件
def test_2():
    with open('data.json', 'r') as fr:
        data = json.load(fr)
    return data


# json.dumps()函数的使用，将字典转化为字符串
def test_3():
    dict1 = {"age": "12"}
    json_info = json.dumps(dict1)
    print("dict1的类型：" + str(type(dict1)))  # <class 'dict'>
    print("通过json.dumps()函数处理：")
    print("json_info的类型：" + str(type(json_info)))  # <class 'str'>


# json.loads()函数的使用，将字符串转化为字典
def test_4():
    json_info = '{"age": "12"}'
    dict1 = json.loads(json_info)
    print("json_info的类型：" + str(type(json_info)))  # <class 'str'>
    print("通过json.dumps()函数处理：")
    print("dict1的类型：" + str(type(dict1)))  # <class 'dict'>


def test_5():
    data1 = {
        'no': 1,
        'name': 'Runoob',
        'url': 'http://www.runoob.com'
    }

    # Python 字典类型转换为 JSON 对象
    json_str = json.dumps(data1)
    print("Python 原始数据：", repr(data1))
    print("JSON 对象：", json_str)

    # 将 JSON 对象转换为 Python 字典
    data2 = json.loads(json_str)
    print("data2['name']: ", data2['name'])
    print("data2['url']: ", data2['url'])


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
