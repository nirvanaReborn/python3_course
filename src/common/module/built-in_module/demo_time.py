#!/usr/bin/env python
# -*- coding: UTF-8 -*-

'''python中时间日期格式化符号：
%y 两位数的年份表示（00-99）
%Y 四位数的年份表示（000-9999）
%m 月份（01-12）
%d 月内中的一天（0-31）
%H 24小时制小时数（0-23）
%I 12小时制小时数（01-12）
%M 分钟数（00=59）
%S 秒（00-59）

%a 本地简化星期名称
%A 本地完整星期名称
%b 本地简化的月份名称
%B 本地完整的月份名称
%c 本地相应的日期表示和时间表示
%j 年内的一天（001-366）
%p 本地A.M.或P.M.的等价符
%U 一年中的星期数（00-53）星期天为星期的开始
%w 星期（0-6），星期天为星期的开始
%W 一年中的星期数（00-53）星期一为星期的开始
%x 本地相应的日期表示
%X 本地相应的时间表示
%Z 当前时区的名称
%% %号本身
'''

import time
import sys


def test_help():
    help(time)


def test_dir():
    for i in dir(time):
        print(i)


def test_1():
    print(time.time())  # 1591435511.8601482  时间戳 1970年1月1日之后的秒
    print(time.mktime(time.localtime()))  # 1591435511.0

    print(time.gmtime())  # 可加时间戳参数
    print(time.localtime())  # 可加时间戳参数
    print(time.strptime('2014-11-11', '%Y-%m-%d'))
    # time.struct_time(tm_year=2014, tm_mon=11, tm_mday=11, tm_hour=0, tm_min=0, tm_sec=0, tm_wday=1, tm_yday=315, tm_isdst=-1)

    print(time.ctime())  # Sat Jun  6 17:25:11 2020
    print(time.ctime(time.time()))  # Sat Jun  6 17:25:11 2020
    print(time.asctime())  # Sat Jun  6 17:25:11 2020
    print(time.asctime(time.localtime()))  # Sat Jun  6 17:25:11 2020
    print(time.strftime("%Y%m%d %H:%M:%S", time.localtime()))


def test_2():
    print(time.strftime("%a %b %d %Y %H:%M:%S GMT%z", time.localtime()))  # Sat Jun 06 2020 17:39:04 GMT+0800
    print(time.strftime("%Y-%m-%d %H:%M:%S"))  # 2020-06-06 17:39:04
    print(time.strftime("%Y%m%d"))  # 20200606
    print(time.strftime("%H%M%S"))  # 173904

    print(time.strftime('%a %b', time.localtime()))
    print(time.strftime('%c', time.localtime()))
    print(time.strftime('%A', time.localtime()))  # Saturday
    print(time.strftime('%B', time.localtime()))  # June
    print(time.strftime('%D', time.localtime()))  # 06/06/20
    print(time.strftime('%y', time.localtime()))  # 20
    print(time.strftime('%Z', time.localtime()))  # ?D1¨²¡À¨º¡Á?¨º¡À??
    print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()))  # 2020-06-06 17:39:04
    print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(1234567890)))  # 2009-02-14 07:31:30 将秒转换为GMT日期


def test_4():
    begin_time = time.ctime()
    # print(begin_time)  # Sat Sep 16 14:10:29 2017
    time.sleep(1)
    end_time = time.ctime()
    print(end_time)  # Sat Sep 16 14:10:30 2017


def test_5():
    # 以毫秒为单位获取当前时间
    milliseconds = int(round(time.time() * 1000))
    print(milliseconds)  # 1591435740397
    print(time.gmtime(milliseconds))


def test_6():
    time_stamp = time.time()
    struct_time = time.gmtime(time_stamp)  # 逆函数 time.mktime
    # struct_time = time.localtime(time_stamp)
    str_time = time.strftime("%Y-%m-%d %H:%M:%S", struct_time)  # 逆函数 time.strptime
    struct_time_2 = time.strptime(str_time, "%Y-%m-%d %H:%M:%S")
    time_stamp_2 = time.mktime(struct_time_2)


def test_7():
    time_stamp = time.strftime("%Y%m%d_%H%M%S")
    print(time_stamp)


def test_8():
    def _convert_int_to_date(dt_int):
        import datetime
        year, r = divmod(dt_int, 10000)
        month, day = divmod(r, 100)
        return datetime.datetime(year, month, day)

    def convert_int_to_date(dt_int):
        dt_int = int(dt_int)
        if dt_int > 100000000:
            dt_int //= 1000000
        return _convert_int_to_date(dt_int)

    now_date = time.strftime("%Y%m%d")
    print(convert_int_to_date(now_date))


def test_9():
    # time_ns是python3.7版本之后才支持的方法
    if sys.version_info >= (3, 7):
        begin_time = time.time_ns()
        time.sleep(0.001)
        end_time = time.time_ns()
    else:
        begin_time = time.time()
        time.sleep(0.001)
        end_time = time.time()

    take_time = end_time-begin_time
    print(take_time / 1000000000, 's')


def test_10():
    # Python 3.8 已移除 clock() 方法 可以使用 time.perf_counter() 或 time.process_time() 方法替代。
    if sys.version_info >= (3, 8):
        begin_time = time.perf_counter()
        time.sleep(0.001)
        end_time = time.perf_counter()
    else:
        begin_time = time.clock()
        time.sleep(0.001)
        end_time = time.clock()

    take_time = end_time-begin_time
    print(take_time, 's')


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
