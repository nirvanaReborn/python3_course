#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# os 提供操作系统的接口，常用的有文件系统相关和进程相关
# sys 提供python解释器系统的通用配置和函数，影响着解释器的行为
# platform 提供平台相关的信息


import platform


def test_help():
    help(platform)


def test_dir():
    for i in dir(platform):
        print(i)


def get_platform_python_info():
    '''
    python中，platform模块给我们提供了很多方法去获取操作系统的信息,
    还可以获得计算机中python的一些信息：
        platform.python_build()
        platform.python_compiler()
        platform.python_branch()
        platform.python_implementation()
        platform.python_revision()
        platform.python_version()
        platform.python_version_tuple()
    '''

    def get_python_build():
        ''' the Python build number and date as strings'''
        return platform.python_build()

    def get_python_compiler():
        '''Returns a string identifying the compiler used for compiling Python'''
        return platform.python_compiler()

    def get_python_branch():
        '''Returns a string identifying the Python implementation SCM branch'''
        return platform.python_branch()

    def get_python_implementation():
        '''Returns a string identifying the Python implementation. Possible return values are: ‘CPython’, ‘IronPython’, ‘Jython’, ‘PyPy’.'''
        return platform.python_implementation()

    def get_python_version():
        '''Returns the Python version as string 'major.minor.patchlevel'
        '''
        return platform.python_version()

    def get_python_revision():
        '''Returns a string identifying the Python implementation SCM revision.'''
        return platform.python_revision()

    def get_python_version_tuple():
        '''Returns the Python version as tuple (major, minor, patchlevel) of strings'''
        return platform.python_version_tuple()

    def show_python_all_info():
        '''打印python的全部信息'''
        print('The Python build number and date as strings : [{}]'.format(get_python_build()))
        print('Returns a string identifying the compiler used for compiling Python : [{}]'.format(get_python_compiler()))
        print('Returns a string identifying the Python implementation SCM branch : [{}]'.format(get_python_branch()))
        print('Returns a string identifying the Python implementation : [{}]'.format(get_python_implementation()))
        print('The version of Python ： [{}]'.format(get_python_version()))
        print('Python implementation SCM revision : [{}]'.format(get_python_revision()))
        print('Python version as tuple : [{}]'.format(get_python_version_tuple()))

    def show_python_info():
        '''只打印python的信息，没有解释部分'''
        print(get_python_build())
        print(get_python_compiler())
        print(get_python_branch())
        print(get_python_implementation())
        print(get_python_version())
        print(get_python_revision())
        print(get_python_version_tuple())

    def test():
        print('计算机中的python信息：')
        print('#' * 50)
        if SHOW_LOG:
            show_python_all_info()
        else:
            show_python_info()

    def init():
        global SHOW_LOG
        SHOW_LOG = True

    # global var
    SHOW_LOG = True  # 是否显示日志信息
    init()
    test()


def get_platform_system_info():
    '''
    python中，platform模块给我们提供了很多方法去获取操作系统的信息
        platform.platform()   #获取操作系统名称及版本号，'Windows-7-6.1.7601-SP1'
        platform.version()    #获取操作系统版本号，'6.1.7601'
        platform.architecture()   #获取操作系统的位数，('32bit', 'WindowsPE')
        platform.machine()    #计算机类型，'x86'
        platform.node()       #计算机的网络名称，'hongjie-PC'
        platform.processor()  #计算机处理器信息，'x86 Family 16 Model 6 Stepping 3, AuthenticAMD'
        platform.uname()      #包含上面所有的信息汇总，uname_result(system='Windows', node='hongjie-PC',
                               release='7', version='6.1.7601', machine='x86', processor='x86 Family
                               16 Model 6 Stepping 3, AuthenticAMD')
    '''

    def get_platform():
        '''获取操作系统名称及版本号'''
        return platform.platform()

    def get_version():
        '''获取操作系统版本号'''
        return platform.version()

    def get_architecture():
        '''获取操作系统的位数'''
        return platform.architecture()

    def get_machine():
        '''计算机类型'''
        return platform.machine()

    def get_node():
        '''计算机的网络名称'''
        return platform.node()

    def get_processor():
        '''计算机处理器信息'''
        return platform.processor()

    def get_system():
        '''获取操作系统类型'''
        return platform.system()

    def get_dist():
        '''获取linux发行版'''
        return platform.dist()

    def get_win():
        '''获取windows版本信息'''
        return platform.win32_ver()

    def get_mac():
        '''获取macOS版本信息'''
        return platform.mac_ver()

    def get_os_version():
        if get_system() == 'Windows':
            return get_win()
        elif get_system() == 'Linux':
            return get_dist()
        elif get_system() == 'Darwin':
            return get_mac()
        else:
            return '不识别操作系统'

    def get_uname():
        '''汇总信息'''
        return platform.uname()

    def show_os_all_info():
        '''打印os的全部信息'''
        print('获取操作系统名称及版本号 : [{}]'.format(get_platform()))
        print('获取操作系统版本号 : [{}]'.format(get_version()))
        print('获取操作系统的位数 : [{}]'.format(get_architecture()))
        print('计算机类型 : [{}]'.format(get_machine()))
        print('计算机的网络名称 : [{}]'.format(get_node()))
        print('计算机处理器信息 : [{}]'.format(get_processor()))
        print('获取操作系统类型 : [{}]'.format(get_system()))
        print('获取操作系统版本信息 : [{}]'.format(get_os_version()))
        print('汇总信息 : [{}]'.format(get_uname()))

    def show_os_info():
        '''只打印os的信息，没有解释部分'''
        print(get_platform())
        print(get_version())
        print(get_architecture())
        print(get_machine())
        print(get_node())
        print(get_processor())
        print(get_system())
        print(get_os_version())
        print(get_uname())

    def test():
        print('操作系统信息:')
        print('#' * 50)
        if SHOW_LOG:
            show_os_all_info()
        else:
            show_os_info()

    def init():
        global SHOW_LOG
        SHOW_LOG = True

    # global var
    SHOW_LOG = True  # 是否显示日志信息
    init()
    test()


def test_3():
    # 操作系统和位数
    print(platform.architecture())
    # CPU架构
    print(platform.machine())
    # CPU处理器版本
    print(platform.processor())


def main():
    dict_choice = {
        "h": "test_help()",
        "d": "test_dir()",
        "1": "get_platform_python_info()",
        "2": "get_platform_system_info()",
        "3": "test_3()",
    }

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
