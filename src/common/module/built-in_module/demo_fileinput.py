#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# https://laike9m.com/blog/python-fileinput-shi-yong-zong-jie,23/
# 【Run】⇒ 【Edit Configurations】⇒ 【Script parameters】 ⇒ *.txt

'''
# http://blog.csdn.net/jerry_1126/article/details/41926407

【默认格式】
fileinput.input (files=None, inplace=False, backup='', bufsize=0, mode='r', openhook=None)
files:                  #文件的路径列表，默认是stdin方式，多文件['1.txt','2.txt',...]  
inplace:                #是否将标准输出的结果写回文件，默认不取代  
backup:                 #备份文件的扩展名，只指定扩展名，如.bak。如果该文件的备份文件已存在，则会自动覆盖。  
bufsize:                #缓冲区大小，默认为0，如果文件很大，可以修改此参数，一般默认即可  
mode:                   #读写模式，默认为只读  
openhook:               #该钩子用于控制打开的所有文件，比如说编码方式等;  

【常用函数】
fileinput.input()       #返回能够用于for循环遍历的对象  
fileinput.filename()    #返回当前文件的名称  
fileinput.lineno()      #返回当前已经读取的行的数量（或者序号）  
fileinput.filelineno()  #返回当前读取的行的行号  
fileinput.isfirstline() #检查当前行是否是文件的第一行  
fileinput.isstdin()     #判断最后一行是否从stdin中读取  
fileinput.close()       #关闭队列  
'''

import fileinput
import glob
import re
import sys


def test_help():
    help(fileinput)


def test_dir():
    for i in dir(fileinput):
        print(i)


# 利用fileinput读取一个文件所有行
def test_3():
    for fileLine in fileinput.input('data.txt'):
        print(fileinput.filename(), '|', 'Line Number:', fileinput.lineno(), '|: ', fileLine)


# 多个文件作为输入, 遍历文件每一行
# 【Run】⇒ 【Edit Configurations】⇒ 【Script parameters】 ⇒ *.txt
def test_4():
    # print(sys.argv[1:])
    all_files = [f for files in sys.argv[1:] for f in glob.glob(files)]
    for line in fileinput.input(all_files):
        print(fileinput.filename(), fileinput.filelineno(), line)


# 在原处修改文件
def test_5(file="test"):
    def process_1(fileLine):
        mark = "tz"
        # endswith() 方法用于判断字符串是否以指定后缀结尾，如果以指定后缀结尾返回True，否则返回False。
        if fileLine.endswith(mark):
            return ""
        else:
            return fileLine

    def process_2(fileLine):
        s1 = re.search("10", fileLine)
        if s1 == None:
            return fileLine
        else:
            # rstrip() 删除 string 字符串末尾的指定字符（默认为空格）
            return fileLine.rstrip() + ' line'

    def process_3(fileLine):
        # 将Windows/DOS格式下的文本文件转为Linux的文件
        # 将CRLF文件转为LF
        if fileLine[-2:] == "\r\n":
            fileLine = fileLine + "\n"

    def deal_file_local(file):
        with fileinput.input(file, inplace=True) as f:
            for fileLine in f:
                fileLine.encode('UTF-8')
                fileLine = fileLine.rstrip()
                print(process_1(fileLine))

    deal_file_local(file)


# 利用fileinput实现文件内容替换，并将原文件作备份
def test_6():
    for line in fileinput.input('data.txt', backup='.bak', inplace=1):
        print(line.rstrip().replace('Python', 'Perl'))


# 利用fileinput及re做日志分析: 提取所有含日期的行
def test_7():
    pattern = '\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}'
    for line in fileinput.input('error.log'):
        if re.search(pattern, line):
            sys.stdout.write("=> ")
            sys.stdout.write(line)


# 利用fileinput及re做分析: 提取符合条件的电话号码
def test_8():
    pattern = '[010|021]-\d{8}'  # 提取区号为010或021电话号码，格式:010-12345678
    for line in fileinput.input('phone.txt'):
        if re.search(pattern, line):
            print('Filename:' + fileinput.filename() + ' | Line Number:' + str(
                fileinput.lineno()) + ' | ' + line, )


# 利用fileinput实现类似于grep的功能
def test_9():
    pattern = re.compile(sys.argv[1])
    for line in fileinput.input(sys.argv[2]):
        if pattern.match(line):
            print(fileinput.filename(), fileinput.filelineno(), line)


# 利用fileinput做正则替换
def test_10():
    for line in fileinput.input():
        line = re.sub(r'\*[(.*)](#(.*))', r'<h2 id="\2">\1</h2>', line.rstrip())
        print(line)


# 利用fileinput做正则替换，不同字模块之间的替换
def test_11():
    regex = re.compile(r'^([^&]*)(&)([^&]*)(&)([^&]*)')
    # 整行以&分割，要实现[@!$First]与[*%-Second]互换
    for line in fileinput.input('test.txt', inplace=1, backup='.bak'):
        print(regex.sub(r'\3\2\1\4\5', line))


# 利用fileinput根据argv命令行输入做替换
def test_12():
    source = sys.argv[1]
    target = sys.argv[2]
    files = sys.argv[3:]

    for line in fileinput.input(files, backup='.bak', openhook=fileinput.hook_encoded("gb2312")):
        # 对打开的文件执行中文字符集编码
        line = line.rstrip().replace(source, target)
        print(line)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
