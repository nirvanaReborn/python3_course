#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/wupeiqi/articles/4963027.html
# 用于加密相关的操作，代替了md5模块和sha模块，主要提供 SHA1, SHA224, SHA256, SHA384, SHA512 ，MD5 算法
import hashlib


def test_1():
    # ######## md5 ########
    hash = hashlib.md5()
    hash.update('admin'.encode())
    print(hash.hexdigest())  # 21232f297a57a5a743894a0e4a801fc3

    # ######## sha1 ########
    hash = hashlib.sha1()
    hash.update('admin'.encode())
    print(hash.hexdigest())

    # ######## sha256 ########
    hash = hashlib.sha256()
    hash.update('admin'.encode())
    print(hash.hexdigest())

    # ######## sha384 ########
    hash = hashlib.sha384()
    hash.update('admin'.encode())
    print(hash.hexdigest())

    # ######## sha512 ########
    hash = hashlib.sha512()
    hash.update('admin'.encode())
    print(hash.hexdigest())


# 以上加密算法虽然依然非常厉害，但时候存在缺陷，即：通过撞库可以反解。
# 所以，有必要对加密算法中添加自定义key再来做加密。
def test_2():
    # ######## md5 ########
    hash = hashlib.md5('898oaFs09f'.encode())
    hash.update('admin'.encode())
    print(hash.hexdigest())  # 67929daefab29024d18340945036e291


# ------------------------------------------------------------------------------
def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
