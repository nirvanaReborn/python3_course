#!/usr/bin/env python
# -*- coding:utf-8 -*-

#
#
import calendar


# https://www.toutiao.com/a6520028124908880387/
def test_1():
    # 从指定的年和月中得到月第一天是星期几的日期码和这个月总共有多少天
    print("Year:2002 - Mouth:2")
    month_range = calendar.monthrange(2002, 2)
    print("weekday of first day of the month:", month_range[0])
    print("number of days in month:", month_range[1])

    print()
    print("Year:2010 - Month:5")
    month_range = calendar.monthrange(2010, 5)
    print("weekday of first day of the month:", month_range[0])
    print("number of days in month:", month_range[1])


def test_2():
    # 打印指定年份的日历
    cal_display = calendar.TextCalendar(calendar.MONDAY)
    # 年：2019
    # 列宽：1
    # 每周行数：1
    # 月列之间的空格数：0
    # 每一列的月份数：2
    print(cal_display.formatyear(2019, 1, 1, 0, 2))


def test_3():
    # 怎样从月份数得到月份名？
    import datetime
    print("mouth name from number 5:")
    mouth_num = 1
    mouth_abbr = datetime.date(2015, mouth_num, 1).strftime("%b")
    mouth_name = datetime.date(2015, mouth_num, 1).strftime("%B")
    print("short name:", mouth_abbr)
    print("full name", mouth_name)

    print("\nList of all mouths from calendar")
    for mouth_val in range(1, 13):
        print(calendar.month_abbr[mouth_val], "-", calendar.month_name[mouth_val])


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
