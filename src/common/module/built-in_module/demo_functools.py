#!/usr/bin/env python
# -*- coding:utf-8 -*-

#
#
import functools


'''
在Python 3里,reduce()函数已经被从全局名字空间里移除了,
它现在被放置在fucntools模块里,用的话要先引入：from functools import reduce 

http://blog.csdn.net/damotiansheng/article/details/44139111
Python3.4中filter函数，map函数和reduce函数

http://www.cnblogs.com/XXCXY/p/5180245.html
reduce()函数接收的参数和 map()类似，一个函数 f，一个list，
但行为和 map()不同，reduce()传入的函数 f 必须接收两个参数，
reduce()对list的每个元素反复调用函数f，并返回最终结果值。
'''


def f(x, y):
    return x + y


def test_1():
    list_1 = [1, 3, 5, 7, 9]
    print(functools.reduce(f, list_1))
    # reduce函数将做如下计算：
    # 先计算头两个元素：f(1, 3)，结果为4；
    # 再把结果和第3个元素计算：f(4, 5)，结果为9；
    # 再把结果和第4个元素计算：f(9, 7)，结果为16；
    # 再把结果和第5个元素计算：f(16, 9)，结果为25；
    # 由于没有更多的元素了，计算结束，返回结果25。
    # 上述计算实际上是对 list 的所有元素求和。
    # 虽然Python内置了求和函数sum()，但是，利用reduce()求和也很简单。


def test_2():
    # reduce()还可以接收第3个可选参数，作为计算的初始值。
    list_1 = [1, 3, 5, 7, 9]
    print(functools.reduce(f, list_1, 100))
    # 结果将变为125
    # 计算初始值和第一个元素：f(100, 1)，结果为101。


def test_3():
    ''' https://www.jb51.net/article/204673.htm
    Python中lru_cache的使用和实现详解
    可以看到， factorial(3) 的结果在计算 factorial(5) 的时候已经被计算过了，但是后面又被重复计算了。
    为了避免这种重复计算，我们可以在定义函数 factorial 的时候加上 lru_cache 装饰器，如下所示
    '''

    # 注意 lru_cache 后的一对括号，证明这是带参数的装饰器
    @functools.lru_cache()
    def factorial(n):
        print(f"计算 {n} 的阶乘")
        return 1 if n <= 1 else n * factorial(n - 1)

    a = factorial(5)
    print(f'5! = {a}')
    b = factorial(3)
    print(f'3! = {b}')


def test_4():
    '''
    简单总结functools.partial的作用就是，把一个函数的某些参数给固定住（也就是设置默认值），返回一个新的函数。
    print(f(3, y=2))
    print(f(4, y=2))
    print(f(7, y=2))
    上面的调用有点繁琐，每次都要传入 y=2，我们想到可以定义一个新的函数，把 y=2 作为默认值，即：
    '''
    new_func = functools.partial(f, 2)  # 表示把 2 赋给了函数 f 的形参 y
    print(new_func(5))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
