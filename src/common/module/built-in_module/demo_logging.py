#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/dkblog/archive/2011/08/26/2155018.html
# https://www.cnblogs.com/yyds/p/6901864.html
'''critical > error > warning > info > debug
级别          日志函数            描述
DEBUG       logging.debug()     最低级别。用于小细节。通常只有在诊断问题时，你才会关心这些消息
INFO        logging.info()      用于记录程序中一般事件的信息，或确认一切工作正常
WARNING     logging.warning()   用于表示可能的问题，它不会阻止程序的工作，但将来可能会
ERROR       logging.error()     用于记录错误，它导致程序做某事失败
CRITICAL    logging.critical()  最高级别。用于表示致命的错误，它导致或将要导致程序完全停止工作

我是用 basicConfig 对日志进行了简单的配置，其实我们还可以进行更为复杂些的配置，在此之前，我们先来了解一下 logging 中的几个概念：
Logger：日志记录器，是应用程序中可以直接使用的接口。
Handler：日志处理器，用以表明将日志保存到什么地方以及保存多久。
Formatter：格式化，用以配置日志的输出格式。
上述三者的关系是：一个 Logger 使用一个 Handler，一个 Handler 使用一个 Formatter。

简单的就用上面所说的 basicConfig，对于比较复杂的我们可以将日志的配置保存在一个配置文件中，然后在主程序中使用 fileConfig 读取配置文件。
'''

import logging
from logging import handlers
import datetime

# logging.disable(logging.DEBUG) # 禁用日志

# logging 库获取日志 requests 和 urllib3(requests 依赖) 对象，调低他们的日志级别，
# 设置为 WARNING 则代表只有 WARNING 以上级别的日志才会被输出出来
# logging.captureWarnings(True)  # 通过捕获警告到日志的方式忽略警告

logging.getLogger("selenium").setLevel(logging.WARNING)
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)


# urllib3.disable_warnings()


# https://www.cnblogs.com/canglongdao/p/12152290.html
# https://blog.csdn.net/weixin_42603578/article/details/107234030
def test_12():
    class Logger(object):
        # 日志级别关系映射
        level_relations = {
            'debug': logging.DEBUG,
            'info': logging.INFO,
            'warning': logging.WARNING,
            'error': logging.ERROR,
            'crit': logging.CRITICAL,
        }

        # def __init__(self, filename, level='info', when='D', backCount=3, fmt='%(asctime)s - %(pathname)s[line:%(lineno)d] - %(levelname)s: %(message)s'):
        def __init__(self, filename, level='info', when='D', backCount=3, fmt='%(message)s'):
            format_str = logging.Formatter(fmt)  # 设置日志格式
            self.logger = logging.getLogger(filename)
            self.logger.setLevel(self.level_relations.get(level))  # 设置日志级别
            sh = logging.StreamHandler()  # 往屏幕上输出
            sh.setFormatter(format_str)  # 设置屏幕上显示的格式
            self.logger.addHandler(sh)  # 把对象加到logger里
            # 往文件里写入:指定间隔时间自动生成文件的处理器
            # 实例化TimedRotatingFileHandler
            # interval是时间间隔，backupCount是备份文件的个数，如果超过这个个数，就会自动删除，when是间隔的时间单位，单位有以下几种：
            # S 秒
            # M 分
            # H 小时、
            # D 天、
            # W 每星期（interval==0时代表星期一）
            # midnight 每天凌晨
            fh = handlers.TimedRotatingFileHandler(filename=filename, when=when, backupCount=backCount, encoding='utf-8')
            fh.setFormatter(format_str)  # 设置文件里写入的格式
            self.logger.addHandler(fh)  # 把对象加到logger里

    log = Logger((datetime.datetime.now()).strftime("%Y%m%d%H%M%S") + '-myProgramLog.log', level='debug')
    log.logger.info('This is info message')


def test_11():
    import datetime
    class MyFormatter(logging.Formatter):
        converter = datetime.datetime.fromtimestamp

        def formatTime(self, record, datefmt=None):
            ct = self.converter(record.created)
            if datefmt:
                s = ct.strftime(datefmt)
            else:
                t = ct.strftime("%Y-%m-%d %H:%M:%S")
                s = "%s,%03d" % (t, record.msecs)
            return s

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    console = logging.StreamHandler()
    logger.addHandler(console)
    formatter = MyFormatter(fmt='%(asctime)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S.%f')
    console.setFormatter(formatter)

    logger.debug('Jackdaws love my big sphinx of quartz.')


def test_10():
    logging.getLogger("logging").setLevel(logging.DEBUG)

    def factorial(n):
        logging.debug('Start of factorial(%s)' % (n))
        total = 1
        for i in range(1, n + 1):
            total *= i
            logging.debug('i is ' + str(i) + ', total is ' + str(total))
        logging.debug('End of factorial(%s)' % (n))
        return total

    logging.debug('Start of program')
    print(factorial(5))
    logging.debug('End of program')


def test_8():
    # https://blog.csdn.net/cxx654/article/details/83216337
    import logging.config
    logging.config.fileConfig('logging2.conf')

    # 负责将日志输出到控制台,获取的是root对应的logger
    console_logger = logging.getLogger()
    console_logger.debug("debug message")
    console_logger.info("info message")
    console_logger.warning("warn message")
    console_logger.error("error message")
    console_logger.critical("critical message")

    # 负责将日志输出保存到文件中
    file_logger = logging.getLogger(name="fileLogger")

    # rotatingFileLogger中就同时配置了consoleHandler,rotatingFileHandler
    # consoleHandler 负责将日志输出到控制台
    # rotatingFileHandler 负责将日志输出保存到文件中
    rotating_logger = logging.getLogger(name="rotatingFileLogger")


def test_7():
    import logging.config
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger("example02")

    logger.debug('This is debug message')
    logger.info('This is info message')
    logger.warning('This is warning message')


def test_6():
    import logging.config
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger("example01")

    logger.debug('This is debug message')
    logger.info('This is info message')
    logger.warning('This is warning message')


# logging之日志回滚
def test_5():
    '''
    logging.StreamHandler: 日志输出到流，可以是sys.stderr、sys.stdout或者文件
    logging.FileHandler: 日志输出到文件

    日志回滚方式，实际使用时用RotatingFileHandler和TimedRotatingFileHandler
    logging.handlers.BaseRotatingHandler
    logging.handlers.RotatingFileHandler
    logging.handlers.TimedRotatingFileHandler

    logging.handlers.SocketHandler: 远程输出日志到TCP/IP sockets
    logging.handlers.DatagramHandler:  远程输出日志到UDP sockets
    logging.handlers.SMTPHandler:  远程输出日志到邮件地址
    logging.handlers.SysLogHandler: 日志输出到syslog
    logging.handlers.NTEventLogHandler: 远程输出日志到Windows NT/2000/XP的事件日志
    logging.handlers.MemoryHandler: 日志输出到内存中的制定buffer
    logging.handlers.HTTPHandler: 通过"GET"或"POST"远程输出到HTTP服务器
    '''
    import logging
    from logging.handlers import RotatingFileHandler
    #################################################################################################
    # 定义一个RotatingFileHandler，最多备份5个日志文件，每个日志文件最大10M
    Rthandler = RotatingFileHandler('myapp.log', maxBytes=10 * 1024 * 1024, backupCount=5)
    Rthandler.setLevel(logging.INFO)
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    Rthandler.setFormatter(formatter)
    logging.getLogger('').addHandler(Rthandler)
    ################################################################################################


# 将日志同时输出到文件和屏幕
def test_4():
    import logging

    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s.%(msecs)03d %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                        datefmt='%Y%m%d %H:%M:%S',
                        filename='myapp.log',
                        filemode='w')

    #################################################################################################
    # 定义一个StreamHandler，将INFO级别或更高的日志信息打印到标准错误，并将其添加到当前的日志处理对象#
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    console.setFormatter(formatter)
    logging.getLogger('').addHandler(console)
    #################################################################################################

    logging.debug('This is debug message')
    logging.info('This is info message')
    logging.warning('This is warning message')


# 通过logging.basicConfig函数对日志的输出格式及方式做相关配置
def test_3():
    '''logging.basicConfig函数各参数:
    filename: 指定日志文件名
    filemode: 和file函数意义相同，指定日志文件的打开模式，'w'或'a'
    format: 指定输出的格式和内容，format可以输出很多有用信息，如上例所示:
    %(levelno)s: 打印日志级别的数值
    %(levelname)s: 打印日志级别名称
    %(pathname)s: 打印当前执行程序的路径，其实就是sys.argv[0]
    %(filename)s: 打印当前执行程序名
    %(funcName)s: 打印日志的当前函数
    %(lineno)d: 打印日志的当前行号
    %(asctime)s: 打印日志的时间
    %(thread)d: 打印线程ID
    %(threadName)s: 打印线程名称
    %(process)d: 打印进程ID
    %(message)s: 打印日志信息
    datefmt: 指定时间格式，同time.strftime()
    level: 设置日志级别，默认为logging.WARNING
    stream: 指定将日志的输出流，可以指定输出到sys.stderr,sys.stdout或者文件，默认输出到sys.stderr，当stream和filename同时指定时，stream被忽略
    '''
    import logging
    logging.basicConfig(level=logging.DEBUG,
                        # filename='myProgramLog.log',
                        # filemode='w',
                        format='[%(asctime)s.%(msecs)03d] - [%(levelname)s] - %(message)s',
                        datefmt='%Y%m%d %H:%M:%S',
                        # format='%(message)s',
                        )

    logging.debug('This is debug message')
    logging.info('This is info message')
    logging.warning('This is warning message')


# 简单的将日志打印到屏幕
# 默认情况下，logging将日志打印到屏幕，日志级别为WARNING。
# 日志级别大小关系为：CRITICAL > ERROR > WARNING > INFO > DEBUG > NOTSET，当然也可以自己定义日志级别。
def test_2():
    # logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    logging.debug('Some debugging details.')
    # 2015-05-18 19:04:26,901 - DEBUG - Some debugging details.
    logging.info('The logging module is working.')
    # 2015-05-18 19:04:35,569 - INFO - The logging module is working.
    logging.warning('An error message is about to be logged.')
    # 2015-05-18 19:04:56,843 - WARNING - An error message is about to be logged.
    logging.error('An error has occurred.')
    # 2015-05-18 19:05:07,737 - ERROR - An error has occurred.
    logging.critical('The program is unable to recover!')
    # 2015-05-18 19:05:45,794 - CRITICAL - The program is unable to recover!


def test_1():
    # logging模块提供的日志记录函数所使用的日志器设置的日志级别是WARNING
    # "%(levelname)s:%(name)s:%(message)s"
    # 日志输出位置默认为:sys.stderr
    logging.log(logging.DEBUG, "This is a debug log.")
    logging.log(logging.INFO, "This is a info log.")
    logging.log(logging.WARNING, "This is a warning log.")
    logging.log(logging.ERROR, "This is a error log.")
    logging.log(logging.CRITICAL, "This is a critical log.")


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    # 屏蔽特定类型的警告
    import warnings
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", UserWarning)
        warnings.simplefilter("ignore", FutureWarning)
        main()
