#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://www.pythondoc.com/pythontutorial3/stdlib.html
# http://blog.csdn.net/hk2291976/article/details/41545881
# CSV是一种广泛使用的文件格式，所谓“CSV”，是Comma Separated Value（逗号分隔值）的英文缩写，通常都是纯文本文件。
# 出现在档案总管中的档案类型是「逗号分格」。
# 这里注意，如果文件是一个对象则要加入b属性，读出来的数据可以看作一个二维列表，每一行就是csv文件的一行，
# 而每个item，就是逗号分隔的每个数据（即excel中的一个单元格）
import csv
import os
import sys

from public_function import GLOBAL_WORK_DIR


def test_help():
    help(csv)


def test_dir():
    for i in dir(csv):
        print(i)


def read_csv(source_file):
    list_2D = []
    # python3.0没有file()了，但还是有open()
    with open(source_file, 'rt') as csvfile:
        data = csv.reader(csvfile)
        for fileLine in data:  # 每一行
            list_1D = []
            for item in fileLine:  # 每个数据（即excel中的一个单元格）
                # print(item)
                list_1D.append(item)
            list_2D.append(list_1D)
    return list_2D


def write_csv(dest_file):
    with open(dest_file, 'w') as csvfile:
        # 写文件格式是以行为单位写，每行‘[’内字符串以‘，’分隔‘]’ 或者使用算术表达式如下
        spamwriter = csv.writer(csvfile)
        spamwriter.writerow(['Spam'] * 5 + ['Baked Beans'])
        spamwriter.writerow(['Spam', 'Lovely Spam', 'Wonderful Spam'])
        # 或是多行写
        list_2D = [
            ['1', '2'],
            ['3', '4'],
        ]
        spamwriter.writerows(list_2D)


def main():
    dest_file = os.path.join(GLOBAL_WORK_DIR, "test.csv")
    source_file = os.path.join(GLOBAL_WORK_DIR, "test.csv")
    dict_choice = {
        "1": "write_csv(source_file)",
        "2": "read_csv(dest_file)",
    }

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
