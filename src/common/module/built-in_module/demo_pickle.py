#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 该pickle模块实现了用于序列化和反序列化Python对象结构的二进制协议。
# 通过pickle模块的序列化操作我们能够将程序中运行的对象信息保存到文件中去，永久存储。
# 通过pickle模块的反序列化操作，我们能够从文件中创建上一次程序保存的对象。
# pickle提供四个功能：dumps,dump,loads,load

import pickle


def main():
    path = 'test'
    f = open(path, 'wb')
    data = {'a': 123, 'b': 'ads', 'c': [[1, 2], [3, 4]]}
    pickle.dump(data, f)
    f.close()

    f1 = open(path, 'rb')
    data1 = pickle.load(f1)
    print(data1)


if __name__ == "__main__":
    main()
