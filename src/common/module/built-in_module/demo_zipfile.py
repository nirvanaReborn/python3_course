#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/kevinelstri/article/details/61921812
# 使用python读取数据科学最常用的文件格式

import zipfile


def main():
    # 压缩
    z = zipfile.ZipFile('laxi.zip', 'w')
    z.write('a.log')
    z.write('data.data')
    z.close()

    # 解压
    source_file = r"D:\share\git\gitee\zhangtao2016\course_python\doc\book\Python编程快速上手——让繁琐工作自动化.zip"
    archive = zipfile.ZipFile(source_file, 'r')
    df = archive.read(r'automate_online-materials/hello.py').decode('utf-8')
    print(df)
    archive.extractall()
    archive.close()


if __name__ == "__main__":
    main()
