#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://www.pythondoc.com/pythontutorial3/stdlib.html
# math 模块为浮点运算提供了对底层C函数库的访问

import random

'''
函数	                                描述
choice(seq)	                            从序列的元素中随机挑选一个元素
randrange ([start,] stop [,step])	    从指定范围内，按指定基数递增的集合中获取一个随机数，基数缺省值为1
random()	                            随机生成下一个实数，它在[0,1)范围内。
seed([x])	                            改变随机数生成器的种子seed。如果你不了解其原理，你不必特别去设定seed，Python会帮你选择seed。
shuffle(lst)	                        将序列的所有元素随机排序
uniform(x, y)	                        随机生成下一个实数，它在[x,y]范围内。
'''


def test_help():
    help(random)


def test_dir():
    for i in dir(random):
        print(i)


def test_1():
    print(random.choice(range(10)))  # 从0到9中随机挑选一个整数
    print(random.choice(['apple', 'pear', 'banana']))
    print(random.random())  # 返回随机生成的一个实数，它在[0,1)范围内。
    print(random.randint(1, 10))  # 用于生成一个指定范围内的整数: 1 <= n <= 10
    print(random.randrange(100, 1000, 2))  # 返回指定递增基数集合中的一个随机数, 本例输出 100 <= number < 1000 间的偶数。
    print(random.sample(range(100), 5))  # 从 range(100) 中随机获取5个元素，作为一个列表返回


def test_2():
    for i in range(5):
        print(random.randint(1, 10))


# 随机验证码实例
def test_3(length=6):
    checkcode = ''
    for i in range(length):
        current = random.randrange(0, length)
        if current != i:
            temp = chr(random.randint(65, 90))
        else:
            temp = random.randint(0, 9)
        checkcode += str(temp)
    print(checkcode)
    return checkcode


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
