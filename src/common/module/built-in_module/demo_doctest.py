#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://www.pythondoc.com/pythontutorial3/stdlib.html
# 标准库中有两个测试包: unittest 和 doctest。

# 有开发高质量软件的方法之一是为每一个函数开发测试代码，并且在开发过程中经常进行测试。
# doctest 模块提供了一个工具，扫描模块并根据程序中内嵌的文档字符串执行测试。
# 测试构造如同简单的将它的输出结果剪切并粘贴到文档字符串中。
# 通过用户提供的例子，它发展了文档，允许 doctest 模块确认代码的结果是否与文档一致:

# 标准库中的第二个测试包是doctest（https://docs.python.org/3/library/doctest.html）
# 使用这个包可以把测试写到文档字符串中，也可以起到文档的作用。
# 它看起来有点像交互式解释器：字符>>> 后面是一个函数调用，下一行是执行结果。
# 你可以在交互式解释器中运行测试并把结果粘贴到测试文件中。

import doctest


def test_1():
    def average(values):
        # Computes the arithmetic mean of a list of numbers.
        # print(average([20, 30, 70])) # 40.0
        return sum(values) / len(values)

    doctest.testmod()  # automatically validate the embedded tests


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
