#!/usr/bin/env python
# -*- coding:utf-8 -*-

#
#

def call_html_parser():
    '''
    # http://blog.csdn.net/u010154424/article/details/52273868
    # Python解析html的几种操作方式

    # HTMLParaer，与SGMLParser原理相识，就是调用的方法不太一样，基本上可以公用.
    # http://www.cnblogs.com/masako/p/5868367.html
    '''
    from html.parser import HTMLParser
    import requests
    import re

    class CommentHTMLParser(HTMLParser):
        def __init__(self):
            HTMLParser.__init__(self)
            self.__start_div_yingping = False
            self.__start_div_item = False
            self.__start_div_gclear = False
            self.__start_div_ratingwrap = False
            self.__start_div_num = False
            # a
            self.__start_a = False
            # span 3中状态
            self.__span_state = 0
            # 数据
            self.__value = {}
            self.data = []

        def handle_starttag(self, tag, attrs):
            if tag == 'div':
                for k, v in attrs:
                    if k == 'class' and v == 'yingping-list-wrap':
                        self.__start_div_yingping = True
                    elif k == 'class' and v == 'item':
                        self.__start_div_item = True
                    elif k == 'class' and v == 'g-clear title-wrap':
                        self.__start_div_gclear = True
                    elif k == 'class' and v == 'rating-wrap g-clear':
                        self.__start_div_ratingwrap = True
                    elif k == 'class' and v == 'num':
                        self.__start_div_num = True
            elif tag == 'a':
                if self.__start_div_yingping and self.__start_div_item and self.__start_div_gclear:
                    self.__start_a = True
                    for k, v in attrs:
                        if k == 'href':
                            self.__value['href'] = v
            elif tag == 'span':
                if self.__start_div_yingping and self.__start_div_item and self.__start_div_gclear:
                    if self.__start_div_ratingwrap:
                        if self.__span_state != 1:
                            for k, v in attrs:
                                if k == 'class' and v == 'rating':
                                    self.__span_state = 1
                                elif k == 'class' and v == 'time':
                                    self.__span_state = 2
                        else:
                            for k, v in attrs:
                                if k == 'style':
                                    score_text = re.search(r'\d+', v).group()
                            self.__value['score'] = int(score_text) / 20
                            self.__span_state = 3
                    elif self.__start_div_num:
                        self.__span_state = 4

        def handle_endtag(self, tag):
            if tag == 'div':
                if self.__start_div_yingping:
                    if self.__start_div_item:
                        if self.__start_div_gclear:
                            if self.__start_div_num or self.__start_div_ratingwrap:
                                if self.__start_div_num:
                                    self.__start_div_num = False
                                if self.__start_div_ratingwrap:
                                    self.__start_div_ratingwrap = False
                            else:
                                self.__start_div_gclear = False
                        else:
                            self.data.append(self.__value)
                            self.__value = {}
                            self.__start_div_item = False
                    else:
                        self.__start_div_yingping = False
            elif tag == 'a':
                if self.__start_div_yingping and self.__start_div_item and self.__start_div_gclear and self.__start_a:
                    self.__start_a = False
            elif tag == 'span':
                self.__span_state = 0

        def handle_data(self, data):
            if self.__start_a:
                self.__value['title'] = data
            elif self.__span_state == 2:
                self.__value['time'] = data
            elif self.__span_state == 4:
                score_text = re.search(r'\d+', data).group()
                self.__value['people'] = int(score_text)
            pass

    def html_parser(url):
        parser = CommentHTMLParser()
        html = requests.get(url).text
        parser.feed(html)
        return parser.data

    url = 'http://www.360kan.com/m/haPkY0osd0r5UB.html'
    value = html_parser(url)
    for row in value:
        print(row)


def main():
    call_html_parser()


if __name__ == "__main__":
    main()
