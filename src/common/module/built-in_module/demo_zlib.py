#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://www.pythondoc.com/pythontutorial3/stdlib.html
# 以下模块直接支持通用的数据打包和压缩格式：zlib， gzip， bz2， lzma， zipfile 以及 tarfile。


import zlib


def test_help():
    help(zlib)


def test_dir():
    for i in dir(zlib):
        print(i)


def test_3():
    s = b'witch which has which witches wrist watch'
    print(len(s))
    t = zlib.compress(s)
    print(len(t))
    print(zlib.decompress(t))
    print(zlib.crc32(s))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
