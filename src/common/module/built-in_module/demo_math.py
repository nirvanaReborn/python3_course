#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://www.pythondoc.com/pythontutorial3/stdlib.html
# math 模块为浮点运算提供了对底层C函数库的访问
# SciPy <http://scipy.org> 项目提供了许多数值计算的模块。

import math


def test_help():
    help(math)


def test_dir():
    for i in dir(math):
        print(i)


def test_3():
    print(math.cos(math.pi / 4.0))
    print(math.log(1024, 2))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
