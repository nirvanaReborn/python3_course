#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/wupeiqi/articles/4963027.html
# 压缩解压
import tarfile


def main():
    # 压缩
    tar = tarfile.open('your.tar', 'w')
    tar.add('/Users/wupeiqi/PycharmProjects/bbs2.zip', arcname='bbs2.zip')
    tar.add('/Users/wupeiqi/PycharmProjects/cmdb.zip', arcname='cmdb.zip')
    tar.close()

    # 解压
    tar = tarfile.open('your.tar', 'r')
    tar.extractall()  # 可设置解压地址
    tar.close()


if __name__ == "__main__":
    main()
