#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
#
import io


def test_1():
    s = "hello, sxt"
    sio = io.StringIO(s)
    print(sio)  # <_io.StringIO object at 0x0000020DC65AF708>
    print(sio.getvalue())  # hello, sxt
    print(sio.seek(7))  # 7
    print(sio.write("g"))  # 1
    print(sio.getvalue())  # hello, gxt


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
