#!/usr/bin/env python
# -*- coding: UTF-8 -*-

'''
pprint() 尽量排列输出元素从而增加可读性
'''
import pprint


def test_help():
    help(pprint)


def test_dir():
    for i in dir(pprint):
        print(i)


def test_1():
    # 使用有序字典OrderedDict()按键排序
    from collections import OrderedDict
    quotes = OrderedDict([
        ('Moe', 'A wise guy, huh?'),
        ('Larry', 'Ow!'),
        ('Curly', 'Nyuk nyuk!'),
    ])
    print(quotes)
    print(pprint.pformat(quotes))  # 等价于下一行代码
    pprint.pprint(quotes)  # 如果字典本身包含嵌套的列表或字典，pprint.pprint()函数就特别有用。


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
