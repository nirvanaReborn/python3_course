#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://www.pythondoc.com/pythontutorial3/stdlib.html
# glob 模块提供了一个函数用于从目录通配符搜索中生成文件列表:
# glob的作用就相当于os.listdir 加上 fnmatch。使用glob以后就不用使用os.listdir获取文件列表了。
# glob比fnmatch更简单，因为他只有 glob,iglob,escape三个函数。

import glob


def test_help():
    help(glob)


def test_dir():
    for i in dir(glob):
        print(i)


def test_3():
    list_file = glob.glob('*.py')
    print(list_file)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
