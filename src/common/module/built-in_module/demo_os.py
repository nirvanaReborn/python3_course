#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

# http://www.pythondoc.com/pythontutorial3/stdlib.html
# 操作系统接口

# 应该用 import os 风格而非 from os import *。
# 这样可以保证随操作系统不同而有所变化的 os.open() 不会覆盖内置函数 open()。
import os

from public_function import GLOBAL_WORK_DIR


# os.getcwd() 获取当前工作目录，即当前python脚本工作的目录路径
# os.chdir("dirname")  改变当前脚本工作目录；相当于shell下cd
# os.curdir  返回当前目录: ('.')
# os.pardir  获取当前目录的父目录字符串名：('..')
# os.makedirs('dirname1/dirname2')    可生成多层递归目录
# os.removedirs('dirname1')    若目录为空，则删除，并递归到上一级目录，如若也为空，则删除，依此类推
# os.mkdir('dirname')    生成单级目录；相当于shell中mkdir dirname
# os.rmdir('dirname')    删除单级空目录，若目录不为空则无法删除，报错；相当于shell中rmdir dirname
# os.listdir('dirname')    列出指定目录下的所有文件和子目录，包括隐藏文件，并以列表方式打印
# os.remove()  删除一个文件
# os.rename("oldname","newname")  重命名文件/目录
# os.link()
# os.unlink()
# os.symlink()
# os.chmod()	修改文件权限和时间戳
# os.exit()	终止进程
# os.mknod()	创建空文件
# os.fork()
# os.sep    输出操作系统特定的路径分隔符，win下为"\\",Linux下为"/"
# os.linesep    输出当前平台使用的行终止符，win下为"\t\n",Linux下为"\n"
# os.pathsep    输出用于分割文件路径的字符串
# os.name    输出字符串指示当前使用平台。win->'nt'; Linux->'posix'
# os.system("bash command")  运行shell命令，直接显示
# os.environ  获取系统环境变量
# os.getenv()和 os.putenv()	读取和设置环境变量
# os.getlogin()
# os.environ 获得系统信息
# os.lstat()返回文件的基本信息（比如：最后访问时间，修改时间……）
# os.stat() 返回文件的基本信息（比如：最后访问时间，修改时间……）（st_atime (访问时间), st_mtime (修改时间), st_ctime（创建时间））
# os.environ['HOME'] 获得家目录
# os.environ['USER']:当前使用用户。
# os.environ['LC_COLLATE']:路径扩展的结果排序时的字母顺序。
# os.environ['SHELL']:使用shell的类型。
# os.environ['LAN']:使用的语言。
# os.environ['SSH_AUTH_SOCK']:ssh的执行路径


# http://www.cnblogs.com/dkblog/archive/2011/03/25/1995537.html
# python os.path模块

# os.path.abspath(path) #返回绝对路径
# os.path.basename(path) #返回文件名
# os.path.commonprefix(list) #返回list(多个路径)中，所有path共有的最长的路径。
# os.path.dirname(path) #返回文件路径
# os.path.exists(path)  #路径存在则返回True,路径损坏返回False
# os.path.lexists  #路径存在则返回True,路径损坏也返回True
# os.path.expanduser(path)  #把path中包含的"~"和"~user"转换成用户目录
# os.path.expandvars(path)  #根据环境变量的值替换path中包含的”$name”和”${name}”
# os.path.getatime(path)  #返回最后一次进入此path的时间。
# os.path.getmtime(path)  #返回在此path下最后一次修改的时间。
# os.path.getctime(path)  #返回path的大小
# os.path.getsize(path)  #返回文件大小（单位是比特），如果文件不存在就返回错误
# os.path.isabs(path)  #判断是否为绝对路径
# os.path.isfile(path)  #判断路径是否为文件
# os.path.isdir(path)  #判断路径是否为目录
# os.path.islink(path)  #判断路径是否为链接
# os.path.ismount(path)  #判断路径是否为挂载点（）
# os.path.join(path1[, path2[, ...]])  #把目录和文件名合成一个路径
# os.path.normcase(path)  #转换path的大小写和斜杠
# os.path.normpath(path)  #规范path字符串形式
# os.path.realpath(path)  #返回path的真实路径
# os.path.relpath(path[, start])  #从start开始计算相对路径
# os.path.samefile(path1, path2)  #判断目录或文件是否相同
# os.path.sameopenfile(fp1, fp2)  #判断fp1和fp2是否指向同一文件
# os.path.samestat(stat1, stat2)  #判断stat tuple stat1和stat2是否指向同一个文件
# os.path.split(path)  #把路径分割成dirname和basename，返回一个元组
# os.path.splitdrive(path)   #一般用在windows下，返回驱动器名和路径组成的元组
# os.path.splitext(path)  #分割路径，返回路径名和文件扩展名的元组
# os.path.splitunc(path)  #把路径分割为加载点与文件
# os.path.supports_unicode_filenames  #设置是否支持unicode路径名
# os.path.walk(path, visit, arg)  #遍历path，进入每个目录都调用visit函数，
#                                  visit函数必须有3个参数(arg, dirname, names)，
#                                     dirname表示当前目录的目录名，
#                                     names代表当前目录下的所有文件名，
#                                     args则为walk的第三个参数


def test_help():
    help(os)


def test_dir():
    for i in dir(os):
        print(i)


def test_1():
    os.system('notepad')  # 打开记事本程序
    # os.system('notepad 1.txt')  # 打开1.txt文件,如果不存在，则创建


def test_2():
    # 直接调用可执行程序
    os.startfile(r"D:\ProgramFiles\1_FreeInstallation\SearchAndReplace_6.5\SR32.EXE")


# 打印所有环境变量
def test_3():
    flag = os.environ.get('READTHEDOCS', None) == 'True'
    print(flag)

    # 打印所有环境变量，遍历字典
    # environ是在os.py中定义的一个dict environ = {}
    env_dist = os.environ
    for key in env_dist:
        print(key + ' : ' + env_dist[key])

    # 找出python中pip的安装路径
    list_path = os.environ["PATH"].split(";")
    for env_path in list_path:
        if "Scripts" in env_path:
            exe_path = os.path.join(env_path, "pip.exe")
            break
        else:
            exe_path = None
            # exe_path = r"D:\ProgramFiles\Python\Anaconda3\Scripts\pip.exe"


def test_4():
    print(repr(os.linesep))  # 打印当前平台使用的行终止符。例如，Windows使用'\r\n'，Linux使用'\n'，而Mac使用'\r'。

    print(os.getpid())  # 获取进程号
    print(os.cpu_count())  # 获取电脑CPU的核心数

    print(os.name)  # 提示正在使用的系统（windows是nt，linux是posix）
    if os.name == "posix":  # linux系统
        print(os.environ['USER'])  # 获取当前用户名
        # print(os.getuid())  # 获取用户ID
        # print(os.getgid())  # 获取用户组ID
        print(os.sep)         # 路径分隔符
    elif os.name == "nt":  # windows系统
        print(os.environ['USERNAME'])  # 获取当前用户名
        print(os.sep)                  # 路径分隔符
    else:
        print("未知系统:", os.name)

    __import__('getpass').getuser()  # 获取当前用户名

    # print(r"C:\Users\%s" % os.environ['USERNAME'])
    print(os.path.expanduser("~"))


def test_5():
    source_file = r"math_test.py"
    # https://www.cnblogs.com/maseng/p/3386140.html
    # 返回相关文件的所有属性
    print(os.stat(source_file))

    os.remove(source_file)  # 删除指定的文件

    new_file = r"new_file.py"
    os.rename(source_file, new_file)  # 重命名文件或目录

    # 返回指定目录下的文件和目录的列表
    for item in os.listdir(os.path.dirname(source_file)):
        print(item)


# 目录操作
def test_6():
    print(os.getcwd())  # 打印当前的工作目录
    os.chdir(GLOBAL_WORK_DIR)  # 把指定目录设为当前工作目录
    print(os.getcwd())  # 获取更改后的当前目录

    source_dir = r"test"
    os.mkdir(source_dir)

    source_dir_deep = r"test1/test2/test3"
    os.makedirs(source_dir_deep)

    os.rmdir(source_dir)
    os.removedirs(source_dir_deep)  # 删除多级目录,只能删除空目录


def test_7():
    # 返回一个有n个byte那么长的一个string，然后很适合用于加密，每次返回结果不一样。
    print(os.urandom(16))
# -----------------------------------------------------------------------------------------------


def test_10():
    path = os.path.join(r'D:\share\test', '1.jpg')
    print(path)

    # 返回当前脚本的绝对路径
    path = os.path.abspath(".")
    print(path)

    # 判断是否为绝对路径
    print(os.path.isabs(path))

    source_file = os.path.join(r"D:\work", "1.xls")
    current_dir = os.path.dirname(os.path.abspath(source_file))
    dest_file = os.path.join(current_dir, "1.sql")
    print(dest_file)

    ########获得创建时间、访问时间、最后修改时间##########
    print(os.path.getctime(source_file))  # 返回创建时间
    print(os.path.getatime(source_file))  # 返回最后访问时间
    print(os.path.getmtime(source_file))  # 返回最后修改时间


def test_11():
    source_file = r"D:\share\git\zhangtao2016\course_c\src\test\.cproject"
    suffix = os.path.splitext(source_file)[1]  # 从路径中分割文件的扩展名
    if len(suffix) > 0:
        return suffix
    else:
        return None


def test_12():
    current_dir = "D:\Music"
    for item in os.listdir(current_dir):
        # print(item)
        # print(os.path.splitext(item)) # 分割路径，返回路径名和文件扩展名的元组
        print(os.path.splitext(item)[0])


def test_13():
    source_file = r'c:\jb51\index.html'
    (filepath, filename) = os.path.split(source_file)  # 对路径进行分割，以列表形式返回
    (shotname, extension) = os.path.splitext(filename)  # 从路径中分割文件的扩展名
    print(filepath, filename, shotname, extension)


def test_14():
    all_files = []
    path = os.getcwd()
    list_files = os.walk(path)
    for dirpath, dirnames, filenames in list_files:
        for dir in dirnames:
            all_files.append(os.path.join(dirpath, dir))
        for name in filenames:
            all_files.append(os.path.join(dirpath, name))

    # 打印子目录和子文件
    for file in all_files:
        print(file)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
