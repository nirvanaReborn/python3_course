#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/xc_zhou/article/details/82079762
# binascii模块包含很多在二进制和ASCII编码的二进制表示转换的方法。
# 通常情况不会直接使用这些功能，而是使用像UU，base64编码，或BinHex封装模块。
# binascii模块包含更高级别的模块使用的，用C语言编写的低级高效功能。
'''
binascii.a2b_uu(string)

binascii.b2a_uu(data)

binascii.a2b_base64(string)

binascii.b2a_base64(data)

binascii.a2b_qp(string[, header])

binascii.b2a_qp(data[, quotetabs, istext, header])

binascii.a2b_hqx(string)

binascii.rledecode_hqx(data)

binascii.rlecode_hqx(data)

binascii.b2a_hqx(data)

binascii.crc_hqx(data, crc)

binascii.crc32(data[, crc])

binascii.b2a_hex(data)

binascii.b2a_hex(data)

binascii.hexlify(data)

binascii.a2b_hex(hexstr)

binascii.unhexlify(hexstr)

异常有：
exception binascii.Error
exception binascii.Incomplete
'''
import binascii


def test_1():
    a = b'worker'
    # 返回二进制数据的十六进制表示
    b = binascii.b2a_hex(a)
    print(b)
    # 与b2a_hex相反
    print(binascii.a2b_hex(b))

    # 这个功能和b2a_hex()一样
    c = binascii.hexlify(a)
    print(c)
    # 这个功能和a2b_hex()一样
    print(binascii.unhexlify(c))
    ######运行结果######
    # b'776f726b6572'
    # b'worker'
    # b'776f726b6572'
    # b'worker'


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
