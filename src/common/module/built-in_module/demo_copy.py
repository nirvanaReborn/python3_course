#!/usr/bin/env python
# -*- coding:utf-8 -*-

#
#
import copy


def test_1():
    spam = ['A', 'B', 'C', 'D']
    cheese = copy.copy(spam)
    cheese[1] = 42
    print(spam)  # ['A', 'B', 'C', 'D']
    print(cheese)  # ['A', 42, 'C', 'D']
    # 现在spam 和cheese 变量指向独立的列表


def test_2():
    # 如果要复制的列表中包含了列表，那就使用copy.deepcopy()函数来代替。
    spam = [[1, 3, 5], 'B', 'C', 'D']
    cheese = copy.deepcopy(spam)
    cheese[0][1] = 42
    print(spam)  # [[1, 3, 5], 'B', 'C', 'D']
    print(cheese)  # [[1, 42, 5], 'B', 'C', 'D']


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
