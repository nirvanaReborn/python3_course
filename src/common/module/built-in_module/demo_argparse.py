#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/yimiaoyikan/p/10375859.html
# 官网文档：https://docs.python.org/3/library/argparse.html
# 此模块是 Python 标准库中推荐的命令行解析模块。
# 还有另外两个模块可以完成同样的任务，称为 getopt(对应于 C 语言中的 getopt() 函数) 和被弃用的 optparse。
# 还要注意 argparse 是基于 optparse 的，因此用法与其非常相似。
import argparse


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("square", help="请输入一个数字", type=int)
    parser.add_argument("input_str", help="请输入一个字母", type=str)
    args = parser.parse_args()
    print(args.square ** 2)
    print(args.input_str)


if __name__ == "__main__":
    main()
