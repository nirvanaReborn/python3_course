#!/usr/bin/env python
# -*- coding:utf-8 -*-

#
#
import sysconfig


def main():
    print(sysconfig.get_makefile_filename()) # D:\ProgramFiles\anaconda3\Lib\config\Makefile
    print(sysconfig.get_config_var('LIBS'))
    print(sysconfig.get_config_var('LINKFORSHARED'))

    extension_suffix = sysconfig.get_config_var('EXT_SUFFIX')
    print(extension_suffix) # .cp311-win_amd64.pyd


if __name__ == "__main__":
    main()
