#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 这个模块用于检测图片文件的文件格式。

import imghdr
import sys

from public_function import GLOBAL_JPG_FILE


def test_1():
    print(imghdr.what(GLOBAL_JPG_FILE))  # jpeg


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
