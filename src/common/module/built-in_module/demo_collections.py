#!/usr/bin/env python
# -*- coding: UTF-8 -*-

'''常用的工具类：
namedtuple：命令元组，它是一个类工厂，接受类型的名称和属性列表来创建一个类。
deque：双端队列，是列表的替代实现。Python中的列表底层是基于数组来实现的，而deque底层是双向链表，
        因此当你需要在头尾添加和删除元素时，deque会表现出更好的性能，渐近时间复杂度为O(1)O(1)。
Counter：dict的子类，键是元素，值是元素的计数，它的most_common()方法可以帮助我们获取出现频率最高的元素。
        Counter和dict的继承关系我认为是值得商榷的，按照CARP原则，Counter跟dict的关系应该设计为关联关系更为合理。
OrderedDict：dict的子类，它记录了键值对插入的顺序，看起来既有字典的行为，也有链表的行为。
defaultdict：类似于字典类型，但是可以通过默认的工厂函数来获得键对应的默认值，相比字典中的setdefault()方法，这种做法更加高效。
'''
import collections


def test_help():
    help(collections)


def test_dir():
    for i in dir(collections):
        print(i)


def test_1():
    # 使用Counter()计数
    breakfast = ['spam', 'spam', 'eggs', 'spam']
    breakfast_counter = collections.Counter(breakfast)
    print(breakfast_counter)

    # 函数most_common() 以降序返回所有元素
    # 或者如果给定一个数字，会返回该数字前的的元素
    print(breakfast_counter.most_common())
    print(breakfast_counter.most_common(1))

    lunch = ['eggs', 'eggs', 'bacon']
    lunch_counter = collections.Counter(lunch)
    print(lunch_counter)

    # 组合计数器
    print(breakfast_counter + lunch_counter)
    print(breakfast_counter - lunch_counter)
    print(lunch_counter - breakfast_counter)
    print(breakfast_counter & lunch_counter)  # 交集运算
    print(breakfast_counter | lunch_counter)  # 并集运算,不同于合并，并集没有把计数加起来，而是取其中较大的值


def test_2():
    # 使用有序字典OrderedDict()按键排序
    from collections import OrderedDict
    quotes = OrderedDict([
        ('Moe', 'A wise guy, huh?'),
        ('Larry', 'Ow!'),
        ('Curly', 'Nyuk nyuk!'),
    ])

    for stooge in quotes:
        print(stooge)


def test_3():
    # deque 是一种双端队列，同时具有栈和队列的特征。
    # 它可以从序列的任何一端添加和删除项。
    # 现在，我们从一个词的两端扫向中间，判断是否为回文。
    # 函数popleft() 去掉最左边的项并返回该项，pop() 去掉最右边的项并返回该项。
    # 从两边一直向中间扫描，只要两端的字符匹配，一直弹出直到到达中间：

    from collections import deque
    def palindrome(word):
        dq = deque(word)
        while len(dq) > 1:
            if dq.popleft() != dq.pop():
                return False
        return True

    print(palindrome('radar'))
    print(palindrome('halibut'))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
