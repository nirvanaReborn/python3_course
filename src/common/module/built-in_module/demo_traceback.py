#!/usr/bin/env python
# -*- coding:utf-8 -*-

# traceback模块被用来跟踪异常返回信息

import traceback


def test_1():
    try:
        raise Exception(SyntaxError, "traceback test")
    except:
        traceback.print_exc()
        # traceback.print_exception()


# 蒋异常信息输出到指定文件
def test_2():
    try:
        num = 1 / 0
    except:
        with open(r'd:/a.txt', 'a') as f:
            traceback.print_exc(file=f)


def test_3():
    try:
        raise Exception('This is the error message.')
    except:
        from public_function import GLOBAL_ERROR_FILE
        errorFile = open(GLOBAL_ERROR_FILE, 'w')
        errorFile.write(traceback.format_exc())
        errorFile.close()
        print('The traceback info was written to ', GLOBAL_ERROR_FILE)


# 通常情况下我们无法将多线程中的异常带回主线程，所以也就无法打印线程中的异常。
# 通过上边学到这些知识，我们可以对线程做如下修改，从而实现捕获线程异常的目的。
def test_4():
    import threading

    def func():
        raise Exception("thread exception")

    class ExceptionThread(threading.Thread):

        def __init__(self, group=None, target=None, name=None, args=(), kwargs=None):
            """
            Redirect exceptions of thread to an exception handler.
            """
            threading.Thread.__init__(self, group, target, name, args, kwargs)
            if kwargs is None:
                kwargs = {}
            self._target = target
            self._args = args
            self._kwargs = kwargs
            self._exc = None

        def run(self):
            try:
                if self._target:
                    self._target()
            except BaseException as e:
                import sys
                self._exc = sys.exc_info()
            finally:
                # Avoid a refcycle if the thread is running a function with
                # an argument that has a member that points to the thread.
                del self._target, self._args, self._kwargs

        def join(self, timeout=None):
            threading.Thread.join(self)
            if self._exc:
                msg = "Thread '%s' threw an exception: %s" % (self.getName(), self._exc[1])
                raise Exception(msg)

    t = ExceptionThread(target=func, name='my_thread')
    t.start()
    try:
        t.join()
    except:
        traceback.print_exc()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
