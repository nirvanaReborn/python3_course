#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.pianshen.com/article/5341896719/
# https://docs.python.org/3/library/struct.html#module-struct
# 该模块的主要作用就是对python基本类型值与用python字符串格式表示的C struct类型间的转化
'''
http://blog.csdn.net/iloveyin/article/details/40743399
struct模块中最重要的三个函数是pack(), unpack(), calcsize()
pack(fmt, v1, v2, ...)    按照给定的格式(fmt)，把数据封装成字符串(实际上是类似于c结构体的字节流)
unpack(fmt, string)       按照给定的格式(fmt)解析字节流string，返回解析出来的tuple
calcsize(fmt)             计算给定的格式(fmt)占用多少字节的内存

1.格式化对照表
FORMAT       C TYPE                 PYTHON TYPE             STANDARD SIZE           NOTES
x            pad byte               no value                -                       -
c            char                   string of length 1      1                       -
b            signed char            integer                 1                       (1)(3)
B            unsigned char          integer                 1                       (3)
?            _Bool                  bool                    1                       (1)
h            short                  integer                 2                       (3)
H            unsigned short         integer                 2                       (3)
i            int                    integer                 4                       (3)
I            unsigned int           integer                 4                       (3)
l            long                   integer                 4                       (3)
L            unsigned long          integer                 4                       (3)
q            long long              integer                 8                       (2), (3)
Q            unsigned long long     integer                 8                       (2), (3)
f            float                  float                   4                       (4)
d            double                 float                   8                       (4)
s            char[]                 string                  -                       -
p            char[]                 string                  -                       -
P            void *                 integer                 -                       (5), (3)

提示：
signed char（有符号位）取值范围是 -128 到 127(有符号位)
unsigned char （无符号位）取值范围是 0 到 255

2.字节顺序，大小和校准


'''
import struct
import collections
import ctypes
import sys


def convert2bigendian(x):
    result = struct.pack('>BBBBBB', *x)
    return result


def test_0():
    # help(struct)

    # for i in dir(struct):
    #     print(i)

    # struct.calcsize()用于计算格式字符串所对应的结果长度。
    # 如果是4，说明是32位的；如果是其他的是，64位的。
    bit_length = 32 if struct.calcsize("P") == 4 else 64
    print(bit_length)  # 64
    print(struct.calcsize("<2l"))  # 8

    # https://blog.csdn.net/weixin_43936250/article/details/105372699
    # 使用系统内置函数判断计算机是大端还是小端模式
    print(sys.byteorder)  # little 或 big

    # 手动判断
    val = 0x12345678
    pk = struct.pack('i', val)
    hex_pk = hex(pk[0])
    if hex_pk == '0x78':
        print('小端')
    elif hex_pk == '0x12':
        print('大端')
    else:
        print('未知')


def test_1():
    # http://python.jobbole.com/81554/
    a = 20
    b = 400

    b_str = struct.pack("ii", a, b)  # 转换后的str虽然是字符串类型，但相当于其他语言中的字节流（字节数组），可以在网络上传输
    print('length:', len(b_str))  # length: 8
    print("type", type(b_str))  # type <class 'bytes'>
    print(repr(b_str))  # b'\x14\x00\x00\x00\x90\x01\x00\x00'
    a1, a2 = struct.unpack("ii", b_str)
    print(a1, a2)  # 20 400

    buf = ctypes.create_string_buffer(12)
    print(type(buf))  # <class 'ctypes.c_char_Array_12'>
    print(repr(buf.raw))  # b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
    struct.pack_into("iii", buf, 0, 1, 2, -1)
    print(repr(buf.raw))  # b'\x01\x00\x00\x00\x02\x00\x00\x00\xff\xff\xff\xff'
    print(struct.unpack_from('iii', buf, 0))  # (1, 2, -1)


def test_2():
    # native byteorder
    buffer = struct.pack("ihb", 1, 2, 3)
    # 函数str() 用于将值转化为适于人阅读的形式，而repr() 转化为供解释器读取的形式。
    print(repr(buffer))  # b'\x01\x00\x00\x00\x02\x00\x03'
    print(struct.unpack("ihb", buffer))  # (1, 2, 3)

    # data from a sequence, network byteorder
    data = [1, 2, 3]
    # !表示我们要使用网络字节顺序解析
    buffer = struct.pack("!ihb", *data)
    print(repr(buffer))  # b'\x00\x00\x00\x01\x00\x02\x03'
    print(struct.unpack("!ihb", buffer))  # (1, 2, 3)


def test_3():
    def read_file(file_path):
        person = []
        file = open(file_path, "rb")
        name = file.read(8)
        name = struct.unpack('<8s', name)[0]
        male = file.read(1)
        male = struct.unpack('<b', male)[0]
        age = file.read(2)
        age = struct.unpack('<H', age)[0]
        height = file.read(4)
        height = int.from_bytes(height, byteorder='little')
        # height = struct.unpack('<I', height)[0]
        weight = file.read(4)
        weight = struct.unpack('<f', weight)[0]
        feature_size = file.read(4)
        feature_size = int.from_bytes(feature_size, byteorder='little')
        features = []
        for i in range(feature_size // 4):
            feature_value = file.read(4)
            feature_value = struct.unpack('<f', feature_value)[0]
            features.append(feature_value)
        person.append({
            "name": name,
            "male": male,
            "age": age,
            "height": height,
            "weight": weight,
            "feature_size": feature_size,
            "features": features,
        })
        return person

    def write_file(file_path, person, features):
        file = open(file_path, "wb")
        # file.seek(0, 2)
        # 把字符串的地方转为字节类型,还要先转成utf-8的编码(否则报错string argument without an encoding)
        name = struct.pack('<8s', person['name'].encode('utf-8'))
        file.write(name)
        male = struct.pack('<B', person['male'])
        file.write(male)
        age = struct.pack('<H', person['age'])
        file.write(age)
        height = struct.pack('<I', person['height'])
        file.write(height)
        weight = struct.pack('<f', person['weight'])
        file.write(weight)
        features_size = struct.pack('<I', 5 * 4)
        file.write(features_size)
        for key, value in features.items():
            feature_value = struct.pack('<f', value)
            file.write(feature_value)
        file.close()

    person = collections.OrderedDict()
    person.update({'name': 'Jame'})
    person.update({'male': True})
    person.update({'age': 25})
    person.update({'height': 178})
    person.update({'weight': 64.0})
    features = {'Strength': 54.0,  # 力量
                'Intelligence': 78.0,  # 智力
                'Constitution': 32.0,  # 体力
                'Dexterity': 78.0,  # 敏捷
                'Mentality': 53.0  # 精神
                }
    file_path = "./person_info.pkg"
    write_file(file_path, person, features)
    person_info = read_file(file_path)
    print(person_info)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
