#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 官方文档: https://docs.python.org/zh-cn/3.7/library/ctypes.html
# https://www.cnblogs.com/gaowengang/p/7919219.html
"""
ctypes 作为 Python 和 C 联系的桥梁，它定义了专有的数据类型来衔接这两种编程语言。如下表:
---------------------------------------
Ctypes数据类型           C数据类型
---------------------------------------
c_char                    char
c_short                   short
c_int                     int
c_long                    long
c_float                   float
c_doule                   double
c_void_p                  void *
---------------------------------------
注：Python 中的类型，除了 None，int， long， Byte String，Unicode String 作为 C 函数的参数默认提供转换外，其它类型都必须显式提供转换。
None：对应 C 中的 NULL
int, long： 对应 C 中的 int，具体实现时会根据机器字长自动适配。
Byte String：对应 C 中的一个字符串指针 char * ，指向一块内存区域。
Unicode String ：对应 C 中一个宽字符串指针 wchar_t *，指向一块内存区域。

# https://blog.csdn.net/acdefghb/article/details/111223700
ctype常用函数:
ctypes.addressof(obj): 以整数形式返回一片memory的地址。
ctypes.memmove(dst, src, count): 与标准的C memmove库函数相同，将count个字节，从src负值到dst。
ctypes.sizeof(obj_or_type): 与标准C sizeof()函数相同，都是返回ctype内省或实例缓存区大小，以字节位单位。
ctypes.string_at(address[, size]): 返回从内存地址address开始的字符串。如果指定了size，则将其用作size，否则假定字符串以零结尾。
"""

import ctypes
from ctypes import *
import platform
import os
import binascii


def to_bytes(bytes_or_str):
    if isinstance(bytes_or_str, str):
        value = bytes_or_str.encode('utf-8')
    else:
        value = bytes_or_str
    return value  # Instance of bytes


def get_lib(lib_path=None):
    if lib_path:
        (filepath, filename) = os.path.split(lib_path)
        (shotname, extension) = os.path.splitext(filename)
        if platform.system() == 'Windows':
            lib_name = os.path.join(filepath, shotname + '.dll')
            libc = ctypes.cdll.LoadLibrary(lib_name)
            # struct.calcsize用于计算格式字符串所对应的结果长度。
            # 如果是4，说明是32位的；如果是其他的是，64位的。
            bit_length = 32 if __import__('struct').calcsize("P") == 4 else 64
            if bit_length == 32:
                # （需要32位版本的python运行）
                print("需要切换为32位版本的python运行")
            else:
                print("需要切换为64位版本的python运行")
        elif platform.system() == 'Linux':
            lib_name = os.path.join(filepath, shotname + '.so')
            libc = ctypes.cdll.LoadLibrary(lib_name)
        else:
            print("未识别的操作系统", platform.system())
            libc = None
            exit(1)
    else:
        if platform.system() == 'Windows':
            # （需要32位版本的python运行）
            libc = ctypes.cdll.LoadLibrary('msvcrt.dll')
        elif platform.system() == 'Linux':
            libc = ctypes.cdll.LoadLibrary('libc.so.6')
        else:
            print("未识别的操作系统", platform.system())
            libc = None
            exit(1)
    return libc


# ctypes 数据类型
def test_1():
    libc = get_lib()

    print(libc.printf('%s\n'.encode(), 'here!'.encode()))  # here!
    libc.printf('%S\n'.encode(), u'there!')  # there!
    libc.printf('%d\n'.encode(), 42)  # 42
    libc.printf('%ld\n'.encode(), 60000000)  # 60000000

    # libc.printf('%f\n', 3.14)          #>>> ctypes.ArgumentError
    # libc.printf('%f\n', c_float(3.14)) #>>> dont know why 0.000000
    libc.printf('%f\n'.encode(), ctypes.c_double(3.14))  # 3.140000


# 修改字符串
def test_2():
    # Python 默认的 string 是不可变的，所以不能传递 string 到一个 C 函数去改变它的内容，所以需要使用 create_string_buffer
    p = ctypes.create_string_buffer(5)
    print(ctypes.sizeof(p))  # 5
    print(repr(p.raw))  # b'\x00\x00\x00\x00\x00'
    print(repr(p.value))  # b''

    p.raw = to_bytes('Hi')
    print(repr(p.raw))  # b'Hi\x00\x00\x00'
    print(repr(p.value))  # b'Hi'
    print(p.value.decode())  # Hi

    p = ctypes.create_string_buffer(to_bytes("Hello"))
    print(ctypes.sizeof(p), repr(p.value))  # 6  b'Hello'
    print(p.value.decode())  # Hello


# 传递自定义参数类型到 C 函数
# ctypes 允许你创建自定义参数类型，它会自动去搜索自定义数据的 _as_parameter 属性，将其作为 C 函数的参数
def test_3():
    libc = get_lib()

    class Bottles(object):
        def __init__(self, number):
            self._as_parameter_ = number  # here only accept integer, string, unicode string

    bottles = Bottles(42)
    libc.printf('%d bottles of beer\n'.encode(), bottles)


# 也可以为你的数据定义 _as_parameter 属性
def test_4():
    libc = get_lib()

    class Bottles(object):
        def __init__(self):
            self._as_parameter_ = None  # only accept integer, string, unicode string

        @property
        def aspram(self):
            return self._as_parameter_

        @aspram.setter
        def aspram(self, number):
            self._as_parameter_ = number

    bottles = Bottles()
    bottles.aspram = 63
    libc.printf('%d bottles of beer\n'.encode(), bottles)


# 指定 C 函数的参数类型
def test_5():
    libc = get_lib()
    # 指定 C 函数的返回类型(默认为 int 类型)
    libc.printf.restype = ctypes.c_int
    # 指定 C 函数的参数类型
    libc.printf.argtypes = [ctypes.c_char_p, ctypes.c_char_p, ctypes.c_int, ctypes.c_double]

    libc.printf('String is "%s", Int is %d, Double is %f\n'.encode(), 'Hi'.encode(), 10, 2.2)
    libc.printf('%s, %d, %f\n'.encode(), 'X'.encode(), 2, 3)
    try:
        libc.printf("%d %d %d".encode(), 1, 2, 3)
    except ctypes.ArgumentError as e:
        print("*** ERROR: %s" % str(e))


# 指定 C 函数的返回值类型
# 如果不指定 C 函数的返回值， ctypes 默认返回 int 类型，如果要返回特定类型，需要指定返回类型 restype
def test_6(source_str=ctypes.c_char_p()):
    libc = get_lib()
    print('1->', libc.strchr('abcdefghij', ctypes.c_char(b'd')))

    # 指定 C 函数的返回类型(默认为 int 类型)
    libc.strchr.restype = ctypes.c_char_p

    print('2->', libc.strchr('abcdefghij', ctypes.c_char(b'd')))
    print('3->', libc.strchr('abcdefghij', b'd'))  # Note, here C function strchr not know what 'd' mean, so rerurn None

    libc.strchr.argtypes = [ctypes.c_char_p, ctypes.c_char]
    # if sys.version_info[0] == 2:
    # print('4->', libc.strchr('abcdefghij',  ctypes.c_char(b'd')))
    # else:
    print(source_str, type(source_str))
    if source_str.value is None:
        # source_str = ctypes.create_string_buffer(to_bytes(''))
        source_str = ''.encode("utf-8")
    # print(source_str, type(source_str))
    source_str = ctypes.create_string_buffer(to_bytes('abcdefghij'))
    print('5->', libc.strchr(source_str, ctypes.c_char(b'd')))
    print('6->', libc.strchr('abcdefghij'.encode(), 'd'.encode()).decode())


# 按引用传递参数
def test_7():
    libc = get_lib()
    i = ctypes.c_int()
    f = ctypes.c_float()
    s = ctypes.create_string_buffer(32)
    print('i.val =', i.value)
    print('f.val =', f.value)
    print('repr(s.value) =', repr(s.value))
    libc.sscanf('1 3.14 Hello'.encode(), '%d %f %s'.encode(), ctypes.byref(i), ctypes.byref(f), s)
    print('after, i.val =', i.value)
    print('after, f.val =', f.value)
    print('after, repr(s.value) =', repr(s.value.decode()))


# 结构体: ctypes 支持结构体的使用，从 Structure 类派生，数据放在 _fields_ 中
def test_8():
    class Point(ctypes.Structure):
        _fields_ = [('x', ctypes.c_int), ('y', ctypes.c_int)]

    point = Point(10, 20)
    print('point.x =', point.x)
    print('point.y =', point.y)

    point = Point(y=5)
    print('after, point.x =', point.x)
    print('after, point.y =', point.y)
    print()

    class Rect(ctypes.Structure):
        _fields_ = [('upperleft', Point), ('lowerright', Point)]

    rc = Rect(point)
    print('rc.upperleft.x = %d, rc.upperleft.y = %d' % (rc.upperleft.x, rc.upperleft.y))
    print('rc.lowerright.x = %d, rc.lowerright.y = %d' % (rc.lowerright.x, rc.lowerright.y))

    r = Rect(Point(1, 2), Point(3, 4))
    print('r.upperleft.x = %d, r.upperleft.y = %d' % (r.upperleft.x, r.upperleft.y))
    print('r.lowerright.x = %d, r.lowerright.y = %d' % (r.lowerright.x, r.lowerright.y))


# 位域
def test_9():
    class IntBit(ctypes.Structure):
        _fields_ = [('x', ctypes.c_uint, 2), ('y', ctypes.c_uint, 4)]

    IB = IntBit(1, 15)
    print('IB.x = %d' % IB.x)
    print('IB.y = %d' % IB.y)

    IB2 = IntBit(4, 16)
    print('-> IB2.x = %d' % IB2.x)
    print('-> IB2.y = %d' % IB2.y)


# 数组
def test_10():
    TenIntArrayType = ctypes.c_int * 10
    ta = TenIntArrayType(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    for item in ta:
        print(item, end='\t')
    print()

    class PointEx(ctypes.Structure):
        _fields_ = [('x', ctypes.c_int), ('y', ctypes.c_int)]

    class MyStruct(ctypes.Structure):
        _fields_ = [('a', ctypes.c_int), ('b', ctypes.c_int), ('pointex_array', PointEx * 4)]

    ms = MyStruct(4, 5, ((1, 1), (2, 2), (3, 3), (4, 4)))
    for item in ms.pointex_array:
        print('(item.x, item.y) = (%d, %d)' % (item.x, item.y))


# 指针
# ctypes中使用POINTER和pointer表示指针，在使用POINTER时需要设置指向的数据类型，而pointer则直接从变量中得到一个特定类型的指针。
# pointer(n) 返回的相当于指针左值T* p=&n，可以改变，可以取地址;
# byref(n) 返回的相当于C的指针右值&n，本身没有被分配空间。
def test_11():
    i = ctypes.c_int(42)
    # python 对象三要素：Id，Type，Value
    print(id(i), type(i), i.value)  # 1442669218888 <class 'ctypes.c_long'> 42
    print('before, i.value =', i.value)
    pi = ctypes.pointer(i)
    print(pi)  # <__main__.LP_c_long object at 0x0000020491924CC8>
    print(id(pi), type(pi), pi.contents.value)  # 1442669219016 <class '__main__.LP_c_long'> 42
    pi[0] = 57
    print('after, i.value =', i.value)
    print(id(pi), type(pi), pi.contents.value)

    # create NULL pointer
    null_ptr = ctypes.POINTER(ctypes.c_int)()
    print('bool(null_ptr) =', bool(null_ptr))  # False


# 类型转换 cast()
def test_12():
    class Bar(ctypes.Structure):
        _fields_ = [('count', ctypes.c_int), ('values', POINTER(ctypes.c_int))]

    bar = Bar()
    bar.count = 3
    bar.values = (ctypes.c_int * 3)(1, 2, 3)
    print(ctypes.sizeof(bar))  # 16
    for i in range(bar.count):
        print('bar.values[%d] = %d' % (i, bar.values[i]))

    bar.values = None  # 设置指针为NULL
    # use cast to convert
    # try:
    #     bar.values = (c_byte * 4)()
    # except TypeError as e:
    #     print('*** ERROR: %s' % str(e))

    # cast()函数需要两个参数，第一个是转换前的指针实例，第二个是目标指针类型。
    bar.values = ctypes.cast((ctypes.c_byte * 4)(), POINTER(ctypes.c_int))
    print(ctypes.sizeof(bar.values))  # 8
    print(bar.values)  # <__main__.LP_c_long object at 0x0000025928B26DC8>
    p = bar.values.contents.value
    print(p)  # 0
    print(type(p))  # <class 'int'>
    for idx in range(3):
        print('now, bar.values[%d] = %d' % (idx, bar.values[idx]))


# 回调函数 CFUNCTYPE()
# CFUNCTYPE(restype, *argtypes, **kw)
def test_13():
    libc = get_lib()

    IntArray5 = ctypes.c_int * 5
    ia = IntArray5(5, 1, 7, 33, 99)

    CmpFuncType = ctypes.CFUNCTYPE(ctypes.c_int,
                                   ctypes.POINTER(ctypes.c_int),
                                   ctypes.POINTER(ctypes.c_int))

    def py_cmp_func(a, b):
        if a[0] > b[0]:
            return 1
        elif a[0] < b[0]:
            return -1
        else:
            return 0

    cmpfunc = CmpFuncType(py_cmp_func)

    print('before sort, the ia list is: ', )
    for item in ia:
        print(item, end='\t')

    # void qsort (void* base, size_t num, size_t size, int (*compar)(const void*,const void*));
    libc.qsort(ia, len(ia), ctypes.sizeof(ctypes.c_int), cmpfunc)

    print('\nafter sort, the ia list is: ', )
    for item in ia:
        print(item, end='\t')


# Resize Space
# ctypes 提供了 resize 变量占用空间的方法，注意: 只能增大，不能小于原始空间。
def test_14():
    short_array = (ctypes.c_short * 4)(1, 2, 3, 4)
    print('sizeof(short_array) =', ctypes.sizeof(short_array))
    # get 8, means short_array take 8-byte memory
    print('len(short_array) =', len(short_array))
    print('before resize, short_array is: ', )
    for idx in range(len(short_array)):
        print(short_array[idx], end='\t')
    print()

    try:
        ctypes.resize(short_array, 4)
        # resize short_array to 4-byte, raise error, due to cannot resize smaller than original
    except ValueError as e:
        print('ERROR: %s' % str(e))

    ctypes.resize(short_array, 32)
    print('after succeed resize to 32-byte, now sizeof(short_array) =', ctypes.sizeof(short_array))
    print('after succeed resize to 32-byte, now len(short_array) =', len(short_array))
    print('after reszie, short_array is: ', )
    for idx in range(len(short_array)):
        print(short_array[idx], end='\t')


# Other
def test_15():
    class cell(ctypes.Structure):
        pass

    cell._fields_ = [('name', ctypes.c_char_p), ('next', ctypes.POINTER(cell))]

    c1 = cell()
    c2 = cell()
    c1.name = 'foo'.encode()
    c2.name = 'bar'.encode()
    c1.next = ctypes.pointer(c2)
    c2.next = ctypes.pointer(c1)

    p = c1
    for i in range(10):
        print(p.name.decode(), end='  ')
        p = p.next[0]


# 坑
# ctypes 相对于其它工具，使用起来有点繁琐，而且有很多坑，需要小心谨慎，
def test_16():
    class POINT(ctypes.Structure):
        _fields_ = [('x', ctypes.c_int), ('y', ctypes.c_int)]

    class RECT(ctypes.Structure):
        _fields_ = [('a', POINT), ('b', POINT)]

    p1 = POINT(1, 2)
    p2 = POINT(3, 4)
    rc = RECT(p1, p2)

    print('rc.a.x =', rc.a.x)
    print('rc.a.y =', rc.a.y)
    print('rc.b.x =', rc.b.x)
    print('rc.b.y =', rc.b.y)

    rc.a, rc.b = rc.b, rc.a

    print('after swap, bad result due to this is the pointer,')
    print('rc.a.x =', rc.a.x)
    print('rc.a.y =', rc.a.y)
    print('rc.b.x =', rc.b.x)
    print('rc.b.y =', rc.b.y)


# --------------------------------------------------------------------------
# https://blog.csdn.net/acdefghb/article/details/111223700
# 结构体与字符串的相互转换
def test_18():
    class ProMsgHdr(ctypes.Structure):
        _pack_ = 1  # 1字节对齐
        _fields_ = [('pro_fix_head', ctypes.c_ushort),
                    ('src_mac', ctypes.c_ubyte * 6),
                    ('dsr_mac', ctypes.c_ubyte * 6),
                    ('module_id', ctypes.c_ushort),
                    ('cli_crc', ctypes.c_ushort),
                    ('msg_id', ctypes.c_uint),
                    ('msg_len', ctypes.c_uint)
                    ]

    # ------------------------用python来解析字符串-------------------------------
    buf = b"676700000000000048555c0009bb02006de0020000000500000001001400005050"
    # 赋值
    msg_hdr = ProMsgHdr()
    # 得到类的长度
    msghdrlen = ctypes.sizeof(ProMsgHdr)

    # 将16进制转换为字符串
    str_bytes = binascii.unhexlify(buf)
    print(str_bytes)
    # 将msg_hdr地址指向str_bytes
    ctypes.memmove(ctypes.addressof(msg_hdr), str_bytes, msghdrlen)

    print("module id:%d, msg id:%d" % (msg_hdr.module_id, msg_hdr.msg_id))
    # module id:2, msg id:2

    # --------------------------用python来生成字符串------------------------
    # 得到类的实例
    msg_hdr2 = ProMsgHdr()
    # 赋值
    msg_hdr2.module_id = 2
    msg_hdr2.msg_id = 2
    # 得到字符串
    hdr_byte = ctypes.string_at(ctypes.addressof(msg_hdr2), ctypes.sizeof(msg_hdr2))
    print(hdr_byte)


# --------------------------------------------------------------------------
def test_19():
    # char类型指针：
    version = '中国'  # 先定义一个字符串变量；
    version = bytes(version, encoding='utf-8')  # 将字符串转为二进制类型；
    version_p = ctypes.c_char_p(version)  # 直接用c_char_p获取字符串指针，指向version，用version_p.value可获取指针指向的值；
    print(type(version_p), version_p)
    print(version_p.value.decode())

    # int类型指针：
    status = ctypes.c_int(56)  # 先构造一个c_int类型；
    status_p = ctypes.pointer(status)  # 生成一个指针对象，获取c_int指针。
    print(status_p.contents)  # 获取c_int的值: c_long(56)
    print(status_p.contents.value)  # 获取python格式的int的值: 56

    # int类型
    num = ctypes.c_int(1992)
    # int指针
    p = ctypes.POINTER(ctypes.c_int)
    print(p)
    # 对实例取地址，可作为指针参数传递
    ref = ctypes.byref(num)
    print(ref)


# --------------------------------------------------------------------------
# https://blog.csdn.net/arbboter/article/details/102756187
# 自定义基类结构体
class MyStructure(ctypes.Structure):
    # 结构体转字典
    def dump_dict(self):
        info = {}
        # 通过_fields_获取每一个字段
        # 检查每个字段的类型，根据不同类型分别处理
        # 支持递归迭代
        for k, v in self._fields_:
            av = getattr(self, k)
            if isinstance(v, ctypes.Structure):
                av = av.dump_dict()
            elif isinstance(v, ctypes.Array):
                av = ctypes.cast(av, ctypes.c_char_p).value.decode()
            else:
                pass
            info[k] = av
        return info

    # 字符串信息
    def string(self):
        return ctypes.string_at(ctypes.addressof(self), ctypes.sizeof(self))

    # 字符串转换
    def __str__(self):
        # return "{}: {{{}}}".format(self.__class__.__name__,
        #                            ", ".join(["{}: {}".format(field[0],
        #                                                       getattr(self,field[0]))
        #                                       for field in self._fields_]))
        return repr(self.dump_dict())

    # 打印信息
    def show(self):
        from pprint import pprint
        pprint(self.dump_dict())

    # 按表格输出二进制数据
    def show_meminfo(self, ctp=ctypes.c_char, row_num=8, full=False):
        """
        参数 ctp : 每一列的数据类型，其类型为ctypes数据类型，如c_char, c_int
        参数 row_num : 每一行的列数
        参数 full : 是否完整显示数据，如果是且结构体大小不是ctp大小的整数倍，则将ctp强制转为c_char类型
        """
        import struct
        # 长度不够时，取char类型
        if full and ctypes.sizeof(self) % ctypes.sizeof(ctp):
            ctp = ctypes.c_char
        block = int(ctypes.sizeof(self) / ctypes.sizeof(ctp))
        addr = ctypes.string_at(ctypes.addressof(self), ctypes.sizeof(self))
        for i in range(block):
            v = struct.unpack(ctp._type_, addr[i * ctypes.sizeof(ctp):(i + 1) * ctypes.sizeof(ctp)])
            pend = '\n' if (i + 1) % row_num == 0 else ' '
            print(repr(v[0]), end=pend)
        print()


def test_20():
    class ST_DATA(MyStructure):
        _fields_ = [
            ('day1', c_int * 3),
            ('day2', c_int * 4),
            ('day3', c_int * 5),
            ('day4', c_int * 6),
            ('day5', c_int * 7),
        ]

    # ST_DATA 测试代码
    data = ST_DATA()
    data.day1 = (c_int * 3)(11, 12, 13)
    data.day2 = (c_int * 4)(21, 22, 23, 24)
    data.day3 = (c_int * 5)(31, 32, 33, 34, 35)
    data.day4 = (c_int * 6)(41, 42, 43, 44, 45, 46)
    data.day5 = (c_int * 7)(51, 52, 53, 54, 55, 56, 57)
    data.show_meminfo(c_int, 4)


def test_21():
    # 地址资料
    class ST_ADDR(MyStructure):
        _fields_ = [
            ('Addr', c_char * 32),
            ('Port', c_int),
        ]

    # HOOK
    class ST_HOOK(MyStructure):
        _fields_ = [
            ('UserID', c_uint),  # 请求者的ID号码
            ('HostName', c_char * 64),  # 主机名
            ('QueueName', c_char * 64),  # 队列名
            ('QueueType', c_uint),  # 队列类型
        ]

    # ST_PACKHEAD:包头结构
    class ST_PACKHEAD(MyStructure):
        _fields_ = [
            ('RequestType', c_uint),  # 请求号码（交易编码）
            ('addr', ST_ADDR),  # 请求着的地址（6个子节）
            ('hook', ST_HOOK),  # 请求者的私有数据(通讯平台内部使用的)
            ('userdata', c_uint),  # 请求者用户数据（应答包会原样返回）
            ('ParmBits', c_ubyte * 64),  # 包体参数描述
        ]

    # ST_ADDR 测试代码
    addr = ST_ADDR()
    # 显示:{'Addr': '', 'Port': 0}
    addr.show()
    # 显示:{'Addr': '', 'Port': 0}
    print(addr)

    # 赋值
    addr.Addr = b'127.0.0.1'
    addr.Port = 8080
    # 显示:{'Addr': '127.0.0.1', 'Port': 8080}
    addr.show()
    print(addr._fields_[0][0], addr.Addr)
    print(addr._fields_[1][0], addr.Port)

    # ST_HOOK 测试代码
    hook = ST_HOOK()
    # 显示:{'HostName': '', 'QueueName': '', 'QueueType': 0, 'UserID': 0}
    hook.show()
    # 显示:{'UserID': 0, 'HostName': '', 'QueueName': '', 'QueueType': 0}
    print(hook)
    # 赋值
    hook.UserID = 578
    hook.HostName = b'127.0.0.1'
    hook.QueueName = b'q_req'
    hook.QueueType = 5205
    # 显示:{'UserID': 578, 'HostName': '127.0.0.1', 'QueueName': 'q_req', 'QueueType': 5205}
    print(hook)

    # ST_PACKHEAD 复杂结构体，嵌套结构体
    head = ST_PACKHEAD()
    # 显示内容如下:
    # 'ParmBits': '',
    # 'RequestType': 0,
    # 'addr': {'Addr': '', 'Port': 0},
    # 'hook': {'HostName': '', 'QueueName': '', 'QueueType': 0, 'UserID': 0},
    # 'userdata': 0}
    head.show()

    # 显示:{'RequestType': 0, 'addr': {'Addr': '', 'Port': 0}, 'hook': {'UserID': 0, 'HostName': '', 'QueueName': '', 'QueueType': 0}, 'userdata': 0, 'ParmBits': ''}
    print(head)

    # 赋值
    # head.ParmBits = ""
    head.RequestType = 404456
    head.userdata = 1234
    head.addr.Addr = b'127.0.0.1'
    head.hook = hook
    # 显示内容如下:
    # {'ParmBits': '',
    #  'RequestType': 404456,
    #  'addr': {'Addr': '127.0.0.1', 'Port': 0},
    #  'hook': {'HostName': '127.0.0.1',
    #           'QueueName': 'q_req',
    #           'QueueType': 5205,
    #           'UserID': 578},
    #  'userdata': 1234}
    head.show()


# --------------------------------------------------------------------------
# https://blog.csdn.net/cahesi/article/details/80256638
# ctypes传数组
'''
// list 包含 string
float maxlist(char ** modul, float * images, int num) {
	const char * split = ",";    // 用来进行切割
	char * p;
	float degree = 0;
 
	for ( int i = 0; i < num; i++ ){
		int j = 0;
		float degreetmp = 0;
		// split  用strok函数需要用，否则会告诉你不安全，要你用strok_s替代#pragma warning (disable : 4996)
		p = strtok( modul[i] , split );
		float ptmp = std::stof(p);    // this is much better way to do it
		while( p != NULL ){
			// 进行比较
			if (images[j] != ptmp){
				degreetmp = degreetmp + 10086 * float(ptmp + images[j]);
			}else{
				degreetmp += 1;
			}
			// 比较完继续遍历
			j++;
			p = strtok( NULL , split);
			if ( p != NULL)
				ptmp = std::stof(p);		
		}
		degreetmp = float(degreetmp/ j);
		if (degreetmp > degree){
			degree = degreetmp;
		}
	}
	return degree;
}
'''


def test_22():
    module = ['2.0, 2.0, 2.0, 2.0, 2.00', '2.0, 5.0, 2.4, 2.1, 2.0', '3.0, 1.0, 2.0, 2.5, 2.00']
    hist = [2, 2, 2, 2, 2]
    # print len(module)
    test = str(module)[1:-1]
    test_1 = str(hist)[1:-1]
    list_test = []

    # 这里简单粗暴一下，复制了三次。。。
    list_test.append(test.replace(' ', ''))
    list_test.append(test.replace(' ', ''))
    list_test.append(test.replace(' ', ''))

    test_2 = (c_char_p * 3)()
    for i in range(0, 3):
        # 这里给的是地址
        test_2[i] = list_test[i]

    test2 = (c_float * 5)()
    for i in range(0, 5):
        # 这里给的是数字
        test2[i] = hist[i]

    test_ = cdll.LoadLibrary("dll.dll")

    test_.maxlist.restype = c_float
    results1 = test_.maxlist(test_2, test2, 3)
    print("result1:", results1)


# --------------------------------------------------------------------------


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
