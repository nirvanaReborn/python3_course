#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# re.compile ：返回regrexobject对象， 用来重复使用regrexobject；
# re.match ：只从字符串的开始与正则表达式匹配，匹配成功返回matchobject，否则返回none；
# re.fullmatch: match函数的完全匹配（从字符串开头到结尾）版本
# re.search ：将字符串的所有字串尝试与正则表达式匹配，如果所有的字串都没有匹配成功，返回none，否则返回matchobject；（re.search相当于perl中的默认行为）
# re.ignorecase ：用来在匹配时忽略大小写；
# re.split ：使用给定的表达式来分割字符串；
# re.findall ：返回所有的与给定的表达式匹配的一个list；
# re.finditer ：返回所有与给定的表达式匹配的matchobject的迭代器；
# re.sub ：使用新的字符串替换表达式匹配的字符串；返回替换后的结果。
# re.subn ：使用新的字符串替换表达式匹配的字符串；返回一个元组，元组第一个元素是替换后的结果，第二个元素是替换后的总数。
# re.purge: 清除隐式编译的正则表达式的缓存
# re.escape

# 正则表达式可以包含一些可选标志修饰符来控制匹配的模式。修饰符被指定为一个可选的标志。
# 修饰符	描述
# re.I	    使匹配对大小写不敏感
# re.L	    做本地化识别（locale-aware）匹配
# re.M	    多行匹配，影响 ^ 和 $
# re.S	    使 . 匹配包括换行在内的所有字符
# re.U	    根据Unicode字符集解析字符。这个标志影响 \w, \W, \b, \B.
# re.X	    该标志通过给予你更灵活的格式以便你将正则表达式写得更易于理解。

# http://blog.csdn.net/u013679490/article/details/54598321
# 正则表达式一定要和Linux Shell 通配符需要分开，不然会很混乱的
# Linux shell通配符 * ? [a-z] {“a”,”x”} [!a-z] \

import re

# global_space_char
space_char = ' '

####################################################################################################
'''
Python 正则表达式入门（初级篇）
http://www.cnblogs.com/chuxiuhong/p/5885073.html

Python 正则表达式入门（中级篇）
http://www.cnblogs.com/chuxiuhong/p/5907484.html
'''


# 向前向后查找
def test_1():
    key = r"<html><body><h1>hello world<h1></body></html>"  # 这段是你要匹配的文本
    # 第一个?<=表示在被匹配字符前必须得有<h1>，后面的?=表示被匹配字符后必须有<h1>
    p1 = r"(?<=<h1>).+?(?=<h1>)"  # 这是我们写的正则表达式规则，你现在可以不理解啥意思
    pattern1 = re.compile(p1)  # 我们在编译这段正则表达式
    matcher1 = re.search(pattern1, key)  # 在源文本中搜索符合正则表达式的部分
    print(matcher1.group(0))  # hello world


def test_2():
    key = r"javapythonhtmlvhdl"  # 这是源文本
    p1 = r"python"  # 这是我们写的正则表达式
    pattern1 = re.compile(p1)  # 同样是编译
    matcher1 = re.search(pattern1, key)  # 同样是查询
    print(matcher1.group(0))  # python


def test_3():
    key = r"<h1>hello world<h1>"  # 源文本
    p1 = r"<h1>.+<h1>"  # 我们写的正则表达式，下面会将为什么
    pattern1 = re.compile(p1)
    # print(re.search(pattern1, key).group(0)) # <h1>hello world<h1>
    # 发没发现，我怎么写成findall了？咋变了呢？
    print(pattern1.findall(key))  # ['<h1>hello world<h1>']


def test_4():
    key = r"afiouwehrfuichuxiuhong@hit.edu.cnaskdjhfiosueh"
    p1 = r"chuxiuhong@hit\.edu\.cn"
    pattern1 = re.compile(p1)
    print(pattern1.findall(key))  # ['chuxiuhong@hit.edu.cn']


def test_5():
    key = r"http://www.nsfbuhwe.com and https://www.auhfisna.com"  # 胡编乱造的网址，别在意
    p1 = r"https*://"  # 看那个星号！
    pattern1 = re.compile(p1)
    print(pattern1.findall(key))  # ['http://', 'https://']


def test_6():
    key = r"lalala<hTml>hello</Html>heiheihei"
    p1 = r"<[Hh][Tt][Mm][Ll]>.+?</[Hh][Tt][Mm][Ll]>"
    pattern1 = re.compile(p1)
    print(pattern1.findall(key))  # ['<hTml>hello</Html>']


def test_7():
    key = r"mat cat hat pat"
    p1 = r"[^p]at"  # 这代表除了p以外都匹配
    pattern1 = re.compile(p1)
    print(pattern1.findall(key))  # ['mat', 'cat', 'hat']


# 贪婪匹配
def test_8():
    key = r"chuxiuhong@hit.edu.cn"
    p1 = r"@.+\."  # 我想匹配到@后面一直到“.”之间的，在这里是hit
    pattern1 = re.compile(p1)
    print(pattern1.findall(key))  # ['@hit.edu.']


# 懒惰匹配
def test_9():
    key = r"chuxiuhong@hit.edu.cn"
    p1 = r"@.+?\."  # 我想匹配到@后面一直到“.”之间的，在这里是hit
    pattern1 = re.compile(p1)
    print(pattern1.findall(key))  # ['@hit.']


def test_10():
    key = r"saas and sas and saaas"
    p1 = r"sa{1,2}s"
    pattern1 = re.compile(p1)
    print(pattern1.findall(key))  # ['saas', 'sas']


# 回溯引用
'''
看到\1了吗？原本那个位置应该是[1-6]，但是我们写的是\1，
我们之前说过，转义符\干的活就是把特殊的字符转成一般的字符，把一般的字符转成特殊字符。
普普通通的数字1被转移成什么了呢？
在这里1表示第一个子表达式，也就是说，它是动态的，是随着前面第一个子表达式的匹配到的东西而变化的。
比方说前面的子表达式内是[1-6]，在实际字符串中找到了1，那么后面的\1就是1，如果前面的子表达式在实际字符串中找到了2，那么后面的\1就是2。
类似的，\2,\3,....就代表第二个第三个子表达式。
所以回溯引用是正则表达式内的一个“动态”的正则表达式，让你根据实际的情况变化进行匹配。
'''


def test_0():
    key = r"<h1>hello world</h1>"
    p1 = r"<h([1-6])>.*?</h\1>"
    pattern1 = re.compile(p1)
    matcher1 = re.search(pattern1, key)
    print(matcher1.group(0))


####################################################################################################
# re.match首先是从字符串的头开始匹配的，如果匹配不到，就失败了，如果匹配到，访问m.group()
def test_11():
    regx = re.compile("abc")
    m11 = re.match(regx, "abcdefg")
    print(m11)
    print("m11 match group is:", m11.group())
    print("m11 match start is:", m11.start())
    print("m11 match span is:", m11.span())


def test_12():
    # 匹配除\n之外所有字符(.|\n) v
    m12 = re.match("ab(.|\n)cd", "ab\ncd")
    print("m12 match is ", m12.group())


def test_13():
    # \ 转义字符 ,可以用[]
    # m13 = re.match('\*',"*abc")
    m13 = re.match('[*]', "*abc")
    print("m13 match is ", m13.group())


def test_14():
    # […]字符集 匹配字符中任意一个字符,[^abc]内的^代表不包含[]内的字符集
    m14 = re.match("[^abcdef]", "zabcxyz")
    print("m14 match is ", m14.group())


def test_15():
    # * 匹配前一个字符0次或者多次，
    # linux shell 通配符*代表任意字符并且数量不限
    m31 = re.match("ab*", "abbbbbbbbbbc")
    print("m31.match is ", m31.group())

    m311 = re.match("ab*", "a")
    print("m311 match is ", m311.group())

    # -------------------------------------------------------------------------------
    # + 匹配前一个字符一次或者多次
    m32 = re.match("ab+", "abbbbbbbbbbc")
    print("m32 match is ", m32.group())

    m321 = re.match("ab+", "ab")
    print("m321 match is ", m321.group())

    # -------------------------------------------------------------------------------
    # ?匹配前一个字符0次或者1次
    m33 = re.match("ab?", "a")
    print("m33 match is ", m33.group())

    # -------------------------------------------------------------------------------
    # {m} 匹配前一个字符m次
    m34 = re.match("a{5}", "aaaaab")
    print("m34 match is ", m34.group())

    # -------------------------------------------------------------------------------
    # {m,n}匹配前一个字符每m次到n次至少m次，至多n次,m < n
    m35 = re.match("a{3,5}", "aaabbb")
    print("m35 match is ", m35.group())


def test_16():
    # ^匹配^后面一个字符开头的字符串
    m41 = re.match("^abc", "abcdef")
    print("m41 match is ", m41.group())

    # -------------------------------------------------------------------------------
    # $匹配以前面一个字符结尾的字符串
    m42 = re.search(r"c$", "2abc")
    print("m42 match is ", m42.group())

    # -------------------------------------------------------------------------------
    # \A 匹配\A后面一个字符开头的字符串
    m43 = re.match("\Aab", "abc")
    print("m43 match is ", m43.group())

    # -------------------------------------------------------------------------------
    # \Z以前一个字符结束的字符串
    m44 = re.search(r"c\Z", "abc")
    print("m44 match is ", m44.group())

    # -------------------------------------------------------------------------------
    # \b 匹配前面一个字符\w(单词字符)且后面一个字符\W(非单词字符)的字符串
    m45 = re.match(r"a\b#", "a#c")
    print("m45 match is ", m45.group())

    # -------------------------------------------------------------------------------
    # [^\b]

    # -------------------------------------------------------------------------------
    #  | 匹配|左右两边任意一串字符串
    m51 = re.match("abc|abd", "abdxyz")
    print("m51 match is ", m51.group())


def test_17():
    # () 作为分组匹配
    m52 = re.match("(abc)", "abcxabc")
    print("m52 match is ", m52.group())


def test_18():
    # (?P )分组，除原有编号外指定一个名为name的别名
    m53 = re.match("(?P<name>123)", "123")
    print("m53 match is ", m53.group())
    print("m531 dict is ", m53.groupdict())


def test_19():
    # \ r”(abc)-(\1)” 将编号为number的分组匹配到字符串
    m54 = re.match("(abc)-\\1", "abc-abc")
    print("m54 match is ", m54.group())

    m541 = re.match(r"(abc)-(\1)", "abc-abc")
    print("m541 match is ", m541.group())


def test_20():
    # (?P)(?P=name)分组 将别名为name的分组匹配到字符串
    m55 = re.match("(?P<name>abc)-(?P=name)", "abc-abc")
    print("m55 match is ", m55.group())
    print("m551 dict is ", m55.groupdict())

    # -------------------------------------------------------------------------------
    # (?#..) #后面的作为注释，
    m61 = re.match("asb(?#iambaby)123", "asb123")
    print("m61 match is ", m61.group())

    # -------------------------------------------------------------------------------
    # (?= ..) 前一个字符等于后一个字符才能匹配

    # -------------------------------------------------------------------------------
    # (?!…)

    # -------------------------------------------------------------------------------
    # (?<= …)

    # -------------------------------------------------------------------------------
    # (?

    # -------------------------------------------------------------------------------
    # (?(id/name)yes-pattern/no-pattern)

    # -------------------------------------------------------------------------------
    # 贪婪模式 m71.group(1) 打印出来是6-789-123 本来期望123456-789-123，是因为.+ 匹配了大部分数字
    m71 = re.match(".+(\d+-\d+-\d+)",
                   "abcedfasdfa;lasdfjasdfkasdf::123456-789-123")
    print("m71 match is ", m71.group(1))

    # -------------------------------------------------------------------------------
    # 非贪婪模式 .+? *? ?? {m,n}?            非贪婪模式打印出来是123456-789-123
    m72 = re.match(".+?(\d+-\d+-\d+)",
                   "abcedfasdfa;lasdfjasdfkasdf::123456-789-123")
    print("m72 matchis is ", m72.group(1))

    # -------------------------------------------------------------------------------


def test_21():
    # 预定义字符 \d 等价于 [0-9]
    m21 = re.match("\d", "321a")
    print("m21 match is ", m21.group())


def test_22():
    # 预定义字符\D 等价于[^0-9]
    m22 = re.match("\D", "a321")
    print("m22 match is ", m22.group())


def test_23():
    # \s 空白字符 等价于[\t\r\n\f\v]
    m23 = re.match("\s", "    abc")
    print("m23 match is ", m23.group())


def test_24():
    # \S 非空白字符 等价于[^\s]
    m24 = re.match("\S", "abc")
    print("m24 match is ", m24.group())


def test_25():
    # 单词字符 \w [A-Za-z0-9]
    m25 = re.match("\w", "abc")
    print("m25 match is ", m25.group())


def test_26():
    # \W 等价于 [^\w]
    m26 = re.match("\W", "?abc")
    print("m26 match is ", m26.group())


####################################################################################################


# http://blog.csdn.net/u013679490/article/details/54598321
# http://wiki.ubuntu.org.cn/Python正则表达式操作指南
# https://www.ibm.com/developerworks/cn/opensource/os-cn-pythonre/
# -------------------------------------------------------------------------------
# 返回pattern对象
def test_31():
    p = re.compile("abc")
    m81 = re.match(p, "abc")
    print("m81 match is ", m81.group())


# -------------------------------------------------------------------------------
# re.match(pattern,string,flags)
def test_32():
    m82 = re.match("(ben1949)-(\d{4}-\d{2}-\d{2})-(\\1)",
                   "ben1949-2017-01-18-ben1949xxxyyywww")
    print("m82 match is ", m82.group(1), m82.group(2))


# -------------------------------------------------------------------------------
# re.search(pattern,string,flags)
def test_33():
    m83 = re.search("(ben1949)-(\d{4}-\d{2}-\d{2})-(\\1)",
                    "aaaaaaben1949-2017-01-18-ben1949xxxyyywwwaaaaaa")
    print("m83 match is ", m83.group(1), m83.group(2))


# -------------------------------------------------------------------------------
# re.split(pattern,string)
def test_34():
    m84 = re.split("-", "ben1949-2017-01-18-ben1949xxxyyywww")
    print("m84 is ", m84)  # ['ben1949', '2017', '01', '18', 'ben1949xxxyyywww']

    m84 = re.split("-", "ben1949-2017-01-18-ben1949xxxyyywww", maxsplit=2)
    print("m84 is ", m84)  # ['ben1949', '2017', '01-18-ben1949xxxyyywww']
# -------------------------------------------------------------------------------
# re.findall(pattern,string,flags)
def test_35():
    m85 = re.findall("-", "ben1949-2017-01-18-ben1949xxxyyywww")
    print("m85 is ", m85)


# -------------------------------------------------------------------------------
# re.finditer(pattern,string,flags) 返回的是迭代器
def test_36():
    m86 = re.finditer("-", "ben1949-2017-01-18-ben1949xxxyyywww")
    for m in m86:
        print("m86 is ", m.group())


# -------------------------------------------------------------------------------
# re.sub(pattern,repl,string): 使用新的字符串替换表达式匹配的字符串；返回替换后的结果。
# 将书籍卖价都提高2.02
def test_37():
    def func(m):
        price = float(m.group(2))
        price += 2.02
        return "%s%s" % (str.ljust(m.group(1).strip(), 10, "."), price)

    str3 = ["english  100.0", "china       120.0"]
    m87 = []

    for i in range(2):
        m87.append(re.sub(r"(\w+\s+)(\d+\.?\d?)", func, str3[i]))

    for i in range(2):
        print("m87 is %s" % (m87[i]))


# -------------------------------------------------------------------------------
# re.subn(pattern,repl,string):使用新的字符串替换表达式匹配的字符串；返回一个元组，元组第一个元素是替换后的结果，第二个元素是替换后的总数。
def test_38():
    def func(m):
        price = float(m.group(2))
        price += 2.02
        return "%s%s" % (str.ljust(m.group(1).strip(), 10, "."), price)

    str3 = ["english  100.0", "china       120.0"]
    m88 = []
    for i in range(2):
        m88.append(re.subn(r"(\w+\s+)(\d+\.?\d?)", func, str3[i]))

    # 列表的元素是元组
    # print m88
    # print m88[0]
    # print m88[1]
    for i in range(2):
        print("m88 is ", (m88[i]))
        # print "m88 is %s" % (m88[i])


# -------------------------------------------------------------------------------
# (?P<name>...)
def test_39():
    print(re.sub("(?P<year>\d{4})-(?P<month>\d{2})-(?P<date>\d{2})",
                 "\g<date>-\g<month>-\g<year>", "2017-01-18"))  # 18-01-2017

    # 因为存在转义字符，因此我们需要用r表示原始字符，避免使用了转义后的字符
    print(re.sub("(\d{4})-(\d{2})-(\d{2})", r"\3-\2-\1", "2017-01-18"))  # 18-01-2017
    print(re.sub("(\d{4})-(\d{2})-(\d{2})", r"\3/\2/\1", "2017-01-18"))  # 18/01/2017
    print(re.sub("(\d{4})-(\d{2})-(\d{2})", r"\2/\3/\1", "2017-01-18"))  # 01/18/2017


####################################################################################################
def test_replace():
    s1 = 'tea for too'.replace('too', 'two')
    print(s1)


def test_search():
    s1 = "bb 3 cc 45 dd 88 gg 89"
    m = re.search("\d+", s1)
    # 只返回第一个匹配
    print(m.group())  # 3


# http://www.111cn.net/phper/157/37171_1.htm
def test_search_match():
    s1 = "helloworld, i am 30 !"

    w1 = "world"
    m1 = re.search(w1, s1)
    if m1:
        print("find : %s" % m1.group())  # find : world

    m3 = re.match(w1, s1)
    if m3 == None:
        print("cannot match")  # cannot match

    w2 = "helloworld"
    m2 = re.match(w2, s1)
    if m2:
        print("match : %s" % m2.group())  # match : helloworld


def test_compile():
    regex = '\d{3}-\d{7}'

    regexobject = re.compile(regex)
    print(regexobject.search("ccc 010-123456"))  # None
    print(regexobject.search("aaa 027-4567892").group())  # 027-4567892
    print(regexobject.search("bbb 021-1234567").group())  # 021-1234567


def test_ignorecase():
    print(re.search('world', "hello world !"))
    print(re.search('world', "hello world !").group())
    # AttributeError: module 're' has no attribute 'ignorecase'
    print(re.search('world', "hello world !", re.ignorecase).group())


def test_matchobject():
    m = re.match(r"(?P<year>\d{4})-(?P<month>\d{2})-(?P<date>\d{2})", "2010-10-01, i am very happy")
    print(m.group())  # 2010-10-01
    print(m.group(0))  # 2010-10-01

    print(m.groups())  # ('2010', '10', '01')

    print(m.group(1))  # 2010
    print(m.groups()[0])  # 2010

    print(m.group(2))  # 10
    print(m.groups()[1])  # 10

    print(m.group(3))  # 01
    print(m.groups()[2])  # 01

    print(m.groupdict())  # {'year': '2010', 'month': '10', 'date': '01'}

    print(m.lastindex)  # 3


def testreandmatchobjectmethonds():
    # split findall finditer sub
    s1 = "i am working in microsoft !"
    list_1D = re.split("\s+", s1)
    print(list_1D)  # ['i', 'am', 'working', 'in', 'microsoft', '!']

    s2 = "aa 12 bb 3 cc 45 dd 88 gg 89"
    l2 = re.findall('\d+', s2)
    print(l2)  # ['12', '3', '45', '88', '89']

    it = re.finditer('\d+', s2)  # it is one iterator type
    for i in it:  # i is matchobject type
        print(i.group())  # 12\n   3\n   45\n    88\n    89\n

    s3 = re.sub('\d+', '200', s2)
    print(s3)  # aa 200 bb 200 cc 200 dd 200 gg 200


# python正则表达式获取IP
def getIPAddFromFile(fobj):
    regex = re.compile(r'\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b', re.IGNORECASE)
    ipadds = re.findall(regex, fobj)
    print(ipadds)
    return ipadds


# python正则表达式获取手机号
def getPhoneNumFromFile(fobj):
    regex = re.compile(r'1\d{10}', re.IGNORECASE)
    phonenums = re.findall(regex, fobj)
    print(phonenums)
    return phonenums


# python正则表达式获取邮箱
def getMailAddFromFile(fobj):
    regex = re.compile(r"\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\b", re.IGNORECASE)
    mails = re.findall(regex, fobj)
    print(mails)
    return mails


# python正则表达式获取网址
def getUrlFromFile(fobj):
    regex = re.compile(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*,]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", re.IGNORECASE)
    urls = regex.findall(fobj)
    print(urls)
    return urls


def test_99():
    _sys_version_parser = re.compile(
        r'([\w.+]+)\s*'  # "version<space>"
        r'(?:\|[^|]*\|)?\s*'  # version extra
        r'\(#?([^,]+)'  # "(#buildno"
        r'(?:,\s*([\w ]*)'  # ", builddate"
        r'(?:,\s*([\w :]*))?)?\)\s*'  # ", buildtime)<space>"
        r'\[([^\]]+)\]?', re.ASCII)  # "[compiler]"

    # sys_version = '3.5.2 |Continuum Analytics, Inc.| (default, Jul  2 2016, 17:53:06) \n[GCC 4.4.7 20120313 (Red Hat 4.4.7-1)]'
    sys_version = '3.5.2 |Anaconda 4.2.0 (64-bit)| (default, Jul  2 2016, 17:53:06) \n[GCC 4.4.7 20120313 (Red Hat 4.4.7-1)]'
    match = _sys_version_parser.match(sys_version)
    print(match.groups())


def test_100():
    list_func = ['sys.version', ]


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    dict_choice.update({
        "h": "test_help()",
        "d": "test_dir()",
        "51": "test_replace()",
        "52": "test_search()",
        "53": "test_search_match()",
        "54": "test_compile()",
        "55": "test_ignorecase()",
        "56": "test_matchobject()",
        "57": "testreandmatchobjectmethonds()",
        "58": "getIPAddFromFile()",
        "59": "getPhoneNumFromFile()",
        "60": "getMailAddFromFile()",
        "61": "getUrlFromFile()",
    })
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
