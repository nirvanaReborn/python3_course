#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/lilyxiaoyy/p/10942922.html
# hex-based message authentication code(哈希消息认证码)
# 需要注意传入的key和message都是bytes类型，str类型需要首先编码为bytes。
import hmac


def test_1():
    h = hmac.new(b'This is my secret key')
    h.update('Hello world'.encode())
    print(h.hexdigest())  # b8908a20bd70f465330b434e18441d3b


def test_2():
    secret_key1 = b'This is my secret key'
    message1 = b'Hello world'
    hex_res1 = hmac.new(secret_key1, message1, digestmod="MD5").hexdigest()
    print(hex_res1)  # b8908a20bd70f465330b434e18441d3b

    secret_key2 = b'This is my secret key'
    message2 = b'Hello world'
    hex_res2 = hmac.new(secret_key2, message2, digestmod="MD5").hexdigest()
    print(hex_res2)  # b8908a20bd70f465330b434e18441d3b

    compare_res = hmac.compare_digest(hex_res1, hex_res2)  # 比较两个密文是否相同
    print(compare_res)  # True

    secret_key3 = b'This is my secret key'
    message3 = b'Hello world!'
    hex_res3 = hmac.new(secret_key3, message3, digestmod="MD5").hexdigest()
    print(hex_res3)  # a314490e13ff3d1dfa9cd18db8c4c3e8

    compare_res = hmac.compare_digest(hex_res1, hex_res3)  # 比较两个密文是否相同
    print(compare_res)  # False

    hmac_hex = hmac.new(secret_key3, message3, digestmod='md5').hexdigest()
    print(hmac_hex)  # a314490e13ff3d1dfa9cd18db8c4c3e8

    print(hmac.new(secret_key3, message3, 'md5').digest())  # b'\xa3\x14I\x0e\x13\xff=\x1d\xfa\x9c\xd1\x8d\xb8\xc4\xc3\xe8'
    print(len(hmac.new(secret_key3, message3, 'md5').digest()))  # 16

    content = "hello world"
    content_bytes = content.encode("utf-8")
    content_bytes_upper = content_bytes.upper()  # 今天才知道,还可以对bytes进行upper
    print(content_bytes_upper.decode("utf-8"))  # HELLO WORLD


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
