#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://docs.python.org/zh-tw/3/library/faulthandler.html
# 转储 Python 的跟踪信息
import sys
import faulthandler

# 方法一
# faulthandler.dump_traceback(file=sys.stderr, all_threads=True)
# 方法二
# faulthandler.enable(file=sys.stderr, all_threads=True)
faulthandler.enable(file=open("1.log", "a+"), all_threads=True)
# 通过signal建立关系
# faulthandler.register(signal.SIGUSR1,file=open("./tt.txt", "w+"))


def test_1():
    import ctypes
    ctypes.string_at(0)


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
