#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 这个模块用于在不同的颜色系统（RGB、YIQ、HSV 以及HLS）间进行转换。
# http://blog.csdn.net/jy692405180/article/details/72758054
'''
colorsys模块提供了用于RGB和YIQ/HLS/HSV颜色模式的双向转换的接口。
它提供了六个函数，其中三个用于将RGB转YIQ/HLS/HSV，另外三个用于将YIQ/HLS/HSV转为RGB。

colorsys.rgb_to_yiq(r, g, b) 
colorsys.rgb_to_hls(r, g, b) 
colorsys.rgb_to_hsv(r, g, b)

colorsys.yiq_to_rgb(y, i, q) 
colorsys.hls_to_rgb(h, l, s) 
colorsys.hsv_to_rgb(h, s, v)

需要注意的是：除了I和Q之外的其他参数取值都是在[0, 1]范围内的浮点数。
所以传入RGB参数的时候还需要额外做一个除以255的操作。
'''
import colorsys


def test_1():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
