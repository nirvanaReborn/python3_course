#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/franknihao/p/7326849.html
# 对已经销毁的对象，Python不会自动释放其占据的内存空间。
# 为了能够充分地利用分配的内存，避免程序跑到一半停止，要时不时地进行内存回收，这时候gc（garbage collector）就隆重登场啦！
# 通过Python扩展模块gc 来查看不能回收的对象的详细信息。
# 内存泄漏指由于疏忽或错误造成程序未能释放已经不再使用的内存。
# 内存泄漏并非指内存在物理上的消失，而是应用程序分配某段内存后，
# 由于设计错误，导致在释放该段内存之前就失去了对该段内存的控制，从而造成了内存的浪费。
# 有__del__() 函数的对象间的循环引用是导致内存泄露的主凶。
# 不使用一个对象时使用: del object 来删除一个对象的引用计数就可以有效防止内存泄露问题。
# 可以通过 sys.getrefcount(obj) 来获取对象的引用计数，并根据返回值是否为0来判断是否内存泄露。

import gc
import sys


def main():
    a = [1]
    b = [2]
    a.append(b)
    b.append(a)
    ####此时a和b之间存在循环引用####
    sys.getrefcount(a)  # 结果应该是3
    sys.getrefcount(b)  # 结果应该是3
    del a
    del b
    ####删除了变量名a，b到对象的引用，此时引用计数应该减为1，即只剩下互相引用了####
    try:
        sys.getrefcount(a)
    except UnboundLocalError:
        print('a is invalid')

    ####此时，原来a指向的那个对象引用不为0，python不会自动回收它的内存空间####
    ####但是我们又没办法通过变量名a来引用它了，这就导致了内存泄露####
    unreachable_count = gc.collect()
    ####gc.collect()专门用来处理这些循环引用，返回处理这些循环引用一共释放掉的对象个数。这里返回是2####


if __name__ == "__main__":
    main()
