#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://docs.python.org/zh-cn/3.11/py-modindex.html
# python自带的标准库

import sys
import pkgutil


def test_1():
    std_lib_names = []
    for importer, modname, ispkg in pkgutil.walk_packages(path=sys.path, prefix='_'):
        std_lib_names.append(modname)

    print(std_lib_names)


def test_2():
    all_modules = []
    for importer, modname, ispkg in pkgutil.walk_packages(path=sys.path):
        all_modules.append(modname)

    print(all_modules)


def test_3():
    # 输出所有已安装的标准库
    for importer, modname, ispkg in pkgutil.iter_modules():
        print(modname)


def test_4():
    from distutils import sysconfig
    import os
    std_lib = sysconfig.get_python_lib(standard_lib=1)
    for top, dirs, files in os.walk(std_lib):
        for nm in files:
            if nm != '__init__.py' and nm[-3:] == '.py':
                lib_name = os.path.join(top, nm)[len(std_lib) + 1:-3].replace(os.sep, '.')
                if lib_name.startswith('site-packages.'):
                    continue
                print(lib_name)


def test_5():
    import requests
    from bs4 import BeautifulSoup

    standard_library = []
    url = "https://docs.python.org/zh-cn/3.11/py-modindex.html"
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")
    all_div = soup.find_all('code', attrs={'class': 'xref'})
    for row in all_div:
        standard_library.append(row.text)

    module_file = r"D:\python标准库.txt"
    with open(module_file, 'w', encoding='utf-8', errors='ignore') as f:
        f.write('\n'.join(standard_library))


def main():
    # dict_choice = {}
    # for i in range(0, 100):
    #     dict_choice[str(i)] = "test_{}()".format(str(i))
    #
    # choice = str(input("Enter a positive integer to choice: "))
    # if choice in dict_choice.keys():
    #     exec(dict_choice[choice])
    # else:
    #     print("输入有误，请重输！", choice)
    test_5()


if __name__ == '__main__':
    main()
