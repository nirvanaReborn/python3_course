#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://www.pythondoc.com/pythontutorial3/stdlib.html
# 【Run】⇒ 【Edit Configurations】⇒ 【Script parameters】 ⇒ one two three

# os提供操作系统的接口，常用的有文件系统相关和进程相关
# sys提供python解释器系统的通用配置和函数，影响着解释器的行为
# platform提供平台相关的信息


import sys


# sys.setrecursionlimit(10000) #例如这里设置为一万


def test_help():
    help(sys)


def test_dir():
    for item in dir(sys):
        print(item)


# 通用工具脚本经常调用命令行参数。这些命令行参数以链表形式存储于 sys 模块的 argv 变量。
def test_1():
    print(sys.argv)
    print(len(sys.argv))
    print(sys.getsizeof(sys.argv))
    default_encoding = sys.getdefaultencoding()
    filesystem_encoding = sys.getfilesystemencoding()
    print(f"Default encoding: {default_encoding}")
    print(f"Filesystem encoding: {filesystem_encoding}")


# 打印当前文件名、当前函数名、当前行号
def test_2():
    print("当前文件名:", sys._getframe().f_code.co_filename)
    print("当前的行号:", sys._getframe().f_lineno)
    print('当前函数名:', sys._getframe().f_code.co_name)
    # 文件名:行号,函数名
    print("{}:{},{}".format(sys._getframe().f_code.co_filename, sys._getframe().f_lineno, sys._getframe().f_code.co_name))


# 错误输出重定向
def test_3():
    sys.stderr.write('Warning, log file not found starting a new one\n')


# 大多脚本的直接终止都使用 sys.exit()。
def test_4():
    while True:
        print('Type exit to exit.')
        response = input()
        if response == 'exit':
            sys.exit(0)  # 退出程序，正常退出时exit(0)
        print('You typed ' + response + '.')


def test_5():
    # python版本而非os版本
    print(sys.version)

    # 最大嵌套调用层数
    print(sys.getrecursionlimit())  # 1000

    # 获取对象的引用计数,并根据返回值是否为0来判断是否内存泄露
    print(sys.getrefcount(1))  # 3126

    # print(sys.maxint)
    print(sys.maxsize)  # 9223372036854775807
    print(sys.maxunicode)  # 1114111


# 查看当前平台
def test_6():
    '''
    平台                             值
    Linux (2.x and 3.x)             'linux2'
    Windows                         'win32' # 实际上这个`win32`应该是指Win32 API
    Windows/Cygwin                  'cygwin'
    Mac OS X                        'darwin'
    OS/2                            'os2'
    OS/2 EMX                        'os2emx'
    RiscOS                          'riscos'
    AtheOS                          'atheos'
    '''
    print(sys.platform)


# 查看自己的python项目都导入了那些模块，
# 以下只列出了当前已经import的module，磁盘上没有import的不会被列出来。
# 之后进入/usr/local/lib/pythonx.x/  这里看
def test_7():
    for module in sys.modules:
        print(module)


# sys.path(模块搜索路径，不是os的环境变量)
def test_8():
    for search_path in sys.path:
        print(search_path)


def test_9():
    # 判断计算机是大端还是小端模式
    print(sys.byteorder)  # little 或 big
    print("本机存储模式是:{} Endian.\n".format(sys.byteorder.capitalize()))


# ------------------------------------------------------------------------------
def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
