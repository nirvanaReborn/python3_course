#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
# https://blog.csdn.net/budong282712018/article/details/80281267
# https://selfboot.cn/2016/06/13/python_performance_analysis/
# 一般的测试最多统计到函数的执行时间，很多时候我们想知道函数里面每一行代码的执行效率，这时候就可以用到 line_profiler 了。
# line_profiler 的使用特别简单，在需要监控的函数前面加上 @profile 装饰器。然后用它提供的 kernprof -l -v [source_code.py] 行进行诊断。
line_profiler 执行时间的估计不是特别精确，不过可以用来分析当前函数中哪些行是瓶颈。
输出每列的含义如下：
    Line #: 行号
    Hits: 当前行执行的次数.
    Time: 当前行执行耗费的时间，单位为 “Timer unit:”
    Per Hit: 平均执行一次耗费的时间.
    % Time: 当前行执行时间占总时间的比例.
    Line Contents: 当前行的代码

在安装过程中遇到问题，比如文件缺失，说明没有安装相关依赖，在Ubuntu中
sudo apt-get install python-dev libxml2-dev libxslt-dev
'''
import os

from line_profiler import profile
import numpy as np
import func_timeout
import time


@profile
def test_1():
    a = np.full(shape=(600, 700), fill_value=99.0)
    return a



@func_timeout.func_set_timeout(5)
@profile
def test_2():
    try:
        time.sleep(10)
    except TimeoutError:
        print("处理超时")


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
