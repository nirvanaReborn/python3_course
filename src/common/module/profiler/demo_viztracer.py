#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://github.com/gaogaotiantian/viztracer
# https://www.bilibili.com/video/BV1d34y1C78W
# https://mp.weixin.qq.com/s/9ZL4t_L3_CESW3eST5Oqbg
# https://zhuanlan.zhihu.com/p/705608942
# VizTracer：一款轻量级 Python 代码可视化分析和调试利器
"""
• 时间轴可视化： 以时间轴的形式展示每个函数的调用过程，包括函数进入、退出时间以及源码。
• 多线程支持： 无需修改代码即可追踪多线程程序的执行过程，并直观地展现线程之间的交互关系。
• 多进程支持： 支持 Subprocess、Multiprocessing、os.fork()、concurrent.futures 和 Loky 等多进程场景。
• 异步支持： 支持 asyncio 异步编程，并提供选项增强异步代码的追踪效果。
• 火焰图生成： 可以生成火焰图，帮助快速识别代码中的性能瓶颈。
• 远程调试： 支持远程连接到任意 Python 进程进行追踪和调试。
• 灵活的过滤功能： 可以根据函数名、文件路径、执行时间等条件对追踪数据进行过滤，降低内存占用并提高追踪效率。
• 自定义事件记录： 允许用户在代码中添加自定义事件，以便更精准地分析程序行为。

# viztracer my_script.py arg1 arg2
# 该命令将会生成一个名为 result.json 的追踪文件，可以使用 vizviewer 命令打开该文件：vizviewer result.json
# 生成火焰图: vizviewer --flamegraph result.json
# 生成HTML报告: viztracer -o result.html scalene_data.py
"""
# pip install viztracer
from viztracer import VizTracer
from viztracer import get_tracer
import time
import numpy as np


def process_data(data=None):
    def compute(data):
        result = []
        for item in data:
            result.append(np.sin(item) ** 2 + np.cos(item) ** 2)
        return result

    if data is None:
        data = np.random.rand(1000000)
    start_time = time.time()
    compute(data)
    print(f"Computation took {time.time() - start_time} seconds")


# 自定义事件追踪
def test_1():
    tracer = VizTracer()
    tracer.start()

    # 要追踪的代码
    result = process_data()

    tracer.stop()
    # tracer.save("result.json")
    tracer.save("result.html")


# 动态追踪
def test_2():
    tracer = get_tracer()
    tracer.start()

    # 需要追踪的代码段
    result = process_data()

    tracer.stop()
    tracer.save("result.json")


# 多线程和多进程支持
def test_3():
    from concurrent.futures import ThreadPoolExecutor

    def thread_task(data):
        return process_data(data)

    with VizTracer(output_file="optional.json") as tracer:
        with ThreadPoolExecutor(max_workers=4) as executor:
            results = list(executor.map(thread_task, [data1, data2, data3, data4]))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
