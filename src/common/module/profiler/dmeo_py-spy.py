#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://zhuanlan.zhihu.com/p/358466229
# 如果你想对正在运行的python进程进行性能分析，cProfile则无能为力，因为程序已经在运行，
# 如果是生产环境，不可能为了配合你分析性能而重新启动程序。
# Py-spy可以对正在运行的python进程进行分析，其原理是获取调用堆栈:
# 在linux系统下，通过process_vm_readv，
# 在mac下使用vm_read，
# 在windows上使用ReadProcessMemory。


# 下面语句是一种特殊的导入语句，它允许你在 Python 2.x 中使用 Python 3.x 的除法行为。
from __future__ import division
"""
没有 from __future__ import division 的情况下：
    / 执行整数除法（如果是两个整数）。
    // 执行浮点除法或整数除法，取决于操作数。

有 from __future__ import division 的情况下：
    / 总是执行浮点除法。
    // 执行整数除法（行为与 Python 3.x 相同）。
"""


def main():
    def get_pi(number):
        number1 = number + 10
        b = 10 ** number1
        x1 = b * 4 // 5
        x2 = b // -239
        he = x1 + x2
        number *= 2

        for i in range(3, number, 2):
            x1 //= -25
            x2 //= -57121
            x = (x1 + x2) // i
            he += x

        pai = he * 4
        pai //= 10 ** 10
        paistring = str(pai)
        result = paistring[0] + str('.') + paistring[1:len(paistring)]
        print(result)

    get_pi(1000000)


if __name__ == "__main__":
    main()

# 1、 dump：了解函数之间的调用关系
#       py-spy dump --pid 21512
# 2、 top：展示哪些函数被调用执行的次数最多
#       py-spy top --pid 22135
# 3、火焰图可以更加形象直观的展示进程内资源耗费情况
#       py-spy record -o profile.svg --pid 21512
