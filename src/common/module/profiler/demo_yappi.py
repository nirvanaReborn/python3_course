#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/luchengtao11/article/details/124156682
# 一般对分析python性能的工具都会用cprofile。但是这玩意对多线程和asyncio的支持并不友好，
# 如果用它对asyncio分析，会发现CPU都耗费在了poll上面，无法确定到底耗费在哪些协程上。
# https://github.com/sumerc/yappi
# yappi 这个工具可以测多线程、asycio等多种场景。

import asyncio
import sys
# pip install yappi
import yappi

test_time = 5  # 测试时间


async def exit():
    await asyncio.sleep(5)
    yappi.get_func_stats().print_all()
    yappi.stop()
    stats = yappi.convert2pstats(yappi.get_func_stats())
    stats.dump_stats("test.prof")
    asyncio.get_event_loop().stop()
    sys.exit(0)


async def do_something(num):
    for i in range(num):
        i = i + 1
        await asyncio.sleep(0)


async def test1():
    await do_something(10000)


async def test2():
    await do_something(100000)


async def main():
    asyncio.get_event_loop().create_task(exit())
    while True:
        await test1()
        await test2()


yappi.set_clock_type("cpu")
yappi.start()
loop = asyncio.get_event_loop()
with yappi.run():
    loop.run_until_complete(main())
