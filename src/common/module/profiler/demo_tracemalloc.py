#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# tracemalloc是用来分析Python程序内存分配的工具
import tracemalloc


def main():
    tracemalloc.start()

    # ... run your application ...

    # snapshot结果中可以看到每个模块最终创建分配的内存大小，在追查内存问题上很有帮助。
    snapshot = tracemalloc.take_snapshot()
    top_stats = snapshot.statistics('lineno')


if __name__ == '__main__':
    main()
