#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
# https://www.cnblogs.com/herbert/p/3395268.html
Python时间性能测量
主要有以下三种方式：

一，CPU时间
time.clock()
测量CPU时间，比较精准，通过比较程序运行前后的CPU时间差，得出程序运行的CPU时间。
time.clock() 在Unix/Linux下返回的是CPU时间(浮点数表示的秒数)，Win下返回的是以秒为单位的真实时间(Wall-clock time)。

二， 时钟时间
time.time()
测量时钟时间，也就是通常的类似掐表计时。

三，基准时间
timeit.timeit(stmt='pass', setup='pass', timer=<default timer>, number=1000000)
它既提供了命令行接口，又能用于代码文件之中。
具体参数使用可以用命令 python -m timeit -h 查看帮助。使用较多的是下面的选项：
-s S, --setup=S: 用来初始化statement中的变量，只运行一次；
-n N, --number=N: 执行statement的次数，默认会选择一个合适的数字；
-r N, --repeat=N: 重复测试的次数，默认为3；
'''


def countDiv(n):
    "Return the count number of divisors of n."
    count = 1
    for i in range(1, n):
        if n % i == 0:
            count += 1
    return count


def countDiv2(n):
    return len([x for x in range(1, n + 1) if n % x == 0])


def countDiv3(n):
    s = set()
    for i in range(1, n):
        if i in s:
            break
        else:
            if n % i == 0:
                s.update({i, n / i})
    return len(s)


def test_1():
    import time

    start_CPU = time.clock()
    a = countDiv(73920)
    end_CPU = time.clock()
    print("Method 1: %f CPU seconds" % (end_CPU - start_CPU))

    start_CPU = time.clock()
    a = countDiv2(73920)
    end_CPU = time.clock()
    print("Method 2: %f CPU seconds" % (end_CPU - start_CPU))

    start_CPU = time.clock()
    a = countDiv3(73920)
    end_CPU = time.clock()
    print("Method 3: %f CPU seconds" % (end_CPU - start_CPU))
    '''结果：最快的是方法三,方法二和方法一,其实不相上下.
    Method 1: 0.022805 CPU seconds
    Method 2: 0.015988 CPU seconds
    Method 3: 0.000141 CPU seconds
    '''


def test_2():
    import time

    start_Real = time.time()
    a = countDiv(73920)
    end_End = time.time()
    print("Method 1: %f real seconds" % (end_End - start_Real))

    start_Real = time.time()
    a = countDiv2(73920)
    end_End = time.time()
    print("Method 2: %f real seconds" % (end_End - start_Real))

    start_Real = time.time()
    a = countDiv3(73920)
    end_End = time.time()
    print("Method 3: %f real seconds" % (end_End - start_Real))
    '''在精度不够的情况下,都无法得知,方法三真正的运行时间.
    Method 1: 0.016001 real seconds
    Method 2: 0.016001 real seconds
    Method 3: 0.000000 real seconds
    真的想知道,方法三比方法一或者二快多少倍,还是要使用timeit。
    timeit可以重复执行代码一定次数，这样更加稳定的反应程序的执行时间，不会因为一次的执行而产生较大的误差。
    '''


def test_3():
    import timeit  # timeit返回是时钟时间。
    print(timeit.timeit("countDiv(73920)", setup="from __main__ import countDiv", number=100))
    print(timeit.timeit("countDiv2(73920)", setup="from __main__ import countDiv2", number=100))
    print(timeit.timeit("countDiv3(73920)", setup="from __main__ import countDiv3", number=100))
    # 通过timeit可以看出，方法二基本和方法一的性能是相同的。方法二基本是方法三耗时的130倍左右。


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
