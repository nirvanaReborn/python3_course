#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/xybaby/p/7491656.html
# objgraph是一个非常轻巧的工具，但在排查内存泄露的时候非常有用。
# objgraph的代码很短，只有一个文件，其主要依靠标准库中的gc模块来获取对象之间的创建引用关系。
import objgraph


def main():
    # 列出最多实例的类型
    objgraph.show_most_common_types(shortnames=False)
    # 显示两次调用之间的对象数量变化
    objgraph.show_growth(limit=None)
    # 获取指定类型的对象
    objgraph.by_type('Foobar')
    # 查找反向引用
    objgraph.find_backref_chain(obj, objgraph.is_proper_module)


if __name__ == '__main__':
    main()
