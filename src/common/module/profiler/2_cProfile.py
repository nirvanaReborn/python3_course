#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/budong282712018/article/details/80279424
# cProfile自python2.5以来就是标准版Python解释器默认的性能分析器
# cProfile是一种确定性分析器，只测量CPU时间，并不关心内存消耗和其他与内存相关联的信息。
# 其他版本的python，比如PyPy里没有cProfile的。

try:
    import cProfile
except:
    import Profile


def primes(n):
    if n == 2:
        return [2]
    elif n < 2:
        return []

    s = []
    for i in range(3, n + 1):
        if i % 2 != 0:
            s.append(i)

    mroot = n ** 0.5
    half = (n + 1) / 2 - 1
    i = 0
    m = 3

    while m <= mroot:
        if s[i]:
            j = (m * m - 3) / 2
            s[j] = 0
            while j < half:
                s[j] = 0
                j += m

        i = i + 1
        m = 2 * i + 3

    l = [2]
    for x in s:
        if x:
            l.append(x)

    return l


def test_1():
    """
    pyprof2calltree 是一个用于将 Python 的 cProfile 模块生成的性能分析数据可视化的工具。
    它可以将分析数据转换为 kcachegrind 和 qcachegrind 可读取的格式，从而帮助开发者更直观地分析和优化代码性能。
    pip install pyprof2calltree
    使用 pyprof2calltree 将分析结果转换为 kcachegrind 可读取的格式：
    pyprof2calltree -i profile_results -o profile_results.callgrind
    """
    import pyprof2calltree

    # https://blog.csdn.net/weixin_40775077/article/details/109689706
    prof = cProfile.Profile()
    prof.runcall(primes, 6)
    prof.print_stats()
    tmp_file = "callgrind.prof"
    dest_file = "callgrind.prof.log"
    prof.dump_stats(tmp_file)
    # 转化文件: callgrind.prof --> callgrind.prof.log
    pyprof2calltree.convert(tmp_file, dest_file)
    # pyprof2calltree.visualize(prof.getstats())
    # pyprof2calltree.visualize(tmp_file)
    # 在终端使用命令: pyprof2calltree -i callgrind.prof  生成 callgrind.prof.log文件
    # 用qcachegrind打开callgrind.prof.log文件


def test_2():
    '''很多时候我们感兴趣的可能只有耗时最多的几个函数，这个时候先将cProfile 的输出保存到诊断文件中，
    然后用 pstats 定制更加有好的输出
    如果觉得 pstats 使用不方便，还可以使用一些图形化工具，比如:
    gprof2dot(https://github.com/jrfonseca/gprof2dot) 来可视化分析 cProfile 的诊断结果。
    vprof(https://github.com/nvdv/vprof) 也是一个不错的可视化工具，可以用来分析 Python 程序运行时间情况。
    '''
    import pstats

    cProfile.run("primes()", "timeit")
    p = pstats.Stats('timeit')
    p.sort_stats('time')
    p.print_stats(6)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
