#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/-wenli/p/13374186.html
# https://www.cnblogs.com/baby123/p/14234279.html
# Scalene是一个用于Python脚本的CPU和内存分析器，能够正确处理多线程代码，还能区分Python代码和本机代码的运行时间。
# 你不需要修改代码，只需运行Scalene脚本，它就会生成一个文本形式的报告，
# 显示出每一行代码的CPU和内存的使用情况。通过这个文本报告，开发人员可以提高代码的效率。
# calene的速度快、准确率高，还能够对高耗能的代码行进行标注。
# 开源地址https://github.com/emeryberger/scalene
# pip install -U scalene
# https://pypi.org/project/scalene/#files
# Scalene 是一个 Python 的高性能 CPU 和 内存分析器，它可以做到很多其他Python分析器不能做到的事情。它在能提供更多详细信息的同时，比其他的分析器要快几个数量级。
# Scalene 是 很快的。 它使用采样的方式而不是直接测量或者依靠Python的追踪工具。它的开销一般不超过10-20% (通常更少)。
# Scalene 是 精确的。和大部分其他的Python分析器不同，Scalene 在 行级别 下执行CPU分析，在你的程序中指出对应代码行的执行时间。和大多数分析器所返回的功能级分析结果相比，这种程度的细节可能会更有用。
# Scalane 可以区分在Python中运行的时间和在native代码(包括库)中花费的时间。大多数的Python程序员并不会去优化native代码(通常在Python实现中或者所依赖的外部库)，所以区分这两种运行时间，有助于开发者能够将优化的工作专注于他们能够实际改善的代码上。
# Scalene 可以 分析内存使用情况。除了追踪CPU使用情况，Scalene还指出对应代码行的内存增长。这是通过指定内存分配器来实现的。
# NEW! Scalene 会生成 每行 的内存分析，以此更容易的追踪内存泄露。
# NEW! Scalene 会分析 内存拷贝量, 从而易于发现意外的内存拷贝。特别是因为跨越Python和底层库的边界导致的意外 (例如：意外的把 numpy 数组转化成了Python数组，反之亦然)。

# 只支持linux系统
# pip install scalene


import time


def test():
    i = 3
    while i > 0:
        print("test")
        time.sleep(10)
        i = i - 1


def main():
    # dict_choice = {}
    # for i in range(0, 100):
    #     dict_choice[str(i)] = "test_{}()".format(str(i))
    #
    # choice = str(input("Enter a positive integer to choice: "))
    # if choice in dict_choice.keys():
    #     exec(dict_choice[choice])
    # else:
    #     print("输入有误，请重输！", choice)
    test()


if __name__ == "__main__":
    main()

# scalene demo_scalene.py
# python -m scalene demo_scalene.py