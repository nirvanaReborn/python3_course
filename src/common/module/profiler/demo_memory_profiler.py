#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.jb51.net/article/248170.htm
# 输出每一行代码增减的内存
# line_profiler 和 memory_profiler 一个鲜为人知的特性就是在 IPython 上都有快捷命令。你所能做的就是在 IPython 上键入以下命令：
# %load_ext memory_profiler
# %load_ext line_profiler
# 直接在你的 IPython 会话上继续分析吧。
# In [1]: from demo_memory_profiler import test
# In [2]: %mprun -f demo_memory_profiler test(1000)
# In [3]: %lprun -f demo_memory_profiler test(1000)

# pip install memory_profiler psutil
from memory_profiler import profile
# 在这里建议安装 psutil 是因为该包能提升 memory_profiler 的性能。
import psutil
import numpy as np
import os
import gc


@profile
def test():
    a = np.full(shape=(600, 700), fill_value=99.0)
    return a


if __name__ == '__main__':
    a = test()
    print('A：%.2f MB' % (psutil.Process(os.getpid()).memory_info().rss / 1024 / 1024))
    del a
    gc.collect()
    print('B：%.2f MB' % (psutil.Process(os.getpid()).memory_info().rss / 1024 / 1024))

#  python -m memory_profiler demo_memory_profiler.py
