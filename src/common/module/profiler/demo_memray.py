#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://github.com/bloomberg/memray
# https://blog.csdn.net/2301_81967703/article/details/137413452
# Memray可以跟踪python代码、本机扩展模块和python解释器本身中内存分配，可以生成多种不同类型的报告，帮助您分析python代码内存使用情况。
# 工具的主要特点： 跟踪每个函数的调用，能够准确的跟踪调用栈 能跟踪c/c++库的调用 分析速度很快 收集内存数据，输出各种图标 使用python线程 与本地线程一起工作
# 可以帮助解决的问题： 分析应用程序中内存分配，发现高内存使用率的原因 查找内存泄漏的原因 查找导致内存大量分配的代码热点
# memray使用帮助: python3 -m memray --help
# pip3 install memray

def main():
    pass


if __name__ == '__main__':
    main()
