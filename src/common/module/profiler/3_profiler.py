#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 性能分析软件有两类方法论：基于事件的性能分析（event-based profiling）和统计式性能分析（statistical profiling）。
# 在使用这两类软件时， 应该牢记它们各自的优缺点。
#
# 基于事件的性能分析器（event-based profiler， 也称为轨迹性能分析器， tracing profiler） 是通过收集程序执行过程中的具体事件进行工作的。
# 这些性能分析器会产生大量的数据。 基本上， 它们需要监听的事件越多， 产生的数据量就越大。
# 这导致它们不太实用， 在开始对程序进行性能分析时也不是首选。
# 但是， 当其他性能分析方法不够用或者不够精确时， 它们可以作为最后的选择。
# 如果你想分析程序中所有返回语句的性能， 那么这类性能分析器就可以为你提供完成任务应该有的颗粒度，
# 而其他性能分析器都不能为你提供如此细致的结果。
#
# 统计式性能分析器以固定的时间间隔对程序计数器（program counter）进行抽样统计。
# 这样做可以让开发者掌握目标程序在每个函数上消耗的时间。
# 由于它对程序计数器进行抽样， 所以数据结果是对真实值的统计近似。
# 不过， 这类软件足以窥见被分析程序的性能细节， 查出性能瓶颈之所在。
# 优点：1.分析的数据更少； 2.对性能造成的影响更小。

import sys


def profiler(frame, event, arg):
    print('PROFILER: %r %r' % (event, arg))


sys.setprofile(profiler)


def fib_seq(n):
    # 计算斐波那契数列的简单（也是非常低效的） 示例
    def fib(n):
        if n == 0:
            return 0
        elif n == 1:
            return 1
        else:
            return fib(n - 1) + fib(n - 2)

    seq = []
    if n > 0:
        seq.extend(fib_seq(n - 1))
    seq.append(fib(n))
    return seq


def main():
    print(fib_seq(10))


if __name__ == "__main__":
    main()
