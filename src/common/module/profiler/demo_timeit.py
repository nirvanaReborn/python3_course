#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://www.pythondoc.com/pythontutorial3/stdlib.html
# 性能度量
# 有些用户对了解解决同一问题的不同方法之间的性能差异很感兴趣。
# Python 提供了一个度量工具，为这些问题提供了直接答案。
# 例如，使用元组封装和拆封来交换元素看起来要比使用传统的方法要诱人的多。timeit 证明了后者更快一些
# 相对于 timeit 的细粒度，profile 和 pstats 模块提供了针对更大代码块的时间度量工具。

import timeit


def test_help():
    help(timeit)


def test_dir():
    for i in dir(timeit):
        print(i)


def countDiv(n):
    return len([x for x in range(1, n + 1) if n % x == 0])


def test_1():
    print(timeit.timeit("countDiv(73920)", setup="from __main__ import countDiv", number=100))


def test_2(func=test_dir):
    t1 = timeit.Timer(lambda: func())
    print("{:<20}{:10.6} s".format(func.__name__ + ":", t1.timeit(10000)))


def test_3():
    s = timeit.Timer('t=a; a=b; b=t', 'a=1; b=2').timeit()
    print(s)
    t = timeit.Timer('a,b = b,a', 'a=1; b=2').timeit()
    print(t)


def test_4():
    print(timeit.timeit("num = 5; num *= 2", number=1))
    print(timeit.repeat("num = 5; num *= 2", number=1, repeat=3))


def test_5():
    # 算法和数据结构
    def make_list_1():
        result = []
        for value in range(1000):
            result.append(value)
        return result

    def make_list_2():
        result = [value for value in range(1000)]
        return result

    print('make_list_1 takes', timeit.timeit(make_list_1, number=1000), 'seconds')
    print('make_list_2 takes', timeit.timeit(make_list_2, number=1000), 'seconds')


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    print("执行脚本耗时 %s 秒" % timeit.timeit(main, number=1))
    main()
