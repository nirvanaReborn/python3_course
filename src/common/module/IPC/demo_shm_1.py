#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
#
from multiprocessing import shared_memory


def test_1():
    # https://blog.csdn.net/a984120978/article/details/112911613
    shm_a = shared_memory.ShareableList(['张三', 2, 'abc'], name='123')


def test_2():
    # https://wanghao1314.blog.csdn.net/article/details/109753804
    # 进程A创建共享内存，名称为shmTest555666
    shm = shared_memory.SharedMemory(name='shmTest555666', create=True, size=10)

    buf = shm.buf
    print('Process A, buf len: {}, {}'.format(len(buf), shm.size))
    print('shm name: {}'.format(shm.name))

    buf[:4] = bytearray([22, 33, 44, 55])
    buf[4] = 100

    while True:
        if buf[4] == 200:  # 当buf[4]变成200，就结束循环
            break

    shm.close()  # 关闭共享内存
    shm.unlink()  # 释放共享内存，也可以由B进程释放


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
