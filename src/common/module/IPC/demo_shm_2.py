#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
#
from multiprocessing import shared_memory


def test_1():
    # https://blog.csdn.net/a984120978/article/details/112911613
    shm_b = shared_memory.ShareableList(name='123')
    print(shm_b[0])  # ‘张三’
    print(shm_b[1])  # 2
    print(shm_b[2])  # ‘abc


def test_2():
    # https://wanghao1314.blog.csdn.net/article/details/109753804
    # 进程B使用A创建的共享内存，通过名字shmTest555666就可以找到A的共享内存，并进行修改。
    shm = shared_memory.SharedMemory(name='shmTest555666')
    buf = shm.buf
    print('Process B, buf len: {}, {}'.format(len(buf), shm.size))
    print('shm name: {}'.format(shm.name))

    for i in range(10):
        print('{} '.format(buf[i]), end='')

    buf[4] = 200

    print('')


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
