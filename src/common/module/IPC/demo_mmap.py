#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/zywvvd/article/details/122778874
# https://blog.csdn.net/qq_37085158/article/details/126799856
# https://blog.csdn.net/qq_34904125/article/details/125038390
# https://www.cnblogs.com/zhoujinyi/p/6062907.html
# https://zhuanlan.zhihu.com/p/166330573
'''
1、mmap是什么
mmap是一种内存映射文件的方法，即将一个文件或者其它对象映射到进程的地址空间，
实现文件磁盘地址和进程虚拟地址空间中一段虚拟地址的一一对映关系。实现这样的映射关系后，
进程就可以采用指针的方式读写操作这一段内存，而系统会自动回写脏页面到对应的文件磁盘上，
即完成了对文件的操作而不必再调用read,write等系统调用函数。
相反，内核空间对这段区域的修改也直接反映用户空间，从而可以实现不同进程间的文件共享。

2、mmap和常规文件操作的区别
常规文件操作需要从磁盘到页缓存(处于内核空间，不能被用户进程直接寻址)，再到用户主存的两次数据拷贝。
而mmap操控文件，只需要从磁盘到用户主存的一次数据拷贝过程
（创建新的虚拟内存区域，建议文件磁盘地址和虚拟内存区域映射，一次拷贝）。

3、mmap的优点
mmap系统调用使得进程之间通过映射同一个普通文件实现共享内存。
普通文件被映射到进程地址空间后，进程可以像访问普通内存一样对文件进行访问，不必再调用read()，write（）等操作。
mmap并不分配空间, 只是将文件映射到调用进程的地址空间里, 然后你就可以用memcpy等操作写文件, 而不用write()了.
写完后用msync()同步一下, 你所写的内容就保存到文件里了.
不过这种方式没办法增加文件的长度, 因为要映射的长度在调用mmap()的时候就决定了.

mmap的应用场景:
1.将一个普通文件映射到内存中，通常在需要对文件进行频繁读写时使用，这样用内存映射读写取代I/O缓存读写，以获得较高的性能；
2.进程间的内存共享。多个进程将同一个文件map到同一段内核地址上，即实现了相互之间的共同访问。
'''

import mmap  # 核心库
import os
import struct
import numpy as np
import cv2


# 写入数据进共享内存
def test_1():
    # 创建内存映射文件句柄
    fd = os.open('tmp/mmaptest', os.O_CREAT | os.O_TRUNC | os.O_RDWR)

    # 建立内存缓冲区
    if os.name == "nt":  # "Windows"
        buf = mmap.mmap(fd, 67108864, access=mmap.ACCESS_WRITE)
    else:  # Linux
        buf = mmap.mmap(fd, mmap.PAGESIZE, mmap.MAP_SHARED, mmap.PROT_WRITE)

    # 向buf中写入文件(二进制格式)
    f = open('tmp/test2.bmp', 'rb').read()
    buf.write(b'abc')  # 写入字符串
    buf.write(f)  # 写入文件

    # 当前指针位置
    buf.tell()

    # 移动指针到第11个字节
    buf.seek(10)

    # 将内存中的信息写入文件
    buf.flush(0, 100)

    # 关闭
    buf.close()


# 从共享内存中读取数据
def test_2():
    # 创建内存映射文件句柄
    fd = os.open('share_memory/tmp/mmaptest', os.O_RDONLY)

    # 建立内存缓冲区
    if os.name == "nt":  # "Windows"
        buf = mmap.mmap(fd, 67108864, access=mmap.ACCESS_READ)
    else:  # Linux
        buf = mmap.mmap(fd, mmap.PAGESIZE, mmap.MAP_SHARED, mmap.PROT_READ)

    # 读取并打印缓冲区中指定区域内存
    string_length = 136
    string, = struct.unpack('{}s'.format(string_length), buf[:string_length])
    print(string)

    # 将部分区域转换为需要的格式并使用(图像)
    np_str = np.fromstring(buf[:string_length], dtype='uint8')
    img = cv2.imdecode(np_str, flags=-1)

    # 纯图像像素数据(仅需转换字符串为uint8)即可
    data = np.fromstring(buf[:string_length], dtype='uint8')
    img = data.reshape([10, 10])


def test_3():
    mmap_file = None

    # 从内存中读取信息，
    def read_mmap_info():
        global mmap_file
        mmap_file.seek(0)
        ##把二进制转换为字符串
        info_str = mmap_file.read().translate(None, b'\x00').decode()
        print(info_str)

    # 如果内存中没有对应信息，则向内存中写信息以供下次调用使用
    def get_mmap_info():
        global mmap_file
        ##第二个参数1024是设定的内存大小，单位：字节。如果内容较多，可以调大一点
        mmap_file = mmap.mmap(-1, 1024, access=mmap.ACCESS_WRITE, tagname='share_mmap')
        ##读取有效比特数，不包括空比特
        cnt = mmap_file.read_byte()
        if cnt == 0:
            print("Load data to memory")
            mmap_file = mmap.mmap(0, 1024, access=mmap.ACCESS_WRITE, tagname='share_mmap')
            mmap_file.write(b"This is the test data")
        else:
            print("The data is in memory")
            read_mmap_info()

    # 修改内存块中的数据
    def reset_mmp_info():
        global mmap_file
        mmap_file.seek(0)
        mmap_file.write(b'\x00')
        mmap_file.write(b"Load data to memory agine")

    get_mmap_info()


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
