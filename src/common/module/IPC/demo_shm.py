#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/cheer-lingmu/p/16443963.html
'''
多进程共享内存有两种方式，即 shm(shared memory)和 mmap 方式。
前者直接共享物理内存，后者通过一个中间文件间接共享内存。

三、内存映射和共享内存的区别
1、mmap保存到实际硬盘，实际存储并没有反映到主存上。
    优点：储存量可以很大（多于主存）；
    缺点：进程间读取和写入速度要比主存的要慢。—— 每个进程地址空间中开辟出一块空间进行映射
2、shm保存到物理存储器（主存），实际的储存量直接反映到主存上。
    优点，进程间访问速度（读写）比磁盘要快；
    缺点，储存量不能非常大（多于主存）。—— 每个进程最终会映射到同一块物理内存
3、mmap系统调用并不是完全为了用于共享内存而设计的。
    它本身提供了不同于一般对普通文件的访问方式，进程可以像读写内存一样对普通文件的操作。
    而Posix或系统V的共享内存IPC则纯粹用于共享目的，当然mmap()实现共享内存也是其主要应用之一。
4、共享内存是内存映射文件的特例，它映射的是一块内存而不是磁盘上的文件。

四、注意
1、物理内存和虚拟内存
　　物理内存是指由于安装内存条而获得的临时储存空间。
    主要作用是在计算机运行时为操作系统和各种程序提供临时储存。
　　虚拟内存是计算机系统内存管理的一种技术。
    它使得应用程序认为它拥有连续可用的内存（一个连续完整的地址空间），它通常是被分隔成多个物理内存碎片，
    还有部分暂时存储在外部磁盘存储器上，在需要时进行数据交换。
2、虚拟内存的工作模式
　　当每个进程创建的时候，内核会为进程分配4G的虚拟内存，当进程还没有开始运行时，这只是一个内存布局。
    实际上并不立即就把虚拟内存对应位置的程序数据和代码（比如.text .data段）拷贝到物理内存中，
    只是建立好虚拟内存和磁盘文件之间的映射就好（叫做存储器映射）。这个时候数据和代码还是在磁盘上的。
    当运行到对应的程序时，进程去寻找页表，发现页表中地址没有存放在物理内存上，而是在磁盘上，
    于是发生缺页异常，于是将磁盘上的数据拷贝到物理内存中。
    另外在进程运行过程中，要通过malloc来动态分配内存时，也只是分配了虚拟内存，
    即为这块虚拟内存对应的页表项做相应设置，当进程真正访问到此数据时，才引发缺页异常。

共享内存区是最快的IPC形式。
一旦这样的内存映射到共享它的进程的地址空间，这些进程间数据传递不再涉及到内核，
换句话说是进程不再通过执行进入内核的系统调用来传递彼此的数据。

注1：共享内存并未提供同步机制，
也就是说，在第一个进程结束对共享内存的写操作之前，并无自动机制可以阻止第二个进程开始对它进行读取，
所以我们通常需要用其他的机制来同步对共享内存的访问，例如信号量。

注2：共享存储的数据格式，
当成功导入一块共享内存后，它只是相当于一个字符串指针来指向一块内存。
这也就给系统开发人员提供了发挥的空间，在内存使用范围内可以存储任意格式的数据，
对于复杂的数据类型格式的解析、转化也带来了开发难度。

python3.8新功能：multiprocessing 模块增加一个 SharedMemory 类，用于创建进程之间的共享内存。
python基于基于POSIX规范mmap方式实现共享内存。
http://study.yali.edu.cn/pythonhelp/library/multiprocessing.shared_memory.html

https://blog.csdn.net/aiqinhaidekuqi/article/details/120830515
在我们实际的使用场景中，多进程基本就两种模式：进程派生模式和无相关(进程间没有依赖关系)模式。
对于python的派生模式还细分为fork和spawn两种方式。
在fork模式和无相关模式下，因为python本身提供了内存垃圾回收的机制，在代码中一旦创建或者关联共享内存，
会默认启动一个resource_tracker的跟踪子进程。如图所示，在fork模式下，每个进程都附加一个跟踪子进程。
当主进程退出后，垃圾回收会自动销毁共享内存空间。这样就会导致其他进程无法再次访问该共享内存的问题。
这对于我们开发人员来说是不能接受的，共享内存的生命周期应该手动来管理。
在linux环境下，无论是创建还是关联共享内存，都会在resource_tracker进程中进行注册。
那么我们可以在自己的代码中调用unregister操作，取消自动跟踪。
from multiprocessing.resource_tracker import unregister
unregister('/' + shm.name, "shared_memory")

该模块提供了一个 SharedMemory 类，用于分配和管理多核或对称多处理器（SMP）机器上进程间的共享内存。
为了协助管理不同进程间的共享内存生命周期，同时也提供了一个 BaseManager 的子类： SharedMemoryManager。
'''
# https://blog.csdn.net/weixin_44815943/article/details/126544916
import multiprocessing
from multiprocessing import shared_memory


def f(name):
    shm_child = shared_memory.SharedMemory(name=name, create=False)
    shm_child.buf[:10] = bytearray(list(range(10, 0, -1)))
    shm_child.close()


if __name__ == "__main__":
    shm_main = shared_memory.SharedMemory(create=True, size=10)  # size 是指字节数
    shm_main.buf[:] = bytearray(list(range(10)))  # 只能通过字节的方式进行操作

    for _ in shm_main.buf:
        print(_, end=", ")  # 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    print('')

    p = multiprocessing.Process(target=f, args=(shm_main.name,))
    p.start()
    p.join()

    for _ in shm_main.buf:
        print(_, end=", ")  # 10, 9, 8, 7, 6, 5, 4, 3, 2, 1,

    shm_main.close()
    shm_main.unlink()

