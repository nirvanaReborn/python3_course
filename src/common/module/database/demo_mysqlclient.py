#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/zyever/p/9449344.html

# 如果是追求极致性能，建议使用mysqlclient，如果想使用ORM，建议使用mysql connector for python
# https://blog.csdn.net/razorluo/article/details/88209765

# pip install mysqlclient
import MySQLdb


def test_1():
    # connect() 方法用于创建数据库的连接，里面可以指定参数：用户名，密码，主机等信息。
    # 这只是连接到了数据库，要想操作数据库需要创建游标。
    conn = MySQLdb.connect(
        host='localhost',
        port=3306,
        user='root',
        passwd='123456',
        db='test',
    )

    # 通过获取到的数据库连接conn下的cursor()方法来创建游标。
    cur = conn.cursor()

    # 创建数据表,通过游标cur 操作execute()方法可以写入纯sql语句。通过execute()方法中写如sql语句来对数据进行操作
    cur.execute("create table student(id int ,name varchar(20),class varchar(30),age varchar(10))")

    # 插入一条数据
    cur.execute("insert into student values('2','Tom','3 year 2 class','9')")

    # 修改查询条件的数据
    cur.execute("update student set class='3 year 1 class' where name = 'Tom'")

    # 删除查询条件的数据
    cur.execute("delete from student where age='9'")

    # cur.close() 关闭游标
    cur.close()

    # conn.commit()方法在提交事物，在向数据库插入一条数据时必须要有这个方法，否则数据不会被真正的插入。
    conn.commit()

    # conn.close()关闭数据库连接
    conn.close()


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
