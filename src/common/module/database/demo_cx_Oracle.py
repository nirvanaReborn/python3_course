#!/usr/bin/env python
# -*- coding: utf-8 -*-

# 需要在控制台下边来执行这个python脚本，而不是某些IDE（它们似乎无法载入os的环境变量）。
# http://blog.csdn.net/u012882134/article/details/52386287
# http://www.cnblogs.com/Leo_wl/p/3288402.html
# http://blog.csdn.net/kongxx/article/details/7107661
# http://www.cnblogs.com/daisy89/p/5307727.html
'''
fetchone()  从数据库中取单个元祖，如果没有有效数据返回none。
fetchmany([rows_no]) 从数据库中取下一个rows_no数据
fetchall()  获取所有结果集，返回元祖列表，如果没有有效行，返回空列表。
'''
import logging
import os

# 首先安装配置时，必须把握一个点，就是版本一致！包括：系统版本，python版本，oracle客户端版本，cx_Oracle版本！
import cx_Oracle  # 引用模块cx_Oracle

# os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.ZHS16GBK' # 保证编码一致
os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.UTF8'  # 保证编码一致
# os.environ['path'] =  r'D:\ProgramFiles\oracle\instantclient_12_1'  # 使用即时客户端避免版本不兼容
from public_function import GetConfigInfo, GLOBAL_WORK_DIR


def test_1(dict_login_oracle):
    user = dict_login_oracle["user"]
    password = dict_login_oracle["password"]
    IP = dict_login_oracle["ip"]
    port = dict_login_oracle["port"]
    service_name = dict_login_oracle["service_name"]
    dsn = str(dict_login_oracle["ip"] + ":"
              + str(int(dict_login_oracle["port"])) + "/"
              + dict_login_oracle["service_name"])
    conn = cx_Oracle.connect(user, password, dsn)  # 连接数据库
    tnsnames = '''(DESCRIPTION = 
                        (ADDRESS_LIST = 
                            (ADDRESS = (PROTOCOL = TCP)(HOST = {0})(PORT = {1}))
                        )
                        (CONNECT_DATA = 
                            (SERVICE_NAME = {2})
                        )
                   )
    '''.format(IP, port, service_name)
    # conn = cx_Oracle.connect(user, password, tnsnames)  # 连接数据库
    print("oracle版本：", conn.version)
    list_version = conn.version.split('.')
    print(list_version)
    print(conn.dsn)  # 数据源名称
    cursor = conn.cursor()  # 获取cursor
    x = cursor.execute("select * from dual")  # 使用cursor进行各种操作
    # print(x)
    row = cursor.fetchone()
    print(row[0])
    cursor.close()  # 关闭cursor
    conn.close()  # 关闭连接


# 获取单条记录
def test_2(str_login_info):
    try:
        conn = cx_Oracle.connect(str_login_info)  # 连接数据库
        print("oracle版本：", conn.version)
        cursor = conn.cursor()  # 获取cursor
        x = cursor.execute("SELECT * FROM all_objects where owner like 'HS_%'")  # 使用cursor进行各种操作
        # print(x)

        # 显示每列的详细信息
        des = cursor.description
        print("表的描述:", des)

        # 获取表头
        list_head = [item[0] for item in des]
        print("表头:", ",".join(list_head))

        row = cursor.fetchone()
        if row:
            print(row[0])

        cursor.close()  # 关闭cursor
        conn.close()  # 关闭连接
    except Exception as e:
        logging.error("查询失败：" + "all_objects" + ", 报错信息:" + e.__str__())


# 获取结果集
def test_3(str_login_info):
    try:
        conn = cx_Oracle.connect(str_login_info)
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM all_objects where owner like 'HS_%'")
        rows = cursor.fetchall()
        for row in rows:
            print("%s, %s, %s, %s" % (row[0], row[1], row[2], row[3]))
        print("Number of rows returned: %d" % cursor.rowcount)
        cursor.close()
        conn.close()
    except Exception as e:
        logging.error("查询失败：" + "all_objects" + ", 报错信息:" + e.__str__())


# 批量执行
def test_4(str_login_info):
    list_dict_value = []
    try:
        conn = cx_Oracle.connect(str_login_info)
        cursor = conn.cursor()
        cursor.executemany("SELECT * FROM all_objects where owner like 'HS_%'", list_dict_value)
        rows = cursor.fetchall()
        for row in rows:
            print("%s, %s, %s, %s" % (row[0], row[1], row[2], row[3]))
        print("Number of rows returned: %d" % cursor.rowcount)
        cursor.close()
        conn.close()
    except Exception as e:
        logging.error("查询失败：" + "all_objects" + ", 报错信息:" + e.__str__())


# 绑定变量
def test_5(str_login_info):
    dict_params = {'open_date': 20200605}
    try:
        conn = cx_Oracle.connect(str_login_info)
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM hs_user.sysarg WHERE init_date=:open_date ', dict_params)
        # cursor.execute('SELECT * FROM employees WHERE department_id=:dept_id AND salary>:sal', dept_id=50, sal=1000)
        print(cursor.bindnames())
        rows = cursor.fetchall()
        for row in rows:
            print("%s, %s, %s, %s" % (row[0], row[1], row[2], row[3]))
        print("Number of rows returned: %d" % cursor.rowcount)
        cursor.close()
        conn.close()
    except Exception as e:
        logging.error("查询失败：" + "all_objects" + ", 报错信息:" + e.__str__())


def main():
    def get_all_connect(dict_login_info) -> dict:
        dict_connect = {}
        for key in dict_login_info:
            dict_temp = dict_login_info[key]
            # print(dict_temp)
            # sqlplus user/passwd@ip:port/instance
            dict_connect[key] = str(dict_temp["user"] + "/"
                                    + dict_temp["password"] + "@"
                                    + dict_temp["ip"] + ":"
                                    + str(int(dict_temp["port"])) + "/"
                                    + dict_temp["service_name"])

        return dict_connect

    source_file = os.path.join(GLOBAL_WORK_DIR, r"获取登录信息.xml")
    dict_login_info = GetConfigInfo(source_file).get_dict_config()
    dict_login_oracle = dict_login_info.get("oracle")
    str_login_info = get_all_connect(dict_login_info).get("oracle")
    print(str_login_info)

    dict_choice = {}
    for i in range(0, 100):
        if i == 1:
            str_login_info = dict_login_oracle
        dict_choice[str(i)] = "test_" + str(i) + "(str_login_info)"

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
