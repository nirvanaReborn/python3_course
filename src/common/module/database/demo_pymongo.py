#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.runoob.com/python3/python-mongodb.html
# MongoDB 是目前最流行的 NoSQL 数据库之一，使用的数据类型 BSON（类似 JSON）。
# https://germey.gitbooks.io/python3webspider/content/1.4.2-MongoDB%E7%9A%84%E5%AE%89%E8%A3%85.html
# MongoDB 是由 C++ 语言编写的非关系型数据库，是一个基于分布式文件存储的开源数据库系统，其内容存储形式类似 Json 对象，它的字段值可以包含其他文档，数组及文档数组，非常灵活。
# 官方网站：https://www.mongodb.com
# 官方文档：https://docs.mongodb.com
# GitHub：https://github.com/mongodb
# 中文教程：http://www.runoob.com/mongodb/mongodb-tutorial.html

# ----------------------------------------------------------------------------------------
# https://germey.gitbooks.io/python3webspider/content/1.5.2-PyMongo%E7%9A%84%E5%AE%89%E8%A3%85.html
# 在 Python 中如果想要和 MongoDB 进行交互就需要借助于 PyMongo 库
# GitHub：https://github.com/mongodb/mongo-python-driver
# 官方文档：https://api.mongodb.com/python/current/
# PyPi：https://pypi.python.org/pypi/pymongo

# pip3 install pymongo
import pymongo
from bson.objectid import ObjectId


def test_0():
    print(pymongo.version)


def test_1():
    # 连接MongoDB
    client = pymongo.MongoClient(host='localhost', port=27017)
    # client = pymongo.MongoClient('mongodb://localhost:27017/')

    # 指定数据库: 在这里我以 test 数据库为例进行说明
    db = client.test
    # db = client['test']

    # 指定集合
    collection = db.students
    # collection = db['students']

    # 插入数据

    student1 = {
        'id': '20170101',
        'name': 'Jordan',
        'age': 20,
        'gender': 'male'
    }

    student2 = {
        'id': '20170202',
        'name': 'Mike',
        'age': 21,
        'gender': 'male'
    }

    # 在 MongoDB 中，每条数据其实都有一个 _id 属性来唯一标识，
    # 如果没有显式指明 _id，MongoDB 会自动产生一个 ObjectId 类型的 _id 属性。
    # insert() 方法会在执行后返回的 _id 值。
    result = collection.insert(student1)
    print(result)

    # 实际上在 PyMongo 3.X 版本中，insert() 方法官方已经不推荐使用了，当然继续使用也没有什么问题，
    # 官方推荐使用 insert_one() 和 insert_many() 方法将插入单条和多条记录分开。
    result = collection.insert_one(student1)
    print(result)
    print(result.inserted_id)

    # result = collection.insert([student1, student2])
    result = collection.insert_many([student1, student2])
    print(result)
    print(result.inserted_ids)

    # 单条查询
    result = collection.find_one({'name': 'Mike'})
    print(type(result))
    print(result)

    # 也可以直接根据 ObjectId 来查询
    result = collection.find_one({'_id': ObjectId('593278c115c2602667ec6bae')})
    print(result)

    # 多条查询
    results = collection.find({'age': 20})
    print(results)
    for result in results:
        print(result)

    # 符号	含义	        示例
    # $lt	小于	        {'age': {'$lt': 20}}
    # $gt	大于	        {'age': {'$gt': 20}}
    # $lte	小于等于	    {'age': {'$lte': 20}}
    # $gte	大于等于	    {'age': {'$gte': 20}}
    # $ne	不等于	    {'age': {'$ne': 20}}
    # $in	在范围内	    {'age': {'$in': [20, 23]}}
    # $nin	不在范围内	{'age': {'$nin': [20, 23]}}
    # 查询年龄大于 20 的数据
    results = collection.find({'age': {'$gt': 20}})
    for result in results:
        print(result)

    # 符号	    含义	        示例	                                                示例含义
    # $regex	匹配正则	    {'name': {'$regex': '^M.*'}}	                    name 以 M开头
    # $exists	属性是否存在	{'name': {'$exists': True}}	                        name 属性存在
    # $type	    类型判断	    {'age': {'$type': 'int'}}	                        age 的类型为 int
    # $mod	    数字模操作	{'age': {'$mod': [5, 0]}}	                        年龄模 5 余 0
    # $text	    文本查询	    {'$text': {'$search': 'Mike'}}	                    text 类型的属性中包含 Mike 字符串
    # $where	高级条件查询	{'$where': 'obj.fans_count == obj.follows_count'}	自身粉丝数等于关注数

    # 还可以进行正则匹配查询
    results = collection.find({'name': {'$regex': '^M.*'}})

    # 统计查询结果
    count = collection.find().count()
    print(count)

    # 或者统计符合某个条件的数据：
    count = collection.find({'age': 20}).count()
    print(count)

    # 排序
    # 调用 pymongo.ASCENDING 指定升序，如果要降序排列可以传入 pymongo.DESCENDING。
    results = collection.find().sort('name', pymongo.ASCENDING)
    print([result['name'] for result in results])

    # 偏移: skip() 方法
    # 比如偏移 2，就忽略前 2 个元素，得到第三个及以后的元素。
    results = collection.find().sort('name', pymongo.ASCENDING).skip(2)
    print([result['name'] for result in results])

    # 可以用 limit() 方法指定要取的结果个数
    results = collection.find().sort('name', pymongo.ASCENDING).skip(2).limit(2)
    print([result['name'] for result in results])

    # 值得注意的是，在数据库数量非常庞大的时候，如千万、亿级别，最好不要使用大的偏移量来查询数据，很可能会导致内存溢出，
    # 可以使用类似如下操作来进行查询,这时记录好上次查询的 _id。：
    collection.find({'_id': {'$gt': ObjectId('593278c815c2602678bb2b8d')}})

    # 更新
    condition = {'name': 'Kevin'}
    student = collection.find_one(condition)
    student['age'] = 25
    result = collection.update(condition, student)
    print(result)

    # 也可以使用 $set操作符对数据进行更新
    # 这样可以只更新 student 字典内存在的字段，如果其原先还有其他字段则不会更新，也不会删除。
    # 而如果不用 $set 的话则会把之前的数据全部用 student 字典替换，如果原本存在其他的字段则会被删除。
    result = collection.update(condition, {'$set': student})

    # 另外 update() 方法其实也是官方不推荐使用的方法，在这里也分了 update_one() 方法和 update_many() 方法，
    # 用法更加严格，第二个参数需要使用 $ 类型操作符作为字典的键名
    condition = {'name': 'Kevin'}
    student = collection.find_one(condition)
    student['age'] = 26
    result = collection.update_one(condition, {'$set': student})
    print(result)
    print(result.matched_count, result.modified_count)

    # 在这里我们指定查询条件为年龄大于 20，然后更新条件为 {'$inc': {'age': 1}}，也就是年龄加 1
    condition = {'age': {'$gt': 20}}
    result = collection.update_one(condition, {'$inc': {'age': 1}})
    print(result)
    print(result.matched_count, result.modified_count)

    # 如果调用 update_many() 方法，则会将所有符合条件的数据都更新
    condition = {'age': {'$gt': 20}}
    result = collection.update_many(condition, {'$inc': {'age': 1}})
    print(result)
    print(result.matched_count, result.modified_count)

    # 删除
    result = collection.remove({'name': 'Kevin'})
    print(result)

    # 另外依然存在两个新的推荐方法，delete_one() 和 delete_many() 方法
    # delete_one() 即删除第一条符合条件的数据，delete_many() 即删除所有符合条件的数据
    result = collection.delete_one({'name': 'Kevin'})
    print(result)
    print(result.deleted_count)
    result = collection.delete_many({'age': {'$lt': 25}})
    print(result.deleted_count)


# 另外 PyMongo 还提供了一些组合方法，如find_one_and_delete()、find_one_and_replace()、find_one_and_update()，就是查找后删除、替换、更新操作，用法与上述方法基本一致。
# 另外还可以对索引进行操作，如 create_index()、create_indexes()、drop_index() 等。
# 详细用法可以参见官方文档：http://api.mongodb.com/python/current/api/pymongo/collection.html。
# 另外还有对数据库、集合本身以及其他的一些操作，在这不再一一讲解，可以参见官方文档：http://api.mongodb.com/python/current/api/pymongo/。


def test_2():
    '''
    注意: 在 MongoDB 中，数据库只有在内容插入后才会创建!
          就是说，数据库创建后要创建集合(数据表)并插入一个文档(记录)，数据库才会真正创建。
    '''
    myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    # myclient = pymongo.MongoClient(host='127.0.0.1', port=27017)
    mydb = myclient["runoobdb"]
    # 创建集合: MongoDB 中的集合类似 SQL 的表。
    mytable = mydb["sites"]


def test_3():
    '''判断数据库是否已存在
    # 注意：database_names 在最新版本的 Python 中已废弃，Python3.7+ 之后的版本改为了 list_database_names()。
    '''
    myclient = pymongo.MongoClient('mongodb://localhost:27017/')
    # dblist = myclient.database_names()
    dblist = myclient.list_database_names()
    if "runoobdb" in dblist:
        print("数据库已存在！")


def test_4():
    '''判断集合是否已存在
    # 注意：collection_names 在最新版本的 Python 中已废弃，Python3.7+ 之后的版本改为了 list_collection_names()。
    '''
    myclient = pymongo.MongoClient('mongodb://localhost:27017/')
    mydb = myclient['runoobdb']
    # collist = mydb.collection_names()
    collist = mydb.list_collection_names()
    if "sites" in collist:  # 判断 sites 集合是否存在
        print("集合已存在！")


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
