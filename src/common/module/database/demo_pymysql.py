#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# https://germey.gitbooks.io/python3webspider/content/1.4.1-MySQL%E7%9A%84%E5%AE%89%E8%A3%85.html
# MySQL 是一个轻量级的关系型数据库，以表的形式来存储数据，本节我们来了解下它的安装方式。
# 官方网站：https://www.mysql.com/cn
# 下载地址：https://www.mysql.com/cn/downloads
# 中文教程：http://www.runoob.com/mysql/mysql-tutorial.html
# 下载完成之后直接双击安装包即可安装，直接选择默认选项点击下一步安装即可，记住在此页设置的密码.
# 安装完成之后我们可以在[电脑-管理-服务]页面开启和关闭 MySQL 服务

# ----------------------------------------------------------------------------------------
# https://germey.gitbooks.io/python3webspider/content/1.5.1-PyMySQL%E7%9A%84%E5%AE%89%E8%A3%85.html
# 虽然大家可能在python2.x中用习惯了mysqldb，但是在python3.x中已经不支持那个组件了,取而代之的是pymysql。
# 在 Python3 中如果想要将数据存储到 MySQL 中就需要借助于 PyMySQL 来操作
# GitHub：https://github.com/PyMySQL/PyMySQL
# 官方文档：http://pymysql.readthedocs.io/
# PyPi：https://pypi.python.org/pypi/PyMySQL

# ----------------------------------------------------------------------------------------
# 访问mysql数据库
# http://blog.csdn.net/hk2291976/article/details/41545443

# pip3 install PyMySQL
import pymysql
import pandas as pd

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
# pd.set_option('display.height', 1000)
pd.set_option('display.width', 1000)


# 0.连接数据库
def test_0():
    db = pymysql.connect(host='127.0.0.1', port=3306, user='root', passwd='123456', db='mysql')
    cursor = db.cursor()
    cursor.execute("select * from dual")
    for r in cursor.fetchall():
        print(r)

    cursor.close()
    db.close()


# https://germey.gitbooks.io/python3webspider/5.2.1-MySQL%E5%AD%98%E5%82%A8.html
# 1.创建库
# 引用：https://cuiqingcai.com/5578.html
# 这里通过PyMySQL的connect()方法声明一个MySQL连接对象db，
# 此时需要传入MySQL运行的host（即IP）。由于MySQL在本地运行，所以传入的是localhost。
# 如果MySQL在远程运行，则传入其公网IP地址。后续的参数user即用户名，password即密码，port即端口（默认为3306）。
# 连接成功后，需要再调用cursor()方法获得MySQL的操作游标，利用游标来执行SQL语句。
# 这里我们执行了两句SQL，直接用execute()方法执行即可。
# 第一句SQL用于获得MySQL的当前版本，然后调用fetchone()方法获得第一条数据，也就得到了版本号。
# 第二句SQL执行创建数据库的操作，数据库名叫作spiders，默认编码为UTF-8。
# 由于该语句不是查询语句，所以直接执行后就成功创建了数据库spiders。接着，再利用这个数据库进行后续的操作。
def test_1():
    db = pymysql.connect(host="localhost", user='root', password='hundsun', port=3306)
    cursor = db.cursor()
    cursor.execute('SELECT VERSION()')
    data = cursor.fetchone()
    print('Database version is:', data)
    cursor.execute("CREATE DATABASE spiders DEFAULT CHARACTER SET UTF8")
    db.close()


# 2.创建表
# 创建数据库后，在连接时需要额外指定一个参数db，指定连接的数据库
def test_2():
    # MySQL 连接会直接指定当前数据库 spiders，所有操作都是在 spiders 数据库内执行的。
    db = pymysql.connect(host="localhost", user='root', password='hundsun', port=3306, db='spiders')
    cursor = db.cursor()
    sql = 'CREATE TABLE IF NOT EXISTS students (id VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, age INT NOT NULL, PRIMARY KEY (id))'
    cursor.execute(sql)
    db.close()


# 3.插入数据
# 下一步就是向数据库中插入数据了。
# 例如，这里爬取了一个学生信息，学号为20120001，名字为Bob，年龄为20。
# 那么如何将该条数据插入数据库呢？示例代码如下：
#
# 这里首先构造了一个SQL语句，其Value值没有用字符串拼接的方式来构造，如：
# sql = 'INSERT INTO students(id, name, age) values(' + id + ', ' + name + ', ' + age + ')'
# 这样的写法烦琐而且不直观，所以我们选择直接用格式化符%s来实现。有几个Value写几个%s，
# 我们只需要在execute()方法的第一个参数传入该SQL语句，Value值用统一的元组传过来就好了。
# 这样的写法既可以避免字符串拼接的麻烦，又可以避免引号冲突的问题。
#
# 之后值得注意的是，需要执行db对象的commit()方法才可实现数据插入，这个方法才是真正将语句提交到数据库执行的方法。
# 对于数据插入、更新、删除操作，都需要调用该方法才能生效。
# 接下来，我们加了一层异常处理。如果执行失败，则调用rollback()执行数据回滚，相当于什么都没有发生过。
#
# 这里涉及事务的问题。事务机制可以确保数据的一致性，也就是这件事要么发生了，要么没有发生。
# 比如插入一条数据，不会存在插入一半的情况，要么全部插入，要么都不插入，这就是事务的原子性。
# 另外，事务还有3个属性——一致性、隔离性和持久性。这4个属性通常称为ACID特性，具体如表5-2所示。
def test_3():
    db = pymysql.connect(host='localhost', user='root', password='hundsun', port=3306, db='spiders')
    cursor = db.cursor()
    # sql = 'INSERT INTO students(id, name, age) values(%s, %s, %s)' # 支持占位符，也可以使用字典形式
    sql = 'INSERT INTO students(id, name, age) values(%(id)s, %(name)s, %(age)s)'
    try:
        # execute()方法的第一个参数传入sql变量，第二个参数传入data的键值构造的元组，就可以成功插入数据了。
        # cursor.execute(sql, (id, user, age))
        cursor.execute(sql, {'id': '20120001', 'name': 'LiSi', 'age': 17})

        # 批量插入
        list_dict_item = [
            {'id': '20120001', 'name': 'LiSi', 'age': 17},
            {'id': '20120002', 'name': 'LiSan', 'age': 18}
        ]
        cursor.executemany(sql, tuple(list_dict_item))

        db.commit()
    except pymysql.err.Error as e:
        print('Failed:', e)
        db.rollback()
    db.close()


# 4.插入数据(优化)
# 假如突然增加了一个字段，那么我们构造的 SQL 语句就需要改动
# 在很多情况下，我们要达到的效果是插入方法无需改动，做成一个通用方法，只需要传入一个动态变化的字典给就好了。
def test_4():
    dict_sql = {
        "op_type": "I",
        "db": 'spiders',
        "table": "students",
        "condition": 'age > 20',
        "data": {
            'id': '20120001',
            'name': 'Bob',
            'age': 20
        },
    }

    db = pymysql.connect(host='localhost', user='root', password='123456', port=3306, db=dict_sql["db"])
    cursor = db.cursor()

    keys = ', '.join(dict_sql["data"].keys())
    values = ', '.join(['%s'] * len(dict_sql["data"]))
    sql = 'INSERT INTO {table}({keys}) VALUES ({values})'.format(table=dict_sql["table"], keys=keys, values=values)
    try:
        if cursor.execute(sql, tuple(dict_sql["data"].values())):
            print('Successful')
            db.commit()
    except pymysql.Error as e:
        print('Failed:', e)
        db.rollback()
    db.close()


# 5.更新数据
# 这里同样用占位符的方式构造SQL，然后执行execute()方法，传入元组形式的参数，同样执行commit()方法执行操作。
# 如果要做简单的数据更新的话，完全可以使用此方法。
def test_5():
    dict_sql = {
        "op_type": "U",
        "db": 'spiders',
        "table": "students",
        "condition": 'age > 20',
        "data": {
            'id': '20120001',
            'name': 'Bob',
            'age': 21
        },
    }

    db = pymysql.connect(host='localhost', user='root', password='123456', port=3306, db=dict_sql["db"])
    cursor = db.cursor()

    keys = ', '.join(dict_sql["data"].keys())
    values = ', '.join(['%s'] * len(dict_sql["data"]))
    sql = 'INSERT INTO {table}({keys}) VALUES ({values}) ON DUPLICATE KEY UPDATE'.format(table=dict_sql["table"], keys=keys, values=values)
    update = ','.join([" {key} = %s".format(key=key) for key in dict_sql["data"]])
    sql += update
    try:
        if cursor.execute(sql, tuple(dict_sql["data"].values()) * 2):
            print('Successful')
            db.commit()
    except:
        print('Failed')
        db.rollback()
    db.close()


# 6.删除数据
# 删除操作相对简单，直接使用DELETE语句即可，只是需要指定要删除的目标表名和删除条件，而且仍然需要使用db的commit()方法才能生效。
# 示例如下：
# 因为删除条件有多种多样，运算符有大于、小于、等于、LIKE等，条件连接符有AND、OR等，所以不再继续构造复杂的判断条件。
# 这里直接将条件当作字符串来传递，以实现删除操作。
def test_6():
    dict_sql = {
        "op_type": "D",
        "db": 'spiders',
        "table": "students",
        "condition": 'age > 20',
        "data": {},
    }

    db = pymysql.connect(host='localhost', user='root', password='123456', port=3306, db=dict_sql["db"])
    cursor = db.cursor()
    sql = 'DELETE FROM  {table} WHERE {condition}'.format(table=dict_sql["table"], condition=dict_sql["condition"])
    try:
        cursor.execute(sql)
        db.commit()
    except:
        db.rollback()
    db.close()


# 7.查询数据
# 说完插入、修改和删除等操作，还剩下非常重要的一个操作，那就是查询。查询会用到SELECT语句，示例如下：
# 调用了fetchone()方法，这个方法可以获取结果的第一条数据，返回结果是元组形式，元组的元素顺序跟字段一一对应
# 随后，我们又调用了fetchall()方法，它可以得到结果的所有数据。
# 然后将其结果和类型打印出来，它是二重元组，每个元素都是一条记录，我们将其遍历输出出来。
#
# 但是这里需要注意一个问题，这里显示的是3条数据而不是4条，fetchall()方法不是获取所有数据吗？
# 这是因为它的内部实现有一个偏移指针用来指向查询结果，最开始偏移指针指向第一条数据，取一次之后，指针偏移到下一条数据，
# 这样再取的话，就会取到下一条数据了。我们最初调用了一次fetchone()方法，这样结果的偏移指针就指向下一条数据，
# fetchall()方法返回的是偏移指针指向的数据一直到结束的所有数据，所以该方法获取的结果就只剩3个了。
def test_7():
    dict_sql = {
        "op_type": "S",
        "db": 'spiders',
        "table": "students",
        "condition": 'age >= %s',
        "data": {},
    }

    db = pymysql.connect(host='localhost', user='root', password='123456', port=3306, db=dict_sql["db"])
    cursor = db.cursor()
    sql = 'SELECT * FROM  {table} WHERE {condition}'.format(table=dict_sql["table"], condition=dict_sql["condition"])
    try:
        cursor.execute(sql, (10,))
        print('Count:', cursor.rowcount)
        # 返回单条
        # row = cursor.fetchone()
        # while row:
        #     print('Row:', row)
        #     row = cursor.fetchone()

        # 结果集返回
        results = cursor.fetchall()
        print('Results:', results)
        print('Results Type:', type(results))
        for row in results:
            print(row)
    except Exception as e:
        print('[Error] ', e)
    db.close()


def test_8():
    # 操作数据库与操作文件类似，在读取修改开始和结束时都需要进行连接（打开），断开（关闭）等固定操作，
    # 文件读写时可以使用 with （上下文管理器）来简化操作，数据库当然也是可以的
    class MyDB():
        def __init__(self, host='localhost', port=3306, db='', user='root', passwd='root', charset='utf8'):
            # 建立连接
            self.conn = pymysql.connect(host=host, port=port, db=db, user=user, passwd=passwd, charset=charset)
            # 创建游标，操作设置为字典类型
            self.cur = self.conn.cursor(cursor=pymysql.cursors.DictCursor)

        def __enter__(self):
            # 返回游标
            return self.cur

        def __exit__(self, exc_type, exc_val, exc_tb):
            # 提交数据库并执行
            self.conn.commit()
            # 关闭游标
            self.cur.close()
            # 关闭数据库连接
            self.conn.close()

    sql = 'select * from course'
    with MyDB(host='192.168.68.129', user='root', passwd='zhumoran', db='text3') as cursor:
        cursor.execute(sql)
        # print(cursor)
        # for row in cursor:
        #     print(row)  # dict类型
        df = pd.DataFrame(list(cursor))
        print(df)


def test_9():
    '''
    CREATE TABLE `users` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `email` varchar(255) COLLATE utf8_bin NOT NULL,
        `password` varchar(255) COLLATE utf8_bin NOT NULL,
        PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
    AUTO_INCREMENT=1 ;
    '''
    # Connect to the database
    connection = pymysql.connect(host='localhost',
                                 user='user',
                                 password='passwd',
                                 database='db',
                                 cursorclass=pymysql.cursors.DictCursor)

    with connection:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "INSERT INTO `users` (`email`, `password`) VALUES (%s, %s)"
            cursor.execute(sql, ('webmaster@python.org', 'very-secret'))

        # connection is not autocommit by default. So you must commit to save your changes.
        connection.commit()

        with connection.cursor() as cursor:
            # Read a single record
            sql = "SELECT `id`, `password` FROM `users` WHERE `email`=%s"
            cursor.execute(sql, ('webmaster@python.org',))
            result = cursor.fetchone()
            print(result)


# ------------------------------------------------------------------------------
def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    print(pymysql.__version__)
    # print(pymysql.VERSION)
    dict_login_info = {
        "host": 'localhost',
        "user": 'root',
        "password": '123456',
        "port": 3306,
    }
    main()
