#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://github.com/tursodatabase/limbo
# https://www.toutiao.com/article/7448098082706768422
# Limbo是SQLite的一个重写版本，使用Rust语言实现，旨在提供与SQLite完全兼容的数据库，但具有完整的内存安全性和基于现代架构的新特性。
# Limbo项目的一个关键特性是异步I/O支持，它使用io_uring接口，这是由Meta的工程师为高性能异步I/O开发的Linux内核系统调用。
# 这种设计允许每个应用线程同时执行计算和I/O任务，从而显著减少尾部延迟，初步评估显示，这种方法可以将尾部延迟减少高达100倍。
# pip install pylimbo
import limbo as sl


def main():
    source_db_path = r"D:\get_snapshot_20241202.db"
    con = sl.connect(source_db_path)
    cur = con.cursor()
    res = cur.execute("SELECT * FROM snapshot")
    print(res.fetchone())

    # 执行失败
    # res = cur.execute("SELECT name FROM sqlite_master WHERE type='table';")
    # query = 'SELECT * FROM snapshot WHERE code = ?'
    # res = cur.execute(query, ("600570.SS",))
    # rows = res.fetchall()
    # for row in rows:
    #     print(row)


if __name__ == '__main__':
    main()
