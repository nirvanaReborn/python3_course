#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 这是Oracle官方提供的库，它旨在与Oracle数据库紧密集成。
# 它提供了额外的功能，如直接的SQL执行和结果集的操作。
# 性能方面，它应该与cx_Oracle相当，因为它也是直接与数据库交互的。
# 但是，它的使用可能不如cx_Oracle广泛，因此社区支持和资源可能较少。
import oracledb


def main():
    oracledb.init_pool(user="username", password="password", dsn="host:port/database", min_pool_size=5, max_pool_size=10)
    connection = oracledb.connect()
    cursor = connection.cursor()
    cursor.execute("SELECT * FROM table_name")
    for row in cursor:
        print(row)


if __name__ == '__main__':
    main()
