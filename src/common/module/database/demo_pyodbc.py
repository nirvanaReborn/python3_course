#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 数据库连接网上大致有两种方法，
# 一种是使用win32com.client
# 另一种是使用pyodbc,测试了很多遍，最终只有pyodbc成功，而且比较好用，所以这里只介绍这种方法
# https://www.connectionstrings.com/oracle-in-oraclient11g_home1/

import os

import pyodbc

from public_function import GLOBAL_WORK_DIR
from public_function import GetConfigInfo

os.environ['NLS_LANG'] = 'SIMPLIFIED CHINESE_CHINA.ZHS16GBK'  # 保证编码一致


def execute_immediate_sql(cursor):
    dict_sql = {
        "table_name": "allbranch",
    }
    # sql_1 = "INSERT INTO WeatherInfo(name, weather, temp) VALUES('%s','%s','%f')"
    # data = ("kashima", "Cloud", 24.8)
    # cursor.execute(sql_1 % data)

    # 其实就是sql语句不同而已OTL
    # sql_2 = "DELETE FROM table1 WHERE cond1='%s'"
    # cursor.execute(sql_2 % "xxx")

    # sql_3 = "UPDATE table3 SET cond1 = '%s' WHERE cond2 = '%s'"
    # cursor.execute(sql_3 % ("for_cond1", "for_cond2"))
    sql_4 = "SELECT * FROM hs_user.allbranch"
    resultSet = cursor.execute(sql_4).fetchall()  # ((xxx,1),(yyy,2)) 返回这样的查询结果
    list_head = [item[0] for item in cursor.description]
    print(list_head)
    print("查询成功：%s-%d条" % (dict_sql["table_name"], len(resultSet)))
    for fileLine in resultSet:
        print(fileLine)
        for cell in fileLine:
            # print(cell)
            pass


def odbc_connect_DSN(dict_login_oracle):
    # 连接示例:使用DSN方式
    # CONN = "DSN=%s;PWD=%s" % (DSN, PWD)
    CONN = str("DSN=%s;UID=%s;PWD=%s" %
               (dict_login_oracle["odbc_dsn"],
                dict_login_oracle["user"],
                dict_login_oracle["password"]))
    return CONN


def odbc_connect_DBQ(dict_login_oracle):
    # CONN = "DRIVER=%s;DBQ=%s;UID=%s;PWD=%s" % (DRIVER, DBQ, UID, PWD)
    CONN = str("DRIVER=%s;DBQ=%s;UID=%s;PWD=%s" %
               (dict_login_oracle["driver"], dict_login_oracle["dbq"],
                dict_login_oracle["user"], dict_login_oracle["password"]))
    return CONN


def odbc_connect_2(dict_login_oracle):
    # CONN = "DRIVER=%s;SERVER=%s;UID=%s;PWD=%s" % (DRIVER, SERVER, UID, PWD)
    CONN = str("DRIVER=%s;SERVER=%s;UID=%s;PWD=%s" %
               (dict_login_oracle["driver"], dict_login_oracle["ip"],
                dict_login_oracle["user"], dict_login_oracle["password"]))
    return CONN


def odbc_connect_3(dict_login_oracle):
    # CONN = "ODBC;DRIVER=%s;SERVER=%s;UID=%s;PWD=%s;DBQ=%s" % (DRIVER, SERVER, UID, PWD, SERVER)
    CONN = str("ODBC;DRIVER=%s;SERVER=%s;UID=%s;PWD=%s;DBQ=%s" %
               (dict_login_oracle["driver"], dict_login_oracle["ip"],
                dict_login_oracle["user"], dict_login_oracle["password"], dict_login_oracle["ip"]))
    return CONN


def odbc_connect_4(dict_login_oracle):
    # DRIVER = "{SQL Server Native Client 11.0}"
    DRIVER = "{Oracle in OraClient11g_home1}"
    # SERVER = "localhost"
    # PORT = 1521
    # DATABASE = SERVER + ':' + str(PORT) + '/' + DBQ
    # CONN = "DRIVER=%s;SERVER=%s;DATABASE=%s;UID=%s;PWD=%s" % (DRIVER, SERVER, DATABASE, UID, PWD)
    CONN = str("DRIVER=%s;SERVER=%s;DATABASE=%s;UID=%s;PWD=%s" %
               (dict_login_oracle["driver"], dict_login_oracle["ip"], dict_login_oracle["DATABASE"],
                dict_login_oracle["user"], dict_login_oracle["password"]))
    return CONN


def test_1():
    source_file = os.path.join(GLOBAL_WORK_DIR, r"获取登录信息.xml")
    dict_login_info = GetConfigInfo(source_file).get_dict_config()
    dict_login_oracle = dict_login_info.get("oracle")
    # CONN = odbc_connect_DSN(dict_login_oracle)
    # CONN = odbc_connect_2(dict_login_oracle)
    CONN = odbc_connect_DBQ(dict_login_oracle)
    db = pyodbc.connect(CONN)
    cursor = db.cursor()  # cursor和pymysql是一样用法
    cursor.fast_executemany = True  # 默认False
    cursor.connection.autocommit = False  # 默认False
    execute_immediate_sql(cursor)
    db.commit()  # commit才生效,可以进行一堆骚操作之后再commit,节省时间
    db.close()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
