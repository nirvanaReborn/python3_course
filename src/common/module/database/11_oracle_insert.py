#!/usr/bin/env python3
# coding:utf-8

# 将stkcode插入语句

import datetime

import cx_Oracle


# 方法一：用ORACLE的insert all
# insert all into hs_asset.ptfundjour(init_date,serial_no) values(20180321,1)
#           into hs_asset.ptfundjour(init_date,serial_no) values(20180321,2)
# select 1
#  from dual;
class CSVImporter:
    def __init__(self, tnsNames):
        self.TnsNames = tnsNames

    def Open(self, fileName):
        self.CsvFile = open(fileName, 'r')

        self.Rows = self.CsvFile.readlines()

    def GetColumn(self):
        # 默认表头在第一行
        tmp = self.Rows[0].strip('\n').split(',')

        for i in range(0, len(tmp)):
            tmp[i] = tmp[i].strip('"')

        self.columns = ','.join(tmp)

    def GetValue(self, Row):
        tmp = Row.strip('\n').split(',')

        for i in range(0, len(tmp)):
            tmp[i] = '\'' + tmp[i].strip('"').replace('\'', '\'\'') + '\''

        # print(','.join(tmp))
        return ','.join(tmp)

    def Connect(self, user, password):
        self.db = cx_Oracle.connect(user, password, self.TnsNames)

        self.cur = self.db.cursor()

    def DBClose(self):
        self.cur.close()

        self.db.commit()

        self.db.close()

        self.CsvFile.close()

    def Deal(self, fileName, user, password, tableName):

        self.Connect(user, password)

        self.Open(fileName)

        self.GetColumn()

        SQLBegin = 'insert all \n'
        SQLEnd = 'select 1 from dual'
        SQL = ''

        for i in range(1, len(self.Rows)):
            SQL = SQL + '           into ' + user + '.' + tableName + '(' + self.columns + ') values(' + self.GetValue(self.Rows[i]) + ')\n'

            if i % 100 == 0:
                # print(SQLBegin + SQL + SQLEnd)
                self.cur.execute(SQLBegin + SQL + SQLEnd)

                self.db.commit()

                SQL = ''

        if SQL != '':
            print(SQLBegin + SQL + SQLEnd)
            self.cur.execute(SQLBegin + SQL + SQLEnd)

            self.db.commit()

            SQL = ''

        self.DBClose()

        return True


# 方法二(这种方法会快很多,104932条记录,35秒!)：用cx_Oracle的方法executemany:
# values1 = [{'init_date': 20180321, 'serial_no': 100001},
#           {'init_date': 20180321, 'serial_no': 100002},
#           {'init_date': 20180321, 'serial_no': 100003}]
# cur.executemany('insert into hs_asset.ptfundjour(init_date,serial_no) values(:init_date,:serial_no)',values1)
class CSVImporter2:
    def __init__(self, tnsNames):
        self.TnsNames = tnsNames

    def Open(self, fileName):
        self.CsvFile = open(fileName, 'r')

        self.Rows = self.CsvFile.readlines()

    def GetColumn(self):
        # 默认表头在第一行
        tmp = self.Rows[0].strip('\n').split(',')

        for i in range(0, len(tmp)):
            tmp[i] = tmp[i].strip('"')

        self.columns = ','.join(tmp)

    def GetTab(self):
        #:0,:1,:2,:3
        tmp = self.Rows[0].strip('\n').split(',')

        for i in range(0, len(tmp)):
            if i == 0:
                self.tab = ':' + tmp[i].strip('"')
            else:
                self.tab = self.tab + ',:' + tmp[i].strip('"')

    def GetValue(self, Row):
        tableCol = self.Rows[0].strip('\n').split(',')

        tmp = Row.strip('\n').split(',')

        value = {}

        for i in range(0, len(tmp)):
            value[tableCol[i].strip('"')] = tmp[i].strip('"').replace('\'', '\'\'')

        return value

    def Connect(self, user, password):
        self.db = cx_Oracle.connect(user, password, self.TnsNames)

        self.cur = self.db.cursor()

    def DBClose(self):
        self.cur.close()

        self.db.commit()

        self.db.close()

        self.CsvFile.close()

    def Deal(self, fileName, user, password, tableName):
        self.Connect(user, password)

        self.Open(fileName)

        self.GetColumn()

        self.GetTab()

        values = []

        for i in range(1, len(self.Rows)):
            values.append((self.GetValue(self.Rows[i])))

            if i % 1000 == 0:
                self.cur.executemany('insert into ' + user + '.' + tableName + '(' + self.columns + ') values(' + self.tab + ')', values)

                self.db.commit()

                values.clear()

        if len(values) > 0:
            self.cur.executemany('insert into ' + user + '.' + tableName + '(' + self.columns + ') values(' + self.tab + ')', values)

            self.db.commit()

            values.clear()

        self.DBClose()

        return True


def main():
    print(datetime.datetime.now())

    IP = "10.20.23.16"
    port = 1521
    instance = "PBOX"
    tnsnames = '''(DESCRIPTION = 
                        (ADDRESS_LIST = 
                            (ADDRESS = (PROTOCOL = TCP)(HOST = {0})(PORT = {1}))
                        )
                        (CONNECT_DATA = 
                            (SERVICE_NAME = {2})
                        )
                   )
    '''.format(IP, port, instance)
    csvImp = CSVImporter2(tnsnames)
    csvImp.Deal('hs_user.stkcode.csv', 'hs_user', 'hundsun', 'stkcode')

    print(datetime.datetime.now())


if __name__ == '__main__':
    main()
