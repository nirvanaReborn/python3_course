#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 这个包包含一个纯 Python 和 Trio 增强的 MySQL 客户端库。
# 它是 PyMySQL 的一个非常简单的克隆，添加了与 Trio 框架兼容的异步方法。
# https://github.com/python-trio/trio-mysql
# pip install trio_mysql

import trio_mysql.cursors


def test_1():
    # Connect to the database
    connection = trio_mysql.connect(host='localhost',
                                 user='user',
                                 password='passwd',
                                 db='db',
                                 charset='utf8mb4',
                                 cursorclass=trio_mysql.cursors.DictCursor)

    async with connection as conn:
        async with conn.cursor() as cursor:
            # Create a new record
            sql = "INSERT INTO `users` (`email`, `password`) VALUES (%s, %s)"
            await cursor.execute(sql, ('webmaster@python.org', 'very-secret'))

        # connection is not autocommit by default. So you must commit to save
        # your changes.
        conn.commit()

        # You can set up a transaction:
        async with conn.transaction():  # 开启事务
            async with conn.cursor() as cursor:
                # Create a new record
                sql = "INSERT INTO `users` (`email`, `password`) VALUES (%s, %s)"
                await cursor.execute(sql, ('webmistress@python.org', 'totally-secret'))

        # ... or use a cursor directly, for autocommit:
        async with conn.cursor() as cursor:
            # Read a single record
            sql = "SELECT `id`, `password` FROM `users` WHERE `email`=%s"
            await cursor.execute(sql, ('webmaster@python.org',))
            result = await cursor.fetchone()
            print(result)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
