#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.runoob.com/python3/python-mysql-connector.html
# 用 mysql-connector 来连接使用 MySQL， mysql-connector 是 MySQL 官方提供的驱动器。
# authentication plugin 'calling_sha2_password' is not supported.

# python3 -m pip install mysql-connector-python
import mysql.connector


def test_1():
    # 创建数据库连接
    mydb = mysql.connector.connect(
        host="localhost",  # 数据库主机地址
        user="yourusername",  # 数据库用户名
        passwd="yourpassword"  # 数据库密码
    )
    print(mydb)


    mycursor = mydb.cursor()
    # 创建数据库前我们也可以使用 "SHOW DATABASES" 语句来查看数据库是否存在：
    mycursor.execute("SHOW DATABASES")
    for x in mycursor:
        print(x)

    # 创建数据库
    mycursor.execute("CREATE DATABASE runoob_db")

    # 可以使用 "SHOW TABLES" 语句来查看数据表是否已存在：
    mycursor.execute("SHOW TABLES")
    for x in mycursor:
        print(x)

    # 创建数据表
    mycursor.execute("CREATE TABLE sites (name VARCHAR(255), url VARCHAR(255))")


def test_2():
    '''直接连接数据库'''
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="123456",
        database="runoob_db"
    )
    # 如果数据库不存在，会输出错误信息
    print(mydb)


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
