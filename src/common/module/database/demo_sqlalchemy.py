#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://zhuanlan.zhihu.com/p/91169446
# https://yangsijie666.github.io/2018/07/31/sqlalchemy%E6%A8%A1%E5%9D%97/

import sqlalchemy
from sqlalchemy import create_engine, Column, String, Integer
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import os
from public_function import GetConfigInfo, GLOBAL_WORK_DIR


Base = declarative_base()


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)
    name = Column(String(64), unique=True)
    age = Column(Integer)

    def __init__(self, id, name, age):
        self.id = id
        self.name = name
        self.age = age


def test_1():
    '''
    create_engine还支持以下几个参数：
        connect_args: 一个字典，用于自定义数据库连接的参数，如指定客户端使用的字符编码
        pool_size和max_overflow: 指定连接池的大小
        poolclass: 指定连接池的实现
        echo: 布尔值，指定是否打印执行的SQL语句到日志中
    '''
    # 创建连接
    # sqlite可以创建内存数据库（其他数据库不可以），格式如下：
    # # format 1
    # engine = create_engine('sqlite://')
    # # format 2
    # engine = create_engine('sqlite:///:memory:', echo=True)
    engine = create_engine('sqlite:///my-test.db')
    # Windows
    engine = create_engine(r'sqlite:///C:\path\to\foo.db')
    # engine = create_engine("oracle+cx_oracle://username:password@host:port/database")
    # engine = create_engine('mysql+mysqlconnector://root:password@localhost:3306/test')
    # 创建DBSession类型:DBSession对象可视为当前数据库连接。
    DBSession = sessionmaker(bind=engine)
    # 创建session对象:
    session = DBSession()

    # 关闭session:
    session.close()


def test_2():
    # 初始化数据库连接:
    engine = create_engine(r'sqlite:///.\my-test.db')
    # 创建DBSession类型:DBSession对象可视为当前数据库连接。
    DBSession = sessionmaker(bind=engine)
    # 创建session对象:
    session = DBSession()
    try:
        # 创建新User对象:
        new_user = User(id=5, name='Bob', age=18)
        # 添加到session:
        session.add(new_user)
        # 提交即保存到数据库:
        session.commit()
        # 批量插入共有以下几种方法，对它们的性能做了比较，分别是：
        # session.add_all() < bulk_save_object() < bulk_insert_mappings() < SQLAlchemy_core()
    except Exception as e:
        session.rollback()
        print(e)

    # 创建Query查询，filter是where条件，最后调用first()返回唯一行，如果调用all()则返回所有行:
    instance = session.query(User).filter_by(id=5).all()
    for item in instance:
        print(type(item), item.name)

    # 关闭session:
    session.close()


def test_3():
    '''
    更新数据有两种方法，一种是使用query中的update方法：
                    另一种是操作对应的表模型：
    这两种方式呢，一般批量更新的话我会选前者，而要对查询获取对象属性之后再更新的场景就需要使用后者。
    '''
    engine = sqlalchemy.create_engine('sqlite://my-test.db')
    DBSession = sessionmaker(bind=engine)
    session = DBSession()
    try:
        # 更新数据有两种方法，一种是使用query中的update方法：
        session.query(User).filter_by(id=1).update({'name': "Jack"})
        # 另一种是操作对应的表模型：
        users = session.query(User).filter_by(name="Jack").first()
        users.name = "test"
        session.add(users)
        session.commit()
    except Exception as e:
        session.rollback()
        print(e)
    finally:
        session.close()


def test_4():
    engine = sqlalchemy.create_engine('sqlite://my-test.db')
    DBSession = sessionmaker(bind=engine)
    session = DBSession()
    try:
        # 和更新数据类似，删除数据也有两种方法，第一种：
        delete_users = session.query(User).filter(User.name == "test").first()
        if delete_users:
            session.delete(delete_users)
            session.commit()
        # 另一种是操作对应的表模型：(批量删除时推荐使用)
        session.query(User).filter(User.name == "test").delete()
        session.commit()
    except Exception as e:
        session.rollback()
        print(e)
    finally:
        session.close()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    print(sqlalchemy.__version__)
    print(sqlalchemy.sql.compiler.SQLCompiler)
    main()
