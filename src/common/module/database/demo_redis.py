#!/usr/bin/env python3
# -*- coding:utf-8 -*-


# https://germey.gitbooks.io/python3webspider/content/1.4.3-Redis%E7%9A%84%E5%AE%89%E8%A3%85.html
# Redis 是一个基于内存的高效的非关系型数据库
# 官方网站：https://redis.io
# 官方文档：https://redis.io/documentation
# 中文官网：http://www.redis.cn
# GitHub：https://github.com/antirez/redis
# 中文教程：http://www.runoob.com/redis/redis-tutorial.html
# Redis Desktop Manager：https://redisdesktop.com
# Redis Desktop Manager GitHub：https://github.com/uglide/RedisDesktopManager

# ----------------------------------------------------------------------------------------
# https://germey.gitbooks.io/python3webspider/content/1.5.3-RedisPy%E7%9A%84%E5%AE%89%E8%A3%85.html
# 对于 Redis 来说，我们要使用 RedisPy 库来与其交互
# GitHub：https://github.com/andymccurdy/redis-py
# 官方文档：https://redis-py.readthedocs.io/

# ----------------------------------------------------------------------------------------
# https://germey.gitbooks.io/python3webspider/content/1.5.4-RedisDump%E7%9A%84%E5%AE%89%E8%A3%85.html
# RedisDump 是一个用于 Redis 数据导入导出的工具，是基于 Ruby 实现的，所以要安装 RedisDump 需要先安装Ruby。
# GitHub：https://github.com/delano/redis-dump
# 官方文档：http://delanotes.com/redis-dump
#
# 有关 Ruby 的安装方式可以参考：http://www.ruby-lang.org/zh_cn/documentation/installation，
# 这里列出了所有平台的所有安装方式，可以根据对应的平台选用合适的安装方式。
#
# 安装完成之后，我们就可以执行 gem 命令了，它类似于 Python 中的 pip 命令，利用 gem 我们可以安装 RedisDump，命令如下：
# gem install redis-dump

# pip3 install redis
import redis
from redis import StrictRedis, ConnectionPool


def test_0():
    print(redis.VERSION)


# RedisPy 库提供两个类 Redis 和 StrictRedis 用于实现Redis 的命令操作。
# StrictRedis 实现了绝大部分官方的命令，参数也一一对应，比如 set() 方法就对应 Redis 命令的 set 方法。
# 而Redis 是 StrictRedis 的子类，它的主要功能是用于向后兼容旧版本库里的几个方法，
# 为了做兼容，将方法做了改写，比如 lrem() 方法就将 value 和 num 参数的位置互换，和Redis 命令行的命令参数不一致。
# 官方推荐使用 StrictRedis，所以本节我们也用 StrictRedis类的相关方法作演示。
def test_1():
    redis = StrictRedis(host='localhost', port=6379, db=0, password='foobared')
    redis.set('name', 'Bob')
    print(redis.get('name'))


def test_2():
    pool = ConnectionPool(host='localhost', port=6379, db=0, password='foobared')
    redis = StrictRedis(connection_pool=pool)


# 另外 ConnectionPool 还支持通过 URL 来构建，URL 的格式支持如下三种：
# redis://[:password]@host:port/db
# rediss://[:password]@host:port/db
# unix://[:password]@/path/to/socket.sock?db=db
def test_3():
    url = 'redis://:foobared@localhost:6379/0'
    pool = ConnectionPool.from_url(url)
    redis = StrictRedis(connection_pool=pool)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    dict_login_info = {
        "host": 'localhost',
        "user": 'root',
        "password": '123456',
        "port": 27017,
    }
    # dict_arg = {}
    main()
