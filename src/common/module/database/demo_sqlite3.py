#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/i6865581926615351820/
# 对于 SQLite，占位符是 ?，而对于 MySQL 和 PostgreSQL，占位符是 %s。
import sqlite3 as sl
import pandas as pd


def test_1():
    # 创建到数据库的连接
    conn = sl.connect('my-test.db')

    # 创建表
    with conn:
        conn.execute("""
            CREATE TABLE USER (
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                name TEXT,
                age INTEGER
            );
        """)

    # 插入记录
    sql = 'INSERT INTO USER (id, name, age) values(?, ?, ?)'
    data = [
        (1, 'Alice', 21),
        (2, 'Bob', 22),
        (3, 'Chris', 23)
    ]
    with conn:
        conn.executemany(sql, data)

    # 查询表
    with conn:
        data = conn.execute("SELECT * FROM USER WHERE age <= 22")
        for row in data:
            print(row)


# 参数化查询:为了防止SQL注入攻击，使用参数化查询而不是字符串拼接。
def test_2():
    conn = sl.connect('my-test.db')
    cursor = conn.cursor()

    # 标准SQL语句
    standard_sql = "SELECT * FROM USER WHERE age = ?"

    # 参数化查询:为了防止SQL注入攻击，使用参数化查询而不是字符串拼接。
    parameter = ('22',)

    # 执行
    cursor.execute(standard_sql, parameter)
    rows_sqlite = cursor.fetchall()
    print(rows_sqlite)

    # 关闭连接
    cursor.close()
    conn.close()


# 与Pandas无缝融合
def test_3():
    conn = sl.connect('my-test.db')

    df = pd.DataFrame({
        'user_id': [1, 1, 2, 2, 3, 3, 3],
        'skill': ['Network Security', 'Algorithm Development', 'Network Security',
                  'Java', 'Python', 'Data Science', 'Machine Learning']
    })

    # 可以简单地调用数据帧的to_sql()方法将其保存到数据库中。
    # 就这样，我们甚至不需要预先创建表，列的数据类型和长度都会被推断出来。
    # 当然，如果你想的话，仍然可以事先定义它。
    df.to_sql('SKILL', conn)


def test_4():
    conn = sl.connect('my-test.db')

    # 假设我们要连接表USER和SKILL，并将结果读入Pandas数据框。它也是无缝的。
    df = pd.read_sql('''
        SELECT s.user_id, u.name, u.age,s.skill
        FROM USER u LEFT JOIN SKILL s ON u.id = s.user_id
        ''', conn)

    # 让我们把结果写到一个名为USER_SKILL的新表中
    df.to_sql('USER_SKILL', conn)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    print(sl.version)
    print(sl.sqlite_version)
    main()
