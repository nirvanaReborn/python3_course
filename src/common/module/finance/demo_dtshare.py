#!/usr/bin/env python
# -*- coding:utf-8 -*-

# DTShare是一个完全开源免费的数据开放项目。
# 截至到目前，我们主要从证券金融作为为出发点，提供了包括股票、期货、基金和宏观经济等方面的数据
# https://github.com/DTShare/dtshare
# 详细的使用说明和文档，请访问官网地址：http://dt-share.com
# pip install dtshare
import dtshare as dt


def test_1(stock_code='600570'):
    # 股票数据
    dict_stock = {
        "实时tick数据": dt.get_tick_data(stock_code),
    }

    for key in dict_stock:
        df = dict_stock[key]
        print(df)
        df.to_excel(r'{0}_{1}.xlsx'.format(key, stock_code))


def test_2(stock_code='600570'):
    # 指数数据
    dict_index_stock = {
        "股票指数成份": dt.index_stock_info(),
    }

    for key in dict_index_stock:
        df = dict_index_stock[key]
        print(df)
        df.to_excel(r'{0}_{1}.xlsx'.format(key, stock_code))


def test_3():
    # 基金数据
    dict_fund = {
        "基金会员机构综合查询": dt.amac_member_info(),
        "基金从业人员资格注册信息": dt.amac_person_org_list(),
        "基金从业人员资格注册外部公示信息": dt.amac_person_org_list_ext(),
        "私募基金管理人综合查询": dt.amac_manager_info(),
        "私募基金管理人分类公示": dt.amac_manager_classify_info(),
        "证券公司私募基金子公司管理人信息公示":dt.amac_member_sub_info(),
        "私募基金管理人基金产品": dt.amac_fund_info(),
        "证券公司集合资管产品公示": dt.amac_securities_info(),
        "证券公司直投基金": dt.amac_aoin_info(),
        "证券公司私募投资基金":dt.amac_fund_sub_info(),
        "基金公司及子公司集合资管产品公示":dt.amac_fund_account_info(),
        "资产支持专项计划":dt.amac_fund_abs(),
        "期货公司集合资管产品公示":dt.amac_futures_info(),
        "违反自律规则黑名单":dt.amac_manager_xxgs_hmd(),
        "纪律处分": dt.amac_manager_xxgs_jlcf(),
        "撤销管理人登记的名单": dt.amac_manager_xxgs_cxdj(),
        "已注销私募基金管理人名单": dt.amac_manager_cancelled_info(),
        "中国私募指数数据":dt.zdzk_fund_index(index_type=32, plot=True),
    }

    for key in dict_fund:
        df = dict_fund[key]
        print(df)
        df.to_excel(r'%s.xlsx' % key)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
