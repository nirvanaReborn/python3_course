#!/usr/bin/env python
# -*- coding:utf-8 -*-

# BaoStock:一个免费、开源的python证券数据接口包
# https://blog.csdn.net/dongdong2980/article/details/79059789
# http://baostock.com/baostock/index.php

import baostock as bs
import pandas as pd

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
# pd.set_option('display.height', 1000)
pd.set_option('display.width', 1000)


def test_0():
    #### 登陆系统 ####
    lg = bs.login(user_id="anonymous", password="123456")
    # 显示登陆返回信息
    print('login respond error_code:' + lg.error_code)  # 当为“0”时表示成功，当为非0时表示失败
    print('login respond error_msg:' + lg.error_msg)

    #### 获取历史K线数据 ####
    # 详细指标参数，参见“历史行情指标参数”章节
    rs = bs.query_history_k_data("sh.600000",
                                 "date,code,open,high,low,close,preclose,volume,amount,adjustflag,turn,tradestatus,pctChg,peTTM,pbMRQ,psTTM,pcfNcfTTM,isST",
                                 start_date='2017-06-01',  # 为空时取2015-01-01
                                 end_date='2017-12-31',  # 为空时取最近一个交易日；
                                 frequency="d",  # 取日k线(d=日k线、w=周、m=月、5=5分钟、15=15分钟、30=30分钟、60=60分钟k线数据，不区分大小写；周线每周最后一个交易日才可以获取，月线第月最后一个交易日才可以获取。)
                                 adjustflag="3")  # 默认3-不复权, 可查询: 3-不复权、2-前复权、1-后复权数据。
    print('query_history_k_data respond error_code:' + rs.error_code)
    print('query_history_k_data respond  error_msg:' + rs.error_msg)

    #### 打印结果集 ####
    '''
    参数名称	参数描述
    date	    交易所行情日期
    code	    证券代码
    open	    开盘价
    high	    最高价
    low	        最低价
    close	    收盘价
    preclose	昨日收盘价
    volume	    成交量（累计 单位：股）
    amount	    成交额（单位：人民币元）
    adjustflag	复权状态(1：后复权， 2：前复权，3：不复权）
    turn	    换手率
    tradestatus	交易状态(1：正常交易 0：停牌）
    pctChg	    涨跌幅
    peTTM	    动态市盈率
    pbMRQ	    市净率
    psTTM	    市销率
    pcfNcfTTM	市现率
    isST	    是否ST股，1是，0否
    '''
    data_list = []
    while (rs.error_code == '0') & rs.next():
        # 获取一条记录，将记录合并在一起
        data_list.append(rs.get_row_data())
    result = pd.DataFrame(data_list, columns=rs.fields)

    #### 结果集输出到csv文件 ####
    result.to_csv("history_k_data.csv", encoding="gbk", index=False)
    print(result)

    #### 登出系统 ####
    bs.logout(user_id="anonymous")


def query_history_k_data():
    # 详细指标参数，参见“历史行情指标参数”章节；“分钟线”参数与“日线”参数不同。“分钟线”不包含指数。
    # 分钟线指标：date,time,code,open,high,low,close,volume,amount,adjustflag
    # 周月线指标：date,code,open,high,low,close,volume,amount,adjustflag,turn,pctChg
    rs = bs.query_history_k_data_plus(code="sh.600570",
                                      fields="date,code,open,high,low,close,preclose,volume,amount,adjustflag,turn,tradestatus,pctChg,isST",
                                      start_date='2017-07-01',
                                      end_date='2017-12-31',
                                      frequency="d",
                                      adjustflag="3")
    if rs.error_code != 0:
        print('query_history_k_data_plus respond error_code:' + rs.error_code)
        print('query_history_k_data_plus respond  error_msg:' + rs.error_msg)

    #### 打印结果集 ####
    data_list = []
    while (rs.error_code == '0') & rs.next():
        # 获取一条记录，将记录合并在一起
        data_list.append(rs.get_row_data())
    result = pd.DataFrame(data_list, columns=rs.fields)
    return result


def get_all_market_codes(day=None):
    #### 获取某日所有证券信息 ####
    rs = bs.query_all_stock(day="2024-10-25")
    print('query_all_stock respond error_code:' + rs.error_code)
    print('query_all_stock respond  error_msg:' + rs.error_msg)

    #### 打印结果集 ####
    data_list = []
    while (rs.error_code == '0') & rs.next():
        # 获取一条记录，将记录合并在一起
        data_list.append(rs.get_row_data())
    result = pd.DataFrame(data_list, columns=rs.fields)
    return result


def switch(x):
    return {
        # 获取历史A股K线数据
        "1": query_history_k_data(),
        # 获取某日所有证券信息
        "2": get_all_market_codes(),
    }.get(x, pd.DataFrame())


def get_data(choice):
    #### 登陆系统 ####
    lg = bs.login()
    # 显示登陆返回信息
    if lg.error_code != 0:
        print('login respond error_code:' + lg.error_code)
        print('login respond  error_msg:' + lg.error_msg)

    result = switch(choice)

    #### 结果集输出到csv文件 ####
    # result.to_csv("D:\\history_A_stock_k_data.csv", index=False)
    print(result)

    #### 登出系统 ####
    bs.logout()


def main():
    # dict_choice = {}
    # for i in range(0, 100):
    #     dict_choice[str(i)] = "test_{}()".format(str(i))
    #
    # choice = str(input("Enter a positive integer to choice: "))
    # if choice in dict_choice.keys():
    #     exec(dict_choice[choice])
    # else:
    #     print("输入有误，请重输！", choice)
    choice = str(input("Enter a positive integer to choice: "))
    get_data(choice)


if __name__ == "__main__":
    main()
