#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Tushare ：一个可以提供免费股票、基金、期货、港股等金融数据的 Python 开源数据。
# 官网: (http://tushare.org)
# 本接口即将停止更新，请尽快使用Pro版接口：https://tushare.pro/document/2
# https://blog.csdn.net/sinat_40875078/article/details/105475342
# https://blog.csdn.net/new_stranger/article/details/83346258
# https://www.zhihu.com/topic/20064087/hot

import tushare as dt


# https://blog.csdn.net/codeksy/article/details/90647140
def test_1(stock_code='600570'):
    dict_stock = {
        "获取历史数据": dt.get_hist_data(stock_code),
        "获取所有股票的交易数据": dt.get_today_all(),
    }

    for key in dict_stock:
        df = dict_stock[key]
        print(df)
        df.to_excel(r'{0}_{1}.xlsx'.format(key, stock_code))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
