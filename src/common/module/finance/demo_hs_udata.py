#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# tabulate默认没有考虑中文字符宽度，因此无法对齐，如要实现对齐，需要wcwidth 包。
from tabulate import tabulate
import wcwidth

tabulate.PRESERVE_WHITESPACE = True  # 保留空格
tabulate.WIDE_CHARS_MODE = False  # 宽（全角中日韩）符号

import pandas as pd

pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
# pd.set_option('display.height', 1000)
pd.set_option('display.width', 1000)

import hs_udata as hsd

hsd.set_token(token='_OZO0lW19H9pmbik2meLXCuYDoAOVsLimbgWuQY5DR8eriACzkx6jtZ0rxPwDrwi')


def switch(x):
    kwargs = {
        "factor_name": "",
        "stock_code": "600570.SH",
        "start_date": "20211201",
        "end_date": "20211231",
    }
    return {
        # 获取因子列表
        "1": hsd.get_factor_list(),
        # 获取贝塔因子
        "2": hsd.get_beta_factor(**kwargs),
        # 获取波动率因子
        "3": hsd.get_volatility_factor(**kwargs),
        # 获取偿债因子
        "4": hsd.get_solvency_factor(**kwargs),
        # 获取成长因子
        "5": hsd.get_growth_factor(**kwargs),
        # 获取动量因子
        "6": hsd.get_momentum_factor(**kwargs),
        # 获取分钟统计因子
        "7": hsd.get_minute_factor(**kwargs),
        # 获取风格barra因子
        "8": hsd.get_barra_factor(**kwargs),
        # 获取风格基础因子
        "9": hsd.get_barra_basic_factor(**kwargs),
        # 获取风格描述因子
        "10": hsd.get_barra_description_factor(**kwargs),
        # 获取杠杆因子
        "11": hsd.get_leverage_factor(**kwargs),
        # 获取估值因子
        "12": hsd.get_valueation_factor(**kwargs),
        # 获取流动性因子
        "13": hsd.get_liquidity_factor(**kwargs),
        # 获取情绪因子
        "14": hsd.get_sentiment_factor(**kwargs),
        # 获取市场规模因子
        "15": hsd.get_size_factor(**kwargs),
        # 获取事件因子
        "16": hsd.get_event_factor(**kwargs),
        # 获取现金流因子
        "17": hsd.get_cash_flow_factor(**kwargs),
        # 获取一致预期因子
        "18": hsd.get_consensus_factor(**kwargs),
        # 获取盈利因子
        "19": hsd.get_earning_factor(**kwargs),
        # 获取营运能力因子
        "20": hsd.get_operation_factor(**kwargs),
        # 获取质量因子
        "21": hsd.get_quality_factor(**kwargs),
    }.get(x, pd.DataFrame())


def main():
    choice = str(input("Enter a positive integer to choice: "))
    data = switch(choice)
    # print(data.head().to_string(index=False))
    # https://blog.csdn.net/The_Time_Runner/article/details/88791587
    print(tabulate(data.head(), headers='keys', tablefmt='fancy_grid'))


if __name__ == '__main__':
    main()
