#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/lemonbit/article/details/114324625
# 可视化神器highcharts
# pip install python-highcharts
from highcharts import Highchart


def test_1():
    # 1-实例化
    chart = Highchart()

    # 2-配置项设置
    options = {
        'chart': {
            'inverted': True  # 翻转x轴和y轴
        },
        'title': {  # 主标题
            'text': 'Atmosphere Temperature by Altitude'
        },
        'subtitle': {  # 副标题
            'text': 'According to the Standard Atmosphere Model'
        },
        'xAxis': {  # x轴设置
            'reversed': False,
            'title': {
                'enabled': True,
                'text': 'Altitude'
            },
            'labels': {
                'formatter': 'function () {\
                    return this.value + "km";\
                }'
            },
            'maxPadding': 0.05,
            'showLastLabel': True
        },
        'yAxis': {  # y轴设置
            'title': {
                'text': 'Temperature'
            },
            'labels': {
                'formatter': "function () {\
                    return this.value + '°';\
                }"
            },
            'lineWidth': 2
        },
        'legend': {  # 图例设置
            'enabled': False
        },
        'tooltip': {  # 提示工具设置
            'headerFormat': '<b>{series.name}</b><br/>',
            'pointFormat': '{point.x} km: {point.y}°C'
        }
    }

    # 3-实例化对象中添加字典形式的配置项
    chart.set_dict_options(options)

    # 4-准备绘图所需的数据和添加数据
    data = [[0, 15],
            [10, -50],
            [20, -56.5],
            [30, -46.5],
            [40, -22.1],
            [50, -2.5],
            [60, -27.7],
            [70, -55.7],
            [80, -76.5]]
    # 添加数据
    chart.add_data_set(data, 'spline', 'Temperature', marker={'enabled': False})

    # 5-notebook在线绘图
    chart.save_file('1')


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
