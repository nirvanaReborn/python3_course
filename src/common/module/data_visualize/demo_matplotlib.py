#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
想绘制函数图象，自然想到了python中强大的绘图库matplotlib。
网上查询资料，说是matplotlib下的模块pyplot和pylab均可以，
于是便产生疑问，这二者之间有何区别和联系？于是展开调查。

对Pyplot的解说：“方便快速绘图matplotlib通过pyplot模块提供了一套和MATLAB类似的绘图API，
                将众多绘图对象所构成的复杂结构隐藏在这套API内部。”

对pylab的解说：“matplotlib还提供了一个名为pylab的模块，其中包括了许多NumPy和pyplot模块中常用的函数，
                方便用户快速进行计算和绘图，十分适合在IPython交互式环境中使用。”

意思就是说pylab结合了pyplot和numpy，对交互式使用来说比较方便，既可以画图又可以进行简单的计算。
但是，对于一个项目来说，建议分别导入使用。
'''
import numpy as np
import matplotlib.pyplot as plt
# import pylab  # 由于pylab是matplotlib中的一部分，所以直接下载matplotlib库。


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
