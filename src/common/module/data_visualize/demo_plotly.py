#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://zhuanlan.zhihu.com/p/26461072?utm_source=weibo&utm_medium=social
# 利用plotly for Python进行数据可视化

# plotly是一款强大的作图工具，可以快速制作各种精美的图表，而且生成的图表可以实时与用户产生交互，大家可以参考官网(https://plot.ly/)的示例。
# plotly本身是基于javascript开发的，但是提供了大量与其他主流数据分析语言的API，比如Python， R， Matlab，可以参考https://plot.ly/api/。
# 如果你是Python用户，plotly不仅可以从原生的Python获得支持，如果你正在使用matplotlib，pandas等库，plotly也有专门为其量身定制的接口。


import pandas as pd
import plotly.graph_objs as go
import plotly.plotly as py
import plotly.express as px


def cross(x, y):
    line1 = dict(
        type='line',
        xref='x',
        yref='y',
        x0=x - 4,
        y0=y - 5,
        x1=x + 4,
        y1=y + 5,
        line=dict(
            color='black',
            width=2,
        )
    )
    line2 = dict(
        type='line',
        xref='x',
        yref='y',
        x0=x - 4,
        y0=y + 5,
        x1=x + 4,
        y1=y - 5,
        line=dict(
            color='black',
            width=2,
        )
    )
    return line1, line2


def test_1():
    csv_file = '30_-10_full.csv'
    data = pd.read_csv(csv_file)

    aes = go.Scatter(
        x=data['MH5'],
        y=data['MH3'],
        mode='markers',
        name='MH',
        marker=dict(
            color=data['MH'],
            size='5',
            colorscale='Viridis',
            showscale=True
        )
    )
    line1 = dict(
        type='line',
        xref='x',
        yref='y',
        x0=200,
        y0=100,
        x1=200,
        y1=600,
        line=dict(
            color='black',
            width=2,
        )
    )
    line2 = dict(
        type='line',
        xref='x',
        yref='y',
        x0=100,
        y0=180.385,
        x1=519.615,
        y1=600,
        line=dict(
            color='black',
            width=2,
        )
    )
    layout = go.Layout(
        title='$MH_5, MH_3$',
        autosize=False,
        width=1000,
        height=800,
        shapes=[
            line1,
            line2,
            cross(230, 530)[0],
            cross(230, 530)[1],
            cross(230, 430)[0],
            cross(230, 430)[1],
            cross(230, 330)[0],
            cross(230, 330)[1],
            cross(300, 530)[0],
            cross(300, 530)[1],
            cross(300, 430)[0],
            cross(300, 430)[1],
            cross(370, 530)[0],
            cross(370, 530)[1]
        ],
        xaxis=dict(
            title='$MH_5$'
        ),
        yaxis=dict(
            title='$MH_3$'
        ),
    )
    fig = go.Figure(data=[aes], layout=layout)
    py.iplot(fig)


def test_2():
    # https://www.toutiao.com/a7067069750807003655
    import pandas as pd
    data = pd.read_excel("D:/data/旭日图数据.xlsx")
    data.head()
    '''
    第一个参数：data，就是第一步中读取进来的数据。
    第二个参数：path，即路径，就是将数据中的前三列的名称搬下来。
    第三个参数：values，数值，这里引用数据集中的“数量”列。
    第四个参数：color，颜色，这里设置为“数量”，即用颜色来区分数量。
    '''
    fig = px.sunburst(data, path=['地区', '省/自治区', '城市'], values='数量', color='数量')
    fig.show()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
