#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.jianshu.com/p/865b01f83378
# Python交互式数据分析报告框架Dash介绍
'''
用Python做数据分析,试用了包括Bokeh、mpld3、Highcharts，以及键冬同学基于百度Echarts开发的PyEcharts，
但是这些都是基于Web的交互视图库，而非Shiny那种能将文档、表格、视图整合在一起的交互式数据分析报告框架。
今年翻译《2017年10大Python库》这篇文章时看到了Dash，这正是我寻觅已久的，可以用它制作出更优秀的数据分析报告。。
另外，后来还发现了Pyxley，也是基于React和Flask的，有兴趣的朋友也可以了解一下。

Dash与Excel都采用了“响应式”的程序模型。在Excel中，输入单元格发生变化时，输出单元格也会自动更新。
所有单元格都可以是输出或输入，也可以同时既是输入也是输出。


开源库:
Dash入门指南：https://plot.ly/dash/

Github上的Dash源码：https://github.com/plotly/dash

Dash后端：https://github.com/plotly/dash

Dash前端：https://github.com/plotly/dash-renderer

Dash核心组件库：https://github.com/plotly/dash-core-components

Dash HTML组件库：https://github.com/plotly/dash-html-components

Dash组件原型 (React-to-Dash工具链)：https://github.com/plotly/dash-components-archetype

Dash文档与使用指南：https://github.com/plotly/dash-docs, https://plot.ly/dash

Plotly.js — Dash使用的图形库：https://github.com/plotly/plotly.js


'''
# pip install dash dash-renderer dash-html-components dash-core-components
import dash
import dash_core_components
import dash_html_components
import numpy


# http://blog.csdn.net/hu_zhenghui/article/details/78303032
def test_1():
    t = numpy.linspace(0, 2 * numpy.pi, 100)
    x = 10 * (2 * numpy.sin(t) - numpy.sin(2 * t))
    y = 10 * (2 * numpy.cos(t) - numpy.cos(2 * t))

    # 实例化一个Dash对象，每个dash应用必须步骤
    app = dash.Dash()

    app.layout = dash_html_components.Div(children=[
        dash_html_components.H1(children='Hello! Dash love you! '),

        dash_core_components.Graph(
            id='heart-curve',
            figure={
                'data': [
                    {
                        'x': x,
                        'y': y,
                        'type': 'Scatter',
                        'name': 'Heart'
                    },
                ],
                'layout': {
                    'title': 'Heart Curve'  # 图表名称
                }
            }
        )
    ])

    # 端口默认为8050
    app.run_server(debug=True)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
