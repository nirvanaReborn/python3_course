#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://cloud.tencent.com/developer/article/2040007?from=15425
# https://blog.csdn.net/qq_36793268/article/details/119155633
# https://www.cjavapy.com/article/243/
# https://www.cnblogs.com/new-june/p/9401002.html
# https://www.cnblogs.com/louzi/p/10651767.html
# https://pycryptodome.readthedocs.io/en/latest/src/vs_pycrypto.html
# pip install pycryto
# PyCrypto 是加密工具包 Crypto 的 python 版，该模块实现了各种算法和协议的加密，
# 提供了各种加密方式对应的算法的实现，包括单向加密、对称加密及公钥加密和随机数操作等。
# 但是 PyCrypto 工具包在 2013 年之后就不再更新了，而PyCryptodome 是来代替 PyCrypto的。
# https://blog.csdn.net/qq_28205153/article/details/55798628
import os, random, struct
# https://blog.csdn.net/weixin_44133008/article/details/107102575
# pip install pycryptodome
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes


try:
    from Crypto.Util.Padding import pad, unpad
except ImportError:
    from Crypto.Util.py3compat import bchr, bord


    def pad(data_to_pad, block_size):
        padding_len = block_size - len(data_to_pad) % block_size
        padding = bchr(padding_len) * padding_len
        return data_to_pad + padding


    def unpad(padded_data, block_size):
        pdata_len = len(padded_data)
        if pdata_len % block_size:
            raise ValueError("Input data is not padded")
        padding_len = bord(padded_data[-1])
        if padding_len < 1 or padding_len > min(block_size, pdata_len):
            raise ValueError("Padding is incorrect.")
        if padded_data[-padding_len:] != bchr(padding_len) * padding_len:
            raise ValueError("PKCS#7 padding is incorrect.")
        return padded_data[:-padding_len]


# 文件加密
def encrypt_file(key, in_filename, out_filename=None, chunksize=64 * 1024):
    if not out_filename:
        out_filename = in_filename + '.enc'
    iv = os.urandom(16)
    encryptor = AES.new(key, AES.MODE_CBC, iv)
    filesize = os.path.getsize(in_filename)
    with open(in_filename, 'rb') as infile:
        with open(out_filename, 'wb') as outfile:
            outfile.write(struct.pack('<Q', filesize))
            outfile.write(iv)
            pos = 0
            while pos < filesize:
                chunk = infile.read(chunksize)
                pos += len(chunk)
                if pos == filesize:
                    chunk = pad(chunk, AES.block_size)
                outfile.write(encryptor.encrypt(chunk))


# 文件解密
def decrypt_file(key, in_filename, out_filename=None, chunksize=64 * 1024):
    if not out_filename:
        out_filename = in_filename + '.dec'
    with open(in_filename, 'rb') as infile:
        filesize = struct.unpack('<Q', infile.read(8))[0]
        iv = infile.read(16)
        encryptor = AES.new(key, AES.MODE_CBC, iv)
        with open(out_filename, 'wb') as outfile:
            encrypted_filesize = os.path.getsize(in_filename)
            pos = 8 + 16  # the filesize and IV.
            while pos < encrypted_filesize:
                chunk = infile.read(chunksize)
                pos += len(chunk)
                chunk = encryptor.decrypt(chunk)
                if pos == encrypted_filesize:
                    chunk = unpad(chunk, AES.block_size)
                outfile.write(chunk)


def test_1():
    encrypt_file("keyskeyskeyskeys".encode('utf-8'), r"D:\drivedbg.txt")
    decrypt_file("keyskeyskeyskeys".encode('utf-8'), r"D:\drivedbg.txt.enc")


def test_2():
    key = get_random_bytes(16)
    print(key)
    cipher = AES.new(key, AES.MODE_EAX)
    # 要加密的数据
    data = "test".encode('utf-8')
    ciphertext = cipher.encrypt(data)

    with open("encrypted.bin", "wb") as f:
        f.write(ciphertext)
    # 当然我们如果不知道密钥，只知道AES加密后的密文，依旧很难推算出明文，
    # AES加密正是因为它的快速和绝对安全，才会在诸多领域有着广泛的应用，
    # 比如：家用路由器就普遍采用AES的加密算法。


def test_3():
    key = b'\x8cy\xff1\x91q/\xa4)\x9c\xd5jH\x12\xd6\xb0'
    cipher = AES.new(key, AES.MODE_EAX)
    with open("encrypted.bin", "rb") as f:
        print(cipher.decrypt(f.read()).decode())


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
