#!/usr/bin/env python
# -*- coding: utf-8 -*-

# https://www.toutiao.com/a6315136816966910210/
# 利用Python自动生成暴力破解的字典

import itertools


def auto_generation_brute_force_dict(words):
    with open("pass.txt", "a") as fw:
        for i in itertools.product(words, repeat=6):
            fw.write("".join(i))


def main():
    words = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    auto_generation_brute_force_dict(words)


if __name__ == "__main__":
    main()
