#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import base64


def main():
    # 示例文本
    text = "Hello, World!"

    # 编码将字符串转换为字节，然后进行Base64编码
    # 编码
    encoded_bytes = base64.b64encode(text.encode('utf-8'))
    encoded_str = encoded_bytes.decode('utf-8')
    print(f"编码后的字符串: {encoded_str}")  # SGVsbG8sIFdvcmxkIQ==

    # 解码将Base64解码为字节，然后解码为字符串
    # 解码
    decoded_bytes = base64.b64decode(encoded_str.encode('utf-8'))
    decoded_str = decoded_bytes.decode('utf-8')
    print(f"解码后的字符串: {decoded_str}")


if __name__ == '__main__':
    main()
