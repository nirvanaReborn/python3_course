#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.jb51.net/article/171500.htm
# 哈希方法主要有MD、SHA、Murmur、CityHash、MAC等几种方法。
# mmh3全程murmurhash3,是一种非加密的哈希算法，常用于hadoop等分布式存储情境中。
'''
建议安装的版本的Visual Studio2015，因为2015对应的是C++ 14的版本，python3是基于C++ 14编译的,
如果是python2的mmh3需要对应C++ 9的版本，这主要是因为python2是基于C++ 9编译的，
亲测安装VS2019并没有解决C++ 14缺失的问题。
'''
# pip install mmh3
import mmh3


def test_1():
    print(mmh3.hash('foo'))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
