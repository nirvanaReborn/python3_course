#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a7052976693597831691
# 用Python加密文件
# 加密文件或文件夹中的所有文件: python encrypt.py e 文件/文件夹
# 解密文件或文件夹中的所有文件: python encrypt.py d 文件/文件夹

import sys
import os


def is_text(filename):
    """
    判断文件是否文本文件
    :param filename:
    :return:
    """
    suffix = ['txt', 'text', 'py']
    if filename.split('.')[-1] in suffix:
        return True
    else:
        return False


def text_encrypt(filename):
    """
    文本文件加密
    :param filename:
    :return:
    """
    try:
        f = open(filename, 'r+', encoding='gbk')
        content = f.read()
    except UnicodeDecodeError as e:
        f = open(filename, 'r+', encoding='utf-8')
        content = f.read()

    f.seek(0)
    for c in content:
        f.write(chr(ord(c) + 10))
    print('加密文件{}成功！'.format(filename))


def text_decrypt(filename):
    """
    文本文件解密
    :param filename:
    :return:
    """
    try:
        f = open(filename, 'r+', encoding='gbk')
        content = f.read()
    except UnicodeDecodeError as e:
        f = open(filename, 'r+', encoding='utf-8')
        content = f.read()

    f.seek(0)
    for c in content:
        f.write(chr(ord(c) - 10))
    print('解密文件{}成功！'.format(filename))


def bytes_encrypt(filename):
    with open(filename, 'rb+') as f:
        first = f.read(10)
        f.seek(-10, 2)
        last = f.read()
        f.seek(0)
        f.write(last)
        f.seek(-10, 2)
        f.write(first)


def encrypt(filename):
    """
    加密文件
    :param filename:
    :return:
    """
    if is_text(filename):
        text_encrypt(filename)
    else:
        bytes_encrypt(filename)
        print('加密文件{}成功！'.format(filename))


def decrypt(filename):
    """
    解密文件
    :param filename:
    :return:
    """
    if is_text(filename):
        text_decrypt(filename)
    else:
        bytes_encrypt(filename)
    print('解密文件{}成功！'.format(filename))


def get_file_list(path):
    for root, dirs, files in os.walk(path):
        return [os.path.join(root, f) for f in files]


def main():
    try:
        _, type, path = sys.argv
    except Exception as e:
        exit('请输入正确的参数')

    if type == 'e':
        if os.path.isdir(path):
            # 加密当前文件夹下所有文件
            files = get_file_list(path)
            for file in files:
                encrypt(file)
        else:
            # 加密文件
            encrypt(path)
    elif type == 'd':
        if os.path.isdir(path):
            # 解密当前文件加下所有文件
            files = get_file_list(path)
            for file in files:
                decrypt(file)
        else:
            # 解密文件
            decrypt(path)
    else:
        print('请输入正确的参数')


if __name__ == "__main__":
    main()
