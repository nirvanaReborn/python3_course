#!/usr/bin/env python
# -*- coding: UTF-8 -*-

'''
http://www.toutiao.com/a6320045003499176194/
更安全的加密工具：bcrypt
gitlab就使用默认的bcrypt作为密码的加密算法。
'''
# pip install bcrypt
import bcrypt


def main():
    password = b"123456"

    # 首先生成盐，此函数有两个参数，
    # 第一个参数是工作复杂度系数，参数范围：4到31，默认12；
    # 第二个参数为前缀(prefix)，可选(b"2a", b"2b"),默认：b"2b"。
    salt = bcrypt.gensalt()

    # 使用密码和盐进行加密
    encrypt_password = bcrypt.hashpw(password, salt)
    print(encrypt_password)

    # 验证过程其实就是对明文和密文重新加密再与密文比较
    if bcrypt.hashpw(password, encrypt_password) == encrypt_password:
        print("It Matches!")
    else:
        print("It Does not Match!")


if __name__ == "__main__":
    main()
