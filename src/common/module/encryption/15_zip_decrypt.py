#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a7055303873539441190
# 用Python实现zip密码破解实例
# python 15_zip_decrypt.py -f xxx.zip -d pass.txt

import zipfile
import optparse
from threading import Thread


def extractFile(zFile, password):
    try:
        zFile.extractall(pwd=password)
        print("[+] Found password " + password + '\n')
    except Exception as e:
        print("[-] " + password + "  Failed:", e)


def main():
    parser = optparse.OptionParser("usage%prog" + "-f <zipfile> -d <dictionary>")
    parser.add_option('-f', dest='zname', type='string', help='specify zip file')
    parser.add_option('-d', dest='dname', type='string', help='specify dictionary file')
    (options, args) = parser.parse_args()
    if (options.zname is None) | (options.dname is None):
        print(parser.usage)
        exit(0)
    else:
        zname = options.zname
        dname = options.dname
    zFile = zipfile.ZipFile(zname)
    passFile = open(dname)
    for line in passFile.readlines():
        password = line.strip('\n')
        print("[*] Trying password: " + password)
        t = Thread(target=extractFile, args=(zFile, password))
        t.start()


if __name__ == '__main__':
    main()
