#!/usr/bin/env python
# -*- coding: UTF-8 -*-


'''
http://www.cnblogs.com/the4king/archive/2012/02/06/2340660.html
MD5
    MD5的全称是Message-Digest Algorithm 5（信息-摘要算法）。128位长度。目前MD5是一种不可逆算法。
    具有很高的安全性。它对应任何字符串都可以加密成一段唯一的固定长度的代码。

SHA1
    SHA1的全称是Secure Hash Algorithm(安全哈希算法) 。SHA1基于MD5，加密后的数据长度更长，
    它对长度小于264的输入，产生长度为160bit的散列值。比MD5多32位。
    因此，比MD5更加安全，但SHA1的运算速度就比MD5要慢了。

http://www.cnblogs.com/thinkingfor/archive/2010/09/13/1824766.html
使用Python进行文件Hash计算有两点必须要注意：
1、文件打开方式一定要是二进制方式，既打开文件时使用b模式，
    否则Hash计算是基于文本的那将得到错误的文件Hash(网上看到有人说遇到Python的Hash计算错误在大多是由于这个原因造成的)。
2、对于MD5如果需要16位(bytes)的值那么调用对象的digest(),而hexdigest(）默认是32位(bytes)，
    同理Sha1的digest()和hexdigest()分别产生20位(bytes)和40位(bytes)的hash值
'''
# Python 内置的 hashlib 模块就包括了 md5 和 sha1 算法。
import hashlib

'''
处理大文件：
上面说过可以用MD5来检测两个文件是否相同，但想想，如果是两个很大的文件，担心内存不够用，这时怎么办？
这就要使用 update 方法了。
'''


def test_update():
    x = hashlib.md5()
    x.update('hello, '.encode())
    x.update('python'.encode())
    print(x.hexdigest())
    print(hashlib.md5('hello, python'.encode()).hexdigest())
    # 这两次的输出是一样的


def test_file(source_file):
    with open(source_file, 'rb') as fd:
        m = hashlib.md5()

        while True:
            data = fd.read(10240)
            if not data:
                break

            m.update(data)

        file_md5 = m.hexdigest()
    return file_md5


def test_md5(data):
    hash_value = hashlib.md5(data.encode())
    # hash.digest() 返回摘要，作为二进制数据字符串值
    print(hash_value.digest())  # 产生16位(bytes)的值
    # hash.hexdigest() 返回摘要，作为十六进制数据字符串值
    print(hash_value.hexdigest())  # 产生32位(bytes)的值


def test_sha1(data):
    hash_value = hashlib.sha1(data.encode())
    print(hash_value.digest())  # 产生20位(bytes)的值
    print(hash_value.hexdigest())  # 产生40位(bytes)的值


# ----------------------------------------------------------------
def data_mining(data, hash_type):
    if type(data).__name__ == "str":
        data = data.encode()

    if type(data).__name__ == "bytes":
        if "md5" == hash_type:
            hash_value = hashlib.md5(data)
        elif "sha1" == hash_type:
            hash_value = hashlib.sha1(data)
        else:
            hash_value = None
    else:
        print(type(data).__name__)
        return
    return hash_value.hexdigest()


def file_mining(source_file, hash_type, block_size=256 * 128, hr=True):
    if "md5" == hash_type:
        m = hashlib.md5()
    elif "sha1" == hash_type:
        m = hashlib.sha1()
    else:
        m = None

    with open(source_file, 'rb') as fd:
        for chunk in iter(lambda: fd.read(block_size), b''):
            m.update(chunk)
    if hr:
        return m.hexdigest()

    return m.digest()


def main():
    # linux系统: echo -n "This is a test！"|md5sum
    data = "This is a test！"
    # test_md5(data)
    # test_sha1(data)
    # test_update()
    # test_file(source_file)

    dict_choice = {
        "0": "md5",
        "1": "sha1"
    }

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        # print(dict_choice[choice], ":", data_mining(data, dict_choice[choice]))
        source_file = r"D:\SVN\SVN_test\trunk\LDP\bin\components\libtcp.so"
        print(dict_choice[choice], ":", file_mining(source_file, dict_choice[choice]))
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
