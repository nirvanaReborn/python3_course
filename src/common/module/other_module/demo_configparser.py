#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 该类的作用是使用配置文件生效，配置文件的格式和windows的INI文件的格式相同
# https://blog.csdn.net/a54288447/article/details/81027593
# https://blog.csdn.net/angelpumpkin/article/details/80067411
# import ConfigParser
# 当前模块的名称在 python 3.x 版本中变更为 configparser。
import configparser


# 创建配置文件
# 创建的key-value中里面如果有数字，需要加引号变成string形式，否则会报数据类型错误。
def test_1():
    config = configparser.ConfigParser()
    # 开始创建第一个section
    config.add_section('Section1')
    config.set('Section1', 'an_int', '15')
    config.set('Section1', 'a_bool', 'true')
    config.set('Section1', 'a_float', '3.1415')
    config.set('Section1', 'baz', 'fun')
    config.set('Section1', 'bar', 'Python')
    config.set('Section1', 'foo', '%(bar)s is %(baz)s!')

    # 开始创建第二个section，类似于list赋值方式，值为dict
    config['DEFAULT'] = {'compressionlevel': '9',
                         'serveraliveinterval': '45',
                         'compression': 'yes'}
    config['DEFAULT']['forwardx11'] = 'yes'

    # 创建第三个section
    config['bitbucket.org'] = {}
    # 类似于通过list的下标来赋值元素
    config['bitbucket.org']['user'] = 'hg'

    # 创建第四个section
    config['topsecret.server.com'] = {}
    # 通过中间变量来替代
    topsecret = config['topsecret.server.com']
    topsecret['host port'] = '50022'
    topsecret['forwardx11'] = 'no'

    config['login'] = {
        'username': '8888',
        'password': '123456',
    }

    # Writing our configuration file to 'example.cfg'
    with open('example.cfg', 'w') as configfile:
        config.write(configfile)


# 删除
def test_2():
    config = configparser.ConfigParser()
    file = 'example.cfg'

    # 一定要先read()到内存，不然删除报错
    config.read(file)

    # remove_option()方法删除某个section下的键
    config.remove_option('login', 'username')
    config.remove_option('login', 'password')

    # remove_section()方法删除某个section
    config.remove_section('login')

    # 注意：如果要删除default的全局默认option，必须指定section为default后才能删除
    config.remove_option('DEFAULT', 'compression')

    with open(file, 'w') as configfile:
        config.write(configfile)


# 修改配置文件
def test_3():
    config = configparser.ConfigParser()
    file = 'example.cfg'

    # 如果不加上read()方法，写入是直接从头开始写的，
    # 使用了read()之后，是从读取完后的光标开始写入，类似追加模式'a'一样。
    # 使用read()和'w'写入模式，来实现追加新增，修改配置文件
    config.read(file)

    if not config.has_section('login'):
        # 用来新增section, section可以理解为一个模块
        config.add_section('login')

    # set():用来新增对应section下的某个键值对
    config.set('login', 'username', '1111')
    config.set('login', 'password', '2222')

    # 涉及增删改的操作 都需要使用write()方法之后才会生效
    with open(file, 'w') as configfile:
        config.write(configfile)


# 读配置文件
def test_4():
    config = configparser.ConfigParser()
    file = 'example.cfg'
    config.read(file)

    # getfloat() raises an exception if the value is not a float
    # getint() and getboolean() also do this for their respective types
    a_float = config.getfloat('Section1', 'a_float')
    an_int = config.getint('Section1', 'an_int')
    print(a_float + an_int)

    # Notice that the next output does not interpolate '%(bar)s' or '%(baz)s'.
    # This is because we are using a RawConfigParser().
    if config.getboolean('Section1', 'a_bool'):
        print(config.get('Section1', 'foo'))

    # sections()方法返回可用的section，默认DEFAULT是不会返回的
    list_section_name = config.sections()
    print(list_section_name)

    # 返回default section的键值对
    dict_default = config.defaults()
    print(dict_default)

    # has_section()方法判断section是否存在，存在返回True，不存在返回False
    print(config.has_section('login'))
    print(config.has_section('test'))

    # options()返回对应section下可用的键
    list_key = config.options('login')
    print(list_key)

    list_key_value = config.items('login')
    print(list_key_value)

    print(config['login']['username'])

    # has_option()方法判断section下，某个键是否存在，存在返回True，不存在返回False
    print(config.has_option('login', 'username'))
    print(config.has_option('login', 'pass'))


# 遍历section内的key值
def test_5():
    config = configparser.ConfigParser()
    file = 'example.cfg'
    config.read(file)

    for key in config['bitbucket.org']:
        print(key)

    for key in config.options('bitbucket.org'):
        print(key)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
