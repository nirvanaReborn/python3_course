#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
https://juejin.cn/post/6943619078221365261
常用信号量
signal.SIGHUP   # 连接挂断，这个信号的默认操作为终止进程，因此会向终端输出内容的那些进程就会终止。不过有的进程可以捕捉这个信号并忽略它。比如wget。
signal.SIGINT   # 连接中断，程序终止(interrupt)信号，按下CTRL + C的时候触发。
signal.SIGTSTP # 暂停进程，停止进程的运行，按下CTRL + Z的时候触发， 该信号可以被处理和忽略。
signal.SIGCONT # 继续执行，让一个停止(stopped)的进程继续执行。本信号不能被阻塞。
signal.SIGKILL # 终止进程，用来立即结束程序的运行，本信号无法被阻塞、处理和忽略。
signal.SIGALRM # 超时警告，时钟定时信号，计算的是实际的时间或时钟时间

# https://www.jianshu.com/p/52cb0b2b4572
# windows10 安装superset 0.37.0 报错module 'signal' has no attribute 'SIGALRM'

https://cloud.tencent.com/developer/article/2066139
# 超时异常
程序由于种种原因运行了异常多的时间，甚至死循环
处理此类问题的思路有新建线程和使用 signal 两种思路
signal 对 Windows 支持很有限，在Linux下运行良好
常用的工具包有：timeout-decorator(基于signal)，func_timeout，stopit
解决问题的框架都是为需要计时的函数添加装饰器，在装饰器中使用线程或信号量技术控制运行时间
'''

import signal
import time
import os


def timeoutFn(func, args=(), kwargs={}, timeout_duration=1, default=None):
    class TimeoutError(Exception):
        pass

    def handler(signum, frame):
        raise TimeoutError()

    # set the timeout handler
    signal.signal(signal.SIGALRM, handler)
    signal.alarm(timeout_duration)
    try:
        result = func(*args, **kwargs)
    except TimeoutError as exc:
        result = default
    finally:
        signal.alarm(0)
        signal.signal(signal.SIGALRM, signal.SIG_DFL)

    return result


def test_demo():
    import time
    time.sleep(10)

# ----------------------------------------------------------------------
# 自定义超时异常
class TimeoutError(Exception):
    def __init__(self, msg):
        super(TimeoutError, self).__init__()
        self.msg = msg


def time_out(interval, callback):
    def decorator(func):
        def handler(signum, frame):
            raise TimeoutError("run func timeout")

        def wrapper(*args, **kwargs):
            try:
                signal.signal(signal.SIGALRM, handler)
                signal.alarm(interval)       # interval秒后向进程发送SIGALRM信号
                result = func(*args, **kwargs)
                signal.alarm(0)              # 函数在规定时间执行完后关闭alarm闹钟
                return result
            except TimeoutError as e:
                callback(e)
        return wrapper
    return decorator


def timeout_callback(e):
    print(e.msg)


@time_out(2, timeout_callback)
def task1():
    print("task1 start")
    time.sleep(3)
    print("task1 end")

def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    if os.name == "posix":
        main()
    else:
        print("当前系统不支持")
