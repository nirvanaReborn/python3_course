#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://github.com/kyan001/ping3/blob/master/README.md
# pip install ping3
import ping3
# ping3.DEBUG = True  # Default is False.


def test_1():
    print(ping3.ping("example.com"))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
