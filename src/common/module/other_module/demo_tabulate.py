#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys

# https://pypi.python.org/pypi/tabulate
# http://blog.csdn.net/wangeen/article/details/16824459
# 打印漂亮的表格，比prettytable更方便
import tabulate


table = [
    ["Sun", 696000, 1989100000],
    ["Earth", 6371, 5973.6],
    ["Moon", 1737, 73.5],
    ["Mars", 3390, 641.85]
]


#
def test_1():
    print(tabulate.tabulate(table))


# 名为headers的第二个可选参数定义了要使用的列标题列表：
def test_2():
    print(tabulate.tabulate(table, headers=["Planet", "R (km)", "mass (x 10^29 kg)"]))


# 如果headers ="firstrow"，那么使用第一行数据：
def test_3():
    list_2D = [["Name", "Age"], ["Alice", 24], ["Bob", 19]]
    print(tabulate.tabulate(list_2D, headers="firstrow"))


# 如果headers ="keys"，则使用字典/数据框的键或列索引：
def test_4():
    dict_table = {"Name": ["Alice", "Bob"], "Age": [24, 19]}
    print(tabulate.tabulate(dict_table, headers="keys"))


# 名为tablefmt的第三个可选参数define将表格格式化。
'''支持的表格格式是：
"plain"
"simple"
"grid"
"pipe"
"orgtbl"
"rst"
"mediawiki"
'''


# plain 表格不使用任何伪图形来画线：
def test_5():
    table = [["spam", 42], ["eggs", 451], ["bacon", 0]]
    headers = ["item", "qty"]
    print(tabulate.tabulate(table, headers, tablefmt="plain"))


# simple 是默认格式
def test_6():
    table = [["spam", 42], ["eggs", 451], ["bacon", 0]]
    headers = ["item", "qty"]
    print(tabulate.tabulate(table, headers, tablefmt="simple"))


def test_7():
    table = [["spam", 42], ["eggs", 451], ["bacon", 0]]
    headers = ["item", "qty"]
    print(tabulate.tabulate(table, headers, tablefmt="grid"))


def test_8():
    table = [["spam", 42], ["eggs", 451], ["bacon", 0]]
    headers = ["item", "qty"]
    print(tabulate.tabulate(table, headers, tablefmt="pipe"))


def test_9():
    table = [["spam", 42], ["eggs", 451], ["bacon", 0]]
    headers = ["item", "qty"]
    print(tabulate.tabulate(table, headers, tablefmt="orgtbl"))


def test_10():
    table = [["spam", 42], ["eggs", 451], ["bacon", 0]]
    headers = ["item", "qty"]
    print(tabulate.tabulate(table, headers, tablefmt="rst"))


def test_11():
    table = [["spam", 42], ["eggs", 451], ["bacon", 0]]
    headers = ["item", "qty"]
    print(tabulate.tabulate(table, headers, tablefmt="mediawiki"))


# 列对齐
def test_12():
    print(tabulate.tabulate([[1.2345], [123.45], [12.345], [12345], [1234.5]]))
    print(tabulate.tabulate([[1.2345], [123.45], [12.345], [12345], [1234.5]], numalign="right"))


# 列表允许定义应用于所有十进制数字列的自定义数字格式。使用floatfmt命名参数：
def test_13():
    print(tabulate.tabulate([["pi", 3.141593], ["e", 2.718282]], floatfmt=".4f"))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
