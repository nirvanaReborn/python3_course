#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/wudenggang/article/details/109135792
# Traitlets 允许Python自定义类拥有类型检查、动态计算默认值和Change回调这三种特性。
# https://pypi.org/project/traitlets/
import getpass
from traitlets import HasTraits, Unicode, default


class Identity(HasTraits):
    username = Unicode()

    @default('username')
    def _username_default(self):
        return getpass.getuser()


def test_1():
    pass


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
for i in range(0, 100):
    dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
