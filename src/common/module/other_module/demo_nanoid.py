#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://zhuanlan.zhihu.com/p/387924041
# 为什么 NanoID 会取代 UUID
# https://github.com/puyuan/py-nanoid
# 一个小巧的、安全的、URL 友好的、独特的 Python 字符串 ID 生成器。
# 不要忘记在 [ID碰撞概率计算器](https://zelark.github.io/nano-id-cc/) 中检查您的 ID 长度的安全性。
# pip install nanoid
import nanoid


def test_1():
    # 主模块使用 URL 友好符号 (A-Za-z0-9_-) 并返回一个包含 21 个字符的 ID（具有类似于 UUID v4 的碰撞概率）
    print(nanoid.generate())
    # 如果您想减少 ID 长度（并增加冲突概率），您可以将长度作为参数传递。
    print(nanoid.generate(size=10))


def test_2():
    # 自定义字母或长度
    print(nanoid.generate(alphabet='1234567890abcdef', size=10))
    print(nanoid.non_secure_generate(alphabet='1234567890abcdef', size=10))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
