#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/budong282712018/article/details/80281912
# https://github.com/nvdv/vprof
# vprof 是一个 Python 分析器，为各种 Python 程序特点提供了丰富的交互式可视化，比如运行时间和内存使用。
# 这是一个图形化工具，基于 Node.JS，可在网页上展示结果。


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
