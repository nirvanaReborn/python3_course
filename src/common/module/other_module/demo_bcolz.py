#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/chen50130/article/details/90210648
# bcolz 是一个高压缩率，读写效率高的python库
# https://gitee.com/mirrors/bcolz
# 官网: http://bcolz.blosc.org/en/latest/
# https://blog.csdn.net/weixin_42902669/article/details/103068794
# https://blog.csdn.net/chen50130/article/details/90210648
# bcolz 分 ctable, carray 两种存储类型
import bcolz


def test_1():
    pass


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
