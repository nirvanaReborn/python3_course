#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/mouday/article/details/84062932
# pkgutil是Python自带的用于包管理相关操作的库，pkgutil能根据包名找到包里面的数据文件，然后读取为bytes型的数据。
# 如果数据文件内容是字符串，那么直接decode()以后就是正文内容了。
# 使用pkgutil还有一个好处，就是只要知道包名就可以找到对应包下面的数据文件，数据文件并不一定要在当前包里面。
import pkgutil


def test_1():
    import colorama
    # data_bytes = pkgutil.get_data(colorama, '__init__.py')
    # data_str = data_bytes.decode()
    # print(data_str)


def test_2():
    import other_module
    for filefiner, name, ispkg in pkgutil.iter_modules(path=other_module.__path__, prefix=other_module.__name__+'.'):
        print("{0} name: {1:12}, is_sub_package: {2}".format(filefiner, name, ispkg))



def test_3():
    '''函数 iter_modules() 和 walk_packages() 的区别在于：后者会迭代所有深度的子包'''
    import other_module
    for filefiner, name, ispkg in pkgutil.walk_packages(path=other_module.__path__, prefix=other_module.__name__+'.'):
        print("{0} name: {1:12}, is_sub_package: {2}".format(filefiner, name, ispkg))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
