#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6912658977318814211/
# https://github.com/tiangolo/typer
# Typer跟FastAPI的原理相同，都是Python上用来构建API服务的一个高性能框架。
# 它是FastAPI的升级版，不仅能够准确地记录代码，还能够轻松地进行CLI验证。
# Typer易于学习和使用，不需要用户阅读复杂的教程文档即可上手。支持编辑器（如VSCode）代码自动补全，提高开发人员的开发效率，减少bug的数量。
# 其次，Typer还能配合命令行神器Click使用，就可以利用Click的优点和插件，实现更复杂的功能。
# pip install typer
import typer


app = typer.Typer()


@app.command()
def hello(name: str):
    typer.echo(f"Hello {name}")


@app.command()
def goodbye(name: str, formal: bool = False):
    if formal:
        typer.echo(f"Goodbye Ms. {name}. Have a good day.")
    else:
        typer.echo(f"Bye {name}!")


if __name__ == "__main__":
    app()
