#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/i6561576442730119693/
# Python第三方库juba中文文本处理详细使用方法
# https://pypi.org/project/juba/
'''
juba是由AiMath编写的一个python类库，用于处理中文文本，实现文档词汇矩阵、词汇文档矩阵、文档相似、词汇关联分析、自动文档生成等基础功能，
本库与Python第三方库jieba（结巴）的定位和功能不同，jieba主要用于中文分词、词性标注，是中文文本处理的最基础部分。
juba是在中文文本数据预处理后（中文分词、去掉停用词等）对中文文本进行词向量化，是情感分析、文本聚类、文本分类、信息检索的基础。
因此读者应该将jieba和juba结合起来使用，即先用jieba进行文本预处理，然后利用juba进行文本词向量化，是高级应用的基础。
Juba含有两个类 Similar和Markov。

Word2Vec是从大量文本语料中以无监督的方式学习语义知识的一种模型，它被大量地用在自然语言处理（NLP）中。
Word2Vec模型中，主要有Skip-Gram和CBOW两种模型，从直观上理解：
Skip-Gram是给定input word来预测上下文；
而CBOW是给定上下文，来预测input word。
'''
import sys

import jieba
# pip install juba
import juba


# Similar类的使用
def test_1():
    docs = [
        ['我', '喜欢', '你'],
        ['我', '讨厌', '你'],
        ['狗', '喜欢', '吃', '骨头']
    ]

    # docs是二维矩阵，是Similar类接收的数据结构
    similar = juba.Similar(docs)

    # 词汇表是docs出现过的词汇形成的列表
    print(similar.vocabularyList)

    # 词汇表字典是docs共有词汇表字典，存储每个词出现总次数
    print(similar.vocabulary)

    # 文档词汇矩阵（document term matrix，dtm）
    # 有了文档词汇矩阵，就可以对文档进行高级文本分析，如文档相似、文档分类、情感分析、文档聚类等，与通常的机器学习没有什么区别。
    '''
    # tf_dtm()：词频模式
    # prob_dtm()：概率模式
    # tfidf_dtm()：词频逆文档频率模式
    '''
    print(similar.tf_dtm())
    print(similar.prob_dtm())
    print(similar.tfidf_dtm())

    # 词汇文档矩阵（term document matrix，tdm）
    # 词汇文档矩阵可以看成文档词汇矩阵的转置，不过是以字典结构给出，即按照文档顺序将每个词汇表示为向量。
    '''
    # tf_tdm()：词频模式
    # prob_tdm()：概率模式
    # tfidf_tdm()：词频逆文档频率模式
    '''
    print(similar.tf_tdm())
    print(similar.prob_tdm())
    print(similar.tfidf_tdm())

    # 文档相似,juba提供四种文档相似度计算方法
    '''
    1.cosine_sim(self, dtm='tfidf_dtm')：余弦相似度，值越大，越相似，默认参数dtm='tfidf_dtm'，即默认使用tfidf_dtm矩阵计算相似度，还有其他选项dtm=' prob_dtm'，' tf_dtm'。
    从情感分析的角度来看，"我喜欢你"和"我讨厌你"属于相反文档，因此dtm='tfidf_dtm'模式比较真实地反映这点。
    
    2.weight_jaccard_sim()：权重jaccard相似度
    
    3.jaccard_sim()：jaccard相似度
    
    4.bm25_sim()：bm25相似度，bm25算法请参考（https://en.wikipedia.org/wiki/Okapi_BM25）。
    '''
    print(similar.cosine_sim())
    print(similar.weight_jaccard_sim())
    print(similar.jaccard_sim())
    print(similar.bm25_sim())

    # 词汇关联分析,有两种模式：
    '''
    1.two_term_assocs(word_one,word_two,tdm='tf_tdm',norm='False'):计算两个词汇的相关系数，norm参数只对tdm='tfidf_tdm'起作用。

    2.find_assocs(word,mu=0,tdm='tf_tdm',norm='False')：找出word的相关系数的绝对值不少mu的所有词汇，norm参数只对tdm='tfidf_tdm'起作用。
    '''
    print(similar.two_term_assocs('喜欢', '讨厌', tdm='tfidf_tdm', norm='True'))
    print(similar.two_term_assocs('喜欢', '讨厌', tdm='tfidf_tdm', norm='False'))
    # 从上述结果可以看出，"讨厌"和"喜欢"的相关性较大，但它们是负相关。

    print(similar.find_assocs('我', mu=0, tdm='tf_tdm', norm='False'))


# Markov类的使用
'''
Markov类用于自动文档生成，当然需要输入原始文章，甚至可以输入不用风格文章，就可以生成混合风格文章。
自动文档生成的原理是基于马尔科夫（Markov）随机模型，作者可以搜索马尔科夫模型相关知识。
使用方法很简单:
准备原始文章，我这里使用了一首诗（在春天，http://www.zgshige.com/c/2018-04-23/6012125.shtml），
需要注意的是尽量保持诗歌的结构，段落与段落之间要保持空格。
'''


def test_2():
    text = '月光朦胧下 夜并未脱光衣服 梦却倒在卧榻上赤裸裸的等待着 清亮的春姑娘 风在窗外吼着 我想既然已是春天 你又何必学着隆冬的狼嚎 ' ' 虫声新透 窗纱并没有绿 况且窗外还有那么多不洁之物在 萌发 我的花儿还在藏着掖着 人言可畏呀 你这大地上失贞的玫瑰 被那急于表现自己的迎春花所代替 ' ' 早春陷在残雪的泥淖里 蓝天却洗净了足迹 我要带着一条鱼去旅游 要经过蓝天的驿站 当然还要带着 美酒 以及不曾消弭的冰冷而又飘忽的 白云 ' ' 我可敬的绿植翻卷着波涛般的梦幻 如细沙堆起的海市蜃楼 如深陷泥潭的孤岛 如一朵蘑菇在花园里悄悄盛开'
    list_doc = list(jieba.cut(text))  # 使用jieba进行分词。
    markov = juba.Markov(list_doc)
    # random_text(textlength,firstWord) ，其中textlength衡量生成文档的词汇数量，firstWord是生成文档的第一个词汇。
    print(markov.random_text(200, '我'))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
