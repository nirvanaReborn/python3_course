#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/hackpig/p/8179773.html
# 将汉语转换为汉语拼音

import xpinyin


def main():
    p = xpinyin.Pinyin()
    print(p.get_initial())  # 'N'

    print(p.get_pinyin(u"上海"))  # 'shang-hai'
    print(p.get_pinyin(u"上海", ''))  # 'shanghai'
    print(p.get_pinyin(u"上海", ' '))  # 'shang hai'
    print(p.get_pinyin(u"上海", tone_marks='marks'))  # 'shàng-hǎi'
    print(p.get_pinyin(u"上海", tone_marks='numbers'))  # shang4-hai3

    print(p.get_initial(u"上"))  # 'S'
    print(p.get_initials(u"上海"))  # 'S-H'
    print(p.get_initials(u"上海", u''))  # 'SH'
    print(p.get_initials(u"上海", u' '))  # 'S H'


if __name__ == "__main__":
    main()
