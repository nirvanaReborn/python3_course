#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.liaoxuefeng.com/wiki/1016959663602400/1017985577429536
# asyncio可以实现单线程并发IO操作。如果仅用在客户端，发挥的威力不大。
# 如果把asyncio用在服务器端，例如Web服务器，由于HTTP连接就是IO操作，因此可以用单线程+coroutine实现多用户的高并发支持。
# asyncio实现了TCP、UDP、SSL等协议，
# aiohttp则是基于asyncio实现的HTTP框架。
# pip install aiohttp

import asyncio
import sys

from aiohttp import web


# 注意aiohttp的初始化函数init()也是一个coroutine，loop.create_server()则利用asyncio创建TCP服务。
def test_1():
    async def index(request):
        await asyncio.sleep(0.5)
        return web.Response(body=b'<h1>Index</h1>')

    async def hello(request):
        await asyncio.sleep(0.5)
        text = '<h1>hello, %s!</h1>' % request.match_info['name']
        return web.Response(body=text.encode('utf-8'))

    async def init(loop):
        app = web.Application(loop=loop)
        app.router.add_route('GET', '/', index)
        app.router.add_route('GET', '/hello/{name}', hello)

        # DeprecationWarning: Application.make_handler(...) is deprecated, use AppRunner API instead
        runner = web.AppRunner(app)
        await runner.setup()
        site = web.TCPSite(runner, '127.0.0.1', 8000)
        print('server started at http://127.0.0.1:8000...')
        await site.start()

        # 以前的写法
        # srv = await loop.create_server(app.make_handler(), '127.0.0.1', 8000)
        # print('Server started at http://127.0.0.1:8000...')
        # return srv

    loop = asyncio.get_event_loop()
    loop.run_until_complete(init(loop))
    loop.run_forever()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
