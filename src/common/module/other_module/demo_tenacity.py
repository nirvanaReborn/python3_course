#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://tenacity.readthedocs.io/en/latest/
# https://zhuanlan.zhihu.com/p/391812968
# https://blog.csdn.net/djstavaV/article/details/112261899
# https://blog.csdn.net/u010339879/article/details/126323247

from tenacity import retry, wait_random_exponential, stop_after_attempt


def main():
    pass


if __name__ == '__main__':
    main()
