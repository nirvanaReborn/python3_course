#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/kevinelstri/article/details/61921812
#

import os
import sys

import numpy
import pandas

from public_function import GLOBAL_WORK_DIR


def test_help():
    help(pandas)


def test_dir():
    for i in dir(pandas):
        print(i)


# 读取xlsx文件
def test_1():
    source_file = os.path.join(GLOBAL_WORK_DIR, r"获取登录信息.xls")
    dataframe = pandas.read_excel(source_file, sheet_name='登录信息')
    print(dataframe)
    # print([[c, *dataframe[c].tolist()] for c in dataframe.columns])
    list_sheet = list(numpy.array(dataframe).tolist())  # 已经跳过标题行
    list_sheet.insert(0, dataframe.columns.values.tolist())
    print(list_sheet)


# 读取csv文件
def test_2():
    pf = pandas.read_csv('train.csv')
    print(pf)


# 读取JSON文件
def test_3():
    pf = pandas.read_json('train.json')
    print(pf)


# 读取HDF5文件
def test_4():
    pf = pandas.read_hdf('train.h5')
    print(pf)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
