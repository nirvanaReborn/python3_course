#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/wkfvawl/p/9487165.html
# https://www.cnblogs.com/xiaoyh/p/9919590.html
'''
(1)、jieba库概述
         jieba是优秀的中文分词第三方库
         - 中文文本需要通过分词获得单个的词语
         - jieba是优秀的中文分词第三方库，需要额外安装
         - jieba库提供三种分词模式，最简单只需掌握一个函数

(2)、jieba分词的原理
         Jieba分词依靠中文词库
         - 利用一个中文词库，确定汉字之间的关联概率
         - 汉字间概率大的组成词组，形成分词结果
         - 除了分词，用户还可以添加自定义的词组

2、jieba库使用说明
  (1)、jieba分词的三种模式:
        精确模式、全模式、搜索引擎模式
         - 精确模式：把文本精确的切分开，不存在冗余单词
         - 全模式：把文本中所有可能的词语都扫描出来，有冗余
         - 搜索引擎模式：在精确模式基础上，对长词再次切分
'''

# jieba.cut（s）                被运用于精确模式，将会返回一个可迭代的数据类型
# jieba.cut（s，cut_all=True）　被运用于全模式，输出文本s中的所有可能单词
# jieba.cut_for_search(s)       搜索引擎模式，适合搜索引擎建立索引的分词结果
# jieba.lcut（s）               被运用于精确模式，将会返回一个列表类型
# jieba.lcut（s，cut_all=True） 被运用于全模式，返回一个列表类型
# jieba.lcut_for_search(s)      搜索引擎模式，返回一个列表类型
# jieba.add_word（w）           向分词词典加入新词


import logging

import jieba  # 分词包


logging.getLogger("jieba").setLevel(logging.WARNING)


def test_1():
    # 精准模式：
    print(jieba.lcut("中国是一个伟大的国家"))
    # 全模式：
    print(jieba.lcut("中国是一个伟大的国家", cut_all=True))
    # 搜索引擎模式：
    print(jieba.lcut_for_search("中华人民共和国是伟大的"))


# 向分词词典增加新词：
def test_2():
    jieba.add_word("蟒蛇语言")
    jieba.lcut("python是蟒蛇语言")


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
