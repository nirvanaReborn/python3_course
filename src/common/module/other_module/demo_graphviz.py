#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://pypi.org/project/graphviz/
# https://www.cnblogs.com/shuodehaoa/p/8667045.html
# https://blog.csdn.net/qq_35603331/article/details/81591949
# graphviz实际是一种绘图工具，可以根据dot脚本来画出树形图，我们可以利用python代码生成dot脚本，然后调用graphviz来解析脚本，生成一张图片
# dot脚本语言：是开源工具包Graphviz上用来画图的一门脚本语言，语法非常简单，官方文档也只有8页。
# 安装graphviz软件和安装graphviz的python模块, 需要重启电脑
# 进入cmd窗口，输入命令dot -version 查看graphviz版本
# dot input.dot -T png -o output.txt
# 有向图：diagraph申明，结点关系为"->"，可以//注释
# 无向图：graph 申明， 结点关系"--"
# 子图  ：subgraph声明， 父图是无向图他本身也得是无向图，父图是有向图本身也得是有向图

# dot     渲染的图具有明确方向性。
# neato   渲染的图缺乏方向性。
# twopi   渲染的图采用放射性布局。
# circo   渲染的图采用环型布局。
# fdp     渲染的图缺乏方向性。
# sfdp    渲染大型的图，图片缺乏方向性。

# digraph G{
#   { a b c} -> { d e f }
# }

# pip install graphviz
from graphviz import Digraph


def test_1():
    g = Digraph('pic')
    g.node(name='a', color='red')
    g.node(name='b', color='blue')
    g.edge('a', 'b', color='green')
    g.view()


def test_2():
    dot = Digraph(comment='The Round Table')

    # 添加圆点 A, A的标签是 King Arthur
    dot.node('A', 'king')
    # dot.view()  # 后面这句就注释了，也可以使用这个命令查看效果

    # 添加圆点 B, B的标签是 Sir Bedevere the Wise
    dot.node('B', 'Sir Bedevere the Wise')
    # dot.view()

    # 添加圆点 L, L的标签是 Sir Lancelot the Brave
    dot.node('L', 'Sir Lancelot the Brave')
    # dot.view()

    # 创建一堆边，即连接AB的边，连接AL的边。
    dot.edges(['AB', 'AL'])
    # dot.view()

    # 在创建两圆点之间创建一条边
    dot.edge('B', 'L', constraint='false')
    # dot.view()

    # 获取DOT source源码的字符串形式
    print(dot.source)

    # 保存source到文件，并提供Graphviz引擎
    dot.render('test-output/round-table.gv', view=True)


# https://blog.csdn.net/qq_42459987/article/details/115691352
def test_3():
    dot = Digraph(comment='The Test Table')  # 给dot脚本语言加注释

    # ----------创建结点语句----
    # 添加圆点A,A的标签是Dot A
    dot.node('A', 'Dot A')
    # 添加圆点 B, B的标签是Dot B
    dot.node('B', 'Dot B')
    # 添加圆点 C, C的标签是Dot C
    dot.node(name='C', label='Dot C', color='red')

    # ----------创建边的语句-----（两种方式）
    # 创建一堆边，即连接AB的两条边，连接AC的一条边。
    dot.edges(['AB', 'AC'])
    # 在创建两圆点之间创建一条边
    dot.edge('B', 'C', 'test')

    # --------观看原dot语言的表达形式-----
    # 获取DOT source源码的字符串形式
    print(dot.source)

    # --------显示图像的-----------
    # dot.view()  直接显示，使用Digraph.gv默认名
    dot.render('test-table.gv', view=True)  # 设置图片名，以及直接显示


def test_4():
    from graphviz import Digraph

    sub_g0 = Digraph(comment="process1", graph_attr={"style": 'filled', "color": 'lightgrey'}, node_attr={"style": "filled", "color": "red"})
    sub_g0.node("a0", "a0")
    sub_g0.node("a1", "a1")
    sub_g0.node("a2", "a2")
    sub_g0.node("a3", "a3")
    sub_g0.edge("a0", "a1")
    sub_g0.edge("a1", "a2")
    sub_g0.edge("a2", "a3")
    sub_g0.edge("a3", "a0")

    sub_g1 = Digraph(comment="process1", graph_attr={"style": 'filled'})
    sub_g1.node("B", "b0")
    sub_g1.node("C", "b1")
    sub_g1.node("D", "b2")
    sub_g1.node("E", "b3")
    sub_g1.edges(["BC", "CD", "DE"])

    grap_g = Digraph("G", format="pdf")

    grap_g.node(
        "start", label="start", shape="Mdiamond")
    grap_g.node(
        "end", label="end", shape="Mdiamond")

    grap_g.subgraph(sub_g0)  # 将sub_g0和sub_g0  的Digraph加入到grap_g的Digraph中，方便一次显示
    grap_g.subgraph(sub_g0)

    grap_g.edge("start", "a0")
    grap_g.edge("start", "B")

    grap_g.edge("a1", "E")
    grap_g.edge("D", "a3")

    grap_g.edge("a3", "end")
    grap_g.edge("E", "end")

    # --------观看原dot语言的表达形式-----
    # 获取DOT source源码的字符串形式
    print(grap_g.source)

    grap_g.view()
    # grap_g.render('test-table2.gv', view=True)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
