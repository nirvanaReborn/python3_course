#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 使用logbook库进行异步记录日志
import asyncio
import logbook
import sys

# 创建一个异步Logger
async_logger = logbook.Logger("AsyncLogger")

# 设置logbook的日志记录器为异步
logbook.StreamHandler(sys.stdout, level=logbook.INFO).push_application()
logbook.compat.redirect_logging()


# 异步写入日志的函数
async def write_log(msg):
    async_logger.info(msg)


# 异步任务
async def main():
    for i in range(10):
        await write_log(f"Log message {i}")
        await asyncio.sleep(1)


if __name__ == "__main__":
    asyncio.run(main())
