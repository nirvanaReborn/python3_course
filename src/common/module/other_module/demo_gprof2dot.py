#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://github.com/jrfonseca/gprof2dot
# 使用一些图形化工具，比如 gprof2dot 来可视化分析 cProfile 的诊断结果。


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
