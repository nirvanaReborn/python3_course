#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/xiao-apple36/p/8433400.html
# urllib和urllib3包使用
# Urllib3是一个功能强大，条理清晰，用于HTTP客户端的Python库，许多Python的原生系统已经开始使用urllib3。
# Urllib3提供了很多python标准库里所没有的重要特性：
# 1.线程安全
# 2.连接池
# 3.客户端SSL/TLS验证
# 4.文件分部编码上传
# 5.协助处理重复请求和HTTP重定位
# 6.支持压缩编码
# 7.支持HTTP和SOCKS代理
# 注：urllib3中没有直接设置cookies的方法和参数，只能将cookies设置到headers中


import requests
# pip install urllib3
import urllib3

#  忽略警告：InsecureRequestWarning: Unverified HTTPS request is being made. Adding certificate verification is strongly advised.
requests.packages.urllib3.disable_warnings()

# 一个PoolManager实例来生成请求, 由该实例对象处理与线程池的连接以及线程安全的所有细节
# 让所有的request都遵循一个timeout，可以将timeout参数定义在PoolManager中：
http = urllib3.PoolManager(timeout=3.0)

header = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.108 Safari/537.36'
}


def test_1():
    # 通过request()方法创建一个请求：
    # 使用timeout，可以控制请求的运行时间。在一些简单的应用中，可以将timeout参数设置为一个浮点数：
    r = http.request('GET', 'http://cuiqingcai.com/', timeout=3.0)
    print(r.status)  # 200
    # 获得html源码,utf-8解码
    print(r.data.decode())


def test_2():
    # GET请求(添加数据)
    r = http.request('GET',
                     'https://www.baidu.com/s?',
                     fields={'wd': 'hello'},
                     headers=header)
    print(r.status)  # 200
    print(r.data.decode())


def test_3():
    # post请求: 你还可以通过request()方法向请求(request)中添加一些其他信息，如：
    r = http.request('POST',
                     'http://httpbin.org/post',
                     fields={'hello': 'world'},
                     headers=header)
    print(r.data.decode())


def test_4():
    # 对于POST和PUT请求(request),需要手动对传入数据进行编码，然后加在URL之后：
    encode_arg = urllib3.parse.urlencode({'arg': '我的'})
    print(encode_arg.encode())
    r = http.request('POST',
                     'http://httpbin.org/post?' + encode_arg,
                     headers=header)
    # unicode解码
    print(r.data.decode('unicode_escape'))


# 发送json数据
def test_5():
    # JSON:在发起请求时,可以通过定义body 参数并定义headers的Content-Type参数来发送一个已经过编译的JSON数据：
    import json
    data = {'attribute': 'value'}
    encode_data = json.dumps(data).encode()

    r = http.request('POST',
                     'http://httpbin.org/post',
                     body=encode_data,
                     headers={'Content-Type': 'application/json'}
                     )
    print(r.data.decode('unicode_escape'))


# 上传文件
def test_6():
    # 使用multipart/form-data编码方式上传文件,可以使用和传入Form data数据一样的方法进行,并将文件定义为一个元组的形式(file_name,file_data):
    with open('1.txt', 'r+', encoding='UTF-8') as f:
        file_read = f.read()

    r = http.request('POST',
                     'http://httpbin.org/post',
                     fields={'filefield': ('1.txt', file_read, 'text/plain')})
    print(r.data.decode('unicode_escape'))


# 二进制文件:图片
def test_7():
    with open('websocket.jpg', 'rb') as f2:
        binary_read = f2.read()

    r = http.request('POST',
                     'http://httpbin.org/post',
                     body=binary_read,
                     headers={'Content-Type': 'image/jpeg'})
    #
    # print(json.loads(r.data.decode('utf-8'))['data'] )
    print(r.data.decode('utf-8'))


# 对重试和重定向进行控制
def test_8():
    # 通过设置retries参数对重试进行控制。Urllib3默认进行3次请求重试，并进行3次方向改变。
    r = http.request('GET',
                     'http://httpbin.org/ip', retries=5)  # 请求重试的次数为5

    print(r.data.decode('utf-8'))
    ##关闭请求重试(retrying request)及重定向(redirect)只要将retries定义为False即可：
    r = http.request('GET',
                     'http://httpbin.org/redirect/1', retries=False, redirect=False)
    print('d1', r.data.decode('utf-8'))
    # 关闭重定向(redirect)但保持重试(retrying request),将redirect参数定义为False即可
    r = http.request('GET',
                     'http://httpbin.org/redirect/1', redirect=False)


# 设置代理
def test_9():
    proxy = urllib3.ProxyManager('http://50.233.137.33:80', headers={'connection': 'keep-alive'})
    resp = proxy.request('get', 'http://httpbin.org/ip')
    print(resp.status)  # 200
    print(resp.data)  # b'{"origin":"50.233.136.254"}\n'


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
