#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/hongfei/p/3982259.html
# progressbar是一个进度条库，该库提供了一个文本模式的progressbar。

from __future__ import division

import sys
import time

# pip install progressbar
from progressbar import *


def test_1(total=1000):
    pbar = ProgressBar()
    for i in pbar(range(total)):
        time.sleep(0.01)


def test_2(total=1000):
    pbar = ProgressBar().start()
    for i in range(total):
        pbar.update(int((i / (total - 1)) * 100))
        time.sleep(0.01)
    pbar.finish()


def test_3(total=1000):
    '''
    widgets可选参数含义：
        'Progress: ' ：设置进度条前显示的文字
        Percentage() ：显示百分比
        Bar('#') ： 设置进度条形状
        ETA() ： 显示预计剩余时间
        Timer() ：显示已用时间
    '''
    widgets = ['Progress: ',
               Percentage(), ' ',
               Bar('#'), ' ',
               Timer(), ' ',
               ETA(), ' ',
               FileTransferSpeed()]
    pbar = ProgressBar(widgets=widgets, maxval=10 * total).start()
    for i in range(total):
        # do something
        pbar.update(10 * i + 1)
        time.sleep(0.01)
    pbar.finish()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
