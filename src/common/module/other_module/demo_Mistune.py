#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
# https://www.toutiao.com/a6525191832400298509/
Mistune 是一个纯 Python 实现的 Markdown 解析器，功能齐全，包括表格、注释、代码块等。
Mistune 据称是所有纯 Python markdown 解析器中速度最快的（基准测试结果）。它在设计时考虑了模块化，以提供一个清晰易用的可扩展的 API 。
'''

import mistune


def test_1():
    print(mistune.markdown('I am using **mistune markdown parser**'))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
