#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys

# https://www.cnblogs.com/sxhui/p/5939289.html
# pyperclip 模块有copy()和paste()函数，可以向计算机的剪贴板发送文本，或从它接收文本。
# pip install pyperclip
import pyperclip


def test_1():
    if len(sys.argv) > 1:
        # Get address from command line.
        address = ' '.join(sys.argv[1:])
    else:
        # Get address from clipboard.
        address = pyperclip.paste()
    print(address)


def main():
    pyperclip.copy('Hello world!')
    content = pyperclip.paste()
    print(content)


if __name__ == "__main__":
    main()
