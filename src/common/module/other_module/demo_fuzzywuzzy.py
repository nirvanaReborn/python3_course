#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6583575930227130887/
# fuzzywuzzy是一个可以对字符串进行模糊匹配的库


from fuzzywuzzy import fuzz


def main():
    r = fuzz.ratio("Hit me with your best shot", "Hit me with your pet shark")
    print(r)


if __name__ == "__main__":
    main()
