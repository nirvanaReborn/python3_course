#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6779111283670123012/
# 我要和import说再见了！这个Python库能让我偷偷懒
'''
一、如果需要的库不在pyforest中怎么办？
1.首先，pyforest支持大部分流行的数据科学库，比如pandas，numpy，matplotlib，seaborn，sklearn，tensorflow等等，
以及常用的辅助库如os，sys，re，pickle等。
2.其次，如果真的没有也没关系，pyforest支持向其中添加库。
操作方法也很简单，找到pyforest库的user_imports.py文件，然后添加一个语句就好了，比如像下面这样：
#############################
### User-specific imports ###
#############################
# You can save your own imports in ~/.pyforest/user_imports.py
# Please note: imports in ~/.pyforest/user_imports.py take precedence over the
# imports above.


二、所有的库添加进入运行速度会不会变慢？
答案是不会，因为只有你后面真正使用了到了pyforest里的包含的库，程序才会真正import，否则不会。
'''
# pip install pyforest
from pyforest import *


def main():
    # 可以通过以下方式导出所有导入语句：
    active_imports()


if __name__ == "__main__":
    main()
