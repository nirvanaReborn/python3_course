#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/weixin_42716570/article/details/113574908
# https://blog.csdn.net/weixin_43360896/article/details/115563610
# pkg_resources模块提供了运行时工具，用于查找，自省，激活和使用已安装的Python发行版。
# pip install pkg_resources
import pkg_resources
from pathlib import Path
from pprint import pprint


def test_1():
    # 打印所需依赖包
    pprint(pkg_resources.require('pandas'))


def test_2():
    file = '../requirements.txt'
    for x in pkg_resources.parse_requirements(Path(file).open()):
        # 打印包的名称和版本
        print(x.name, x.specifier)


def test_3():
    def check_requirements(file='requirements.txt', exclude=()):
        # Check installed dependencies meet requirements
        requirements = [f'{x.name}{x.specifier}' for x in pkg_resources.parse_requirements(Path(file).open())
                        if x.name not in exclude]
        # 判断已经安装的包是否符合要求
        pprint(pkg_resources.require(requirements))

    check_requirements('../requirements.txt')


def test_4():
    installedPackages = {pkg.key for pkg in pkg_resources.working_set}
    required = {'yfinance', 'pandas', 'matplotlib', 'stockstats', 'stable-baselines', 'gym', 'tensorflow'}
    missing = required - installedPackages
    # if missing:
    #     !pip install yfinance
    #     !pip install pandas
    #     !pip install matplotlib
    #     !pip install stockstats
    #     !pip install gym
    #     !pip install stable-baselines[mpi]
    #     !pip install tensorflow==1.15.4


def main():
    from public_function import dict_choice

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
