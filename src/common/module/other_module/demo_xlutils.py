#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.python-excel.org/
# 此软件包收集需要xlrd和xlwt的实用程序，包括复制和修改或过滤现有Excel文件的功能。
# 注意： 一般来说，这些用例现在被openpyxl所覆盖！
'''
# http://blog.csdn.net/tianzhu123/article/details/7225809
# xlutils（依赖于xlrd和xlwt）提供复制excel文件内容和修改文件的功能。
# 其实际也只是在xlrd.Book和xlwt.Workbook之间建立了一个管道而已

# http://www.bubuko.com/infodetail-820380.html
'''
import os

import xlrd as ExcelRead
from xlutils.copy import copy


# http://www.jb51.net/article/63497.htm
def write_append(file_name):
    values = ["Ann", "woman", 22, "UK"]

    r_xls = ExcelRead.open_workbook(file_name)
    r_sheet = r_xls.sheet_by_index(0)
    rows = r_sheet.nrows
    w_xls = copy(r_xls)
    sheet_write = w_xls.get_sheet(0)

    for i in range(0, len(values)):
        sheet_write.write(rows, i, values[i])

    w_xls.save(file_name + '.out' + os.path.splitext(file_name)[-1])


def main():
    write_append("./test_append.xls")


if __name__ == "__main__":
    main()
