#!/usr/bin/env python
# -*- coding: utf-8 -*-

# http://www.python-excel.org/
# http://blog.csdn.net/hk2291976/article/details/41545881

import datetime
import sys

import xlwt

from public_function import GLOBAL_XLS_FILE


def test_write_excel(dest_file):
    wb = xlwt.Workbook()
    ws = wb.add_sheet('mysheet')
    ws.write(0, 0, 1.01)  # 把表格想象成二维表，前2各参数是行列
    ws.write(0, 1, 'haha')
    ws.write(1, 0, 'A')
    ws.write(1, 1, 'B')
    ws.write(1, 2, 'SUM')
    ws.write(2, 0, 123)  # A3
    ws.write(2, 1, 456)  # B3
    ws.write(2, 2, xlwt.Formula("A3+B3"))
    wb.save(dest_file)


# 在写入文件的时候还可以加入格式
def test_write_excel_style(dest_file):
    style0 = xlwt.easyxf('font: name Times New Roman, color-index red, bold on', num_format_str='#,##0.00')
    style1 = xlwt.easyxf(num_format_str='D-MM-YYYY')

    wb = xlwt.Workbook()
    ws = wb.add_sheet('A Test Sheet')
    ws.write(0, 0, 1234.56, style0)
    ws.write(1, 0, datetime.datetime.now(), style1)
    ws.write(2, 0, 1)
    ws.write(2, 1, 1)
    ws.write(2, 2, xlwt.Formula("A3+B3"))
    wb.save(dest_file)


def main():
    dest_file = GLOBAL_XLS_FILE
    dict_choice = {
        "1": "test_write_excel(dest_file)",
        "2": "test_write_excel_style(dest_file)",
    }

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
