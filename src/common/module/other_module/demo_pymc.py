#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6583575930227130887/
# scikit-learn似乎是所有人的宠儿，但在我看来，PyMC更有魅力。PyMC主要用来做Bayesian分析。
'''
PyMC是一个实现贝叶斯统计模型和马尔科夫链蒙塔卡洛采样工具拟合算法的Python库。
PyMC的灵活性及可扩展性使得它能够适用于解决各种问题。
除了包含核心采样功能，PyMC还包含了统计输出、绘图、拟合优度检验和收敛性诊断等方法。
'''


def test_1():
    from pymc.examples import disaster_model
    from pymc import MCMC

    m = MCMC(disaster_model)
    m.sample(iter=10000, burn=1000, thin=10)
    print(m)


# http://hao.jobbole.com/pymc/
def test_2():
    return


# https://blog.csdn.net/philthinker/article/details/78240657?locationNum=11&fps=1


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
