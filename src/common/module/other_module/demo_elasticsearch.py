#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/xiao987334176/p/10130712.html
# https://blog.csdn.net/qq_43083284/article/details/91383794
# ElasticSearch是一个基于Lucene的搜索服务器。它提供了一个分布式多用户能力的全文搜索引擎，基于RESTful web接口。
# 下面介绍了利用Python API接口进行数据查询，方便其他系统的调用。


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
