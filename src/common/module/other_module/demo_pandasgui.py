#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
这个工具是在Pandas的基础之上加了GUI界面，它所具备的主要功能有：
    查看DataFrame数据集与Series数据集
    交互式地绘制图表
    过滤数据
    统计分析
    数据的修改与复制粘贴
    拖放导入csv文件
    搜索工具栏
'''
# pip install pandasgui
import pandasgui
import pandas as pd


def main():
    print(pandasgui.__version__)
    df = pd.read_excel(
        io=r'../../../../doc/Python3.xlsx',
        engine="openpyxl", sheet_name="Python3标准库",
        skiprows=3, usecols="B:R",
        nrows=1000
    )
    pandasgui.show(df)


if __name__ == "__main__":
    main()
