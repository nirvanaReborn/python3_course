#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/wupeiqi/articles/4963027.html
# paramiko是基于Python(2.7，3.4+)版本实现和封装了SSHv2协议，底层是用cryptography实现，
# paramiko是一个用于做远程控制的模块，使用该模块可以对远程服务器进行命令或文件操作，
# 值得一说的是，fabric和ansible内部的远程管理就是使用的paramiko来现实。

# http://www.cnblogs.com/haigege/p/5513759.html
# 使用paramiko执行远程linux主机命令
# ssh: 安全外壳协议
# paramiko是python的SSH库，可用来连接远程linux主机，然后执行linux命令或者通过SFTP传输文件。
# 类似于SSH协议，Paramiko主要分为SSHClient和SFTPClient，
# 前者主要对远程主机进行操作，输入命令对远程主机进行控制，后者主要实现了从远程主机上上传下载文件

import re
import time

import paramiko


# 定义一个类，表示一台远端linux主机
class Linux(object):
    # 通过IP, 用户名，密码，超时时间初始化一个远程Linux主机
    def __init__(self, host, username, password, timeout=30, port=22):
        self.host = host
        self.username = username
        self.password = password
        self.timeout = timeout
        # transport和chanel
        self.t = ''
        self.chan = ''
        # 链接失败的重试次数
        self.try_times = 3

    # 调用该方法连接远程主机
    def connect(self):
        while True:
            # 连接过程中可能会抛出异常，比如网络不通、链接超时
            try:
                self.t = paramiko.Transport(sock=(self.host, 22))
                self.t.connect(username=self.username, password=self.password)
                self.chan = self.t.open_session()
                self.chan.settimeout(self.timeout)
                self.chan.get_pty()
                self.chan.invoke_shell()
                # 如果没有抛出异常说明连接成功，直接返回
                print(u'连接%s成功' % self.host)
                # 接收到的网络数据解码为str
                print(self.chan.recv(65535).decode('utf-8'))
                return
            # 这里不对可能的异常如socket.error, socket.timeout细化，直接一网打尽
            except Exception as  e1:
                if self.try_times != 0:
                    print(u'连接%s失败，进行重试' % self.host)
                    self.try_times -= 1
                else:
                    print(u'重试3次失败，结束程序')
                    exit(1)

    # 断开连接
    def close(self):
        self.chan.close()
        self.t.close()

    # 发送要执行的命令
    def send(self, cmd):
        cmd += '\r'
        # 通过命令执行提示符来判断命令是否执行完成
        p = re.compile(r'$')

        result = ''
        # 发送要执行的命令
        self.chan.send(cmd)
        # 回显很长的命令可能执行较久，通过循环分批次取回回显
        while True:
            time.sleep(0.5)
            ret = self.chan.recv(65535)
            ret = ret.decode('utf-8')
            result += ret
            if p.search(ret):
                print(result)
                return result


def test_0():
    dict_user = {
        "host": r"ftp://ftp1.linuxidc.com/",
        "port": 22,
        "username": r"ftp1.linuxidc.com",
        "password": r"www.linuxidc.com",
        "timeout": 20,
    }
    # 主机IP错误，无法连接的情况
    # host = Linux('192.168.180.12', 'root', 'xxxx')
    # 链接正常的情况
    host = Linux(**dict_user)
    host.connect()
    host.send('ls -l')
    host.close()


# 执行命令 - 通过用户名和密码连接服务器
def test_1():
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect('192.168.1.108', 22, 'alex', '123')
    stdin, stdout, stderr = ssh.exec_command('df')
    print(stdout.read())

    # https://blog.csdn.net/lyj1101066558/article/details/51377599
    # exec_command 返回 shell 脚本 exit 值
    channel = stdout.channel
    status = channel.recv_exit_status()
    print(status)

    ssh.close()


# 执行命令 - 过密钥链接服务器
def test_2():
    private_key_path = '/home/auto/.ssh/id_rsa'
    key = paramiko.RSAKey.from_private_key_file(private_key_path)

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect('192.168.1.108', 22, 'alex', key)  # 密码替换为key

    stdin, stdout, stderr = ssh.exec_command('df')
    print(stdout.read())
    ssh.close()


# 上传或者下载文件 - 通过用户名和密码
def test_3():
    t = paramiko.Transport(('182.92.219.86', 22))
    t.connect(username='wupeiqi', password='123')
    sftp = paramiko.SFTPClient.from_transport(t)
    sftp.put('/tmp/test.py', '/tmp/test.py')  # 上传
    sftp.get('/tmp/test.py', '/tmp/test2.py')  # 下载
    t.close()


# 上传或者下载文件 - 通过通过密钥
def test_4():
    pravie_key_path = '/home/auto/.ssh/id_rsa'
    key = paramiko.RSAKey.from_private_key_file(pravie_key_path)

    t = paramiko.Transport(('182.92.219.86', 22))
    t.connect(username='wupeiqi', pkey=key)

    sftp = paramiko.SFTPClient.from_transport(t)
    sftp.put('/tmp/test.py', '/tmp/test.py')  # 上传
    sftp.get('/tmp/test.py', '/tmp/test2.py')  # 下载
    t.close()


# https://blog.csdn.net/qq_24674131/article/details/95618304
def test_5():
    import os
    import time
    import datetime
    from cmdline import parse_args
    from util_log import Logging

    class SSHConnection:
        # 初始化连接创建Transport通道
        def __init__(self, host='xxx.xxx.xxx.xxx', port=22, user='xxx', pwd='xxxxx'):
            self.host = host
            self.port = port
            self.user = user
            self.pwd = pwd
            self.__transport = paramiko.Transport((self.host, self.port))
            self.__transport.connect(username=self.user, password=self.pwd)
            self.sftp = paramiko.SFTPClient.from_transport(self.__transport)

        # 关闭通道
        def close(self):
            self.sftp.close()
            self.__transport.close()

        # 上传文件到远程主机
        def upload(self, local_path, remote_path):
            self.sftp.put(local_path, remote_path)

        # 从远程主机下载文件到本地
        def download(self, local_path, remote_path):
            self.sftp.get(remote_path, local_path)

        # 在远程主机上创建目录
        def mkdir(self, target_path, mode='0777'):
            self.sftp.mkdir(target_path, mode)

        # 删除远程主机上的目录
        def rmdir(self, target_path):
            self.sftp.rmdir(target_path)

        # 查看目录下文件以及子目录（如果需要更加细粒度的文件信息建议使用listdir_attr）
        def listdir(self, target_path):
            return self.sftp.listdir(target_path)

        # 删除文件
        def remove(self, target_path):
            self.sftp.remove(target_path)

        # 查看目录下文件以及子目录的详细信息（包含内容和参考os.stat返回一个FSTPAttributes对象，对象的具体属性请用__dict__查看）
        def listdirattr(self, target_path):
            try:
                list = self.sftp.listdir_attr(target_path)
            except BaseException as e:
                print(e)
            return list

        # 获取文件详情
        def stat(self, remote_path):
            return self.sftp.stat(remote_path)

        # SSHClient输入命令远程操作主机
        def cmd(self, command):
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)
            ssh._transport = self.__transport
            stdin, stdout, stderr = ssh.exec_command(command)
            result = stdout.read()
            print(result)
            return result


    ssh = None
    logging = Logging().get_logger()

    # 递归遍历远程目录下的所有文件
    def gci(remote_path, pathlist):
        for f in ssh.listdirattr(remote_path):
            if (str(f.longname).split(" ")[0].startswith('d')):
                gci(os.path.join(remote_path, f.filename), pathlist)
            else:
                pathlist.append(os.path.join(remote_path, f.filename))
        return pathlist


    # 接收参数
    args = parse_args()
    # 初始化ssh
    ssh = SSHConnection(host=args.srchost, user=args.srcuser, pwd=args.srcpasswd)
    while True:
        try:
            # 创建本地接收文件目录（按天创建目录）
            datedir = os.path.join(args.dstpath, datetime.datetime.now().strftime('%Y%m%d'))
            # 创建子目录之前保证父级目录创建否则抛出异常
            os.mkdir(args.dstpath)
            os.mkdir(datedir)
        except BaseException as e:
            pass
        beforedict = dict()
        afterdict = dict()
        pathlist = []
        # 获取所有文件名以及每个文件的当前大小
        for i in gci(args.srcpath, pathlist):
            beforedict.setdefault(i, ssh.stat(i).st_size)
        # 隔interval秒获取每个文件名以及当前大小
        time.sleep(int(args.interval))
        for i in gci(args.srcpath, pathlist):
            afterdict.setdefault(i, ssh.stat(i).st_size)
        # 对比时间前后文件大小如果一致认为文件已经生成完成，将文件下载到本地
        for i in beforedict.keys():
            if beforedict.get(i) == afterdict.get(i):
                try:
                    ssh.download(os.path.join(datedir, os.path.basename(i)), i)
                    logging.info('File ' + i + ' download completed')
                    ssh.remove(i)
                    logging.info('File ' + i + ' deleted')
                except BaseException as e:
                    logging.error('File ' + i + ' download failed')
                    logging.error(e)
                    logging.error('File ' + i + ' delete failed')
    ssh.close()


# ------------------------------------------------------------------------------
def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
