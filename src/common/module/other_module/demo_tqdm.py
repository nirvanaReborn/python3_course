#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# tqdm是一个用来生成进度条的优秀的库
# https://github.com/tqdm/tqdm
# pip install tqdm
import tqdm


def test_1():
    for i in tqdm.tqdm(range(10000)):
        pass


def test_2():
    with tqdm.tqdm(total=100) as pbar:
        for i in range(10):
            pbar.update(10)


def test_3():
    text = ""
    for char in tqdm.tqdm(["a", "b", "c", "d"]):
        text = text + char


def test_4():
    pbar = tqdm.tqdm(["a", "b", "c", "d"])
    for char in pbar:
        pbar.set_description("Processing %s" % char)


def test_5():
    import asyncio

    async def wait_with_progress(coros):
        for f in tqdm.tqdm(asyncio.as_completed(coros), total=len(coros)):
            await f


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
