#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
# psutil:获取系统信息模块，可以获取CPU，内存，磁盘等的使用情况
import psutil
import time
import datetime
from threading import Timer


def MonitorSystem(logfile=None):
    '''logfile：监测信息写入文件'''
    # 获取cpu使用情况
    cpuper = psutil.cpu_percent()
    # 获取内存使用情况：系统内存大小，使用内存，有效内存，内存使用率
    mem = psutil.virtual_memory()
    # 内存使用率
    memper = mem.percent
    # 获取当前时间
    now = datetime.datetime.now()
    ts = now.strftime('%Y-%m-%d %H:%M:%S')
    line = f'{ts} cpu:{cpuper}%, mem:{memper}%'
    print(line)
    if logfile:
        logfile.write(line)

    # 启动定时器任务，每三秒执行一次
    Timer(3, MonitorSystem).start()


def MonitorNetWork(logfile=None):
    # 获取网络收信息
    netinfo = psutil.net_io_counters()
    # 获取当前时间
    now = datetime.datetime.now()
    ts = now.strftime('%Y-%m-%d %H:%M:%S')
    line = f'{ts} bytessent={netinfo.bytes_sent}, bytesrecv={netinfo.bytes_recv}'
    print(line)
    if logfile:
        logfile.write(line)

    # 启动定时器任务，每秒执行一次
    Timer(1, MonitorNetWork).start()


# def test_1():
#     def loopMonitor():
#         while True:
#             MonitorSystem()
#             # 2s检查一次
#             time.sleep(3)
#
#     loopMonitor()


def test_2():
    MonitorSystem()
    MonitorNetWork()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
