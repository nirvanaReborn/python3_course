#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6583575930227130887/
# wget是Python版的网络爬虫库，简单好用。

import os
import tarfile

import wget


def test_1():
    # 网络地址
    DATA_URL = 'http://www.robots.ox.ac.uk/~ankush/data.tar.gz'
    # 本地硬盘文件
    # DATA_URL = '/home/xxx/book/data.tar.gz'

    out_filename = 'abc.tar.gz'

    filename = wget.download(DATA_URL, out=out_filename)
    print(filename)
    # 提取压缩包
    tar = tarfile.open(out_filename)
    tar.extractall()
    tar.close()
    # 删除下载文件
    os.remove(out_filename)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
