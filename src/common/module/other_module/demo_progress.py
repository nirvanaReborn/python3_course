#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.pythonf.cn/read/45067
# https://blog.csdn.net/superxgl/article/details/105186898
# 高级进度条整理
# pip install progress
import time

import PySimpleGUI as sg


def test_1():
    from progress.bar import IncrementalBar

    mylist = [i for i in range(1, 9)]
    bar = IncrementalBar('进度条1', max=len(mylist))
    for item in mylist:
        bar.next()
        time.sleep(0.1)
    bar.finish()


def test_2():
    from progress.bar import Bar

    bar = Bar('Loading', fill='~', suffix='%(percent)d%%')
    for item in range(100):
        bar.next()
        time.sleep(0.1)
    bar.finish()


def test_3():
    from progress.bar import Bar

    bar = Bar('进度条3', max=100)
    for item in range(100):
        bar.next()
        time.sleep(0.1)
    bar.finish()


def test_4():
    from progress.bar import ChargingBar

    bar = ChargingBar('进度条4', max=100)
    for item in range(100):
        bar.next()
        time.sleep(0.1)
    bar.finish()


def test_5():
    from progress.bar import FillingSquaresBar

    bar = FillingSquaresBar('进度条5', max=100)
    for item in range(100):
        bar.next()
        time.sleep(0.1)
    bar.finish()


def test_6():
    from progress.bar import FillingCirclesBar

    bar = FillingCirclesBar('进度条6', max=100)  # max的值100，可调节

    for i in range(100):  # 这个也需要适当调节
        bar.next()
        time.sleep(0.1)  # 延迟时间，可调节，0.1~1之间最佳

    bar.finish()


def test_7():
    from progress.bar import IncrementalBar

    bar = IncrementalBar('进度条7', max=100)  # max的值100，可调节

    for i in range(100):  # 这个也需要适当调节
        bar.next()
        time.sleep(0.1)  # 延迟时间，可调节，0.1~1之间最佳

    bar.finish()


def test_8():
    from progress.bar import PixelBar

    bar = PixelBar('进度条8', max=100)  # max的值100，可调节

    for i in range(100):  # 这个也需要适当调节
        bar.next()
        time.sleep(0.1)  # 延迟时间，可调节，0.1~1之间最佳

    bar.finish()


def test_9():
    from progress.bar import ShadyBar
    import time

    bar = ShadyBar('进度条9', max=100)  # max的值100，可调节

    for i in range(100):  # 这个也需要适当调节
        bar.next()
        time.sleep(0.1)  # 延迟时间，可调节，0.1~1之间最佳

    bar.finish()


def test_10():
    from progress.spinner import Spinner
    # from progress.spinner import MoonSpinner
    # from progress.spinner import PieSpinner
    # from progress.spinner import PixelSpinner
    # from progress.spinner import LineSpinner

    bar = Spinner('进度条10', max=100)  # max的值100，可调节
    # bar = MoonSpinner('进度条10', max=100)
    # bar = PieSpinner('进度条10', max=100)
    # bar = PixelSpinner('进度条10', max=100)
    # bar = LineSpinner('进度条10', max=100)

    for i in range(100):  # 这个也需要适当调节
        bar.next()
        time.sleep(0.1)  # 延迟时间，可调节，0.1~1之间最佳

    bar.finish()


def test_11():
    pass


def test_12():
    mylist = [1, 2, 3, 4, 5, 6, 7, 8]

    for i, item in enumerate(mylist):
        sg.one_line_progress_meter('竖直进度条', i + 1, len(mylist), '-key-')
        time.sleep(1)  # 时间选择1最好，因为8个有点短


# 经典，整合后出现在GUI界面上
def test_13():
    mylist = [1, 2, 3, 4, 5, 6, 7, 8]
    progressbar = [[sg.ProgressBar(len(mylist), orientation='h', size=(51, 10), key='progressbar')]]
    outputwin = [[sg.Output(size=(78, 20))]]
    layout = [[sg.Frame('Progress', layout=progressbar)], [sg.Frame('Output', layout=outputwin)], [sg.Submit('Start'), sg.Cancel()]]

    window = sg.Window('Custom Progress Meter', layout)

    progress_bar = window['progressbar']

    while True:
        event, values = window.read(timeout=10)
        if event == 'Cancel' or event is None:
            break
        elif event == 'Start':
            for i, item in enumerate(mylist):
                print(item)
                time.sleep(1)
                progress_bar.UpdateBar(i + 1)

    window.close()


def ZhuMain():
    fig_dict = {'进度条1': test_1, '进度条2': test_2, '进度条3': test_3, '进度条4': test_4, '进度条5': test_5,
                '进度条6': test_6, '进度条7': test_7, '进度条8': test_8, '进度条9': test_9, '进度条10': test_10,
                '进度条11': test_11, '进度条12': test_12, '进度条13': test_13}

    sg.theme('LightGreen')  # 主题背景设计，默认银河灰


def login():  # 登录界面，第1个窗口
    layout = [
        [sg.Text('账号：'), sg.Input(size=(20, 1), key=('k1'))],
        [sg.Text('密码：'), sg.Input(size=(20, 1), key=('k2'), password_char='*')],
        [sg.Submit(), sg.Cancel()],
    ]

    window = sg.Window('登录界面', layout)
    event, values = window.read()

    while True:
        if event == 'Cancel' or event == None:  # 成功
            window.close()
            break
        elif event == 'Submit':
            if values['k1'] == 'admin' and values['k2'] == '123':
                # 这是指定账号和密码的，如果注册后，再从注册后的txt中调出，来判断？
                sg.Popup('登录成功！')
                window.close()
                ZhuMain()

                break
            else:
                sg.Popup('请输入正确的账号和密码，否则退出。')
                break

    window.close()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    # main()
    login()
