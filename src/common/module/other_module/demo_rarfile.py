#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os

#
# pip3 install rarfile  # 安装rarfile库
import rarfile


def test_1(source_file, dest_dir):
    # 需要WinRAR软件提供的UnRAR.exe文件：将winrar的目录中的UnRAR.exe拷贝到python脚本目录下即可
    rarfile.RarFile(source_file).extract(dest_dir)


def test_2(source_file, dest_dir):
    rar_command = r'"d:\programfiles\winrar\unrar.exe" x %s %s' % (source_file, dest_dir)
    if os.system(rar_command) == 0:
        print('Successful')
    else:
        print('FAILED')


def main():
    source_file = "E:\\New\\New.rar"  # 待解压文件
    dest_dir = "E:\\New"  # 指定文件解压路径
    test_1(source_file, dest_dir)


if __name__ == "__main__":
    main()
