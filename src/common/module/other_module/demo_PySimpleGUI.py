#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# PySimpleGUI基于Tkinter，Remi、wxPython和PyQt。
# Remi将应用程序的界面转换为HTML以在Web浏览器中呈现”。
# 当然，如果您寻找一种使用Python方法而不是C++方法的GUI，则可能需要查看Toga或Kivy。
# https://pysimplegui.readthedocs.io/en/latest/
import PySimpleGUI as sg


def test_1():
    # Create some widgets
    text = sg.Text("What's your name?")
    text_entry = sg.InputText()
    ok_btn = sg.Button('OK')
    cancel_btn = sg.Button('Cancel')
    layout = [[text, text_entry],
              [ok_btn, cancel_btn]]

    # Create the Window
    window = sg.Window('Hello PySimpleGUI', layout)

    # Create the event loop
    while True:
        event, values = window.read()
        if event in (None, 'Cancel'):
            # User closed the Window or hit the Cancel button
            break
        print(f'Event: {event}')
        print(str(values))

    window.close()


def test_2():
    # https://www.jianshu.com/u/69f40328d4f0
    # https://github.com/china-testing/python-api-tesing
    # https://china-testing.github.io/
    # support q group: 630011153 144081101
    import PySimpleGUI as sg

    # Create some widgets
    ok_btn = sg.Button('Open Second Window')
    cancel_btn = sg.Button('Cancel')
    layout = [[ok_btn, cancel_btn]]

    # Create the first Window
    window = sg.Window('Window 1', layout)

    win2_active = False

    # Create the event loop
    while True:
        event1, values1 = window.read(timeout=100)

        if event1 in (None, 'Cancel'):
            # User closed the Window or hit the Cancel button
            break

        if not win2_active and event1 == 'Open Second Window':
            win2_active = True
            layout2 = [[sg.Text('Window 2')],
                       [sg.Button('Exit')]]

            window2 = sg.Window('Window 2', layout2)

        if win2_active:
            events2, values2 = window2.Read(timeout=100)
            if events2 is None or events2 == 'Exit':
                win2_active = False
                window2.close()

    window.close()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
