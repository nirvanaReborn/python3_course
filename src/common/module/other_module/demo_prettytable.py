#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6583575930227130887/
# prettytable库托管在GoogleCode。主要用于在终端或浏览器端构建很好的输出。

# https://www.jianshu.com/p/82689c1e3247
# prettytable学习

import prettytable


def test_1():
    table = prettytable.PrettyTable(["animal", "ferocity"])
    # table = prettytable.PrettyTable(border=False, header=True)
    # table._set_field_names(header.split())
    table.align = "l"  # 水平对齐方式（None，“l”（左对齐），“c”（居中），“r”右对齐）
    table.add_row(["cat", -1])
    table.add_row(["grizzly", 87])
    table.sort_key("ferocity")
    table.reversesort = True
    print(table)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
