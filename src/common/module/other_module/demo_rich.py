#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/i6960835419148026376/
# 官方文档: https://rich.readthedocs.io/en/stable/
# https://github.com/willmcgugan/rich
# Rich 是一个 Python 库，可以为您在终端中提供富文本和精美格式。
# Rich API不仅能够在终端输出提供丰富的彩色文本和精美的格式，还提供了精美的表格、进度条、编辑器、追踪器、语法高亮等。
# 它还可以安装在Python REPL上，所有的数据结构都可以漂亮地输出或标注。
# Rich 适用于 Linux，OSX 和 Windows。
# 真彩色/表情符号可与新的 Windows 终端一起使用，Windows 的经典终端仅限 8 种颜色。
# Rich 还可以与Jupyter 笔记本一起使用，而无需其他配置。
# 但是请注意，Rich必须要Python 3.6.1或以上版本。
# pip install spyder==4.0.0 rich
import rich  # 控制台输出
from rich.panel import Panel
from rich.console import Console
from rich.text import Text
from rich.highlighter import RegexHighlighter, Highlighter
from rich.theme import Theme
from rich.prompt import Prompt, Confirm
from rich.table import Table
from rich.syntax import Syntax
from rich.progress import track
from rich.tree import Tree
from rich.layout import Layout


def test_1():
    '''下面例子中输出的Hello World中Hello被设置为红色斜体，World为默认值。'''
    rich.print("[italic red]Hello[/italic red] World!")
    rich.print("[bold red]alert![/bold red] Something happened")


def test_2():
    '''我们采用了Panel（面板）,面板的轮廓为红色，内容文案为加粗的黄色。'''
    panel = Panel.fit("[bold yellow]Hi, I'm a Panel", border_style="red")
    rich.print(panel)

    panel = Panel(Text("大家好，我是Python之眼。欢迎关注微信公众号：程序员启航！", justify="center"))
    rich.print(panel)

    rich.print(Panel("Hello, [red]World!", title="Welcome"))


def test_3():
    console = Console()
    console.print("Hello", style="magenta")
    console.print("Rich库有点意思啊", style="red on white")


def test_4():
    console = Console()
    text = Text("0123456789")
    text.stylize("bold magenta", 0, 6)
    console.print(text)


def test_5():
    """文本高亮"""
    class EmailHighlighter(RegexHighlighter):
        """Apply style to anything that looks like an email."""

        base_style = "example."
        highlights = [r"(?P<email>[\w-]+@([\w-]+\.)+[\w-]+)"]

    theme = Theme({"example.email": "bold magenta"})
    console = Console(highlighter=EmailHighlighter(), theme=theme)
    console.print("Send funds to money@example.org")


def test_6():
    '''将文本每个字符设置成随机的颜色'''
    from random import randint

    class RainbowHighlighter(Highlighter):
        def highlight(self, text):
            for index in range(len(text)):
                text.stylize(f"color({randint(16, 255)})", index, index + 1)


    rainbow = RainbowHighlighter()
    rich.print(rainbow("大家好，我是Python之眼，是不是每个字的颜色都不一样？"))


def test_7():
    '''Rich有个Prompt类，用于提示我们进行输入（类似input功能），不过它还支持指定值输入及选择判断等。'''
    # 提示输入：
    # name = Prompt.ask("Enter your name")

    # 指定值输入：
    # name = Prompt.ask("Enter your name", choices=["123", "321", "1234567"], default="Python之眼")

    # 选择判断：
    is_rich_great = Confirm.ask("Do you like rich?")
    assert is_rich_great


def test_8():
    '''表情符号: 将名称放在两个冒号之间即可在控制台输出中插入表情符号。'''
    console = Console()
    console.print(":smiley: :pile_of_poo: :thumbs_up: ")


def test_9():
    '''表格'''
    table = Table(title="Star Wars Movies")

    table.add_column("Released", justify="right", style="cyan", no_wrap=True)
    table.add_column("Title", style="magenta")
    table.add_column("Box Office", justify="right", style="green")

    table.add_row("Dec 20, 2019", "Star Wars: The Rise of Skywalker", "$952,110,690")
    table.add_row("May 25, 2018", "Solo: A Star Wars Story", "$393,151,347")
    table.add_row("Dec 15, 2017", "Star Wars Ep. V111: The Last Jedi", "$1,332,539,889")
    table.add_row("Dec 16, 2016", "Rogue One: A Star Wars Story", "$1,332,439,889")

    console = Console()
    console.print(table)


def test_10():
    '''语法高亮: Rich 使用pygments库来实现语法高亮显示'''
    my_code = '''
    def iter_first_last(values: Iterable[T]) -> Iterable[Tuple[bool, bool, T]]:
        """Iterate and generate a tuple with a flag for first and last value."""
        iter_values = iter(values)
        try:
            previous_value = next(iter_values)
        except StopIteration:
            return
        first = True
        for value in iter_values:
            yield first, False, previous_value
            first = False
            previous_value = value
        yield first, True, previous_value
    '''
    syntax = Syntax(my_code, "python", theme="monokai", line_numbers=True)
    console = Console()
    console.print(syntax)


def test_11():
    '''markdown格式输出'''
    MARKDOWN = """
    # 这是一级标题

    Rich 库能比较**完美**的输出markdown.

    1. This is a list item
    2. This is another list item

    ```python
    from rich.console import Console
    from rich.markdown import Markdown

    console = Console()
    md = Markdown(MARKDOWN)
    console.print(md)
    ```
    ![二维码](https://gitee.com/dxawdc/pic/raw/master/image/qrcode_for_gh_ce68560ed124_258.jpg)
    """
    from rich.console import Console
    from rich.markdown import Markdown

    console = Console()
    md = Markdown(MARKDOWN)
    console.print(md)


def test_12():
    '''进度条'''
    import time

    for step in track(range(100)):
        time.sleep(0.1)


def test_13():
    tree = Tree("地球")
    baz_tree = tree.add("亚洲")
    baz_tree.add("[red]中国").add("[green]北京").add("[yellow]海淀区")
    rich.print(tree)


def test_14():
    '''Padding填充'''
    from rich.padding import Padding

    test = Padding("Hello", (2, 4), style="on blue", expand=False)
    rich.print(test)


def test_15():
    '''layout布局'''
    layout = Layout()
    layout.split_column(
        Layout(name="upper", size=10),
        Layout(name="lower", size=10)
    )

    layout["lower"].split_row(
        Layout(name="left"), Layout(name="right"),

    )
    layout["right"].split(
        Layout(Panel("Hello")),
        Layout(Panel("World!"))
    )
    rich.print(layout)


def test_16():
    '''Live动态'''
    import time

    from rich.live import Live
    from rich.table import Table

    table = Table()
    table.add_column("Row ID")
    table.add_column("Description")
    table.add_column("Level")

    with Live(table, refresh_per_second=4):  # update 4 times a second to feel fluid
        for row in range(12):
            time.sleep(0.4)  # arbitrary delay
            # update the renderable internally
            table.add_row(f"{row}", f"description {row}", "[red]ERROR")


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
