#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6507114432093487620/
# PrettyPrinter是Python 3.6 及以上版本中的一个功能强大、支持语法高亮、描述性的美化打印包。
# 它使用了改进的Wadler-Leijen布局算法，和Haskell打印美化库中的prettyprinter
# 以及anti-wl-pprint、
# JavaScript的Prettier、
# Ruby的prettypreinter.rb
# 以及 IPython的Ipython.lib.pretty类似。
# Python的PrettyPrinter集以上众家之所长，并在此基础上继续改进，因此也成为目前Python最强大的美化输出工具。

import datetime

import prettyprinter
from prettyprinter import register_pretty


# 上面介绍了如何使用Pretty_call，接下来定义我们自己的类型。
class MyClass:
    def __init__(self, one, two):
        self.one = one
        self.two = two


# 使用register_pretty修饰符，可以为MyClass类定义美化方式：
@register_pretty(MyClass)
def pretty_myclass(value, ctx):
    return prettyprinter.pretty_call(ctx, MyClass, one=value.one, two=value.two)


def main():
    print(prettyprinter.get_default_config())

    print(datetime.datetime.now())
    prettyprinter.cpprint(datetime.datetime.now())

    # layout = prettyprinter.pretty_call(ctx, sorted, [5, 3, 6, 1], reversed=True)
    # print(layout)
    prettyprinter.cpprint(MyClass((1, 2, 3), {'a': 1, 'b': 2}))
    prettyprinter.cpprint({'beautiful output': datetime.datetime.now(), 'beautiful MyClass instance': MyClass((1, 2, 3), {'a': 1, 'b': 2})})


if __name__ == "__main__":
    main()
