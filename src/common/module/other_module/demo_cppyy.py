#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 可以将c/c++代码封装成python可以直接调用的模块，在这中间，用户不需要任何额外的代码
# https://cppyy.readthedocs.io/en/latest/

import cppyy



def main():
    # variadic template example; the code can also live in a header which is loaded
    # with "cppyy.include()"
    cppyy.cppdef(r"""
        template <typename Arg, typename... Args>
        void log(int level, Arg&& arg, Args&&... args)
        {   
            std::stringstream out;
            out << std::forward<Arg>(arg);
            using expander = int[];
            (void)expander{0, (void(out << ' ' << std::forward<Args>(args)), 0)...};
            std::cerr << out.str() << '\n';
        }
    """)

    level = 1
    cppyy.gbl.log(level, 1, "string", 3.14)


if __name__ == "__main__":
    main()
