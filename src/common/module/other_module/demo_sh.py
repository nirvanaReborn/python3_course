#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6583575930227130887/
# sh库用来将shell命令作为函数导入到Python中。
# sh(以前叫pbs)是Python成熟的subprocess接口,可以把shell命令当做函数来调用。
# sh 1.12.14 is currently only supported on linux and osx.
# please install pbs 0.110 (http://pypi.python.org/pypi/pbs) for windows support.

import sh


# https://www.aliyun.com/jiaocheng/460363.html
def main():
    print(sh.ls("."))


if __name__ == "__main__":
    main()
