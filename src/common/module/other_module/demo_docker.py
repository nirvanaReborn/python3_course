#!/usr/bin/env python3
# -*- coding:utf-8 -*-


import docker


def main():
    client = docker.DockerClient(base_url='unix://var/run/docker.sock')
    container = client.containers.run(image='nginx', name='mynginx', ports={'80/tcp': 8989}, detach=True)
    print(container)
    container.kill()
    container.remove()


if __name__ == "__main__":
    main()
