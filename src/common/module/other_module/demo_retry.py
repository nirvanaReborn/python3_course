#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys

# https://www.toutiao.com/a6518309130229252622/
#
import requests
import retry


def test_1(dict_arg):
    print(dict_arg)

    if dict_arg["tries"] <= 0:
        print('retry many times')
        return None

    try:
        return requests.get(dict_arg["url"], timeout=0.01).text
    except Exception as e:
        print(e.args)
        dict_arg["tries"] = dict_arg["tries"] - 1
        return test_1(dict_arg)


# tries=3 表示重复次数
@retry.retry(exceptions=requests.exceptions.ConnectTimeout, tries=3)
def test_2(dict_arg):
    try:
        return requests.get(dict_arg["url"], timeout=1).text
    except Exception as e:
        pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    dict_arg = {
        "url": "https://www.baidu.com",
        "tries": 3,
    }

    main()
