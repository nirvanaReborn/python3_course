#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://blog.csdn.net/freeking101/article/details/81103945
# Python 远程部署利器 Fabric 模块详解
'''
Fabric 是一个 Python 的库，同时它也是一个命令行工具。
它提供了丰富的同 SSH 交互的接口，可以用来在本地或远程机器上自动化、流水化地执行 Shell 命令。
使用 fabric 提供的命令行工具，可以很方便地执行应用部署和系统管理等操作。
因此它非常适合用来做应用的远程部署及系统维护。其上手也极其简单，你需要的只是懂得基本的 Shell 命令。

fabric 依赖于 paramiko 进行 ssh 交互，fabric 的设计思路是通过几个 API 接口来完成所有的部署，
因此 fabric 对系统管理操作进行了简单的封装，比如执行命令，上传文件，并行操作和异常处理等。

From：http://python.jobbole.com/87241/

From：Python模块学习 - fabric（Python3）：https://www.cnblogs.com/xiao-apple36/p/9124292.html

fabric 官网英文文档：http://www.fabfile.org/

fabric 中文站点：http://fabric-chs.readthedocs.io/zh_CN/chs/

python三大神器之一fabric使用：https://www.cnblogs.com/rufus-hua/p/5144210.html

如何用Fabric实现无密码输入提示的远程自动部署：https://blog.csdn.net/slvher/article/details/50414675

fabric实现远程操作和部署：http://python.jobbole.com/83716/

自动化运维管理 fabric：http://www.ttlsa.com/python/automation-operation-and-maintenance-tool-fabric/

Python3自动化运维之Fabric模版详解：https://www.imooc.com/article/38448
'''
# python3 安装时使用的是fabric3 :（ 安装fabric3之前，需要先卸载fabric。）
# pip uninstall fabric
# pip3 install fabric3
from fabric.api import *
from fabric.contrib.console import confirm
from fabric.utils import abort
from fabric.colors import *

env.hosts = ['192.168.5.128']
env.port = 22
env.user = 'root'
env.password = 'mysql123'


def hostname():
    run('hostname')


def ls(path='.'):
    run('ls {0}'.format(path))


def tail(path='/etc/pas', line=10):
    run('tail -n {0} {1}'.format(line, path))


def hello():
    with settings(hide('everything'), warn_only=True):  # 关闭显示
        result = run('anetstat -lntup|grep -w 25')
        print(result)  # 命令执行的结果
        print(result.return_code)  # 返回码，0表示正确执行，1表示错误
        print(result.failed)


def main():
    pass


if __name__ == "__main__":
    main()
