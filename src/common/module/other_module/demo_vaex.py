#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/i6862586227456279044/
# Vaex
'''
1.1 Vaex：一种具有超级字符串功能的DataFrame Python库。
1.2 一种更快、更安全、更方便的方法，可以使用几乎任意大小的数据进行数据研究分析，适合大数据，比如秒开100G。
1.3 与Pandas（Python生态系统中最受欢迎的DataFrame库）相比，四核笔记本电脑上的字符串操作速度提高了约30-100倍，而32核设备上的字符串操作速度提高了1000倍。
1.4 Vaex使用ApacheArrow数据结构和C++，几乎所有Pandas的字符串操作都受支持，并且内存使用率几乎为零，因为延迟计算是以块的形式完成的。
'''
import vaex


def test_1():
    # 自带数据集，需要安装hdf5   df = vaex.example()
    # 打开本地：不同文件格式的方法很简单
    df1 = vaex.open("somedata.hdf5")
    df2 = vaex.open("somedata.fits")
    df2 = vaex.open("somedata.arrow")
    df4 = vaex.open("somedata.csv")


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
