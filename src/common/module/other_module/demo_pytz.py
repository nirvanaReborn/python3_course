#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# 支持跨平台时区计算，并将 [tz database](https://en.wikipedia.org/wiki/Tz_database) 引入 Python。
import pytz


def test_help():
    help(pytz)


def test_dir():
    for i in dir(pytz):
        print(i)


def test_3():
    t31 = pytz.timezone('Asia/Shanghai')
    print(t31)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
