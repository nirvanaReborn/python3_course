#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://segmentfault.com/a/1190000005182997
# python爬虫神器PyQuery的使用方法
'''
# http://blog.csdn.net/cnmilan/article/details/8727308
# Python中PyQuery库的使用总结
# pyquery库是jQuery的Python实现，可以用于解析HTML网页内容
'''
import pyquery


def main():
    pass


if __name__ == "__main__":
    main()
