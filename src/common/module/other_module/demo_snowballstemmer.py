#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6583575930227130887/
# snowballstemmer是一款非常瘦小的语言转换库，支持15种语言。

import snowballstemmer


def main():
    print(snowballstemmer.EnglishStemmer().stemWord("Gregory"))
    print(snowballstemmer.SpanishStemmer().stemWord("amarillo"))


if __name__ == "__main__":
    main()
