#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://blog.csdn.net/r_coder/article/details/79419318
# 将汉语转换为汉语拼音
# 与xpinyin相比，pypinyin更强大。

import pypinyin


# 不带声调的(style=pypinyin.NORMAL)
def hp(word):
    s = ''
    for i in pypinyin.pinyin(word, style=pypinyin.NORMAL):
        s += ''.join(i) + "-"
    return s


# 带声调的(默认)
def hp2(word):
    s = ''
    # heteronym=True开启多音字
    for i in pypinyin.pinyin(word, heteronym=True):
        s += ''.join(i) + " "
    return s


def main():
    print(hp("中国中央电视台春节联欢晚会"))
    print(hp2("中国中央电视台春节联欢晚会"))


if __name__ == "__main__":
    main()
