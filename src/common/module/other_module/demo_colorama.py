#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6583575930227130887/
# colorama主要用来给文本添加各种颜色，并且非常简单易用。
# https://www.cnblogs.com/xiao-apple36/p/9151883.html

# Fore是针对字体颜色，Back是针对字体背景颜色，Style是针对字体格式
# Fore: BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE, RESET.
# Back: BLACK, RED, GREEN, YELLOW, BLUE, MAGENTA, CYAN, WHITE, RESET.
# Style: DIM, NORMAL, BRIGHT, RESET_ALL

# 记得要及时关闭colorma的作用范围,如果不关闭的话,后面所有的输出都会是你指定的颜色
# print(Style.RESET_ALL)


import colorama


# https://blog.csdn.net/weixin_43269020/article/details/86418555
def test_1():
    # print(\033[显示方式;前景色;背景色m输出内容\033[0m)
    print("显示方式：")
    content = "SuixinBlog: https://suixinblog.cn"
    print("\033[0m%s\033[0m" % content)  # 默认
    print("\033[1m%s\033[0m" % content)  # 粗体
    print("\033[4m%s\033[0m" % content)  # 下划线
    print("\033[5m%s\033[0m" % content)  # 闪烁
    print("\033[7m%s\033[0m" % content)  # 反白显示
    print("字体色：")
    print("\033[30m%s\033[0m" % content)  # 黑色
    print("\033[31m%s\033[0m" % content)  # 红色
    print("\033[32m%s\033[0m" % content)  # 绿色
    print("\033[33m%s\033[0m" % content)  # 黄色
    print("\033[34m%s\033[0m" % content)  # 蓝色
    print("\033[35m%s\033[0m" % content)  # 紫色
    print("\033[36m%s\033[0m" % content)  # 青色
    print("\033[37m%s\033[0m" % content)  # 白色
    print("背景色：")
    print("\033[40m\t%s\033[0m" % content)  # 黑色
    print("\033[41m\t%s\033[0m" % content)  # 红色
    print("\033[42m\t%s\033[0m" % content)  # 绿色
    print("\033[43m\t%s\033[0m" % content)  # 黄色
    print("\033[44m\t%s\033[0m" % content)  # 蓝色
    print("\033[45m\t%s\033[0m" % content)  # 紫色
    print("\033[46m\t%s\033[0m" % content)  # 青色
    print("\033[47m\t%s\033[0m" % content)  # 白色
    print("混合显示：")
    print("\033[1;37;40m\t%s\033[0m" % content)  # 粗体，白字，黑底


def test_2():
    # 在windows系统终端输出颜色要使用init(wrap=True)
    # colorama.init(wrap=True)  # 初始化，并且设置自动恢复到默认颜色
    colorama.init(autoreset=True)  # 初始化，并且设置自动恢复到默认颜色

    print(colorama.Fore.RED + 'some red text')
    print(colorama.Fore.BLACK + 'some red text')
    print(colorama.Fore.BLUE + 'some red text')
    print(colorama.Fore.CYAN + 'some red text')
    print(colorama.Fore.GREEN + 'some red text')
    print(colorama.Fore.WHITE + 'some red text')
    print(colorama.Fore.YELLOW + 'some red text')

    # 如果未设置autoreset=True，需要使用如下代码重置终端颜色为初始设置
    # 记得要及时关闭colorma的作用范围,如果不关闭的话,后面所有的输出都会是你指定的颜色
    # print(colorama.Fore.RESET + colorama.Back.RESET + colorama.Style.RESET_ALL) # autoreset=True


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
