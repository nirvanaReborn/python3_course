#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.jb51.net/article/117602.htm
# http://blog.phpsoho.com/2017/07/01/python%E4%BD%BF%E7%94%A8filetype%E7%B2%BE%E7%A1%AE%E5%88%A4%E6%96%AD%E6%96%87%E4%BB%B6%E7%B1%BB%E5%9E%8B/
# Python使用filetype精确判断文件类型
'''
支持类型
图片 
• jpg  –  image/jpeg 
• png  –  image/png 
• gif  –  image/gif 
• webp  –  image/webp 
• cr2  –  image/x-canon-cr2 
• tif  –  image/tiff 
• bmp  –  image/bmp 
• jxr  –  image/vnd.ms-photo 
• psd  –  image/vnd.adobe.photoshop 
• ico  –  image/x-icon
视频 
• mp4  –  video/mp4 
• m4v  –  video/x-m4v 
• mkv  –  video/x-matroska 
• webm  –  video/webm 
• mov  –  video/quicktime 
• avi  –  video/x-msvideo 
• wmv  –  video/x-ms-wmv 
• mpg  –  video/mpeg 
• flv  –  video/x-flv
音频 
• mid  –  audio/midi 
• mp3  –  audio/mpeg 
• m4a  –  audio/m4a 
• ogg  –  audio/ogg 
• flac  –  audio/x-flac 
• wav  –  audio/x-wav 
• amr  –  audio/amr
资料库 
• epub  –  application/epub+zip 
• zip  –  application/zip 
• tar  –  application/x-tar 
• rar  –  application/x-rar-compressed 
• gz  –  application/gzip 
• bz2  –  application/x-bzip2 
• 7z  –  application/x-7z-compressed 
• xz  –  application/x-xz 
• pdf  –  application/pdf 
• exe  –  application/x-msdownload 
• swf  –  application/x-shockwave-flash 
• rtf  –  application/rtf 
• eot  –  application/octet-stream 
• ps  –  application/postscript 
• sqlite  –  application/x-sqlite3 
• nes  –  application/x-nintendo-nes-rom 
• crx  –  application/x-google-chrome-extension 
• cab  –  application/vnd.ms-cab-compressed 
• deb  –  application/x-deb 
• ar  –  application/x-unix-archive 
• Z  –  application/x-compress 
• lz  –  application/x-lzip
字体 
• woff  –  application/font-woff 
• woff2  –  application/font-woff 
• ttf  –  application/font-sfnt 
• otf  –  application/font-sfnt

'''
import filetype


def test_help():
    help(filetype)


def test_dir():
    for i in dir(filetype):
        print(i)


def test_1():
    import getpass
    kind = filetype.guess(r'D:\{0}\Pictures\爱壁纸UWP\风景\风景 - 26.jpg'.format(getpass.getuser()))
    if kind is None:
        print('Cannot guess file type!')
        return
    else:
        print('File extension: %s' % kind.extension)
        print('File MIME type: %s' % kind.mime)


def test_2():
    kind = filetype.guess(r'D:\SVN\SVN_test\trunk\kungfu\python\kungfu\wingchun\wc_configs.py')
    if kind is None:
        print('Cannot guess file type!')
        return
    else:
        print('File extension: %s' % kind.extension)
        print('File MIME type: %s' % kind.mime)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
