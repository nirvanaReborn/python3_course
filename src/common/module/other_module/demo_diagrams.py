#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 程序员在编程的时候，有时候需要跟同事解释他设计的程序代码之间复杂的结构关系，然而这不是一两句话能说清楚的，需要画表或者做脉络图。
# 一般情况下，程序员使用GUI工具处理图表，并将文稿进行可视化处理。但是还有更好的方法，比如说使用Diagrams库。
# Diagrams让不需要任何设计类工具，直接在Python代码中绘制云系统结构。它们的图标来自多家云服务商，包括AWS, Azure, GCP等。
# 仅需几行代码，就可以简单地创造出箭头符号和结构图。
# 由于它使用Graphviz来渲染图，所以还需要先安装好Graphviz。(校验安装成功的cmd窗口命令:dot -version)
# 开源地址：https://github.com/mingrammer/diagrams
'''
# using pip (pip3)
$ pip install diagrams

# using pipenv
$ pipenv install diagrams

# using poetry
$ poetry add diagrams
'''

from diagrams import Diagram, Cluster
from diagrams.aws.compute import EC2
from diagrams.aws.database import RDS
from diagrams.aws.network import ELB


def test_1():
    dest_file = "Web_Service"  # 默认生成png文件
    with Diagram(dest_file, show=False):
        ELB("lb") >> EC2("web") >> RDS("userdb")  # 箭头关系


def test_2():
    dest_file = "Web_Service_Cluster"  # 默认生成png文件
    with Diagram(dest_file, show=True):
        with Cluster("DB Cluster"):
            db_master = RDS("master")
            db_master - [RDS("slave1"), RDS("slave2")]  # 直线连接
        ELB("lb") >> EC2("web") >> db_master


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
