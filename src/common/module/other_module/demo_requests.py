#!/usr/bin/env python
# -*- coding: UTF-8 -*-

'''
# https://blog.csdn.net/byweiker/article/details/79234853
HTTP 状态码有许多种，可以根据第一个（百位）数字来分成五类。
• 1xx（信息）
    服务器收到了请求，但是需要客户端发送一些额外的信息。
• 2xx（成功）
    请求成功。除了200 以外，其他的状态码还会包含一些特殊含义。
• 3xx（重定向）
    资源位置发生改变，所以响应会返回一个新的URL 给客户端。
• 4xx（客户端错误）
    客户端发生错误，比如最出名的404（页面不存在）。418（我是一个茶壶）是一个愚人节笑话。
• 5xx（服务端错误）
    500 是最常见的错误。你可能也见到过502（网关错误），这表示Web 服务器程序和后端的应用服务器之间无法连接。

状态码判断：常见的网页状态码：
100: ('continue',),
101: ('switching_protocols',),
102: ('processing',),
103: ('checkpoint',),
122: ('uri_too_long', 'request_uri_too_long'),

200: ('ok', 'okay', 'all_ok', 'all_okay', 'all_good', '\\o/', '✓'),
201: ('created',),
202: ('accepted',),
203: ('non_authoritative_info', 'non_authoritative_information'),
204: ('no_content',),
205: ('reset_content', 'reset'),
206: ('partial_content', 'partial'),
207: ('multi_status', 'multiple_status', 'multi_stati', 'multiple_stati'),
208: ('already_reported',),
226: ('im_used',),

# Redirection.
300: ('multiple_choices',),
301: ('moved_permanently', 'moved', '\\o-'),
302: ('found',),
303: ('see_other', 'other'),
304: ('not_modified',),
305: ('use_proxy',),
306: ('switch_proxy',),
307: ('temporary_redirect', 'temporary_moved', 'temporary'),
308: ('permanent_redirect', 'resume_incomplete', 'resume',), # These 2 to be removed in 3.0

# Client Error.
400: ('bad_request', 'bad'),
401: ('unauthorized',),
402: ('payment_required', 'payment'),
403: ('forbidden',),
404: ('not_found', '-o-'),
405: ('method_not_allowed', 'not_allowed'),
406: ('not_acceptable',),
407: ('proxy_authentication_required', 'proxy_auth', 'proxy_authentication'),
408: ('request_timeout', 'timeout'),
409: ('conflict',),
410: ('gone',),
411: ('length_required',),
412: ('precondition_failed', 'precondition'),
413: ('request_entity_too_large',),
414: ('request_uri_too_large',),
415: ('unsupported_media_type', 'unsupported_media', 'media_type'),
416: ('requested_range_not_satisfiable', 'requested_range', 'range_not_satisfiable'),
417: ('expectation_failed',),
418: ('im_a_teapot', 'teapot', 'i_am_a_teapot'),
421: ('misdirected_request',),
422: ('unprocessable_entity', 'unprocessable'),
423: ('locked',),
424: ('failed_dependency', 'dependency'),
425: ('unordered_collection', 'unordered'),
426: ('upgrade_required', 'upgrade'),
428: ('precondition_required', 'precondition'),
429: ('too_many_requests', 'too_many'),
431: ('header_fields_too_large', 'fields_too_large'),
444: ('no_response', 'none'),
449: ('retry_with', 'retry'),
450: ('blocked_by_windows_parental_controls', 'parental_controls'),
451: ('unavailable_for_legal_reasons', 'legal_reasons'),
499: ('client_closed_request',),

# Server Error.
500: ('internal_server_error', 'server_error', '/o\\', '✗'),
501: ('not_implemented',),
502: ('bad_gateway',),
503: ('service_unavailable', 'unavailable'),
504: ('gateway_timeout',),
505: ('http_version_not_supported', 'http_version'),
506: ('variant_also_negotiates',),
507: ('insufficient_storage',),
509: ('bandwidth_limit_exceeded', 'bandwidth'),
510: ('not_extended',),
511: ('network_authentication_required', 'network_auth', 'network_authentication'),
'''
'''
# Requests 库是一个阻塞式 HTTP 请求库，当我们发出一个请求后，程序会一直等待服务器的响应，
# 直到得到响应后程序才会进行下一步的处理，其实这个过程是比较耗费资源的。
# 如果程序可以在这个等待过程中做一些其他的事情，如进行请求的调度、响应的处理等等，那么爬取效率一定会大大提高。
# Requests 是⽤Python语⾔编写，基于urllib，采⽤Apache2 Licensed开源协议的 HTTP 库。
# 它⽐ urllib 更加⽅便，可以节约我们⼤量的⼯作，完全满⾜HTTP测试需求。
# http://blog.csdn.net/xie_0723/article/details/51361006
# requests的content和text方法的区别:
# response.text返回的是Unicode型的数据。
# response.content返回的是bytes型也就是二进制的数据。
# response.json()返回的是json格式数据
#
# 也就是说，如果你想取文本，可以通过response.text。
# 如果想取图片，文件，则可以通过response.content。

# https://blog.csdn.net/byweiker/article/details/79234853
# requests.post('http://httpbin.org/post')
# requests.head('http://httpbin.org/get')
# requests.put('http://httpbin.org/put')
# requests.delete('http://httpbin.org/delete')
# requests.options('http://httpbin.org/get')

# get 和 post比较常见 GET请求将提交的数据放置在HTTP请求协议头中
# GET： 请求指定的页面信息，并返回实体主体。
# POST： 请求服务器接受所指定的文档作为对所标识的URI的新的从属实体。
# HEAD： 只请求页面的首部。
# PUT： 从客户端向服务器传送的数据取代指定的文档的内容。
# DELETE： 请求服务器删除指定的页面。
# OPTIONS：

# GET 和 POST 请求方法有如下区别：
# 1.GET 方式请求中参数是包含在 URL 里面的，数据可以在 URL 中看到，
#   而 POST 请求的 URL 不会包含这些数据，数据都是通过表单的形式传输，会包含在 Request Body 中。
# 2.GET 方式请求提交的数据最多只有 1024 字节，而 POST 方式没有限制。
#
# 所以一般来说，网站登录验证的时候，需要提交用户名密码，这里包含了敏感信息，
# 使用GET方式请求的话密码就会暴露在URL里面，造成密码泄露，所以这里最好以POST方式发送。
# 文件的上传时，由于文件内容比较大，也会选用POST方式。
'''

import json
import requests
# https://blog.csdn.net/xiaoxin_OK/article/details/107683007
requests.packages.urllib3.disable_warnings()
# warnings.filterwarnings('ignore')

# logging 库获取日志 requests 和 urllib3(requests 依赖) 对象，调低他们的日志级别，
# 设置为 WARNING 则代表只有 WARNING 以上级别的日志才会被输出出来
## __import__('logging').captureWarnings(True)  # 通过捕获警告到日志的方式忽略警告
## __import__('urllib3').disable_warnings()  # 主要用于消除警告信息
import logging
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)

# from requests.packages.urllib3.exceptions import InsecureRequestWarning
# import ssl
# ssl._create_default_https_context = ssl._create_unverified_context
# requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

print(requests.__version__)


def test_help():
    help(requests)


def test_dir():
    for i in dir(requests):
        print(i)


def test_1():
    url = r'http://www.iqiyi.com/a_19rrgja8xd.html'
    response = requests.get(url)
    print(response)
    print(type(response.status_code), response.status_code)
    print('Request Successfully') if response.status_code == requests.codes.ok else exit()
    print(type(response.headers), response.headers)
    print(type(response.cookies), response.cookies)
    print(type(response.url), response.url)
    print(type(response.history), response.history)
    print(type(response.text), response.text)  # 返回的是Unicode型的数据。
    print(type(response.content), response.content)  # 返回的是bytes型也就是二进制的数据。
    print(type(response.json), response.json) # 返回的是json格式数据


# 基本的GET请求
def test_2():
    response = requests.get('http://httpbin.org/get')
    print(response.text)  # 返回的是Unicode型的数据。


# 带参数的GET请求
def test_3():
    response = requests.get("http://httpbin.org/get?name=germey&age=22")
    print(response.text)  # 返回的是Unicode型的数据。

    # 或者使用params的方法：
    data = {
        'name': 'germey',
        'age': 22
    }
    response = requests.get("http://httpbin.org/get", params=data)
    print(response.text)  # 返回的是Unicode型的数据。


# 解析json
def test_4():
    response = requests.get("http://httpbin.org/get")
    print(type(response.text))  # <class 'str'>
    print(response.json())  # 返回的是json格式数据
    print(json.loads(response.text))
    print(type(response.json()))  # <class 'dict'>


# 获取二进制数据
def test_5():
    response = requests.get("https://github.com/favicon.ico")
    print(type(response.text), type(response.content))  # <class 'str'> <class 'bytes'>
    print(response.text)  # 返回的是Unicode型的数据。
    print(response.content)  # 返回的是bytes型也就是二进制的数据。


# 添加headers
def test_6():
    # 有些网站访问时必须带有浏览器等信息，如果不传入headers就会报错，如下
    # response = requests.get("https://www.zhihu.com/explore")
    # print(response.text) # 400 Bad Request

    # 当传入headers时：
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36'
    }
    response = requests.get("https://www.zhihu.com/explore", headers=headers)
    print(response.text)


# 基本POST请求
def test_7():
    data = {'name': 'germey', 'age': '22'}
    response = requests.post("http://httpbin.org/post", data=data)
    print(response.text)


# Prepared Request对象
# 有了这个对象，我们就可以将一个个请求当做一个独立的对象来看待，
# 这样在进行队列调度的时候会非常方便，后面我们会有一节使用它来构造一个 Request 队列。
def test_8():
    url = 'http://httpbin.org/post'
    data = {
        'name': 'germey'
    }
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36'
    }
    s = requests.Session()
    req = requests.Request('POST', url, data=data, headers=headers)
    prepped = s.prepare_request(req)
    r = s.send(prepped)
    print(r.text)


# 文件上传
def test_9():
    files = {'file': open('cookie.txt', 'rb')}
    response = requests.post("http://httpbin.org/post", files=files)
    print(response.text)


# 获取cookie
def test_10():
    response = requests.get("https://www.baidu.com")
    print(response.cookies)
    for key, value in response.cookies.items():
        print(key + '--->' + value)

    # 如果某个响应中包含一些Cookie，你可以快速访问它们：
    response = requests.get('http://www.google.com.hk/')
    print(response.cookies['NID'])
    print(tuple(response.cookies))


def test_11():
    # 要想发送你的cookies到服务器，可以使用cookies参数：
    url = 'http://httpbin.org/cookies'
    cookies = {'testCookies_1': 'Hello_Python3', 'testCookies_2': 'Hello_Requests'}
    # 在Cookie Version 0中规定空格、方括号、圆括号、等于号、逗号、双引号、斜杠、问号、@，冒号，分号等特殊符号都不能作为Cookie的内容。
    response = requests.get(url, cookies=cookies)
    print(response.json())


# 会话维持
def test_12():
    s = requests.Session()
    s.get('http://httpbin.org/cookies/set/number/123456789')
    r = s.get('http://httpbin.org/cookies')
    print(r.text)


# SSL证书验证
def test_13():
    # 因为12306有一个错误证书，我们那它的网站做测试会出现下面的情况，证书不是官方证书，浏览器会识别出一个错误
    # response = requests.get('https://www.12306.cn')
    # print(response.status_code)

    # 将verify设置位False即可，返回的状态码为200
    response = requests.get('https://www.12306.cn', verify=False)
    print(response.status_code)

    # 当然我们也可以指定一个本地证书用作客户端证书，可以是单个文件（包含密钥和证书）或一个包含两个文件路径的元组。
    response = requests.get('https://www.12306.cn', cert=('/path/server.crt', '/path/key'))
    print(response.status_code)


# 代理设置
# 在进行爬虫爬取时，有时候爬虫会被服务器给屏蔽掉，这时采用的方法主要有降低访问时间，通过代理ip访问
def test_14():
    '''
    ip可以从网上抓取，或者某宝购买

    如果代理需要设置账户名和密码,只需要将字典更改为如下：
    proxies = {
    "http":"http://user:password@127.0.0.1:9999"
    }

    如果你的代理是通过sokces这种方式则需要pip install "requests[socks]"
    proxies= {
    "http":"socks5://127.0.0.1:9999",
    "https":"sockes5://127.0.0.1:8888"
    }
    '''
    proxies = {
        "http": "http://127.0.0.1:9743",
        "https": "https://127.0.0.1:9743",
    }

    response = requests.get("https://www.taobao.com", proxies=proxies)
    print(response.status_code)


# 超时设置
# 访问有些网站时可能会超时，这时设置好timeout就可以解决这个问题
def test_15():
    try:
        response = requests.get("http://httpbin.org/get", timeout=0.5)
        print(response.status_code)
    except requests.exceptions.ReadTimeout:
        print('Timeout')


# 认证设置
# 如果碰到需要认证的网站可以通过requests.auth模块实现
def test_16():
    # from requests.auth import HTTPBasicAuth
    #
    # response = requests.get("http://120.27.34.24:9001/", auth=HTTPBasicAuth("user", "123"))
    # print(response.status_code)

    # 当然这里还有一种方式
    response = requests.get("http://120.27.34.24:9001/", auth=("user", "123"))
    print(response.status_code)


# 异常处理
def test_17():
    # 遇到网络问题（如：DNS查询失败、拒绝连接等）时，Requests会抛出一个ConnectionError 异常。
    #
    # 遇到罕见的无效HTTP响应时，Requests则会抛出一个 HTTPError 异常。
    #
    # 若请求超时，则抛出一个 Timeout 异常。
    #
    # 若请求超过了设定的最大重定向次数，则会抛出一个 TooManyRedirects 异常。
    #
    # 所有Requests显式抛出的异常都继承自 requests.exceptions.RequestException。
    print(requests.exceptions.RequestException)


# https://blog.csdn.net/m0_61011147/article/details/124846902
def test_18():
    import io
    import sys
    sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')

    f = open(os.path.expanduser(r"~\Desktop\20220518.txt"), 'r')
    url = f.readlines()
    length = len(url)
    url_result_success = []
    url_result_failed = []
    for i in range(0, length):
        try:
            response = requests.get(url[i].strip(), verify=False, allow_redirects=True, timeout=5)
            if response.status_code != 200:
                raise requests.RequestException(u"Status code error: {}".format(response.status_code))
        except requests.RequestException as e:
            url_result_failed.append(url[i])
            continue
        url_result_success.append(url[i])
    f.close()
    result_len1 = len(url_result_failed)
    result_len2 = len(url_result_success)
    f = open('1.txt', 'w')
    # for i in range(0,result_len1):
    #   print (url_result_failed[i].strip()+"打开失败")
    # print("   ")

    for j in range(0, result_len2):
        # print (url_result_success[j].strip()+"打开成功")
        f.write(url_result_success[j].strip())
        f.write('\n')


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
