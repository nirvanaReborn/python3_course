#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
# Emoji库是一个表情包库，比较的有意思，特别是在分析媒体数据时，
# 例如微博、知乎等，用户评论里面就会有很多的Emoji，这个时候就会用上Emoji包
import emoji


def test_1():
    result = emoji.emojize('Python is :thumbs_up:')
    print(result)


def test_2():
    result = emoji.demojize('Python is 👍')
    print(result)


def main():
    from public_function import dict_choice

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
