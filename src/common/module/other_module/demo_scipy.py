#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys

#
#
import scipy


# https://www.toutiao.com/a6524951417617121806/
def test_1():
    # 新建矩阵
    a = scipy.mat("[1,2,3;3,5,8;7,8,9]")
    print(a)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
