#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 可以使用ast模块来检查Python代码语句，以防止JavaScript注入攻击。
# ast模块可以帮助解析Python代码并构建抽象语法树（Abstract Syntax Tree），从而可以对代码进行静态分析和检查。
import ast


def check_python_code(code):
    try:
        tree = ast.parse(code)
        for node in ast.walk(tree):
            if isinstance(node, ast.Call):
                if isinstance(node.func, ast.Attribute) and node.func.attr.lower() in ['exec', 'eval']:
                    raise Exception("Potential JavaScript injection detected in code")
    except Exception as e:
        print("Error:", e)
        return False
    return True


def main():
    # 测试代码
    code1 = "print('Hello, World!')"
    code2 = "eval('alert(\"XSS\")')"
    code3 = "os.system('rm -rf /')"
    code4 = "exec('print(\"Hello\")')"

    print(check_python_code(code1))  # True
    print(check_python_code(code2))  # False
    print(check_python_code(code3))  # True
    print(check_python_code(code4))  # False


if __name__ == '__main__':
    main()
