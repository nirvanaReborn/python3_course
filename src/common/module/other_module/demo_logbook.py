#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/imyalost/p/9026379.html
# logbook的设计初衷，就是用来取代python的标准库日志模块：logging。
# https://logbook.readthedocs.io/en/stable/index.html
# 输入 pip install logbook 命令进行安装
# 安装成功后，输入 pip show logbook 命令，查看相关信息。
import logbook
from logbook import Logger,StreamHandler,FileHandler,TimedRotatingFileHandler
from logbook.more import ColorizedStderrHandler
import os


def log_type(record,handler):
    log = "[{date}] [{level}] [{filename}] [{func_name}] [{lineno}] {msg}".format(
        # date = record.time.replace(microsecond=0),       # 日志时间
        date=record.time,                                # 日志时间
        level = record.level_name,                       # 日志等级
        filename = os.path.split(record.filename)[-1],   # 文件名
        func_name = record.func_name,                    # 函数名
        lineno = record.lineno,                          # 行号
        msg = record.message                             # 日志内容
    )
    return log


def test_1():
    # 日志存放路径
    LOG_DIR = os.path.join("Log")
    if not os.path.exists(LOG_DIR):
        os.makedirs(LOG_DIR)
    # 日志打印到屏幕
    log_std = ColorizedStderrHandler(bubble=True)
    log_std.formatter = log_type
    # 日志打印到文件
    log_file = TimedRotatingFileHandler(os.path.join(LOG_DIR, '%s.log' % 'log'), date_format='%Y-%m-%d', bubble=True, encoding='utf-8')
    log_file.formatter = log_type
    # 脚本日志
    run_log = logbook.Logger("script_log")
    logbook.set_datetime_format("local")
    run_log.handlers = []
    run_log.handlers.append(log_file)
    run_log.handlers.append(log_std)

    run_log.info('This is info message')


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
