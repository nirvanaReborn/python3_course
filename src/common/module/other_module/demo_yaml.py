#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# ruamel.yaml 和 PyYAML 都是 Python 中常用的 YAML 解析库,但之间有以下主要区别:
# 1. 兼容性:ruamel.yaml 与 PyYAML 兼容,但 ruamel.yaml 支持更新版本的 YAML 标准,如 YAML 1.2。
# 2. 安全性:ruamel.yaml 在解析 YAML 时具有更强的类型安全性检查,可以避免许多潜在的安全问题。
# 3. 可变性:ruamel.yaml 在加载 YAML 文档后返回的对象是可变的,可以在不破坏递归引用的情况下编辑 YAML 对象。PyYAML 返回的对象是不可变的。
# 4. 注释支持:ruamel.yaml 在 dump YAML 文档时可以保留注释,而 PyYAML 会清除注释。
# 5. 兼容模式:ruamel.yaml 可以在兼容模式下解析 YAML 文档,以最大限度的向后兼容。 PyYAML 默认采用最新 YAML 标准解析文档。
# 6. 扩展名:ruamel.yaml 还支持 .yml 扩展名,而 PyYAML 只支持 .yaml 扩展名。
# 7. 文档组:ruamel.yaml 支持将多个 YAML 文档编组在一个文件中,并可以分别加载各个文档。 PyYAML 不能直接支持这种用法。
# 除此之外,这两个库在功能和性能上都比较接近。所以选择哪个库主要取决于具体需求和偏好。
# ruamel.yaml 的主要优势是:
# 1. 更安全:具有更强的类型检查,可以避免许多安全问题。
# 2. 更标准:支持最新 YAML 版本和更广泛的功能。
# 3. 可变和注释支持:方便编辑和调试 YAML 文档。
# 而 PyYAML 的优势是:
# 1. 简单易用:API 相对简单一些,学习门槛略低。
# 2. 成熟稳定:PyYAML 已经使用多年,十分稳定成熟。
# 3. 高兼容:可以最大限度兼容老版本的 YAML 文档。

try:
    # pip install ruamel.yaml
    from ruamel import yaml
except:
    # pip install pyyaml
    import yaml
import os
import pprint


def test_1():
    def parse_file(source_file):
        try:
            # 设置以utf-8解码模式读取文件，encoding参数必须设置，
            # 否则默认以gbk模式读取文件，当文件中包含中文时，会报错
            with open(source_file, 'r', encoding='utf-8') as fd:
                # https://blog.csdn.net/chengdong996/article/details/123962061
                # 编译器提示 yaml 的 load()函数 缺少 1 个必要参数：'Loader'，这是因为 Yaml 5.1 版本后弃用了 yaml.load(file) 这个用法。
                config = yaml.safe_load(fd)
                config = dict(sorted(config.items(), key=lambda d: d[0]))
                pprint.pprint(config)

        except Exception as e:
            print("配置文件解析失败:", source_file, e)

    # source_file = r"D:\SVN\PBOX-FLY\trunk\Sources\fly_IQ_trunk\fly_config\config\IQData_config.yml"
    source_file = r"D:\SVN\PBOX-FLY\trunk\Sources\fly_rdc_base\IQE-Server\IQData\plugin_config.yml"
    parse_file(source_file)


def test_2():
    # 生成yaml文档
    def generate_yaml_doc(config):
        current_path = os.path.abspath(".")
        yaml_path = os.path.join(current_path, "generate.yaml")
        with open(yaml_path, 'w', encoding='utf-8') as fw:
            # allow_unicode=True 表示正常显示中文
            # Dumper: 这是一个类，用于指定自定义的 YAML 转储器。PyYAML 库提供了几种内置的 Dumper 类，例如 yaml.Dumper 和 yaml.RoundTripDumper。
            #         yaml.RoundTripDumper 会尽可能保留 Python 对象的样式和结构，包括缩进和换行符，适合在需要保持 YAML 文件格式一致性的情况下使用。
            yaml.dump(config, fw, allow_unicode=True, sort_keys=True, Dumper=yaml.RoundTripDumper)

    source_file = r"D:\SVN\PBOX-FLY\trunk\Sources\fly_IQ_trunk\fly_config\config\IQData_config.yml"
    with open(source_file, 'r', encoding='utf-8') as fd:
        config = yaml.safe_load(fd)
    generate_yaml_doc(config)


def test_3():
    def generate_yaml(source_file, yaml_path):
        try:
            with open(source_file, 'r', encoding='utf-8') as fd:
                config = yaml.safe_load(fd)
            config = dict(sorted(config['plugins'].items(), key=lambda d: d[0]))
            with open(yaml_path, 'w', encoding='utf-8') as fw:
                yaml.dump(config, fw, allow_unicode=True, Dumper=yaml.RoundTripDumper)
        except Exception as e:
            print("配置文件解析失败:", source_file, e)

    source_file = r"D:\SVN\PBOX-FLY\trunk\Sources\fly_rdc_base\IQE-Server\IQData\plugin_config.yml"
    yaml_path = r"D:\SVN\PBOX-FLY\trunk\Sources\fly_rdc_base\IQE-Server\IQData\plugin_config-001.yml"
    generate_yaml(source_file, yaml_path)


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    print(yaml.__version__)
    main()
