#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 你若还在为生成名字、地址、IP地址而发愁，试试Faker库吧。
# 它是专业生成假数据的神器，但生成的数据看起来又如此“不假”。
# pip install Faker
import faker


def test_1():
    fake = faker.Faker(locale="zh_CN")
    print(fake.name())
    print(fake.address())
    print(fake.text())


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
