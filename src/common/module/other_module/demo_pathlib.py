#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.jianshu.com/p/ae194371cf7c
# https://www.toutiao.com/a6738562203433042446/
'''
pathlib相对于os.path有以下优势：
◆ pathlib导入更加清晰；
◆ 采用面向对象编程，能同时存储更多状态，os.path返回的通常只有一个字符串；
◆ pathlib使用更简单

pathlib劣势：
我注意到的第一个缺陷是 pathlib.Path 的方法中缺少与 shutil 等效的功能。
同样也没有与 os.chdir 等效的 pathlib 功能。
也没有与 os.walk 函数等价的 pathlib 函数。
'''
import pathlib
import pathlib2


def test_1():
    # os.path 笨拙
    import os
    base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    templates_dir = os.pardir.join(base_dir, 'templates')

    # os.path 模块需要函数嵌套，但是 pathlib 模块的 path 类允许我们链式操作 Path 对象上的方法和属性，以获得等效的路径表示。
    base_dir = pathlib.Path(__file__).resolve().parent.parent
    print(base_dir)
    templates_dir = base_dir.joinpath('templates')


def test_2():
    # 我们可以使用 glob.glob 函数查找与特定模式匹配的文件：
    from glob import glob
    top_level_csv_file = glob('*.csv')
    all_csv_file = glob('**/*.csv', recursive=True)

    # 新的 pathlib 模块同样包括类似 glob 的功能。
    top_level_csv_file = pathlib.Path.cwd().glob('*.csv')
    all_csv_file = pathlib.Path.cwd().rglob('*.csv')


def test_3():
    # 你可以使用 with 语句块打开文件、读取其内容然后关闭文件：
    from glob import glob
    file_contents = []
    for file_name in glob('**/*.csv', recursive=True):
        with open(file_name) as python_file:
            file_contents.append(python_file.read())

    # 或者你可以用 Path 对象的 read_text 方法，在一行代码中用列表解析功能将文件内容读取到一个新列表中：
    file_contents = [
        path.read_text()
        for path in pathlib.Path.cwd().rglob('*.py')
    ]


def test_4():
    # 可以使用 open 上下文管理器：
    with open('.editorconfig') as config:
        config.write('# config goes here')

    # 或者使用 write_text 方法
    pathlib.Path('.editorconfig').write_text('# config goes here')

    with pathlib.Path('.editorconfig').open(mode='wt') as config:
        config.write('# config goes here')

    file = pathlib.Path('.editorconfig')
    with open(file, mode='wt') as config:
        config.write('# config goes here')


def test_5():
    import datetime
    # JSON 对象可以反序列化到字典，
    # 日期在本地使用 datetime.date 对象表示，
    # 文件系统路径现在可以使用 pathlib.path 对象统一表示。
    # 以下是这些对象更有用的表示：
    person = {"name": "Trey Hunner", "location": "San Diego"}
    pycon_2019 = datetime.date(2019, 5, 1)
    home_directory = pathlib.Path('/home/trey')


def test_6():
    source_file = r"D:\SVN\PBOX\trunk\Sources\DevCodes\1.function_design"
    print(pathlib2.Path(source_file).parent.parent.name)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
