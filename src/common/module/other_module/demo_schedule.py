#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/longsongpong/p/10998619.html
# python中有一个轻量级的定时任务调度的库：schedule。
# 他可以完成每分钟，每小时，每天，周几，特定日期的定时任务。因此十分方便我们执行一些轻量级的定时任务.
'''
Python 定时任务的实现方式:
1、while循环中使用sleep
缺点：不容易控制，而且是个阻塞函数，适合简单测试

2、schedule模块
优点：可以管理和调度多个任务,可以进行控制
缺点：需要在循环中检测任务，阻塞式函数

3、Threading模块中的Timer
优点：非阻塞
缺点：不易管理多个任务；对定点任务来说，需要检查当前时间点；

4、sched模块
sched模块实现了一个时间调度程序，该程序可以通过单线程执行来处理按照时间尺度进行调度的时间。

5、定时框架APScheduler
# https://www.cnblogs.com/meipu/p/14372817.html

6、异步框架celery

'''
import schedule
import time
import threading


def test_1():
    def job(name):
        print("her name is : ", name)

    '''
    每隔十分钟执行一次任务
    每隔一小时执行一次任务
    每天的10:30执行一次任务
    每隔5到10天执行一次任务
    每周一的这个时候执行一次任务
    每周三13:15执行一次任务
    '''
    name = "longsongpong"
    schedule.every(10).minutes.do(job, name)
    schedule.every().hour.do(job, name)
    schedule.every().day.at("10:30").do(job, name)
    schedule.every(5).to(10).days.do(job, name)
    schedule.every().monday.do(job, name)
    schedule.every().wednesday.at("13:15").do(job, name)

    while True:
        # run_pending：运行所有可以运行的任务
        schedule.run_pending()
        time.sleep(1)


# schedule方法是串行的，也就是说，如果各个任务之间时间不冲突，那是没问题的；如果时间有冲突的话，会串行的执行命令
def test_2():
    def job():
        print("I'm working... in job1  start")
        time.sleep(15)
        print("I'm working... in job1  end")

    def job2():
        print("I'm working... in job2")

    schedule.every(10).seconds.do(job)
    schedule.every(10).seconds.do(job2)

    while True:
        schedule.run_pending()
        time.sleep(1)


def test_3():
    def job():
        print("I'm working... in job1  start")
        time.sleep(15)
        print("I'm working... in job1  end")

    def job2():
        print("I'm working... in job2")

    def run_threaded(job_func):
        job_thread = threading.Thread(target=job_func)
        job_thread.start()

    schedule.every(10).seconds.do(run_threaded, job)
    schedule.every(10).seconds.do(run_threaded, job2)

    while True:
        schedule.run_pending()
        time.sleep(1)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
