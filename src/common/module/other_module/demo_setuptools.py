#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 该脚本执行命令: python build.py build_ext --inplace
# https://blog.csdn.net/mingqi1996/article/details/88766619
# https://blog.csdn.net/xu_fengyu/article/details/105360648
'''
https://sighingnow.github.io/%E7%BC%96%E7%A8%8B%E8%AF%AD%E8%A8%80/use_vs2015_build_cython.html
在 Windows 平台上，Python 默认使用的编译器是 MSVC，如果要使用 mingw 作为编译器，
在 Python 安装目录下的 Lib\distutils 下面的 distutils.cfg 文件(如果没有这个文件，创建一个即可)中加入一下内容：
[build]
compiler = mingw32

而想要指定使用 MSCV 编译器，在 distutils.cfg 中加入:
[build]
compiler = msvc

注意：这种方法支持python的版本仅仅到3.4，需要自己创建distutils.cfg*** 文件，因为python默认是Microsoft Build Tools*

更多详细的信息可以参考相关说明：

https://wiki.python.org/moin/WindowsCompilers#Microsoft_Visual_C.2B-.2B-_14.2_standalone:Build_Tools_for_Visual_Studio_2019.28x86.2C_x64.2C_ARM.2C_ARM64.29
————————————————
版权声明：本文为CSDN博主「TensorME」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/xu_fengyu/article/details/105360648

https://blog.csdn.net/mingqi1996/article/details/88766619
https://note.qidong.name/2018/03/setup-warning-strict-prototypes/
https://www.it610.com/article/1290261252605550592.htm
'''
# from distutils.core import setup
# from distutils import sysconfig
# setuptools是distutils的增强版。(Python3.10版本中distutils 被弃用)
from setuptools import setup, Extension, find_packages
from setuptools.command.build_ext import build_ext
from Cython.Build import cythonize
import os
import traceback
import numpy


# print("numpy include_dirs:", numpy.get_include())


# def py2pyd(source_file):
#     # 扩展模块
#     ext_module = Extension(
#         # 模块名称
#         name='libtest_example',
#         # 源码
#         sources=[r'src\libtest_example.cpp',
#                  r'src\fft_tools.cpp'],
#     )

# https://note.qidong.name/2018/03/setup-warning-strict-prototypes/
# 解决setup.py编译C++代码的-Wstrict-prototypes警告
class BuildExt(build_ext):
    def build_extensions(self):
        self.compiler.compiler_so.remove('-Wstrict-prototypes')
        super(BuildExt, self).build_extensions()


def py2c(source_file):
    # 返回distutils.extension.Extension对象列表
    ext_module = Extension(
        # 模块名称
        os.path.splitext(os.path.basename(source_file))[0],
        language="c++",
        # 源码
        sources=[source_file],
        # 包含头文件
        include_dirs=[
            r'D:\ProgramFiles\Anaconda3\include',
            r'D:\share\git\gitee\zhangtao2016\course_cpp\src\cpp_python_interacting\include',
            # r'D:\opencv-4.1.0\opencv\build\include',
            numpy.get_include(),
        ],
        # 库目录
        library_dirs=[
            # r'D:\opencv-4.1.0\opencv\build\x64\vc15\lib',
        ],
        # 链接库文件
        libraries=[
            # r'opencv_world410',
        ],
        # 编译参数列表
        extra_compile_args=["-std=c++11"] if os.name == "posix" else [],
        # 链接参数列表
        extra_link_args=[],
    )

    if source_file.endswith(".py") or source_file.endswith(".pyx"):
        module_list = cythonize(ext_module, language_level=3)
    else:
        module_list = [ext_module]

    setup(
        name="lib_example",  # 包名称
        version="1.0.0.0",  # 包版本号
        description='A simaple demo',
        author="zt",  # 作者
        author_email="xxx@xxx.com",  # 作者邮箱
        ext_modules=module_list,  # 扩展模块
        # install_requires=['numpy']
        # cmdclass={'build_ext': BuildExt},
        packages=find_packages(),  # 动态获取packages
    )


def main():
    source_file = r'xxx.py'
    try:
        py2c(source_file)
    except Exception as e:
        print(source_file, e)
        traceback.print_exc()


if __name__ == '__main__':
    main()
