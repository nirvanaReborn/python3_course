#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/bincoding/p/15658176.html
# python利用thriftpy2将json、struct、obj互相转换
# https://github.com/Thriftpy/thriftpy2
# pip install thriftpy2
import thriftpy2.protocol.json as proto


def test_struct_to_json():
    obj = TItem(id=13, phones=["5234", "12346456"])
    json = proto.struct_to_json(obj)
    # 返回dict类型，str(json)转字符串
    assert {"id": 13, "phones": ["5234", "12346456"]} == json


def test_struct_to_obj():
    json = {"id": 13, "phones": ["5234", "12346456"]}
    obj = TItem()

    obj = proto.struct_to_obj(json, obj)

    assert obj.id == 13 and obj.phones == ["5234", "12346456"]


def test_1():
    pass


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
