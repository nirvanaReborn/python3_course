#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://zhidao.baidu.com/question/2014148124740026028.html
# pip install chardet chardet2
import chardet


def test_help():
    help(chardet)


def test_dir():
    for i in dir(chardet):
        print(i)


def test_1():
    import urllib.request
    testdata = urllib.request.urlopen('https://www.baidu.com/').read()
    print(chardet.detect(testdata))


def test_2():
    testdata = open(r"D:\MongoDB\log\mongodb.log", "rb").read()
    print(chardet.detect(testdata))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
