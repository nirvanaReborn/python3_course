#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/xiao-apple36/p/9089027.html
# Click 是 Flask 的开发团队 Pallets 的另一款开源项目，它是用于快速创建命令行的第三方模块。
# 我们知道，Python 内置了一个 Argparse 的标准库用于创建命令行，但使用起来有些繁琐，Click 相比于 Argparse，就好比 requests 相比于 urllib。
# 参考文档 http://click.pocoo.org/6/options/
'''
Click 对argparse 的主要改进在易用性，使用Click 分为两个步骤：
1.使用 @click.command() 装饰一个函数，使之成为命令行接口；
2.使用 @click.option() 等装饰函数，为其添加命令行选项等。
    option 常用的设置参数如下：
        default: 设置命令行参数的默认值
        help: 参数说明
        type: 参数类型，可以是 string, int, float 等
        prompt: 当在命令行中没有输入相应的参数时，会根据 prompt 提示用户输入
        nargs: 指定命令行参数接收的值的个数
        metavar：如何在帮助页面表示值
'''
# pip install click
import click


@click.command()
@click.option('--count', default=1, help='Number of greetings.')
@click.option('--name', prompt='Your name', help='The person to greet.')
def hello(count, name):
    '''
    @click.command() 使函数 hello 成为命令行接口；
    @click.option 的第一个参数指定了命令行选项的名称，可以看到，count 的默认值是 1；
    使用 click.echo 进行输出是为了获得更好的兼容性，因为 print 在 Python2 和 Python3 的用法有些差别。
    '''
    for x in range(count):
        click.echo('Hello %s!' % name)


if __name__ == "__main__":
    hello()

# python demo_click.py --count=5