#!/usr/bin/env python
# -*- coding: UTF-8 -*-

'''
 【图像处理】Python-Image 基本的图像处理操作
http://blog.csdn.net/followingturing/article/details/7996495
http://blog.csdn.net/chenriwei2/article/details/42071517

Pillow:基于PIL，处理python 3.x的图形图像库.因为PIL只能处理到python 2.x，而这个模块能处理Python3.x，目前用它做图形的很多.
http://www.cnblogs.com/apexchu/p/4231041.html
'''
# pip install Pillow
from PIL import Image


def test_1():
    help(Image)


def test_2():
    for i in dir(Image):
        print(i)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
