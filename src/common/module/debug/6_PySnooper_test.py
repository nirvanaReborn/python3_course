#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Python里三个最高逼格的调试神器(不借助IDE)
# https://blog.csdn.net/wade1203/article/details/102927380
# PySnooper也是一个Python的第三方库，他的特点是能够精准的显示每条代码的执行顺序、执行时间以及随之带来的局部变量的改变等等。
# @pysnooper.snoop()支持的参数还有很多，分别对应了不同的功能，例如监控自定义表达式、监控底层函数、支持多线程等等，详见项目文档。
# pip install pysnooper
# conda install -c conda-forge pysnooper
import pysnooper
import random


# @pysnooper.snoop()
@pysnooper.snoop('file.log')
def foo():
    lst = []
    for i in range(10):
        lst.append(random.randrange(1, 1000))
    lower = min(lst)
    upper = max(lst)
    mid = (lower + upper) / 2
    print(lower, mid, upper)


def foo_2():
    lst = []
    for i in range(10):
        lst.append(random.randrange(1, 1000))
    # pysnooper还支持局部监控
    with pysnooper.snoop():
        lower = min(lst)
        upper = max(lst)
        mid = (lower + upper) / 2
    print(lower, mid, upper)


def main():
    foo()
    # foo_2()


if __name__ == "__main__":
    main()
