#! /usr/bin/python 
# -*- coding: UTF-8 -*-

# http://www.cnblogs.com/zhangyingai/p/7097920.html
# https://www.runoob.com/python/python-exceptions.html
# http://www.365yg.com/group/6431880244865466626/
# 测试工程师必备神器python之异常处理总结

# 目前大多数采用的方法主要有以下几种：
#     Print函数
#     Log日志
#     IDE调试器
# 但是这些方法有着无法忽视的弱点：繁琐,过度依赖工具。

"""
语法：
try:
    <语句>             #运行别的代码
except <名字>：
    <语句>             #如果在try部份引发了'name'异常
except <名字>，<数据>:
    <语句>             #如果引发了'name'异常，获得附加的数据
else:
    <语句>             #如果没有异常发生
finally:
    <语句>             #退出try时总会执行
"""


def test_1(source_file="test.txt"):
    try:
        fh = open(source_file, "r")
        print(fh.read())
    except IOError:
        print("Error: can\'t find file or read data")
        return 1
    else:
        print("Written content in the file successfully")
        fh.close()
        return 2
    finally:
        print("我是永远要执行的，尽管出错了")
        return 0


# -------------------------------------------------------------------------------

"""
try:
    <语句>
finally:
    <语句>    #退出try时总会执行
raise	
"""


def test_2(source_file="test.txt"):
    try:
        fh = open(source_file, "w")
        fh.write("This is my test file for exception handling!!")
    finally:
        print("Error: can\'t find file or read data")


# -------------------------------------------------------------------------------

def test_3(source_file="test.txt"):
    try:
        fh = open(source_file, "w")
        try:
            fh.write("This is my test file for exception handling!!")
        finally:
            print("Going to close the file")
            fh.close()
    except IOError:
        print("Error: can\'t find file or read data")


# --------------------------异常的参数-------------------------------------------
"""
一个异常可以带上参数，可作为输出的异常信息参数。
你可以通过except语句来捕获异常的参数。
"""


def test_4():
    def temp_convert(var):
        try:
            return int(var)
        except ValueError as Argument:
            print("The argument does not contain numbers\n", Argument)

    temp_convert("xyz")


# ----------------------------触发异常-------------------------------------------	 
def test_5(level=0):
    if level < 1:
        raise Exception("Invalid level!", level)
        # The code below to this would not be executed
        # if we raise the exception


# ----------------------------用户自定义异常-------------------------------------
class NetworkError(RuntimeError):
    def __init__(self, args):
        self.args = args

    def __str__(self):
        return str(self.args) + ',网络错误。'


# 在你定义以上类后，你可以触发该异常
def test_6():
    try:
        raise NetworkError("Bad hostname")
    except NetworkError as e:
        print(e.args)


# -------------------------------------------------------------------------------
def test_7():
    try:
        file = open("a.txt")
        file.close()
    except FileNotFoundError as e:
        print("文件出错了，原因是%s" % str(e))


# 基本语法 结合finally的语法
def test_8():
    try:
        int('abc')
    except ValueError as e:
        print("出错了，原因是%s" % str(e))
    finally:
        print("我是永远要执行的，尽管出错了")


# 多个异常的处理方式
def test_9():
    try:
        int('abc')
        file = open("a.txt")
        file.close()
    except FileNotFoundError as e:
        print("出错了，原因是%s" % str(e))
    except ValueError as e:
        print("出错了，原因是%s" % str(e))


def test_10():
    try:
        int('abc')
        file = open("a.txt")
        file.close()
    except (FileNotFoundError, ValueError):
        print("出错了")


# raise 程序引出一个异常
def test_11():
    try:
        int('abc')
        file = open("a.txt")
        file.close()
    except ValueError as e:
        print("出错了，原因是%s" % str(e))
        raise FileNotFoundError("引出一个异常")


# 结合else（这个语法挺好玩的），没有任何异常时，执行else的代码
def test_12():
    try:
        int('abc')
    except ValueError as e:
        print("出错了，原因是%s" % str(e))
    else:
        print("没有任何异常")


# 在python的异常中，有一个万能异常：Exception，他可以捕获任意异常
def test_13():
    s1 = 'hello'
    try:
        int(s1)
    except Exception as e:
        '丢弃或者执行其他逻辑'
        print(e)


def test_14():
    import math

    try:
        # 计算一个非常大的浮点数，导致溢出
        large_number = math.exp(1000)
    except OverflowError as e:
        print(f"溢出错误： {e}")


# -------------------------------------------------------------------------------
def main():
    # dict_choice = {}
    # for i in range(0, 100):
    #     dict_choice[str(i)] = "test_{}()".format(str(i))
    #
    # choice = str(input("Enter a positive integer to choice: "))
    # if choice in dict_choice.keys():
    #     exec(dict_choice[choice])
    # else:
    #     print("输入有误，请重输！", choice)
    print(test_1())


if __name__ == '__main__':
    main()
