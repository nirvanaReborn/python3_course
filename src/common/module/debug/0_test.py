#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.cnblogs.com/xybaby/p/7183854.html
# 程序员必知的Python陷阱与缺陷列表
# 本文关注的主要是python陷阱，具体而言，是指CPython，而且除非特别说明，所有代码示例都是在python2.7运行的。


def test_0():
    def func(ok):
        import random
        if ok:
            a = random.random()  # UnboundLocalError: local variable 'random' referenced before assignment
        else:
            import random
            a = random.randint(1, 10)  # 随机整数
        return a

    func(True)


'''
# 陷阱一：以mutable对象作为默认参数
# 这个估计是最广为人知的了，Python和其他很多语言一样，提供了默认参数，默认参数确实是个好东西，
# 可以让函数调用者忽略一些细节（比如GUI编程，Tkinter，QT），对于lambda表达式也非常有用。
# 但是如果使用了可变对象作为默认参数，那么事情就不那么愉快了
'''


def test_1():
    def func(lst=[]):
        lst.append(1)
        return lst

    print(func())  # [1]
    print(func())  # [1, 1]


# stackoverflow上有一个更适当的例子来说明默认参数是在定义的时候求值，而不是调用的时候。
# https://stackoverflow.com/questions/530530/python-2-x-gotchas-and-landmines
def test_2():
    import time
    def report(when=time.time()):
        return when

    print(report())  # 1521281436.1672466
    time.sleep(1)
    print(report())  # 1521281436.1672466


# 标准的解决办法
def test_3():
    import time
    def report(when=None):
        if when is None:
            when = time.time()
        return when

    print(report())  # 1521281511.1412866
    time.sleep(1)
    print(report())  # 1521281512.1425998


'''
# 陷阱二: x += y   vs   x = x + y
# 一般来说，二者是等价的，至少看起来是等价的（这也是陷阱的定义 -- 看起来都OK，但不一定正确）。
'''


def test_4():
    x = 1
    x += 1
    print(x)

    x = 1
    x = x + 1
    print(x)

    x = [1]
    x += [2]
    print(x)

    x = [1]
    x = x + [2]
    print(x)


def test_5():
    x = [1]
    print(id(x))  # 2155781107784
    x += [2]  # x在原来的对象上修改
    print(id(x))  # 2155781107784

    x = [1]
    print(id(x))  # 2155781068552
    x = x + [2]  # x指向一个新的对象
    print(id(x))  # 2155780304840

    # 当然，那种效果是正确的取决于应用场景。至少，得知道，二者有时候并不一样


'''
# 陷阱三: 神奇的小括号()
# 小括号（parenthese）在各种编程语言中都有广泛的应用，
# python中，小括号还能表示元组（tuple）这一数据类型, 元组是immutable的序列。
'''


def test_6():
    a = (1, 2)
    print(type(a))  # <class 'tuple'>
    print(type(()))  # <class 'tuple'>

    a = (1)
    print(type(a))  # <class 'int'>

    # 如果要表示只有一个元素的元组，正确的姿势是:
    a = (1,)
    print(type(a))  # <class 'tuple'>


'''
# 陷阱四: 生成一个元素是列表的列表
# 这个有点像二维数组，当然生成一个元素是字典的列表也是可以的，更通俗的说，生成一个元素是可变对象的序列。
'''


def test_7():
    a = [[]] * 10
    print(a)  # [[], [], [], [], [], [], [], [], [], []]

    a[0].append(10)
    print(a[0])  # [10]
    print(a)  # [[10], [10], [10], [10], [10], [10], [10], [10], [10], [10]]

    # 我猜，这应该不是你预期的结果吧，究其原因，还是因为python中list是可变对象，上述的写法大家都指向的同一个可变对象，正确的姿势
    a = [[] for _ in range(10)]
    print(a)  # [[], [], [], [], [], [], [], [], [], []]
    a[0].append(10)
    print(a)  # [[10], [], [], [], [], [], [], [], [], []]


'''
# 陷阱五: 在访问列表的时候，修改列表
# 列表（list）在python中使用非常广泛，当然经常会在访问列表的时候增加或者删除一些元素。
# 比如，下面这个函数，试图删掉列表中为3的倍数的元素：
'''


def test_8():
    def modify_lst(lst):
        for idx, elem in enumerate(lst):
            if elem % 3 == 0:
                del lst[idx]

    lst = [1, 2, 3, 4, 5, 6]
    modify_lst(lst)
    print(lst)  # [1, 2, 4, 5]

    # 好像没什么错，不过这只是运气好
    lst = [1, 2, 3, 6, 5, 4]
    modify_lst(lst)
    print(lst)  # [1, 2, 6, 5, 4]
    # 上面的例子中，6这个元素就没有被删除。如果在modify_lst函数中print idx， item就可以发现端倪：
    # lst在变短，但idx是递增的，所以在上面出错的例子中，当3被删除之后，6变成了lst的第2个元素（从0开始）。
    # 在C++中，如果遍历容器的时候用迭代器删除元素，也会有同样的问题。
    # 如果逻辑比较简单，使用list comprehension是不错的注意


'''
# 陷阱六: 闭包与lambda
# 这个也是老生长谈的例子，在其他语言也有类似的情况。先看一个例子:
'''


def test_9():
    def create_multipliers():
        return [lambda x: i * x for i in range(5)]

    for multiplier in create_multipliers():
        print(multiplier(2))
    # 预期的结果是0，2，4，6，8. 但结果是5个8，意外不意外。


# 在上面的例子中，i就是在闭包作用域（enclousing），
# 而Python的闭包是 迟绑定 ， 这意味着闭包中用到的变量的值，是在内部函数被调用时查询得到的。
# 解决办法也很简单，那就是变闭包作用域为局部作用域。
def test_10():
    def create_multipliers():
        return [lambda x, i=i: i * x for i in range(5)]

    for multiplier in create_multipliers():
        print(multiplier(2))


'''
# 陷阱七: 定义__del__
# 大多数计算机专业的同学可能都是先学的C、C++，构造、析构函数的概念应该都非常熟。
# 于是，当切换到python的时候，自然也想知道有没有相应的函数。
# 比如，在C++中非常有名的RAII，即通过构造、析构来管理资源（如内存、文件描述符）的声明周期。
# 那在python中要达到同样的效果怎么做呢，即需要找到一个对象在销毁的时候一定会调用的函数，
# 于是发现了__init__, __del__函数，可能简单写了两个例子发现确实也能工作。
# 如果在循环引用中的对象定义了__del__,那么python gc不能进行回收，因此，存在内存泄漏的风险。
'''

'''
# 陷阱八: 不同的姿势import同一个module
# 示例：https://stackoverflow.com/questions/1459236/module-reimported-if-imported-from-different-path
# 在不同函数中import的 模块 不是同一个module，ID不同。
# 在很多项目中，为了import的时候方便，会往sys.path加入一堆路径：sys.path.append('../')
# 那么在项目中，大家同意一种import方式就非常有必要了
'''

'''
# 陷阱九: python升级
# python3.x并不向后兼容，所以如果从2.x升级到3.x的时候得小心了，下面列举两点：
# 1.在python2.7中，range的返回值是一个列表；而在python3.x中，返回的是一个range对象。
# 2.map()、filter()、 dict.items()在python2.7返回列表，而在3.x中返回迭代器。
#   当然迭代器大多数都是比较好的选择，更加pythonic，但是也有缺点，就是只能遍历一次。
#   示例：http://www.infoq.com/cn/articles/instagram-pycon-2017
'''

'''
# 陷阱十: ++i 和 --i
# 这个陷阱主要是坑来自C、C++背景的同学。简单来说，
# ++i是对i取两次正号，--i是对i取两次负号，运算完之后i的值不变。
'''

'''
# 陷阱十一： __setattr__ __getattr__ __getattribute__
# Python中有大量的magic method（形似__xx__），其中许多跟属性访问有关，
# 比如__get__， __set__，__delete_，__getattr__, __setattr__, __delattr__, __getattribute__。
# 前三个跟descriptor相关，详细可参见：http://www.cnblogs.com/xybaby/p/6266686.html。
# 坑爹的是，__getattr__与__setattr__相差很大，在 http://www.cnblogs.com/xybaby/p/6270551.html 一文中有详细介绍。
# 简单说来，__setattr__与__getattribute__是对应的，都是修改python默认的属性修改、查找机制，
# 而__getattr__只是默认查找机制无法找到属性的时候才会调用，__setattr__应该叫__setattribute__才恰当！
'''

'''
# 陷阱十二: gil
# 以GIL结尾，因为gil是Python中大家公认的缺陷！
# GIL 的全程为 Global Interpreter Lock ，意即全局解释器锁。
# 在 Python 语言的主流实现 CPython 中，GIL 是一个货真价实的全局线程锁，在解释器解释执行任何 Python 代码时，都需要先获得这把锁才行，在遇到 I/O 操作时会释放这把锁。
# 如果是纯计算的程序，没有 I/O 操作，解释器会每隔 100 次操作就释放这把锁，让别的线程有机会执行（这个次数可以通过 sys.setcheckinterval 来调整）。
# 所以虽然 CPython 的线程库直接封装操作系统的原生线程，但 CPython 进程做为一个整体，同一时间只会有一个获得了 GIL 的线程在跑，其它的线程都处于等待状态等着 GIL 的释放。
# 这也就解释了我们上面的实验结果：虽然有两个死循环的线程，而且有两个物理 CPU 内核，但因为 GIL 的限制，两个线程只是做着分时切换，总的 CPU 占用率还略低于 50％。
# 在介绍Python中的线程之前，先明确一个问题，Python中的多线程是假的多线程！ 
# 注意python中并没有实现线程调度，python的多线程调度完全依赖于操作系统。所以python多线程编程中没有线程优先级等概念。
# 从其他语言过来的同学可能看到python用threading模块，拿过来就用，结果发现效果不对啊，然后就会喷，什么鬼
# https://www.cnblogs.com/MnCu8261/p/6357633.html
'''

'''
# 陷阱十三: 
'''


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
