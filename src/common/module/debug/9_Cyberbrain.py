#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://mp.weixin.qq.com/s?__biz=MzA5MTkxNTMzNg==&mid=2650287277&idx=1&sn=12a0a985dc07fcc3c45fac0531806b86&chksm=8879998abf0e109c91f611f6269c58d1b748c4a7e12f9fe224b3b81a10ae21bf398563b0facb&mpshare=1&scene=23&srcid=04157YYVGl0WfYqpxHzY0gzu&sharer_sharetime=1618441759592&sharer_shareid=c96eba000fd40506e91f8bd3e9af6621#rd
# Python 调试神器 Cyberbrain
# 这款名叫Cyberbrain（赛博大脑）的强大的工具，最亮眼的功能，是回溯代码中的变量更改历史，查看程序执行的状态。
# pip install Cyberbrain
from cyberbrain import trace


@trace
def container():
    x = list(range(100))
    return x


def main():
    container()


if __name__ == "__main__":
    main()
