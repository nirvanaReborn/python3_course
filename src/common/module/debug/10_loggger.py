#!/usr/bin/env python
# -*- coding:utf-8 -*-

#
#


# https://www.cnblogs.com/nancyzhu/p/8551506.html
# class Logger(object):
#     def __init__(self, filename,
#                  fmt='%(asctime)s - %(module)s - %(lineno)d - %(levelname)s : %(message)s'):
#         logging.config.fileConfig(log_config_file)
#         self.logger = logging.getLogger("rotatingFileLogger")
#         self.logger.setLevel(self.logger.level)  # 设置日志级别
#         format_str = logging.Formatter(fmt)  # 设置日志格式
#
#         consoleHandler = logging.StreamHandler()  # 往屏幕上输出
#         consoleHandler.setFormatter(format_str)  # 设置屏幕上显示的格式
#
#         fileHandler = logging.handlers.RotatingFileHandler(filename=filename, mode='a', maxBytes=1048576, backupCount=5, encoding='UTF-8')
#         fileHandler.setFormatter(format_str)  # 设置文件里写入的格式
#
#         self.logger.addHandler(consoleHandler)  # 把对象加到logger里
#         self.logger.addHandler(fileHandler)  # 把对象加到logger里


def main():
    pass


if __name__ == "__main__":
    main()
