#!/usr/bin/env python3
# -*- coding:utf-8 -*-


def test_1(level=0):
    if level < 1:
        raise Exception("Invalid level!", level)
        # The code below to this would not be executed
        # if we raise the exception


# 抛出自定义错误
def test_2():
    class NetworkError(Exception):
        def __init__(self, error_info):
            self.error_info = error_info

        def __str__(self):
            return str(self.error_info) + ',网络错误。'

    # 在你定义以上类后，你可以触发该异常
    def test_demo():
        try:
            raise NetworkError("Bad hostname")
        except NetworkError as e:
            print(e.args)

    test_demo()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
