#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Python里三个最高逼格的调试神器(不借助IDE)
# https://blog.csdn.net/wade1203/article/details/102927380
# python debug 工具——better_exceptions
'''
better-exceptions是一个Python第三方库，作者对他的定义是“使异常信息更加美观和详尽”。
在正式使用之前先说下这个库的安装：
第一步，使用pip install better_exceptions安装better-exceptions库；
第二步，在控制台使用export BETTER_EXCEPTIONS=1(Linux / OSX)或setx BETTER_EXCEPTIONS 1(Windows)设置环境变量。


https://blog.csdn.net/yahamatarge/article/details/88963674
https://www.ctolib.com/better-exceptions.html
http://blog.nsfocus.net/python-better-exceptions/
'''
import better_exceptions


better_exceptions.MAX_LENGTH = None  # 全部输出
# better_exceptions.MAX_LENGTH = 200 # 屏蔽这些“垃圾”信息


def divisionnumber(number, div):
    for i in range(div):
        print(number / i)


def main():
    divisionnumber(10, 10)


if __name__ == "__main__":
    main()
