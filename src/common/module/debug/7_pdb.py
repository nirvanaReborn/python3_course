#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Python里三个最高逼格的调试神器(不借助IDE)
# https://blog.csdn.net/wade1203/article/details/102927380
'''
调试是开发过程中不可避免的一个环节，在Python中我们使用print、logging、assert等方法进行调试既简单又实用，但毕竟有其局限性。
今天这篇文章为大家带来三个工具，其中有Python的内置模块也有第三方库，它们提供了调试代码所需的大部分常用功能，将极大的提升我们的开发和bug排除效率。
pdb是Python中的一个内置模块，启用pdb后可以对代码进行断点设置和跟踪调试。
在终端中输入 python -m pdb pdb_test.py 命令，进入pdb的调试模式：
例如:
l可以查看所有代码，
n是执行下一步代码，
s是单步执行子函数中的代码，
p可以查看当前变量等
c命令就会直接跳转到下一个断点位置
'''
import pdb


def countnumber(number):
    for i in range(number):
        print(i)
        pdb.set_trace()  # 在代码中设置断点


def main():
    countnumber(10)


if __name__ == "__main__":
    main()
