#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# PrettyErrors是一个精简Python错误信息的工具，特点是界面十分简洁友好。
# 它最显著的功能是支持在终端进行彩色输出，标注出文件栈踪迹，发现错误信息，
# 过滤掉冗余信息，提取出关键部分，并且进行彩色标注，从而提高开发者的效率。
# 开源地址：https://github.com/onelivesleft/PrettyErrors
# pip install pretty_errors
import pretty_errors

pretty_errors.configure(
    separator_character='*',
    filename_display=pretty_errors.FILENAME_EXTENDED,
    line_number_first=True,
    display_link=True,
    lines_before=5,
    lines_after=2,
    line_color=pretty_errors.RED + '> ' + pretty_errors.default_config.line_color,
    code_color='  ' + pretty_errors.default_config.line_color,
    truncate_code=True,
    display_locals=True
)

pretty_errors.blacklist('c:/python')


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
