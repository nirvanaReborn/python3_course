#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/jianhong1990/article/details/45844759
# RPDB-让PDB（Python调试工具）支持远程调试功能
# https://pypi.org/project/rpdb/#files
# pip install rpdb
from rpdb import Rpdb
from random import randint
from time import sleep


def add(i, j):
    r = i + j
    return r


def main():
    pdb = Rpdb()
    # pdb = Rpdb(9999) # debug port:9999
    pdb.set_trace()
    while True:
        i = randint(1,10)
        j = randint(1,10)
        r = add(i, j)
        print(r)
        sleep(1)


if __name__ == "__main__":
    main()
