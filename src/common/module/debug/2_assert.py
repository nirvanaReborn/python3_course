#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 断言assert
# 在运行Python 时传入-O 选项，可以禁用断言。
'''
在代码中，assert语句包含以下部分：
• assert 关键字
• 条件（即求值为True 或False 的表达式）
• 逗号
• 当条件为False 时显示的字符串
'''


def main():
    podBayDoorStatus = 'open'
    assert podBayDoorStatus == 'open', 'The pod bay doors need to be "open".'

    podBayDoorStatus = 'I\'m sorry, Dave. I\'m afraid I can\'t do that.'
    assert podBayDoorStatus == 'open', 'The pod bay doors need to be "open".'


if __name__ == "__main__":
    main()
