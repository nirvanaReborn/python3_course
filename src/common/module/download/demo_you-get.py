#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/qinxiaoqin/p/7152025.html
# https://github.com/soimort/you-get/wiki/%E4%B8%AD%E6%96%87%E8%AF%B4%E6%98%8E
# https://www.jianshu.com/p/9e9e3599cf35
# pip3 install you-get
import os
import subprocess
import itertools
import pprint

# os.path.expanduser(r"~\AppData\Roaming\Python\Python37\site-packages\you_get\extractors")
exe_path = os.path.expanduser(r"~\AppData\Roaming\Python\Python37\Scripts\you-get.exe")


def execute_cmd(list_cmd):
    list_result = []
    child = subprocess.Popen(list_cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1)

    for line in iter(child.stdout.readline, b''):
        list_result.append(str(line, encoding='utf-8'))
        # print(str(line, encoding='utf-8'))
    child.stdout.close()
    child.wait()  # 等待子进程结束
    return list_result


def test_1(list_text):
    list_1D = []
    for fileLine in list_text:
        if str(fileLine).strip().startswith("- format:"):
            format = str(fileLine).replace("- format:", "").strip()
            list_1D.append(format)
            continue
        if str(fileLine).strip().startswith("container:"):
            format = str(fileLine).replace("container:", "").strip()
            list_1D.append(format)
            continue
        if str(fileLine).strip().startswith("quality:"):
            format = str(fileLine).replace("quality:", "").strip()
            list_1D.append(format)
            continue
        if str(fileLine).strip().startswith("size:"):
            format = str(fileLine).replace("size:", "").strip()
            list_1D.append(format)
            continue
    return list_1D


def get_format(source_url):
    list_2D = []
    list_cmd = [
        exe_path,
        # "--debug",
        "-i",
        source_url
    ]
    list_result = execute_cmd(list_cmd)
    pprint.pprint(list_result)
    it = iter(list_result)
    for i, fileLine in enumerate(list_result):
        if str(fileLine).strip().startswith("- format:"):
            list_1D = test_1(list_result[i:i + 5])
            if list_1D:
                list_2D.append(list_1D)
                next(itertools.islice(it, 4, 4), None)  # 类似continue(5)
    pprint.pprint(list_2D)
    try:
        row = int(input("Enter a positive integer to choice: "))
        col = int(input("Enter a positive integer to choice: "))
        format = list_2D[row][col]
    except:
        row = 0
        col = 0
        format = None
    return format


def main():
    source_url = r"https://www.ixigua.com/6968084068630183949"
    format = get_format(source_url)
    list_cmd = [exe_path,
                "--format=%s" % format if format else " ",
                "-o", r"D:\%s\Videos" % os.environ['USERNAME'],
                source_url]
    execute_cmd(list_cmd)


if __name__ == "__main__":
    main()
