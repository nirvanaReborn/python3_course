#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 阿里云盘是一款云存储服务，可以将文件存储在云端，并随时随地访问和分享。
# 它提供了多种存储方式，如文件存储、对象存储、NAS存储等。
# 阿里云盘的官方网站是：https://www.aliyun.com/product/oss
# 以下是一些关于阿里云盘的代码示例：


import oss2


# 上传文件到阿里云盘
def upload():
    # 阿里云盘的AccessKey ID和AccessKey Secret
    access_key_id = '<your-access-key-id>'
    access_key_secret = '<your-access-key-secret>'

    # 阿里云盘的Endpoint和Bucket名称
    endpoint = 'oss-cn-hangzhou.aliyuncs.com'
    bucket_name = '<your-bucket-name>'

    # 创建阿里云盘的Bucket对象
    auth = oss2.Auth(access_key_id, access_key_secret)
    bucket = oss2.Bucket(auth, endpoint, bucket_name)

    # 上传文件到阿里云盘
    object_key = 'example.txt'  # 文件在阿里云盘中的名称
    local_file = '/path/to/example.txt'  # 本地文件路径
    bucket.put_object_from_file(object_key, local_file)


# 下载文件从阿里云盘
def download():
    # 阿里云盘的AccessKey ID和AccessKey Secret
    access_key_id = '<your-access-key-id>'
    access_key_secret = '<your-access-key-secret>'

    # 阿里云盘的Endpoint和Bucket名称
    endpoint = 'oss-cn-hangzhou.aliyuncs.com'
    bucket_name = '<your-bucket-name>'

    # 创建阿里云盘的Bucket对象
    auth = oss2.Auth(access_key_id, access_key_secret)
    bucket = oss2.Bucket(auth, endpoint, bucket_name)

    # 下载文件从阿里云盘
    object_key = 'example.txt'  # 文件在阿里云盘中的名称
    local_file = '/path/to/example.txt'  # 本地文件路径
    bucket.get_object_to_file(object_key, local_file)


def main():
    upload()
    download()


if __name__ == "__main__":
    main()
