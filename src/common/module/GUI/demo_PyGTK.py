#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://zhuanlan.zhihu.com/p/52809594
# https://iowiki.com/pygtk/pygtk_quick_guide.html
# https://python-gtk-3-tutorial.readthedocs.io/en/latest/
# 在老版本的PyGTK2中, 我们或许可以直接import gtk或者pygtk, 但是gtk和pytk模块仅适用于Python 2。
# 对于Python 3，需要通过gi (GObject Introspection)模块使用Gtk+。
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


def test_1():
    win = Gtk.Window()
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
