#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.programcreek.com/python/index/322/win32gui
# https://www.jb51.net/python/3110751hw.htm
# pywin32是一个第三方模块库，主要的作用是方便python开发者快速调用windows API的一个模块库。
# 同时pywin32也是绝大部分windows上第三方python模块库的前提，
# 例如wmi，如果没有安装pywin32是无法正常使用wmi这个三方模块库的。
# 安装包里有个PyWin32.chm文件，安装到D:\ProgramFiles\Anaconda3\Lib\site-packages目录下的。

"""
# Python安装pywin32之后，出现ModuleNotFoundError: No module named 'win32api' 问题?
# cd %PYTHON_INSTALL_DIR% && "%PYTHON_INSTALL_DIR%\python.exe" Scripts/pywin32_postinstall.py -install
"""

# pip install pywin32
import win32api
import win32con
import win32gui
import win32clipboard as w


# https://www.cnblogs.com/darkspr/p/9868761.html
def test_1():
    # 查找窗口句柄
    win32gui.FindWindow()
    # 查找指定窗口的菜单
    win32gui.GetMenu()
    # 查找某个菜单的子菜单
    win32gui.GetSubMenu()
    # 获得子菜单的ID
    win32gui.GetMenuItemID()
    # 获得某个子菜单的内容
    win32gui.GetMenuItemInfo()
    # 给句柄发送通知（点击事件）
    win32gui.PostMessage()


def test_2():
    def get_file_version(dllPath):
        info = win32api.GetFileVersionInfo(dllPath, '\\')
        ms = info['FileVersionMS']
        ls = info['FileVersionLS']
        return (win32api.HIWORD(ms), win32api.LOWORD(ms),
                win32api.HIWORD(ls), win32api.LOWORD(ls))

    def getDllVersion(dllPath):
        info = win32api.GetFileVersionInfo(dllPath, '\\')
        return '{:d}.{:d}.{:d}.{:d}'.format(
            info['FileVersionMS'] // 65536,
            info['FileVersionMS'] % 65536,
            info['FileVersionLS'] // 65536,
            info['FileVersionLS'] % 65536
        )

    dllPath = r'D:\ProgramFiles\Anaconda3\python.exe'
    print(get_file_version(dllPath))
    print(getDllVersion(dllPath))


def test_3():
    def getFileProperties(fname):
        """
        Read all properties of the given file return them as a dictionary.
        """
        propNames = ('Comments', 'InternalName', 'ProductName',
                     'CompanyName', 'LegalCopyright', 'ProductVersion',
                     'FileDescription', 'LegalTrademarks', 'PrivateBuild',
                     'FileVersion', 'OriginalFilename', 'SpecialBuild')

        props = {'FixedFileInfo': None, 'StringFileInfo': None, 'FileVersion': None}

        try:
            # backslash as parm returns dictionary of numeric info corresponding to VS_FIXEDFILEINFO struc
            fixedInfo = win32api.GetFileVersionInfo(fname, '\\')
            props['FixedFileInfo'] = fixedInfo
            props['FileVersion'] = "%d.%d.%d.%d" % (fixedInfo['FileVersionMS'] / 65536,
                                                    fixedInfo['FileVersionMS'] % 65536,
                                                    fixedInfo['FileVersionLS'] / 65536,
                                                    fixedInfo['FileVersionLS'] % 65536)

            # \VarFileInfo\Translation returns list of available (language, codepage)
            # pairs that can be used to retreive string info. We are using only the first pair.
            lang, codepage = win32api.GetFileVersionInfo(fname, '\\VarFileInfo\\Translation')[0]

            # any other must be of the form \StringfileInfo\%04X%04X\parm_name, middle
            # two are language/codepage pair returned from above

            strInfo = {}
            for propName in propNames:
                strInfoPath = u'\\StringFileInfo\\%04X%04X\\%s' % (lang, codepage, propName)
                # print(strInfoPath.decode())
                strInfo[propName] = win32api.GetFileVersionInfo(fname, strInfoPath.decode())

            props['StringFileInfo'] = strInfo
        except:
            pass

        return props

    dllPath = r'D:\ProgramFiles\Anaconda3\python.exe'
    print(getFileProperties(dllPath))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
