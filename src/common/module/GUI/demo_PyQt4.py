#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 由于PyQt工具包比普通的Python扩展类库要复杂得多，所以不能直接pip install PyQt4或者easy_install PyQt4，
# 需要上官网去下专门的安装包：
# 下载地址：https://riverbankcomputing.com/software/pyqt/download

# 下载地址：http://www.lfd.uci.edu/~gohlke/pythonlibs/
# 下载 PyQt4‑4.11.4‑cp36‑cp36m‑win_amd64.whl
# 下载 PyQt4-4.11.4-cp36-cp36m-win32.whl

# http://www.linuxidc.com/Linux/2012-06/63652.htm
# 由于在实际的工作中需要使用到Python来进行GUI的开发，基于python的GUI开发，只是去考虑具体使用依赖那个GUI库来进行开发。
# GUI库的选择：
# 1、TKinter，python配备的标准gui库，但是功能比较弱，似乎只适合开发非常简单的界面。
# 2、Wxpython，目前官方网站还没有release针对python3.0以上版本的库，虽然在国外的博客上有针对python3的版本。
# 3、Pyqt，Pyqt基于QT,在GUI开发上可以参考QT的开发 。
# 对比了Tkinter,Wxpython,Pyqt之后，选择使用Pyqt来进行GUI的开发，PyQt基于GPL许可开发。


from PyQt4 import QtGui, QtCore
import sys


def test_2():
    app = QtGui.QApplication(sys.argv)
    b = QtGui.QPushButton("Hello Kitty!")
    b.show()
    app.connect(b, QtCore.SIGNAL("clicked()"), app, QtCore.SLOT("quit()"))
    app.exec_()


def test_1():
    # Creat a simple window
    app = QtGui.QApplication(sys.argv)
    first_window = QtGui.QWidget()
    first_window.resize(400, 300)
    first_window.move(300, 300)
    first_window.setWindowTitle("我的第一个程序")
    first_window.show()
    sys.exit(app.exec_())

def main():
    choice = input("Enter a positive integer to choice: ")
    if choice == "1" :
        test_1()
    elif choice == "2":
        test_2()
    # elif choice == "3":
    #     test_3()
    # elif choice == "4":
    #     test_4()
    # elif choice == "5":
    #     test_5()
    # elif choice == "6":
    #     test_6()
    else:
        print("未知命令", choice)


if __name__ == "__main__":
    main()