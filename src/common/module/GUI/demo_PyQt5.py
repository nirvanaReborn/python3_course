#!/usr/bin/env python
# -*- coding:utf-8 -*-

# python3.5
# https://zh.osdn.net/projects/sfnet_pyqt/releases/  下载  PyQt5-5.6-gpl-Py3.5-Qt5.6.0-x32-2.exe


# http://blog.csdn.net/zhulove86/article/details/52298203
# http://bbs.fishc.com/thread-59816-1-1.html
# http://blog.csdn.net/u013232740/article/details/46973595
# Pyqt5系列(一)-Pyqt5的安装
# 由于在实际的工作中需要使用到Python来进行GUI的开发，基于python的GUI开发，只是去考虑具体使用依赖那个GUI库来进行开发。
# GUI库的选择：
# 1、TKinter，python配备的标准gui库，但是功能比较弱，似乎只适合开发非常简单的界面。
# 2、Wxpython，目前官方网站还没有release针对python3.0以上版本的库，虽然在国外的博客上有针对python3的版本。
# 3、Pyqt，Pyqt基于QT,在GUI开发上可以参考QT的开发 。
# 对比了Tkinter,Wxpython,Pyqt之后，选择使用Pyqt来进行GUI的开发，PyQt基于GPL许可开发。

from PyQt5 import QtGui, QtWidgets
import sys


def test_0():
    from PyQt5.Qt import PYQT_VERSION_STR
    print("PyQt5 Version is: {}".format(PYQT_VERSION_STR))


def test_1():
    from PyQt5.QtCore import QT_VERSION_STR
    from sip import SIP_VERSION_STR

    app = QtWidgets.QApplication(sys.argv)
    print("Qt5 Version Number is: {0}".format(QT_VERSION_STR))

    print("Sip Version is: {}".format(SIP_VERSION_STR))
    sys.exit(app.exec_())


def test_2():
    app = QtWidgets.QApplication(sys.argv)
    first_window = QtWidgets.QWidget()
    first_window.resize(400, 300)
    first_window.move(300, 300)
    first_window.setWindowTitle("我的第一个程序")
    first_window.show()
    sys.exit(app.exec_())


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
