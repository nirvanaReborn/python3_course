#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
tkinter 官方网址：https://docs.python.org/3.7/library/tk.html
或者：http://effbot.org/tkinterbook/ （相对规整，适合初学者查找）

# http://www.runoob.com/python/python-gui-tkinter.html
# Python GUI编程(Tkinter)

tk官网的教程学习：http://www.tkdocs.com/tutorial/firstexample.html

# https://www.cnblogs.com/aland-1415/p/6849193.html
# tkinter模块常用参数(python3)

# https://my.oschina.net/TyLucifer/blog/112961
# Python Tkinter参考资料之（通用控件属性）(资料来源——《Python与Tkinter编程》附录B)

# 详细的窗体的文本 : http://effbot.org/tkinterbook/tkinter-classes.htm
# style组件外观属性: http://effbot.org/tkinterbook/tkinter-widget-styling.htm

Python 提供了多个图形开发界面的库，几个常用 Python GUI 库如下：
Tkinter： Tkinter 模块(Tk 接口)是 Python 的标准 Tk GUI 工具包的接口 .
          Tk 和 Tkinter 可以在大多数的 Unix 平台下使用,同样可以应用在 Windows 和 Macintosh 系统里。
          Tk8.0 的后续版本可以实现本地窗口风格,并良好地运行在绝大多数平台中。

wxPython：wxPython 是一款开源软件，是 Python 语言的一套优秀的 GUI 图形库，
          允许 Python 程序员很方便的创建完整的、功能键全的 GUI 用户界面。

Jython：Jython 程序可以和 Java 无缝集成。除了一些标准模块，Jython 使用 Java 的模块。
        Jython 几乎拥有标准的Python 中不依赖于 C 语言的全部模块。
        比如，Jython 的用户界面将使用 Swing，AWT或者 SWT。
        Jython 可以被动态或静态地编译成 Java 字节码。

Tkinter 是 Python 的标准 GUI 库。
Python 使用 Tkinter 可以快速的创建 GUI 应用程序。
由于 Tkinter 是内置到 python 的安装包中、只要安装好 Python 之后就能 import Tkinter 库、
而且 IDLE 也是用 Tkinter 编写而成、对于简单的图形界面 Tkinter 还是能应付自如。
'''

import tkinter


def test_1():
    '''
    title: 设置窗口标题
    geometry: 设置窗口大小
    resizable():设置窗口是否可以变化长 宽
    '''
    root = tkinter.Tk()  # 创建窗口对象的背景色
    root.title("hello world")
    # 通过geometry(‘wxh±x±y’)设置主窗口位置和大小。
    # w 为宽度，h 为高度。
    # +x 表示距屏幕左边的距离；-x 表示距屏幕右边的距离；
    # +y表示距屏幕上边的距离；-y 表示距屏幕下边的距离。
    root.geometry('1000x618+100+150')  # 是x 不是*
    root.resizable(width=False, height=True)  # 宽不可变, 高可变,默认为True
    root.mainloop()  # 进入死循环,让窗口一直显示


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
