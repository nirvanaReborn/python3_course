#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://github.com/JLUNeverMore/easy_2048-in-200-lines/blob/master/2048.py
# wxPython API文档：https://docs.wxpython.org/

# https://www.jianshu.com/p/33081a77b2c8
# wxFormBuilder是一款基于wxWidgets的GUI可视化编辑工具，可用于Python的GUI设计。
# wxFormBuilder项目GitHUb：www.wxformbuilder.org/

# pip install wxPython
import wx


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
