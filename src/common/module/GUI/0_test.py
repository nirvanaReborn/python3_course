#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.douban.com/group/topic/120805177
# Python开发者必备的11个Python GUI库
'''
1. Tkinter
是一个轻量级的跨平台图形用户界面（GUI）开发工具，是Tk图形用户界面工具包标准的Python接口，
目前可以运行于绝大多数的Unix平台、Windows和Macintosh系统。

2. Flexx
许多Python GUI库基于以其他语言编写的库，例如用C++编写的库有wxWidgets和libavg。
但Flexx是用Python创建的，使用Web技术，也正因为如此 Flexx 是跨平台的。只需要有 Python 和浏览器就可以运行。

3. CEF Python
该框架基于Google Chromium，面向Windows，MAC OS和Linux，其主要用于在第三方应用程序中嵌入式浏览器的使用上。

4. Dabo
Dabo是一个跨平台的应用程序开发框架，基于wxpython的再封装库。它提供数据库访问，商业逻辑以及用户界面。

5. Kivy
Kivy基于OpenGL ES 2，能够让使用相同源代码创建的程序能跨平台运行。这个框架是事件驱动的，基于主循环，非常适合开发游戏。

6. Pyforms
Pyforms是一个用于开发GUI应用程序的Python 2.7 / 3.x跨环境运行框架。

7. PyGObject
通过PyGObject，你可以为GNOME项目编写Python应用程序，你也可以使用GTK +编写Python应用程序。

8. PyQt
Qt是一个跨平台的框架。它是用C ++编写的。这是一个非常全面的库。它包含许多工具和API。
它被广泛应用于许多行业。它涵盖了很多平台。PyQt是Qt库的Python版本。PyQt3支持Qt1到Qt3。 PyQt4支持Qt4。

9. PySide
Qt(cute)是使用“C++”语言编写的应用程序/用户界面(UI)框架。
“PySide”是“Qt”的封装。与PySide的不同之处在于PyQt可以商用。

10. PyGUI
PyGUI的一个主要目的就是尽量减少Python应用与平台底层GUI之间的代码量，面向Unix，Macintosh和Windows平台

11. wxPython
wxPython 是 Python 语言的一套优秀的 GUI 图形库，允许 Python 程序员很方便的创建完整的、功能键全的 GUI 用户界面。
'''


def test_1():
    pass


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
