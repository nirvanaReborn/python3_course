#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/article/7425644140065227264
# Dear PyGui是一个便于使用、功能强大的Python GUI框架。
# 但是它与其他的Python GUI却有着根本上的不同。
# 它使用了即时模式范式和计算机的GPU来实现动态界面。
# 即时模式范式在视频游戏中非常流行，这意味着它的动态GUI不需要保留任何数据，而是逐帧独立绘制的。同时，它还使用GPU来建构动态界面。
# Dear PyGui还可以绘图、创建主题、创建2D游戏，还拥有一些小工具，比如说内置文档、日志记录、源代码查看器之类的，这些小工具可以协助App的开发。
# 支持它的系统有：Windows 10（DirectX 11），Linux（OpenGL 3）和macOS（Metal）等。
# 开源地址：https://github.com/hoffstadt/DearPyGui
# pip install dearpygui
import dearpygui.dearpygui as dpg


def save_callback():
    print("Save Clicked")


def test_1():
    with dpg.window(label="Example Window"):
        dpg.add_text("Hello world")
        dpg.add_button(label="Save", callback=save_callback)
        dpg.add_input_text(label="string")
        dpg.add_slider_float(label="float")

    dpg.setup_viewport()
    dpg.start_dearpygui()


def test_2():
    dpg.create_context()
    dpg.create_viewport()
    dpg.setup_dearpygui()

    with dpg.window(label="Example Window"):
        dpg.add_text("Hello world")
        dpg.add_button(label="Save", callback=save_callback)
        dpg.add_input_text(label="string")
        dpg.add_slider_float(label="float")

    dpg.show_viewport()
    dpg.start_dearpygui()
    dpg.destroy_context()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
