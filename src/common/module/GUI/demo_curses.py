#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# http://python.jobbole.com/87519/
# python curses使用

# curses库不支持Windows
# 需要跳转到官网下载curses库的whl文件：https://www.lfd.uci.edu/~gohlke/pythonlibs/#curses

# python 中curses封装了c语言的curses，把c中复杂部分简单化，
# 比如addstr(),mvaddstr(),mvwaddstr()合并成了一个addstr()方法。

import curses

# 在任何代码执行前都先要初始化curses。初始化操作就是调用initscr()函数，如下。
# 该函数根据不同设备返回一个window对象代表整个屏幕，这个window对象通常叫做stdscr，和c语言报错一致。
stdscr = curses.initscr()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
