#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
https://zhuanlan.zhihu.com/p/268163358
https://www.panda3d.org/
https://blog.csdn.net/keny88888/article/details/123973681

Panda3D 是一个用于 Python 和 C++ 程序的 3D 渲染和游戏开发框架

Panda3D 是一个用 C++ 编写的强大的 3D 引擎，具有一套完整的 Python 绑定。
与其他引擎不同，这些绑定是自动生成的，这意味着它们始终是最新的和完整的：
引擎的所有功能都可以通过 Python 进行控制。
所有主要的 Panda3D 应用程序都是用 Python 编写的，这是使用该引擎的预期方式。

Panda3D 现在支持自动着色器生成，这意味着您现在可以使用法线贴图、光泽贴图、
发光贴图、HDR、卡通着色等，而无需编写任何着色器。

Panda3D 是一个现代引擎，支持着色器、模板和渲染到纹理等高级功能。
Panda3D 的不同寻常之处在于它强调学习曲线短、开发速度快、稳定性和鲁棒性极强。
Panda3D 是在 Windows、Linux 或 macOS 下运行的免费软件。
'''

from direct.showbase.ShowBase import ShowBase


def test_1():
    class MyApp(ShowBase):
        def __init__(self):
            super(MyApp, self).__init__()

    app = MyApp()
    app.run()


def test_2():
    class MyApp(ShowBase):
        def __init__(self):
            super(MyApp, self).__init__()

            # 加载场景模型（场景树）
            self.scene = self.loader.loadModel("models/environment")
            # 将render设为场景模型的父节点
            self.scene.reparentTo(self.render)

            # 改变场景大小，并设置镜头初始位置
            self.scene.setScale(0.25, 0.25, 0.25)
            self.scene.setPos(-8, 42, 0)

    app = MyApp()
    app.run()


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
