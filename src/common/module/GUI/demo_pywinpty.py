#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/gitblog_01188/article/details/141799492
# WinPTY是一个Windows系统上的伪终端（Pseudo Terminal）工具。
# 它为Windows提供了类UNIX终端的功能，允许用户在Windows上使用像Bash、SSH等工具。
# pywinpty是一个Python库，它提供了在Python中使用WinPTY的功能。
# pip install pywinpty
from winpty import PtyProcess


def test_1():
    # 启动一个 Python 进程
    proc = PtyProcess.spawn('python')

    # 向进程发送命令
    proc.write('print("hello world")\r\n')
    proc.write('exit()\r\n')

    # 读取进程输出
    while proc.isalive():
        print(proc.readline())


# 自动化测试
def test_2():
    proc = PtyProcess.spawn('C:\\path\\to\\your\\test_script.py')
    proc.write('run_tests()\r\n')

    while proc.isalive():
        print(proc.readline())


def test_3():
    import socket
    # 假设有一个 TCP 服务器在监听
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('127.0.0.1', 12345))

    proc = PtyProcess.spawn('cmd.exe')

    while True:
        command = s.recv(1024).decode()
        if command == 'exit':
            break
        proc.write(command + '\r\n')
        response = proc.readline()
        s.send(response.encode())

    s.close()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
