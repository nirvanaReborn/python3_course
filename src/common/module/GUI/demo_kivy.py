#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Kivy是Python的跨平台GUI库，不仅支持windows，macOS ，linux，还支持android和iOS。
# https://kivy.org/#home
# https://www.jianshu.com/p/f95576c03f5b
# https://zhuanlan.zhihu.com/p/63688456
# https://tqraf.cn/2019/10/hfutcommunity-kivymd.html
# python -m pip install docutils pygments pypiwin32 kivy.deps.sdl2 kivy.deps.glew kivy.deps.gstreamer kivy kivy_examples

import kivy
kivy.require('1.0.6') # 用你当前的kivy版本替换

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.label import Label


def test_1():
    class TestApp(App):
        def build(self):
            return Button(text='iPaoMi')

    TestApp().run()


def test_2():
    class MyApp(App):
        def build(self):
            return Label(text='Hello world')

    MyApp().run()


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
