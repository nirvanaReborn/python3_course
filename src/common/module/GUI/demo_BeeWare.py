#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# BeeWare 是一个基于 Python 构建的跨平台应用开发框架,
# 能够让 Python 编写的图形程序在 iOS, Android, Windows, MacOS, Linux, Web, 和 tvOS 上运行，看起来是很强大的。
# https://www.cnpython.com/pypi/beeware
# https://beeware.org/
# https://zhuanlan.zhihu.com/p/398126847
# 这边要特别注意一点（提前说）：briefcase new创建完项目之后，一定要使用briefcase dev运行一遍，这样才会生成后面toga.style的包

# pip install beeware
# 下载WIX工具集 并安装: https://wixtoolset.org/releases/


def test_1():
    pass


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
