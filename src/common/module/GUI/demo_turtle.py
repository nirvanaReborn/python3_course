#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/sandalphon4869/article/details/99443949
# Turtle是python内嵌的绘制线、圆以及其他形状（包括文本）的图形模块。
# Turtle库是Python内置的图形化模块，属于标准库之一.
# turtle的官方文档：https://docs.python.org/3/library/turtle.html

# https://blog.csdn.net/qq_39980514/article/details/77975874
# 少儿python海龟画图编程汉化增强版说明档案 (turtle库中文翻译）

# Python3安装turtle提示错误：Command "python setup.py egg_info" failed with error code 1
# https://oomake.com/question/178949
# https://blog.csdn.net/weixin_42429173/article/details/90034524
# ---------------------------------------------------------------
# https://blog.csdn.net/haeasringnar/article/details/78902683
# 画笔控制函数
# turtle.pendown() # 放下画笔
# turtle.penup() # 抬起画笔
# turtle.pensize(int) # 设置画笔宽度，值为整数型
# turtle.pencolor(color) # 设置画笔颜色
# turtle.speed(s) # 设置画笔颜色，为整数类型，且取值在1-10之间


# 方向控制函数
# turtle.right(angle) # 将画笔右转一定的角度
# turtle.left(angle) # #将画笔左转一定的角度
# turtle.setheading(angle) # 设定turtle箭头的方向为指定方向，0–东 90—北


# 运动控制函数
# turtle.forward(float) # 讲话比向前移动一定的角度
# turtle.backward(float) # 将画笔向后移动一定的角度
# turtle.goto(x,y) # 将画笔移动到一个指定的绝对坐标
# turtle.setx(x) # 设置画笔向x方向移动的距离，值为实数
# turtle.sety(y) # 设置画笔向y方向移动的距离，值为实数
# turtle.home() # 将画笔返回到原点
# turtle.circle(r,ext,steps=int) # 绘制一个设置半径和阶数的圆(设置之后会绘制多边形)
# turtle.dot(d,color) # 绘制一个指定直径的圆点，颜色为字符串类型
# turtle.undo() #取消最后一个图操作
# turtle.color(‘str’) # 设置画笔颜色，为字符串类型
# turtle.fillcolor(‘str’) # 设置填充颜色，为字符串类型
# turtle.begin_fill() # 结束填充
# turtle.end_fill() # 开始填充
# turtle.filling() # 返回填充状态，True表示填充，False表示没有填充
# turtle.clear() # 清除窗口所有内容
# turtle.reset() # 清除窗口，将状态和位置复位为初始值
# turtle.screensize(w,h) # 设置turtle显示的大小，并设置宽度和高度
# turtle.hideturtle() # 隐藏turtle箭头
# turtle.showturtle() # 显示turtle窗口
# turtle.done() # 使turtle窗口不会自动消失
# turtle.isvisible() # 如果turtle可见，返回turtle
# turtle.write(‘str’,font=(“Arial”,8,”normal”)) # 在turtle位置编写字符串s，字体由字体名、字体大小、字体类型三部分组成
# turtle.position() # 获取画笔的坐标，返回一个元组，值为浮点型
# ---------------------------------------------------------------
import turtle


def test_1():
    printer = turtle.Turtle()
    # printer = turtle.Pen()
    printer.showturtle()  # 显示箭头
    printer.write("高淇")  # 写字符串
    printer.forward(300)  # 前进300 像素
    printer.color("red")  # 画笔颜色改为red
    printer.left(90)  # 箭头左转90 度
    printer.forward(300)
    printer.goto(0, 50)  # 去坐标（0,50）
    printer.goto(0, 0)
    printer.penup()  # 抬笔。这样，路径就不会画出来
    printer.goto(0, 100)
    printer.pendown()  # 下笔。这样，路径就会画出来
    printer.circle(100)  # 画圆


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
