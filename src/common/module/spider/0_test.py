#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''https://www.csdn.net/tags/NtDaIg0sMjQ4NjItYmxvZwO0O0OO0O0O.html
requests对HTTP协议进行高度封装，支持非常丰富的链接访问功能。

scrapy很强大的爬虫框架，用于抓取网站并从其页面中提取结构化数据。可用于从数据挖掘到监控和自动化测试的各种用途。

pyspider是一个国人编写的强大的网络爬虫系统并带有强大的WebUI。

bs4-beautifulsoup4库是用于解析和处理HTML和XML。

Crawley是用来高速爬取对应网站的内容，支持关系和非关系数据库，数据可以导出为JSON、XML等。

Portia: Scrapy可视化爬取网页内容。

cola是分布式爬虫框架。
'''


def test_1():
    import requests
    from bs4 import BeautifulSoup


def test_2():
    import pyspider


def test_3():
    import scrapy


def test_4():
    import Portia


def test_5():
    import cola


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
