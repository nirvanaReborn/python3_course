#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://blog.csdn.net/m0_57021623/article/details/139757692
# 汇总Python发邮件的15个常用方式


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
