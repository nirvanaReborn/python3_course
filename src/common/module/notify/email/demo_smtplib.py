#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-smtp.html
# Python3 SMTP发送邮件
"""
正如HTTP 是计算机用来通过因特网发送网页的协议，简单邮件传输协议（SMTP）是用于发送电子邮件的协议。
SMTP 规定电子邮件应该如何格式化、加密、在邮件服务器之间传递，以及在你点击发送后，计算机要处理的所有其他细节。
但是，你并不需要知道这些技术细节，因为Python 的smtplib 模块将它们简化成几个函数。
SMTP 只负责向别人发送电子邮件。另一个协议，名为IMAP，负责取回发送给你的电子邮件。

提供商                      SMTP 服务器域名
Gmail                       smtp.gmail.com
Outlook.com/Hotmail.com     smtp-mail.outlook.com
Yahoo Mail                  smtp.mail.yahoo.com
AT&T                        smpt.mail.att.net (port 465)
Comcast                     smtp.comcast.net
Verizon                     smtp.verizon.net (port 465)
"""

import smtplib
from email.header import Header
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formataddr


# 需要在 localhost 运行一个邮件服务器。
def test_0():
    server = smtplib.SMTP('localhost')
    server.sendmail('soothsayer@example.org', 'jcaesar@example.org',
                    """To: jcaesar@example.org
                    From: soothsayer@example.org

                    Beware the Ides of March.
                    """)
    server.quit()


def test_1():
    sender = 'from@qq.com'
    receivers = ['727317813@qq.com']  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱

    # 三个参数：第一个为文本内容，第二个 plain 设置文本格式，第三个 utf-8 设置编码
    message = MIMEText('Python 邮件发送测试...', 'plain', 'utf-8')
    message['From'] = Header("菜鸟教程", 'utf-8')
    message['To'] = Header("测试", 'utf-8')
    message['Subject'] = Header('Python SMTP 邮件测试', 'utf-8')

    try:
        # 如果SMTP在你的本机上，你只需要指定服务器地址为 localhost 即可
        smtpObj = smtplib.SMTP('localhost')
        smtpObj.sendmail(sender, receivers, message.as_string())
        print("邮件发送成功")
    except smtplib.SMTPException:
        print("Error: 无法发送邮件")


def test_2():
    # 第三方 SMTP 服务
    mail_host = "smtp.XXX.com"  # 设置服务器
    mail_user = "XXXX"  # 用户名
    mail_pass = "XXXXXX"  # 口令

    sender = 'from@runoob.com'
    receivers = ['429240967@qq.com']  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱

    message = MIMEText('Python 邮件发送测试...', 'plain', 'utf-8')
    message['From'] = Header("菜鸟教程", 'utf-8')
    message['To'] = Header("测试", 'utf-8')

    subject = 'Python SMTP 邮件测试'
    message['Subject'] = Header(subject, 'utf-8')

    try:
        smtpObj = smtplib.SMTP()
        smtpObj.connect(mail_host, 25)  # 25 为 SMTP 端口号
        smtpObj.login(mail_user, mail_pass)
        smtpObj.sendmail(sender, receivers, message.as_string())
        print("邮件发送成功")
    except smtplib.SMTPException:
        print("Error: 无法发送邮件")


# 使用Python发送HTML格式的邮件
def test_3():
    sender = 'from@runoob.com'
    receivers = ['429240967@qq.com']  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱

    mail_msg = """
    <p>Python 邮件发送测试...</p>
    <p><a href="http://www.runoob.com">这是一个链接</a></p>
    """
    message = MIMEText(mail_msg, 'html', 'utf-8')
    message['From'] = Header("菜鸟教程", 'utf-8')
    message['To'] = Header("测试", 'utf-8')

    subject = 'Python SMTP 邮件测试'
    message['Subject'] = Header(subject, 'utf-8')

    try:
        smtpObj = smtplib.SMTP('localhost')
        smtpObj.sendmail(sender, receivers, message.as_string())
        print("邮件发送成功")
    except smtplib.SMTPException:
        print("Error: 无法发送邮件")


# Python 发送带附件的邮件
def test_4():
    sender = 'from@runoob.com'
    receivers = ['429240967@qq.com']  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱

    # 创建一个带附件的实例
    message = MIMEMultipart()
    message['From'] = Header("菜鸟教程", 'utf-8')
    message['To'] = Header("测试", 'utf-8')
    subject = 'Python SMTP 邮件测试'
    message['Subject'] = Header(subject, 'utf-8')

    # 邮件正文内容
    message.attach(MIMEText('这是菜鸟教程Python 邮件发送测试……', 'plain', 'utf-8'))

    # 构造附件1，传送当前目录下的 test.txt 文件
    att1 = MIMEText(open('test.txt', 'rb').read(), 'base64', 'utf-8')
    att1["Content-Type"] = 'application/octet-stream'
    # 这里的filename可以任意写，写什么名字，邮件中显示什么名字
    att1["Content-Disposition"] = 'attachment; filename="test.txt"'
    message.attach(att1)

    # 构造附件2，传送当前目录下的 runoob.txt 文件
    att2 = MIMEText(open('runoob.txt', 'rb').read(), 'base64', 'utf-8')
    att2["Content-Type"] = 'application/octet-stream'
    att2["Content-Disposition"] = 'attachment; filename="runoob.txt"'
    message.attach(att2)

    try:
        smtpObj = smtplib.SMTP('localhost')
        smtpObj.sendmail(sender, receivers, message.as_string())
        print("邮件发送成功")
    except smtplib.SMTPException:
        print("Error: 无法发送邮件")


# 在 HTML 文本中添加图片
def test_5():
    sender = 'from@runoob.com'
    receivers = ['429240967@qq.com']  # 接收邮件，可设置为你的QQ邮箱或者其他邮箱

    msgRoot = MIMEMultipart('related')
    msgRoot['From'] = Header("菜鸟教程", 'utf-8')
    msgRoot['To'] = Header("测试", 'utf-8')
    subject = 'Python SMTP 邮件测试'
    msgRoot['Subject'] = Header(subject, 'utf-8')

    msgAlternative = MIMEMultipart('alternative')
    msgRoot.attach(msgAlternative)

    mail_msg = """
    <p>Python 邮件发送测试...</p>
    <p><a href="http://www.runoob.com">菜鸟教程链接</a></p>
    <p>图片演示：</p>
    <p><img src="cid:image1"></p>
    """
    msgAlternative.attach(MIMEText(mail_msg, 'html', 'utf-8'))

    # 指定图片为当前目录
    fp = open('test.png', 'rb')
    msgImage = MIMEImage(fp.read())
    fp.close()

    # 定义图片 ID，在 HTML 文本中引用
    msgImage.add_header('Content-ID', '<image1>')
    msgRoot.attach(msgImage)

    try:
        smtpObj = smtplib.SMTP('localhost')
        smtpObj.sendmail(sender, receivers, msgRoot.as_string())
        print("邮件发送成功")
    except smtplib.SMTPException:
        print("Error: 无法发送邮件")


def test_6():
    my_sender = '429240967@qq.com'  # 发件人邮箱账号
    my_pass = 'xxxxxxxxxx'  # 发件人邮箱密码
    my_user = '429240967@qq.com'  # 收件人邮箱账号，我这边发送给自己

    def mail():
        ret = True
        try:
            msg = MIMEText('填写邮件内容', 'plain', 'utf-8')
            msg['From'] = formataddr(["FromRunoob", my_sender])  # 括号里的对应发件人邮箱昵称、发件人邮箱账号
            msg['To'] = formataddr(["FK", my_user])  # 括号里的对应收件人邮箱昵称、收件人邮箱账号
            msg['Subject'] = "菜鸟教程发送邮件测试"  # 邮件的主题，也可以说是标题

            server = smtplib.SMTP_SSL("smtp.qq.com", 465)  # 发件人邮箱中的SMTP服务器，端口是25
            server.login(my_sender, my_pass)  # 括号中对应的是发件人邮箱账号、邮箱密码
            server.sendmail(my_sender, [my_user, ], msg.as_string())  # 括号中对应的是发件人邮箱账号、收件人邮箱账号、发送邮件
            server.quit()  # 关闭连接
        except Exception:  # 如果 try 中的语句没有执行，则会执行下面的 ret=False
            ret = False
        return ret

    ret = mail()
    if ret:
        print("邮件发送成功")
    else:
        print("邮件发送失败")


def test_7():
    def test_outlook(dict_email):
        # 1.调用smtplib.SMTP()创建一个SMTP对象，传入域名作为一个字符串参数，传入端口作为整数参数。
        smtp_obj = smtplib.SMTP('smtp-mail.outlook.com', 587)
        # 如果smtplib.SMTP()调用不成功，你的SMTP 服务器可能不支持TLS 端口587。
        # 在这种情况下，你需要利用smtplib.SMTP_SSL()和465 端口，来创建SMTP 对象。
        # smtp_obj = smtplib.SMTP_SSL('smtp.gmail.com', 465)

        # 2.发送SMTP 的“Hello”消息,向SMTP 电子邮件服务器“打招呼”
        print(smtp_obj.ehlo())
        # (250, b'KL1PR0302CA0018.outlook.office365.com Hello [115.199.251.211]\nSIZE 157286400\n
        # PIPELINING\nDSN\nENHANCEDSTATUSCODES\nSTARTTLS\n8BITMIME\nBINARYMIME\nCHUNKING\nSMTPUTF8')
        # 如果在返回的元组中，第一项是整数250（SMTP 中“成功”的代码），则问候成功了。
        # 如果没有连接到因特网，Python 将抛出socket.gaierror: [Errno 11004] getaddrinfofailed 或类似的异常。

        # 3.开始TLS 加密
        # 如果要连接到465 端口（使用SSL），加密已经设置好了，你应该跳过这一步。
        print(smtp_obj.starttls())
        # (220, b'2.0.0 SMTP server ready')
        # starttls()让SMTP 连接处于TLS 模式。返回值220 告诉你，该服务器已准备就绪。

        # 4.登录到SMTP 服务器
        print(smtp_obj.login(dict_email["user"], dict_email["password"]))
        # (235, b'2.7.0 Authentication successful')
        # 如果密码不正确，Python 会抛出smtplib. SMTPAuthenticationError 异常。

        # 5.发送电子邮件
        # 电子邮件正文字符串必须以'Subject: \n'开头，作为电子邮件的主题行。
        # '\n'换行符将主题行与电子邮件的正文分开。
        # print(smtp_obj.sendmail(dict_email["user"], dict_email["send_to"],
        #                        'Subject:'+ dict_email["theme"]  + "\n"+ dict_email["email"]))
        message = MIMEText(dict_email["email"], 'plain', 'utf-8')
        message['Subject'] = Header(dict_email["Subject"], 'utf-8')
        # message['From'] = Header(dict_email["user"], 'utf-8') # 会报错
        # message['To'] = Header(dict_email["send_to"], 'utf-8') # 会报错
        message['From'] = dict_email["user"]
        message['To'] = dict_email["send_to"]
        print(smtp_obj.sendmail(dict_email["user"], dict_email["send_to"], message.as_string()))
        # {}
        # 返回值是一个字典。对于电子邮件传送失败的每个收件人，该字典中会有一个键值对。
        # 空的字典意味着对所有收件人已成功发送电子邮件。

        # 6.从SMTP 服务器断开
        print(smtp_obj.quit())
        # (221, b'2.0.0 Service closing transmission channel')
        # 返回值221 表示会话结束。

    dict_email = {
        "user": "*",
        "password": "*",
        "send_to": ["*", "*"],  # 多个收件人
        "Subject": "Python SMTP 邮件测试",
        "email": "Python 邮件发送测试...!",
    }

    test_outlook(dict_email)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
