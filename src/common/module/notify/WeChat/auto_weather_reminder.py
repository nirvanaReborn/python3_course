#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://mp.weixin.qq.com/s/m5Xp9SWuoY15bD4yFmNQHw
# 如何用Python向微信发送消息:每天早上给好友发送一条天气预报。

import time
from datetime import datetime

import itchat
import requests


# 获取天气数据
def get_weather():
    url = 'https://api.openweathermap.org/data/2.5/weather?q=北京&appid=YOUR_API_KEY&units=metric'
    response = requests.get(url)
    data = response.json()
    weather = data['weather'][0]['description']
    temperature = data['main']['temp']
    return f"今天的天气是 {weather}，温度是 {temperature}°C"


# 发送天气提醒
def send_weather_reminder():
    friend = itchat.search_friends(name='朋友A')[0]
    message = get_weather()
    itchat.send_msg(message, toUserName=friend['UserName'])


def main():
    # 登录微信
    itchat.auto_login(hotReload=True)

    # 每天早上8点发送天气提醒
    while True:
        now = datetime.now()
        if now.hour == 8 and now.minute == 0:
            send_weather_reminder()
            time.sleep(60)  # 防止重复发送

    # 保持程序运行
    while True:
        time.sleep(1)


if __name__ == '__main__':
    main()
