#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://mp.weixin.qq.com/s/m5Xp9SWuoY15bD4yFmNQHw
# 如何用Python向微信发送消息

import itchat


def receive_message():
    @itchat.msg_register(itchat.content.TEXT)
    def text_reply(msg):
        # 打印接收到的消息
        print(f"收到消息：{msg['Text']}，来自：{msg['FromUserName']}")

        # 回复消息
        itchat.send_msg('我收到了你的消息！', toUserName=msg['FromUserName'])

    # 运行消息监听
    itchat.run()


def main():
    # 登录微信:这个函数会生成一个二维码，扫描后即可登录。
    # hotReload=True 表示保持登录状态，这样下次运行时就不需要重新扫码了。
    itchat.auto_login(hotReload=True)

    # 获取好友列表
    # update=True 表示强制更新好友列表
    friends = itchat.get_friends(update=True)

    # 打印好友列表
    for friend in friends:
        # friend['NickName']：好友的昵称
        print(friend['NickName'])

    # 查找好友
    friend = itchat.search_friends(name='朋友A')[0]

    # 发送消息
    itchat.send_msg('你好，这是来自Python的消息！', toUserName=friend['UserName'])

    # 发送图片
    itchat.send_image('path/to/your/image.jpg', toUserName=friend['UserName'])


if __name__ == '__main__':
    main()
