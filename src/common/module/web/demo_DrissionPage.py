#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://gitee.com/g1879/DrissionPage#https://gitee.com/link?target=https%3A%2F%2Fdrissionpage.cn
# https://drissionpage.cn/
# DrissionPage 是一个基于 python 的网页自动化工具。
# 本库采用全自研的内核，内置了无数实用功能，对常用功能作了整合和优化，对比 selenium，有以下优点：
# pip install DrissionPage


def main():
    pass


if __name__ == '__main__':
    main()
