#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.chinaunix.net/uid-31410005-id-5834739.html
# python写http接口（get/post）给别人调用
# @function: post service of fastapi

import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


class Item(BaseModel):
    a: int = None
    b: int = None


@app.post('/test')
def calculate(request_data: Item):
    a = request_data.a
    b = request_data.b
    c = a + b
    res = {"res": c}
    return res


def main():
    uvicorn.run(app=app,
                host="0.0.0.0",
                port=8080,
                workers=1)


if __name__ == "__main__":
    main()
