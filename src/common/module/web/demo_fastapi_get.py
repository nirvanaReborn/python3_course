#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.chinaunix.net/uid-31410005-id-5834739.html
# python写http接口（get/post）给别人调用
# FastAPI是一个现代的、快速（高性能）的web框架，用于基于标准Python类型提示使用Python 3.6+构建api。
# 具有快速、快速编码、更少的错误、直观、简单、简便、健壮。简易而且本地win10能够跑起来。
# @function: get service of fastapi

# pip install uvicorn
import uvicorn
# pip install fastapi
from fastapi import FastAPI

app = FastAPI()


@app.get('/test/a={a}/b={b}')
def calculate(a: int = None, b: int = None):
    c = a + b
    res = {"res": c}
    return res


def main():
    uvicorn.run(app=app,
                host="0.0.0.0",
                port=8080,
                workers=1)
    # 接口访问:http://127.0.0.1:8080/test/a=1/b=4


if __name__ == "__main__":
    main()
