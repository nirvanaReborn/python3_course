#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# https://www.cnblogs.com/fnng/archive/2013/05/29/3106515.html
# selenium + python自动化测试环境搭建

import os
import pprint
import time

import selenium
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
from public_function import PROJECT_ROOT
import logging

# 设置为 WARNING 则代表只有 WARNING 以上级别的日志才会被输出出来
logging.getLogger("selenium").setLevel(logging.WARNING)


def test_help():
    help(selenium)


def test_dir():
    for i in dir(selenium):
        print(i)


# Selenium 是一个自动化测试工具，利用它我们可以驱动浏览器执行特定的动作，如点击、下拉等等操作，
# 对于一些 JavaScript 渲染的页面来说，此种抓取方式非常有效。
# 但这样还不够，我们还需要浏览器如 Chrome、Firefox、PhantomJS 等浏览器来配合 Selenium 工作。
def test_0():
    print(selenium.__version__)


# 使用chrome浏览器需要自己下载chromedriver.exe
# 驱动下载地址：http://chromedriver.storage.googleapis.com/index.html
#              http://npm.taobao.org/mirrors/chromedriver/
# 可以看做它是链接WebDriver和Chrome浏览器的桥梁，放在Chrome浏览器的安装目录chrome.exe的同级目录下。
# 驱动放置位置有以下3中方法：
# 1.直接指定executable_path参数为驱动路径即可
# 2.可以看做它是链接WebDriver和Chrome浏览器的桥梁，放在python的Scripts目录下即可
# 3.如果chromedriver.exe放在Chrome浏览器的安装目录并和chrome.exe同级，需要多写下面3行代码
#   chromedriver = r"C:\Program Files (x86)\Google\Chrome\Application\chromedriver.exe"
#   os.environ["webdriver.chrome.driver"] = chromedriver
#   browser = webdriver.Chrome(chromedriver)
def test_1():
    print(selenium.__version__)

    def get_browser_chrome():
        options = webdriver.ChromeOptions()
        # 在 Chrome 59 版本开始已经开始支持了 Headless 模式，也就是无界面模式，这样爬取的时候就不会弹出浏览器了。
        options.add_argument('--headless')
        options.add_argument('--disable-gpu')
        # 让chrome浏览器不出现‘Chrome正在受到自动软件的控制’的提示语
        options.add_argument('disable-infobars')
        # options.add_argument('--window-size=1920,1080')  # 设置窗口界面大小
        browser = webdriver.Chrome(
            executable_path=r"D:\ProgramFiles\1_FreeInstallation\python_selenium_driver\chromedriver.exe",
            options=options,
        )
        # print(type(browser)) # <class 'selenium.webdriver.chrome.webdriver.WebDriver'>
        return browser

    browser = get_browser_chrome()
    pprint.pprint(browser.capabilities)
    browser.get(dict_arg["url"])  # Load page
    print(browser.title, browser.current_url)
    print(browser.capabilities['browserVersion'])  # 打印浏览器version的值
    assert "Yahoo" in browser.title
    elem = browser.find_element_by_name("p")  # Find the query box
    elem.send_keys("seleniumhq" + Keys.RETURN)
    time.sleep(0.2)  # Let the page load, will be added to the API
    try:
        browser.find_element_by_xpath("//a[contains(@href,'http://seleniumhq.org')]")
    except NoSuchElementException:
        assert 0, "can't find seleniumhq"
    browser.close()


# 微软的Edge浏览器
# 驱动下载地址：https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/#downloads
# 最新版本 Release 16299，Version: 5.16299，支持 Edge version supported: 16.16299 。
# 注意edge驱动只有在edge浏览器未启动的情况下才能正常运行，否则会报错。
def test_2():
    # 要使用headless的edge，还需 pip install msedge-selenium-tools
    from msedge.selenium_tools import Edge, EdgeOptions
    options = EdgeOptions()
    options.use_chromium = True  # if we miss this line, we can't make Edge headless
    # A little different from Chrome cause we don't need two lines(--) before 'headless' and 'disable-gpu'
    options.add_argument('headless')
    options.add_argument('disable-gpu')
    browser = Edge(
        executable_path=os.path.join(PROJECT_ROOT, r"D:\ProgramFiles\1_FreeInstallation\python_selenium_driver\msedgedriver.exe"),
        options=options,
    )
    try:
        browser.get(dict_arg["url"])  # Load page
        print(browser.title, browser.current_url, browser.page_source)
    finally:
        browser.close()


# GeckoDriver用于驱动Firefox
# 驱动下载地址：https://github.com/mozilla/geckodriver/releases
# geckodriver.exe下载后放在python的Scripts目录下即可
def test_3():
    options = webdriver.FirefoxOptions()
    options.add_argument('--headless')
    browser = webdriver.Firefox(
        executable_path=os.path.join(PROJECT_ROOT, r"D:\ProgramFiles\1_FreeInstallation\python_selenium_driver\geckodriver.exe"),
        firefox_options=options,
    )
    pprint.pprint(browser.capabilities)
    browser.get(dict_arg["url"])  # Load page
    print(browser.title, browser.current_url)
    assert "Yahoo!" in browser.title
    elem = browser.find_element_by_name("p")  # Find the query box
    elem.send_keys("seleniumhq" + Keys.RETURN)
    time.sleep(0.2)  # Let the page load, will be added to the API
    try:
        browser.find_element_by_xpath("//a[contains(@href,'http://seleniumhq.org')]")
    except NoSuchElementException:
        assert 0, "can't find seleniumhq"
    browser.close()


# 苹果的Safari浏览器
def test_4():
    browser = webdriver.Safari()
    try:
        browser.get(dict_arg["url"])  # Load page
        print(browser.title, browser.current_url, browser.page_source)
    finally:
        browser.close()


# 使用webkit无界面浏览器PhantomJS
# 下载地址：http://phantomjs.org/download.html
# UserWarning: Selenium support for PhantomJS has been deprecated, please use headless versions of Chrome or Firefox instead
# 将其卸载pip uninstall selenium，重新安装并指定版本号pip install selenium==2.48.0即可。
def test_5():
    browser = webdriver.PhantomJS(executable_path=r"D:\ProgramFiles\1_FreeInstallation\phantomjs\bin\phantomjs.exe")
    pprint.pprint(browser.capabilities)
    browser.get(dict_arg["url"])  # Load page
    print(browser.title, browser.current_url)
    browser.close()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    dict_arg = {
        # "url": "http://www.yahoo.com",
        "url": "http://www.baidu.com",
        # "url": 'http://www.python.org/',
    }

    main()
