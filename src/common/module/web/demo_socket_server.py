#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.runoob.com/python/python-socket.html
'''
# https://www.cnblogs.com/hls91/p/11714827.html
# python基础之socket与socketserver
Socket的英文原义是“孔”或“插座”，在Unix的进程通信机制中又称为‘套接字’。
套接字实际上并不复杂，它是由一个ip地址以及一个端口号组成。
Socket正如其英文原意那样，像一个多孔插座。
一台主机犹如布满各种插座（ip地址）的房间，每个插座有很多插口（端口），
通过这些插口接入电线（进程）我们可以烧水，看电视，玩电脑……

应用程序通常通过"套接字"向网络发出请求或者应答网络请求。

套接字的作用之一就是用来区分不同应用进程，当某个进程绑定了本机ip的某个端口，
那么所有传送至这个ip地址上的这个端口的所有数据都会被内核送至该进程进行处理。

Python 提供了两个基本的 socket 模块。
1.低级别的网络服务支持基本的 Socket，它提供了标准的 BSD Sockets API，可以访问底层操作系统Socket接口的全部方法。
2.高级别的网络服务模块 SocketServer， 它提供了服务器中心类，可以简化网络服务器的开发。

----socket
    我们知道，现在的应用程序大多为C/S架构，也就是分为客户端/服务器端。

    服务器端：服务器端进程需要申请套接字，然后自己绑定在这个套接字上，并对这个套接字进行监听。
            当有客户端发送数据了，则接收数据进行处理，处理完成后对客户端进行响应。

    客户端：客户端则相对简单些，客户端只需要申请一个套接字，然后通过这个套接字连接服务器端的套接字，连接建立后就可以进行后续操作了。

----socketserver
    虽说用Python编写简单的网络程序很方便，但复杂一点的网络程序还是用现成的框架比较好。
    这样就可以专心事务逻辑，而不是套接字的各种细节。
    SocketServer模块简化了编写网络服务程序的任务。同时SocketServer模块也是Python标准库中很多服务器框架的基础。

    socketserver在python2中为SocketServer,在python3种取消了首字母大写，改名为socketserver。

    socketserver中包含了两种类，一种为服务类（server class），一种为请求处理类（request handle class）。
    前者提供了许多方法：像绑定，监听，运行…… （也就是建立连接的过程）
    后者则专注于如何处理用户所发送的数据（也就是事务逻辑）。

    一般情况下，所有的服务，都是先建立连接，也就是建立一个服务类的实例，
    然后开始处理用户请求，也就是建立一个请求处理类的实例。
'''

import socket


def main():
    # 建立一个服务端
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(('localhost', 6999))  # 绑定要监听的端口
    server.listen(5)  # 开始监听 表示可以使用五个链接排队
    while True:  # conn就是客户端链接过来而在服务端为期生成的一个链接实例
        conn, addr = server.accept()  # 等待链接,多个链接的时候就会出现问题,其实返回了两个值
        print(conn, addr)
        while True:
            try:
                data = conn.recv(1024)  # 接收数据
                print('recive:', data.decode())  # 打印接收到的数据
                conn.send(data.upper())  # 然后再发送数据
            except ConnectionResetError as e:
                print('关闭了正在占线的链接！', e.errno)
                break
        conn.close()


if __name__ == "__main__":
    main()
