#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://germey.gitbooks.io/python3webspider/content/1.6.2-Tornado%E7%9A%84%E5%AE%89%E8%A3%85.html
# Tornado 是一个支持异步的Web框架，通过使用非阻塞 I/O 流，它可以支撑成千上万的开放连接，效率非常高
# GitHub：https://github.com/tornadoweb/tornado
# PyPi：https://pypi.python.org/pypi/tornado
# 官方文档：http://www.tornadoweb.org
# pip3 install tornado

# 直接运行程序，可以发现系统在 8888 端口运行了 Web 服务，控制台没有输出内容，
# 此时访问：http://127.0.0.1:8888/，可以观察到网页中呈现了 Hello, world

import tornado.ioloop
import tornado.web


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")


def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
    ])


def main():
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
