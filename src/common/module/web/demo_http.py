#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''https://docs.python.org/zh-cn/3.7/library/http.html
# https://www.cnblogs.com/91allan/p/4889167.html
# Python搭建简易HTTP服务（3.x版本和2.x版本的）

1）Python2.x 内置了一个简单的HTTP服务器，只需要在命令行下面敲一行命令，一个HTTP服务器就起来了：
python -m SimpleHTTPServer 8080

2）Python3.x 内置了一个简单的HTTP服务器，只需要在命令行下面敲一行命令，一个HTTP服务器就起来了：
python -m http.server 8080

后面的8080端口是可选的，不填会采用缺省端口8000。注意，这会将当前所在的文件夹设置为默认的Web目录，试着在浏览器敲入本机地址：
http://localhost:8080

如果需要手机端显示：与电脑同wifi下浏览器输入你电脑的ip地址+文件名：
192.168.0.101：8000/index.html

如果当前文件夹有index.html文件，会默认显示该文件，否则，会以文件列表的形式显示目录下所有文件。
这样已经实现了最基本的文件分享的目的，你可以做成一个脚本，再建立一个快捷方式，就可以很方便的启动文件分享了。
如果有更多需求，完全可以根据自己需要定制，具体的请参见官方文档SimpleHTTPServer，或者直接看源码。
拷贝一段，方便参考：
'''
# http://www.lifeba.org/arch/python_http_simplehttpserver_basehttpserver_cgihttpserver.html
# 基于BaseHTTPServer的http server实现，包括get，post方法，get参数接收，post参数接收。

from http.server import BaseHTTPRequestHandler, HTTPServer
import io
import shutil
import urllib
import sys


def test_1(port = 8080):
    def get_httpd(port):
        if sys.version_info[0] == 3:
            import http.server
            import socketserver

            Handler = http.server.SimpleHTTPRequestHandler
            httpd = socketserver.TCPServer(("", port), Handler)
        else:
            import SimpleHTTPServer
            import SocketServer

            Handler = SimpleHTTPServer.SimpleHTTPRequestHandler
            httpd = SocketServer.TCPServer(("", port), Handler)

        return httpd

    httpd = get_httpd(port)
    print("http://localhost:" + str(port))
    httpd.serve_forever()


class MyRequestHandler(BaseHTTPRequestHandler):
    def transDicts(self, params):
        dicts = {}
        if len(params) == 0:
            return
        params = params.split('&')
        for param in params:
            dicts[param.split('=')[0]] = param.split('=')[1]
        return dicts

    def do_GET(self):
        self.process(2)

    def do_POST(self):
        self.process(1)

    def process(self, type):
        content = ""
        if type == 1:  # post方法，接收post参数
            datas = self.rfile.read(int(self.headers['content-length']))
            datas = urllib.unquote(datas).decode("utf-8", 'ignore')  # 指定编码方式
            datas = self.transDicts(datas)  # 将参数转换为字典
            if datas.has_key('data'):
                content = "data:" + datas['data'] + "\r\n"

        if '?' in self.path:
            query = urllib.splitquery(self.path)
            action = query[0]

            if query[1]:  # 接收get参数
                queryParams = {}
                for qp in query[1].split('&'):
                    kv = qp.split('=')
                    queryParams[kv[0]] = urllib.unquote(kv[1]).decode("utf-8", 'ignore')
                    content += kv[0] + ':' + queryParams[kv[0]] + "\r\n"

            # 指定返回编码
            enc = "UTF-8"
            content = content.encode(enc)
            f = io.BytesIO()
            f.write(content)
            f.seek(0)
            self.send_response(200)
            self.send_header("Content-type", "text/html; charset=%s" % enc)
            self.send_header("Content-Length", str(len(content)))
            self.end_headers()
            shutil.copyfileobj(f, self.wfile)


def test_2():
    try:
        server = HTTPServer(('', 8000), MyRequestHandler)
        print('started httpserver...')
        server.serve_forever()
    except KeyboardInterrupt:
        server.socket.close()
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
