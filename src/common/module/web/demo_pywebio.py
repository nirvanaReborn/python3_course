#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://pywebio.readthedocs.io/en/latest/index.html
# https://segmentfault.com/a/1190000044213566
# 无需繁琐的前端开发，你就可以创建交互式Web应用，让用户感受到前所未有的沉浸式体验。
# PyWebIO不仅仅局限于获取用户输入和输出文本。它还支持绘图、文件上传、图表绘制和更多高级功能。
# pip install pywebio

from pywebio import input, output, start_server
from pywebio.platform.fastapi import webio_view
from fastapi import FastAPI


def test_1():
    def greet_user():
        username = input("请输入你的名字：")
        output.put_text(f"你好，{username}！欢迎使用PyWebIO。")

        app = FastAPI()
        app.add_route("/", webio_view(greet_user))


def test_2():
    '''在线投票应用'''
    def vote_app():
        candidates = input.checkbox("请选择候选人：", options=["候选人A", "候选人B", "候选人C"])
        output.put_text("你的投票已经提交！")
        output.put_text("投票结果：")
        for candidate in candidates:
            output.put_text(f"{candidate}: {candidates.count(candidate)} 票")

    start_server(vote_app, port=8080)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
