#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
webbrowser：是Python 自带的，打开浏览器获取指定页面。
requests：从因特网上下载文件和网页。
Beautiful Soup：解析HTML，即网页编写的格式。
selenium：启动并控制一个Web 浏览器。selenium 能够填写表单，并模拟鼠标在这个浏览器中点击。
'''
# 《Python编程快速上手——让繁琐工作自动化》
# 运行参数:  870 Valencia St, San Francisco, CA 94110
import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
# logging 库获取日志 requests 和 urllib3(requests 依赖) 对象，调低他们的日志级别，
# 设置为 WARNING 则代表只有 WARNING 以上级别的日志才会被输出出来
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib3").setLevel(logging.WARNING)
import webbrowser


def test_1():
    url = 'http://www.python.org/'
    # webbrowser.open(url) # 在浏览器中打开网址
    webbrowser.open_new(url.strip())  # 会在新窗口中打开网址
    # webbrowser.open_new_tab(url.strip()) # 在新标签中打开网址


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
