#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.runoob.com/python/python-socket.html
# Socket又称"套接字"，应用程序通常通过"套接字"向网络发出请求或者应答网络请求，使主机间或者一台计算机上的进程间可以通讯。
# Python 提供了两个级别访问的网络服务：
# 1.低级别的网络服务支持基本的 Socket，它提供了标准的 BSD Sockets API，可以访问底层操作系统Socket接口的全部方法。
# 2.高级别的网络服务模块 SocketServer， 它提供了服务器中心类，可以简化网络服务器的开发。

import socket


def test_1():
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # 声明socket类型，同时生成链接对象
    try:
        client.connect(('localhost', 6999))  # 建立一个链接，连接到本地的6969端口
        while True:
            msg = '欢迎访问菜鸟教程！'  # strip默认取出字符串的头尾空格
            client.send(msg.encode('utf-8'))  # 发送一条信息, python3 只接收btye流
            data = client.recv(1024)  # 接收一个信息，并指定接收的大小,为1024字节
            print('recv:', data.decode())  # 输出我接收的信息
    except Exception as e:
        print(e)
    finally:
        client.close()  # 关闭这个链接


def test_2():
    import sys
    HOST, PORT = "localhost", 9999
    data = " ".join(sys.argv[1:])

    # Create a socket (SOCK_STREAM means a TCP socket)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        # Connect to server and send data
        sock.connect((HOST, PORT))
        sock.sendall(bytes(data + "\n", "utf-8"))

        # Receive data from the server and shut down
        received = str(sock.recv(1024), "utf-8")
    finally:
        sock.close()

    print("Sent:     {}".format(data))
    print("Received: {}".format(received))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
