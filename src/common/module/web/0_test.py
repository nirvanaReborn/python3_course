#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
Django是一个开放源代码的Web应用框架，由Python写成，是Python生态中最流行的开源Web应用框架。
Django采用模型、模板和视图的编写模式，称为MTV模式。

Pyramid是一个通用、开源的Python Web应用程序开发框架。
它主要的目的是让Python开发者更简单的创建Web应用，相比Django，它是一个相对小巧、快速、灵活的开源Python Web框架。

Flask是轻量级Web应用框架，相比Django和Pyramid，它也被称为微框架。
使用Flask开发Web应用十分方便，甚至几行代码即可建立一个小型网站。
Flask核心十分简单，并不直接包含诸如数据库访问等的抽象访问层，而是通过扩展模块形式来支持。
'''


def test_1():
    pass


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
