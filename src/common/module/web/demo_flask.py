#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://germey.gitbooks.io/python3webspider/content/1.6.1-Flask%E7%9A%84%E5%AE%89%E8%A3%85.html
# Flask 是一个轻量级的 Web 服务程序，简单、易用、灵活
# GitHub：https://github.com/pallets/flask
# 官方文档：http://flask.pocoo.org
# 中文文档：http://docs.jinkan.org/docs/flask
# PyPi：https://pypi.python.org/pypi/Flask
# pip3 install flask

# 直接运行代码，可以发现系统会在 5000 端口开启 Web 服务
# 直接访问：http://127.0.0.1:5000/，可以观察到网页中呈现了 Hello World!

from flask import Flask, jsonify

app = Flask(__name__)


@app.route('/')
def root():
    return jsonify(
        app_name="我的小工具",
        app_user="zhenguo"
    )

@app.route("/hello")
def hello():
    return "Hello World!"


def main():
    app.run()


if __name__ == "__main__":
    main()
