#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.zhihu.com/question/451397804/answer/3118860310
# 如何进行异步并发? 用Task。本质上是Future，但我们目前只需要用到task
# 用asyncio.create_task就可以把Coroutine打包成Task。它们的不同点在于，Coroutine是同步的，而Task是异步的。
#
# 之前我们提到过，想执行异步函数，我们要把它提交给asyncio，让它帮我们执行。如果我们有一个Coroutine，我们必须await它，才能把相应的异步函数提交给asyncio。
# 而Task则不然，它在创建的时候就已经把相应的异步函数提交给了asyncio，因此，之后我们await任何东西，asyncio都会尝试执行Task里边的异步函数。
# gather一组Coroutine，（基本上）就相当于把它们打包成Task，然后一个一个await。并发不一定要用gather，它只是提供了一种更简单的写法。
#
# await一个Coroutine的含义是 开始执行、等待执行完成、获取结果。
# 而await一个Task，由于Task在之前就已经开始了，所以这里的含义是等待执行完成、获取结果。

import asyncio


async def my_coroutine(t):
    print(f"Coroutine {t} is executing...")
    await asyncio.sleep(t)
    print(f"Coroutine {t} is done.")
    myID = '114514_' + str(t)
    return myID


async def main():
    print("直接await协程对象：")
    id1 = await my_coroutine(1)
    id2 = await my_coroutine(2)
    print(f'{id1=}, {id2=}')
    print('----------------------------------------------------------------')

    print("包装成任务，然后await：")
    task1 = asyncio.create_task(my_coroutine(1))
    task2 = asyncio.create_task(my_coroutine(2))
    id1 = await task1
    print("在这行之前，task2就已经开始执行了：")
    id2 = await task2
    print(f'{id1=}, {id2=}')
    print('----------------------------------------------------------------')

    print("随便await什么，任务其实都能开始执行：")
    task1 = asyncio.create_task(my_coroutine(1))
    task2 = asyncio.create_task(my_coroutine(2))
    await asyncio.sleep(0.1)
    print('----------------------------------------------------------------')


if __name__ == '__main__':
    asyncio.run(main())
