#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.zhihu.com/question/451397804/answer/3118860310
# 下面代码展示了不await Task也能运行Task，个人认为很有助于理解Task的使用。
import asyncio


async def my_coroutine(id: int, results: list):
    print(f"Coroutine {id} is executing...")
    await asyncio.sleep(id)
    print(f"Coroutine {id} is done.")
    results.append(f"result of Coroutine {id}")


async def main():
    results = []
    asyncio.create_task(my_coroutine(1, results))
    asyncio.create_task(my_coroutine(2, results))
    print(results)
    await asyncio.sleep(3)
    print(results)


if __name__ == '__main__':
    asyncio.run(main())
