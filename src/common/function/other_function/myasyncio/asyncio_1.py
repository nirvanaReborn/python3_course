#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.zhihu.com/question/451397804/answer/3118860310
#

import time

"""
import asyncio

async def aio_func(x):
    await asyncio.sleep(3)
    print(x+'!')

async def main():
    await aio_func("芝士异步函数")

if __name__ == '__main__':
    asyncio.run(main())
"""

# 上面的代码就类似于:

def func(x):
    time.sleep(3)
    print(x + '!')


def main():
    func("芝士同步函数")


if __name__ == '__main__':
    main()
