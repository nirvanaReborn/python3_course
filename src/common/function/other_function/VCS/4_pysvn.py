#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 版本控制系统VCS（version control system）
# http://blog.csdn.net/riag/article/details/3961283
# 获取svn版本库里当天日志
# 从svn repos里 获取当天的日志
# 后来注意到pysvn源码包中有个INSTALL.html文件，打开看了下按照上面说的步骤操作就成功了。
'''
软件下载和使用文档：
    pysvn软件下载：http://pysvn.tigris.org/project_downloads.html
    pysvn非常好的官方使用说明文档： http://pysvn.tigris.org/docs/pysvn_prog_ref.html
'''

import time

import pysvn


def print_reposinfo(reposInfo):
    ReposComitTime = time.strftime("%Y-%m-%d", time.localtime(reposInfo.date));
    print(reposInfo.author)
    print(ReposComitTime)
    print(reposInfo.message.encode("cp936"))
    print(reposInfo.revision.number)


def getTody_log(TodayTime, SVNURL):
    global g_LogList
    g_LogList = []
    client = pysvn.Client()
    revision_start = pysvn.Revision(pysvn.opt_revision_kind.head)

    bContinue = True
    while bContinue:

        LogList = client.log(SVNURL, revision_start, limit=10)
        print("LogList: %s" % (TodayTime))
        Number = LogList[len(LogList) - 1].revision.number

        revision_start = pysvn.Revision(pysvn.opt_revision_kind.number, Number)

        for LogInfo in LogList:
            LogTime = time.strftime("%Y-%m-%d", time.localtime(LogInfo.date))
            print(LogTime)

            if LogTime == TodayTime:
                print(LogInfo)
                g_LogList.append(LogInfo)
            else:
                bContinue = False

        if len(g_LogList) == 0:
            break


def WritLog(TodayTime):
    with open("./ChangLog.txt", "wb") as f:
        print(g_LogList)
        Text = "%s 修改日志/r/n/r/n" % (TodayTime)
        f.write(Text)

        for Log in g_LogList:
            Text = "%s : " % (Log.author.ljust(15))
            f.write(Text)
            f.write(Log.message.encode("cp936"))
            f.write("/r/n")

        if len(g_LogList) == 0:
            f.write("没有修改日志")


def main():
    TodayTime = time.strftime("%Y-%m-%d", time.localtime())
    SVNURL = "http://192.168.7.242/svn/test/trunk"
    # SVNURL = "E://riag//work//HMI-Contrib"
    g_LogList = []
    Repos_NUM = 0
    getTody_log(TodayTime, SVNURL)
    WritLog(TodayTime)


if __name__ == '__main__':
    main()
