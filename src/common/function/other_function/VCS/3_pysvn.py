#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/bewolf/p/7266210.html
# python利用pysvn发布lib的小程序
'''
软件下载和使用文档：
    pysvn软件下载：http://pysvn.tigris.org/project_downloads.html
    pysvn非常好的官方使用说明文档： http://pysvn.tigris.org/docs/pysvn_prog_ref.html
'''

import os
import shutil
import time

import pysvn

# 发布lib时对应的适配代码路径
code_path = "F:/xyp_work"
# lib的归档路径
dest_path = "F:/AR_lib发布"
# 自己的备份信息
myself_save_path = "F:/save/备份.txt"
# 填写依赖的路径
dependence_path = "F:/依赖"
# 维护一个专门记录lib和br标签号的文档用于读取上一次发lib时的标签号
old_num_path = "F:/save/old_num.txt"

# svn 路径
lib_svn = "lib的svn路径"
br_svn = "br的svn路径"

# 版本号和标签号
# verson = "esap v2R2C00B100";
# lib_num = 2;
# br_num = 7;

###读取上一次的标签号和用户输入的本次lib的标签号和版本号###
print("输入版本号")
verson = input()
print("输入lib的标签号")
lib_num = int(input())
print("输入适配的标签号")
br_num = int(input())

fp = open(old_num_path, "r")
last_lib_num = fp.readline()
last_br_num = fp.readline()
fp.close()

print("last lib num is  " + str(last_lib_num))
print("last br num is  " + str(last_br_num))
print("current verson is " + str(verson) + "\n")
print("current lib num is  " + str(lib_num) + "\n")
print("current br num is  " + str(br_num) + "\n")
print("please confirm : y/n")

res = input()
if "y" == res:
    print("now start")
else:
    exit()

###################创建文件夹和文件######################
vasp = dest_path + '/' + "vasp"
vaspadp = dest_path + '/' + "vaspadp"
svn_txt = dest_path + '/' + "svn.txt"

isExists = os.path.exists(dest_path)
if not isExists:
    os.makedirs(dest_path)
else:
    print("AR_lib发布文件夹已存在，看看是否搞错了")
    exit()

isExists = os.path.exists(vasp)
if not isExists:
    os.makedirs(vasp)

isExists = os.path.exists(vaspadp)
if not isExists:
    os.makedirs(vaspadp)

# svn.txt
fp = open(svn_txt, "a")
fp.write(verson + "\n")
fp.write((lib_svn + " " + str(lib_num) + "\n"))
fp.write((br_svn + " " + str(br_num) + "\n"))
fp.close()

# 自己的备份记录
fp = open(myself_save_path, "a")
fp.write("\n")
fp.write("\n")
fp.write("\n")
fp.write(time.strftime("%d/%m/%Y") + "\n")
fp.write(verson + "\n")
fp.write((lib_svn + " " + str(lib_num) + "\n"))
fp.write((br_svn + " " + str(br_num) + "\n"))
fp.close()

# 依赖文件 复制release notes.xlsx，然后重命名，否则直接创建出来不是共享的
shutil.copyfile(dependence_path + "/release notes.xlsx", dependence_path + "/" + verson + " release notes.xlsx")

# 维护一个专门记录lib和br标签号的文档用于读取上一次发lib时的标签号
fp = open(old_num_path, "w")
fp.truncate()  # 清除文件内容
fp.write(str(lib_num) + "\n")
fp.write(str(br_num) + "\n")
fp.close()
##############################################

pysvn_current_br_num = pysvn.Revision(pysvn.opt_revision_kind.number, br_num)
pysvn_log_start = pysvn.Revision(pysvn.opt_revision_kind.number, last_lib_num)
pysvn_log_end = pysvn.Revision(pysvn.opt_revision_kind.number, lib_num)
"""
retcode - boolean, False if no username and password are available. True if subversion is to use the username and password.
username - string, the username to uses
password - string, the password to use
save - boolean, return True if you want subversion to remember the username and password in the configuration directory. return False to prevent saving the username and password.
"""
# def get_login( realm, username, may_save ):
#    return retcode, username, password, save

client = pysvn.Client()
# client.callback_get_login = get_login

client.revert(code_path, True)

client.update(code_path, True, pysvn_current_br_num, False)

client.export(code_path + "/vasp", vasp, True, pysvn_current_br_num, None, False)
client.export(code_path + "/product/ar/vaspadp", vaspadp, True, pysvn_current_br_num, None, False)

"""
log returns a list of log entries; each log entry is a dictionary. The dictionary contains: 
author - string - the name of the author who committed the revision
date - float time - the date of the commit
message - string - the text of the log message for the commit
revision - pysvn.Revision - the revision of the commit
changed_paths - list of dictionaries. Each dictionary contains:
path - string - the path in the repository
action - string
copyfrom_path - string - if copied, the original path, else None
copyfrom_revision - pysvn.Revision - if copied, the revision of the original, else None
"""
logmessege = client.log(code_path, pysvn_log_start, pysvn_log_end, False, False)
log_file = dest_path + '/' + "log.txt"
fp = open(log_file, "a")
for log_one in logmessege:
    fp.write(log_one.author + " " + log_one.message + "\n")
fp.close()
