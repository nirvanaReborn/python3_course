#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# http://ju.outofmemory.cn/entry/35762
# 完整和详细的API请参考: http://pysvn.tigris.org/docs/pysvn_prog_ref.html
'''
软件下载和使用文档：
    pysvn软件下载：http://pysvn.tigris.org/project_downloads.html
    pysvn非常好的官方使用说明文档： http://pysvn.tigris.org/docs/pysvn_prog_ref.html
'''

import pysvn


# check out一份工作副本
def test_1():
    client = pysvn.Client()  # check out the current version of the pysvn project
    client.checkout('http://localhost/example/trunk', './examples/pysvn')  # check out revision 11 of the pysvn project
    client.checkout('http://localhost/example/trunk', './examples/pysvn-11',
                    revision=pysvn.Revision(pysvn.opt_revision_kind.number, 11))


# 添加一个文件或者目录到仓库
def test_2():
    # write a file foo.txt
    f = open('./examples/pysvn/foo.txt', 'w')
    f.write('Sample versioned file via pithon \n')
    f.close()
    client = pysvn.Client()  # schedule the addition;#  the working copy will now track the file as a scheduled change
    client.add('./examples/pysvn/foo.txt')  # committing the change actually adds the file to the repository
    client.checkin(['./examples/pysvn/foo.txt'], 'Adding a sample file')


# 更新工作副本
def test_3():
    client = pysvn.Client()
    client.update('./examples/pysvn')


# 提交更新到仓库
def test_4():
    # edit the file foo.txt
    f = open('./examples/pysvn/foo.txt', 'w')
    f.write('Sample versioned file via python\n')
    f.close()  # checkin the change with a log message
    client = pysvn.Client()
    client.checkin(['./examples/pysvn'], 'Corrected spelling of python in foo.txt')


# 放弃工作副本修改
def test_5():
    # edit the file foo.txt
    f = open('./examples/pysvn/foo.txt', 'w')
    f.write('This change will never be seen\n')
    f.close()  # discard the edits
    client = pysvn.Client()
    client.revert('./examples/pysvn/foo.txt')


# 重命名或者移动文件
def test_6():
    client = pysvn.Client()  # rename the file client side
    client.move('./examples/pysvn/foo.txt', './examples/pysvn/foo2.txt')  # checkin the change removes the file from the repository
    client.checkin(['./examples/pysvn/foo.txt', './examples/pysvn/foo2.txt'], 'Foo has become Foo2')


# 从仓库中删除文件或目录
def test_7():
    client = pysvn.Client()  # schedule the removal;#  the file will be removed from the working copy
    client.remove('./examples/pysvn/foo2.txt')  # committing the change removes the file from the repository
    client.checkin(['./examples/pysvn/foo2.txt'], 'Removing sample file')


# 确定等待变动
def test_8():
    client = pysvn.Client()
    changes = client.status('./examples/pysvn')
    print('files to be added:')
    print([f.path for f in changes if f.text_status == pysvn.wc_status_kind.added])
    print('files to be removed:')
    print([f.path for f in changes if f.text_status == pysvn.wc_status_kind.deleted])
    print('files that have changed:')
    print([f.path for f in changes if f.text_status == pysvn.wc_status_kind.modified])
    print('files with merge conflicts:')
    print([f.path for f in changes if f.text_status == pysvn.wc_status_kind.conflicted])
    print('unversioned files:')
    print([f.path for f in changes if f.text_status == pysvn.wc_status_kind.unversioned])


# 生成差异或补丁
def test_9():
    client = pysvn.Client()
    diff_text = client.diff('./tmp-file-prefix-', '.')


# 获取仓库URL
def test_10():
    client = pysvn.Client()
    entry = client.info('.')
    print('Url:', entry.url)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
