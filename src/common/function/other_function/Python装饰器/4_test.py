#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# http://cn.voidcc.com/question/p-zykuvphb-tz.html
# Python 用于打印递归树的装饰器

from functools import wraps


def printRecursionTree(func):
    global _recursiondepth
    _print = print
    _recursiondepth = 0

    def getpads():
        if _recursiondepth == 0:
            strFn = '{} └──'.format(' │ ' * (_recursiondepth - 1))
            strOther = '{} ▒▒'.format(' │ ' * (_recursiondepth - 1))
            strRet = '{} '.format(' │ ' * (_recursiondepth - 1))
        else:
            strFn = ' {} ├──'.format(' │ ' * (_recursiondepth - 1))
            strOther = ' {} │▒▒'.format(' │ ' * (_recursiondepth - 1))
            strRet = ' {} │ '.format(' │ ' * (_recursiondepth - 1))

        return strFn, strRet, strOther

    def indentedprint():
        @wraps(print)
        def wrapper(*args, **kwargs):
            strFn, strRet, strOther = getpads()
            _print(strOther, end=' ')
            _print(*args, **kwargs)

        return wrapper

    @wraps(func)
    def wrapper(*args, **kwargs):
        global _recursiondepth
        global print

        strFn, strRet, strOther = getpads()

        if args and kwargs:
            _print(strFn, '{}({}, {}):'.format(func.__qualname__, ', '.join(args), kwargs))
        else:
            _print(strFn, '{}({}):'.format(func.__qualname__, ', '.join(map(str, args)) if args else '', kwargs if kwargs else ''))
        _recursiondepth += 1
        print, backup = indentedprint(), print
        retval = func(*args, **kwargs)
        print = backup
        _recursiondepth -= 1
        _print(strRet, '╰', retval)
        if _recursiondepth == 0:
            _print()
        return retval

    return wrapper


@printRecursionTree
def fib(n):
    if n <= 1:
        print('Base Case')
        return n
    print('Recursive Case')
    return fib(n - 1) + fib(n - 2)


# This works with mutually recursive functions too,
# since the variable _recursiondepth is global
@printRecursionTree
def iseven(n):
    print('checking if even')
    if n == 0: return True
    return isodd(n - 1)


@printRecursionTree
def isodd(n):
    print('checking if odd')
    if n == 0: return False
    return iseven(n - 1)


def main():
    iseven(5)
    fib(5)


if __name__ == "__main__":
    main()
