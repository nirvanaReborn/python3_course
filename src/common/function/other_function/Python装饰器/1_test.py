#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.zhihu.com/question/26930016/answer/796287827
# 如何理解Python装饰器？

'''
# 1.什么是Python装饰器？
# 顾名思义，从字面意思就可以理解，它是用来"装饰"Python的工具，使得代码更具有Python简洁的风格。
# 换句话说，它是一种函数的函数，因为装饰器传入的参数就是一个函数，然后通过实现各种功能来对这个函数的功能进行增强。
#
# 2.为什么用装饰器？
# 前面提到了，装饰器是通过某种方式来增强函数的功能。当然，我们可以通过很多方式来增强函数的功能，只是装饰器有一个无法替代的优势--简洁。
# 你只需要在每个函数上方加一个@就可以对这个函数进行增强。
#
# 3.在哪里用装饰器？
# 装饰器最大的优势是用于解决重复性的操作，其主要使用的场景有如下几个：
# 计算函数运行时间
# 给函数打日志
# 类型检查
# 当然，如果遇到其他重复操作的场景也可以类比使用装饰器。

# 装饰器的顺序
# @a
# @b
# @c
# def f ():
# 等效于:
# f = a(b(c(f)))
'''

from time import time, sleep


def fun_one():
    start = time()
    sleep(1)
    end = time()
    cost_time = end - start
    print("func one run time {}".format(cost_time))


def fun_two():
    start = time()
    sleep(1)
    end = time()
    cost_time = end - start
    print("func two run time {}".format(cost_time))


def fun_three():
    start = time()
    sleep(1)
    end = time()
    cost_time = end - start
    print("func three run time {}".format(cost_time))


def test_1():
    fun_one()
    fun_two()
    fun_three()


# --------------------------------------------------------------------------------
# 使用装饰器的方法是这样的:

def run_time(func):
    def wrapper():
        start = time()
        func()  # 函数在这里运行
        end = time()
        cost_time = end - start
        print("func three run time {}".format(cost_time))

    return wrapper


@run_time
def fun_one():
    sleep(1)


@run_time
def fun_two():
    sleep(1)


@run_time
def fun_three():
    sleep(1)


def test_2():
    fun_one()
    fun_two()
    fun_three()


# --------------------------------------------------------------------------------
# 带参数的装饰器:

def logger(msg=None):
    def run_time(func):
        def wrapper(*args, **kwargs):
            start = time()
            func()  # 函数在这里运行
            end = time()
            cost_time = end - start
            print("[{}] func three run time {}".format(msg, cost_time))

        return wrapper

    return run_time


@logger(msg="One")
def fun_one():
    sleep(1)


@logger(msg="Two")
def fun_two():
    sleep(1)


@logger(msg="Three")
def fun_three():
    sleep(1)


def test_3():
    fun_one()
    fun_two()
    fun_three()


# --------------------------------------------------------------------------------
# 自定义属性的装饰器:


# 利用自定义属性来修改日志等级
import logging
from functools import partial


# 它的功能是把一个函数func编程一个对象obj的属性，然后通过调用wrapper_property，
# 给装饰器添加了两个属性set_message和set_level，分别用于改变输出日志的内容和改变输出日志的等级。


def wrapper_property(obj, func=None):
    if func is None:
        return partial(attach_wrapper, obj)
    setattr(obj, func.__name__, func)
    return func


def logger_info(level, name=None, message=None):
    def decorate(func):
        logmsg = message if message else func.__name__

        def wrapper(*args, **kwargs):
            logging.log(level, logmsg)
            return func(*args, **kwargs)

        @wrapper_property(wrapper)
        def set_level(newlevel):
            nonlocal level
            level = newlevel

        @wrapper_property(wrapper)
        def set_message(newmsg):
            nonlocal logmsg
            logmsg = newmsg

        return wrapper

    return decorate


@logger_info(logging.WARNING)
def test_log(x, y):
    return x + y


def test_4():
    test_log(3, 3)
    test_log.set_level(logging.ERROR)  # 改变一下输出日志等级
    test_log(5, 5)


# --------------------------------------------------------------------------------
# 保留元信息的装饰器:
# 什么是函数的元信息？
# 就是函数携带的一些基本信息，例如函数名、函数文档等，我们可以通过func.__name__获取函数名、
# 可以通过func.__doc__获取函数的文档信息，用户也可以通过注解等方式为函数添加元信息。

from functools import wraps


def run_time(func):
    @wraps(func)  # <- 这里加 wraps(func) 即可
    def wrapper(*args, **kwargs):
        start = time()
        func()  # 函数在这里运行
        end = time()
        cost_time = end - start
        print("func three run time {}".format(cost_time))

    return wrapper


@run_time
def fun_one():
    '''
    func one doc.
    '''
    sleep(1)


def test_5():
    fun_one()
    print(fun_one.__name__)
    print(fun_one.__doc__)


# --------------------------------------------------------------------------------
# 装饰器的顺序（代码的执行顺序是：从内到外）

def sayName(func):
    print('name')

    def inner():
        print("I'm Yu")
        return func()

    return inner


def sayAge(func):
    print('age')

    def inner():
        print("i'm 30")
        return func()

    return inner


@sayName
@sayAge
def sayHi():
    print('Hello, World')


def test_6():
    '''
    首先，python解释器执行到第一个装饰器@sayName，在接下来发现装饰器下边不是一个函数而是另一个装饰器，
    解释器会执行第二个的装饰器@sayAge，然后把sayHi函数传入装饰器，所以首先输出了“age”，
    当@sayAge装饰完成，此时的sayHi函数地址指向了sayAge.inner的地址，解释器会返回去执行@sayName装饰器来装饰新的sayHi，从而输出了“name”，
    接着函数当前指向sayName.inner会先输出“I'm Yu”，
    在这里返回的func()其实就是返回的sayAge.inner，所以在下面输出i'm 30，
    最后输出原本sayHi的“Hello, World”
    '''
    sayHi()


# --------------------------------------------------------------------------------
def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    make_choice(dict_choice)


if __name__ == "__main__":
    main()
