#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
# http://cn.voidcc.com/question/p-zykuvphb-tz.html
from functools import wraps
import builtins


def printRecursionTree(func):
    global _recursiondepth
    _print = print
    _recursiondepth = 0

    def getpads():
        if _recursiondepth == 0:
            strFn = '{} └──'.format(' │ ' * (_recursiondepth - 1))
            strOther = '{} ▒▒'.format(' │ ' * (_recursiondepth - 1))
            strRet = '{} '.format(' │ ' * (_recursiondepth - 1))
        else:
            strFn = ' {} ├──'.format(' │ ' * (_recursiondepth - 1))
            strOther = ' {} │▒▒'.format(' │ ' * (_recursiondepth - 1))
            strRet = ' {} │ '.format(' │ ' * (_recursiondepth - 1))

        return strFn, strRet, strOther

    def indentedprint():
        @wraps(print)
        def wrapper(*args, **kwargs):
            strFn, strRet, strOther = getpads()
            _print(strOther, end=' ')
            _print(*args, **kwargs)

        return wrapper

    @wraps(func)
    def wrapper(*args, **kwargs):
        global _recursiondepth  # no more need for global print up here

        strFn, strRet, strOther = getpads()

        if args and kwargs:
            _print(strFn, '{}({}, {}):'.format(func.__qualname__, ', '.join(args), kwargs))
        else:
            _print(strFn, '{}({}):'.format(func.__qualname__, ', '.join(map(str, args)) if args else '', kwargs if kwargs else ''))
        _recursiondepth += 1
        builtins.print, backup = indentedprint(), print  # change here
        retval = func(*args, **kwargs)
        builtins.print = backup  # and here
        _recursiondepth -= 1
        _print(strRet, '╰', retval)
        if _recursiondepth == 0:
            _print()
        return retval


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
