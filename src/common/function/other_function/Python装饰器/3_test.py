#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/amghost/p/3572128.html
# python用装饰器实现缓存函数执行结果

import time
import hashlib
import pickle

cache = {}


def is_obsolete(entry, duration):
    d = time.time() - entry['time']
    return d > duration


def compute_key(function, args, kwargs):
    key = pickle.dumps((function.func_name, args, kwargs))
    return hashlib.sha1(key).hexdigest()


# 这里memoize就是一个装饰器，duration是缓存过期时间。
# compute_key函数合并调用的函数的名称、参数并计算出key。
# 函数执行的结果result缓存在cache[key]中
def memoize(duration=10):
    def _memorize(function):
        def __memorize(*args, **kwargs):
            key = compute_key(function, args, kwargs)

            if key in cache and not is_obsolete(cache[key], duration):
                print('we got a winner')
                return cache[key]['value']

            result = function(*args, **kwargs)
            cache[key] = {'value': result, 'time': time.time()}
            return result

        return __memorize

    return _memorize


@memoize()
def complex(a, b):
    time.sleep(1)
    return a + b


def main():
    complex(1, 2)


if __name__ == "__main__":
    main()
