#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Python 有两种格式化字符串的方式，我们习惯简单地称之为旧式（old style）和新式（new style）。
# 1.旧式格式化的形式为string % data。
# 2.使用{}和format的新式格式化


# 使用%的旧式格式化
def test_1():
    '''
    %s 字符串
    %d 十进制整数
    %x 十六进制整数
    %o 八进制整数
    %f 十进制浮点数
    %e 以科学计数法表示的浮点数
    %g 十进制或科学计数法表示的浮点数
    %% 文本值% 本身
    '''
    n = 42
    f = 7.03
    s = 'string cheese'
    print('%-10d %-10f %-10s' % (n, f, s))
    print('%*.*d %*.*f %*.*s' % (10, 4, n, 10, 4, f, 10, 4, s))


# 使用{}和format的新式格式化
def test_2():
    n = 42
    f = 7.03
    s = 'string cheese'
    print('{} {} {}'.format(n, f, s))

    # 0 代表第一个参数f；1 代表字符串s；2 代表最后一个参数，整数n。
    print('{2} {0} {1}'.format(f, s, n))

    print('{n} {f} {s}'.format(n=42, f=7.03, s='string cheese'))  # 参数可以是字典或者命名变量
    print('{n:d} {f:f} {s:s}'.format(n=42, f=7.03, s='string cheese'))

    d = {'n': 42, 'f': 7.03, 's': 'string cheese'}
    # {0} 代表整个字典，{1} 则代表字典后面的字符串'other'：
    print('{0[n]} {0[f]} {0[s]} {1}'.format(d, 'other'))

    print('{0:10d} {1:10f} {2:10s}'.format(n, f, s))
    print('{0:>10d} {1:>10f} {2:>10s}'.format(n, f, s))  # 使用> 字符设定右对齐
    print('{0:<10d} {1:<10f} {2:<10s}'.format(n, f, s))  # 使用< 字符设定左对齐
    print('{0:^10d} {1:^10f} {2:^10s}'.format(n, f, s))  # 使用^ 字符设定居中

    '''
    新式格式化与旧式格式化相比有一处明显的不同：精度（precision，小数点后面的数字）
    对于浮点数而言仍然代表着小数点后的数字个数，
    对于字符串而言则代表着最大字符个数，
    但在新式格式化中你无法对整数设定精度
    '''
    # print('{0:>10.4d} {1:>10.4f} {2:10.4s}'.format(n, f, s))
    print('{0:>10d} {1:>10.4f} {2:10.4s}'.format(n, f, s))
    print('{0:!^20s}'.format('BIG SALE'))


def test_3():
    f = 7.03
    print('{t}'.format(t=f))  # 参数可以是字典或者命名变量


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
