#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/annulsluo/article/details/49534899
# python 中英混合字符串格式对齐所引发的思考


def is_chinese(uchar):
    """判断一个unicode是否是汉字"""
    if uchar >= u'\u4e00' and uchar <= u'\u9fa5':
        return True
    else:
        return False


def align(text, width, just="left"):
    stext = str(text)
    cn_count = 0

    for u in stext:
        if is_chinese(u):
            cn_count += 2  # 计算中文字符占用的宽度
        else:
            cn_count += 1  # 计算英文字符占用的宽度

    if just == "right":
        return " " * (width - cn_count) + stext
    elif just == "left":
        return stext + " " * (width - cn_count)


def string_ljust(text, width):
    return align(text, width, "left")


def string_rjust(text, width):
    return align(text, width, "right")


def main():
    print("str %s|" % string_rjust("中文", 10))
    print("str %s|" % string_rjust("ab", 10))
    print("str %s|" % string_rjust("a中文", 10))

    print("str %s|" % string_ljust("中文", 10))
    print("str %s|" % string_ljust("ab", 10))
    print("str %s|" % string_ljust("a中文", 10))


if __name__ == "__main__":
    main()
