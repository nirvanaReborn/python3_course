#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Unicode Code Charts 页面（http://www.unicode.org/charts/）包含了通往目前已定义的所有字符集的链接，且包含字符图示。
# 在Unicode 字符名称索引页（http://www.unicode.org/charts/charindex.html）可以查到字符对应的标准名称。
# 对字符串进行处理时，并不需要在意Python 中Unicode 字符的存储细节。
# 但当需要与外界进行数据交互时则需要完成两件事情：
# • 将字符串编码为字节； -----encode()
# • 将字节解码为字符串。

import unicodedata


def test_1():
    def unicode_test(value):
        name = unicodedata.name(value)
        value2 = unicodedata.lookup(name)
        print('value="%s", name="%s", value2="%s"' % (value, name, value2))

    unicode_test('A')  # 测试纯ASCII 字符
    unicode_test('$')  # 测试ASCII 标点符号
    unicode_test('\u00a2')  # Unicode 货币字符
    unicode_test('\u20ac')  # Unicode 货币字符
    unicode_test('\u2603')  # 尝试打印SNOWMAN 字符得到的结果，这里使用的是dingbat 字体


def test_2():
    place = 'café'
    print(place)
    place = 'caf\u00e9'
    print(place)
    place = 'caf\N{LATIN SMALL LETTER E WITH ACUTE}'
    print(place)

    u_umlaut = '\N{LATIN SMALL LETTER U WITH DIAERESIS}'
    drink = 'Gew' + u_umlaut + 'rztraminer'
    print('Now I can finally have my', drink, 'in a', place)


def test_3():
    # 字符串函数len 可以计算字符串中Unicode 字符的个数，而不是字节数：
    print(len('$'))
    print(len('\U0001f47b'))


def test_4():
    # 编码 : 编码是将字符串转化为一系列字节的过程。
    # UTF-8 是一种变长编码方式
    # 字符串的encode() 函数所接收的第一个参数是编码方式名。
    # encode() 函数可以接受额外的第二个参数来帮助你避免编码异常。它的默认值是'strict'，
    snowman = '\u2603'
    print(len(snowman))  # 1
    ds = snowman.encode('utf-8')  # strict
    print(len(ds))  # 3
    print(ds, type(ds))  # b'\xe2\x98\x83' <class 'bytes'>

    print(snowman.encode('ascii', 'ignore'))  # ignore

    # 'replace' 会将所有无法进行编码的字符替换为?：
    print(snowman.encode('ascii', 'replace'))  # replace

    # 'backslashreplace' 则会创建一个和unicode-escape 类似的Unicode 字符串
    print(snowman.encode('ascii', 'backslashreplace'))  # backslashreplace

    # 下面的代码可以用于创建网页中使用的字符实体串
    print(snowman.encode('ascii', 'xmlcharrefreplace'))  # xmlcharrefreplace


def test_5():
    place = 'caf\u00e9'
    print(place)
    place_bytes = place.encode('utf-8')
    print(place_bytes)
    place2 = place_bytes.decode('utf-8')
    print(place2)
    # place3 = place_bytes.decode('ascii')
    # print(place3)
    place4 = place_bytes.decode('latin-1')
    print(place4)
    place5 = place_bytes.decode('windows-1252')
    print(place5)


def test_6():
    # NFD和NFKD将可能的字符进行分解，而NFC和NFKC将可能的字符进行组合。
    print(unicodedata.normalize('NFD', '\u4e66\u7b7e\u680f'))
    print(unicodedata.normalize('NFKD', '\u4e66\u7b7e\u680f'))
    print(unicodedata.normalize('NFC', '\u4e66\u7b7e\u680f'))
    print(unicodedata.normalize('NFKC', '\u4e66\u7b7e\u680f'))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
