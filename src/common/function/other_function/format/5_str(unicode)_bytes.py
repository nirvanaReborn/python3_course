#!/usr/bin/env python
# -*- coding: UTF-8 -*-

'''
python3字符串编码总结-str(unicode)_bytes
http://blog.csdn.net/Setul/article/details/52203894

1. 字符串的编码形式
字符串的编码最一开始是ascii，使用8位二进制表示，因为英文就是编码的全部。
后来其他国家的语言加入进来，ascii就不够用了，所以一种万国码就出现了，它的名字就叫unicode。
unicode编码对所有语言使用两个字节，部分汉语使用三个字节。
但是这就导致一个问题，就是unicode不仅不兼容ascii编码，而且会造成空间的浪费，
于是uft-8编码应运而生了，utf-8编码对英文使用一个字节的编码，由于这样的特点，很快得到全面的使用。

2. 字节码bytes
python3中bytes用b’xxx’表示，其中的x可以用字符，也可以用ascii表示。
python3中的二进制文件（如文本文件）统一采用字节码读写。

python3默认使用的是str类型对字符串编码，默认使用bytes操作二进制数据流，两者不能混淆！！

Python3有两种表示字符序列的类型：bytes和str。
前者的实例包含原始的8位值，
后者的实例包含Unicode字符。

Python2也有两种表示字符序列的类型，分别叫做str和Unicode，
与Python3不同的是，str实例包含原始的8位值；
而unicode的实例，则包含Unicode字符。

'''

import sys
import ctypes


def compatible_conver(filed):
    if sys.version_info[0] == 3:
        if isinstance(filed, str):
            return ''.encode() if filed is None else filed.encode()
        elif isinstance(filed, ctypes.c_char_p):
            return ''.encode() if filed.value is None else filed
    return filed


def batch_compatible_conver(*filed):
    if sys.version_info[0] == 3:
        filed = tuple(map(compatible_conver, filed))
    return filed


# 为了方便开发者的使用，可以编写函数，无论输入的是str还是bytes类型都输出str。
def to_str(bytes_or_str):
    if isinstance(bytes_or_str, bytes):
        value = bytes_or_str.decode('utf-8')
    else:
        value = bytes_or_str
    return value  # Instance of str


# 为了方便开发者的使用，可以编写函数，无论输入的是str还是bytes类型都输出bytes。
def to_bytes(bytes_or_str):
    if isinstance(bytes_or_str, str):
        value = bytes_or_str.encode('utf-8')
    else:
        value = bytes_or_str
    return value  # Instance of bytes


# 字节码的使用举例
def test_1():
    b = b'asd\64'
    print(b)
    print(type(b))  # <class 'bytes'>


# 要修改bytes中的某一个字节必须要将其转换为bytearray以后才可以。
def test_2():
    b = b'asd\64'
    c = bytearray(b)
    print(type(c))  # <class 'bytearray'>
    print(c)  # bytearray(b'asd4')
    c[0] = 110
    print(c)  # bytearray(b'nsd4')


# 字节码bytes与字符之间的关系
# 将表示二进制的bytes进行适当编码就可以变为字符了，比如utf-8或是gbk等等编码格式都可以。
# （所以我个人的理解就是：有utf-8格式的bytes，也有gbk格式的bytes等等）。
def test_3():
    pass


# 字节码bytes与unicode编码的相互转换
def test_4():
    pass


# 字节码bytes与str类型的相互转换
# str(unicode)类型是基准！要将str类型转化为bytes类型，使用encode()内置函数；
# 反过来，使用decode()函数。
def test_5():
    oath = '我爱中国'
    print(type(oath))  # <class 'str'>
    oath = oath.encode('utf-8')  # str ----> bytes
    print(type(oath))  # <class 'bytes'>
    print(oath)  # b'\xe6\x88\x91\xe7\x88\xb1\xe4\xb8\xad\xe5\x9b\xbd'
    oath = oath.decode()  # bytes ----> str
    print(type(oath))  # <class 'str'>
    print(oath)  # 我爱中国


# 文件的编码
# 在open文件的时候用“wb”方式打开，即二进制写的方式，
# 所以下面的write函数对象用的是bytes类型的b'sui'，这个时候如果使用f.write('sui')就会出错的。
def test_6():
    with open('sun.bin', 'wb') as f:
        f.write(b'sui')


# 网页的编码
def test_7():
    import urllib.request
    response = urllib.request.urlopen('http://www.baidu.com')
    html = response.read()
    # print(html)
    print(type(html))  # <class 'bytes'>
    print(html.decode())  # 所以要正常显示的话，就要使用decode()方法了。


def test_8():
    # bytes object
    b = b"example"

    # str object
    s = "example"

    # str to bytes
    print(bytes("example", encoding="utf8"))  # b'example'
    print(str.encode("example"))  # b'example'

    # bytes to str
    print(str(b"example", encoding="utf8"))  # example
    print(bytes.decode(b"example"))  # example


def test_9():
    a = "str1"
    b = "str2"
    c = "str3"
    a = compatible_conver(a)
    print(a, b, c)
    a, b, c = batch_compatible_conver(a, b, c)
    print(a, b, c)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
