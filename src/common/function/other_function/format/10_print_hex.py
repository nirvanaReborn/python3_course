#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# python打印16进制

def test_1():
    '''使用 hex() 函数'''
    number = 255
    hex_string = hex(number)
    print(hex_string)  # 输出: 0xff


def test_2():
    '''使用 format() 函数'''
    number = 255
    hex_string = format(number, 'x')  # 小写十六进制，不带前缀
    hex_string_with_prefix = format(number, '#x')  # 小写十六进制，带前缀
    hex_string_upper = format(number, 'X')  # 大写十六进制，不带前缀
    hex_string_upper_with_prefix = format(number, '#X')  # 大写十六进制，带前缀

    print(hex_string)  # 输出: ff
    print(hex_string_with_prefix)  # 输出: 0xff
    print(hex_string_upper)  # 输出: FF
    print(hex_string_upper_with_prefix)  # 输出: 0XFF


def test_3():
    '''使用格式化字符串（f-strings）'''
    number = 255
    hex_string = f"{number:x}"  # 小写十六进制，不带前缀
    hex_string_with_prefix = f"{number:#x}"  # 小写十六进制，带前缀
    hex_string_upper = f"{number:X}"  # 大写十六进制，不带前缀
    hex_string_upper_with_prefix = f"{number:#X}"  # 大写十六进制，带前缀

    print(hex_string)  # 输出: ff
    print(hex_string_with_prefix)  # 输出: 0xff
    print(hex_string_upper)  # 输出: FF
    print(hex_string_upper_with_prefix)  # 输出: 0XFF


def test_4():
    '''使用 % 格式化'''
    number = 255
    hex_string = "%x" % number  # 小写十六进制，不带前缀
    hex_string_with_prefix = "%#x" % number  # 小写十六进制，带前缀
    hex_string_upper = "%X" % number  # 大写十六进制，不带前缀
    hex_string_upper_with_prefix = "%#X" % number  # 大写十六进制，带前缀

    print(hex_string)  # 输出: ff
    print(hex_string_with_prefix)  # 输出: 0xff
    print(hex_string_upper)  # 输出: FF
    print(hex_string_upper_with_prefix)  # 输出: 0XFF


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
