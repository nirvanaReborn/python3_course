#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 格式化打印

info = [{"id": 1580615, "name": "皮的嘛", "packageName": "com.renren.mobile.android", "iconUrl": "app/com.renren.mobile.android/icon.jpg", "stars": 2,
         "size": 21803987, "downloadUrl": "app/com.renren.mobile.android/com.renren.mobile.android.apk",
         "des": "2011-2017你的铁头娃一直在这儿。中国最大的实名制SNS网络平台，嫩头青"},
        {"id": 1540629, "name": "不存在的", "packageName": "com.ct.client", "iconUrl": "app/com.ct.client/icon.jpg", "stars": 2, "size": 4794202,
         "downloadUrl": "app/com.ct.client/com.ct.client.apk", "des": "斗鱼271934走过路过不要错过，这里有最好的鸡儿"}]


def test_1():
    from pprint import pprint
    pprint(info, indent=4, width=100)


def test_2():
    '''与 pprint 相比 json.dumps 可以说完胜：
    1.两个参数就能实现所有我的需求（打印中文与双引号）
    2.就算在 Python 2 下，使用中文也不需要用 u'中文' 这种写法
    3.Python2 和 Python3 的写法完全一致，对于这一点不需要考虑兼容问题
    '''
    import json
    # indent=4：以 4 个空格为缩进单位
    # ensure_ascii=False：接收非 ASCII 编码的字符，这样才能使用中文
    print(json.dumps(info, indent=4, ensure_ascii=False))


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
