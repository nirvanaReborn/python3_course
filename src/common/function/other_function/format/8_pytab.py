#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.linuxidc.com/Linux/2020-05/163219.htm
# 使用Python轻松制作漂亮的表格

# pip install pytab
import pytab as pt


def main():
    data = {
        'Linux': [29, 23, 29, 20, 25, 23, 26],
        'LinuxMi': [26, 23, 29, 28, 20, 22, 29],
    }

    pt.table(
        data=data,
        th_type='dark',
        table_type='striped'
    )

    pt.show()


if __name__ == "__main__":
    main()
