#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# python打印二进制

def test_1():
    '''使用 hex() 函数(Python 3.5 及以上版本)'''
    byte_str = b'\xf7\x1e\x14k3\xcag\xeaW\x19[~\xed\x1eI\x10b\x16\x91\x89G\xab\xad\xbdL#\xdau\xe4\x84\x88\xc9'
    hex_str = byte_str.hex()
    # hex_str = byte_str.hex().upper() # 大写格式的十六进制字符串

    print(hex_str)
    # 输出: f71e146b33ca67ea57195b7eed1e49106216918947abadbd4c23da75e48488c9


def test_2():
    '''使用 str.join 和 format'''
    byte_str = b'\xf7\x1e\x14k3\xcag\xeaW\x19[~\xed\x1eI\x10b\x16\x91\x89G\xab\xad\xbdL#\xdau\xe4\x84\x88\xc9'
    hex_str = ''.join(format(x, '02x') for x in byte_str)

    print(hex_str)
    # 输出: f71e146b33ca67ea57195b7eed1e49106216918947abadbd4c23da75e48488c9


def test_3():
    '''使用 binascii 模块'''
    import binascii

    byte_str = b'\xf7\x1e\x14k3\xcag\xeaW\x19[~\xed\x1eI\x10b\x16\x91\x89G\xab\xad\xbdL#\xdau\xe4\x84\x88\xc9'
    hex_str = binascii.hexlify(byte_str).decode('ascii')

    print(hex_str)
    # 输出: f71e146b33ca67ea57195b7eed1e49106216918947abadbd4c23da75e48488c9


def test_4():
    '''使用 % 格式化'''
    number = 255
    hex_string = "%x" % number  # 小写十六进制，不带前缀
    hex_string_with_prefix = "%#x" % number  # 小写十六进制，带前缀
    hex_string_upper = "%X" % number  # 大写十六进制，不带前缀
    hex_string_upper_with_prefix = "%#X" % number  # 大写十六进制，带前缀

    print(hex_string)  # 输出: ff
    print(hex_string_with_prefix)  # 输出: 0xff
    print(hex_string_upper)  # 输出: FF
    print(hex_string_upper_with_prefix)  # 输出: 0XFF


def test_5():
    '''确认数据编码'''
    byte_data = b'\x8f\x1e\x14k3\xca'  # 示例字节数据，可能需要更换为实际数据

    for encoding in ["utf-8", "latin1"]:
        try:
            decoded_str = byte_data.decode(encoding)
            print(f"{encoding} 编码: {decoded_str}")
        except UnicodeDecodeError:
            print(f"无法使用 {encoding} 解码")


def test_6():
    '''忽略错误'''
    byte_data = b'\x8f\x1e\x14k3\xca'

    decoded_str = byte_data.decode('utf-8', errors='ignore')
    print(decoded_str)  # 输出将忽略无效字节


def test_7():
    '''使用替代字符'''
    byte_data = b'\x8f\x1e\x14k3\xca'

    decoded_str = byte_data.decode('utf-8', errors='replace')
    print(decoded_str)  # 无效字节将被替代字符取代


# -------------------------------------------------------------------------------
# 处理非文本字节数据在许多应用程序中是很常见的，例如加密、压缩、图像处理等。
# 因为这些数据通常不是以人类可读的格式存在，所以不需要将它们解码为字符串。

def test_10():
    # 读取二进制文件
    with open('data.bin', 'rb') as file:
        binary_data = file.read()

    # 处理二进制数据
    # 示例：计算二进制数据的长度
    data_length = len(binary_data)
    print(f"Data length: {data_length} bytes")

    # 写入二进制文件
    with open('output.bin', 'wb') as file:
        file.write(binary_data)


def test_11():
    '''使用 struct 模块解析二进制数据'''
    import struct

    # 假设我们有一个包含 4 字节整数和 8 字节浮点数的字节数据
    binary_data = b'\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\xf0\x3f'

    # 解包字节数据
    integer, float_number = struct.unpack('iQd', binary_data)
    print(f"Integer: {integer}, Float: {float_number}")

    # 打包数据
    packed_data = struct.pack('iQd', 1, 0, 1.0)
    print(f"Packed Data: {packed_data}")


def test_12():
    '''使用 binascii 模块解析二进制数据'''
    import binascii

    binary_data = b'\xf7\x1e\x14k3\xcag\xeaW\x19[~\xed\x1eI\x10b\x16\x91\x89G\xab\xad\xbdL#\xdau\xe4\x84\x88\xc9'

    # 将字节数据转换为十六进制表示
    hex_string = binascii.hexlify(binary_data)
    print(f"Hex String: {hex_string}")

    # 将十六进制字符串转换回字节数据
    original_data = binascii.unhexlify(hex_string)
    print(f"Original Data: {original_data}")


def test_13():
    '''使用 hashlib 模块进行哈希计算'''
    import hashlib

    binary_data = b'my binary data'

    # 计算 MD5 哈希值
    md5_hash = hashlib.md5(binary_data).hexdigest()
    print(f"MD5 Hash: {md5_hash}")

    # 计算 SHA-256 哈希值
    sha256_hash = hashlib.sha256(binary_data).hexdigest()
    print(f"SHA-256 Hash: {sha256_hash}")


def test_14():
    '''使用 cryptography 模块进行加密和解密'''
    from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
    from cryptography.hazmat.backends import default_backend

    # 加密
    key = b'sixteen byte key'  # 16 字节密钥
    iv = b'sixteen byte iv '  # 16 字节初始化向量
    cipher = Cipher(algorithms.AES(key), modes.CFB(iv), backend=default_backend())
    encryptor = cipher.encryptor()

    plaintext = b'my binary data'
    ciphertext = encryptor.update(plaintext) + encryptor.finalize()
    print(f"Ciphertext: {ciphertext}")

    # 解密
    decryptor = cipher.decryptor()
    decrypted_text = decryptor.update(ciphertext) + decryptor.finalize()
    print(f"Decrypted Text: {decrypted_text}")


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
