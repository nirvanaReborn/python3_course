#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/jueblog/article/details/50512705
# Python中中文的字符串格式化对齐
# https://github.com/Jueee/04-LiaoXueFeng/blob/master/81-Chinese.py


def format_Chinese(data, size):
    count = 0
    for s in data:
        if ord(s) > 127:
            count += 1
    newStr = '{0:{wd}}'.format(data, wd=size - count)
    return newStr


def main():
    c = [
        '决',
        '决决',
        '决决决',
        '决决决决',
        '决决决决决',
        '决决决决决决',
        '决决决决决决决'
    ]
    print('----正常字符串格式化：----')
    for i in range(len(c)):
        print('|%20s|' % c[i])
    print('----通过函数计算长度格式化：----')
    for i in range(len(c)):
        newStr = format_Chinese(c[i], 20)
        print('|%s|' % newStr)


if __name__ == "__main__":
    main()
