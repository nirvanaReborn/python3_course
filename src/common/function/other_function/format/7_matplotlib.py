#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.linuxidc.com/Linux/2020-05/163219.htm
# 使用Python轻松制作漂亮的表格

import matplotlib.pyplot as plt
import pandas as pd


def test_1():
    data = {
        'linux': [1.2, 2.2, 3.1, '-', 2.0, 1.0, 2.1, 3.5, 4.0, 2.0, ],
        'linuxmi': [5.2, 6.7, 7.9, 8.3, 1.2, 5.7, 6.1, 7.2, 8.3, '-', ],
    }

    df = pd.DataFrame(data)

    fig, ax = plt.subplots(figsize=(3, 3))

    ax.axis('off')
    ax.axis('tight')

    ax.table(cellText=df.values,
             colLabels=df.columns,
             bbox=[0, 0, 1, 1],
             )

    plt.show()


def test_2():
    data = {
        'Linux': [29, 23, 29, 20, 25, 23, 26],
        'linuxmi': [26, 23, 29, 28, 24, 22, 29],
    }

    df = pd.DataFrame(data)

    fig, ax = plt.subplots(figsize=(3, 3))

    ax.axis('off')
    ax.axis('tight')

    tb = ax.table(cellText=df.values,
                  colLabels=df.columns,
                  bbox=[0, 0, 1, 1],
                  )

    # 改变设计,新增如下代码
    tb[0, 0].set_facecolor('#363636')
    tb[0, 1].set_facecolor('#363636')
    tb[0, 0].set_text_props(color='w')
    tb[0, 1].set_text_props(color='w')

    plt.show()


def main():
    test_2()


if __name__ == "__main__":
    main()
