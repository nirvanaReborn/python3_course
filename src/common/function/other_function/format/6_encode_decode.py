#!/usr/bin/env python
# -*- coding: utf-8 -*-

# https://foofish.net/how-python3-handle-charset-encoding.html

'''
python2 与 python3 字节与字符的对应关系

python2	python3	表现	转换	作用
str	    bytes	字节	encode	存储
unicode	str	    字符	decode	显示

首先，Python3 把系统默认编码设置为 UTF-8。

然后，文本字符和二进制数据区分得更清晰，分别用 str 和 bytes 表示。
文本字符全部用 str 类型表示，str 能表示 Unicode 字符集中所有字符，
而二进制字节数据用一种全新的数据类型，用 bytes 来表示。

Python3 中，在字符引号前加‘b’，明确表示这是一个 bytes 类型的对象，
实际上它就是一组二进制字节序列组成的数据，
bytes 类型可以是 ASCII范围内的字符和其它十六进制形式的字符数据，但不能用中文等非ASCII字符表示。

bytes 类型提供的操作和 str 一样，支持分片、索引、基本数值运算等操作。
但是 str 与 bytes 类型的数据不能执行 + 操作，尽管在py2中是可行的。
'''


def test_1():
    a = "a"
    print(type(a))

    b = "禅"
    print(type(b))


def test_2():
    c = b'a'
    print(type(c))

    d = b'\xe7\xa6\x85'
    print(type(d))

    # e = b'禅'
    # print(type(e))
    # SyntaxError: bytes can only contain ASCII literal characters.


def test_3():
    print(b"a" + b"c")
    print(b"a" * 2)
    print(b"abcdef\xd6"[1:])
    print(b"abcdef\xd6"[-1])
    # print(b"a" + "b") # TypeError: can't concat bytes to str


def test_4():
    s = "Python之禅"
    print(s.encode())
    print(s.encode("gbk"))
    print(b'Python\xe4\xb9\x8b\xe7\xa6\x85'.decode())
    print(b'Python\xd6\xae\xec\xf8'.decode("gbk"))


def main():
    test_1()
    test_2()
    test_3()
    test_4()


if __name__ == '__main__':
    main()
