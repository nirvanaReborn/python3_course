#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 2.进程池
# https://docs.python.org/3/library/multiprocessing.html#module-multiprocessing.pool
# Pool类的主要方法如下。
# apply： 这个方法在独立的子进程中运行一个函数。 它还会在被调用函数返回结果之前阻塞进程。
#
# apply_async： 这个方法会在独立子进程中异步地运行一个函数，就是说进程会立即返回一个ApplyResult对象。
#              要获得真实的返回值， 需要使用get()方法。 get()在异步执行的函数结束之前都会被阻塞。
#
# map： 这个方法对一组数值应用同一个函数。 它是一个阻塞动作，所以返回值是每个值经过函数映射的列表。


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
