#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.jb51.net/article/53918.htm
# Python中多线程threading的实现方法
import threading
import time


def test_1():
    def threading_test(a):
        global count, mutex
        # 获得线程名
        threadname = threading.currentThread().getName()

        for x in range(0, int(a)):
            # 取得锁
            mutex.acquire()
            count = count + 1
            # 释放锁
            mutex.release()
            print(threadname, x, count)
            time.sleep(1)

    global count, mutex
    threads = []
    num = 4
    count = 0
    # 创建一个锁
    mutex = threading.Lock()
    # 先创建线程对象
    for x in range(0, num):
        threads.append(threading.Thread(target=threading_test, args=(str(3),)))

    # 启动所有线程
    for t in threads:
        t.start()

    # 主线程中等待所有子线程退出
    for t in threads:
        t.join()


def test_2():
    class Test(threading.Thread):
        def __init__(self, num):
            threading.Thread.__init__(self)
            self._run_num = num

        def run(self):
            global count, mutex
            threadname = threading.currentThread().getName()

            for x in range(0, int(self._run_num)):
                mutex.acquire()
                count = count + 1
                mutex.release()
                print(threadname, x, count)
                time.sleep(1)

    global count, mutex
    threads = []
    num = 4
    count = 0
    # 创建锁
    mutex = threading.Lock()
    # 创建线程对象
    for x in range(0, num):
        threads.append(Test(3))

    # 启动线程
    for t in threads:
        t.start()

    # 等待子线程结束
    for t in threads:
        t.join()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
