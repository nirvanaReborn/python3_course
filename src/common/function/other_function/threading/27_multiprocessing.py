#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 3.进程间通信 -- Queue（队列） 方法
# 之前已经提过， 进程间通信的方式不像线程间通信那么简单。 但是，Python提供了一些工具帮助我们解决问题。
# Queue类是一个既线程安全又进程安全的先进先出（ FIFO， first in first out）数据交换机制。
# https://docs.python.org/3/library/multiprocessing.html#exchangingobjects-between-processes
# multiprocessing提供的Queue类基本是Queue.Queue的克隆版本， 因此两者API基本相同。

import random
from multiprocessing import Queue, Process


def generate(queue):
    while True:
        value = random.randrange(10)
        queue.put(value)
        print("Value added to queue: %s" % (value))


def reader(queue):
    while True:
        value = queue.get()
        print("Value from queue: %s" % (value))


if __name__ == "__main__":
    queue = Queue()
    p1 = Process(target=generate, args=(queue,))
    p2 = Process(target=reader, args=(queue,))
    p1.start()
    p2.start()
