#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.jianshu.com/p/972c040fd65d

import trio
import numpy as np


async def thread1(send_channel):
    print("线程1，公司老板")
    async with send_channel:
        for i in range(10):
            await trio.sleep(1.)  # 休息
            cmd = np.random.randint(10)
            print("老板发出指示：{!r}".format(cmd))  # 给秘书发指示
            await send_channel.send(cmd)


async def thread2(receive_channel):
    print("线程2，秘书")
    async with receive_channel:
        async for value in receive_channel:  # 等待老板指示
            print("秘书收到指示：{!r}".format(value))


async def main():
    async with trio.open_nursery() as nursery:
        send_channel, receive_channel = trio.open_memory_channel(0)  # 创建通道，传入通道对象
        nursery.start_soon(thread1, send_channel)
        nursery.start_soon(thread2, receive_channel)


if __name__ == "__main__":
    trio.run(main)
