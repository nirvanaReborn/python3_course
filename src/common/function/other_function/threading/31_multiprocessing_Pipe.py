#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/ctwy291314/article/details/89358144
'''
正如我们在Linux多线程中介绍的管道PIPE和消息队列message queue，
multiprocessing包中有Pipe类和Queue类来分别支持这两种IPC机制。
Pipe和Queue可以用来传送常见的对象。

Pipe可以是单向(half-duplex)，也可以是双向(duplex)。
我们通过mutiprocessing.Pipe(duplex=False)创建单向管道 (默认为双向)。
一个进程从PIPE一端输入对象，然后被PIPE另一端的进程接收，
单向管道只允许管道一端的进程输入，而双向管道则允许从两端输入。
下面的程序展示了Pipe的使用:
Pipe对象建立的时候，返回一个含有两个元素的表，每个元素代表Pipe的一端(Connection对象)。
我们对Pipe的某一端调用send()方法来传送对象，在另一端使用recv()来接收。
'''

import multiprocessing


def proc1(pipe):
    pipe.send('hello')
    print('proc1 rec:', pipe.recv())


def proc2(pipe):
    print('proc2 rec:', pipe.recv())
    pipe.send('hello, too')


# Build a pipe
pipe = multiprocessing.Pipe()


if __name__ == '__main__':
    # Pass an end of the pipe to process 1
    p1 = multiprocessing.Process(target=proc1, args=(pipe[0],))
    # Pass the other end of the pipe to process 2
    p2 = multiprocessing.Process(target=proc2, args=(pipe[1],))
    p1.start()
    p2.start()
    p1.join()
    p2.join()
