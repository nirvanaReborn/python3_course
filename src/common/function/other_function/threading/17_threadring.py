#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.cnblogs.com/tyomcat/p/5486827.html
# python并发编程之多进程、多线程、异步和协程
# 详情请看：Linux多线程与同步——http://www.cnblogs.com/vamei/archive/2012/10/09/2715393.html

# 一、多线程
'''
多线程就是允许一个进程内存在多个控制权，以便让多个函数同时处于激活状态，从而让多个函数的操作同时运行。
即使是单CPU的计算机，也可以通过不停地在不同线程的指令间切换，从而造成多线程同时运行的效果。
多线程相当于一个并发(concunrrency)系统。并发系统一般同时执行多个任务。
如果多个任务可以共享资源，特别是同时写入某个变量的时候，就需要解决同步的问题，
比如多线程火车售票系统：两个指令，一个指令检查票是否卖完，另一个指令，多个窗口同时卖票，可能出现卖出不存在的票。
在并发情况下，指令执行的先后顺序由内核决定。
同一个线程内部，指令按照先后顺序执行，但不同线程之间的指令很难说清除哪一个会先执行。
因此要考虑多线程同步的问题。同步(synchronization)是指在一定的时间内只允许某一个线程访问某个资源。 
'''


# thread模块
def test_1():
    pass


# threading模块
def test_2():
    import threading
    import time
    import os
    global i
    global lock

    def booth(tid):
        global i
        global lock
        while True:
            lock.acquire()
            if i != 0:
                i = i - 1
                print("窗口:", tid, ",剩余票数:", i)
                time.sleep(1)
            else:
                print("Thread_id", tid, "No more tickets")
                os._exit(0)
            lock.release()
            time.sleep(1)

    i = 10
    lock = threading.Lock()
    for k in range(10):
        new_thread = threading.Thread(target=booth, args=(k,))
        new_thread.start()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
