#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
http://xiaorui.cc/archives/4542
https://blog.csdn.net/weixin_39755712/article/details/111434443
http://www.360doc.com/content/19/0114/12/58006001_808766007.shtml

看了下代码,一共也就3个方法,用起来很方便：
_get_handle_for_pid(pid, ro=True)
通过一个pid获取进程

get_process_affinity_mask(pid)
通过pid获取这个进程当前的affinity mask相关掩码 ,返回一个长整形('2l',就是在用2这个cpu)

set_process_affinity_mask(pid, value)
绑定pid这个进程到value所指的这个cpu上去(set_process_affinity_mask(3516, 1),就是用1这个cpu去执行pid为3516这个进程)
'''
import psutil
# import affinity
import multiprocessing, time, signal


def test_1():
    pass


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


if __name__ == "__main__":
    p = multiprocessing.Process(target=time.sleep, args=(1,))
    p.start()
    # pid = p.pid
    # print(pid)
    #
    # # 先看了下这个进程用的是那个cpu
    # print(affinity.get_process_affinity_mask(pid))
    #
    # # 将这个进程用2这个cpu跑
    # affinity.set_process_affinity_mask(pid, 2)

    count = psutil.cpu_count()
    p = psutil.Process()
    cpu_lst = p.cpu_affinity()
    print(cpu_lst)  # [0, 1, 2, 3, 4, 5, 6, 7]
    p.cpu_affinity([0, 1])  # 将进程绑定到0和1这两个cpu核上
