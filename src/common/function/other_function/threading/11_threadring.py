#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/shennongzhaizhu/article/details/51544151
# http://python.jobbole.com/81546/
# https://www.jianshu.com/p/2e98a5b5a248
'''
多线程编程中可能用得到的模块:
模块                    描述
thread                  基本的、低级别的线程模块(python3中已经改名为_thread)
threading               高级别的线程和同步对象
Queue                   供多线程使用的同步先进先出（FIFO）队列
mutex                   互斥对象
SocketServer            具有线程控制的TCP和UDP管理器
# --------------------------------------------------------------------------------------------------
threading模块对象        描述
Thread                  表示一个线程的执行的对象
Lock                    锁原语对象（跟thread模块里的锁对象相同）
RLock                   可重入锁对象。使单线程可以再次获得已经获得了的锁（递归锁定）
Condition               条件变量对象能让一个线程停下来，等待其他线程满足了某个“条件”。如状态的改变或值的改变
Event                   通用的条件变量。多个线程可以等待某个时间的发生，在事件发生后，所有的线程都被激活
Semaphore               为等待锁的线程提供一个类似“等候室”的结构
BoundedSemaphore        与Semaphore类似，只是它不允许超过初始值
Timer                   与thread类似，只是它要等待一段时间后才开始运行
# --------------------------------------------------------------------------------------------------
函数                     描述
start()                 开始线程的执行
run()                   定义线程的功能的函数（一般会被子类重写）
join(timeout=None)      程序挂起，直到线程结束；如果给了timeout，则最多阻塞timeout秒
getName()               返回线程的名字
setName(name)           设置线程的名字
isAlive()               布尔标志，表示这个线程是否还在运行中
isDaemon()              返回线程的daemon标志
setDaemon(daemonic)     把线程的daemon标志设为daemonic（一定要在调用start()函数前调用）
ident()                 返回当前线程的 ID
'''

import threading
import time

global_value = 0


def run(threadName, lock):
    global global_value
    # time.sleep(1)
    lock.acquire()
    # time.sleep(1)
    local_copy = global_value
    print("%s with value %s" % (threadName, local_copy))
    global_value = local_copy + 1
    lock.release()


def test_1():
    lock = threading.Lock()
    for i in range(10):
        t = threading.Thread(target=run, args=("Thread-" + str(i), lock))
        t.start()


# -----------------------------------------------------------------------
def test_2():
    class MyThread(threading.Thread):
        def __init__(self, count):
            threading.Thread.__init__(self)
            self.total = count

    def run(self):
        for i in range(self.total):
            time.sleep(1)
            print("Thread: %s - %s" % (self.name, i))

    t = MyThread(4)
    t2 = MyThread(3)
    t.start()
    t2.start()
    print("This program has finished")


# 程序在其他内容出现之前先发送退出消息。在这里这并不是什么大问题。但是，在下面这种情况下就有问题了：
def test_3():
    class MyThread(threading.Thread):
        def __init__(self, count, fp):
            threading.Thread.__init__(self)
            self.total = count

    def run(self):
        for i in range(self.total):
            time.sleep(1)
            print("Thread: %s - %s" % (self.name, i))

    f = open("output-file.txt", "w+")
    t = MyThread(4, f)
    t2 = MyThread(3, f)
    t.start()
    t2.start()
    f.close()  # 两个线程都已经完成，关闭文件处理器
    print("This program has finished")


# 如果从主线程中调用join方法，它可以保证在两个线程执行完成之前，程序不会运行主线程命令。
def test_4():
    class MyThread(threading.Thread):
        def __init__(self, count, fp):
            threading.Thread.__init__(self)
            self.total = count

    def run(self):
        for i in range(self.total):
            time.sleep(1)
            print("Thread: %s - %s" % (self.name, i))

    f = open("output-file.txt", "w+")
    t = MyThread(4, f)
    t2 = MyThread(3, f)
    t.start()
    t2.start()
    # 两个线程同时运行，停止主线程
    t.join()
    t2.join()
    f.close()  # 两个线程都已经完成，关闭文件处理器
    print("This program has finished")


# -----------------------------------------------------------------------
import urllib.request

sites = [
    "http://www.google.com",
    "http://www.bing.com",
    "http://stackoverflow.com",
    "http://facebook.com",
    "http://twitter.com"
]


class HTTPStatusChecker(threading.Thread):
    def __init__(self, url):
        threading.Thread.__init__(self)
        self.url = url
        self.status = None

    def getURL(self):
        return self.url

    def getStatus(self):
        return self.status

    def run(self):
        self.status = urllib.request.urlopen(self.url).getcode()


def test_5():
    threads = []
    for url in sites:
        t = HTTPStatusChecker(url)
        t.start()  # 启动线程
        threads.append(t)

    # 让主线程join其他子线程，
    # 这样我们就可以在子线程完成后打印全部的结果
    for t in threads:
        t.join()
    for t in threads:
        print("%s: %s" % (t.url, t.status))


# -----------------------------------------------------------------------
class ThreadA(threading.Thread):
    def __init__(self, event):
        threading.Thread.__init__(self)
        self.event = event

    def run(self):
        count = 0

        while count < 5:
            time.sleep(1)
            if self.event.is_set():
                print("A")
                # clear: 重置内部标记。
                # 调用wait()方法的线程，在调用set()方法之前都将被阻塞。
                self.event.clear()
            count += 1


class ThreadB(threading.Thread):
    def __init__(self, evnt):
        threading.Thread.__init__(self)
        self.event = evnt

    def run(self):
        count = 0
        while count < 5:
            time.sleep(1)
            if not self.event.is_set():
                print("B")
                # set：把内部标记设置为True。
                # 它可以唤醒等待被设置标记的所有线程。
                # 调用wait()方法的线程将不再被阻塞。
                self.event.set()
            count += 1


def test_6():
    event = threading.Event()
    ta = ThreadA(event)
    tb = ThreadB(event)
    ta.start()
    tb.start()


def main():
    # print("执行脚本耗时 %s 秒" % __import__('timeit').timeit(test_1, number=1))
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
