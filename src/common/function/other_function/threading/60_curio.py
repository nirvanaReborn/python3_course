#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Curio 是 Python 第三方实现的协程库。
# Python 的并发编程 async/await 是一套接口，允许第三方实现，官方的实现是标准库asyncio。
# Curio 相比官方的 asyncio 使用起来更简单直观一些。
# pip install curio

from curio import run, tcp_server


async def echo_client(client, addr):
    print('Connection from', addr)
    while True:
        data = await client.recv(100000)
        if not data:
            break
        await client.sendall(data)
    print('Connection closed')


if __name__ == '__main__':
    run(tcp_server, '', 25000, echo_client)
