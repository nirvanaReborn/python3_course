#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
#
import multiprocessing
from multiprocessing import shared_memory


def f(ls):
    ls[1] = 99
    ls[2] += 99
    ls.shm.close()

    return


if __name__ == "__main__":
    x = shared_memory.ShareableList([1, 2, 3])
    print(x)  # ShareableList([1, 2, 3], name='wnsm_8ff28f7b')

    p = multiprocessing.Process(target=f, args=(x,))
    p.start()
    p.join()

    for _ in range(len(x)):
        print(x[_], end=", ")  # 1, 99, 102

    x.shm.close()
    x.shm.unlink()
