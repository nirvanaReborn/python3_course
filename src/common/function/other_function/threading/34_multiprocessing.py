#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/ctwy291314/article/details/89358144
# 共享内存


import multiprocessing


# Value/Array
def func1(a, arr):
    a.value = 3.14
    for i in range(len(arr)):
        arr[i] = 0
    a.value = 0


if __name__ == '__main__':
    num = multiprocessing.Value('d', 1.0)  # num=0
    arr = multiprocessing.Array('i', range(10))  # arr=range(10)
    p = multiprocessing.Process(target=func1, args=(num, arr))
    p.start()
    p.join()
    print(num.value)
    print(arr[:])
'''
0.0
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

这里我们实际上只有主进程和Process对象代表的进程。
我们在主进程的内存空间中创建共享的内存，也就是Value和Array两个对象。
对象Value被设置成为双精度数(d), 并初始化为1.0。
而Array则类似于C中的数组，有固定的类型(i, 也就是整数)。
在Process进程中，我们修改了Value和Array对象。
回到主程序，打印出结果，主程序也看到了两个对象的改变，说明资源确实在两个进程之间共享。
'''
