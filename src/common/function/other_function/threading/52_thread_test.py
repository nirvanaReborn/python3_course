#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.jianshu.com/p/cac56b3d9a18
# 方法 4：asyncio
import asyncio

from databases import Database

user = xx
passwd = xx
port = xx
hosts = []  # 500台 db列表


async def query(host):
    DATABASE_URL = f'mysql+pymysql://{user}:{passwd}@{host}:{port}/mysql?charset=utf8mb4'
    async with Database(DATABASE_URL) as database:
        query = 'select sleep(0.1);'
        rows = await database.fetch_all(query=query)
        print(rows[0])


async def main():
    tasks = [asyncio.create_task(query(host)) for host in hosts]
    await asyncio.gather(*tasks)


# main entrance
if __name__ == '__main__':
    asyncio.run(main())
