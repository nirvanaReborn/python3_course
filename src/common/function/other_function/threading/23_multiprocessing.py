#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/cskchenshengkun/article/details/45770627
# 多进程并发与多线程并发总结

# 对于windows系统的多进程并发，程序文件里必须含有“入口函数”（如main函数），且结尾处必须调用入口点。
# 例如以if __name__ == '__main__': main()结尾。
'''
一.多进程并发
1.队列实现——使用multiprocessing.JoinableQueue
multiprocessing是python标准库中支持多进程并发的模块，我们这里采用multiprocessing中的数据结构：JoinableQueue，
它本质上仍是一个FIFO的队列，它与一般队列（如queue中的Queue)的区别在于它是多进程安全的，这意味着我们不用担心它的互斥和死锁问题。
JoinableQueue主要可以用来存放执行的任务和收集任务的执行结果。
举例来看（以下皆省去导入包的过程）：
'''
import multiprocessing
import random
import time


def read(q):
    while True:
        try:
            value = q.get()
            print('Get %s from queue.' % value)
            time.sleep(random.random())
        finally:
            q.task_done()


def main():
    q = multiprocessing.JoinableQueue()
    pw1 = multiprocessing.Process(target=read, args=(q,))
    pw2 = multiprocessing.Process(target=read, args=(q,))
    pw1.daemon = True
    pw2.daemon = True
    pw1.start()
    pw2.start()
    for c in [chr(ord('A') + i) for i in range(26)]:
        q.put(c)
    try:
        q.join()
    except KeyboardInterrupt:
        print("stopped by hand")


if __name__ == '__main__':
    main()
