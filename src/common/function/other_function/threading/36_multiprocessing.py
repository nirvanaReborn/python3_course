#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.51cto.com/u_15127585/4204179
# python的multiprocessing模块提供两种共享内存: sharedctypes与Manager。
# Manager效率较低，但支持远程共享内存。
# sharedctypes效率较高，快Manager两个数量级，在多进程访问时与普通内存访问相当

from datetime import datetime

size = 1000000


def tranverse(a):
    t = datetime.now()
    for i in range(size):
        a[i] = i
    print('elapsed %s' % (datetime.now() - t))


if __name__ == "__main__":
    choice = int(input("Enter a positive integer to choice: "))

    if choice == 1:
        import array

        a = array.array('i', [i for i in range(size)])
        print('test array')
        tranverse(a)

    elif choice == 2:
        a = {}
        for i in range(size):
            a[i] = i

        print('test dict')
        tranverse(a)

    elif choice == 3:
        from multiprocessing import Manager

        manager = Manager()
        a = manager.list([i for i in range(size)])
        print('test shared manager list')
        tranverse(a)

    elif choice == 4:
        from multiprocessing.sharedctypes import RawArray

        a = RawArray('i', [i for i in range(size)])
        print('test sharedctypes list in main process')
        tranverse(a)

    elif choice == 5:
        from multiprocessing.sharedctypes import RawArray
        from multiprocessing import Process

        a = RawArray('i', [i for i in range(size)])
        ps = [Process(target=tranverse, args=(a,)) for i in range(8)]
        print('test sharedctypes list in subprocess')
        for p in ps:
            p.start()
        for p in ps:
            p.join()

    else:
        pass
