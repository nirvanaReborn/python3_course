#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 3.进程间通信 -- Event（事件） 方法
# 开发者唯一需要记住的是， 事件对象不能被传递到子进程函数中。
# 如果你那么做，就会导致运行时错误， 信号对象只能通过继承机制在进程间共享。

import time
from multiprocessing import Event, Pool

event = Event()
event.set()


def worker(i):
    if event.is_set():
        time.sleep(0.1)
        print("A - %d - %s" % (i, time.time()))
        event.clear()
    else:
        time.sleep(0.1)
        print("B - %d - %s" % (i, time.time()))
        event.set()


if __name__ == "__main__":
    pool = Pool(3)
    pool.map(worker, range(9))
