#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/ctwy291314/article/details/89358144
# Manager是通过共享进程的方式共享数据。
# Manager管理的共享数据类型有：Value、Array、dict、list、Lock、Semaphore等等，同时Manager还可以共享类的实例对象。

from multiprocessing import Process, Manager


def func1(shareList, shareValue, shareDict, lock):
    with lock:
        shareValue.value += 1
        shareDict[1] = '1'
        shareDict[2] = '2'
        for i in range(len(shareList)):
            shareList[i] += 1


if __name__ == '__main__':
    manager = Manager()
    list1 = manager.list([1, 2, 3, 4, 5])
    dict1 = manager.dict()
    array1 = manager.Array('i', range(10))
    value1 = manager.Value('i', 1)
    lock = manager.Lock()
    proc = [Process(target=func1, args=(list1, value1, dict1, lock)) for i in range(20)]
    for p in proc:
        p.start()
    for p in proc:
        p.join()
    print(list1)
    print(dict1)
    print(array1)
    print(value1)
'''
[21, 22, 23, 24, 25]
{1: '1', 2: '2'}
array('i', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
Value('i', 21)
'''