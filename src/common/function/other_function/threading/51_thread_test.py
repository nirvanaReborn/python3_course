#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.jianshu.com/p/cac56b3d9a18
# IO 密集型应用CPU等待IO时间远大于CPU 自身运行时间，太浪费；
# 常见的 IO 密集型业务包括：浏览器交互、磁盘请求、网络爬虫、数据库请求等，在 IO 密集型业务中可以优先使用 asyncio。
'''
# Python 世界对于 IO 密集型场景的并发提升有 3 种方法：多进程、多线程、异步 IO(asyncio);
# 理论上讲asyncio是性能最高的，原因如下：
# 1.进程、线程会有CPU上下文切换
# 2.进程、线程需要内核态和用户态的交互，性能开销大；而协程对内核透明的,只在用户态运行
# 3.进程、线程并不可以无限创建，最佳实践一般是 CPU*2；而协程并发能力强，并发上限理论上取决于操作系统IO多路复用(Linux下是 epoll)可注册的文件描述符的极限

那asyncio的实际表现是否如理论上那么强，到底强多少呢？我构建了如下测试场景：
访问500台 DB，并sleep 100ms模拟业务查询
方法 1；顺序串行一台台执行
方法 2：多进程
方法 3：多线程
方法 4：asyncio
方法 5：asyncio+uvloop,最后的 asyncio+uvloop 和官方asyncio 最大不同是用 Cython+libuv 重新实现了asyncio 的事件循环(event loop)部分,官方测试性能是 node.js的 2 倍，持平 golang。
'''
import records

user = xx
passwd = xx
port = xx
hosts = []  # 500台 db列表


def query(host):
    conn = records.Database(f'mysql+pymysql://{user}:{passwd}@{host}:{port}/mysql?charset=utf8mb4')
    rows = conn.query('select sleep(0.1);')
    print(rows[0])


# 方法 1；顺序串行一台台执行
def test_1():
    for h in hosts:
        query(h)


# 多进程
def test_2():
    from concurrent import futures
    with futures.ProcessPoolExecutor() as executor:
        for future in executor.map(query, hosts):
            pass


# 多线程
def test_3():
    from concurrent import futures
    with futures.ThreadPoolExecutor() as executor:
        for future in executor.map(query,hosts):
            pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
