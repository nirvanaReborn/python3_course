#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.cnblogs.com/fnng/p/3670789.html
# python 多线程就这么简单


import datetime
import threading
import time


def music(func):
    for i in range(2):
        current_time = "[" + str(datetime.datetime.today()) + "]"
        print("%s I was listening to %s." % (current_time, func))
        time.sleep(1)


def move(func):
    for i in range(2):
        current_time = "[" + str(datetime.datetime.today()) + "]"
        print("%s I was at the %s!" % (current_time, func))
        time.sleep(5)


# 单线程
def test_1():
    music('爱情买卖')
    move('阿凡达')
    current_time = "[" + str(datetime.datetime.today()) + "]"
    print("%s all over!" % current_time)


# 多线程
def test_2():
    threads = []
    t1 = threading.Thread(target=music, args=(u'爱情买卖',))
    threads.append(t1)
    print(t1, type(t1))
    t2 = threading.Thread(target=move, args=(u'阿凡达',))
    threads.append(t2)
    for t in threads:
        '''
        setDaemon(True)将线程声明为守护线程，必须在start() 方法调用之前设置，如果不设置为守护线程程序会被无限挂起。
        子线程启动后，父线程也继续执行下去，当父线程执行完最后一条语句print "all over %s" %ctime()后，
        没有等待子线程，直接就退出了，同时子线程也一同结束。
        定义：守护线程--也称“服务线程”，在没有用户线程可服务时会自动离开。
        优先级：守护线程的优先级比较低，用于为系统中的其它对象和线程提供服务。
        '''
        t.setDaemon(True)  # 表示这个线程“不重要”
        t.start()  # 开始线程活动

    for t in threads:
        t.join()  # 在子线程完成运行之前，这个子线程的父线程将一直被阻塞。
    current_time = "[" + str(datetime.datetime.today()) + "]"
    print("%s all over!" % current_time)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
