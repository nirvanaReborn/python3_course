#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/ctwy291314/article/details/89358144
'''
multiprocessing包是Python中的多进程管理包。
与threading.Thread类似，它可以利用multiprocessing.Process对象来创建一个进程。
该Process对象与Thread对象的用法相同，也有start(), run(), join()的方法。
此外multiprocessing包中也有Lock/Event/Semaphore/Condition类 (这些对象可以像多线程那样，通过参数传递给各个进程)，用以同步进程，其用法与threading包中的同名类一致。
所以，multiprocessing的很大一部份与threading使用同一套API，只不过换到了多进程的情境。

但在使用这些共享API的时候，我们要注意以下几点:
1.在UNIX平台上，当某个进程终结之后，该进程需要被其父进程调用wait，否则进程成为僵尸进程(Zombie)。
  所以，有必要对每个Process对象调用join()方法 (实际上等同于wait)。对于多线程来说，由于只有一个进程，所以不存在此必要性。
2.multiprocessing提供了threading包中没有的IPC(比如Pipe和Queue)，效率上更高。
  应优先考虑Pipe和Queue，避免使用Lock/Event/Semaphore/Condition等同步方式 (因为它们占据的不是用户进程的资源)。
3.多进程应该避免共享资源。在多线程中，我们可以比较容易地共享资源，比如使用全局变量或者传递参数。
  在多进程情况下，由于每个进程有自己独立的内存空间，以上方法并不合适。
  此时我们可以通过共享内存和Manager的方法来共享资源。
  但这样做提高了程序的复杂度，并因为同步的需要而降低了程序的效率。
'''

'''
我们可以从下面的程序中看到Thread对象和Process对象在使用上的相似性与结果上的不同。
各个线程和进程都做一件事：打印PID。
但问题是，所有的任务在打印的时候都会向同一个标准输出(stdout)输出。这样输出的字符会混合在一起，无法阅读。
使用Lock同步，在一个任务输出完成之后，再允许另一个任务输出，可以避免多个任务同时向终端输出
'''
import multiprocessing
import os
import threading

# Main
print('Main:', os.getpid())


# worker function
def worker(sign, lock):
    lock.acquire()
    print(sign, os.getpid())
    lock.release()


if __name__ == '__main__':
    # Multi-thread
    record = []
    lock = threading.Lock()

    for i in range(5):
        thread = threading.Thread(target=worker, args=('thread', lock))
        thread.start()
        record.append(thread)

    for thread in record:
        thread.join()

    # Multi-process
    record = []
    lock = multiprocessing.Lock()

    for i in range(5):
        process = multiprocessing.Process(target=worker, args=('process', lock))
        process.start()
        record.append(process)

    for process in record:
        process.join()
