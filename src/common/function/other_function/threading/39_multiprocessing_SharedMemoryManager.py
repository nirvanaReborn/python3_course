#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
#
import multiprocessing
from multiprocessing.managers import SharedMemoryManager


def f(ls, s, e):
    for idx in range(s, e):
        ls[idx - s] = ls[e - 1 - idx]

    return


if __name__ == "__main__":
    with SharedMemoryManager() as smm:
        x = smm.ShareableList(list(range(10)))

        p = multiprocessing.Process(target=f, args=(x, 0, len(x)))
        p.start()
        p.join()

        for _ in x:
            print(_, end=", ")
