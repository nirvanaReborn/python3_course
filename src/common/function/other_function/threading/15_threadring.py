#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/eastmount/article/details/50155353
# [python] 专题八.多线程编程之thread和threading
'''
Python提供了几个用于多线程编程的模块，包括thread、threading和Queue等。
(1) thread模块: 允许程序员创建和管理线程，它提供了基本的线程和锁的支持。
(2) threading模块: 允许程序员创建和管理线程，它提供了更高级别，更强的线程管理的功能。
(3) Queue模块: 允许用户创建一个可用于多个线程间共享数据的队列数据结构。

下面简单分析为什么需要避免使用thread模块？
(1) 首先更高级别的threading模块更为先进，对线程的支持更为完善，而且使用thread模块里的属性有可能会与threading出现冲突。
(2) 其次，低级别的thread模块的同步原语很少（实际只有一个），而threading模块则有很多。
(3) 另一个原因是thread对你的进程什么时候应该结束完全没有控制，当主线程结束时，所有的线程都会被强制结束掉，没有警告也不会有正常的清除工作。而threading模块能确保重要的子线程退出后进程才退出。
'''


# thread实现多线程
def test_1():
    import _thread, time

    def loop0():
        print('Start loop 0 at:', time.ctime())
        time.sleep(4)
        print('Loop 0 done at:', time.ctime())

    def loop1():
        print('Start loop 1 at:', time.ctime())
        time.sleep(2)
        print('Loop 1 done at:', time.ctime())

    try:
        print('Starting at:', time.ctime())
        _thread.start_new_thread(loop0, ())
        _thread.start_new_thread(loop1, ())
        time.sleep(6)
        print('All done at:', time.ctime())
    except Exception as e:
        print('Error:', e)
    finally:
        print('END')


# thread实现线程加锁方法
def test_2():
    import _thread, time

    def loop(nloop, nsec, lock):
        print('start loop', nloop, 'at:', time.ctime())
        time.sleep(nsec)
        print('loop', nloop, 'done at:', time.ctime())
        lock.release()  # 解锁

    print('Starting at:', time.ctime())
    locks = []
    loops = [4, 2]  # 等待时间
    nloops = range(len(loops))  # 以loops数组创建列表并赋值给nloops
    for i in nloops:
        lock = _thread.allocate_lock()  # 创建锁对象
        lock.acquire()  # 获取锁对象 加锁
        locks.append(lock)  # 追加到locks[]数组中

    # 执行多线程 (函数名,函数参数)
    for i in nloops:
        _thread.start_new_thread(loop, (i, loops[i], locks[i]))

    # 循环等待顺序检查每个所都被解锁才停止
    for i in nloops:
        while locks[i].locked():
            pass
    print('All done at:', time.ctime())


# threading实现多线程
def test_3():
    import threading, time

    def loop(nloop, nsec):
        print('start loop', nloop, 'at:', time.ctime())
        time.sleep(nsec)
        print('loop', nloop, 'done at:', time.ctime())

    print('Starting at:', time.ctime())
    threads = []
    loops = [4, 2]  # 等待时间
    nloops = range(len(loops))  # 以loops数组创建列表并赋值给nloops
    # 创建线程
    for i in nloops:
        t = threading.Thread(target=loop, args=(i, loops[i]))
        threads.append(t)

    # 开始线程
    for i in nloops:
        threads[i].start()

    # 等待所有结束线程
    for i in nloops:
        threads[i].join()
    print('All done at:', time.ctime())


# threading实现多线程：创建一个Thread实例，传给它一个可调用的类对象
def test_4():
    import threading, time

    class ThreadFunc(object):
        def __init__(self, func, args, name=''):
            self.name = name
            self.func = func
            self.args = args

        '''
        apply(func [, args [, kwargs ]]) 
        函数：用于当函数参数已经存在于一个元组或字典中时，间接地调用函数。
        args是一个包含将要提供给函数的按位置传递的参数的元组。
        如果省略了args，任何参数都不会被传递，kwargs是一个包含关键字参数的字典。
        '''

        def __call__(self):
            # apply(self.func, self.args)
            self.func(*self.args)

    def loop(nloop, nsec):
        print('start loop', nloop, 'at:', time.ctime())
        time.sleep(nsec)
        print('loop', nloop, 'done at:', time.ctime())

    print('Starting at:', time.ctime())
    threads = []
    loops = [4, 2]  # 等待时间
    nloops = range(len(loops))  # 以loops数组创建列表并赋值给nloops
    # 调用ThreadFunc类实例化的对象，创建所有线程
    for i in nloops:
        t = threading.Thread(ThreadFunc(loop, (i, loops[i]), loop.__name__))
        threads.append(t)

    # 开始线程
    for i in nloops:
        threads[i].start()

    # 等待所有结束线程
    for i in nloops:
        threads[i].join()
    print('All done at:', time.ctime())


# threading实现多线程：Thread派生一个子类，创建这个子类的实例
def test_5():
    import threading, time

    class MyThread(threading.Thread):
        def __init__(self, func, args, name=''):
            threading.Thread.__init__(self)
            self.name = name
            self.func = func
            self.args = args

        def run(self):  # run()函数
            self.func(*self.args)

    def loop(nloop, nsec):
        print('start loop', nloop, 'at:', time.ctime())
        time.sleep(nsec)
        print('loop', nloop, 'done at:', time.ctime())

    print('Starting at:', time.ctime())
    loops = [4, 2]  # 等待时间
    nloops = range(len(loops))  # 以loops数组创建列表并赋值给nloops
    threads = []
    # 子类MyThread实例化，创建所有线程
    for i in nloops:
        t = MyThread(loop, (i, loops[i]), loop.__name__)
        threads.append(t)

    # 开始线程
    for i in nloops:
        threads[i].start()

    # 等待所有结束线程
    for i in nloops:
        threads[i].join()
    print('All done at:', time.ctime())


# 线程运行斐波那契、阶乘和加和
def test_6():
    import threading, time

    class MyThread(threading.Thread):
        def __init__(self, func, args, name=''):
            threading.Thread.__init__(self)
            self.name = name
            self.func = func
            self.args = args

        def getResult(self):
            return self.result

        def run(self):  # run()函数
            print("Starting", self.name, 'at:', time.ctime())
            self.result = self.func(*self.args)
            print(self.name, 'finished at:', time.ctime())

    # 斐波那契函数
    def fib(x):
        time.sleep(0.005)
        if x < 2:
            return 1
        return (fib(x - 2) + fib(x - 1))

    # 阶乘函数 factorial calculation
    def fac(x):
        time.sleep(0.1)
        if x < 2:
            return 1
        return (x * fac(x - 1))

    # 求和函数
    def sum(x):
        time.sleep(0.1)
        if x < 2:
            return 1
        return (x + sum(x - 1))

    funcs = [fib, fac, sum]
    n = 14
    nfuncs = range(len(funcs))

    print('*****单线程方法*****')
    for i in nfuncs:
        print('Starting', funcs[i].__name__, 'at:', time.ctime())
        print(funcs[i](n))
        print('Finished', funcs[i].__name__, 'at:', time.ctime())
    print('*****结束单线程*****')
    print()
    print('*****多线程方法*****')
    threads = []
    for i in nfuncs:
        # 调用MyThread类实例化的对象，创建所有线程
        t = MyThread(funcs[i], (n,), funcs[i].__name__)
        threads.append(t)

    # 开始线程
    for i in nfuncs:
        threads[i].start()

    # 等待所有结束线程
    for i in nfuncs:
        threads[i].join()
        print(threads[i].getResult())
    print('*****结束多线程*****')


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
