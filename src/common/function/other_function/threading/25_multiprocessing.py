#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 1.进程退出状态
# 当进程结束（或中断） 的时候， 会产生一个退出码（exitcode） ， 它是一个数字， 表示执行的结果。
# 不同的数字分别表示进程正常完结， 异常完结， 或是由另一个进程中断的状态。


import multiprocessing
import time


def first():
    print("There is no problem here")


def second():
    raise RuntimeError("Error raised!")


def third():
    time.sleep(3)
    print("This process will be terminated")


if __name__ == "__main__":
    workers = [multiprocessing.Process(target=first),
               multiprocessing.Process(target=second),
               multiprocessing.Process(target=third)]

    for w in workers:
        w.start()

    workers[-1].terminate()

    for w in workers:
        w.join()

    # workers[-1].terminate()

    for w in workers:
        print(w.exitcode)
