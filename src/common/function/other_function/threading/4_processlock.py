#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# http://www.redicecn.com/html/Python/20131108/464.html
# Python跨进程级锁的一种实现
# 跨进程级锁可以使独立进程间互斥地访问某共享资源（比如，多进程共同写一个文件）。
# 这里通过文件锁实现，源码如下：

import os
import time


# 进程锁 - 进程间互斥锁
try:
    import fcntl

    LOCK_EX = fcntl.LOCK_EX
except ImportError:
    # Windows平台下没有fcntl模块
    fcntl = None
    import win32con
    import win32file
    import pywintypes

    LOCK_EX = win32con.LOCKFILE_EXCLUSIVE_LOCK
    overlapped = pywintypes.OVERLAPPED()


class ProcessLock:
    """进程锁
    """

    def __init__(self, filename='processlock.txt'):
        self.filename = filename
        # 如果文件不存在则创建
        self.handle = open(filename, 'w')

    def __del__(self):
        try:
            self.handle.close()
            os.remove(self.filename)
        except:
            pass

    def acquire(self):
        # 给文件上锁
        if fcntl:
            fcntl.flock(self.handle, LOCK_EX)
        else:
            hfile = win32file._get_osfhandle(self.handle.fileno())
            win32file.LockFileEx(hfile, LOCK_EX, 0, -0x10000, overlapped)

    def release(self):
        # 文件解锁
        if fcntl:
            fcntl.flock(self.handle, fcntl.LOCK_UN)
        else:
            hfile = win32file._get_osfhandle(self.handle.fileno())
            win32file.UnlockFileEx(hfile, 0, -0x10000, overlapped)


if __name__ == '__main__':
    # 测试：依次运行本程序多个实例，第N个实例运行耗时是第一个的N倍
    print('Time: %s' % time.time())

    lock = ProcessLock()
    try:
        lock.acquire()
        time.sleep(20)
    finally:
        lock.release()

    print('Time: %s' % time.time())
