#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.cnblogs.com/tyomcat/p/5486827.html
# python并发编程之多进程、多线程、异步和协程

'''
1、子进程（subprocess包）
在python中，通过subprocess包，fork一个子进程，并运行外部程序。
调用系统的命令的时候，最先考虑的os模块。用os.system()和os.popen()来进行操作。
但是这两个命令过于简单，不能完成一些复杂的操作，如给运行的命令提供输入或者读取命令的输出，
判断该命令的运行状态，管理多个命令的并行等等。这时subprocess中的Popen命令就能有效的完成我们需要的操作
'''


def test_1():
    import subprocess, shlex
    command_line = input()  # ping www.baidu.com
    args = shlex.split(command_line)
    p = subprocess.Popen(args)
    print(p.communicate())

    # 利用subprocess.PIPE将多个子进程的输入和输出连接在一起，构成管道(pipe)
    # child1 = subprocess.Popen(["ls","-l"], stdout=subprocess.PIPE)
    # child2 = subprocess.Popen(["wc"], stdin=child1.stdout,stdout=subprocess.PIPE)
    # # communicate() 方法从stdout和stderr中读出数据，并输入到stdin中。
    # out = child2.communicate()
    # print(out)


'''
2、多进程（multiprocessing包）
详情请看：Python多进程编程——http://www.cnblogs.com/kaituorensheng/p/4445418.html
multiprocessing包是Python中的多进程管理包。与threading.Thread类似，它可以利用multiprocessing.Process对象来创建一个进程。
Pool()                  进程池 (Process Pool)可以创建多个进程。
apply_async(func,args)  从进程池中取出一个进程执行func，args为func的参数。它将返回一个AsyncResult的对象，你可以对该对象调用get()方法以获得结果。
close()                 进程池不再创建新的进程
join()                  wait进程池中的全部进程。必须对Pool先调用close()方法才能join。
'''


def test_2():
    import multiprocessing, os, time

    def long_time_task(name):
        print('Run task %s (%s)...' % (name, os.getpid()))
        start = time.time()
        time.sleep(3)
        end = time.time()
        print('Task %s runs %0.2f seconds.' % (name, (end - start)))

    print('Parent process %s.' % os.getpid())
    p = multiprocessing.Pool()
    for i in range(4):
        p.apply_async(long_time_task, args=(i,))
    print('Waiting for all subprocesses done...')
    p.close()
    p.join()
    print('All subprocesses done.')


def test_3():
    import multiprocessing, time, random

    def write(q):
        for value in ['A', 'B', 'C', 'D']:
            print("Put %s to Queue!" % value)
            q.put(value)
            time.sleep(random.random())

    def read(q, lock):
        while True:
            lock.acquire()
            if not q.empty():
                value = q.get(True)
                print("Get %s from Queue" % value)
                time.sleep(random.random())
            else:
                break
            lock.release()

    manager = multiprocessing.Manager()
    q = manager.Queue()
    p = multiprocessing.Pool()
    lock = manager.Lock()
    pw = p.apply_async(write, args=(q,))
    pr = p.apply_async(read, args=(q, lock))
    p.close()
    p.join()
    print("所有数据都写入并且读完")


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
