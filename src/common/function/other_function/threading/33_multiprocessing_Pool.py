#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/ctwy291314/article/details/89358144
# 进程池 (Process Pool)可以创建多个进程。这些进程就像是随时待命的士兵，准备执行任务(程序)。
# 一个进程池中可以容纳多个待命的进程。
# 除了map()方法外，Pool还有下面的常用方法:
# apply_async(func,args) 从进程池中取出一个进程执行func，args为func的参数。
#                       它将返回一个AsyncResult的对象，你可以对该对象调用get()方法以获得结果。
# close() 进程池不再创建新的进程
# join() wait进程池中的全部进程。必须对Pool先调用close()方法才能join。


import multiprocessing


def f(x):
    return x ** 2


if __name__ == '__main__':
    pool = multiprocessing.Pool(5)
    rel = pool.map(f, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    print(rel)  # [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
