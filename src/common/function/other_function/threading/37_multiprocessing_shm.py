#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
#
import time
import struct
from multiprocessing import Manager
# python3.8新功能：multiprocessing 模块增加一个 SharedMemory 类，用于创建进程之间的共享内存。
from multiprocessing import shared_memory


if __name__ == '__main__':
    manager = Manager()
    pool = manager.dict({})
    for i in range(2):
        pool[i] = manager.list([])

    # 测试经典方法
    last_time = time.time()
    for i in range(1000):
        pool[i].append(['r'])
    print("manager方法用时 "+str(time.time()-last_time))

    # mem方法
    shm = shared_memory.SharedMemory(create=True, size=4096)
    last_time = time.time()
    for j in range(1000):
        for i in range(500):
            t = b'rrrr'
            t = struct.unpack('4s', t)[0]  # 解封
            t = struct.pack('4s', t)  # 把str封装
            shm.buf[i:i + 4] = t

    print("mem方法用时 " + str(time.time() - last_time))


