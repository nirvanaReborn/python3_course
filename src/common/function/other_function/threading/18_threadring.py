#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.cnblogs.com/tyomcat/p/5486827.html
# python并发编程之多进程、多线程、异步和协程
# 详情请看：解释'yield'和'Generators(生成器)'——http://www.oschina.net/translate/improve-your-python-yield-and-generators-explained
# 详情请看：Python几种并发实现方案的性能比较——http://www.elias.cn/Python/PyConcurrency

'''协程（又称微线程，纤程）
协程，与线程的抢占式调度不同，它是协作式调度。
协程也是单线程，但是它能让原来要使用异步+回调方式写的非人类代码,可以用看似同步的方式写出来。
1、协程在python中可以由生成器（generator）来实现。
首先要对生成器和yield有一个扎实的理解.
调用一个普通的python函数，一般是从函数的第一行代码开始执行，结束于return语句、异常或者函数执行（也可以认为是隐式地返回了None）。
一旦函数将控制权交还给调用者，就意味着全部结束。
而有时可以创建能产生一个序列的函数，来“保存自己的工作”，这就是生成器（使用了yield关键字的函数）。
能够“产生一个序列”是因为函数并没有像通常意义那样返回。
return隐含的意思是函数正将执行代码的控制权返回给函数被调用的地方。
而"yield"的隐含意思是控制权的转移是临时和自愿的，我们的函数将来还会收回控制权。
看一下生产者/消费者的例子：
当程序执行到produce的yield i时，返回了一个generator并暂停执行，
当我们在custom中调用p.next()，程序又返回到produce的yield i 继续执行，
这样 list 中又append了元素，然后我们print list.pop()，直到p.__next__()引发了StopIteration异常。
'''


# 生产者 消费者 模式
def test_1():
    import time
    import sys
    # 生产者
    def produce(list):
        i = 0
        while 1:
            if i < 10:
                list.append(i)
                yield i
                i = i + 1
                time.sleep(1)
            else:
                return

    # 消费者
    def consume(list):
        p = produce(list)
        while 1:
            try:
                p.__next__()
                while len(list) > 0:
                    print(list.pop())
            except StopIteration:
                sys.exit(0)

    list = []
    consume(list)


# 2、Stackless Python

# 3、greenlet模块
# 基于greenlet的实现则性能仅次于Stackless Python，大致比Stackless Python慢一倍，比其他方案快接近一个数量级。
# 其实greenlet不是一种真正的并发机制，而是在同一线程内，在不同函数的执行代码块之间切换，
# 实施“你运行一会、我运行一会”，并且在进行切换时必须指定何时切换以及切换到哪。

# 4、eventlet模块


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
