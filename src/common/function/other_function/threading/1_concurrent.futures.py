#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/cskchenshengkun/article/details/45770627
# 多进程并发与多线程并发总结

# 对于windows系统的多进程并发，程序文件里必须含有“入口函数”（如main函数），且结尾处必须调用入口点。
# 例如以if __name__ == '__main__': main()结尾。
# concurrent.functures库统一了线程池和进程池，简化了编程。体现了Python简单的思想哲学。
# ThreadPoolExecutor异步调用的线程池的Executor
# ProcessPoolExecutor异步调用的进程池的Executor
# 一.多进程并发
# 1.进程池实现——使用concurrent.futures.ProcessPoolExecutor
#
# 二.多线程并发
# 对于IO密集型程序，多线程并发可能要优于多进程并发。
# 因为对于网络通信等IO密集型任务来说，决定程序效率的主要是网络延迟，这时候是使用进程还是线程就没有太大关系了。
# 1.线程池实现——使用concurrent.futures.ThreadPoolExecutor
# 2.队列实现——使用queue.Queue
#     程序与多进程基本一致，只是这里我们不必使用multiProcessing.JoinableQueue对象了，一般的队列（来自queue.Queue)就可以满足要求：


import concurrent.futures
import os
import queue
import random
import threading
import time


# 线程池的实现
def test_1():
    def read(q):
        print('Get %s from queue.' % q)
        time.sleep(random.random())

    futures = set()
    with concurrent.futures.ThreadPoolExecutor(os.cpu_count() * 4) as executor:
        for q in (chr(ord('A') + i) for i in range(26)):
            future = executor.submit(read, q)
            futures.add(future)
    try:
        for future in concurrent.futures.as_completed(futures):
            err = future.exception()
            if err is not None:
                raise err
    except KeyboardInterrupt:
        print("stopped by hand")


# 队列的实现
def test_2():
    def read(q):
        while True:
            try:
                value = q.get()
                print('Get %s from queue.' % value)
                time.sleep(random.random())
            finally:
                q.task_done()

    q = queue.Queue()
    pw1 = threading.Thread(target=read, args=(q,))
    pw2 = threading.Thread(target=read, args=(q,))
    pw1.daemon = True
    pw2.daemon = True
    pw1.start()
    pw2.start()
    for c in [chr(ord('A') + i) for i in range(26)]:
        q.put(c)
    try:
        q.join()
    except KeyboardInterrupt:
        print("stopped by hand")


# --------------------------------------------------------------------------
# 进程池的实现
def test_10():
    def read(q):
        print('Get %s from queue.' % q)
        time.sleep(random.random())

    futures = set()
    with concurrent.futures.ProcessPoolExecutor() as executor:
        for q in (chr(ord('A') + i) for i in range(26)):
            future = executor.submit(read, q)
            futures.add(future)
    try:
        for future in concurrent.futures.as_completed(futures):
            err = future.exception()
            if err is not None:
                raise err
    except KeyboardInterrupt:
        print("stopped by hand")


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    print("执行脚本耗时 %s 秒" % __import__('timeit').timeit(main, number=1))
    # main()
