#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
https://docs.python.org/2/library/multiprocessing.html）
如前所述， 由于GIL的存在， Python的多线程并没有实现真正的并行。因此， 一些问题使用threading模块并不能真正解决。
不过Python为多线程提供了一个替代方案： 多进程。 在多进程里， 线程被换成了一个个子进程。
每个进程都运作各自的GIL（这样Python就可以并行开启多个进程， 没有数量限制） 。
需要明确的是， 线程都是同一个进程的组成部分， 它们共享同一块内存、 存储空间和计算资源。
而进程却不会与生成它们的父进程共享内存， 因此进程间的通信比线程间通信更加复杂。

由于Windows生成子进程的机制与Linux不同， 所以如果在Windows系统上运行上面的代码，
请把multiprocessing放到if __name__ == '__main__':下面。
'''

import multiprocessing
import time


def run(pname):
    time.sleep(1)
    print(pname)


if __name__ == "__main__":
    list_tasks = []
    for i in range(10):
        p = multiprocessing.Process(target=run, args=("Process-" + str(i),))
        list_tasks.append(p)
        p.start()
        # p.join()

    for p in list_tasks:
        p.join()
