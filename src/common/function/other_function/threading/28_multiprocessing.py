#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 3.进程间通信 -- Pipe（管道） 方法
# https://docs.python.org/3/library/multiprocessing.html#exchangingobjects-between-processes
# 为两个进程提供了一种双向通信的机制。 Pipe()函数返回一对连接对象， 每个对象表示管道的一端。
# 每个连接对象都有send()和recv()方法


import random
from multiprocessing import Pipe, Process


def generate(pipe):
    while True:
        value = random.randrange(10)
        pipe.send(value)
        print("Value send: %s" % (value))


def reader(pipe):
    f = open("output.txt", 'w')
    while True:
        value = pipe.recv()
        f.write(str(value))
        print(".")


if __name__ == "__main__":
    input_p, output_p = Pipe()
    p1 = Process(target=generate, args=(input_p,))
    p2 = Process(target=reader, args=(output_p,))
    p1.start()
    p2.start()
