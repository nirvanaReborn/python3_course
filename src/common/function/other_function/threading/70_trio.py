#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/dianyin7770/article/details/101935997
'''
Trio 是一个用于异步并发和  I/O 的 Python 库，其目标是为 Python 生成一个可用于生产的 async/await-native I/O 库。
https://trio.readthedocs.io/en/stable/tutorial.html
pip install trio
从外观上看异步方法和标准方法没什么区别只是前面多了个async。
“Async” 是“asynchronous”的简写，为了区别于异步函数，我们称标准函数为同步函数，
从用户角度异步函数和同步函数有以下区别：
1.要调用异步函数，必须使用await关键字。
2.不能在同步函数里使用await，否则会出错。
异步函数相对于常规函数的全部优势在于异步函数可以通过await调用其他异步函数。

注意：在trio中不能使用asyncio.sleep()。

记住如果运行时警告:coroutine 'RuntimeWarning: coroutine '...' was never awaited'，也就意味这有个地方你没有写await。
'''

import trio


# 我们知道上面的多个任务都是在一个线程中进行切换操作的，但是对于如何切换的我们并不了解，
# 幸运的是，trio提供了一组用于检查和调试程序的工具。
# 我们可以通过编写一个Tracer类，来实现trio.abc.Instrumen接口。代码如下:
class Tracer(trio.abc.Instrument):
    def before_run(self):
        print("!!! run started")

    def _print_with_task(self, msg, task):
        # repr(task) is perhaps more useful than task.name in general,
        # but in context of a tutorial the extra noise is unhelpful.
        print("{}: {}".format(msg, task.name))

    def task_spawned(self, task):
        self._print_with_task("### new task spawned", task)

    def task_scheduled(self, task):
        self._print_with_task("### task scheduled", task)

    def before_task_step(self, task):
        self._print_with_task(">>> about to run one step of task", task)

    def after_task_step(self, task):
        self._print_with_task("<<< task step finished", task)

    def task_exited(self, task):
        self._print_with_task("### task exited", task)

    def before_io_wait(self, timeout):
        if timeout:
            print("### waiting for I/O for up to {} seconds".format(timeout))
        else:
            print("### doing a quick check for I/O")
        self._sleep_time = trio.current_time()

    def after_io_wait(self, timeout):
        duration = trio.current_time() - self._sleep_time
        print("### finished I/O check (took {} seconds)".format(duration))

    def after_run(self):
        print("!!! run finished")


async def async_double(x):
    return 2 * x


async def double_sleep(x):
    await trio.sleep(2 * x)


async def child1():
    print("  child1: started! sleeping now...")
    await trio.sleep(1)
    print("  child1: exiting!")


async def child2():
    print("  child2: started! sleeping now...")
    await trio.sleep(1)
    print("  child2: exiting!")


async def parent():
    print("parent: started!")
    '''
    我们知道在读文件时候我们使用with open()...去创建一个文件句柄,with里面牵扯到两个魔法函数：
    在代码块开始的时候调用__enter__()，结束时再去调用__exit__()，我们称open()为上下文管理器。
    async with someobj语句和with差不多只不过它调用的异步方法的魔法函数：__aenter__和__aexit__。
    我们称someobj为“异步上下文管理器”。
    '''
    async with trio.open_nursery() as nursery:
        print("parent: spawning child1...")
        nursery.start_soon(child1)

        print("parent: spawning child2...")
        nursery.start_soon(child2)

        print("parent: waiting for children to finish...")
        # -- we exit the nursery block here --
    print("parent: all done!")


def test_1():
    print(trio.run(async_double, 3))  # returns 6


def test_2():
    # does nothing for 6 seconds then returns
    print(trio.run(double_sleep, 3))


def test_3():
    '''
    如果你熟悉线程的话，你会发现这个运作机制和多线程类似。
    但是这里并不是线程，这里的代码全部在一个线程里面的完成，
    为了区别线程我们称这里的child1和child2为两个任务,
    有了任务，我们只能在某些我们称之为“checkpoints”的指定地点进行切换。
    '''
    # trio.run(parent)
    trio.run(parent, instruments=[Tracer()])


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
