#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
# python2 安装 pip
sudo python get-pip.py --trusted-host=files.pythonhosted.org --trusted-host=pypi.org

# 升级pip工具
python -m pip install --upgrade pip

# 显示模块
pip list

# 显示过期模块
pip list --outdated --format=columns

# 安装模块
pip install -i https://pypi.douban.com/simple --trusted-host pypi.douban.com xxx
pip install pyecharts==

# 升级模块
pip install --upgrade xxx

# 生成requirements.txt
pip freeze --all > requirements.txt
pip install -r requirements.txt

'''
import pip
import os
import numpy
import pandas
from public_class_PythonTool import PythonTool
from public_function import GLOBAL_WORK_DIR


def install_all_module_by_excel():
    PythonTool().get_all_install_module()

    list_module = []
    source_file = os.path.join(GLOBAL_WORK_DIR, r"pip_install.xlsx")
    dataframe = pandas.read_excel(source_file, sheet_name='pip安装')
    list_sheet = list(numpy.array(dataframe).tolist())  # 已经跳过标题行
    for i, fileLine in enumerate(list_sheet):
        if fileLine[0] == "add":
            list_module.append(fileLine[2])
    print(list_module)
    PythonTool().install_module(list_module)


def main():
    choice = str(input("Enter a positive integer to choice: "))
    if choice == "1":
        str_module = str(input("请输入要安装的第三方模块: "))
        list_module = str_module.split(" ")
        PythonTool().install_module(list_module)
    elif choice == "2":
        str_module = str(input("请输入要卸载的第三方模块: "))
        list_module = str_module.split(" ")
        PythonTool().uninstall_module(list_module)
    elif choice == "3":
        str_module = str(input("请输入要升级的第三方模块: "))
        list_module = str_module.split(" ")
        PythonTool().update_module(list_module)
    elif choice == "4":
        PythonTool().update_all_module()
    elif choice == "5":
        print("安装所有模块：")
        # PythonTool().install_all_module()
        install_all_module_by_excel()
    elif choice == "6":
        str_module = str(input("请输入要检测的第三方模块: "))
        list_module = str_module.split(" ")
    elif choice == "7":
        # pip.main(["-V"])
        pip.main(['--timeout=5', 'install', '--user', '--index-url==https://pypi.douban.com/simple', '--trusted-host==pypi.douban.com', 'holidays'])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
