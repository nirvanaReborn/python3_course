#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# http://yshblog.com/blog/117
# https://www.cnblogs.com/zhangqunshi/p/6657208.html
# http://www.cnblogs.com/dkblog/archive/2009/04/16/1980757.html
# Python生成pyc文件
# pyc文件是py文件编译后生成的字节码文件(byte code)。
# pyc文件经过python解释器最终会生成机器码运行。
# 所以pyc文件是可以跨平台部署的，类似Java的.class文件。
# 一般py文件改变后，都会重新生成pyc文件。
# 为什么要手动提前生成pyc文件呢，主要是不想把源代码暴露出来。
# pyc的内容，是跟python的版本相关的，不同版本编译后的pyc文件是不同的。
#
# pyo是优化编译后的程序 python -O 源文件即可将源程序编译为pyo文件
#
# pyd是python的动态链接库。
#
# Linux下还需要注意权限。必要时需要使用对应账户或者root运行一次，才会生产。否则还是py。
import os
import py_compile  # 编译单个 Python 文件
import compileall  # 编译整个目录的所有 Python 文件
import platform
import shutil


# 将这些 .pyc 文件输出到特定的目录
def compile_to_target(source_file, target_dir="dist"):
    # 确保目标目录存在
    os.makedirs(target_dir, exist_ok=True)

    # 获取源文件的绝对路径
    abs_source_file = os.path.abspath(source_file)

    # 生成目标 .pyc 文件路径
    target_file = os.path.join(target_dir, os.path.basename(source_file) + 'c')
    print(target_file)
    # 编译并生成结果
    py_compile.compile(source_file, cfile=target_file, doraise=True)


def test_1():
    '''
    生成单个pyc文件
    命令行为：
        python -m foo.py                                    # -m 相当于脚本中的import
        python -m py_compile /root/src/{file1,file2}.py     # 这是同时转换多个文件
        python -O -m py_compile file.py                     # 编译成pyo文件。
    或者通过代码来生成pyc文件, 函数的格式如下:
    py_compile.compile(file, cfile=None, dfile=None, doraise=False, optimize=-1)
        将源文件编译为字节码并写出字节码缓存文件。源代码是从名为file的文件加载的。
        字节码被写入cfile, cfile默认为file + 'c'(如果在当前解释器中启用了优化，则为'o')。
        如果指定了dfile，它将在错误消息中用作源文件的名称，而不是文件。
        如果doraise是真的，那么在编译文件时遇到错误时将引发PyCompileError。
    '''
    # 最终的pyc文件会生成到同级__pycache__目录下面
    # py_compile.compile(r'./1_pip_install.py', doraise=True)

    compile_to_target(r'./1_pip_install.py')


def test_2():
    '''
    # 批量生成pyc文件
    # 命令行为：python -m compileall <dir>
    # 命令行为：find <dir> -name '*.py' -type f -print -exec rm {} \;
    函数的格式如下:
    compile_dir(dir[, maxlevels[, ddir[, force[, rx[, quiet]]]]])
    参数含义：
        dir: 表示需要编译的文件夹位置
        maxlevels: 表示需要递归编译的子目录的层数，默认是10层，即默认会把10层子目录中的py文件编译为pyc
        ddir: If ddir is given, it is prepended to the path to each file being compiled for use in compilation time tracebacks,
              and is also compiled in to the byte-code file, where it will be used in tracebacks and other messages
              in cases where the source file does not exist at the time the byte-code file is executed. (谁能翻译一下( ⊙o⊙?)不懂)
        force: 如果True，不论是是否有pyc，都重新编译
        rx: 一个正则表达式，排除掉不想要的目录
        quiet：如果为True，则编译不会在标准输出中打印信息
    '''
    compileall.compile_dir(r'D:\share\git\gitee\nirvanaReborn\python3_public_resource\src\public_resource')


def test_3():
    # 最终的pyc文件会生成到同级__pycache__目录下面
    def recursive_compilation(source_file, dest_file):
        if os.path.isfile(source_file):
            py_compile.compile(source_file)
            filepath, basename = os.path.split(source_file)
            shotname, extension = os.path.splitext(basename)
            temp_file = os.path.join(filepath, '__pycache__', shotname + '.cpython-' + ''.join(platform.python_version_tuple()[0:2]) + '.pyc')
            if os.path.exists(temp_file):
                shutil.copy2(temp_file, dest_file)
                print(temp_file, '--->', dest_file)
                shutil.rmtree(os.path.join(filepath, '__pycache__'))
            else:
                print("编译失败")
        elif os.path.isdir(source_file):
            for item in os.listdir(source_file):
                full_path = os.path.join(source_file, item)
                # recursive_compilation(full_path, dest_file)
        else:
            print("未知类型：", source_file)

    # source_file = r"D:\SVN\SVN_test\trunk\LDP\dev\strategy"
    source_file = r"D:\SVN\SVN_test\trunk\LDP\dev\strategy\strategy_python\demo_python_1.py"
    dest_file = r"D:\SVN\SVN_test\trunk\LDP\dev\strategy\demo_python_1.pyc"
    recursive_compilation(source_file, dest_file)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
