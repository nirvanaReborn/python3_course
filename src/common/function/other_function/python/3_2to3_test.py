#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/jerry_1126/article/details/19237467
# python中2to3工具的使用
'''
Python3中自带了个转换工具:D:\ProgramFiles\Python\Anaconda3\Tools\scripts\2to3.py
命令格式：D:\ProgramFiles\Python\Anaconda3\Tools\scripts\2to3.py -w test.py
1. 目录. C:\Python33\Tools\Scripts\2to3.py.  其实在python2.6,2.7中都存在这个工具。
2. 如果不加-w参数，则默认只是把转换过程所对应的diff内容打印输出到当前窗口而已。
3. 加了-w，就是把改动内容，写回到原先的文件了。
4. 不想要生成bak文件，再加上-n即可。  bak最好还是有。

https://www.cnblogs.com/todayisafineday/p/5656429.html
同时，迎接你的可能还是不少异常。例如下面的描述：
现象1：
TypeError: cannot use a string pattern on a bytes-like object
原因：
　　正则表达式是Unicode字符串，但是urlopen()的 read()是ASCII/bytes 字符串。
　　The fix here is to compile a bytes object instead of a text string.
　　即改为
　　　　REGIX = compile(b'#([\d,]+)')

或者在pycharm中使用正则替换:
(print) +([^#\n]*)
$1($2)
'''
import os
import sys

from public_function import PYTHON_2TO3_PATH


def python2_to_python3(source_file):
    print(source_file)
    cmd = PYTHON_2TO3_PATH + " -w " + os.path.abspath(source_file)
    # print(cmd)
    result = os.popen(cmd)
    list_result = result.readlines()
    for i, fileLine in enumerate(list_result):
        print(fileLine)


def batch_deal(source_dir):
    if os.path.isdir(source_dir):
        for parent, dirnames, filenames in os.walk(source_dir):
            for filename in filenames:
                source_file = os.path.join(parent, filename).encode('utf-8').decode('utf-8')
                if source_file.endswith('.py'):
                    # 文件不为空
                    if os.path.getsize(source_file):
                        python2_to_python3(source_file)
                else:
                    with open(source_file, 'r', encoding='gbk', errors='ignore') as fr:
                        first_line = fr.readline()  # 取第一行
                        if "python" in first_line:
                            # 文件不为空
                            if os.path.getsize(source_file):
                                python2_to_python3(source_file)
    else:
        python2_to_python3(source_dir)


def main():
    print("系统默认编码:", sys.getdefaultencoding())  # 系统默认编码
    import locale
    print(locale.getpreferredencoding())
    # source_file = r"D:\share\git\gitee\nirvanaReborn\python3_data_analysis\src\data_analysis\media\picture\18_get_exif.py"
    # python2_to_python3(source_file)
    # source_dir = r"D:\SVN\SVN_test\trunk\kungfu"
    # source_dir = r"D:\share\git\gitee\zhangtao2016\kungfu-0.0.2\kungfu"
    source_dir = r"D:\BaiduNetdiskDownload\rcm-demo2\rcm-demo\python"
    # batch_deal(source_dir)


if __name__ == "__main__":
    main()
