#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
#

import os, re, pprint
from public_function import PYTHON_PIP_PATH


def _sep(s):
    sep = re.compile(r'\s+')
    s = sep.sub(' ', s).strip()
    return s.split(' ')


def getDict():
    alllib = os.popen(PYTHON_PIP_PATH + ' list').read()
    alist = alllib.splitlines()
    assert len(alist) > 2, alllib
    alist = alist.__iter__()
    assert next(alist).startswith('Package'), alllib
    assert next(alist).startswith('---'), alllib
    return dict(map(_sep, alist))


def keepInstalled(libset):
    libdict = getDict()
    pprint.pprint(libdict)
    for lib in libset:
        if lib not in libdict:
            # print(os.popen(f"pip3 install {lib}").read())
            print('需要安装{0}'.format(lib))


def main():
    keepInstalled(['cffi', 'nnpy'])


if __name__ == "__main__":
    main()
