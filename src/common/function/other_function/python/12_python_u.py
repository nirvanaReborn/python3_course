#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://blog.csdn.net/dream_allday/article/details/94646721
# python -u参数分别在python2和3中的情况
# 执行命令: python3 -u 脚本名字.py
import sys


def main():
    sys.stdout.write("stdout1")
    sys.stderr.write("stderr1")
    sys.stdout.write("stdout2")
    sys.stderr.write("stderr2")

    # sys.stdout.buffer.write(b"1")
    # sys.stderr.buffer.write(b"2")
    # sys.stdout.buffer.write(b"3")
    # sys.stderr.buffer.write(b"4")


if __name__ == "__main__":
    main()
