#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 将一个第三方 Python 库的 zip 文件转换为 .whl 格式（wheel 文件）
import zipfile
import tarfile
import os
import subprocess


def main():
    # 路径配置
    # zip_file = r'D:\ARESHelper\pybrain-0.3.1.zip'
    zip_file = r'D:\ARESHelper\xcsc_tushare-1.0.0.tar.gz'
    if str(zip_file).endswith('.zip'):
        extracted_dir = r'D:\ARESHelper\%s' % os.path.basename(zip_file)[:-4]
        # 解压 zip 文件
        with zipfile.ZipFile(zip_file, 'r') as zip_ref:
            zip_ref.extractall(extracted_dir)
    else:
        extracted_dir = r'D:\ARESHelper\%s\%s' % (os.path.basename(zip_file)[:-7], os.path.basename(zip_file).split('-')[0])
        # 创建解压目标目录
        os.makedirs(extracted_dir, exist_ok=True)
        # 解压 .tar.gz 文件
        with tarfile.open(zip_file, 'r:gz') as tar_ref:
            tar_ref.extractall(extracted_dir)

    # 切换到解压目录
    os.chdir(extracted_dir)

    # 生成 wheel 文件
    subprocess.run(['python', 'setup.py', 'bdist_wheel'])


if __name__ == '__main__':
    main()
