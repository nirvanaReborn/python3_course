#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#
#

import os

from public_function import PYTHON_2TO3_PATH
from public_function import PYTHON_PIP_PATH


class PythonTool():
    # 初始化函数，类似于构造函数
    def __init__(self, dest_file="requirements.txt"):
        super(PythonTool, self).__init__()
        self.pip_path = PYTHON_PIP_PATH
        self.python_2to3_path = PYTHON_2TO3_PATH
        self.dest_file = os.path.abspath(dest_file)

    def install_all_module(self):
        # 安装依赖文件
        cmd = self.pip_path + " install -r " + self.dest_file
        result = os.system(cmd)

    def install_module(self, list_module):
        command = "install "
        # command += " --user "
        option1 = "-i https://pypi.douban.com/simple"
        option2 = "--trusted-host pypi.douban.com"
        list_cmd = [self.pip_path, command, option1, option2]

        for module in list_module:
            print("*" * 100)
            print("安装模块：", module)
            list_cmd.append(module)
            cmd = " ".join(list_cmd)
            print(cmd)
            result = os.system(cmd)

    def uninstall_module(self, list_module):
        command = "uninstall"
        list_cmd = [self.pip_path, command, ]
        for module in list_module:
            print("卸载模块：", module)
            list_cmd.append(module)
            cmd = " ".join(list_cmd)
            print(cmd)
            result = os.system(cmd)

    def update_module(self, list_module):
        command = "install --user --upgrade"
        option1 = "-i https://pypi.douban.com/simple"
        option2 = "--trusted-host pypi.douban.com"
        list_cmd = [self.pip_path, command, option1, option2]
        for module in list_module:
            print("更新模块：", module)
            list_cmd.append(module)
            cmd = " ".join(list_cmd)
            print(cmd)
            result = os.system(cmd)

    def update_all_module(self):
        list_module = self.get_all_outdated_module()
        print("更新所有已安装的模块：", list_module)
        if "OK" == input():
            PythonTool().update_module(list_module)

    # 获取过期模块
    def get_all_outdated_module(self):
        list_module = []
        command = "list"
        option1 = "--outdated"
        option2 = "--format=columns"
        list_cmd = [self.pip_path, command, option1, option2]
        cmd = " ".join(list_cmd)
        cmd_result = os.popen(cmd)
        list_1D = cmd_result.readlines()
        for i, fileLine in enumerate(list_1D):
            if i >= 2:
                # ['Package', 'Version', 'Latest', 'Type']
                list_module.append(str(fileLine).split()[0])
        return list_module

    # 获取所有已安装的模块
    def get_all_install_module(self):
        list_module = []
        # 生成依赖文件
        cmd = self.pip_path + " freeze > " + self.dest_file
        result = os.system(cmd)
        # print(result) # 0
        print("生成所有安装模块重定向到文件中：", self.dest_file)

        with open(self.dest_file, 'r', encoding='UTF-8') as fr:
            for fileLine in fr.readlines():
                list_module.append(str(fileLine).split("==")[0])

        # pprint.pprint(list_module)
        return list_module

    def python2_to_python3(self, source_file):
        cmd = self.python_2to3_path + " -w " + os.path.abspath(source_file)
        # print(cmd)
        list_result = os.popen(cmd).readlines()
        for i, fileLine in enumerate(list_result):
            print(fileLine)
