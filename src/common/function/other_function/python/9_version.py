#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys


def main():
    print(sys.version_info)  # sys.version_info(major=3, minor=7, micro=3, releaselevel='final', serial=0)

    if sys.version_info >= (3, 6):
        print("YES")
    else:
        print("NO")


if __name__ == "__main__":
    main()
