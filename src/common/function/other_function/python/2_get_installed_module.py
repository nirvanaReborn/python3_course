#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os

import numpy
import pandas
from public_class_PythonTool import PythonTool

from public_function import PYTHON_PIP_PATH


def test_1():
    # pip freeze --all > requirements.txt
    # 从 Python 3.8 开始，标准库中的 importlib.metadata 模块提供了一种获取已安装包元数据的方法，包括依赖信息。
    import importlib.metadata

    for package in importlib.metadata.distributions():
        print(f"{package.metadata['Name']}=={package.version}")


def get_list_file_content(source_file):
    # pip install chardet chardet2
    import chardet

    with open(source_file, "rb") as fd:
        encoding = chardet.detect(fd.read(1024))["encoding"]

    with open(source_file, 'r', encoding=encoding) as fr:
        list_file_content = fr.readlines()

    return list_file_content


# 对比requirements.txt和pip list命令
def compare_install_all_module(exe_path, dest_file):
    list_file_module = []
    list_file_content = get_list_file_content(dest_file)
    for i, fileLien in enumerate(list_file_content):
        list_file_module.append(str(fileLien).split("==")[0])

    list_module = []
    cmd = exe_path + " list"
    cmd_result = os.popen(cmd)
    list_1D = cmd_result.readlines()
    for i, fileLine in enumerate(list_1D):
        if i >= 2:
            # ['Package', 'Version']
            list_module.append(str(fileLine).split()[0])

    list_1 = list(set(list_module) - set(list_file_module))
    list_2 = list(set(list_file_module) - set(list_module))
    print("文件中缺少的", list_1)
    print("文件中多余的", list_2)
    return list_1, list_2


def get_other_module(source_dir):
    list_module = []
    list_file = os.listdir(source_dir)
    for i, file in enumerate(list_file):
        list_module.append("_".join(str(file).split("_")[0:-1]))

    # print(list_module)
    return list_module


# 获取python3所有的标准库
def get_all_std_module():
    list_module = []
    source_file = r"../../../../../doc/Python3.xlsx"
    dataframe = pandas.read_excel(source_file, sheet_name='Python3标准库')
    list_sheet = list(numpy.array(dataframe).tolist())  # 已经跳过标题行
    for i, fileLine in enumerate(list_sheet):
        list_module.append(fileLine[1])

    # print(list_module)
    return list_module


def main():
    exe_path = PYTHON_PIP_PATH
    print(exe_path)
    dest_file = "requirements.txt"

    choice = str(input("Enter a positive integer to choice: "))
    if choice == "1":
        PythonTool().get_all_install_module()
    elif choice == "2":
        set_2 = set(get_all_std_module())
        set_3 = set(get_other_module(r'../../../module/built-in_module'))
        set_4 = set(get_other_module(r'../../../module/other_module'))

        # PythonTool().install_module(list(set_4))
        print("built-in_module目录中多余的:", sorted(set_3 - set_2))
        print("other_module目录中多余的:", sorted(set_4 & set_2))
    elif choice == "3":
        list_1, list_2 = compare_install_all_module(exe_path, dest_file)
        PythonTool().install_module(list_2)
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
