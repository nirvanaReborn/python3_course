#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
安装：pip install pyinstaller

usage: pyinstaller [-h] [-v] [-D] [-F] [--specpath DIR] [-n NAME]
                   [--add-data <SRC;DEST or SRC:DEST>]
                   [--add-binary <SRC;DEST or SRC:DEST>] [-p DIR]
                   [--hidden-import MODULENAME]
                   [--additional-hooks-dir HOOKSPATH]
                   [--runtime-hook RUNTIME_HOOKS] [--exclude-module EXCLUDES]
                   [--key KEY] [-d {all,imports,bootloader,noarchive}] [-s]
                   [--noupx] [--upx-exclude FILE] [-c] [-w]
                   [-i <FILE.ico or FILE.exe,ID or FILE.icns>]
                   [--version-file FILE] [-m <FILE or XML>] [-r RESOURCE]
                   [--uac-admin] [--uac-uiaccess] [--win-private-assemblies]
                   [--win-no-prefer-redirects]
                   [--osx-bundle-identifier BUNDLE_IDENTIFIER]
                   [--runtime-tmpdir PATH] [--bootloader-ignore-signals]
                   [--distpath DIR] [--workpath WORKPATH] [-y]
                   [--upx-dir UPX_DIR] [-a] [--clean] [--log-level LEVEL]
                   scriptname [scriptname ...]

使用 PyInstaller库需要注意以下问题：
◆ 文件路径中不能出现空格和英文句号（.）；
◆ 源文件必须是UTF-8编码，暂不支持其他编码类型。采用IDLE编写的源文件都保存为UTF -8编码形式，可直接使用即可。

【问题】UnicodeDecodeError: 'utf-8' codec can't decode byte 0xce in position 141: invalid continuation byte
1.在打包的命令行中先输入chcp 65001 然后再输入打包命令。
2.或者修改D:\Python34\Lib\site-packages\PyInstaller\compat.py文件中: out = out.decode(encoding, errors='ignore')

# Python中的图片打包与pyinstaller中的spec文件简介
# https://blog.csdn.net/SCDS_Zyx/article/details/82052396

'''
# pip installer pyinstaller
import os

from public_function import PYTHON_PYINSTALLER_PATH, PYTHON_PATH


# https://www.cnblogs.com/danvy/p/10505731.html
def get_file_version_info(exe_path):
    source_path = os.path.join(os.path.dirname(PYTHON_PATH), r"Lib\site-packages\PyInstaller\utils\cliutils\grab_version.py")
    list_cmd_arg = [
        PYTHON_PATH,
        source_path,
        exe_path,
    ]
    cmd = " ".join(list_cmd_arg)
    print(cmd)
    list_result = os.popen(cmd).readlines()
    for i, fileLine in enumerate(list_result):
        print(fileLine)


def pyinstaller_base_cmd(cmd_arg):
    exe_path = PYTHON_PYINSTALLER_PATH
    if not os.path.exists(exe_path):
        print("路径不存在:", exe_path)
    cmd = exe_path + " " + cmd_arg
    # print(cmd)
    list_result = os.popen(cmd).readlines()
    for i, fileLine in enumerate(list_result):
        print(fileLine)


def pyinstaller_version():
    pyinstaller_base_cmd(" -v ")
    pyinstaller_base_cmd(" -h ")


def python3_to_exe(source_file):
    source_file = os.path.abspath(source_file)
    source_dir = os.path.dirname(source_file)
    list_arg = [
        "-F",  # 生成单个可执行exe文件
        "-w",  # 去掉控制台窗口
        "--clean",  # 清理编译时的临时文件
        # " -i RfReportGenTool.ico", # 表示可执行文件的图标
        "--specpath " + source_dir,  #
        "--distpath " + os.path.join(source_dir, "dist"),  # 指定生成的可执行文件存放的目录，默认存放在dist目录下
        "--workpath " + os.path.join(source_dir, "build"),  # 指定编译中临时文件存放的目录，默认存放在build目录下
        "--version-file " + os.path.join(source_dir, "file_version_info.txt"),  # windows里可把版本信息注射到exe里
        source_file,  # 源文件
    ]
    pyinstaller_base_cmd(" ".join(list_arg))


def main():
    exe_path = r"D:\SVN\PBOX\trunk\Sources\ClientCom_CSharp\HSUCF2.0\HSUCF2.0.exe"
    get_file_version_info(exe_path)

    # pyinstaller_version()

    # source_file = r"D:\share\git\gitee\nirvanaReborn\python3_GUI\src\tkinter\maze\maze_main.py"
    # python3_to_exe(source_file)


if __name__ == "__main__":
    main()
