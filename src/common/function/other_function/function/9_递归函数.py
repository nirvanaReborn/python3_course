#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
每个递归函数必须包含两个部分：
1. 终止条件
    表示递归什么时候结束。一般用于返回值，不再调用自己。
2. 递归步骤
    把第n 步的值和第n-1 步相关联。

# 简单的程序是递归的优点之一。
# 但是递归调用会占用大量的系统堆栈，内存耗用多，在递归调用层次多时速度要比循环慢的多，所以在使用递归时要慎重。
# 递归函数由于会创建大量的函数对象、过量的消耗内存和运算能力。在处理大量数据时，谨慎使用。
'''


# 使用递归函数计算阶乘
def factorial(n):
    if n == 1:
        return 1
    return n * factorial(n - 1)


def main():
    for i in range(1, 6):
        print(i, '!=', factorial(i))


if __name__ == "__main__":
    main()
