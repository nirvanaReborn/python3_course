#! /usr/bin/python
# -*- coding:utf-8 -*-


# ------------------------------1.位置参数-----------------------------------------
# 函数调用时，实参默认按位置顺序传递，需要个数和形参匹配。按位置传递的参数，称为：“位置参数”。


# -----------------------------2.默认值参数----------------------------------------
def test_1():
    def ask_ok(prompt, retries=4, complaint='Yes or no, please!'):
        while True:
            ok = input(prompt)
            if ok in ('y', 'ye', 'yes'):
                return True
            if ok in ('n', 'no', 'nop', 'nope'):
                return False
            retries = retries - 1
            if retries < 0:
                raise OSError('uncooperative user')
            print(complaint)

    # 这个函数可以通过几种不同的方式调用:
    # 这个例子还介绍了 in 关键字。它测定序列中是否包含某个确定的值。

    # 1.只给出必要的参数:
    ask_ok('Do you really want to quit?')

    # 2.给出一个可选的参数:
    ask_ok('OK to overwrite the file?', 2)

    # 3.或者给出所有的参数:
    ask_ok('OK to overwrite the file?', 2, 'Come on, only yes or no!')


# -------------------------------------------------------------------------------
# 重要警告: 默认值只被赋值一次。这使得当默认值是可变对象时会有所不同，比如列表、字典或者大多数类的实例。
# 例如，下面的函数在后续调用过程中会累积（前面）传给它的参数:
def test_2():
    def f(a, L=[]):
        L.append(a)
        return L

    print(f(1))
    print(f(2))
    print(f(3))

    # 这将输出:
    # [1]
    # [1, 2]
    # [1, 2, 3]


# -------------------------------------------------------------------------------
# 如果你不想让默认值在后续调用中累积，你可以像下面一样定义函数:
def test_3():
    def f(a, L=None):
        if L is None:
            L = []
        L.append(a)
        return L

    print(f(1))
    print(f(2))
    print(f(3))

    # 这将输出:
    # [1]
    # [2]
    # [3]


# -----------------------------3.命名参数(关键字参数)---------------------------------------
def test_4():
    def parrot(voltage, state='a stiff', action='voom', type='Norwegian Blue'):
        print("-- This parrot wouldn't", action, end=' ')
        print("if you put", voltage, "volts through it.")
        print("-- Lovely plumage, the", type)
        print("-- It's", state, "!")

    # 接受一个必选参数 (voltage) 以及三个可选参数 (state, action, 和 type)。可以用以下的任一方法调用:
    parrot(1000)  # 1 positional argument
    parrot(voltage=1000)  # 1 keyword argument
    parrot(voltage=1000000, action='VOOOOOM')  # 2 keyword arguments
    parrot(action='VOOOOOM', voltage=1000000)  # 2 keyword arguments
    parrot('a million', 'bereft of life', 'jump')  # 3 positional arguments
    parrot('a thousand',
           state='pushing up the daisies')  # 1 positional, 1 keyword

    # 不过以下几种调用是无效的:
    # parrot()                     # required argument missing
    # parrot(voltage=5.0, 'dead')  # non-keyword argument after a keyword argument
    # parrot(110, voltage=220)     # duplicate value for the same argument
    # parrot(actor='John Cleese')  # unknown keyword argument

    print("#" * 80)


# ------------------------------4.可变参数------------------------------------
# 可变参数指的是“可变数量的参数”。分两种情况：
# (1). *param（一个星号），将多个参数收集到一个“元组”对象中。
# (2). **param（两个星号），将多个参数收集到一个“字典”对象中。

def test_5():
    def cheeseshop(kind, *arguments, **keywords):
        print("-- Do you have any", kind, "?")
        print("-- I'm sorry, we're all out of", kind)
        print("-" * 40)
        for arg in arguments:
            print(arg)
        print("-" * 40)
        keys = sorted(keywords.keys())
        for kw in keys:
            print(kw, ":", keywords[kw])

    # 它可以像这样调用:
    cheeseshop("Limburger",
               "It's very runny, sir.",
               "It's really very, VERY runny, sir.",
               shopkeeper="Michael Palin",
               client="John Cleese",
               sketch="Cheese Shop Sketch")

    print("#" * 80)


'''
最后，一个最不常用的选择是可以让函数调用可变个数的参数。
这些参数被包装进一个元组.
在这些可变个数的参数之前，可以有零到多个普通的参数.
通常，这些可变参数是参数列表中的最后一个.
任何出现在 *args 后的参数是关键字参数.
'''


def test_6():
    def concat(*args, sep="/"):
        return sep.join(args)

    print(concat("earth", "mars", "venus"))
    # earth/mars/venus
    print(concat("earth", "mars", "venus", sep="."))
    # earth.mars.venus

    print("#" * 80)


# ---------------------------------参数列表的分拆-------------------------------
'''
另有一种相反的情况: 当你要传递的参数已经是一个列表，但要调用的函数却接受分开一个个的参数值。
这时候你要把已有的列表拆开来。
例如内建函数 range() 需要要独立的 start，stop 参数。
你可以在调用函数时加一个 * 操作符来自动把参数列表拆开。
以同样的方式，可以使用 ** 操作符分拆关键字参数为字典。
'''


def test_7():
    args = [3, 6]
    print(list(range(*args)))

    # 以同样的方式，可以使用 ** 操作符分拆关键字参数为字典
    def parrot(voltage, state='a stiff', action='voom'):
        print("-- This parrot wouldn't", action, end=' ')
        print("if you put", voltage, "volts through it.", end=' ')
        print("E's", state, "!")

    d = {"voltage": "four million", "state": "bleedin' demised",
         "action": "VOOM"}
    parrot(**d)


# ---------------------------------5.强制命名参数-------------------------------
# 在带星号的“可变参数”后面增加新的参数，必须在调用的时候“强制命名参数”。
def test_8():
    def f1(*a, b, c):
        print(a, b, c)

    # f1(2,3,4) #会报错。由于a 是可变参数，将2,3,4 全部收集。造成b 和c 没有赋值。
    f1(2, b=3, c=4)


# ------------------------------------------------------------------------------
def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
