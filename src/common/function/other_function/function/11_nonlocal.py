#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# nonlocal 用来声明外层的局部变量。
# global   用来声明全局变量。


# 测试nonlocal、global 关键字的用法
a = 100


def outer():
    b = 10

    def inner():
        nonlocal b  # 声明外部函数的局部变量
        print("inner b:", b)
        b = 20
        global a  # 声明全局变量
        a = 1000

    inner()
    print("outer b:", b)


def main():
    outer()
    print("a：", a)


if __name__ == "__main__":
    main()
