#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6605423466722099715/
# 如何用十分钟学会函数式 Python？

# ------------------------------函数式模型----------------------------------------------------
'''
在命令式模型中，执行程序的方式是给计算机一系列指令让它执行。执行过程中计算机会改变状态。
例如，比如 A 的初始值是 5，后来改变了 A 的值。那么 A 就是个变量，而变量的意思就是包含的值会改变。
而在函数式模式中，你不需要告诉计算机做什么，而是告诉计算机是什么。
比如数字的最大公约数是什么，1 到 n 的乘积是什么等等。因此，变量是不能被改变的。
变量一旦被设置，就永远保持同一个值（注意在纯粹的函数式语言中，它们不叫变量）。
因此，在函数式模型中，函数没有副作用。副作用就是函数对函数外的世界做出的改变。
来看看下面这段Python代码的例子：
'''
a = 3


def some_func():
    global a
    a = 5


some_func()
print(a)

'''
代码的输出是 5。在函数式模型中，改变变量的值是完全不允许的，让函数影响函数外的世界也是不允许的。
函数唯一能做的就是做一些计算然后返回一个值。
你可能会想：“没有变量也没有副作用？这有什么好的？”
如果函数使用同样的参数调用两次，那么我们可以保证它会返回同样的结果。如果你学过数学函数，你肯定知道这样做的好。
这叫做引用透明性（referential transparency）。由于函数没有副作用，那么我们可以加速计算某个东西的程序。
比如，如果程序知道 func(2)返回 3，那么可以将这个值保存在表中，这样就不需要重复运行我们早已知道结果的函数了。
通常，函数式编程不使用循环，而是使用递归。
递归是个数学概念，通常的意思是“把结果作为自己的输入”。使用递归函数，函数可以反复调用自己。
函数式编程语言也是懒惰的。
下面就是个使用Python定义的递归函数的例子：
'''


def factorial_recursive(n):
    # Base case: 1! = 1
    if n == 1:
        return 1
    # Recursive case: n! = n * (n-1)!
    else:
        return n * factorial_recursive(n - 1)


print(factorial_recursive(5))

# ----------------------------------映射------------------------------------------------
'''
要理解映射（map），首先需要理解什么是可迭代对象。可迭代对象（iterable）指任何可以迭代的东西。
通常是列表或数组，但 Python 还有许多其他可迭代对象。
甚至可以自定义对象，通过实现特定的魔术方法使其变成可迭代对象。
魔术方法就像 API 一样，能让对象更有 Python 风格。
要让对象变成可迭代对象，需要实现以下两个魔术方法：
'''


class Counter:
    def __init__(self, low, high):
        # set class attributes inside the magic method __init__
        # for "inistalise"
        self.current = low
        self.high = high

    def __iter__(self):
        # first magic method to make this object iterable
        return self

    def __next__(self):
        # second magic method
        if self.current > self.high:
            raise StopIteration
        else:
            self.current += 1
        return self.current - 1


for c in Counter(3, 8):
    print(c)

'''
在 Python 中，迭代器就是只实现了__iter__魔术方法的对象。也就是说，你可以访问对象中都包含的位置，但无法遍历整个对象。
一些对象实现了__next__魔术方法，但没有实现__iter__魔术方法，比如集合（本文稍后会讨论）。
现在我们知道了什么是可迭代的对象，回过头来讨论下映射函数。
映射可以对可迭代对象中的每个元素执行指定的函数。通常，我们对列表中的每个元素执行函数，但要知道映射其实可以针对绝大多数可迭代对象使用。
map(function, iterable)
'''
list_1 = [1, 2, 3, 4, 5]


def square(num):
    return num * num


print(list(map(square, list_1)))
'''
Python中的函数式函数是懒惰的。如果我们不加“list()”，那么函数只会将可迭代对象保存下来，而不会保存结果的列表。
我们需要明确地告诉Python“把它转换成列表”才能得到结果。
这个“square(num)”的确不错，但总觉得有点不对劲。
难道为了仅使用一次的map就得定义整个函数吗？其实我们可以使用lambda函数（匿名函数）。
'''

# ------------------------------------归纳----------------------------------------------
# 归纳（reduce）是个函数，它把一个可迭代对象变成一个东西。通常，我们在列表上进行计算，将列表归纳成一个数字。
# 语法如下：reduce(function, list)
# reduce()传入的函数 f 必须接收两个参数
# reduce()对list的每个元素反复调用函数f，并返回最终结果值。
product = 1
x = [1, 2, 3, 4]
for num in x:
    product = product * num

# 但使用归纳，可以写成这样：
import functools

product_2 = functools.reduce((lambda x, y: x * y), [1, 2, 3, 4])
print(product_2)

# ------------------------------------过滤----------------------------------------------
# 过滤（filter）函数接收一个可迭代对象，然后过滤掉对象中一切不需要的东西。
# 通常过滤接收一个函数和一个列表。它会针对列表中的每个元素执行函数，如果函数返回True，则什么都不做。如果函数返回False，则从列表中去掉那个元素。
# 语法如下：filter(function, list)
x = range(-5, 5)
new_list = []
for num in x:
    if num < 0:
        new_list.append(num)

# 使用过滤可以写成这样：
x = range(-5, 5)
all_less_than_zero = list(filter(lambda num: num < 0, x))

# ------------------------------------高阶函数----------------------------------------------
'''
Python 中的所有函数都是顶级对象。顶级对象是拥有一个或多个以下特征的对象：
1.在运行时生成;
2.赋值给某个数据结构中的变量或元素;
3.作为参数传递给函数;
4.作为函数的结果返回;
所以，所有 Python 中的函数都是对象，都可以用作高阶函数。
'''


# 高阶函数接收函数作为参数，返回另一个函数。一个非常简单的例子如下所示：
def summation(nums):
    return sum(nums)


def action(func, numbers):
    return func(numbers)


print(action(summation, [1, 2, 3]))


# Output is 6

def rtnBrandon():
    return "brandon"


def rtnJohn():
    return "john"


def rtnPerson():
    age = int(input("What's your age?"))
    if age == 21:
        return rtnBrandon()
    else:
        return rtnJohn()


# ------------------------------------部分函数----------------------------------------------
# 部分函数有点难懂，但非常酷。通过它，你不需要提供完整的参数就能调用函数。我们来看个例子。
def power(base, exponent):
    return base ** exponent


# 现在我们需要一个求平方的函数，可以这么写：
def square(base):
    return power(base, 2)


# 这段代码没问题，但如果需要立方函数怎么办？或者四次方函数呢？是不是得一直定义新的函数？这样做也行，但是程序员总是很懒的。
# 如果需要经常重复一件事情，那就意味着一定有办法提高速度，避免重复。
# 我们可以用部分函数实现这一点。下面是使用部分函数求平方的例子：
square = functools.partial(power, exponent=2)
print(square(2))
# output is 4

# 还可以使用循环来生成直到能计算 1000 次方的所有函数。
powers = []
for x in range(2, 1001):
    powers.append(functools.partial(power, exponent=x))
print(powers[0](3))
# output is 9


# ------------------------------------函数式编程不够 Python----------------------------------------------
'''
在 Python IDLE 中敲“import this”，可以看到下面的内容：
这就是Python之禅。这首诗表明了什么叫做Python风格。我们要指出的是这句话：任何事情应该有一个且只有一个方法解决。
在 Python 中，映射和过滤能做到的事情，列表解析式（稍后介绍）也能做到。这就打破了 Python 之禅，因此我们说函数式编程的部分不够“Python”。
另一个常被提及的地方就是lambda。在Python中，lambda函数就是个普通的函数。
普通的函数能做到一切 lambda 能做到的事情，但反过来却不行。lambda 不能完成普通函数能完成的一切事情。
'''

# ------------------------------------列表解析式----------------------------------------------
# 之前我说过，任何能用映射或过滤完成的事情都可以用列表解析式完成。这就是我们要学的东西。
# 列表解析式是 Python 生成列表的方式。语法如下：[function for item in iterable]
# 要想求列表中每个数字的平方，可以这么写：
print([x * x for x in [1, 2, 3, 4]])

# 可以看到，我们给列表中的每个元素应用了一个函数。那么怎样才能实现过滤呢？
x = range(-5, 5)
all_less_than_zero = [num for num in x if num < 0]
# 像这样，列表解析式支持 if 语句。这样就不需要写一堆函数来实现了。实际上，如果你需要生成某种列表，那么很有可能使用列表解析式更方便、更简洁。

# 如果想求所有小于 0 的数字的平方呢？使用 Lambda、映射和过滤可以写成：
x = range(-5, 5)
all_less_than_zero = list(map(lambda num: num * num, list(filter(lambda num: num < 0, x))))

# 看上去似乎很长，而且有点复杂。用列表解析式只需写成：
x = range(-5, 5)
all_less_than_zero = [num * num for num in x if num < 0]
# 不过列表解析式只能用于列表。映射和过滤能用于一切可迭代对象。那为什么还要用列表解析式呢？其实，解析式可以用在任何可迭代的对象上。

# ------------------------------------其他解析式----------------------------------------------
# 可以在任何可迭代对象上使用解析式。
# 任何可迭代对象都可以用解析式生成。从 Python 2.7 开始，甚至可以用解析式生成字典（哈希表）。
DIAL_CODES = [
    (86, 'China'),
    (91, 'India'),
    (1, 'United States'),
    (62, 'Indonesia'),
    (55, 'Brazil'),
    (92, 'Pakistan'),
    (880, 'Bangladesh'),
    (234, 'Nigeria'),
    (7, 'Russia'),
    (81, 'Japan'),
]

country_code = {country: code for code, country in DIAL_CODES}
print(country_code)
print({code: country.upper() for country, code in country_code.items() if code < 66})

import sys

if sys.version_info[0] == 2:  # python2
    from unicodedata import name

    print({chr(i) for i in range(32, 256) if 'SIGN' in name(chr(i), '')})
