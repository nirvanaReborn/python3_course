#!/usr/bin/env python
# -*- coding:utf-8 -*-


# http://www.pythondoc.com/pythontutorial3/controlflow.html#tut-defining
# 函数注解


def function_annotations(ham: 42, eggs: int = 'spam') -> "Nothing to see here":
    print("Annotations:", f.__annotations__)
    print("Arguments:", ham, eggs)


def main():
    function_annotations('wonderful')


if __name__ == "__main__":
    main()
