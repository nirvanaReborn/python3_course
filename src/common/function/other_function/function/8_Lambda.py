#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
lambda 表达式可以用来声明匿名函数。lambda 函数是一种简单的、在同一行中定义函数的方法。
Lambda表达式就是只有一行的函数。lambda 函数实际生成了一个函数对象。
lambda 表达式只允许包含一个表达式，不能包含复杂语句，该表达式的计算结果就是函数的返回值。
lambda 表达式的基本语法如下：
    lambda arg1,arg2,arg3... : <表达式>
    其中arg1/arg2/arg3 为函数的参数。<表达式>相当于函数体。运算结果是：表达式的运算结果。

比如下面这个lambda表达式可以求出给定数字的平方：
有了lambda表达式，所有参数都放在左边，操作都放在右边。虽然看上去有点乱，但不能否认它的作用。
实际上能写出只有懂得函数式编程的人才能看懂的代码还是有点小兴奋的。而且把函数变成一行也非常酷。
'''


def test_1():
    square = lambda x: x * x
    print(square(3))
    # 这样我们的求平方的代码可以简化成一行：
    list_1 = [1, 2, 3, 4, 5]
    print(list(map(lambda num: num * num, list_1)))


def test_2():
    f = lambda a, b, c: a + b + c
    print(f)
    print(f(2, 3, 4))
    g = [lambda a: a * 2, lambda b: b * 3, lambda c: c * 4]
    print(g[0](6), g[1](7), g[2](8))


def main():
    test_1()
    test_2()


if __name__ == "__main__":
    main()
