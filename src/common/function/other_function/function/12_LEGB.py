#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
Python 在查找“名称”时，是按照LEGB 规则查找的：Local-->Enclosed-->Global-->Built in
    Local 指的就是函数或者类的方法内部
    Enclosed 指的是嵌套函数（一个函数包裹另一个函数，闭包）
    Global 指的是模块中的全局变量
    Built in 指的是Python 为自己保留的特殊名称。

如果某个name 映射在局部(local)命名空间中没有找到，
接下来就会在闭包作用域(enclosed)进行搜索，
如果闭包作用域也没有找到，Python 就会到全局(global)命名空间中进行查找，
最后会在内建(built-in)命名空间搜索
（如果一个名称在所有命名空间中都没有找到，就会产生一个NameError）。
'''
# 测试LEGB:我们依次将几个str 注释掉，观察控制台打印的内容，体会LEBG 的搜索顺序。
str = "3_global"


def outer():
    str = "2_outer"

    def inner():
        str = "1_inner"
        print(str)

    inner()


def main():
    outer()


if __name__ == "__main__":
    main()
