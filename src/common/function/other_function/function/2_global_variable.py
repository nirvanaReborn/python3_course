#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
全局变量：
    1. 在函数和类定义之外声明的变量。作用域为定义的模块，从定义位置开始直到模块结束。
    2. 全局变量降低了函数的通用性和可读性。应尽量避免全局变量的使用。
    3. 全局变量一般做常量使用。
    4. 函数内要改变全局变量的值，使用global 声明一下
局部变量：
    1. 在函数体中（包含形式参数）声明的变量。
    2. 局部变量的引用比全局变量快，优先考虑使用。
    3. 如果局部变量和全局变量同名，则在函数内隐藏全局变量，只使用同名的局部变量
    4. 局部变量的查询和访问速度比全局变量快，优先考虑使用，尤其是在循环的时候。
       在特别强调效率的地方或者循环次数较多的地方，可以通过将全局变量转为局部变量提高运行速度。
'''

import time

# 测试局部变量、全局变量的效率
import math


def test01():
    start = time.time()
    for i in range(10000000):
        math.sqrt(30)
    end = time.time()
    print("全局变量耗时{0}".format((end - start)))


def test02():
    b = math.sqrt
    start = time.time()
    for i in range(10000000):
        b(30)
    end = time.time()
    print("局部变量耗时{0}".format((end - start)))


def main():
    test01()
    test02()


if __name__ == "__main__":
    main()
