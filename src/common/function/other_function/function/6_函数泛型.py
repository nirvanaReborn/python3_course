#!/usr/bin/env python
# -*- coding:utf-8 -*-

# python中定义泛型方法,需要用到functools.singledispatch

import functools


# 泛型方法
@functools.singledispatch
def function_genericity(arg, verbose=False):
    if verbose:
        print('Let me just say:', end=' ')
    print(arg)


# 重载方法
# @function_genericity.register(int)
# def _(arg, verbose=False):
#     if verbose:
#         print('accept int param:', end=' ')
#     print(arg)


def main():
    function_genericity('123', True)
    function_genericity(123, True)


if __name__ == "__main__":
    main()
