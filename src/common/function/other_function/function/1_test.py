#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''Python 中函数分为如下几类：
1. 内置函数
    我们前面使用的str()、list()、len()等这些都是内置函数，我们可以拿来直接使用。
2. 标准库函数
    我们可以通过import 语句导入库，然后使用其中定义的函数
3. 第三方库函数
    Python 社区也提供了很多高质量的库。下载安装这些库后，也是通过import 语句导入，然后可以使用这些第三方库的函数
4. 用户自定义函数
    用户自己定义的函数，显然也是开发中适应用户自身需求定义的函数。今天我们学习的就是如何自定义函数。
'''


def test_1(a, b, c):
    '''我们调用help(函数名.__doc__)可以打印输出函数的文档字符串。'''
    print(a, b, c)
    print(locals())  # 打印输出的局部变量
    print("#" * 20)
    print(globals())  # 打印输出的全局变量


# 高阶函数-map、filter、reduce
# 在python 3.0.0.0以后, reduce已经不在built-in function里了, 要用它就得from functools import reduce.
def test_2():
    '''
    # 归纳（reduce）是个函数，它把一个可迭代对象变成一个东西。通常，我们在列表上进行计算，将列表归纳成一个数字。
    # 语法如下：reduce(function, list)
    # reduce()传入的函数 f 必须接收两个参数
    # reduce()对list的每个元素反复调用函数f，并返回最终结果值。
    '''
    from functools import reduce
    product_2 = reduce((lambda x, y: x * y), [1, 2, 3, 4])
    print(product_2)


def main():
    print(test_1.__doc__)
    # help(test_1.__doc__)
    test_1(2, 3, 4)


if __name__ == "__main__":
    main()
