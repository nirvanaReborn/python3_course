#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
参数的传递
    函数的参数传递本质上就是：从实参到形参的赋值操作。Python 中“一切皆对象”，所有的赋值操作都是“引用的赋值”。
    所以，Python 中参数的传递都是“引用传递”，不是“值传递”。具体操作时分为两类：
    1. 对“可变对象”进行“写操作”，直接作用于原对象本身。
    2. 对“不可变对象”进行“写操作”，会产生一个新的“对象空间”，并用新的值填充这块空间。（起到其他语言的“值传递”效果，但不是“值传递”）
    可变对象有：字典、列表、集合、自定义的对象等
    不可变对象有：数字、字符串、元组、function 等


浅拷贝：不拷贝子对象的内容，只是拷贝子对象的引用。
深拷贝：会连子对象的内存也全部拷贝一份，对子对象的修改不会影响源对象
我们可以使用内置函数：copy(浅拷贝)、deepcopy(深拷贝)。
'''


# 参数传递：传递可变对象的引用
def test_3():
    b = [10, 20]

    def f2(m):
        print("m:", id(m))  # b 和m 是同一个对象
        m.append(30)  # 由于m 是可变对象，不创建对象拷贝，直接修改这个对象

    f2(b)
    print("b:", id(b))
    print(b)


# 参数传递：传递不可变对象的引用
def test_4():
    a = 100

    def f1(n):
        print("n:", id(n))  # 传递进来的是a 对象的地址
        n = n + 200  # 由于a 是不可变对象，因此创建新的对象n
        print("n:", id(n))  # n 已经变成了新的对象
        print(n)

    f1(a)
    print("a:", id(a))


# 测试浅拷贝和深拷贝
def test_5():
    import copy

    def testCopy():
        '''测试浅拷贝'''
        a = [10, 20, [5, 6]]
        b = copy.copy(a)
        print("a", a)
        print("b", b)
        b.append(30)
        b[2].append(7)
        print("浅拷贝......")
        print("a", a)
        print("b", b)

    def testDeepCopy():
        '''测试深拷贝'''
        a = [10, 20, [5, 6]]
        b = copy.deepcopy(a)
        print("a", a)
        print("b", b)
        b.append(30)
        b[2].append(7)
        print("深拷贝......")
        print("a", a)
        print("b", b)

    testCopy()
    print("*************")
    testDeepCopy()


# 传递不可变对象包含的子对象是可变的情况
def test_6():
    # 传递不可变对象时, 不可变对象里面包含的子对象是可变的。则方法内修改了这个可变对象，源对象也发生了变化。
    a = (10, 20, [5, 6])
    print("a:", id(a))

    def test01(m):
        print("m:", id(m))
        m[2][0] = 888
        print(m)
        print("m:", id(m))

    test01(a)
    print(a)


# ------------------------------------------------------------------------------
def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    make_choice(dict_choice)


if __name__ == "__main__":
    main()
