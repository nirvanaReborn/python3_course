#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://blog.csdn.net/blockchain_lemon/article/details/80891527
# 为“瘦小的”区块链“增肥”
'''
在了解了什么是交易之后，我们需要一个方法，将交易添加到之前的区块链网络节点中（这些节点由普通的电脑组成），
为此，我们将创造一个简单的HTTP服务器，便于交易用户将交易信息上报节点。
一个节点能够接收一个带有交易信息的POST请求来作为请求主体。
这就是为什么交易是JSON格式的原因。我们需要将它们传送到服务器的请求主体中。
'''
import datetime
import json

import requests
from flask import Flask
from flask import request

from function.other_function.blockchain.blockchain_1 import Block

node = Flask(__name__)

# Store the transactions that
# this node has in a list
this_nodes_transactions = []
miner_address = "q3nf394hjg-random-miner-address-34nf3i4nflkn3oi"


# 这样我们就完成了对用户间互相发送SnakeCoin信息的保存。
# 这就是为什么大家将区块链称为公共账本和分布式账本的原因：
# 所有交易存储在网络内的每个节点之中，重要的是，对所有用户是可见的。
@node.route('/txion', methods=['POST'])
def transaction():
    if request.method == 'POST':
        # On each new POST request,
        # we extract the transaction data
        new_txion = request.get_json()

        # Then we add the transaction to our list
        this_nodes_transactions.append(new_txion)

        # Because the transaction was successfully
        # submitted, we log it to our console
        print("New transaction")
        print("FROM:{}".format(new_txion['from']))
        print("TO:{}".format(new_txion['to']))
        print("AMOUNT:{}\n".format(new_txion['amount']))

        # Then we let the client know it worked out
        return "Transaction submissionsuccessful\n"


# ...blockchain
# ...Block class definition
'''
在SnakeCoin中，我们要创建的是一个简单的PoW算法。要创建一个新的区块，矿工的电脑需要增加一个数字。
当该数字可被9（“SnakeCoin”中的字母数）和最后一个区块的证明编号整除时，
一个新的SnakeCoin区块就会被开采出来，矿工也会得到一个SnakeCoin作为奖励
'''


def proof_of_work(last_proof):
    # Create a variable that wewill use to find our next proof of work
    incrementor = last_proof + 1

    # Keep incrementing the incrementor until it's equal to a number divisible by 9
    # and the proof of work of the previous block in the chain
    while not (incrementor % 9 == 0 and incrementor % last_proof == 0):
        incrementor += 1

    # Once that number is found, we can return it as a proof of our work
    return incrementor


@node.route('/mine', methods=['GET'])
def mine(blockchain):
    # Get the last proof of work
    last_block = blockchain[len(blockchain) - 1]
    last_proof = last_block.data['proof-of-work']

    # Find the proof of work for the current block being mined
    # Note: The program will hang here until a new proof of work is found
    proof = proof_of_work(last_proof)

    # Once we find a valid proof of work,
    # we know we can mine a blockso
    # we reward the miner by addinga transaction
    this_nodes_transactions.append(
        {"from": "network", "to": miner_address, "amount": 1}
    )

    # Now we can gather the data needed to create the new block
    new_block_data = {
        "proof-of-work": proof,
        "transactions": list(this_nodes_transactions)
    }

    new_block_index = last_block.index + 1
    new_block_timestamp = this_timestamp = datetime.datetime.now()
    last_block_hash = last_block.hash

    # Empty transaction list
    this_nodes_transactions[:] = []

    # Now create the new block!
    mined_block = Block(
        new_block_index,
        new_block_timestamp,
        new_block_data,
        last_block_hash
    )

    blockchain.append(mined_block)

    # Let the client know we mined a block
    return json.dumps({
        "index": new_block_index,
        "timestamp": str(new_block_timestamp),
        "data": new_block_data,
        "hash": last_block_hash
    }) + "\n"


@node.route('/blocks', methods=['GET'])
def get_blocks(blockchain):
    chain_to_send = blockchain
    # Convert our blocks into dictionaries
    # so we can send them as json objects later
    for block in chain_to_send:
        block_index = str(block.index)
        block_timestamp = str(block.timestamp)
        block_data = str(block.data)
        block_hash = block.hash
        block = {
            "index": block_index,
            "timestamp": block_timestamp,
            "data": block_data,
            "hash": block_hash
        }
    # Send our chain to whomever requested it
    chain_to_send = json.dumps(chain_to_send)
    return chain_to_send


'''
现在，我们可以控制在一个特定时间段内可开采的区块数量，到这里，我们就可以自由掌握一定时间内多少个区块可以被开发出来了。
但是就像前面所说的，这一切操作只是在一个节点上进行的。但区块链应该是去中心的，我们怎么保证这条链在其他节点上也是一样的呢？
我们可以让每个节点对外广播自己链的版本，其他节点既可以接收广播，又可以自己对外广播同样的内容。
此后，每个节点需要去对其他节点所发出的信息进行验证，验证通过后，网络上的所有节点则可以达成一致。这称为一致性算法。

在这个例子中，我们所采用的一致性算法相当简单：如果一个节点的链与其他节点的链不同（即有争议时），
那么网络上最长的链会保留而较短的链将会被删除，如果所有节点都达成一致，那么则进行下一步：
'''


def find_new_chains(peer_nodes):
    # Get the blockchains of every other node
    other_chains = []
    for node_url in peer_nodes:
        # Get their chains using a GETrequest
        block = requests.get(node_url + "/blocks").content
        # Convert the JSON object to aPython dictionary
        block = json.loads(block)
        # Add it to our list
        other_chains.append(block)

    return other_chains


def consensus(blockchain, peer_nodes):
    # Get the blocks from other nodes
    other_chains = find_new_chains(peer_nodes)
    # If our chain isn't longest,
    # then we store the longest chain
    longest_chain = blockchain
    for chain in other_chains:
        if len(longest_chain) < len(chain):
            longest_chain = chain
    # If the longest chain wasn't ours,
    # then we set our chain to the longest
    blockchain = longest_chain


'''
# To be tested on multi-node system
def consensus(blockchain, peer_nodes):
  bool fraud
  # Get the blocks from other nodes
  other_chains = find_new_chains(peer_nodes)
  # Get current blockchain
  current_chain = blockchain
  for chain in other_chains:
    fraud = false
    if(len(current_chain) < len(chain)):
      for i in range(len(chain)):
        if(current_chain[i].hash != chain[i].hash):
            print "Fraudalence Detected"
            fraud = true
        if(fraud):
          break
      if(!fraud):
        current_chain = chain
  blockchain = current_chain
'''


def main():
    node.run()


if __name__ == "__main__":
    main()
