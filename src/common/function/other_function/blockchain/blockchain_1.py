#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://blog.csdn.net/blockchain_lemon/article/details/80891527
# 50行python代码构建一个最小的区块链
'''
区块链：是一种分布式记账技术，使用区块链进行交易的比特币以及其他加密货币的交易记录会被按时间顺序存储，并且是公开存储。
我们可以把区块链看做一个公共数据库，其中新数据存储在一个称为区块的容器中，然后被添加到一条不可更改的链上（也就是区块链），
同时，链上保存着之前的数据记录。这些数据是比特币或其他加密货币之前的交易信息，被分门别类地添加到链上。
在比特币和其他加密货币的情况下，这些数据是一组交易记录，当然，数据可以是任何类型的。

区块链的诞生，引发了以Bitcoin(比特币)和Litecoin(莱特币)为首的加密货币的崛起。它们不是由中央机构发行或管理的。
由于加密货币的去中心化属性,对于那些本来就不信任银行系统的人来说，简直是带来了新世界。
此外，区块链还给分布式计算带来了革新，出现了很多诸如Ethereum(以太坊)这样的新平台，也引入了智能合约的概念。

接下来，我将用不超过50行的 Python 代码创建一个简单的区块链，并给它取了一个名字叫SnakeCoin。以此帮助大家理解区块链。
首先，我们先对区块链进行定义。在区块链中，每个区块上都有一个时间戳，有时还会有一个索引。
在SnakeCoin 中，我们两个都有。同时，为了保证整个区块链的完整性，每一个区块都有一个唯一的哈希值，用于自我标识。
比如比特币，每一个区块的哈希值是由区块的索引、时间戳、数据以及前一个区块的哈希，经过加密后得到的。
其中，数据可以选取任意值。

当然，作为第一个区块（也叫创世区块），自然很特殊。在多数情况下，它是手动地被添加到链上，或者通过独特的逻辑将它添加到链上。

如果希望在控制台中查看更多信息，可以编辑完整的源文件并打印每个块的时间戳或数据。这就是SnakeCoin要提供的所有东西。
为了使SnakeCoin规模达到今天生产区块链的规模，必须添加更多的功能，比如服务器层，以跟踪多台机器上的链变化，以及在给定的时间段内限制添加的块数量的工作算法。
'''
import datetime
import hashlib


class Block:
    def __init__(self, index, timestamp, data, previous_hash):
        self.index = index
        self.timestamp = timestamp
        self.data = data
        self.previous_hash = previous_hash
        self.hash = self.hash_block()

    def hash_block(self):
        sha = hashlib.sha256()
        sha.update((str(self.index) +
                    str(self.timestamp) +
                    str(self.data) +
                    str(self.previous_hash)).encode("utf8"))
        return sha.hexdigest()


# 创建第一个区块
def create_genesis_block():
    #  Manually construct ablock with
    #  index zero andarbitrary previous hash
    return Block(0, datetime.datetime.now(), "GenesisBlock", "0")


'''
我们还需要一个函数来生成链上更多的区块。
该函数将链上前一个区块作为参数，为后面的区块生成数据，并返回具有带有数据的新区块。
当生成的新区块包含了前一个区块的哈希值，区块链的完整性就会随着每个区块的增加而增加这样的操作虽然看起来有点复杂，
但如果不这么做，其他人就会很容易篡改链上的数据，甚至把整条链都给换了。
所以，链上区块的哈希值就充当了密码证明，确保区块一旦被添加到区块链上，就不能被替换或者删除。
'''


def next_block(last_block):
    this_index = last_block.index + 1
    this_timestamp = datetime.datetime.now()
    this_data = "Hey! I'm block " + str(this_index)
    previous_hash = last_block.hash
    return Block(this_index, this_timestamp, this_data, previous_hash)


def create_blockchain(num_of_blocks_to_add=20):
    # Create the blockchain and add the genesis block
    blockchain = [create_genesis_block()]
    previous_block = blockchain[0]

    # Addblocks to the chain
    for i in range(0, num_of_blocks_to_add):
        block_to_add = next_block(previous_block)
        blockchain.append(block_to_add)
        previous_block = block_to_add
        # Telleveryone about it!
        print("Block #{} has been added to theblockchain!".format(block_to_add.index))
        print("Hash:{}\n".format(block_to_add.hash))


def main():
    create_blockchain()


if __name__ == "__main__":
    main()
