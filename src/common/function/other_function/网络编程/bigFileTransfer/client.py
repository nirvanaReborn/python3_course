#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/qq_38784098/article/details/107599498
# 利用tcp协议实现大文件传输


import os
import json
import struct
import socket


def main():
    sk = socket.socket()
    sk.connect(('127.0.0.1', 2000))

    # 文件名\文件大小
    abs_path = r'test.docx'
    filename = os.path.basename(abs_path)
    filesize = os.path.getsize(abs_path)

    dic = {'filename': filename, 'filesize': filesize}
    str_dic = json.dumps(dic)
    b_dic = str_dic.encode("utf-8")
    mlen = struct.pack('i', len(b_dic))
    sk.send(mlen)
    sk.send(b_dic)

    with open(abs_path, mode='rb') as f:
        while filesize > 0:
            content = f.read(1024)
            filesize -= 1024
            sk.send(content)
    sk.close()


if __name__ == "__main__":
    main()
