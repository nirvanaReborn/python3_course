#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/qq_38784098/article/details/107599498
# 利用tcp协议实现大文件传输


import socket
import json
import struct


def main():
    sk = socket.socket()
    sk.bind(('127.0.0.1', 2000))
    sk.listen()

    conn, addr = sk.accept()
    msg_len = conn.recv(4)
    dic_len = struct.unpack("i", msg_len)[0]  # 防止粘包
    msg = conn.recv(dic_len).decode("utf-8")
    msg = json.loads(msg)

    with open(msg['filename'], 'wb') as f:
        while msg['filesize'] > 0:
            content = conn.recv(1024)
            msg['filesize'] -= len(content)  # tcp协议将1024字节分开发送
            f.write(content)

    conn.close()
    sk.close()


if __name__ == "__main__":
    main()
