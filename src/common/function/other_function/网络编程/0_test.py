#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/alwaysrun/article/details/121434991
# 序列化与反序列化之Json与struct
# 在进行数据传递（如网络通讯时），就需要对数据做序列化与反序列化:
# 对于json类型可方便地通过json模块处理；
# 但对于C语言的struct数据，则需要使用struct模块。


def test_1():
    pass


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
