#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.163.com/root_yang/blog/static/26083503120164195542582/

import os
import sys
import threading
import time


class back(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def cmd(self, c):
        c = os.popen(c).read()
        return c

    def run(self):
        while True:
            cmd_ = self.cmd(
                "cat /var/wifi1.txt |grep -|awk -F 'from ' '{print$2}'|sort |uniq ").replace('"', '').split('\n')
            nohup = ''
            self.cmd("echo > /wifi1.txt  ")
            x = 0
            for i in cmd_:
                j = '  "%d":"%s",' % (x, i)

                nohup += j
                x += 1

            nohup = '{%s}' % nohup[:len(nohup) - 1]
            cmd_h = self.cmd(
                "cat /var/lib/dhcp/dhcpd.leases|grep host|awk '{print$2}' ").replace('"', '').split('\n')
            x = 0
            dhcp = ''
            for i in cmd_h:
                j = '  "%d":"%s",' % (x, i)
                dhcp += j
                x += 1
            dhcp = '{%s}' % dhcp[:len(dhcp) - 1]
            open('Host.json', 'w').write(dhcp)
            open('Mac.json', 'w').write(nohup)
            print(dhcp, nohup)
            print("reading ..")
            time.sleep(30)


def get_httpd(port):
    if sys.version_info[0] == 3:
        import http.server
        import socketserver

        Handler = http.server.SimpleHTTPRequestHandler
        httpd = socketserver.TCPServer(("", port), Handler)
    else:
        import SimpleHTTPServer
        import SocketServer

        Handler = SimpleHTTPServer.SimpleHTTPRequestHandler
        httpd = SocketServer.TCPServer(("", port), Handler)

    return httpd


def main():
    back().start()
    port = 8009
    httpd = get_httpd(port)
    print("http://localhost:" + str(port))
    httpd.serve_forever()


if __name__ == "__main__":
    main()
