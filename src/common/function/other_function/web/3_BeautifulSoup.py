#!/usr/bin/env python
# -*- coding:utf-8 -*-


import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
# logging 库获取日志 requests 和 urllib3(requests 依赖) 对象，调低他们的日志级别，
# 设置为 WARNING 则代表只有 WARNING 以上级别的日志才会被输出出来
logging.getLogger("requests").setLevel(logging.WARNING)


def test_1():
    # pip install beautifulsoup4
    def get_links(url):
        import requests
        from bs4 import BeautifulSoup

        result = requests.get(url)
        page = result.text
        doc = BeautifulSoup(page, "lxml")
        links = [element.get('href') for element in doc.find_all('a')]
        return links

    url = "http://boingboing.net"
    for i, link in enumerate(get_links(url), start=1):
        print(i, link)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
