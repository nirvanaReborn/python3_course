#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 《Python语言及其应用(美Lubanovic 2016)》 P194
'''
HTTP 状态码有许多种，可以根据第一个（百位）数字来分成五类。
• 1xx（信息）
    服务器收到了请求，但是需要客户端发送一些额外的信息。
• 2xx（成功）
    请求成功。除了200 以外，其他的状态码还会包含一些特殊含义。
• 3xx（重定向）
    资源位置发生改变，所以响应会返回一个新的URL 给客户端。
• 4xx（客户端错误）
    客户端发生错误，比如最出名的404（页面不存在）。418（我是一个茶壶）是一个愚人节笑话。
• 5xx（服务端错误）
    500 是最常见的错误。你可能也见到过502（网关错误），这表示Web 服务器程序和后端的应用服务器之间无法连接。
'''

import logging

# logging.disable(logging.DEBUG) # 禁用日志
logging.basicConfig(level=logging.DEBUG,
                    # filename='myProgramLog.log',
                    # format='[%(asctime)s] - [%(levelname)s] - %(message)s'
                    format='%(message)s')
# logging 库获取日志 requests 和 urllib3(requests 依赖) 对象，调低他们的日志级别，
# 设置为 WARNING 则代表只有 WARNING 以上级别的日志才会被输出出来
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("urllib").setLevel(logging.WARNING)


# 使用urllib.request
def test_1(url='https://www.baidu.com/'):
    import urllib.request

    conn = urllib.request.urlopen(url)
    print(conn)

    data = conn.read()
    print(data)

    print(conn.status)  # HTTP 状态码

    print(conn.getheader('Content-Type'))
    # text/html
    # text/plain 字符串表示的是一个MIME 类型，它的意思是纯文本。

    for key, value in conn.getheaders():
        print(key, value, sep="----->")


# 使用第三方模块requests可以让Web 客户端开发变得更加简单.
# Requests 是⽤Python语⾔编写，基于urllib，采⽤Apache2 Licensed开源协议的 HTTP 库。
# 它⽐ urllib 更加⽅便，可以节约我们⼤量的⼯作，完全满⾜HTTP测试需求。
# Requests 库是一个阻塞式 HTTP 请求库，当我们发出一个请求后，程序会一直等待服务器的响。
# pip install requests
def test_2(url='https://www.baidu.com/'):
    import requests

    response = requests.get(url)  # 下载网页
    try:
        response.raise_for_status()  # 检查错误
    except Exception as exc:
        print('There was a problem: %s' % (exc))

    response.encoding = "UTF-8"
    print(response)
    print(response.status_code == requests.codes.ok)
    print(response.text)
    playFile = open('RomeoAndJuliet.txt', 'wb')
    # iter_content()方法在循环的每次迭代中，返回一段内容。
    # 每一段都是bytes 数据类型，你需要指定一段包含多少字节。
    # 10 万字节通常是不错的选择，所以将100000作为参数传递给iter_content()。
    for chunk in response.iter_content(100000):
        playFile.write(chunk)
    playFile.close()


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
