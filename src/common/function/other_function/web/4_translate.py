#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6510456812506972680/
# Python Urllib模块 三十行代码编写自己的英语翻译器，随用随翻


def test_1():
    import urllib.request
    import urllib.parse
    import json

    while True:
        content = input('请输入需要翻译的内容（退出输入Q）：')
        if content == 'Q':
            break
        else:
            url = "http://fanyi.youdao.com/translate?smartresult=dict&smartresult=rule&smartresult=ugc&sessionFrom=http://fanyi.youdao.com/"
            data = {}
            data['i'] = content
            data['from'] = 'AUTO'
            data['to'] = 'AUTO'
            data['doctype'] = 'json'
            data['version'] = '2.1'
            data['keyfrom'] = 'fanyi.web'
            data['action'] = 'FY_BY_REALTlME'
            # data['ue'] = 'UTF-8'
            # data['typoResult'] = 'true'
            data = urllib.parse.urlencode(data).encode('utf-8')
            response = urllib.request.urlopen(url, data)
            html = response.read().decode('utf-8')
            target = json.loads(html)
            print('翻译的结果：%s' % target['translateResult'][0][0]['tgt'])


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
