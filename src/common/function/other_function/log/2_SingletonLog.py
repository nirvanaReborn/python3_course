#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.stubbornhuang.com/3022/

import os
from loguru import logger


class Singleton(object):
    def __init__(self, cls):
        self._cls = cls
        self._instance = {}

    def __call__(self):
        if self._cls not in self._instance:
            self._instance[self._cls] = self._cls()
        return self._instance[self._cls]


@Singleton
class LoguruLogger(object):
    def __init__(self):
        current_path = os.path.split(os.path.realpath(__file__))[0]
        self.logger = logger
        self.logger.add(current_path + "/Log/log_{time}.log", enqueue=True, retention="30 days")

    def __call__(self, *args, **kwargs):
        return self.logger

    # @property
    def get_logger(self):
        return self.logger


def main():
    LoguruLogger().get_logger().info('a')


if __name__ == '__main__':
    main()
