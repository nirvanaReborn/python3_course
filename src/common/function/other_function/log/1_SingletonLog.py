#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/weixin_43790276/article/details/101944628
# 实现日志对象单例
# 单例参考：https://blog.csdn.net/weixin_43790276/article/details/101390615
# 线程安全参考：https://blog.csdn.net/weixin_43790276/article/details/91069959
import logging
from logging.handlers import RotatingFileHandler
from threading import Lock


class LoggerProject(object):

    def __init__(self):
        self.mutex = Lock()
        self.formatter = '%(asctime)s -<>- %(filename)s -<>- [line]:%(lineno)d -<>- %(levelname)s -<>- %(message)s'

    def _create_logger(self):
        _logger = logging.getLogger(__name__)
        _logger.setLevel(level=logging.INFO)
        return _logger

    def _file_logger(self):
        size_rotate_file = RotatingFileHandler(filename='size_rotate', maxBytes=1024 * 1024, backupCount=5)
        size_rotate_file.setFormatter(logging.Formatter(self.formatter))
        size_rotate_file.setLevel(logging.INFO)
        return size_rotate_file

    def _console_logger(self):
        console_handler = logging.StreamHandler()
        console_handler.setLevel(level=logging.INFO)
        console_handler.setFormatter(logging.Formatter(self.formatter))
        return console_handler

    def pub_logger(self):
        logger = self._create_logger()
        self.mutex.acquire()
        logger.addHandler(self._file_logger())
        logger.addHandler(self._console_logger())
        self.mutex.release()
        return logger


def main():
    log_pro1 = LoggerProject()
    log_pro2 = LoggerProject()
    logger1 = log_pro1.pub_logger()
    logger2 = log_pro2.pub_logger()
    logger1.info('aaa')
    logger2.info('aaa')
    print('logger1: ', id(logger1))
    print('logger2: ', id(logger2))
    print('log_pro1: ', id(log_pro1))
    print('log_pro2: ', id(log_pro2))


if __name__ == '__main__':
    main()
