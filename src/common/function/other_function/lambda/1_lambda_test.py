#!/usr/bin/env python
# -*- coding: utf-8 -*-

# http://blog.csdn.net/wireless_com/article/details/52713868
# 一行python代码


def test_10():
    # 猜数
    def guess_my_number(n):
        while True:
            # python3中 raw_input() 变成了 input()
            user_input = input("Enter a positive integer to guess: ")
            if len(user_input) == 0 or not user_input.isdigit():
                print("Not a positive integer!")
            else:
                user_input = int(user_input)
                if user_input > n:
                    print("Too big ! Try again!")
                elif user_input < n:
                    print("Too small ! Try again!")
                else:
                    print("You win!")
                    return True

    guess_my_number(42)


# 取一个列表里的随机数
def test_9():
    # print(lambda rnd: rnd.choice([1, 2, 3, 10]))(__import__('random'))
    import random
    print(random.choice([2, 3, 5, 7, 11, 13, 17]))


# 至于字符串排序和快速排序更是手到擒来
def test_8():
    "".join((lambda x: (x.sort(), x)[1])(list('string')))
    qsort = lambda arr: len(arr) > 1 and qsort(
        filter(lambda x: x <= arr[0], arr[1:])) + arr[0:1] + qsort(
        filter(lambda x: x > arr[0], arr[1:])) or arr


# 一行代码实现摄氏度与华氏度之间的转换器
def test_7():
    print((lambda i: i not in [1, 2] and "Invalid input!" or i == 1 and (
        lambda f: f < -459.67 and "Invalid input!" or f)(float(input(
        "Please input a Celsius temperature:")) * 1.8 + 32) or i == 2 and (
                         lambda c: c < -273.15 and "Invalid input!" or c)(
        (float(input("Please input a Fahrenheit temperature:")) - 32) / 1.8))(
        int(input(
            "1,Celsius to Fahrenheit\n2,Fahrenheit to Celsius\nPlease input 1 or 2\n"))))


# 一行代码实现阶乘，而且还带交互
def test_6():
    from functools import reduce
    reduce(lambda x, y: x * y, range(1, input() + 1))


# 一行代码可以输出前100项斐波那契数列的值
def test_5():
    print([x[0] for x in [(a[i][0], a.append((a[i][1], a[i][0] + a[i][1]))) for a in ([[1, 1]],) for i in range(100)]])


# 一行代码计算出1-1000之间的素数
def test_4():
    print(*(i for i in range(2, 1000) if all(tuple(i % j for j in range(2, int(i ** .5))))))


# 一行代码打印九九乘法表
def test_3():
    print('\n'.join([' '.join(['%s*%s=%-2s' % (y, x, x * y) for y in range(1, x + 1)]) for x in range(1, 10)]))


# 让我们用一行代码画一个Mandelbrot图像
def test_2():
    print('\n'.join([''.join(['*' if abs(
        (lambda a: lambda z, c, n: a(a, z, c, n))(lambda s, z, c, n: z if n == 0 else s(s, z * z + c, c, n - 1))(0, 0.02 * x + 0.05j * y,
                                                                                                                 40)) < 2 else ' ' for x in
                              range(-80, 20)]) for y in range(-20, 20)]))


# 在python 里执行它，会输出一个字符拼出的心形
def test_1():
    print('\n'.join([''.join(
        [('AndyLove'[(x - y) % 8] if ((x * 0.05) ** 2 + (y * 0.1) ** 2 - 1) ** 3 - (x * 0.05) ** 2 * (y * 0.1) ** 3 <= 0 else ' ') for x in
         range(-30, 30)]) for y in range(15, -15, -1)]))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
