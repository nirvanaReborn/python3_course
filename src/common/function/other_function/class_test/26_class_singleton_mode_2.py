#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 第二种方法：使用基类
# New 是真正创建实例对象的方法，所以重写基类的new 方法，以此保证创建对象的时候只生成一个实例


class Singleton(object):
    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, '_instance'):
            cls._instance = super(Singleton, cls).__new__(cls, *args, **kwargs)

        return cls._instance


class Foo(Singleton):
    pass


def main():
    foo1 = Foo()
    foo2 = Foo()
    print(foo1 is foo2)  # True


if __name__ == "__main__":
    main()
