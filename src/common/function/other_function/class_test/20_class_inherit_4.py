#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6746755975291601422/
# V形继承

class A:
    def run(self):
        print("a is running")


class B(A):
    pass


class C:
    def run(self):
        print("c is running")


class D(B, C):
    pass


def main():
    D().run()  # a is running
    # 通过D.__mro__去查看继承顺序
    print(D.__mro__)
    # (<class '__main__.D'>, <class '__main__.B'>, <class '__main__.A'>, <class '__main__.C'>, <class 'object'>)


if __name__ == "__main__":
    main()
