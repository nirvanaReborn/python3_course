#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6746755975291601422/
# 菱形继承

class A:
    def run(self):
        print("a is running")


class B(A):
    pass


class C(A):
    def run(self):
        print("c is running")


class D(B, C):
    pass


# 具体原因是 python 的继承会遵循特定的顺序，优先级排在前面的，子类会优先使用。
def main():
    D().run()  # c is running
    # 通过D.__mro__去查看继承顺序
    print(D.__mro__)
    # (<class '__main__.D'>, <class '__main__.B'>, <class '__main__.C'>, <class '__main__.A'>, <class 'object'>)
    # D 排在最前面，如果 D 定义了 run 方法，就会被优先使用，然后是 B, 然后是C， 然后是 A, 最后是 object 基类。


if __name__ == "__main__":
    main()
