#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a7053762740653031940
# Python设计模式：观察者模式(发布订阅模式)
# https://www.cnblogs.com/baxianhua/p/10949842.html
'''
# 观察者模式也叫发布订阅模式，定义了对象之间一对多依赖，
# 当一个对象改变状态时，这个对象的所有依赖者都会收到通知并按照自己的方式进行更新。

1.主要优点如下:
* 降低了目标与观察者之间的耦合关系，两者之间是抽象耦合关系。
* 当需要添加其他对象时，无需对主题和观察者做任何修改
* 可以随时添加和删除观察者
* 目标与观察者之间建立了一套触发机制。

2.主要缺点如下:
* 目标与观察者之间的依赖关系并没有完全解除，而且有可能出现循环引用。
* 通知有时是不可靠的，并导致竞争条件或不一致性。
* 当观察者对象很多时，通知的发布会花费很多时间，影响程序的效率。
'''


# 创建观察者类
class Observer:
    # 初始化具体的成员
    def __init__(self, id, name):
        self.id = id
        self.name = name

    # 向具体的成员发送消息的方法
    def send(self, msg):
        print(str(self.name) + "-" + str(self.id) + " recive the message is: " + msg)


# 创建主题类
class Publisher:
    # 初始化一个主题列表
    def __init__(self):
        self.queues = []

    # 将订阅者添加到队列中
    def add(self, sub):
        if sub not in self.queues:
            self.queues.append(sub)
        else:
            print('failed to add:{}'.format(sub))

    # 从订阅的主题里面移除
    def remove(self, sub):
        try:
            self.queues.remove(sub)
        except ValueError:
            print('failed to remove:{}'.format(sub))

    # 发送通知给相关的主题订阅者
    def notify(self, msg):
        [x.send(msg) for x in self.queues]


if __name__ == '__main__':
    # 实例化具体的Watcher对象，用于去订阅和接收相关信息
    tom = Observer(1001, "tom")
    tony = Observer(1002, "tony")
    jack = Observer(1003, "jack")

    # 实例化Subject对象，定义为添加天气主题
    weather = Publisher()
    weather.add(tom)
    weather.add(tony)

    # 实例化Subject对象，定义为添加军事主题
    military = Publisher()
    military.add(tony)
    military.add(jack)

    # 给订阅者发布天气消息
    weather.notify("it's rain")
    military.notify("it's peace")

    # 将tony从weather and military主题中取消订阅
    weather.remove(tony)
    military.remove(tony)

    # 取消订阅后给剩下的订阅者发布消息
    weather.notify("it's windy")
    military.notify("it's war")
