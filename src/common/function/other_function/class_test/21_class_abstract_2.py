#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/anzhangjun/p/9780463.html
'''
# Python ABC（Abstract Base Class:抽象基类）
# 主要定义了基本类和最基本的抽象方法，可以为子类定义共有的API，不需要具体实现。相当于是Java中的接口或者是抽象类。
# 抽象基类可以不实现具体的方法（当然也可以实现，只不过子类如果想调用抽象基类中定义的方法需要使用super()）,而是将其留给派生类实现。
# 抽象基类提供了逻辑和实现解耦的能力，即在不同的模块中通过抽象基类来调用，可以用最精简的方式展示出代码之间的逻辑关系，让模块之间的依赖清晰简单。
# 同时，一个抽象类可以有多个实现，让系统的运转更加灵活。
# 抽象类实例: 作用统一规范接口，降低使用复杂度。
# 抽象基类的使用：
#     1：直接继承
#         直接继承抽象基类的子类就没有这么灵活，抽象基类中可以声明”抽象方法“和“抽象属性”，
#         只有完全覆写（实现）了抽象基类中的“抽象”内容后，才能被实例化，而虚拟子类则不受此影响。
#     2：虚拟子类
#         将其他的类”注册“到抽象基类下当虚拟子类（调用register方法），
#         虚拟子类的好处是你实现的第三方子类不需要直接继承自基类，可以实现抽象基类中的部分API接口，
#         也可以根本不实现，但是issubclass(), issubinstance()进行判断时仍然返回真值。
#
# Python 对于ABC模块，定义了一个特殊的metaclass：ABCMeta，用于在Python程序中创建抽象基类。
# 抽象基类如果想要声明“抽象方法”，可以使用 @abstractmethod ，
# 抽象基类如果想要声明“抽象属性”，可以使用 @abstractproperty 。
'''

import abc


class Animal(metaclass=abc.ABCMeta):  # 只能被继承，不能实例化，实例化会报错
    @abc.abstractmethod  # 加完这个方法子类必须重写这个方法，否则报错
    def run(self):
        pass

    @abc.abstractmethod
    def eat(self):
        pass


class People(Animal):
    def run(self):
        print("People is walking")

    def eat(self):
        print("people is eating")


class Pig(Animal):
    def run(self):
        print("Pig is running")

    def eat(self):
        print("Pig is eating")


class Dog(Animal):
    def run(self):
        print("Dog is barking")

    def eat(self):
        print("Dog is eating")


def main():
    peo1 = People()
    pig1 = Pig()
    dog1 = Dog()

    peo1.run()
    peo1.eat()


if __name__ == "__main__":
    main()
