#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# __new__是类被创建的时候执行的，注意是类创建，而不是类实例的创建
# 当我们在执行创建类实例的时候执行的是__call__的方法


class test(object):
    # 类被创建的时候执行
    def __new__(cls, *args, **kwargs):
        print('new')
        return super(test, cls).__new__(cls, *args, **kwargs)

    # 构造函数：通常用来初始化实例对象的实例属性
    def __init__(self):
        print('init')

    # 创建类实例的时候执行
    def __call__(self, *args, **kwargs):
        print('call')


def main():
    t = test()
    t()  # new   init   call


if __name__ == '__main__':
    main()
