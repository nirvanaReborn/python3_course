#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6746755975291601422/
# 多重继承

class A:
    def run(self):
        print("a is running")


class B:
    def run(self):
        print("b is running")


class C(A, B):
    pass


# 具体原因是 python 的继承会遵循特定的顺序，优先级排在前面的，子类会优先使用。
def main():
    C().run()  # a is running


if __name__ == "__main__":
    main()
