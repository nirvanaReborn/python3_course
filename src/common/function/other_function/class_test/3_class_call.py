#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://blog.csdn.net/rocky_03/article/details/54891450
# python模块和对象: __call__
# 一个类实例也可以变成一个可调用对象，只需要实现一个特殊方法call()。

# __new__是类被创建的时候执行的，注意是类创建，而不是类实例的创建
# 当我们在执行创建类实例的时候执行的是__call__的方法


class Person(object):
    def __init__(self, name, gender):
        self.name = name
        self.gender = gender

    def __call__(self, friend):
        print('My name is %s...' % self.name)
        print('My friend is %s...' % friend)


def test_1():
    p = Person('Bob', 'male')
    # 单看 p(‘Tim’) 你无法确定 p 是一个函数还是一个类实例，
    # 所以，在Python中，函数也是对象，对象和函数的区别并不显著。
    p('Tim')


def main():
    choice = input("Enter a positive integer to choice: ")
    if choice == "1":
        test_1()
    # elif choice == "2":
    #     test_2()
    # elif choice == "3":
    #     test_3()
    # elif choice == "4":
    #     test_4()
    # elif choice == "5":
    #     test_5()
    # elif choice == "6":
    #     test_6()
    else:
        print("未知命令", choice)


if __name__ == '__main__':
    main()
