#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Python之自动生成代码继承关系
# https://blog.csdn.net/sauphy/article/details/50302007
import os
import re
import webbrowser

gCount = 0


def getList(dirname, pFunc):
    global gCount
    try:
        ls = os.listdir(dirname)
    except:
        print(dirname, 'is access deny')
    else:
        for file in ls:
            temp = os.path.join(dirname, file)
            if (os.path.isdir(temp)):
                getList(temp, pFunc)
            else:
                gCount = gCount + 1
                if re.search(r'.h$', file):
                    pFunc(dirname, file)


def process(dirname, file):
    global gCount
    # print('[%d]->%s' % (gCount, file))
    path = os.path.join(dirname, file)
    list = loadSiteList(path)
    for fileLine in list:
        parseString(fileLine, path)


def loadSiteList(file):
    file = open(file, "r")
    sites = file.readlines()
    return sites


def removeBR(site):
    result = re.search(r'\n', site)
    if result:
        return site.replace('\n', '')
    return site


def getRenderObject(str):
    res = re.search('[^<]?\s*class\s+(\w+)\s*[^\;\w,]', str)
    if res:
        return res.group(1)
    return None


def getParent(str):
    res = re.search(':\s*public\s+(\w+)\s*', str)
    if res:
        return res.group(1)
    return None


objList = []
objInfo = {}
urlList = {}


def parseString(str, path):
    global objList, objInfo
    str = removeBR(str)
    obj = getRenderObject(str)
    if obj != None:
        if obj in objList:
            # print("%s has exist in list" % (obj))
            return
        else:
            objList.append(obj)
            urlList[obj] = path
            objInfo[obj] = getParent(str)
            # print('%s->%s' % (obj, objInfo[obj]))


def link(dest, src):
    return '<a href="%s" target="_blank">%s</a>' % (dest, src)


def HeadLink(dest, src):
    return '<a href="%s" style="color:red;font-weight:bold;" target="_blank">%s</a>' % (dest, src)


def link_form(dest):
    return '-->%s' % (dest)


def getPath(obj):
    global objList, objInfo, urlList

    if obj in objList:
        return urlList[obj]
    else:
        return "#"


def showResult():
    html = ""
    global objList, objInfo, urlList
    for obj in objList:
        tmp = obj
        html += HeadLink(getPath(tmp), tmp)
        while tmp:
            parent = objInfo[tmp]
            html += link_form(link(getPath(parent), parent))
            if parent in objList:
                tmp = parent
            else:
                tmp = None
        html += '<hr>'
    return html


def outputResult(dest_file):
    message = """
    <html>
    <head></head>
    <body>
    <div style="width:1700;border:1px red solid;">
    %s
    </div>
    </body>
    </html>
    """ % (showResult())

    with open(dest_file, 'w') as fw:
        fw.write(message)
    webbrowser.open(dest_file, new=1)


def main():
    # getList(os.path.join(os.getcwd(), "src"), process)
    getList(r"D:\SVN\PBOX\trunk\SourcesEx\FBase2Plugin\fsc_hsoffer_pt\fsc_hsoffer_pt", process)
    dest_file = r"result.html"
    outputResult(dest_file)


if __name__ == "__main__":
    main()
