#!/usr/bin/env python
# -*- coding: UTF-8 -*-

'''
http://blog.csdn.net/rocky_03/article/details/54891450
# 类方法通过装饰器 @classmethod 来定义

要点如下：
1. @classmethod 必须位于方法上面一行
2. 第一个cls 必须有；cls 指的就是“类对象”本身；
3. 调用类方法格式：“类名.类方法名(参数列表)”。参数列表中，不需要也不能给cls 传值。
4. 类方法中访问实例属性和实例方法会导致错误
5. 子类继承父类方法时，传入cls 是子类对象，而非父类对象
'''


class Person:
    # 静态私有成员
    __count = 0  # 类属性

    # 类方法
    @classmethod
    def how_many(cls):  # cls是class的简写，特指类对象
        return cls.__count

    # 初始化方法
    def __init__(self, name, score):
        self.name = name  # 实例属性
        # 私有成员
        self.__score = score  # 实例属性
        Person.__count = Person.__count + 1

    # 返回私有成员的方法
    def get_score(self):  # 实例方法
        return self.__score


def test_1():
    # 类的属性
    print(Person.how_many())
    p1 = Person('Bart', 90)
    p1 = Person('Lisa', 95)
    p1 = Person('Maggie', 96)
    print(Person.how_many())
    print(p1.get_score())
    print(p1.get_score())
    print(p1.get_score())


def main():
    choice = input("Enter a positive integer to choice: ")
    if choice == "1":
        test_1()
    # elif choice == "2":
    #     test_2()
    # elif choice == "3":
    #     test_3()
    # elif choice == "4":
    #     test_4()
    # elif choice == "5":
    #     test_5()
    # elif choice == "6":
    #     test_6()
    else:
        print("未知命令", choice)


if __name__ == '__main__':
    main()
