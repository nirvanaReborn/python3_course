#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.jianshu.com/p/06c960020322
#


import abc


class Base(object, metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def func_a(self, data):
        '''
        :param data:
        :return:
        '''

    @abc.abstractmethod
    def func_b(self, data, out):
        '''
        :param data:
        :param out:
        :return:
        '''

    def func_d(self):
        print('func_d in base')


class Sub(Base):

    def func_a(self, data):
        print('over write func_a', data)

    # 如果还想调用虚类的方法用super
    def func_b(self, data, out):
        super(Sub, self).func_b(data, out)
        print('over write func_b')


class Register(object):
    def func_c(self):
        print('func_c in third class')

    def func_a(self, data):
        print('func_a in third class', data)


'''isinstance() 函数来判断一个对象是否是一个已知的类型，类似 type()。
isinstance() 与 type() 区别：
1.type() 不会认为子类是一种父类类型，不考虑继承关系。
2.isinstance() 会认为子类是一种父类类型，考虑继承关系。
如果要判断两个类型是否相同推荐使用 isinstance()。

issubclass(class, baseClass)用于判断参数 class 是否是类型参数 baseClass 的子类。
'''


def main():
    Base.register(Register)
    print(isinstance(Register, Base))  # False
    print(issubclass(Register, Base))  # True


if __name__ == "__main__":
    main()
