#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://blog.csdn.net/qq_41359051/article/details/86764867
# 利用Sized可以判断一个对象里是否存在__len__方法，即：可否对这个对象使用len()函数。

from collections.abc import Sized


class A(object):
    def __len__(self):
        pass


def main():
    a = A()
    print("存在__len__方法" if isinstance(a, Sized) else "没有__len__方法")
    # 输出：存在__len__方法


if __name__ == "__main__":
    main()
