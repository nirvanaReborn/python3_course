#!/usr/bin/env python
# -*- coding: UTF-8 -*-

'''
http://blog.csdn.net/berguiliu/article/details/9427577
Python中__init__的方法解惑

__init__类似的方法有一点特殊，所有的名字的开头和结尾都有两个下滑线，这些方法都是特殊的方法。
当有一个类的实例被创建时，__init__()方法会自动执行，在类实例创建完毕后执行，类似构造函数。
__init__()可以被当成构造函数，他并不创建实例--它仅仅是你的对象创建后执行的第一个方法。
它的目的是执行一些对象的必要的初始化工作。通过创建自己的__init__()方法，
你可以覆盖默认的__init__()方法(默认的方法什么也不做)，从而能够修饰刚刚创建的对象。

什么是self ?它是类实例自身的引用，其他语言通常使用一个名为this的表示符，在self中自己可以任意添加任意类型的数据成员。
self在Python里不是关键字。self代表当前对象的地址。self能避免非限定调用造成的全局变量。

我们完全可以把__init__()方法比成类的构造函数，将self看成是类中的this指针，时刻作为类的指针，指向这个类的起始地址。

http://blog.csdn.net/JIESA/article/details/50525309
'''


class Student:
    def __init__(self):
        # print 'this is self id = %,in __init__' % (id(self));
        print("Hello in Constructor")

    def ShowSomething(self):
        print('this is self id = %d' % (id(self)))


if __name__ == '__main__':
    print('Hello Python in Main Function!')

    p = Student()
    print("The object address is %d" % id(p))
    p.ShowSomething()

'''
Hello Python in Main Function!
Hello in Constructor
The object address is 1703341506632
this is self id = 1703341506632
'''
