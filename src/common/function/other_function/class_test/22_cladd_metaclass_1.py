#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://blog.csdn.net/m0_37519490/article/details/80825934
# python3 metaclass--创建类的过程分析
# metaclass是指定类由谁创建。能够定制类的创建过程
'''
我们写下如下代码时：
class Foo(object):
    pass

实际上，解释器将其解释为：
Foo = type('Foo', (object,), {})
type()的三个参数：'Foo':类名； (object, ): 类的继承关系，用元组表示； {}： 类的字段，方法。
type也是类，它可以创建类，因此我们叫它元类

以上是类的默认创建方法,由type创建。
python也给我们提供了自定义类的创建的方法，即metaclass。
定制类生成的好处：1.单例模式便可以用 metaclass来实现；2.其他的好处，如ORM。
'''


# 方式一：自定义类的创建过程，那就得写一个像type一样可以创建类的类，那简单，继承就可以办到。
class MyType(type):
    def __new__(cls, *args, **kwargs):
        print('MyType __new__')
        return super().__new__(cls, *args, **kwargs)

    def __init__(cls, *args, **kwargs):
        print('MyTpye __init__')
        super().__init__(*args, **kwargs)

    def __call__(cls, *args, **kwargs):
        print('MyTpye __call__')
        super().__call__(cls, *args, **kwargs)


class Foo(metaclass=MyType):
    pass


# 这样，解释器解释到class Foo(...)的时候，就会转换为：Foo = MyType('Foo', (object,), {})
# ---------------------------------------------------------------------------------------------------

class MyType(type):
    def __new__(cls, *args, **kwargs):
        print('MyType __new__')
        return super().__new__(cls, *args, **kwargs)

    def __init__(cls, *args, **kwargs):
        print('MyTpye __init__')
        super().__init__(*args, **kwargs)

    def __call__(cls, *args, **kwargs):
        print('MyTpye __call__')
        super().__call__(cls, *args, **kwargs)


def with_meta(meta, Base):
    return meta('Foo', (Base,), {})


# 这样解释的时候，与方式一的一样。
class Foo2(with_meta(MyType, object)):
    pass


# 解释器解释到class的定义语句时，会先在class中寻找是否指定自定义的'MyType',
# 没有再往父类找是否指定，
# 没有再在本模块中找，是否本模块指定了统一的'MyType'，
# 若均没有，则用默认的type创建。
# ---------------------------------------------------------------------------------------------------
# 一般我们可以用重写__new__来实现单例
class Singleton(object):
    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, '_instance'):
            cls._instance = super(Singleton, cls).__new__(cls, *args, **kwargs)
        return cls._instance


# 下面再用元类来实现一个单例模式
# __new__是类被创建的时候执行的，注意是类创建，而不是类实例的创建
# 当我们在执行创建类实例的时候执行的是__call__的方法
class MetaSingleton(type):
    def __init__(self, *args, **kwargs):
        self._instance = None
        super(MetaSingleton, self).__init__(*args, **kwargs)

    def __call__(self, *args, **kwargs):
        if self._instance is None:
            self._instance = super(MetaSingleton, self).__call__(*args, *kwargs)
        return self._instance


class Singleton1(object, metaclass=MetaSingleton):
    pass


def main():
    s1 = Singleton()
    s2 = Singleton()
    print('singleton', s1 is s2)

    s1 = Singleton1()
    s2 = Singleton1()
    print('singleton1', s1 is s2)


if __name__ == "__main__":
    main()
