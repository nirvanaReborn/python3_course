#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Python的abc提供了@abstractmethod 装饰器实现抽象方法

from abc import ABC, abstractmethod


class Connect(ABC):
    @abstractmethod
    def __init__(self, host: str, port: int, username: str, password: str, lc_type=None):
        pass

    @abstractmethod
    def __enter__(self):
        pass

    @abstractmethod
    def __exit__(self, exc_type, exc_val, exc_tb):
        """
            Do not handle exceptions
        """
        pass

    @abstractmethod
    def put(self, localpath: str, remotepath: str, progress_bar: bool = False):
        """
            Transfer local files to the remote
        :param str localpath: the local file to copy
        :param str remotepath: the destination path on the SFTP server. Note
        :param str progress_bar: whether show progress bar
            that the filename should be included. Only specifying a directory
            may result in an error.
        """
        pass

    @abstractmethod
    def get(self, remotepath: str, localpath: str, progress_bar: bool = False):
        """
            Transfer remote files to locale
        :param str remotepath: the remote file to copy
        :param str localpath: the destination path on the local host
        :param str progress_bar: whether show progress bar
        """
        pass

    @abstractmethod
    def command(self, command: str):
        """
            Execute command on server
        :param command: the command
        :return: result of command
        :rtype: list
        """
        pass

    @abstractmethod
    def _show_progress_bar(self, transferred_bytes: int, total_bytes: int):
        """
            Show progress bar
        :param transferred_bytes: the bytes transferred so far
        :param total_bytes: the total bytes to be transferred
        """
        pass

    @abstractmethod
    def close(self):
        """
            close connection
        """
        pass


def main():
    pass


if __name__ == "__main__":
    main()
