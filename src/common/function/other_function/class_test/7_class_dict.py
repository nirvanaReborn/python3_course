#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
获取类或对象中的所有成员
注意：类的普通字段属于对象；类中的静态字段和方法等属于类
"""


class Foo:
    age = 10  # 静态字段
    __city = "ZhengZhou"  # 私有静态字段

    def __init__(self, name):
        self.name = name  # 普通字段

    def fun1(self):
        pass

    def __fun2(self):
        pass


def main():
    # 获取类的成员，即：静态字段、方法、和类自带的方法
    print(Foo.__dict__)

    # 获取对象的成员，即：对象的成员普通字段
    obj = Foo(name="zhangsan")
    print(obj.__dict__)  # {'name': 'zhangsan'}


if __name__ == "__main__":
    main()
