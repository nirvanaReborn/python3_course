#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 第三种方法：元类
# 元类是用于创建类对象的类，类对象创建实例对象时一定要调用call方法，
# 因此在调用call时候保证始终只创建一个实例即可，type是python的元类
class Singleton(type):
    def __call__(cls, *args, **kwargs):
        if not hasattr(cls, '_instance'):
            cls._instance = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instance


# Python2
# class Foo(object):
#     __metaclass__ = Singleton


# Python3
class Foo(metaclass=Singleton):
    pass


def main():
    foo1 = Foo()
    foo2 = Foo()
    print(foo1 is foo2)  # True


if __name__ == "__main__":
    main()
