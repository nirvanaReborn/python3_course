#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
__repr__ 目的是为了表示清楚，是为开发者准备的。
__str__  目的是可读性好，是为使用者准备的。

__repr__对于__str__来说相当于是个备胎,
如果有__str__方法,那么,print %s str这些方法都先去执行__str__的方法,并且都先返回__str__的值.
如果没有__str__方法,那么,所有的print %s str,repr(obj)和%r这些方法都会执行__repr__.

在子类中使用__str__,先找子类的__str_ _,
没有的话要向上找,只要父类不是object,就执行父类的__str__
但是如果除了object之外的父类都没有__str__方法,就执行子类的__repr__方法,
如果子类也没有,还要向上继续找父类中的__repr__方法.
一直找不到 再执行object类中的__str__方法
'''


class A:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "**%s**" % self.name

    def __repr__(self):
        return self.name


class B(A):
    def __init__(self, name):
        self.name = name


a = B("asdf")
print(a)  # 结果:**asdf**
print(str(a), repr(a))  # 结果:**asdf** asdf
print("%s|%r" % (a, a))  # 结果:**asdf**| asdf


# ----------------------------------------------------------------------

class A:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "**%s**" % self.name

    def __repr__(self):
        return self.name


class B(A):
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "******"


a = B("asdf")
print(a)  # 结果:******
print(str(a), repr(a))  # 结果:****** asdf
print("%s|%r" % (a, a))  # 结果:******|asdf


# 用自己的str方法,用父类的repr方法
# ----------------------------------------------------------------------

class A:
    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "**%s**" % self.name

    def __repr__(self):
        return self.name


class B(A):
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "******"


a = B("asdf")
print(a)  # 结果:**asdf**
print(str(a), repr(a))  # 结果:**asdf** ******
print("%s|%r" % (a, a))  # 结果:**asdf**|******


#  用父类的str,用自己的repr
# ----------------------------------------------------------------------
def main():
    pass


if __name__ == "__main__":
    main()
