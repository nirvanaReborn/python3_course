#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
http://blog.csdn.net/rocky_03/article/details/54891450
python模块和对象: 初始化函数

# 如果我们不定义__init__方法，系统会提供一个默认的__init__方法。如果我们定义了带参的__init__方法，系统不创建默认的__init__方法。
# 1. Python 中的self 相当于C++中的self 指针，JAVA 和C#中的this 关键字。
# 2. Python 中，self 必须为构造函数的第一个参数，名字可以任意修改。但一般遵守惯例，都叫做self。


'''


class Person(object):
    # 初始化函数，类似于构造函数，**kw表示一个dict(key,value)
    # 注意__init__(self,**kw)中的self相当于java的this
    def __init__(self, name, gender, birth, **kw):
        self.name = name
        self.gender = gender
        self.birth = birth
        self.__dict__.update(kw)


def test_1():
    # 初始化函数
    xiaoming = Person('Xiao Ming', 'Male', '1990-1-1', job='Student')
    print(xiaoming.name)
    print(xiaoming.job)


def main():
    choice = input("Enter a positive integer to choice: ")
    if choice == "1":
        test_1()
    # elif choice == "2":
    #     test_2()
    # elif choice == "3":
    #     test_3()
    # elif choice == "4":
    #     test_4()
    # elif choice == "5":
    #     test_5()
    # elif choice == "6":
    #     test_6()
    else:
        print("未知命令", choice)


if __name__ == "__main__":
    main()
