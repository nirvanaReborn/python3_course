#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 对象的浅拷贝和深拷贝
'''
·变量的赋值操作
    只是形成两个变量，实际还是指向同一个对象。
·浅拷贝
    Python 拷贝一般都是浅拷贝。拷贝时，对象包含的子对象内容不拷贝。因此，源对象和拷贝对象会引用同一个子对象。
·深拷贝
    使用copy 模块的deepcopy 函数，递归拷贝对象中包含的子对象。源对象和拷贝对象所有的子对象也不同。


·组合:
    “is-a”关系，我们可以使用“继承”。从而实现子类拥有的父类的方法和属性。
    “is-a”关系指的是类似这样的关系：狗是动物，dog is animal。狗类就应该继承动物类。

    “has-a”关系，我们可以使用“组合”，也能实现一个类拥有另一个类的方法和属性。
    ”has-a”关系指的是这样的关系：手机拥有CPU。MobilePhone has a CPU。
'''

# 测试对象的引用赋值、浅拷贝、深拷贝
import copy


class MobilePhone:
    def __init__(self, cpu, screen):
        self.cpu = cpu
        self.screen = screen


class CPU:
    def calculate(self):
        print("计算，算个12345")
        print("CPU 对象:", self)


class Screen:
    def show(self):
        print("显示一个好看的画面，亮瞎你的钛合金大眼")
        print("屏幕对象：", self)


def main():
    c = CPU()
    s = Screen()
    m = MobilePhone(c, s)
    m.cpu.calculate()  # 通过组合，我们也能调用cpu 对象的方法。相当于手机对象间接拥有了“cpu 的方法”
    m.screen.show()

    n = m  # 两个变量，但是指向了同一个对象
    print(m, n)
    m2 = copy.copy(m)  # m2 是新拷贝的另一个手机对象
    print(m, m2)
    m.cpu.calculate()
    m2.cpu.calculate()  # m2 和m 拥有了一样的cpu 对象和screen 对象
    m3 = copy.deepcopy(m)
    m3.cpu.calculate()  # m3 和m 拥有不一样的cpu 对象和screen对象


if __name__ == "__main__":
    main()
