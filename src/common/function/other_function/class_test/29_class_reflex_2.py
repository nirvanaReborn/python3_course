#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a7067147613506028062
# Python反射介绍——动态调用


# 比如，一个类中有很多方法，它们提供不同的服务，
# 通过输入的参数来判断执行某个方法，一般的使用如下写法：
class MyService():
    def service1(self):
        print("service1")

    def service2(self):
        print("service2")

    def service3(self):
        print("service3")


def test_1():
    Ser = MyService()
    s = input("请输入您想要的服务: ").strip()
    if s == "service1":
        Ser.service1()
    elif s == "service2":
        Ser.service2()
    elif s == "service3":
        Ser.service3()
    else:
        print("error!")


# 如果函数比较少这样写没有太大问题，如果有很多，
# 这样写就比较复杂了，需要写大量else语句，可以使用反射机制来写：
# 这样是不是简洁了很多，上面的例子中，通过反射，将字符串变成了函数，实现了对对象方法的动态调用。
def test_2():
    Ser = MyService()
    s = input("请输入您想要的服务: ").strip()
    if hasattr(Ser, s):
        func = getattr(Ser, s)
        func()
    else:
        print("error!")


def main():
    from public_function import dict_choice

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
