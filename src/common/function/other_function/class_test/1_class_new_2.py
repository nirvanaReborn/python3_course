#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# __new__是类被创建的时候执行的，注意是类创建，而不是类实例的创建
# 当我们在执行创建类实例的时候执行的是__call__的方法


class A:
    def __init__(self, num):
        print("A Init")

    def fun(self):
        print("A Fun Exec")


class B:
    def __init__(self, num):
        print("B Init")

    def __new__(cls, num):
        if num > 10:
            return super(B, cls).__new__(cls)
        return A(num)

    def fun(self):
        print("B Fun Exec")


def main():
    obj = B(10)  # A Init, # __new__ 在实例化之前决定拿哪个类进行实例化
    obj.fun()  # A Fun Exec
    obj = B(20)  # B Init
    obj.fun()  # B Fun Exec


if __name__ == '__main__':
    main()
