#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# isinstance() 函数来判断一个对象是否是一个已知的类型，类似 type()。区别如下：
# type() 不会认为子类是一种父类类型，不考虑继承关系。
# isinstance() 会认为子类是一种父类类型，考虑继承关系。

class A:
    pass


class B(A):
    pass


def main():
    print(isinstance(A(), A))  # returns True
    print(type(A()) == A)      # returns True
    print(isinstance(B(), A))  # returns True
    print(type(B()) == A)      # returns False


if __name__ == "__main__":
    main()
