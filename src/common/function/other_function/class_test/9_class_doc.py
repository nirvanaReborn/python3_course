#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
表示类的描述信息
"""


class Foo:
    """
    类的描述信息    
    """

    def __init__(self):
        pass


def main():
    obj = Foo()
    print(obj.__doc__)  # 类的描述信息


if __name__ == '__main__':
    main()
