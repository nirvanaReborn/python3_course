#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 打印一个实例化对象时，打印的其实是一个对象的地址。
# 而通过__str__()函数就可以帮助我们打印对象中具体的属性值，或者你想得到的东西。
# 当使用print输出对象的时候，只要自己定义了__str__(self)方法，那么就会打印从在这个方法中return的数据


class Person:  # 默认继承object类

    def __init__(self, name):
        self.name = name

    def __str__(self):
        return "名字是：{0}".format(self.name)


def main():
    p = Person("高淇")
    print(p)


if __name__ == "__main__":
    main()
