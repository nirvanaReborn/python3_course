#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://blog.csdn.net/nasonyehfm/article/details/90167811
#

from collections import abc


# 当然直接继承abc.Mapping 也可以，毕竟MutableMapping是其子类
class MyDic(abc.MutableMapping):
    def __init__(self):
        pass

    def __setitem__(self, key, value):
        print('key: %s  val: %s' % (key, value))

    def __delitem__(self, key):
        print('key: %s ' % key)

    def __getitem__(self, item):
        print('item: %s ' % str(item))

    def __iter__(self):
        pass

    def __len__(self):
        pass


def main():
    # dict是Python中典型的数据结构，其接口的定义形式也来自abc.Mapping和abc.MutableMapping这两种抽象类
    mydic = dict()
    print(issubclass(dict, abc.MutableMapping))  # True
    print(issubclass(dict, abc.Mapping))  # True
    print(isinstance(mydic, abc.MutableMapping))  # True
    print(isinstance(mydic, abc.Mapping))  # True


if __name__ == "__main__":
    main()
