#!/usr/bin/env python
# -*- coding: UTF-8 -*-

'''
# http://blog.csdn.net/rocky_03/article/details/54891450
# python模块和对象: @property 可以将一个“方法调用”变成“属性调用”。
# 注意: @property 修饰的属性，如果没有加setter 方法，则为只读属性。
'''


# 第一个score(self)是get方法，用@property装饰，
# 第二个score(self, score)是set方法，用@score.setter装饰，@score.setter是前一个@property装饰后的副产品
class Student:
    def __init__(self, name, score):
        self.name = name
        self.__score = score

    @property
    def score(self):
        return self.__score

    @score.setter
    def score(self, score):
        if score < 0 or score > 100:
            raise ValueError('invalid score')
        self.__score = score


def test_1():
    s = Student('Bob', 59)
    s.score = 60
    print(s.score)
    # s.score = 600


def main():
    choice = input("Enter a positive integer to choice: ")
    if choice == "1":
        test_1()
    # elif choice == "2":
    #     test_2()
    # elif choice == "3":
    #     test_3()
    # elif choice == "4":
    #     test_4()
    # elif choice == "5":
    #     test_5()
    # elif choice == "6":
    #     test_6()
    else:
        print("未知命令", choice)


if __name__ == '__main__':
    main()
