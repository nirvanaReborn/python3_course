#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 单例模式
'''
单例模式（Singleton Pattern）的核心作用是确保一个类只有一个实例，并且提供一个访问该实例的全局访问点。
单例模式只生成一个实例对象，减少了对系统资源的开销。
当一个对象的产生需要比较多的资源，如读取配置文件、产生其他依赖对象时，可以产生一个“单例对象”，然后永久驻留内存中，从而极大的降低开销。
单例模式有多种实现的方式，我们这里推荐重写__new__()的方法。
主要用于数据池、设备管理器等

单例模式应用的场景一般发现在以下条件下：
资源共享的情况下，避免由于资源操作时导致的性能或损耗等，如日志文件，应用配置。
控制资源的情况下，方便资源之间的互相通信。如线程池等。
1.网站的计数器
2.应用配置
3.多线程池
4.数据库配置 数据库连接池
5.应用程序的日志应用
'''


class MySingleton(object):
    __instance = None
    __init_flag = True

    def __new__(cls, *args, **kwargs):
        if cls.__instance == None:
            cls.__instance = object.__new__(cls)
        return cls.__instance

    def __init__(self, name):
        if MySingleton.__init_flag:
            print("init....")
            self.name = name
            MySingleton.__init_flag = False


def main():
    a = MySingleton("aa")
    print(a)
    b = MySingleton("bb")
    print(b)


if __name__ == "__main__":
    main()
