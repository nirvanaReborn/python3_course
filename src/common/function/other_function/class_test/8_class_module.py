#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# 加载其他模块
from function.other_function.class_test.example_module import example_class


def main():
    """
    返回对象类型所属的模块
    """
    obj = example_class
    print(obj.__module__)  # function.other_function.class_test.example_module


if __name__ == '__main__':
    main()
