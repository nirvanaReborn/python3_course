#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 在子类中，如果想要获得父类的方法时，我们可以通过super()来做。
# super()代表父类的定义，不是父类对象。


class A:
    logLevelMap = {
        0: "调试",
        1: "信息",
        2: "告警",
        3: "错误",
        4: "重要",
        5: "致命",
    }

    def say(self):
        print("A: ", self)
        print("say AAA")


class B(A):
    def say(self):
        # A.say(self) 调用父类的say()方法
        super().say()  # 或者通过super()调用父类的方法
        print("say BBB")
        print(super().logLevelMap.get(2, 0))


def main():
    b = B()
    b.say()
    '''
    A:  <__main__.B object at 0x000001BB7FD5C780>
    say AAA
    say BBB
    '''


if __name__ == "__main__":
    main()
