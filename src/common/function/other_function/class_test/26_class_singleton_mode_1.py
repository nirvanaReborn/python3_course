#!/usr/bin/env python
# -*- coding:utf-8 -*-

# python如何实现单例模式?
# 第一种方法:使用装饰器


def singleton_func(cls):
    instances = {}
    def wrapper(*args, **kwargs):
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]
    return wrapper


@singleton_func
class Foo(object):
    pass


def main():
    foo1 = Foo()
    foo2 = Foo()
    print(foo1 is foo2)  # True


if __name__ == "__main__":
    main()
