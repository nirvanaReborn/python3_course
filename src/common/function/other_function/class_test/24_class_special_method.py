#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 特殊方法和运算符重载
'''
常见的特殊方法统计如下：
方法                说明            例子
__init__            构造方法        对象创建：p = Person()
__del__             析构方法        对象回收
__repr__,__str__    打印，转换       print(a)
__call__            函数调用        a()
__getattr__         点号运算        a.xxx
__setattr__         属性赋值        a.xxx = value
__getitem__         索引运算        a[key]
__setitem__         索引赋值        a[key]=value
__len__             长度            len(a)


每个运算符实际上都对应了相应的方法，统计如下：
运算符         特殊方法                                     说明
运算符+        __add__                                      加法
运算符-        __sub__                                      减法
<,<=,==        __lt__,__le__，__eq__                        比较运算符
>,>=,!=        __gt__,__ge__,__ne__                         比较运算符
|,^,&          __or__,__xor__,__and__                       或、异或、与
<<,>>          __lshift__,__rshift__                        左移、右移
*,/,%,//       __mul__,__truediv__,__mod__,__floordiv__     乘、浮点除、模运算(取余)、整数除
**             __pow__                                      指数运算
'''


# Python 的运算符实际上是通过调用对象的特殊方法实现的。
def test_1():
    a = 20
    b = 30
    c = a + b
    d = a.__add__(b)
    print("c=", c)
    print("d=", d)


# --------------------------------测试运算符的重载--------------------------------
class Person:
    def __init__(self, name):
        self.name = name

    def __add__(self, other):
        if isinstance(other, Person):
            return "{0}--{1}".format(self.name, other.name)
        else:
            return "不是同类对象，不能相加"

    def __mul__(self, other):
        if isinstance(other, int):
            return self.name * other
        else:
            return "不是同类对象，不能相乘"


def main():
    p1 = Person("高淇")
    p2 = Person("高希希")
    x = p1 + p2
    print(x)
    print(p1 * 3)


if __name__ == "__main__":
    main()
