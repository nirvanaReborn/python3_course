#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a7067147613506028062
# Python反射介绍
'''
反射机制是面向对象编程语言中比较重要的功能，可以动态获取对象信息以及动态调用对象。
反射的概念是由Smith在1982年首次提出的，主要是指程序可以访问、检测和修改它本身状态或行为的一种能力。

在程序运行时可以获取对象类型定义信息，例如，Python中的type(obj)将返回obj对象的类型，
这种获取对象的type、attribute或者method的能力称为反射。
通过反射机制，可以用来检查对象里的某个方法，或某个变量是否存在。
也就是可以通过字符串映射对象的方法或者属性。

Python反射常用的内置函数：
    type(obj)：返回对象类型
    isinstance(object, classinfo)：判断一个对象是否是一个已知的类型，类似 type()
    callable(obj)：对象是否可以被调用
    dir([obj])：返回obj属性列表
    getattr(obj, attr)：返回对象属性值
    hasattr(obj, attr)：判断某个函数或者变量是否存在
    setattr(obj, attr, val)：给模块添加属性（函数或者变量）
    delattr(obj, attr)：删除模块中某个变量或者函数

下面介绍两种Python反射的应用场景:动态调用、动态属性设置
'''


class Person():
    def __new__(cls, *args, **kwargs):
        print("__new__!!!")
        return object.__new__(cls)

    def __init__(self, x, y):
        print("__init__!!!")
        self.age = x
        self.height = y

    def __call__(self, *args, **kwargs):
        print("__call__!!!")

    def __str__(self):
        print("__str__!!!")
        return "<%s:%s, %s>" % (self.__class__.__name__, self.age, self.height)  # __class__获取对象的类，__name__取得类名

    def talk(self):
        print(f"My age is {self.age} and height is {self.height}")


def main():
    p = Person(20, 180)  # 自动执行 __new__ 和 __init__ 方法
    # print(p)  # 自动执行 __str__ 方法,否则打印<__main__.Person object at 0x0000021E75C895C0>
    # p() # 自动执行 __call__ 方法
    # 类的静态函数、类函数、普通函数、全局变量以及一些内置的属性都是放在类__dict__里的
    print(Person.__dict__)
    # 对象的__dict__中存储了一些self.xxx的一些东西
    print(p.__dict__)
    p.__dict__['age'] = 22
    print(p.__dict__)
    p.weight = 60
    print(p.__dict__)
    # 返回参数p的属性、方法列表
    print(dir(p))
    '''
    在实例创建之前调用__new__方法，返回值（实例）将传递给__init__方法的第一个参数。
    实例化对象时会自动执行 __init__ 方法
    打印一个对象时，会自动执行__str__ 方法
    调用实例化对象时，会自动触发__call__ 方法
    通过dir()方法可以打印出了对象p的属性。
    '''

    if (callable(p)):
        print("p is callable")
    else:
        print("p is not callable")

    print(isinstance(p, Person))
    print(type(p) == Person)
    print(isinstance(p.age, int))
    print(type(p.age) == int)

    print(hasattr(p, "talk"))
    print(hasattr(p.talk, "__call__"))

    print(getattr(p, "talk"))
    print(getattr(p.talk, "__call__"))

    if hasattr(p, 'walk'):
        print(getattr(p, 'walk'))
    else:
        print("I can't walk")
    print(getattr(p, "walk", None))  # 如果没有walk属性就返回None

    setattr(p,'walk','ON')
    if hasattr(p,'walk'):
        print(getattr(p,'walk'))
    else:
        print("I can't walk")
    print(p.__dict__)

    delattr(p,'walk')
    if hasattr(p,'walk'):
        print(getattr(p,'walk'))
    else:
        print("I can't walk")
    print(p.__dict__)




if __name__ == "__main__":
    main()
