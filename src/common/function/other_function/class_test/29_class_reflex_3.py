#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a7067147613506028062
# Python反射介绍——动态属性设置

from scapy.all import *
'''
>>> ls(IP)
version    : BitField  (4 bits)                  = ('4')
ihl        : BitField  (4 bits)                  = ('None')
tos        : XByteField                          = ('0')
len        : ShortField                          = ('None')
id         : ShortField                          = ('1')
flags      : FlagsField                          = ('<Flag 0 ()>')
frag       : BitField  (13 bits)                 = ('0')
ttl        : ByteField                           = ('64')
proto      : ByteEnumField                       = ('0')
chksum     : XShortField                         = ('None')
src        : SourceIPField                       = ('None')
dst        : DestIPField                         = ('None')
options    : PacketListField                     = ('[]'
'''


def main():
    fields = {"version": 4, "src": "192.168.0.1", "dst": "192.168.10.1"}
    ip = IP()
    print(ip.__dict__)
    for key, value in fields.items():
        setattr(ip, key, value)
    print(ip.__dict__)


if __name__ == "__main__":
    main()
