#!/usr/bin/env python
# -*- coding: UTF-8 -*-

'''
http://blog.csdn.net/rocky_03/article/details/54891450
python模块和对象: __slots__

由于Python是动态语言，任何实例在运行期都可以动态地添加属性。 
如果要限制添加的属性，例如，Student类只允许添加 name、gender和score 这3个属性，就可以利用Python的一个特殊的__slots__来实现。
__slots__的目的是限制当前类所能拥有的属性，如果不需要添加任意动态的属性，使用__slots__也能节省内存。
'''


# 顾名思义，_slots_是指一个类允许的属性列表：
class Student(object):
    __slots__ = ('name', 'gender', 'score')

    def __init__(self, name, gender, score):
        self.name = name
        self.gender = gender
        self.score = score


def test_1():
    s = Student('Bob', 'male', 59)
    s.name = 'Tim'  # OK
    s.score = 99  # OK
    s.grade = 'A'  # AttributeError: 'Student' object has no attribute 'grade'


def main():
    choice = input("Enter a positive integer to choice: ")
    if choice == "1":
        test_1()
    # elif choice == "2":
    #     test_2()
    # elif choice == "3":
    #     test_3()
    # elif choice == "4":
    #     test_4()
    # elif choice == "5":
    #     test_5()
    # elif choice == "6":
    #     test_6()
    else:
        print("未知命令", choice)


if __name__ == '__main__':
    main()
