#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# 未定义的方法被调用时，会调用 __getattr__ 方法


class Foo:
    def fun1(self):
        print("Fun1 Exec")

    def fun3(self):
        print("Fun3 Exec")

    def __getattr__(self, item):
        """
        :param item: str 类型 
        :return: 最后会调用该方法返回的对象，所以要返回一个可调用callable的对象，否则执行到此处会报错“TypeError: 'NoneType' object is not callable”
        """
        if item == "fun2":
            return lambda: print(("访问的属性或方法%s不存在") % (item))
        else:
            return self.fun3


def main():
    obj = Foo()
    obj.fun1()  # Fun1 Exec
    obj.fun2()  # 访问的属性或方法fun2不存在
    obj.fun3()  # Fun3 Exec
    obj.fun4()  # Fun3 Exec


if __name__ == '__main__':
    main()
