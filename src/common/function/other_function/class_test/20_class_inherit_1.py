#!/usr/bin/env python
# -*- coding: UTF-8 -*-

'''
http://blog.csdn.net/rocky_03/article/details/54891450
python模块和对象: 类的继承

如果在类定义中没有指定父类，则默认父类是object 类。
也就是说，object 是所有类的父类，里面定义了一些所有类共有的默认实现，比如：__new__()。

1. 成员继承：子类继承了父类除构造方法之外的所有成员。
2. 方法重写：子类可以重新定义父类中的方法，这样就会覆盖父类的方法，也称为“重写”

通过类的方法mro()或者类的属性__mro__可以输出这个类的继承层次结构。

Python 支持多重继承，一个子类可以继承多个父类。这样，就具备了“多个父类”的特点。
但是由于，这样会被“类的整体层次”搞的异常复杂，尽量避免使用。

Python 支持多继承，如果父类中有相同名字的方法，在子类没有指定父类名时，解释器将“从左向右”按顺序搜索。
MRO（Method Resolution Order）：方法解析顺序。
我们可以通过mro()方法获得“类的层次结构”，方法解析顺序也是按照这个“类的层次结构”寻找的。

'''


class Person(object):
    # 初始化函数
    def __init__(self, name, gender):
        self.name = name
        self.gender = gender
        print("类名:{0}".format(self.__class__.__name__))

    # 重写__str__()方法，类似java的toString方法
    def __str__(self):
        '''将对象转化成一个字符串，一般用于print 方法'''
        return '(Person:%s %s)' % (self.name, self.gender)
        # 直接输入对象并打印，面向开发者

    __repr__ = __str__


class Play(object):
    def __init__(self, name):
        self.name = name

    def do(self):
        return '%s...play..' % (self.name)


# 继承两个类
class Student(Person, Play):
    def __init__(self, name, gender, score, **kw):
        super(Student, self).__init__(name, gender)
        self.score = score
        self.__dict__.update(kw)

    def __str__(self):
        '''将对象转化成一个字符串，一般用于print 方法'''
        return '(Student:%s %s %s)' % (self.name, self.gender, self.score)

    __repr__ = __str__

    # 重写了父类的do()方法
    def do(self):
        return 'Student...do()...'


def test_1():
    p1 = Person('bart', 'man')
    print(p1)


def test_2():
    s1 = Student('lisa', 'women', 98)
    print(s1)
    print(s1.do())


def test_3():
    # 父类转化为子类
    s2 = Person('maggie', 'women')
    print(s2)


# 查看类的继承层次结构
def test_4():
    print(Student.mro())
    # print(Student.__mro__)


# 查看对象所有属性以及和object 进行比对
def test_5():
    obj = object()
    print(dir(obj))

    s2 = Person("高淇", 18)
    print(dir(s2))

    # 比object类多出的属性
    print(set(dir(s2)) - set(dir(obj)))
    # {'__dict__', 'gender', '__module__', '__weakref__', 'name'}


def main():
    choice = input("Enter a positive integer to choice: ")
    if choice == "1":
        test_1()
    elif choice == "2":
        test_2()
    elif choice == "3":
        test_3()
    elif choice == "4":
        test_4()
    elif choice == "5":
        test_5()
    # elif choice == "6":
    #     test_6()
    else:
        print("未知命令", choice)


if __name__ == '__main__':
    main()
