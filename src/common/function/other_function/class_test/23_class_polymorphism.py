#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
# 多态（polymorphism）是指同一个方法调用由于对象不同可能会产生不同的行为。
关于多态要注意以下2 点：
1. 多态是方法的多态，属性没有多态。
2. 多态的存在有2 个必要条件：继承、方法重写。
'''


class Animal:
    def shout(self):
        print("动物叫了一声")


class Dog(Animal):
    def shout(self):
        print("小狗，汪汪汪")


class Cat(Animal):
    def shout(self):
        print("小猫，喵喵喵")


def animalShout(a):
    if isinstance(a, Animal):
        a.shout()  # 传入的对象不同，shout 方法对应的实际行为也不同。


def main():
    animalShout(Dog())
    animalShout(Cat())


if __name__ == "__main__":
    main()
