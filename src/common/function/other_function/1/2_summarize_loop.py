#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# http://www.pythondoc.com/pythontutorial3/datastructures.html#tut-tuples
# 循环技巧


# 在序列中循环时，索引位置和对应值可以使用 enumerate() 函数同时得到
def test_1():
    for i, fileLine in enumerate(list):
        print(i, fileLine)


# 同时循环两个或更多的序列，可以使用 zip() 整体打包
def test_2():
    questions = ['name', 'quest', 'favorite color']
    answers = ['lancelot', 'the holy grail', 'blue']
    for q, a in zip(questions, answers):
        print('What is your {0}?  It is {1}.'.format(q, a))


# 需要逆向循环序列的话，先正向定位序列，然后调用 reversed() 函数
def test_3():
    for item in reversed(range(1, 10, 2)):
        print(item)


# 要按排序后的顺序循环序列的话，使用 sorted() 函数，它不改动原序列，而是生成一个新的已排序的序列
def test_4():
    basket = ['apple', 'orange', 'apple', 'pear', 'orange', 'banana']
    for f in sorted(set(basket)):
        print(f)


# 若要在循环内部修改正在遍历的序列（例如复制某些元素），建议您首先制作副本。
# 在序列上循环不会隐式地创建副本。切片表示法使这尤其方便
# list.copy() ：返回列表的一个浅拷贝。等同于 list[:]。
def test_5():
    words = ['cat', 'window', 'defenestrate']
    for w in words[:]:  # Loop over a slice copy of the entire list.
        if len(w) > 6:
            words.insert(0, w)
    print(words)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
