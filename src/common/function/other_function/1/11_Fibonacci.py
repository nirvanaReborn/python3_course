#! /usr/bin/python
# -*- coding: UTF-8 -*-

# 斐波那契数列:这个数列从第三项开始，每一项都等于前两项之和。
# return Fibonacci series up to n

def fib_2(n):
    """Return a list containing the Fibonacci series up to n."""
    result = []
    a, b = 0, 1
    while a < n:
        result.append(b)  # see below
        a, b = b, a + b
    return result


def fib_1(n):
    a, b = 0, 1
    while a < n:
        print(a, end=',')
        a, b = b, a + b
    print()


def main():
    fib_1(100)
    print(fib_2(100))


if __name__ == "__main__":
    main()
