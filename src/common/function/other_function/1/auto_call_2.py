#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnpython.com/qa/148061
# https://www.cnblogs.com/nebie/p/9145627.html
# https://blog.csdn.net/weixin_39955233/article/details/110345664
# python实现自动打电话软件


def test_1():
    pass


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
