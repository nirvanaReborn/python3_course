#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://qastack.cn/programming/15411967/how-can-i-check-if-code-is-executed-in-the-ipython-notebook
# 如何检查代码是否在IPython笔记本中执行？


def test_1():
    def isnotebook():
        try:
            import IPython
            # from IPython import get_ipython
            shell = IPython.get_ipython().__class__.__name__
            if shell == 'ZMQInteractiveShell':
                return True  # Jupyter notebook or qtconsole
            elif shell == 'TerminalInteractiveShell':
                return False  # Terminal running IPython
            else:
                return False  # Other type (?)
        except NameError:
            return False  # Probably standard Python interpreter


def test_2():
    # def is_interactive():
    #     import __main__ as main
    #     return not hasattr(main, '__file__')
    # from sys import argv
    #
    # if is_interactive():
    #     params = [<list of default parameters>]
    # else:
    #     params = argv[1:]
    pass


def test_3():
    def in_notebook():
        try:
            from IPython import get_ipython
            if 'IPKernelApp' not in get_ipython().config:  # pragma: no cover
                return False
        except ImportError:
            return False
        return True


def test_4():
    def is_run_from_ipython():
        try:
            from IPython import get_ipython
            if get_ipython():  # pragma: no cover
                return True
            else:
                return False
        except NameError:
            return False
    print(is_run_from_ipython())


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
