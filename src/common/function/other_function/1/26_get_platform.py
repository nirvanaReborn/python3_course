#!/usr/bin/env python3
# -*- coding:utf-8 -*-


import pprint


def test_1():
    '''用于检查特定的某些模块是否可用'''
    import os  # 提供操作系统的接口，常用的有文件系统相关和进程相关

    # 该变量返回当前操作系统的类型，当前只注册了3个值：分别是posix , nt(Windows) , java
    if os.name == "nt":
        print("Windows")
    elif os.name == "posix":
        print("Linux")
    elif os.name == "java":
        print("Java")
    else:
        print("未识别操作系统:", os.name)


def test_2():
    '''实际上运行uname和潜在的几个其他函数来确定运行时的系统类型'''
    import platform  # 提供平台相关的信息

    if platform.system() == 'Windows':
        print(platform.win32_ver())
    elif platform.system() == 'Linux':
        print(platform.dist())
        print(platform.python_compiler())
    elif platform.system() == 'Darwin':
        print(platform.mac_ver())
    elif platform.system() == 'Java':
        print(platform.java_ver())
    else:
        print("未识别操作系统:", platform.system())


def test_3():
    '''在构建配置时指定的编译器定义'''
    import sys  # 提供python解释器系统的通用配置和函数，影响着解释器的行为

    if sys.platform.startswith('win32'):
        print("Windows")
    elif sys.platform.startswith('linux'):
        print("Linux")
    elif sys.platform.startswith('darwin'):
        print("darwin")
    elif sys.platform.startswith('cygwin'):
        print("cygwin")
    elif sys.platform.startswith('freebsd'):
        print("freebsd")
    else:
        print("未识别操作系统:", sys.platform)


def main():
    dict_choice = {}
    dict_description = {}
    for i in range(1, 4):
        dict_choice[str(i)] = "test_{}()".format(str(i))
        dict_description[i] = eval("test_" + str(i)).__doc__
    pprint.pprint(dict_description)

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
