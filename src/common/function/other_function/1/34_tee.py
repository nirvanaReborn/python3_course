#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.zhihu.com/question/25045795/answer/29914541
# 用python在windows下实现linux的tee命令


import sys


def tee(filename):
    with open(filename, 'w') as f:
        for line in sys.stdin.readlines():
            sys.stdout.write(line)
            f.write(line)


if __name__ == '__main__':
    filename = sys.argv[1]
    tee(filename)
