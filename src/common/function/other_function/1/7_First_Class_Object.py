#!/usr/bin/env python
# -*- coding: UTF-8 -*-

'''
如何正确理解Python函数是第一类对象
https://foofish.net/function-is-first-class-object.html

在 Python 中万物皆为对象，函数也不例外,这些特性就是第一类对象所特有的:
函数作为对象可以赋值给一个变量,
可以作为元素添加到集合对象中,
可作为参数值传递给其它函数，
还可以当做函数的返回值，
'''


def foo(text):
    return len(text)


# 函数是对象
def test_1():
    print(foo("zen of python"))  # 13

    # 函数身为一个对象，拥有对象模型的三个通用属性：id、类型、和值。
    print(id(foo))  # 50541744
    print(type(foo))  # <class 'function'>
    print(foo)  # <function foo at 0x030334B0>

    # 作为对象，函数可以赋值给一个变量
    bar = foo
    # 赋值给另外一个变量时，函数并不会被调用，仅仅是在函数对象上绑定一个新的名字而已。
    print(bar)  # <function foo at 0x02C034B0>
    print(bar("zen of python"))  # 13

    # 同理，你还可以把该函数赋值给更多的变量，
    # 唯一变化的是该函数对象的引用计数不断地增加，
    # 本质上这些变量最终指向的都是同一个函数对象。
    a = foo
    b = foo
    c = bar
    print(a is b is c)  # True


# 函数可以存储在容器
# 容器对象（list、dict、set等）中可以存放任何对象，包括整数、字符串，函数也可以作存放到容器对象中
def test_2():
    # foo 是我们自定义的函数，str 和 len 是两个内置函数。
    funcs = [foo, str, len]
    print(funcs)  # [<function foo at 0x039A34B0>, <class 'str'>, <built-in function len>]

    for f in funcs:
        print(f("hello"))  # 5  hello  5
    # for 循环逐个地迭代出列表中的每个元素时，函数对象赋值给了 f 变量，
    # 调用 f(“hello”) 与 调用 foo(“hello”) 本质是一样的效果，每次 f 都重新指向一个新的函数对象。
    # 当然，你也可以使用列表的索引定位到元素来调用函数。
    print(funcs[0]("Python之禅"))  # 8


# ---------------------------------------------------------------------------------------------------
def show(func):
    size = func("python 之禅")  # 等效于 foo("Python之禅")
    print("length of string is : %s" % size)


# 函数可以作为参数
def test_3():
    show(foo)


# ---------------------------------------------------------------------------------------------------
def nick():
    return foo


# 函数可以作为返回值
def test_4():
    print(nick)  # <function nick at 0x02DF3390>
    a = nick()
    print(a)  # <function foo at 0x02D834B0>
    print(a("python"))  # 6
    # 还可以简写为 print(nick()("python"))


# ---------------------------------------------------------------------------------------------------

# 函数接受一个或多个函数作为输入或者函数输出（返回）的值是函数时，我们称这样的函数为高阶函数，比如上面的 show 和 nick 都属于高阶函数。
# Python内置函数中，典型的高阶函数是 map 函数，map 接受一个函数和一个迭代对象作为参数，调用 map 时，依次迭代把迭代对象的元素作为参数调用该函数。
# map 函数的作用相当于：[foo(i) for i in ["the","zen","of","python"]],只不过 map 的运行效率更快一点。
def test_5():
    lens = map(foo, ["the", "zen", "of", "python"])
    print(list(lens))  # [3, 3, 2, 6]


# ---------------------------------------------------------------------------------------------------
# 函数可以嵌套
# 函数中里面嵌套的函数不能在函数外面访问，只能是在函数内部使用，超出了外部函数的做用域就无效了。
def test_6():
    def get_length(text):
        def clean(t):  # 最后执行这处代码
            return t[1:]

        new_text = clean(text)  # 然后执行这处代码
        return len(new_text)

    get_length("python")  # 先执行这处代码


# ---------------------------------------------------------------------------------------------------
# 实现了 __call__ 的类也可以作为函数
class Add:
    def __init__(self, n):
        self.n = n

    def __call__(self, x):
        return self.n + x


def test_7():
    add = Add(1)
    print(add(4))  # 执行 add(4) 相当于调用 Add._call__(add, 4)，self 就是实例对象 add，self.n 等于 1，所以返回值为 1+4

    # 确定对象是否为可调用对象可以用内置函数callable来判断。
    print(callable(foo))  # True
    print(callable(1))  # False
    print(callable(int))  # True


# ---------------------------------------------------------------------------------------------------
def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
