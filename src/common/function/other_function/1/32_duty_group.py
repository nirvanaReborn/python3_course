#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 轮流排班
import datetime

import holidays
import pandas


def get_rest_day(year, option):
    start_date = datetime.date(year, 1, 1)
    end_date = datetime.date(year, 12, 31)

    match option:
        case "1":  # 获取某年的所有周末日期
            # 获取当前星期几(0-6)，其中0表示星期一，1表示星期二，依此类推
            holidays_list = [date for date in (start_date + datetime.timedelta(days=i) for i in range((end_date - start_date).days + 1)) if date.weekday() in [5, 6]]
            print(holidays_list)
        case "2":
            # 创建一个中国节假日的holiday对象
            china_holidays = holidays.China(years=year)
            # for dt, name in sorted(china_holidays.items()):
            #     print(dt, name)

            # 获取某年所有节假日
            holidays_list = china_holidays[start_date.strftime('%Y-%m-%d'): end_date.strftime('%Y-%m-%d')]
        case "3":
            dest_file = r"D:\SVN\PBOX-FLY\qtfly\fly_tools\create_trading_day\non_trading_date.csv"
            df = pandas.read_csv(dest_file, dtype=str)
            holidays_list_old = df['#yyyymmdd'].tolist()
            holidays_list = []
            for s in holidays_list_old:
                try:
                    if int(str(year) + "0101") <= int(s) <= int(str(year) + "1231"):
                        date_obj = datetime.datetime.strptime(s, "%Y%m%d").date()
                        holidays_list.append(date_obj)
                except ValueError:
                    # 处理日期格式不正确的情况
                    print(f"Invalid date format: {s}")
            print(holidays_list)
        case "4":
            try:
                weekend_dates = get_rest_day(year, "1")
                holidays_in_range = get_rest_day(year, "3")

                if not isinstance(weekend_dates, list) or not isinstance(holidays_in_range, list):
                    raise ValueError("get_rest_day should return a list")

                all_dates = weekend_dates + holidays_in_range
                holidays_list = sorted(set(all_dates))
            except Exception as e:
                print(f"Error: {e}")
                holidays_list = []
        case _:
            holidays_list = []
            print("请输入一个有效数字！")
    # print(len(holidays_list), holidays_list)
    return holidays_list


# 一个小组5个人,对每周的工作日进行轮流排班
def test_1():
    # 定义一个包含五名成员的小组
    team = ['张三', '李四', '王五', '赵六', '钱七']

    # 获取当前星期几（0-6），其中0表示星期一，1表示星期二，依此类推
    weekday = datetime.date.today().weekday() % len(team)

    print("今天值班人员为：", team[weekday])


# 一个小组5个人,对某年的节假日进行轮流排班
def test_2(year=2024):
    def generate_roster(start_date, end_date):
        holidays_in_range = get_rest_day(year, "2")
        # 初始化排班表
        roster = []

        # 遍历排班日期范围内的所有日期，根据是否为节假日进行排班
        current_date = start_date
        while current_date <= end_date:
            # 计算当前周末是本年的第几个周末（从1开始计数）
            week_number = (current_date - start_date).days // 7 + 1
            temp = {'week_number': week_number}
            if current_date in holidays_in_range:
                temp['days'] = {'date': current_date, 'person': '休假'}
            else:
                temp['days'] = {'date': current_date, 'person': '上班'}
                pass
            roster.append(temp)
            current_date += datetime.timedelta(days=1)

        return roster

    start_date = datetime.date(year, 1, 1)  # 排班起始日期
    end_date = datetime.date(year, 12, 31)  # 排班结束日期
    roster = generate_roster(start_date, end_date)
    for week in roster:
        # print(week)
        print("第{}周：".format(week['week_number']), end='\t')
        for day, person in week['days'].items():
            print("{0}: {1}".format(day, person), end='\t')
        print()


# 一个小组5个人,对某年的每周末进行轮流排班
def test_3(year=2025):
    dict_team = {
        '张涛': "13023661326",
        '何芳涛': "15868893114",
        '张家壮': "17663841265",
        '陈佳望': "18742586008",
        '章淳': "15168228362",
        # '王慧勇': "13758247998",
    }
    # 定义小组成员列表
    team = list(dict_team.keys())

    start_date = datetime.date(year, 1, 1)
    weekend_dates = get_rest_day(year, "4")

    # 初始化每个人的值班次数列表
    duty_counts = [0] * len(team)
    list_2d = []
    # 遍历每个周末，安排值班人员
    for weekend in weekend_dates:
        # 计算当前周末是本年的第几个周末（从1开始计数）
        week_num = weekend.isocalendar()[1]

        # 计算当前周末是第几个人值班（从0开始计数）
        duty_index = (week_num - 1) % len(team)

        # 将值班次数加1
        duty_counts[duty_index] += 1

        list_temp = [str(weekend), "第%d周" % week_num, team[duty_index], dict_team[team[duty_index]], ""]
        print(list_temp)
        list_2d.append(list_temp)
    df = pandas.DataFrame(list_2d, columns="日期 周末次序 值班人员 联系方式 备注".split())
    df.to_excel(r"D:\值班表{0}.xlsx".format(year), sheet_name=str(year), index=False, header=True)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
