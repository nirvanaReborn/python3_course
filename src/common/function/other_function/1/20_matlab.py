#!/usr/bin/env python
# -*- coding:utf-8 -*-

# python + NumPy + Scipy + matplotlib = Matlab
# https://www.toutiao.com/i6837704943449670155/
# Python是一门完全免费的通用编程语言，以开源的方式提供了大量各类用途的库与包，
# 如Numpy（数值计算）、SciPy（数学、科学和工程计算）、Matplotlib（类似MATLAB中plot的绘图工具）等等。
# 如果不是必须用到MATLAB simulink的话，用Python来取代MATLAB 应该是最佳方式了。


def main():
    pass


if __name__ == "__main__":
    main()
