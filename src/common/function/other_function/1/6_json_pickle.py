#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/wupeiqi/articles/4963027.html
# 用于序列化的两个模块
# json:   用于 字符串 和 python数据类型间进行转换
# pickle: 用于 python特有的类型 和 python的数据类型间进行转换
# json模块提供了四个功能：dumps、dump、loads、load
# pickle模块提供了四个功能：dumps、dump、loads、load

import json


# 保存成json格式文件
def test_1(data):
    with open('data.json', 'w') as fw:
        json.dump(data, fw)


# 读取json格式文件
def test_2():
    with open('data.json', 'r') as fr:
        data = json.load(fr)
    return data


# json.dumps()函数的使用，将字典转化为字符串
def test_3():
    dict1 = {"age": "12"}
    json_info = json.dumps(dict1)
    print("dict1的类型：" + str(type(dict1)))  # <class 'dict'>
    print("通过json.dumps()函数处理：")
    print("json_info的类型：" + str(type(json_info)))  # <class 'str'>


# json.loads()函数的使用，将字符串转化为字典
def test_4():
    json_info = '{"age": "12"}'
    dict1 = json.loads(json_info)
    print("json_info的类型：" + str(type(json_info)))  # <class 'str'>
    print("通过json.dumps()函数处理：")
    print("dict1的类型：" + str(type(dict1)))  # <class 'dict'>


def make_choice(dict_choice):
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    make_choice(dict_choice)


if __name__ == "__main__":
    main()
