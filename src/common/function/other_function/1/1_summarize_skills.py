#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6546535684776133127/
# 程序员必备的30个Python技巧和窍门


# 1、交换两个变量的值。
def test_1():
    x, y = 10, 20
    print(x, y)  # (10, 20)

    x, y = y, x
    print(x, y)  # (20, 10)


# 2、比较运算符链
def test_2():
    n = 10
    result = 1 < n < 20
    print(result)  # True

    result = 1 > n <= 9
    print(result)  # False


# 3、三元算子在条件赋值中的应用
def test_3():
    # 三元运算符是IF语句的快捷方式，也称为条件运算符。
    # [on_true] if [expression] else [on_false]

    # 以下几个例子可以让你的代码简洁明了。
    # x = 10 if (y == 9) else 20
    #
    # 同样，我们也可以用类对象做同样的事情。
    # x = (classA if y == 1 else classB)(param1, param2)
    # 以上例子中classA或classB的构造函数会被调用
    #
    # 以下函数可以计算最小值

    def small(a, b, c):
        return a if a <= b and a <= c else (b if b <= a and b <= c else c)

    print(small(1, 0, 1))
    print(small(1, 2, 2))
    print(small(2, 2, 3))
    print(small(5, 4, 3))

    # 我们甚至可以用三元运算符生成列表
    print([m ** 2 if m > 10 else m ** 4 for m in range(50)])


# 4、多行字符串:1.可以用""" 2.还可以将字符串分割成多行，并用括号括起来整个字符串。
def test_4():
    multiStr = """select * from multi_row
        where row_id < 5"""
    print(multiStr)

    multiStr = ("select * from multi_row "
                "where row_id < 5")
    print(multiStr)


# 5、将存储列表元素赋值到新变量中。
def test_5():
    testList = [1, 2, 3]
    x, y, z = testList
    print(x, y, z)


# 6、打印导入模块的文件路径
def test_6():
    import threading
    import socket

    print(threading)
    print(socket)


# 7、使用“_”运算符。在Python控制台中，每当我们测试表达式或调用函数时，结果都会发送到临时名称，即（_）。
def test_7():
    '''
    >>> 2 + 1
    3
    >>> _
    3
    >>> print _
    3
    '''


# 8、字典/集合内嵌初始化
def test_8():
    testDict = {i: i * i for i in range(10)}
    print(testDict)

    testSet = {i * 2 for i in range(10)}
    print(testSet)


# 9、调试脚本:我们可以在<Pb>模块的帮助下在Python脚本中设置断点。请参照下面的例子。
def test_9():
    import pdb

    pdb.set_trace()


# 10、文件共享
def test_10():
    '''
    Python允许运行HTTP服务器，您可以使用HTTP服务器从服务器根目录共享文件。下面是启动服务器的命令。
    # Python 2
    python -m SimpleHTTPServer

    # Python 3
    python3 -m http.server
    上面的命令将启动默认端口8000的服务器。还可以将自定义端口作为最后一个参数传递给上面的命令。
    '''


# 11、Python对象检查:我们可以通过dir()函数查看Python对象内容。
def test_11():
    test = [1, 3, 5, 7]
    print(dir(test))


# 12、简化if语句
def test_12():
    # 验证一个值是否和多个值匹配可以用如下语句：
    # if m in [1, 3, 5, 7]:
    #     print(m)
    # 替代
    # if m == 1 or m == 3 or m == 5 or m == 7:
    #     print(m)
    # 我们也可以用‘{1, 3, 5, 7}’替代‘[1, 3, 5, 7]’，因为集合访问元素的时间复杂度是O(1)
    pass


# 13、运行时检测Python版本
# 有时，如果当前运行的Python引擎小于支持的版本，我们可能不想执行某些程序。
# 为了实现这一点，可以使用下面的编码片段。
def test_13():
    import sys

    print(sys.version_info, sys.hexversion)
    print("Current Python version: ", sys.version)

    # 我们同样可以使用sys.version_info >= (3, 5)来替换sys.hexversion!= 50660080
    if not hasattr(sys, "hexversion") or sys.version_info >= (3, 5):
        print("Sorry, you aren't running on Python 3.5 ")
        print("Please upgrade to 3.5. ")
        sys.exit(1)


# 14、合并多个字符串
def test_14():
    test = ['I', 'Like', 'Python', 'automation']
    print(''.join(test))


# 15、四种方法反转 string/list.
def test_15():
    # 通过列表自身反转
    testList = [1, 3, 5]
    testList.reverse()
    print(testList)

    # 在循环中反转
    for element in reversed([1, 3, 5]):
        print(element)

    # 在同一行中反转标量
    print("Test Python:", [1, 3, 5][::-1])

    # 通过切片反转
    print([1, 3, 5][::-1])


# 16、玩转迭代器
def test_16():
    testlist = [10, 20, 30]
    for i, value in enumerate(testlist):
        print(i, ': ', value)


# 17、不用循环生成一个归一化列表
def test_17():
    import itertools

    test = [[-1, -2], [30, 40], [25, 35]]
    print(list(itertools.chain.from_iterable(test)))
    # [-1, -2, 30, 40, 25, 35]


# 18、函数返回多个值
def test_18():
    def x():
        return 1, 2, 3, 4

    a, b, c, d = x()
    print(a, b, c, d)


# 19、函数参数提取
def test_19():
    def test(x, y, z):
        print(x, y, z)

    testDict = {'x': 1, 'y': 2, 'z': 3}
    test(*testDict)  # x y z
    test(**testDict)  # 1 2 3

    testList = [10, 20, 30]
    test(*testList)  # 10 20 30


# 20、用字典存储开关
def test_20():
    stdcalc = {
        'sum': lambda x, y: x + y,
        'subtract': lambda x, y: x - y
    }

    print(stdcalc['sum'](9, 3))
    print(stdcalc['subtract'](9, 3))


# 21、一行语句计算阶乘
def test_21():
    '''Python 2.x.
    result = (lambda k: reduce(int.__mul__, range(1,k+1),1))(3)
    print(result)
    '''
    # Python 3.x.
    import functools

    result = (lambda k: functools.reduce(int.__mul__, range(1, k + 1), 1))(3)
    print(result)


# 22、元素频率
def test_22():
    mylist = [1, 2, 3, 4, 2, 2, 3, 1, 4, 4, 4]
    # print(min(set(mylist), key=mylist.count)) # 找出列表中出现频率最低的元素
    print(max(set(mylist), key=mylist.count))  # 找出列表中出现频率最高的元素


# 23、重置回归限制
def test_23():
    import sys

    x = 1001
    print(sys.getrecursionlimit())  # 1000
    sys.setrecursionlimit(x)
    print(sys.getrecursionlimit())  # 1001


# 24、查看一个对象的内存占用
def test_24():
    '''In Python 2.7.
    import sys
    x=1
    print(sys.getsizeof(x)) # 24
    '''
    import sys
    x = 1
    print(sys.getsizeof(x))  # 28


# 25、使用__slots__减少内存开销
def test_25():
    import sys

    class FileSystem(object):
        def __init__(self, files, folders, devices):
            self.files = files
            self.folders = folders
            self.devices = devices

    print(sys.getsizeof(FileSystem))  # 1056

    class FileSystem1(object):
        __slots__ = ['files', 'folders', 'devices']

        def __init__(self, files, folders, devices):
            self.files = files
            self.folders = folders
            self.devices = devices

    print(sys.getsizeof(FileSystem1))  # 888


# 26、Lambda表达式模仿print函数
def test_26():
    import sys
    lprint = lambda *args: sys.stdout.write(" ".join(map(str, args)))
    lprint("python", "tips", 1000, 1001)


# 27、通过两个相关序列创建字典
def test_27():
    keys = (1, 2, 3)
    values = (10, 20, 30)
    print(dict(zip(keys, values)))  # {1: 10, 2: 20, 3: 30}


# 28、在字符串中搜索前缀
def test_28():
    print("http://www.google.com".startswith(("http://", "https://")))
    print("http://www.google.co.uk".endswith((".com", ".co.uk")))


# 29、使用Python枚举
def test_29():
    class Shapes:
        Circle, Square, Triangle, Quadrangle = range(4)

    print(Shapes.Circle)
    print(Shapes.Square)
    print(Shapes.Triangle)
    print(Shapes.Quadrangle)


# 30、实现一个真正的switch-case语句
def test_30():
    def xswitch(x):
        return xswitch._system_dict.get(x, None)

    xswitch._system_dict = {'files': 10, 'folders': 5, 'devices': 2}
    print(xswitch('default'))
    print(xswitch('devices'))


# https://www.toutiao.com/a6740081099646435847/
# 1. 重复元素判定
def test_31():
    def all_unique(lst):
        return len(lst) == len(set(lst))

    x = [1, 1, 2, 2, 3, 2, 3, 4, 5, 6]
    y = [1, 2, 3, 4, 5]
    all_unique(x)  # False
    all_unique(y)  # True


# 2.字符元素组成判定:检查两个字符串的组成元素是不是一样的。
def test_32():
    from collections import Counter
    def anagram(first, second):
        return Counter(first) == Counter(second)

    anagram("abcd3", "3acdb")  # True


# 3. 字节占用:下面的代码块可以检查字符串占用的字节数。
def test_33():
    def byte_size(string):
        return (len(string.encode('utf-8')))

    byte_size('')  # 4
    byte_size('Hello World')  # 11


# 分块: 给定具体的大小，定义一个函数以按照这个大小切割列表。
def test_34():
    from math import ceil
    def chunk(lst, size):
        return list(map(lambda x: lst[x * size:x * size + size], list(range(0, ceil(len(lst) / size)))))

    chunk([1, 2, 3, 4, 5], 2)  # [[1,2],[3,4],5]


# 压缩: 这个方法可以将布尔型的值去掉
def test_35():
    def compact(lst):
        return list(filter(bool, lst))

    compact([0, 1, False, 2, '', 3, 'a', 's', 34])  # [ 1, 2, 3, 'a', 's', 34 ]


# 解包: 如下代码段可以将打包好的成对列表解开成两组不同的元组。
def test_36():
    array = [['a', 'b'], ['c', 'd'], ['e', 'f']]
    transposed = zip(*array)
    print(transposed)  # [('a', 'c', 'e'), ('b', 'd', 'f')]


# 链式对比: 我们可以在一行代码中使用不同的运算符对比多个不同的元素。
def test_37():
    a = 3
    print(2 < a < 8)  # True
    print(1 == a < 2)  # False


# 元音统计: 以下方法将统计字符串中的元音 (‘a’, ‘e’, ‘i’, ‘o’, ‘u’) 的个数，它是通过正则表达式做的。
def test_38():
    import re
    def count_vowels(str):
        return len(re.findall(r'[aeiou]', str, re.IGNORECASE))

    count_vowels('foobar')  # 3
    count_vowels('gym')  # 0


# 首字母小写
def test_39():
    def decapitalize(string):
        return str[:1].lower() + str[1:]

    decapitalize('FooBar')  # 'fooBar'


# 展开列表
def test_40():
    def spread(arg):
        ret = []
        for i in arg:
            if isinstance(i, list):
                ret.extend(i)
            else:
                ret.append(i)
        return ret

    def deep_flatten(lst):
        result = []
        result.extend(spread(list(map(lambda x: deep_flatten(x) if type(x) == list else x, lst))))
        return result

    deep_flatten([1, [2], [[3], 4], 5])  # [1,2,3,4,5]


# 列表的差
def test_41():
    def difference(a, b):
        set_a = set(a)
        set_b = set(b)
        comparison = set_a.difference(set_b)
        return list(comparison)

    difference([1, 2, 3], [1, 2, 4])  # [3]


# 通过函数取差
def test_42():
    def difference_by(a, b, fn):
        b = set(map(fn, b))
        return [item for item in a if fn(item) not in b]

    from math import floor
    difference_by([2.1, 1.2], [2.3, 3.4], floor)  # [1.2]
    difference_by([{'x': 2}, {'x': 1}], [{'x': 1}], lambda v: v['x'])  # [ { x: 2 } ]


# 链式函数调用
def test_43():
    def add(a, b):
        return a + b

    def subtract(a, b):
        return a - b

    a, b = 4, 5
    print((subtract if a > b else add)(a, b))  # 9


#  检查重复项:如下代码将检查两个列表是不是有重复项。
def test_44():
    def has_duplicates(lst):
        return len(lst) != len(set(lst))

    x = [1, 2, 3, 4, 5, 5]
    y = [1, 2, 3, 4, 5]
    has_duplicates(x)  # True
    has_duplicates(y)  # False


# 合并两个字典
def test_45():
    def merge_two_dicts(a, b):
        c = a.copy()  # make a copy of a
        c.update(b)  # modify keys and values of a with the ones from b
        return c

    a = {'x': 1, 'y': 2}
    b = {'y': 3, 'z': 4}
    print(merge_two_dicts(a, b))  # {'y': 3, 'x': 1, 'z': 4}

    # 在 Python 3.5 或更高版本中，我们也可以用以下方式合并字典：
    def merge_dictionaries(a, b):
        return {**a, **b}

    a = {'x': 1, 'y': 2}
    b = {'y': 3, 'z': 4}
    print(merge_dictionaries(a, b))  # {'y': 3, 'x': 1, 'z': 4}


# 我们在使用 try/except 语句的时候也可以加一个 else 子句，如果没有触发错误的话，这个子句就会被运行。
def test_46():
    try:
        2 * 3
    except TypeError:
        print("An exception was raised")
    else:
        print("Thank God, no exceptions were raised.")  # Thank God, no exceptions were raised.


# 回文序列
def test_47():
    def palindrome(string):
        import re
        s = re.sub('[\W_]', '', string.lower())
        return s == s[::-1]

    palindrome('taco cat')  # True


# 不使用 if-else 的计算
def test_48():
    import operator
    action = {
        "+": operator.add,
        "-": operator.sub,
        "/": operator.truediv,
        "*": operator.mul,
        "**": pow,
    }
    print(action['-'](50, 25))  # 25


# shuffle将序列的所有元素随机排序: 该算法会打乱列表元素的顺序，它主要会通过 Fisher-Yates 算法对新列表进行排序：
def test_49():
    from copy import deepcopy
    from random import randint
    def shuffle(lst):
        temp_lst = deepcopy(lst)
        m = len(temp_lst)
        while (m): m -= 1
        i = randint(0, m)
        temp_lst[m], temp_lst[i] = temp_lst[i], temp_lst[m]
        return temp_lst

    foo = [1, 2, 3]
    shuffle(foo)  # [2,3,1] , foo = [1,2,3]


def test_50():
    # 1.使用sum内置求和函数
    num = sum([1, 2, 3, 10248])
    print(num)
    # 2.reduce 函数
    from functools import reduce
    num1 = reduce(lambda x, y: x + y, [1, 2, 3, 10248])
    print(num1)


# -------------------------------------------------------------------
def test_51():
    def multipliers():
        return [lambda x: i * x for i in range(4)]

    print([m(3) for m in multipliers()])  # [9, 9, 9, 9]
    '''上述问题产生的原因是python闭包的延迟绑定。
    这意味着内部函数被调用时，参数的值在闭包内进行查找。
    因此，当任何由multipliers()返回的函数被调用时,i的值将在附近的范围进行查找。
    那时，不管返回的函数是否被调用，for循环已经完成，i被赋予了最终的值3.'''


def test_52():
    def multipliers():
        return [lambda x, i=i: i * x for i in range(4)]

    print([m(3) for m in multipliers()])  # [0, 3, 6, 9]


def test_53():
    def multipliers():
        for i in range(4):
            yield lambda x: i * x

    print([m(3) for m in multipliers()])  # [0, 3, 6, 9]


# -------------------------------------------------------------------


def test_54():
    x = (i for i in range(10))
    print(type(x))  # <class 'generator'>  #  生成器


def test_55():
    N = 100
    print([[x for x in range(1, N)][i:i + 3] for i in range(0, N, 3)])


def test_56():
    import os
    import sys
    print(__file__)
    print("{0}".format(sys._getframe().f_code.co_filename))
    print("{0}".format(sys.argv[0]))

    print(os.path.basename(__file__))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
