#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 遍历目录树

import os


def traversal_directory_tree(startpath):
    for folderName, subfolders, filenames in os.walk(startpath):
        print('当前目录：' + folderName)
        for subfolder in subfolders:
            print('  子目录：' + folderName + ': ' + subfolder)
        for filename in filenames:
            print('当前文件：' + folderName + ': ' + filename)
        print('')


def dfs_showdir(startpath, depth):
    if depth == 0:
        # print("root:[" + path + "]")
        print(startpath + os.sep)

    for item in os.listdir(startpath):
        if '.git' not in item:
            if os.path.isdir(startpath + os.sep + item):
                item += os.sep
            print("|      " * depth + "|-- " + item)

            newitem = startpath + os.sep + item
            if os.path.isdir(newitem):
                dfs_showdir(newitem, depth + 1)


def travelTree(currentPath, count):
    if not os.path.exists(currentPath):
        return
    if os.path.isfile(currentPath):
        fileName = os.path.basename(currentPath)
        print('\t' * count + '├── ' + fileName)
    elif os.path.isdir(currentPath):
        print('\t' * count + '├── ' + currentPath + os.sep)
        pathList = os.listdir(currentPath)
        for eachPath in pathList:
            travelTree(currentPath + os.sep + eachPath, count + 1)


def list_files(startpath):
    for currentDir, subdirs, files in os.walk(startpath):
        level = currentDir.replace(startpath, '').count(os.sep)
        dir_indent = "|      " * (level - 1) + "|-- "
        file_indent = "|      " * level + "|-- "
        if not level:
            # print('.')
            print(startpath + os.sep)
        else:
            print('{}{}{}'.format(dir_indent, os.path.basename(currentDir), os.sep))

        for f in files:
            print('{}{}'.format(file_indent, f))


def main():
    # startpath = r'C:\Intel'
    startpath = os.getcwd()
    # traversal_directory_tree(startpath)
    # dfs_showdir(startpath, 0)
    # travelTree(startpath, 0)
    list_files(startpath)


if __name__ == "__main__":
    main()
