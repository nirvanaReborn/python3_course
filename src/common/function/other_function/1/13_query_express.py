#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6518279310250344968/
# 查询快递信息

import json

import requests


def searchPackage():
    # 只有正在途中的快递才可以查到
    packageNum = input("请输入运单号码：")
    url_1 = "http://www.kuaidi100.com/autonumber/autoComNum?resultv2=1&text=" + packageNum
    companyName = json.loads(requests.get(url_1).text)["auto"][0]["comCode"]
    url_2 = "http://www.kuaidi100.com/query?type=" + companyName + "&postide=" + packageNum
    for item in json.loads(requests.get(url_2).text)["data"]:
        print(item["time"], item["context"])


def main():
    searchPackage()


if __name__ == "__main__":
    main()
