#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6532261435840397838/
# python代码监测服务器并实现微信通知
# 源码：https://github.com/uselibrary/Server_Status
'''
程序原理：通过监测端crontab定时运行监测程序来ping被监测端的服在，只进行一次ping操作，
并对反馈回来的数据进行分析，一旦发现ping失败了，就通过server酱推送到微信上，通知服务器已经断线了。
需要配合server酱的sckey使用，自行去sc.ftqq.com申请一个，直接替换程序中的SCKEY即可，
xx.xx.xx.xx为被监测服务器的ip地址，请自行更换。

要使用Ping工具，你应该有一个ServerChan sckey，点击这里(http://sc.ftqq.com/3.version)获得一个免费的密钥。
下载Python文件并运行它crontab，建议每15分钟运行一次。
'''

import datetime
import os

import requests


def main():
    timeout = 10
    time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    key = 'SCKEY'
    title = "VPS在线检测"
    hostname = "xx.xx.xx.xx"
    response = os.system("ping -c 1 " + hostname)
    if response != 0:
        content = "VPS已无法连接" + "\n" + time
        payload = {
            'text': title,
            'desp': content
        }
        url = 'https://sc.ftqq.com/{}.send'.format(key)
        requests.post(url, params=payload, timeout=timeout)
    else:
        content = "VPS正常连接" + "\n" + time


if __name__ == "__main__":
    main()
