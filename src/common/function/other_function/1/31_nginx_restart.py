#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/soulbm/p/6943540.html
# python脚本————nginx启停


import os
from sys import argv

DAEMON = '/usr/local/nginx/sbin/nginx'
CONFIGFILE = '/usr/local/nginx/conf/nginx.conf'
PIDFILE = '/usr/local/nginx/logs/nginx.pid'

NGSTART = '%s -c %s' % (DAEMON, CONFIGFILE)
NGSTOP = 'kill -QUIT `cat %s`' % PIDFILE


def n_start():
    if os.path.isfile(PIDFILE):
        print("nginx is already running!")
    else:
        if os.system(NGSTART) == 0:
            print("nginx start is ok!")


def n_stop():
    if os.path.isfile(PIDFILE):
        if os.system(NGSTOP) == 0:
            print("nginx stop is ok!")
    else:
        print("nginx is not running!")


def n_restart():
    n_stop()
    n_start()


if __name__ == '__main__':
    if argv[1] == 'start':
        n_start()
    elif argv[1] == 'stop':
        n_stop()
    elif argv[1] == 'restart':
        n_restart()
    else:
        print("Usage: %s {start|stop|restart|reload}" % argv[0])
