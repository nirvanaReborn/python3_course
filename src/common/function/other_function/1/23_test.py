#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a7019101439871943207/?log_from=4aad75809a181_1634355757634
# 如何利用Python实现'2021年9月28日'转换位为'2021/9/28'格式？

# 方法一
def test_1():
    date1 = '2021年9月28日'
    date2 = date1.replace("年", "/").replace("月", "/").replace("日", "")
    print(date2)


def test_2():
    date1 = '2021年9月28日'
    year = date1.split("年")[0]
    month = date1.split("年")[1].split("月")[0]
    day = date1.split("年")[1].split("月")[1].split("日")[0]
    print(year, month, day)
    date2 = "/".join([year, month, day])
    print(date2)


def test_3():
    import datetime
    date1 = '2021年9月28日'
    b = datetime.datetime.strptime(date1, '%Y年%m月%d日')
    print(b)
    date2 = b.strftime('%Y{}%m{}%d'.format('/', '/'))
    print(date2)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
