#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.jb51.net/article/167179.htm
# python 实现手机自动拨打电话的方法(通话压力测试)
# 用python结合adb命令去实现安卓手机端的通话压力测试。

# 操作前先在设置里打开power键可以结束通话按钮，否则会导致代码报错
from time import sleep
import os


def test_call(number):
    # 拨打电话
    call = os.popen('adb shell am start -a android.intent.action.CALL -d tel:{}'.format(number))
    sleep(20)
    # 挂断电话
    Hangup = os.popen('adb shell input keyevent 26')
    sleep(5)


def batch_call():
    list_telephone_number = [
        13023661326,
        # 10086,  # 移动
        # 10011,  # 联通网上营业厅
        # 12580,  # 中国移动为广大客户提供的综合信息服务平台
        # 114,  # 为用户提供号码信息服务
    ]
    for number in list_telephone_number:
        test_call(number)


def main():
    batch_call()
    # 截图
    ScreenShot = os.popen('adb shell /system/bin/screencap -p /sdcard/xie.png')

    # 将截图保存到电脑
    SaveScreenShot = os.popen('adb pull /sdcard/xie.png')


if __name__ == "__main__":
    main()
