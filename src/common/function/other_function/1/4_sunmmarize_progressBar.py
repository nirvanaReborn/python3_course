#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 实现进度条功能
import sys
import time


def test_1():
    for i in range(40):
        sys.stdout.write('*')
        # flush的作用相当于照相，拍一张冲洗一张
        sys.stdout.flush()
        time.sleep(0.1)


# 下面代码也能够实现相同的功能
def test_2():
    for i in range(40):
        # print中的flush参数
        print('*', end='', flush=True)
        time.sleep(0.1)


# https://www.cnblogs.com/hongfei/p/3982259.html
def test_3():
    class progressbar(object):
        def __init__(self, finalcount, block_char='.'):
            self.finalcount = finalcount
            self.blockcount = 0
            self.block = block_char
            self.f = sys.stdout
            if not self.finalcount: return
            self.f.write('\n------------------ % Progress -------------------1\n')
            self.f.write('    1    2    3    4    5    6    7    8    9    0\n')
            self.f.write('----0----0----0----0----0----0----0----0----0----0\n')

        def progress(self, count):
            count = min(count, self.finalcount)
            if self.finalcount:
                percentcomplete = int(round(100.0 * count / self.finalcount))
                if percentcomplete < 1: percentcomplete = 1
            else:
                percentcomplete = 100
            blockcount = int(percentcomplete // 2)
            if blockcount <= self.blockcount:
                return
            for i in range(self.blockcount, blockcount):
                self.f.write(self.block)
            self.f.flush()
            self.blockcount = blockcount
            if percentcomplete == 100:
                self.f.write("\n")

    pb = progressbar(8, "*")
    for count in range(1, 9):
        pb.progress(count)
        time.sleep(0.2)
    pb = progressbar(100)
    pb.progress(20)
    time.sleep(0.3)
    pb.progress(47)
    time.sleep(0.3)
    pb.progress(90)
    time.sleep(0.3)
    pb.progress(100)
    print("testing 1:")
    pb = progressbar(1)
    pb.progress(1)


# https://www.runoob.com/python3/python3-date-time.html
def test_4():
    scale = 50

    # // 向下取整：scale // 2 = 25，即字符串的总宽度为25
    # .center() 控制输出的样式，宽度为25，汉字居中，两侧填充 -
    # 无法使左右字符数相等时候，字符串字符数为奇数时左侧字符会比右侧少 1，字符串字符数为偶数时左侧字符会比右侧多 1
    print("执行开始".center(scale // 2, "-"))

    # 调用一次 perf_counter()，从计算机系统里随机选一个时间点A，计算其距离当前时间点B1有多少秒。
    # 当第二次调用该函数时，默认从第一次调用的时间点A算起，距离当前时间点B2有多少秒。
    # 两个函数取差，即实现从时间点B1到B2的计时功能。
    start = time.perf_counter()

    for i in range(scale + 1):
        a = '*' * i  # i 个长度的 * 符号
        b = '.' * (scale - i)  # scale-i） 个长度的 . 符号。符号 * 和 . 总长度为50
        c = (i / scale) * 100  # 显示当前进度，百分之多少
        dur = time.perf_counter() - start  # 计时，计算进度条走到某一百分比的用时

        # \r用来在每次输出完成后，将光标移至行首，这样保证进度条始终在同一行输出，即在一行不断刷新的效果；
        # {:^3.0f}，输出格式为居中，占3位，小数点后0位，浮点型数，对应输出的数为c；
        # {}，对应输出的数为a；{}，对应输出的数为b；{:.2f}，输出有两位小数的浮点数，对应输出的数为dur；
        # end=''，用来保证不换行，不加这句默认换行。
        print("\r{:^3.0f}%[{}->{}]{:.2f}s".format(c, a, b, dur), end='')

        time.sleep(0.1)  # 在输出下一个百分之几的进度前，停止0.1秒

    print("\n" + "执行结果".center(scale // 2, '-'))


def test_5():
    # pip install progress
    from progress.bar import Bar

    bar = Bar('进度条2', max=100)
    for i in range(100):
        time.sleep(0.1)
        bar.next()
    bar.finish()


def test_6():
    # pip install progressbar
    from progressbar import ProgressBar
    pbar = ProgressBar().start()
    for i in range(100):
        time.sleep(0.01)
        pbar.update(int((i / (100 - 1)) * 100))
    pbar.finish()


def test_7():
    # pip install alive_progress
    from alive_progress import alive_bar

    items = range(100)
    with alive_bar(len(items)) as bar:
        for i in items:
            time.sleep(0.1)
            bar()


def test_8():
    # pip install tqdm
    import tqdm

    for i in tqdm.tqdm(range(10000)):
        pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
