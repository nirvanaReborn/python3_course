#!/usr/bin/env python3
# -*- coding:utf-8 -*-

'''
https://qastack.cn/raspberrypi/8077/how-can-i-lower-the-usage-of-cpu-for-this-python-program
当我运行它时，我的CPU达到了100％！
打印，通常是非常昂贵的操作。写入文件要便宜得多。

您也可以尝试nice -n 19 python myscript.py。
nice是一个* nix实用程序，用于设置任务的CPU优先级。19是最大的权重，因此是最慢的设置。
'''

import time


def test_1():
    while True:
        print("me again...")


def test_2():
    while True:
        range(10000)  # some payload code
        print("Me again")  # some console logging
        time.sleep(0.2)  # sane sleep time of 0.1 seconds


def main():
    import sys
    sys.path.append("../../..")
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
