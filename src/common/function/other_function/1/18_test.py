#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# http://www.sohu.com/a/234896064_752099
# 3道经典Python题，9种绝妙解法，你能想出几种？
'''
3.找出下一个大的数字
题目：
给定一个数字比如138，找出这个数字组合后下一个大的数字，比如138,183,318,813,831,很显然下一个大一点的数字是183。
'''


'''
1).屌丝解法：
我们用最简单的暴力破解，先把数字转为字符列表，然后排列组合所有的情况，再把字符转给数字，然后排序，取比原来数字大的那一个就行了。
首先我们利用itertool库里面的牛逼闪闪的permutations，可以对一个列表进行全排序
接着我们把排序好的字符里面，用迭代器合成为一个一个数字
把数字列表进行从大到小的排序，找出比当前数字大的数字，即index+1
如果没有报错，则返回为-1
'''


def test_1():
    from itertools import permutations
    def get_nums(n):
        for m in permutations(list(str(n)), len(str(n))):
            yield int(''.join(m))

    def next_bigger(n):
        if len(str(n)) == 1:
            return -1
        nums = get_nums(n)
        sorted_nums = sorted(set(nums))
        try:
            index = sorted_nums.index(n)
            return sorted_nums[index + 1]
        except Exception as e:
            return -1

    # 我们用assert测一下看看：
    assert next_bigger(50) == -1
    assert next_bigger(13) == 31
    assert next_bigger(217) == 271
    assert next_bigger(16318) == 16381


'''
2).优化算法，进阶解法
既然全排序不行，那就要想一个算法来解决这个问题，比如16318，显然16381是比它大的，因为我是倒着来的，个位数比十位数大，那么取反之后就可以得到一个比原来大的数字。所以我们的思路：
我们是反向操作：从n个位开始依次和n+1(十位)，n+2(百位)，n+3(千位)，n+4(万位)...进行比较
如果发现有大的数字，把n插入到n+1那里，同时把原来的n挪走，得到一个新的数字，放入我们的新列表
个位循环完毕，循环十位，依次重复上面的算法
最后在新的列表中找出最小的那个数字就行了！
'''


def test_2():
    def is_bigger(a, b):
        return True if a > b else False

    def run(num):
        contain = []
        num_str = list(str(num))
        for i in range(1, len(num_str) + 1):
            for j in range(i + 1, len(num_str) + 1):
                if is_bigger(num_str[-i], num_str[-j]):
                    num_str.insert(-j, num_str[-i])
                    head = num_str[:-j]
                    tail = num_str[-j:]
                    tail.pop()
                    num_str = head + sorted(tail, reverse=False)
                    new_num = int(''.join(num_str))
                    contain.append(new_num)
                    num_str = list(str(num))
                    break
        return contain

    def next_bigger(n):
        return min(run(n)) if run(n) else -1


# 神一样的解法
def test_3():
    # 这道题的解法的作者是：pavel.koshev, dumbParsifal, JohnnyJiaJia
    def next_bigger(n):
        n = str(n)[::-1]
        try:
            i = min(i + 1 for i in range(len(n)) if n[i] > n[i + 1])
            j = n[:i].index(min([a for a in n[:i] if a > n[i]]))
            return int(n[i + 1::][::-1] + n[j] + ''.join(sorted(n[j + 1:i + 1] + n[:j])))
        except:
            return -1


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
