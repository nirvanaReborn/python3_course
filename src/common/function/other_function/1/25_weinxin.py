#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a7022150826340729381
# Python发送微信消息（文字、图片、文件）给指定好友和微信群

import pyautogui
import time
import win32api
import win32con
import win32gui
import win32clipboard as w


def FindWindow(chatroom):
    win = win32gui.FindWindow(None, chatroom)
    print("找到窗口句柄：%x" % win)
    if win != 0:
        win32gui.ShowWindow(win, win32con.SW_SHOWMINIMIZED)
        win32gui.ShowWindow(win, win32con.SW_SHOWNORMAL)
        win32gui.ShowWindow(win, win32con.SW_SHOW)
        win32gui.SetWindowPos(win, win32con.HWND_TOP, 0, 0, 500, 700, win32con.SWP_SHOWWINDOW)
        win32gui.SetForegroundWindow(win)  # 获取控制
        time.sleep(1)
        tit = win32gui.GetWindowText(win)
        print('已启动【' + str(tit) + '】窗口')
    else:
        print('找不到【%s】窗口' % chatroom)
        exit()


# 设置和粘贴剪贴板
def ClipboardText(ClipboardText):
    w.OpenClipboard()
    w.EmptyClipboard()
    w.SetClipboardData(win32con.CF_UNICODETEXT, ClipboardText)
    w.CloseClipboard()
    time.sleep(1)
    win32api.keybd_event(17, 0, 0, 0)
    win32api.keybd_event(86, 0, 0, 0)
    win32api.keybd_event(86, 0, win32con.KEYEVENTF_KEYUP, 0)
    win32api.keybd_event(17, 0, win32con.KEYEVENTF_KEYUP, 0)


# 模拟发送动作
def SendMsg():
    win32api.keybd_event(18, 0, 0, 0)
    win32api.keybd_event(83, 0, 0, 0)
    win32api.keybd_event(18, 0, win32con.KEYEVENTF_KEYUP, 0)
    win32api.keybd_event(83, 0, win32con.KEYEVENTF_KEYUP, 0)


# 模拟发送微信文本消息
def SendWxMsg(wxid, sendtext):
    # 先启动微信
    FindWindow('微信')
    time.sleep(1)
    # 定位到搜索框
    pyautogui.moveTo(143, 39)
    pyautogui.click()
    # 搜索微信窗口
    ClipboardText(wxid)
    time.sleep(1)
    # 进入聊天窗口
    pyautogui.moveTo(155, 120)
    pyautogui.click()
    # 粘贴文本内容
    ClipboardText(sendtext)
    # 发送
    SendMsg()
    print('已发送')
    # 关闭微信窗口
    time.sleep(1)
    pyautogui.moveTo(683, 16)
    pyautogui.click()


# 模拟发送文件消息（图片、文档、压缩包等）
def SendWxFileMsg(wxid, imgpath):
    # 先启动微信
    FindWindow('微信')
    time.sleep(1)
    # 定位到搜索框
    pyautogui.moveTo(143, 39)
    pyautogui.click()
    # 搜索微信
    ClipboardText(wxid)
    time.sleep(1)
    # 进入聊天窗口
    pyautogui.moveTo(155, 120)
    pyautogui.click()
    # 选择文件
    pyautogui.moveTo(373, 570)
    pyautogui.click()
    ClipboardText(imgpath)
    time.sleep(1)
    pyautogui.moveTo(784, 509)
    pyautogui.click()
    # 发送
    SendMsg()
    print('已发送')
    # 关闭微信窗口
    time.sleep(1)
    pyautogui.moveTo(683, 16)
    pyautogui.click()


# 转发群里最新的一条消息
def ZhuanfaMsg(wxid, groupname):
    # 先启动微信
    FindWindow('微信')
    time.sleep(1)
    # 定位到搜索框
    pyautogui.moveTo(143, 39)
    pyautogui.click()
    # 搜索群
    ClipboardText(groupname)
    time.sleep(1)
    # 进入群窗口
    pyautogui.moveTo(155, 120)
    pyautogui.click()
    # 开始转发
    pyautogui.moveTo(484, 439)
    time.sleep(1)
    pyautogui.rightClick()
    pyautogui.moveTo(543, 454)
    time.sleep(1)
    pyautogui.click()
    # 搜索用户
    ClipboardText(wxid)
    time.sleep(1)
    pyautogui.moveTo(828, 406)
    pyautogui.click()
    time.sleep(1)
    # 确定转发
    pyautogui.moveTo(1108, 755)
    pyautogui.click()


def test_1():
    # 发送文本消息（微信号或微信昵称或备注，需要发送的文本消息）
    SendWxMsg('Zheng_1994-5', 'Python发送微信消息')


def test_2():
    # 发送文件消息（图片、文档、压缩包等）
    SendWxFileMsg('Zheng_1994-5', r"C:\Users\TANKING\Desktop\cbzqx77.jpg")


def test_3():
    # 转发群里最新的一条消息（微信号或微信昵称或备注，群名称）
    ZhuanfaMsg('Zheng_1994-5', '里客云科技')


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
