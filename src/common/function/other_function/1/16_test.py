#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/a6565000792661557773/
# 3道经典Python题，9种绝妙解法，你能想出几种？

'''
题目要求：
你得到一个可能混合大小写字母的字符串，你的任务是把该字符串转为仅使用小写字母或者大写字母，为了尽可能少的改变：
如果字符串包含的大字母数小于等于小写字母数，则把字符串转为小写。
如果大写的数目大于小写字母数，则把字符串转为全大写。
比如：
solve('coDe')=="code"
solve("CODe")=="CODE"
'''


'''
1).屌丝解法：
对字符串中的每个字符进行判断大写还是小写
大写则计数+1，小写也+1
比较两种计数的大小，然后对字符串进行大小写反转
'''


def test_1(str_test):
    def solve(s):
        upper_count = 0
        lower_count = 0
        for i in s:
            if i.isupper():
                upper_count += 1
            elif i.islower():
                lower_count += 1

        if upper_count > lower_count:
            return s.upper()
        else:
            return s.lower()

    print(solve(str_test))


'''
2).进阶解法：
稍微把算法优化一下，我们对小写的取-1，大写的取1，然后计算和。
大于0则全部大写，否则则全部小写
'''


def test_2(str_test):
    def check_character(c):
        return -1 if c.islower() else 1

    def solve(s):
        nums = list(map(check_character, s))
        return s.upper() if sum(nums) > 0 else s.lower()

    print(solve(str_test))


'''
3).高手解法：
这种解法绝对让人拍案叫绝，值得思考。
'''


def test_3(str_test):
    def solve(s):
        upper = sum(c.isupper() for c in s)
        lower = sum(c.islower() for c in s)
        return [s.lower(), s.upper()][upper + lower]

    print(solve(str_test))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
