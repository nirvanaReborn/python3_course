#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 获取当前机器IP

import pprint


def test_1():
    '''使用 socket 模块（推荐）'''
    import socket

    def get_ip_address():
        hostname = socket.gethostname()
        ip_address = socket.gethostbyname(hostname)
        return ip_address

    print("IP Address:", get_ip_address())


def test_2():
    '''使用 socket 模块并连接到公网IP（更加可靠）'''
    import socket

    def get_ip_address():
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            # The IP address should be a public one such as Google's DNS server
            s.connect(("8.8.8.8", 80))
            ip_address = s.getsockname()[0]
        except Exception:
            ip_address = "Unable to get IP address"
        finally:
            s.close()
        return ip_address

    print("IP Address:", get_ip_address())


def test_3():
    '''使用 netifaces 模块'''
    # pip install netifaces
    import netifaces

    def get_ip_address():
        interfaces = netifaces.interfaces()
        for iface in interfaces:
            addrs = netifaces.ifaddresses(iface)
            # Check for IPv4 addresses
            if netifaces.AF_INET in addrs:
                for addr in addrs[netifaces.AF_INET]:
                    # Skip localhost addresses
                    if addr['addr'] != '127.0.0.1':
                        return addr['addr']
        return "Unable to get IP address"

    print("IP Address:", get_ip_address())


def main():
    dict_choice = {}
    dict_description = {}
    for i in range(1, 5):
        dict_choice[str(i)] = "test_{}()".format(str(i))
        dict_description[i] = eval("test_" + str(i)).__doc__
    pprint.pprint(dict_description)

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
