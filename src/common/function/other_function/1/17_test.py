#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# http://www.sohu.com/a/234896064_752099
# 3道经典Python题，9种绝妙解法，你能想出几种？
'''
题目要求：
写一个函数检验数独是否完成：
如果完成，返回 “Finished!”
如果未完成，返回 “Try again!”

数独规则: 数独为9行9列。
每一行和每一列均由 [1-9] 9个不重复数字组成。
将 9行x9列 的数独分割为9个小区域，每个区域3行3列，且保证每个小区域数字也是从[1-9] 9 个不重复数组成。
'''


'''
屌丝解题思路：
1).可以将上边列表看成一个矩阵图形
2).判断列是否满足条件
3).最后判断行和列是否都满足条件
4).怎么分成9个小块呢
'''


def test_1():
    board = [
        [1, 3, 2, 5, 7, 9, 4, 6, 8],
        [4, 9, 8, 2, 6, 1, 3, 7, 5],
        [7, 5, 6, 3, 8, 4, 2, 1, 9],
        [6, 4, 3, 1, 5, 8, 7, 9, 2],
        [5, 2, 1, 7, 9, 3, 8, 4, 6],
        [9, 8, 7, 4, 2, 6, 5, 3, 1],
        [2, 1, 4, 9, 3, 5, 6, 8, 7],
        [3, 6, 5, 8, 1, 7, 9, 2, 4],
        [8, 7, 9, 6, 4, 2, 1, 3, 5]
    ]

    import numpy as np

    def done_or_not(board):
        board2 = map(list, zip(*board))
        if sum([len(set(row)) for row in board2]) == 81 and sum([len(set(row)) for row in board]) == 81:
            board_array = np.array(board)
            x = [0, 3, 6, 9]
            for y in range(0, len(x) - 1):
                for j in range(0, len(x) - 1):
                    z = board_array[x[y]:x[y + 1], x[j]:x[j + 1]]
                    if len(set([num for row in z for num in row])) != 9:
                        return 'Try again!'
                    else:
                        return 'Finished!'
        else:
            return 'Try again!'


'''
看高手的解法:涉及到多维数组的处理，可以直接用numpy里面的vstack来处理，会简洁和方便
'''


def test_2():
    import numpy as np
    def done_or_not(aboard):  # board[i][j]
        board = np.array(aboard)
        rows = [board[i, :] for i in range(9)]  # 取行
        cols = [board[:, j] for j in range(9)]  # 取列
        sqrs = [board[i:i + 3, j:j + 3].flatten() for i in [0, 3, 6] for j in [0, 3, 6]]  # 分区
        for view in np.vstack((rows, cols, sqrs)):
            if len(np.unique(view)) != 9:
                return 'Try again!'
            else:
                return 'Finished!'


'''
神一样的解法:在上面用numpy的基础上，进一步巧妙的利用not all的用法
'''


def test_3():
    import numpy as np
    def done_or_not(board):  # board[i][j]
        board = np.array(board)
        rows = np.array(board)
        cols = np.transpose(rows)
        subs = [np.ndarray.flatten(rows[x:x + 3, y:y + 3]) for x in range(0, 9, 3) for y in range(0, 9, 3)]
        for test in [rows, cols, subs]:
            if not all(sorted(group) == range(1, 10) for group in test):
                return 'Try again!'
            else:
                return 'Finished!'


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))
    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
