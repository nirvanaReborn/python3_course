#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.jb51.net/article/171619.htm
# 命令行运行Python脚本时传入参数的三种方式详解


def test_1():
    import argparse
    parser = argparse.ArgumentParser(description='manual to this script')
    parser.add_argument('--gpus', type=str, default=None)
    parser.add_argument('--batch-size', type=int, default=32)
    args = parser.parse_args()
    print(args.gpus, type(args.gpus))
    print(args.batch_size, type(args.batch_size))


def main():
    test_1()


if __name__ == "__main__":
    main()
