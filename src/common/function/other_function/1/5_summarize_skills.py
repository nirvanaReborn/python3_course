#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.toutiao.com/article/7086357480539800103
# 24 个好用到爆的 Python 实用技巧


def test_1():
    '''all or any'''
    x = [True, True, False]
    if any(x):
        print("至少有一个True")
    if all(x):
        print("全是True")
    if any(x) and not all(x):
        print("至少一个True和一个False")


def test_2():
    pass


def test_3():
    pass


def test_4():
    pass


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
