#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://chaisw.cn/blog/1919.html
# 用 Python 脚本批量检测网页是否能有效访问
# 保存脚本后，记得将.txt文件和.py文件放在同一个文件目录下，打开终端执行以下命令：python go2url.py -f url.txt

import requests, re
import urllib3
import logging

logging.captureWarnings(True)
from concurrent.futures import ThreadPoolExecutor
import argparse
import time

from requests.packages.urllib3.exceptions import InsecureRequestWarning
# import ssl
# ssl._create_default_https_context = ssl._create_unverified_context
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


def parser_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", help="指定domain文件")
    return parser.parse_args()


def check_url(url):
    header = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36', }

    try:
        res = requests.get(url, headers=header, verify=False, allow_redirects=True, timeout=10)
        code = res.status_code
    except Exception as error:
        code = "无法访问"

    result_code = str(code)
    if result_code != "无法访问":
        try:
            urllib3.disable_warnings()
            res = requests.get(url, headers=header, verify=False, allow_redirects=True, timeout=10)
            res.encoding = res.apparent_encoding
            title = re.findall("(?<=\<title\>)(?:.|\n)+?(?=\<)", res.text, re.IGNORECASE)[0].strip()
        except:
            title = "[ ]"
        content = url + "," + res.url + "," + result_code + "," + title
        print(content)
    else:
        title = " "
        content = url + "," + " " + "," + result_code + "," + title
        print(content)

    with open("result.csv", "a", encoding='utf-8') as f:
        f.write(content + '\n')


if __name__ == '__main__':
    start = time.time()
    with open("result.csv", "a", encoding='utf-8') as f:
        f.write("源地址" + "," + "跳转地址" + "," + "状态码" + "," + "标题" + '\n')

    a = vars(parser_args())
    file = a['file']
    try:
        with ThreadPoolExecutor(max_workers=100) as executor:
            for i in open(file, errors="ignore").readlines():
                executor.submit(check_url, i.strip().strip('\\'))
    except:
        print('-f 指定domain文件')
    end = time.time()
    print("总耗时:", end - start, "秒")
