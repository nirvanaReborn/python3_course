#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# 获取操作系统位数

import pprint


def test_1():
    '''使用 platform 模块判断当前运行的 Python 程序是 32 位还是 64 位'''
    import platform

    def is_64bit_python():
        return platform.architecture()[0] == '64bit'

    if is_64bit_python():
        print("这是一个 64 位 Python 程序")
    else:
        print("这是一个 32 位 Python 程序")


def test_2():
    '''使用 sys 模块判断当前运行的 Python 程序是 32 位还是 64 位'''
    import sys

    def is_64bit_python():
        return sys.maxsize > 2 ** 32

    if is_64bit_python():
        print("这是一个 64 位 Python 程序")
    else:
        print("这是一个 32 位 Python 程序")


def test_3():
    '''使用 ctypes 模块判断当前运行的 Python 程序是 32 位还是 64 位'''
    import ctypes

    def is_64bit_python():
        return ctypes.sizeof(ctypes.c_void_p) == 8

    if is_64bit_python():
        print("这是一个 64 位 Python 程序")
    else:
        print("这是一个 32 位 Python 程序")


def test_4():
    '''使用 struct 模块判断当前运行的 Python 程序是 32 位还是 64 位'''
    import struct

    def is_64bit_python():
        return struct.calcsize("P") * 8 == 64

    if is_64bit_python():
        print("这是一个 64 位 Python 程序")
    else:
        print("这是一个 32 位 Python 程序")


def main():
    dict_choice = {}
    dict_description = {}
    for i in range(1, 5):
        dict_choice[str(i)] = "test_{}()".format(str(i))
        dict_description[i] = eval("test_" + str(i)).__doc__
    pprint.pprint(dict_description)

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
