#!/usr/bin/env python
# -*- coding:utf-8 -*-

import codecs
import collections
import glob
import os
import pprint
import re


def get_public_function(source_dir):
    dict_function = collections.defaultdict(list)
    list_file = glob.glob(os.path.join(source_dir, "*.py"))
    for file in list_file:
        with codecs.open(file, "r", "utf-8") as fr:
            for fileLine in fr.readlines():
                if str(fileLine).startswith("class"):
                    list_result = re.findall("class (.*?)\(", fileLine, re.S)
                    if list_result:
                        # print(list_result)
                        if "main" in list_result:
                            list_result.remove("main")
                        if "__init__" in list_result:
                            list_result.remove("__init__")
                        dict_function[os.path.splitext(os.path.basename(file))[0]].extend(list_result)
                    else:
                        print("没有函数", file)
    return dict_function


def generate_public_function(dict_function):
    str_code = ""
    for key in dict_function:
        if dict_function[key]:
            str_code += "from " + key + " import " + ", ".join(dict_function[key]) + "\n"

    return str_code


def get_file_path(key_filename):
    source_dir = os.path.abspath(r"../../../../../../")

    # 遍历source_dir下所有子文件,并过滤关键文件
    list_file = []
    for parent, dirnames, filenames in os.walk(source_dir):
        for filename in filenames:
            source_file = os.path.join(parent, filename).encode('utf-8').decode('utf-8')
            # 文件不为空
            if os.path.getsize(source_file):
                if ((key_filename != None and key_filename == os.path.basename(source_file))
                        or key_filename == None):
                    list_file.append(source_file)
            else:
                print("空文件:%s" % source_file)
                continue

    pprint.pprint(list_file)
    return list_file


def main():
    source_dir = r"..\..\..\..\public_resource\public_class"
    str_code = generate_public_function(get_public_function(source_dir))
    print(str_code)
    get_file_path("public_function.py")


if __name__ == "__main__":
    main()
