#!/usr/bin/env python
# -*- coding:utf-8 -*-

import glob
# http://blog.csdn.net/jwentao01/article/details/51867114
# 实现简单的ftp与sftp日志上传功能
import operator
import os
import platform
import time
from ftplib import FTP

import paramiko

from public_function import GLOBAL_WORK_DIR
from public_function import GetConfigInfo


# 获取本地时间
def get_localtime():
    ISOTIMEFORMAT = '%Y-%m-%d %X'
    return time.strftime(ISOTIMEFORMAT, time.localtime())


# 读写文件，此处设置只有'r'，才进行读操纵
# 文件和目录不存在就创建，以防出现读取和写入空文件
def rw_file(dict_uesr_info, filename, string, type):
    log_storage_path = dict_uesr_info['log_storage_path']
    content = ''
    flag = False
    # fd = io.TextIOWrapper(1)
    try:
        if not os.path.exists(log_storage_path):
            os.makedirs(log_storage_path)
        file_path = os.path.join(log_storage_path, filename)
        if not platform.system() == 'Windows':
            if not os.path.exists(file_path):
                # mknod用于创建一个指定文件名的文件系统节点
                os.mknod(file_path)

        fd = open(file_path, type)
        if type == 'r':
            content = fd.read()
        else:
            fd.write(string)
        flag = True
        fd.close()
    except Exception as e:
        str_con_abnormal = log_str_title(2, dict_uesr_info) + str(e) + '\n'
        rw_file(dict_uesr_info, dict_uesr_info['log_file'], str_con_abnormal, 'a+')
    finally:
        # 永远要执行的， 尽管出错了
        return flag, content


# 获取本地文件，使用glob模块，可以从目录中过滤出所需要的文件
# glob会将文件的路径一同获取出来
# glob获取不会根据文件名排序，可用list自带的函数sort()进行排序，默认从小到大排序
def get_file(dict_uesr_info):
    filelist = []  # save all file
    ftplist = []  # save ftp file
    get_path = dict_uesr_info['localpath']
    log_storage_path = dict_uesr_info['log_storage_path']
    log_file = dict_uesr_info['log_file']
    log_file_match = dict_uesr_info['log_file_match']
    if not os.path.exists(get_path):
        str_dir_abnormal = (log_str_title(2, dict_uesr_info) +
                            +get_path + ' ' + 'dir get_path:localpath not exist' + '\n')
        rw_file(dict_uesr_info, log_file, str_dir_abnormal, 'a+')
        return ftplist

    strsucc, file_match = rw_file(dict_uesr_info, log_file_match, '', 'r')

    filelist = glob.glob(os.path.join(get_path, "*_11211_*.tar.gz"))
    # print(filelist)
    if len(filelist) == 0:
        str_dir_abnormal = (log_str_title(2, dict_uesr_info) + get_path + ' ' + 'dir is null' + '\n')
        rw_file(dict_uesr_info, log_file, str_dir_abnormal, 'a+')
        return ftplist

    for filetemp in filelist:
        file = os.path.basename(filetemp)
        # if cmp(file[-23:-7], file_match) == 1:
        if operator.eq(file[-17:-7], file_match):
            ftplist.append(file)

    ftplist.sort()
    return ftplist


# ftp连接功能，使用ftplib模块
def ftpconnect(dict_uesr_info):
    host = dict_uesr_info['ip']
    port = dict_uesr_info['port']
    user = dict_uesr_info['user']
    password = dict_uesr_info['password']
    log_file = dict_uesr_info['log_file']
    try:
        ftp = FTP()
        ftp.connect(host, port)
        ftp.login(user, password)
    except Exception as e:
        str_con_abnormal = (log_str_title(2, dict_uesr_info) + str(e) + '\n')
        rw_file(dict_uesr_info, log_file, str_con_abnormal, 'a+')
        return False
    return ftp


# ftp连接建立后的上传功能
def ftp_uploadfile(ftp, filename, dict_uesr_info):
    localpath = dict_uesr_info['localpath']
    remotepath = dict_uesr_info['remotepath']
    log_file = dict_uesr_info['log_file']
    try:
        bufsize = 1024
        fp = open(localpath + filename, 'rb')
        ftp.storbinary('STOR ' + remotepath + filename, fp, bufsize)
    except Exception as e:
        str_up_abnormal = (log_str_title(2, dict_uesr_info) + filename + ' ' + str(e) + '\n')
        rw_file(dict_uesr_info, log_file, str_up_abnormal, 'a+')
        return False
    ftp.set_debuglevel(0)
    fp.close()
    return True


# sftp连接和上传功能，使用paramiko模块
# 注意paramiko.Transport的写法，经过测试，port必须是int，字符串不可以
def sftpconnect_and_upload(dict_uesr_info, filename):
    host = dict_uesr_info['ip']
    port = dict_uesr_info['port']
    user = dict_uesr_info['user']
    password = dict_uesr_info['password']
    localpath = dict_uesr_info['localpath']
    remotepath = dict_uesr_info['remotepath']
    log_file = dict_uesr_info['log_file']
    try:
        sf = paramiko.Transport(host, int(port))
        sf.connect(username=user, password=password)
        sftp = paramiko.SFTPClient.from_transport(sf)
        sftp.put(os.path.join(localpath, filename), os.path.join(remotepath, filename))
    except Exception as e:
        str_sftp_abnormal = (log_str_title(2, dict_uesr_info) + str(e) + '\n')
        rw_file(dict_uesr_info, log_file, str_sftp_abnormal, 'a+')
        return False
    sf.close()
    return True


# 该函数调用上面的ftp与sftp函数，根据ftptype参数，选择对应的上传方式
# 每一次上传都提供三次上传机会，以防出现ftp或sftp连接异常情况
def log_ftp_or_sftp(dict_uesr_info):
    flist = get_file(dict_uesr_info)
    if len(flist) == 0:
        return

    log_file = dict_uesr_info['log_file']
    ftptype = dict_uesr_info['ftptype']
    if ftptype == 'FTP':
        num_ft = 1
        ft = ftpconnect(dict_uesr_info)
        while not ft:
            if num_ft >= 3:
                ftpconnect_fail = (log_str_title(2, dict_uesr_info) + 'three ftp fail' + '\n')
                rw_file(dict_uesr_info, log_file, ftpconnect_fail, 'a+')
                return
            ft = ftpconnect(dict_uesr_info)
            num_ft = num_ft + 1

        for file_upload in flist:
            num_fu = 1
            fu = ftp_uploadfile(ft, file_upload, dict_uesr_info)
            while not fu:
                if num_fu == 3 or num_fu > 3:
                    str_up_fail = (log_str_title(2, dict_uesr_info) + 'three upload fail' + '\n')
                    rw_file(dict_uesr_info, log_file, str_up_fail, 'a+')
                    return
                fu = ftp_uploadfile(ft, file_upload, dict_uesr_info)
                num_fu = num_fu + 1
            str_up_succ = (log_str_title(3, dict_uesr_info) + file_upload + ' ' + 'ftp success' + '\n')
            rw_file(dict_uesr_info, log_file, str_up_succ, 'a+')
            # rw_file(dict_uesr_info,log_file_match,file_upload[-23:-7],'w+')

        ft.quit()

    elif ftptype == 'SFTP':
        for file_upload in flist:
            num_sf = 1
            sftp = sftpconnect_and_upload(dict_uesr_info, file_upload)
            while not sftp:
                if num_sf >= 3:
                    str_sftp_fail = (log_str_title(2, dict_uesr_info) + 'three sftp fail' + '\n')
                    rw_file(dict_uesr_info, log_file, str_sftp_fail, 'a+')
                    return
                sftp = sftpconnect_and_upload(dict_uesr_info, file_upload)
                num_sf = num_sf + 1
            str_sftp_succ = (log_str_title(3, dict_uesr_info) + file_upload + ' ' + 'sftp success' + '\n')
            rw_file(dict_uesr_info, log_file, str_sftp_succ, 'a+')
            # rw_file(dict_uesr_info,log_file_match,file_upload[-23:-7],'w+')

    else:
        ftptype_abnormal = (log_str_title(2, dict_uesr_info) + 'ftptype is False' + '\n')
        rw_file(dict_uesr_info, log_file, ftptype_abnormal, 'a+')
        return


def log_str_title(log_level, dict_uesr_info):
    # 日志格式：时间  错误级别 localpath  ip  port  user  ftptype  remotepath
    return (get_localtime() + ' '
            + dict_log_level[log_level] + ' '
            + dict_uesr_info['ip'] + ' '
            + dict_uesr_info['ftptype'] + ' '
            + dict_uesr_info['port'] + ' '
            + dict_uesr_info['user'] + ' '
            + dict_uesr_info['remotepath'] + ' '
            + dict_uesr_info['localpath'] + ' '
            + ' ')


def main():
    global dict_log_level
    dict_log_level = {
        1: "SYS    ",
        2: "ERROR  ",
        3: "SUCCESS"
    }

    login_file = os.path.join(GLOBAL_WORK_DIR, r"获取登录信息.xml")
    dict_login_info = GetConfigInfo(login_file).get_list_config()[0]

    dict_path_info = {
        'ftptype': 'SFTP',
        'remotepath': '/home/ms/',
        'localpath': r'D:\ARESHelper',
        'log_storage_path': r'D:\ARESHelper\log',
        'log_file': 'upload.log',
        'log_file_match': 'file_match.log',
    }

    dict_uesr_info = dict(dict_login_info, **dict_path_info)
    # print(dict_uesr_info)

    str_head = log_str_title(1, dict_uesr_info) + '处理开始' + '\n'
    rw_file(dict_uesr_info, dict_uesr_info['log_file'], str_head, 'a+')

    log_ftp_or_sftp(dict_uesr_info)

    str_tail = log_str_title(1, dict_uesr_info) + '处理结束' + '\n'
    rw_file(dict_uesr_info, dict_uesr_info['log_file'], str_tail, 'a+')


if __name__ == "__main__":
    main()
