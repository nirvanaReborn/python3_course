#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.cnblogs.com/haigege/p/5517422.html
# 使用paramiko的SFTP get或put整个目录
'''
下面再来考虑下如何传输整个目录？
有两种思路：
1 如果是要get则采用已经定义的connect方法连接到linux主机，
  然后通过send方法执行tar命令将需要传输的整个目录打包，再传输打包后的文件即可，如果是put则需在本地打包。
    缺点是：在远端或者本地进行打包或者解压，并且打包会占用临时存储空间，如果是远端打包还需先SSH链接linux主机。
    优点是：不用做目录扫描处理。
2 遍历需要get或put的目录及其子目录，然后依次传输每一个文件。
    优点是：不需要SSH登陆和打包解压，
    缺点是：需要做目录扫描，但是目录扫描是很简单的，因此我们采用这种方法。
'''
import os
import stat

import paramiko

from public_function import GLOBAL_WORK_DIR
from public_function import GetConfigInfo


# 定义一个类，表示一台远端linux主机
class Linux(object):
    # 通过IP, 用户名，密码，超时时间初始化一个远程Linux主机
    def __init__(self, dict_login_info):
        self.ip = dict_login_info["ip"]
        self.username = dict_login_info["user"]
        self.password = dict_login_info["password"]
        self.timeout = int(dict_login_info["timeout"])
        self.port = int(dict_login_info["port"])
        # transport和chanel
        self.t = ''
        self.chan = ''
        # 链接失败的重试次数
        self.try_times = 3

    # get下载单个文件
    def sftp_get(self, remotefile, localfile):
        t = paramiko.Transport(self.ip, self.port)
        t.connect(username=self.username, password=self.password)
        sftp = paramiko.SFTPClient.from_transport(t)
        sftp.get(remotefile, localfile)
        t.close()

    # put上传单个文件
    def sftp_put(self, localfile, remotefile):
        t = paramiko.Transport(self.ip, self.port)
        t.connect(username=self.username, password=self.password)
        sftp = paramiko.SFTPClient.from_transport(t)
        sftp.put(localfile, remotefile)
        t.close()

    # ------获取远端linux主机上指定目录及其子目录下的所有文件------
    def __get_all_files_in_remote_dir(self, sftp, remote_dir):
        # 保存所有文件的列表
        all_files = list()

        # 去掉路径字符串最后的字符'/'，如果有的话
        if remote_dir[-1] == '/':
            remote_dir = remote_dir[0:-1]

        # listdir_attr()获取当前指定目录下的所有目录及文件，包含属性值
        files = sftp.listdir_attr(remote_dir)
        for x in files:
            # remote_dir目录中每一个文件或目录的完整路径
            filename = remote_dir + '/' + x.filename
            # 如果是目录，则递归处理该目录，这里用到了stat库中的S_ISDIR方法，与linux中的宏的名字完全一致
            if stat.S_ISDIR(x.st_mode):
                all_files.extend(self.__get_all_files_in_remote_dir(sftp, filename))
            else:
                all_files.append(filename)
        return all_files

    # 下载远端文件夹
    def sftp_get_dir(self, remote_dir, local_dir):
        t = paramiko.Transport(self.ip, self.port)
        t.connect(username=self.username, password=self.password)
        sftp = paramiko.SFTPClient.from_transport(t)

        # 获取远端linux主机上指定目录及其子目录下的所有文件
        all_files = self.__get_all_files_in_remote_dir(sftp, remote_dir)
        # 依次get每一个文件
        for x in all_files:
            filename = x.split('/')[-1]
            local_filename = os.path.join(local_dir, filename)
            print('下载\t', '文件传输中...', u'%s' % filename)
            sftp.get(x, local_filename)

    # ------获取本地指定目录及其子目录下的所有文件------
    def __get_all_files_in_local_dir(self, local_dir):
        # 保存所有文件的列表
        all_files = list()

        # 获取当前指定目录下的所有目录及文件，包含属性值
        files = os.listdir(local_dir)
        for x in files:
            # local_dir目录中每一个文件或目录的完整路径
            filename = os.path.join(local_dir, x)
            # 如果是目录，则递归处理该目录
            if os.path.isdir(x):
                all_files.extend(self.__get_all_files_in_local_dir(filename))
            else:
                all_files.append(filename)
        return all_files

    # 上传本地文件夹
    def sftp_put_dir(self, local_dir, remote_dir):
        t = paramiko.Transport(self.ip, self.port)
        t.connect(username=self.username, password=self.password)
        sftp = paramiko.SFTPClient.from_transport(t)

        # 去掉路径字符穿最后的字符'/'，如果有的话
        if remote_dir[-1] == '/':
            remote_dir = remote_dir[0:-1]

        # 获取本地指定目录及其子目录下的所有文件
        all_files = self.__get_all_files_in_local_dir(local_dir)
        # 依次put每一个文件
        for x in all_files:
            filename = os.path.split(x)[-1]
            remote_filename = remote_dir + '/' + filename
            print('上传\t', '文件传输中...', u'%s' % filename)
            sftp.put(x, remote_filename)


def test_file(ssh, dict_login_info):
    local_file = os.path.join(GLOBAL_WORK_DIR, r"test.txt")
    if dict_login_info["user"] != "root":
        remote_file = r"/home/" + dict_login_info["user"] + r"/" + os.path.basename(local_file)
    else:
        remote_file = r"/" + dict_login_info["user"] + r"/" + os.path.basename(local_file)
    # 将本地的xxoo.txt 上传到远端，并保持为xxoo.txt
    ssh.sftp_put(local_file, remote_file)

    # 将远端的xxoo.txt 下载到本地，并保存为ooxx.txt
    ssh.sftp_get(remote_file, local_file)


def test_dir(ssh, dict_login_info):
    local_path = GLOBAL_WORK_DIR
    if dict_login_info["user"] != "root":
        remote_path = r"/home/" + dict_login_info["user"] + r"/appcom"
    else:
        remote_path = r"/" + dict_login_info["user"]
    # 将远端remote_path目录中的所有文件get到本地local_path目录
    ssh.sftp_get_dir(remote_path, local_path)

    # # 将本地local_path目录中的所有文件put到远端remote_path目录
    # ssh.sftp_put_dir(remote_path, local_path)


def main():
    login_file = os.path.join(GLOBAL_WORK_DIR, r"获取登录信息.xml")
    dict_login_info = GetConfigInfo(login_file).get_list_config()[0]
    ssh = Linux(dict_login_info)

    choice = str(input("Enter a positive integer to choice: "))
    if choice == "1":
        test_file(ssh, dict_login_info)
    else:
        test_dir(ssh, dict_login_info)


if __name__ == "__main__":
    main()
