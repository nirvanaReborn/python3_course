#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/linda1000/article/details/8255771

import ftplib
import logging
import os

logging.getLogger("ftplib").setLevel(logging.WARNING)


def test_3(dict_login_info):
    def ftpconnect(ip, user, password, port, timeout):
        ftp = ftplib.FTP()
        ftp.set_debuglevel(2)  # 打开调试级别2，显示详细信息
        # ftp.encoding = 'GBK'
        ftp.connect(ip, port=port, timeout=timeout)  # 连接
        ftp.login(user, password)  # 登录，如果匿名登录则用空串代替即可
        print(ftp.getwelcome())  # 获得欢迎信息
        return ftp

    def getfile(path):
        ftp.cwd(path)
        filelist = []
        # 读取目录下的文件列表
        ftp.retrlines('LIST', filelist.append)
        for f in filelist:
            # 判断是否为目录
            if f.startswith('d'):
                # 此处的49为打印出的各文件列表信息截取，根据实际情况更改
                path_a = ftp.pwd() + '/' + f[56:] + '/'
                getfile(path_a)
                ftp.cwd('..')
            else:
                print('---------' + ftp.pwd() + '/' + f[56:])

    def downloadfile(ftp, remotepath, localpath):
        bufsize = 1024  # 设置缓冲块大小
        fp = open(localpath, 'wb')  # 以写模式在本地打开文件
        ftp.retrbinary('RETR ' + remotepath, fp.write,
                       bufsize)  # 接收服务器上文件并写入本地文件
        ftp.set_debuglevel(0)  # 关闭调试
        fp.close()  # 关闭文件

    def uploadfile(ftp, remotepath, localpath):
        bufsize = 1024
        fp = open(localpath, 'rb')
        ftp.storbinary('STOR ' + remotepath, fp, bufsize)  # 上传文件
        ftp.set_debuglevel(0)
        fp.close()

    ftp = ftpconnect(**dict_login_info)
    getfile(r"./")
    # downloadfile(ftp, "pbox_version.txt", "pbox_version.txt")
    # uploadfile(ftp, "***", "***")
    ftp.quit()


def test_2(dict_login_info):
    ftp = ftplib.FTP()
    ftp.connect(dict_login_info["ip"], dict_login_info["port"], dict_login_info["timeout"])  # 连接FTP服务器
    ftp.login(dict_login_info["user"], dict_login_info["password"])  # 登录
    print(ftp.getwelcome())  # 获得欢迎信息
    ftp.cwd('file/test')  # 设置FTP路径
    list = ftp.nlst()  # 获得目录列表
    for name in list:
        print(name)  # 打印文件名字
        path = os.path.join('d:/Document/', name)  # 文件保存路径
        f = open(path, 'wb')  # 打开要保存文件
        filename = 'RETR ' + name  # 保存FTP文件
        ftp.retrbinary(filename, f.write)  # 保存FTP上的文件
        ftp.delete(name)  # 删除FTP文件
        ftp.storbinary('STOR ' + filename, open(path, 'rb'))  # 上传FTP文件
    ftp.quit()  # 退出FTP服务器


def test_1(dict_login_info):
    ip = 'https://cmake.org/files/'
    DIRN = 'pub/mozilla.org/webtools'
    FILE = 'bugzilla-3.6.7.tar.gz'

    try:
        # f = ftplib.FTP(ip, user=dict_login_info["user"], passwd=dict_login_info["password"])
        f = ftplib.FTP(ip)
    except Exception as e:
        print('ERROR1:cannot reach " %s", 原因是%s' % (ip, str(e)))
        return
    print('***Connected to ip "%s"' % ip)

    try:
        f.login()
    except ftplib.error_perm:
        print('ERROR: cannot login anonymously')
        f.quit()
        return
    print('*** Logged in as "anonymously"')

    try:
        f.cwd(DIRN)
    except ftplib.error_perm:
        print('ERRORL cannot CD to "%s"' % DIRN)
        f.quit()
        return
    print('*** Changed to "%s" folder' % DIRN)

    try:
        # 传一个回调函数给retrbinary() 它在每接收一个二进制数据时都会被调用
        f.retrbinary('RETR %s' % FILE, open(FILE, 'wb').write)
    except ftplib.error_perm:
        print('ERROR: cannot read file "%s"' % FILE)

        os.unlink(FILE)
    else:
        print('*** Downloaded "%s" to CWD' % FILE)
    f.quit()
    return


def main():
    dict_login_info = {
        "ip": "192.168.102.242",
        "user": "BN-test",
        "password": "BN-test@test",
        "timeout": 60,
        "port": 5212,
    }
    # test_1(dict_login_info)
    # test_2()
    test_3(dict_login_info)


if __name__ == '__main__':
    main()
