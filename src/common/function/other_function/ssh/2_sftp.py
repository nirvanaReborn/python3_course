#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://blog.csdn.net/tianpy5/article/details/52507324
# 使用sftp实现上传和下载功能, get或put方法每次只能传输一个文件

import logging

import paramiko

logging.getLogger("paramiko").setLevel(logging.WARNING)
import os
import pprint
from public_function import GetConfigInfo
from public_function import GLOBAL_WORK_DIR


def sftp_upload(dict_login_info, local, remote):
    sf = paramiko.Transport(dict_login_info["ip"], int(dict_login_info["port"]))
    sf.connect(username=dict_login_info["user"], password=dict_login_info["password"])
    sftp = paramiko.SFTPClient.from_transport(sf)
    try:
        if os.path.isdir(local):  # 判断本地参数是目录还是文件
            for file in os.listdir(local):  # 遍历本地目录
                sftp.put(os.path.join(local + file), os.path.join(remote + file))  # 上传目录中的文件
        else:
            sftp.put(local, remote)  # 上传文件
    except Exception as e:
        print('upload exception:', e)
    sf.close()


def sftp_download(dict_login_info, local, remote):
    sf = paramiko.Transport(dict_login_info["ip"], int(dict_login_info["port"]))
    sf.connect(username=dict_login_info["user"], password=dict_login_info["password"])
    sftp = paramiko.SFTPClient.from_transport(sf)
    try:
        if os.path.isdir(local):  # 判断本地参数是目录还是文件
            for file in sftp.listdir(remote):  # 遍历远程目录
                sftp.get(os.path.join(remote + file), os.path.join(local + file))  # 下载目录中文件
        else:
            sftp.get(remote, local)  # 下载文件
    except Exception as e:
        print('download exception:', e)
    sf.close()


def main():
    source_file = os.path.join(GLOBAL_WORK_DIR, r"获取登录信息.xml")
    dict_login_info = GetConfigInfo(source_file).get_list_config()[0]
    pprint.pprint(dict_login_info)
    local = r'D:\ARESHelper\output_test.log'  # 本地文件或目录，与远程一致，当前为windows目录格式，window目录中间需要使用双斜线
    remote = r'/home/ms/output_test.log'  # 远程文件或目录，与本地一致，当前为linux目录格式
    # sftp_upload(dict_login_info, local, remote)
    sftp_download(dict_login_info, local, remote)


if __name__ == '__main__':
    main()
