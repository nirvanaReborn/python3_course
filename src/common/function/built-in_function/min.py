#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# min() 函数: 返回给定参数的最小值，参数可以为序列。

def main():
    print("min(80, 100, 1000) : ", min(80, 100, 1000))
    print("min(-20, 100, 400) : ", min(-20, 100, 400))
    print("min(-80, -20, -10) : ", min(-80, -20, -10))
    print("min([0, 100, -400]) : ", min([0, 100, -400]))
    print("min(['a', 'b', 'c']) : ", min(['a', 'b', 'c']))


if __name__ == "__main__":
    main()
