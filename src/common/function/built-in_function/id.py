#!/usr/bin/env python
# -*- coding:utf-8 -*-


# http://www.runoob.com/python3/python3-built-in-functions.html
# id() 函数: 用于获取对象的内存地址。

def main():
    a = 'runoob'
    print(id(a))

    b = 1
    print(id(b))


if __name__ == "__main__":
    main()
