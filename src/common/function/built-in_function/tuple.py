#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# tuple() 函数：将列表转换为元组

def main():
    list1 = ['Google', 'Taobao', 'Runoob', 'Baidu']
    tuple1 = tuple(list1)
    print(tuple1)
    # ('Google', 'Taobao', 'Runoob', 'Baidu')


if __name__ == "__main__":
    main()
