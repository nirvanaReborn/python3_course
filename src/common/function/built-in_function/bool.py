#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# bool() 函数

'''
bool() 函数用于将给定参数转换为布尔类型，如果没有参数，返回 False。
bool 是 int 的子类。
'''


def test_1():
    '''判断是否为闰年'''
    year = int(input('请输入年份: '))
    # 如果代码太长写成一行不便于阅读 可以使用\对代码进行折行
    is_leap = year % 4 == 0 and year % 100 != 0 or year % 400 == 0
    print(is_leap)


def main():
    print(bool())  # False
    print(bool(0))  # False
    print(bool(1))  # True
    print(issubclass(bool, int))  # True


if __name__ == "__main__":
    main()
