#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# oct() 函数: 将一个整数转换成8进制字符串。

def main():
    print(oct(10))


if __name__ == "__main__":
    main()
