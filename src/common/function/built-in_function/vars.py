#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# vars() 函数：返回对象object的属性和属性值的字典对象。

class Runoob:
    a = 1


def main():
    print(vars())
    print(vars(Runoob))

    runoob = Runoob()
    print(vars(runoob))


if __name__ == "__main__":
    main()
