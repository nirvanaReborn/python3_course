#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# str() 函数


def main():
    dict = {'runoob': 'runoob.com', 'google': 'google.com'}
    print(str(dict))


if __name__ == "__main__":
    main()
