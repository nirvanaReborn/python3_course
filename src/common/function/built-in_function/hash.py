#!/usr/bin/env python
# -*- coding:utf-8 -*-


# http://www.runoob.com/python3/python3-built-in-functions.html
# hash() 函数: 获取取一个对象（字符串或者数值等）的哈希值。
# 哈希算法将任意长度的二进制值映射为固定长度的较小二进制值，这个小的二进制值称为哈希值。
# 哈希值是一段数据唯一且极其紧凑的数值表示形式。
# HASH值还有文件校验的功能,相当于文件的校验码.

def main():
    print(hash('test'))  # 字符串
    print(hash(1))  # 数字
    print(hash(str([1, 2, 3])))  # 集合
    print(hash(str(sorted({'1': 1}))))  # 字典


if __name__ == "__main__":
    main()
