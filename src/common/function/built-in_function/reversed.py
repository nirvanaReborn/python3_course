#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# reversed() 函数: 返回一个反转的迭代器。

def main():
    # 字符串
    seqString = 'Runoob'
    print(list(reversed(seqString)))

    # 元组
    seqTuple = ('R', 'u', 'n', 'o', 'o', 'b')
    print(list(reversed(seqTuple)))

    # range
    seqRange = range(5, 9)
    print(list(reversed(seqRange)))

    # 列表
    seqList = [1, 2, 4, 3, 5]
    print(list(reversed(seqList)))


if __name__ == "__main__":
    main()
