#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# set() 函数: 创建一个无序不重复的元素集合，可进行关系测试，删除重复数据，还可以计算交集、差集、并集等。

def main():
    x = set('runoob')
    y = set('google')
    print(x, y)
    # (set(['b', 'r', 'u', 'o', 'n']), set(['e', 'o', 'g', 'l']))  # 重复的被删除

    print(x & y)  # 交集
    # set(['o'])

    print(x | y)  # 并集
    # set(['b', 'e', 'g', 'l', 'o', 'n', 'r', 'u'])

    print(x - y)  # 差集
    # set(['r', 'b', 'u', 'n'])


if __name__ == "__main__":
    main()
