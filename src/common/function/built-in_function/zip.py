#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# zip() 函数
'''
zip() 函数用于将可迭代的对象作为参数，将对象中对应的元素打包成一个个元组，然后返回由这些元组组成的列表。
如果各个迭代器的元素个数不一致，则返回列表长度与最短的对象相同，利用 * 号操作符，可以将元组解压为列表。
'''


def main():
    a = [1, 2, 3]
    b = [4, 5, 6]
    c = [4, 5, 6, 7, 8]
    zipped = list(zip(a, b))  # 打包为元组的列表
    print(zipped)  # [(1, 4), (2, 5), (3, 6)]

    print(list(zip(a, c)))
    # 元素个数与最短的列表一致
    #  [(1, 4), (2, 5), (3, 6)]

    print(list(zip(*zipped)))
    # 与 zip 相反，可理解为解压，返回二维矩阵式(转置)
    # [(1, 2, 3), (4, 5, 6)]

    # ----------------------------------------------
    english = 'Monday', 'Tuesday', 'Wednesday'
    french = 'Lundi', 'Mardi', 'Mercredi'
    print(list(zip(english, french)))
    print(dict(zip(english, french)))


if __name__ == "__main__":
    main()
