#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# callable() 函数

'''
callable() 函数用于检查一个对象是否是可调用的。
如果返回True，object仍然可能调用失败；但如果返回False，调用对象ojbect绝对不会成功。
对于函数, 方法, lambda函式, 类, 以及实现了 __call__ 方法的类实例, 它都返回 True。
'''


class B:
    def __call__(self):
        return 0


class A:
    def method(self):
        return 0


def add(a, b):
    return a + b


def main():
    print(callable(0))  # 返回 False

    print(callable("runoob"))  # 返回 False

    print(callable(add))  # 返回 True

    print(callable(A))  # 返回 True
    a = A()
    print(callable(a))  # 没有实现 __call__, 返回 False

    print(callable(B))  # 返回 True
    b = B()
    print(callable(b))  # 返回 True


if __name__ == "__main__":
    main()
