#!/usr/bin/env python
# -*- coding:utf-8 -*-


# http://www.runoob.com/python3/python3-built-in-functions.html
# format()格式化函數
# format 函数可以接受不限个参数，位置可以不按顺序。
'''
^, <, > 分别是居中、左对齐、右对齐，后面带宽度
: 号后面带填充的字符，只能是一个字符，不指定则默认是用空格填充
+ 表示在正数前显示 +，负数前显示 -
（空格）表示在正数前加空格
b、d、o、x 分别是二进制、十进制、八进制、十六进制
'''


def main():
    print("{} {}".format("hello", "world"))  # 不设置指定位置，按默认顺序
    print("{0} {1}".format("hello", "world"))  # 设置指定位置
    print("{1} {0} {1}".format("hello", "world"))  # 设置指定位置

    print("网站名：{name}, 地址 {url}".format(name="菜鸟教程", url="www.runoob.com"))  # 也可以设置参数

    # 通过字典设置参数
    site = {"name": "菜鸟教程", "url": "www.runoob.com"}
    print("网站名：{name}, 地址 {url}".format(**site))

    # 通过列表索引设置参数
    my_list = ['菜鸟教程', 'www.runoob.com']
    print("网站名：{0[0]}, 地址 {0[1]}".format(my_list))  # "0" 是可选的

    # 也可以向 str.format() 传入对象：
    class AssignValue(object):
        def __init__(self, value):
            self.value = value

    my_value = AssignValue(6)
    print('value 为: {0.value}'.format(my_value))  # "0" 是可选的

    print("{:.2f}".format(3.1415926))  # 格式化数字

    print("{} 对应的位置是 {{0}}".format("runoob"))  # 可以使用大括号 {} 来转义大括号


if __name__ == "__main__":
    main()
