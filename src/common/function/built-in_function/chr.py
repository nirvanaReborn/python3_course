#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# chr() 函数

'''
chr() 用一个范围在 range（256）内的（就是0～255）整数作参数，返回一个对应的字符。
'''


def main():
    print(chr(0x30), chr(0x31), chr(0x61))  # 十六进制
    # 0 1 a

    print(chr(48), chr(49), chr(97))  # 十进制
    # 0 1 a


if __name__ == "__main__":
    main()
