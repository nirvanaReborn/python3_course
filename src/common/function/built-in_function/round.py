#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# round() 函数: 返回浮点数x的四舍五入值。

def main():
    print("round(70.23456) : ", round(70.23456))
    print("round(56.659,1) : ", round(56.659, 1))
    print("round(80.264, 2) : ", round(80.264, 2))
    print("round(100.000056, 3) : ", round(100.000056, 3))
    print("round(-100.000056, 3) : ", round(-100.000056, 3))


if __name__ == "__main__":
    main()
