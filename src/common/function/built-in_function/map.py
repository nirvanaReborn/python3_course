#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# map() 函数: 会根据提供的函数对指定序列做映射。
# map(function, iterable, ...)
# 参数
#     function -- 函数，有两个参数
#     iterable -- 一个或多个序列
#
# http://blog.csdn.net/damotiansheng/article/details/44139111
# Python3.4中filter函数，map函数和reduce函数


def f1(x, y):
    return (x, y)


def f2(x):
    return x * 2


def f3(x, y):
    return x * 2, y * 2


def main():
    list_1 = [0, 1, 2, 3, 4, 5, 6]
    list_2 = ['Sun', 'M', 'T', 'W', 'T', 'F', 'S']
    list_3 = map(f1, list_1, list_2)
    print(list(list_3))

    print(list(map(f2, list_1)))  # [0, 2, 4, 6, 8, 10, 12]
    print(list(map(f2, list_2)))  # ['SunSun', 'MM', 'TT', 'WW', 'TT', 'FF', 'SS']
    print(list(map(f3, list_1, list_2)))
    # [(0, 'SunSun'), (2, 'MM'), (4, 'TT'), (6, 'WW'), (8, 'TT'), (10, 'FF'), (12, 'SS')]


if __name__ == '__main__':
    main()
