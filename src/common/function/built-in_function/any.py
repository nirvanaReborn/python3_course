#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# any() 函数

'''
any() 函数用于判断给定的可迭代参数 iterable 是否全部为空对象，
        如果都为空、0、false，则返回 False；
        如果不都为空、0、false，则返回 True。

语法：any(iterable)
参数：iterable -- 元组或列表。
返回值：如果都为空、0、false，则返回false；如果不都为空、0、false，则返回true。


函数等价于：
def any(iterable):
    for element in iterable:
        if element:
            return True
    return False
'''


def test_1():
    list_test = ['a', 'b', 'c', 'd']
    print(any(list_test))  # True

    # 列表list，存在一个为空的元素
    list_test[2] = ''
    print(any(list_test))  # True

    # 列表list，存在一个为0的元素
    list_test[2] = 0
    print(any(list_test))  # True

    list_test.clear()
    print(any(list_test))  # False


def test_2():
    x = [True, True, False]
    if any(x):
        print("至少有一个True")
    if all(x):
        print("全是True")
    if any(x) and not all(x):
        print("至少一个True和一个False")


def main():
    test_1()
    test_2()


if __name__ == "__main__":
    main()
