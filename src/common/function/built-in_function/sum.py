#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# sum() 函数： 对系列进行求和计算。
'''
sum(iterable[, start])
参数
    iterable -- 可迭代对象，如列表。
    start -- 指定相加的参数，如果没有设置这个值，默认为0。
'''


def main():
    print(sum([0, 1, 2]))
    print(sum((2, 3, 4), 1))  # 元组计算总和后再加 1
    print(sum([0, 1, 2, 3, 4], 2))  # 列表计算总和后再加 2


if __name__ == "__main__":
    main()
