#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# len()方法: 返回对象（字符、列表、元组等）长度或项目个数。

def main():
    str_1 = "runoob-菜鸟学习"
    print(len(str_1))

    str_2 = ''  # 空字符串
    print(len(str_2))

    # str_3 = None
    # print(len(str_3))



if __name__ == "__main__":
    main()
