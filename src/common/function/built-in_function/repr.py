#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# repr() 函数: 将对象转化为供解释器读取的形式。返回一个对象的 string 格式。


def main():
    import struct
    buffer = struct.pack("ihb", 1, 2, 3)
    # 函数str() 用于将值转化为适于人阅读的形式，而repr() 转化为供解释器读取的形式。
    print(buffer)  # b'\x01\x00\x00\x00\x02\x00\x03'
    print(repr(buffer))  # b'\x01\x00\x00\x00\x02\x00\x03'
    print(struct.unpack("ihb", buffer))  # (1, 2, 3)

    dict = {'runoob': 'runoob.com', 'google': 'google.com'}
    print(repr(dict))

    source_str = r"chrome_bookmarks.json"
    print(ascii(source_str[-4:]))
    print(repr(source_str[-4:]))
    print('''{0}'''.format(source_str[-4:]))


if __name__ == "__main__":
    main()
