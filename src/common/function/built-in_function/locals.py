#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# locals() 函数: 会以字典类型返回当前位置的全部局部变量。
# 对于函数, 方法, lambda 函式, 类, 以及实现了 __call__ 方法的类实例, 它都返回 True。


def runoob(arg):
    z = 1
    print(locals())  # 返回一个名字/值对的字典


def main():
    runoob(4)  # {'z': 1, 'arg': 4}


if __name__ == "__main__":
    main()
