#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# all() 函数

'''
all() 函数用于判断给定的可迭代参数 iterable 中的所有元素是否不为 0、''、False 或者 iterable 为空，
如果是返回 True，否则返回 False。

语法：all(iterable)
参数：iterable -- 元组或列表。
注意：空元组、空列表返回值为True，这里要特别注意。

函数等价于：
def all(iterable):
    for element in iterable:
        if not element:
            return False
    return True
'''


def test_1():
    list_test = ['a', 'b', 'c', 'd']
    print(all(list_test))

    # 列表list，存在一个为空的元素
    list_test[2] = ''
    print(all(list_test))

    # 列表list，存在一个为0的元素
    list_test[2] = 0
    print(all(list_test))

    list_test.clear()
    print(all(list_test))


def test_2():
    x = [True, True, False]
    if any(x):
        print("至少有一个True")
    if all(x):
        print("全是True")
    if any(x) and not all(x):
        print("至少一个True和一个False")


def main():
    test_1()
    test_2()


if __name__ == "__main__":
    main()
