#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
'''
http://blog.csdn.net/churximi/article/details/51648388
enumerate用法总结

enumerate(sequence, [start=0])
参数
    sequence -- 一个序列、迭代器或其他支持迭代对象。
    start -- 下标起始位置。
返回值
    返回 enumerate(枚举) 对象。

enumerate()是python的内置函数
enumerate在字典上是枚举、列举的意思
对于一个可迭代的（iterable）/可遍历的对象（如列表、字符串），enumerate将其组成一个索引序列，利用它可以同时获得索引和值
enumerate多用于在for循环中得到计数
例如对于一个seq，得到：(0, seq[0]), (1, seq[1]), (2, seq[2])
'''


def test_1():
    list1 = ["这", "是", "一个", "测试"]
    for i in range(len(list1)):
        print(i, list1[i])


def test_2():
    list1 = ["这", "是", "一个", "测试"]
    for index, item in enumerate(list1):
        print(index, item)


# enumerate还可以接收第二个参数，用于指定索引起始值
def test_3():
    list1 = ["这", "是", "一个", "测试"]
    for index, item in enumerate(list1, 1):
        print(index, item)


def test_4():
    # 如果要统计文件的行数
    count = len(open("a.txt", 'r').readlines())

    # 可以利用enumerate()
    count = -1
    for index, line in enumerate(open("a.txt", 'r')):
        count += 1


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
