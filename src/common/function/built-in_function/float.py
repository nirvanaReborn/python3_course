#!/usr/bin/env python
# -*- coding:utf-8 -*-


# http://www.runoob.com/python3/python3-built-in-functions.html
# float() 函数: 用于将整数和字符串转换成浮点数。

def main():
    print(float(-123.6))
    print(float('123'))  # 字符串


if __name__ == "__main__":
    main()
