#!/usr/bin/env python
# -*- coding:utf-8 -*-


# http://www.runoob.com/python3/python3-built-in-functions.html
# input() 函数
'''
python input() 相等于 eval(raw_input(prompt)) ，用来获取控制台的输入。
raw_input() 将所有输入作为字符串看待，返回字符串类型。
而 input() 在对待纯数字输入时具有自己的特性，它返回所输入的数字的类型（ int, float ）。

注意：input() 和 raw_input() 这两个函数均能接收字符串 ，但 raw_input() 直接读取控制台的输入（任何类型的输入它都可以接收）。
而对于 input() ，它希望能够读取一个合法的 python 表达式，即你输入字符串的时候必须使用引号将它括起来，否则它会引发一个 SyntaxError 。
除非对 input() 有特别需要，否则一般情况下我们都是推荐使用 raw_input() 来与用户交互。
注意：python3 里 input() 默认接收到的是 str 类型。
'''


def get_input():
    import sys
    try:
        if len(sys.argv) > 2:
            N = int(sys.argv[1])
            M = int(sys.argv[2])
        else:
            while True:
                N = int(input("请输入行号N(1 <= N <= 1000)："))
                if 1 <= N <= 1000:
                    break
                else:
                    print("输入内容不符合约束条件！")

            while True:
                M = int(input("请输入每行字符串的个数M(1 <= M <= 1000)："))
                if 1 <= M <= 1000:
                    break
                else:
                    print("输入内容不符合约束条件！")
    except Exception as e:
        print(str(e))
    return N, M


def main():
    a = input("input:")
    print(type(a))  # <class 'str'>
    N, M = get_input()
    print(N, M)


if __name__ == "__main__":
    main()
