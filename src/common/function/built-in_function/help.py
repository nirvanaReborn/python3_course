#!/usr/bin/env python
# -*- coding:utf-8 -*-


# http://www.runoob.com/python3/python3-built-in-functions.html
# help() 函数:用于查看函数或模块用途的详细说明。


def test_1():
    help('sys')  # 查看 sys 模块的帮助


def test_2():
    help('str')


def test_3():
    a = [1, 2, 3]
    help(a)


def test_4():
    a = [1, 2, 3]
    help(a.append)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
