#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# max() 函数: 返回给定参数的最大值，参数可以为序列。


def main():
    print("max(80, 100, 1000) : ", max(80, 100, 1000))
    print("max(-20, 100, 400) : ", max(-20, 100, 400))
    print("max(-80, -20, -10) : ", max(-80, -20, -10))
    print("max(0, 100, -400) : ", max(0, 100, -400))


if __name__ == "__main__":
    main()
