#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# complex() 函数
'''
我们把形如z=a+bi（a,b均为实数）的数称为复数，其中a称为实部，b称为虚部，i称为虚数单位。
当z的虚部等于零时，常称z为实数；
当z的虚部不等于零时，实部等于零时，常称z为纯虚数。

complex() 函数用于创建一个值为 real + imag * j 的复数或者转化一个字符串或数为复数。
如果第一个参数为字符串，则不需要指定第二个参数。
'''


def main():
    print(complex(1, 2))

    print(complex(1))  # 数字

    print(complex("1"))  # 当做字符串处理

    print(complex("1+2j"))  # 注意：这个地方在"+"号两边不能有空格，也就是不能写成"1 + 2j"，应该是"1+2j"，否则会报错


if __name__ == "__main__":
    main()
