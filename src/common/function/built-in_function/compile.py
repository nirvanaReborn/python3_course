#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# compile() 函数将一个字符串编译为字节代码。
'''
以下是 compile() 方法的语法:
    compile(source, filename, mode[, flags[, dont_inherit]])
参数
    source -- 字符串或者AST（Abstract Syntax Trees）对象。。
    filename -- 代码文件名称，如果不是从文件读取代码则传递一些可辨认的值。
    mode -- 指定编译代码的种类。可以指定为 exec, eval, single。
    flags -- 变量作用域，局部命名空间，如果被提供，可以是任何映射对象。。
    flags和dont_inherit是用来控制编译源码时的标志
返回值
    返回表达式执行结果。
    
    
http://blog.csdn.net/caimouse/article/details/41049949
compile(source, filename, mode, flags=0, dont_inherit=False, optimize=-1)
这个函数用来编译一段字符串的源码，结果可以生成字节码或者AST（抽像语法树），字节码可以使用函数exec()来执行，而AST可以使用eval()来继续编译。
参数source是一串字符串的源码，或者是AST对象数组。
参数filename是读取字符串的文件对象，如果不是从文件里读取源码来编译，那么这里可以放一些用来标识这些代码的字符串。
参数mode是用来指明那种表示的源码类型:
    如果是exec类型，表示这是一个序列语句，可以进行运行；
    如果是eval类型，表示这是一个单一的表达式语句，可以用来计算相应的值出来；
    如果是single类型，表示这是一个单一语句，采用交互模式执行，在这种情况下，如果是一个表达式，一般会输出结果，而不是打印为None输出。
可选参数flags和dont_inherit是用来控制编译源码时的标志，可以查看PEP236文档来了解这些参数，以及相关编译的说明。
    如果两者使用缺省参数（也即两者都是零值），在调用本函数编译时，主要使用代码中指明的编译特征来对待；
    如果flags参数设置有值，而dont_inherit没有设置（即是零值），那么编译代码时，不仅源码的编译特征起作用，而且flags指明的特征也起作用，相当两者的并集；
    如果参数dont_inherit设置有值（即是非零值），编译语句时只有参数flags指明的编译特征值起作用，即是不使用源码里指明的特征。
    编译特征是按位图的方式设置到参数里，可以查看__future__。
可选参数optimize是用来指明编译器使用优化的等级；
    缺省值是-1，表示使用命令行参数-O中获取的优化等级为准；
    如果设置值为0（即是不用优化，__debug__是设置true），是没有优化；
    如果设置值为1，assert语句被删除，__debug__设置为false；
    如果设置值为2，除了设置值为1的功能之外，还会把代码里文档说明也删除掉，达到最佳优化结果。
本函数编译代码时，如果语法出错会返回SyntaxError；如果代码包含一些空字节，则返回类型错误TypeError。
注意事项：
    当采用single或eval类型编译时，如果有多行代码，每行代码后面至少有一个换行符，否则在code模块编译时就会提示编译的源码不完整错误。
    在Python 3.2版本之后，允许输入Windows或Mac的换行符；
    当采用exec模式时，不需要在每个行后面输入换行符；在这个版本之后增加了优化参数。    
'''


def test_1():
    str = "for i in range(0,10): print(i)"
    c = compile(str, '', 'exec')  # 编译为字节代码对象
    print(c)
    exec(c)  # 执行


def test_2():
    str = "3 * 4 + 5"
    a = compile(str, '', 'eval')  # 编译为表达式
    print(a)
    print(eval(a))


def test_3():
    str = 'print("pythoner.com")'
    a = compile(str, '', 'single')
    exec(a)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    main()
