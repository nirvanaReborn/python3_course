#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# __import__() 函数
'''
__import__() 函数用于动态加载类和函数 。
如果一个模块经常变化就可以使用 __import__() 来动态载入。
__import__(name[, globals[, locals[, fromlist[, level]]]])

注意：一般不建议我们自行使用__import__()导入，其行为在python2 和python3 中有差异，会导致意外错误。
如果需要动态导入可以使用importlib 模块。
'''


def main():
    current_date = "%s" % __import__('time').strftime("%Y%m%d")
    print(current_date)

    print("Python {:s} on {:s}".format(__import__('sys').version, __import__('sys').platform))


if __name__ == "__main__":
    main()
