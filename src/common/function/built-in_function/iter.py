#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# iter() 函数: 用来生成迭代器。
'''
iter(object[, sentinel])
参数
    object -- 支持迭代的集合对象。
    sentinel -- 如果传递了第二个参数，则参数 object 必须是一个可调用的对象（如，函数），
                此时，iter 创建了一个迭代器对象，每次调用这个迭代器对象的__next__()方法时，都会调用 object。

'''


def main():
    list_1 = [11, 21, 13]
    for item in iter(list_1):
        print(item)


if __name__ == "__main__":
    main()
