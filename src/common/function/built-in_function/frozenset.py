#!/usr/bin/env python
# -*- coding:utf-8 -*-


# http://www.runoob.com/python3/python3-built-in-functions.html
# frozenset() 函数: 返回一个冻结的集合，冻结后集合不能再添加或删除任何元素。


def main():
    a = frozenset(range(10))  # 生成一个新的不可变集合
    print(a)  # frozenset({0, 1, 2, 3, 4, 5, 6, 7, 8, 9})

    b = frozenset('runoob')
    print(b)  # frozenset({'r', 'n', 'u', 'o', 'b'})


if __name__ == "__main__":
    main()
