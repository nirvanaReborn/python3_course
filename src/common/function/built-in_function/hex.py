#!/usr/bin/env python
# -*- coding:utf-8 -*-


# http://www.runoob.com/python3/python3-built-in-functions.html
# hex() 函数: 用于将10进制整数转换成16进制整数。

def main():
    print(hex(255))  # 0xff
    print(hex(-42))  # -0x2a


if __name__ == "__main__":
    main()
