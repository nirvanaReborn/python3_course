#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# print() 函数
# print 在 Python3.x 是一个函数，但在 Python2.x 版本不是一个函数，只是一个关键字。
'''
print(*objects, sep=' ', end='\n', file=sys.stdout)
参数
    objects -- 复数，表示可以一次输出多个对象。输出多个对象时，需要用 , 分隔。
    sep -- 用来间隔多个对象，默认值是一个空格。
    end -- 用来设定以什么结尾。默认值是换行符 \n，我们可以换成其他字符串。
    file -- 要写入的文件对象。
'''


def test_1():
    '''设置占位符'''
    a = int(input('a = '))
    b = int(input('b = '))
    print('%d + %d = %d' % (a, b, a + b))
    print('%d - %d = %d' % (a, b, a - b))
    print('%d * %d = %d' % (a, b, a * b))
    print('%d / %d = %f' % (a, b, a / b))
    print('%d // %d = %d' % (a, b, a // b))
    print('%d %% %d = %d' % (a, b, a % b))
    print('%d ** %d = %d' % (a, b, a ** b))
    # 说明：上面的print函数中输出的字符串使用了占位符语法，
    # 其中%d是整数的占位符，%f是小数的占位符，
    # %%表示百分号（因为百分号代表了占位符，所以带占位符的字符串中要表示百分号必须写成%%），
    # 字符串之后的%后面跟的变量值会替换掉占位符然后输出到终端中，
    # 运行上面的程序，看看程序执行结果就明白啦。


def test_2():
    '''设置间隔符'''
    print("www", "runoob", "com", sep=".")


def test_3():
    '''设置结束符'''
    print("www", "runoob", "com", end=".")


def test_4():
    '''设置输出文件'''
    with open(r"demo.txt", "w") as f:
        print("www", "runoob", "com", sep=".", file=f)


def test_5():
    import time

    print("---RUNOOB EXAMPLE ： Loading 效果---")
    print("Loading", end="")
    for i in range(20):
        # flush：是否强制刷新流。
        print(".", end='', flush=True)
        time.sleep(0.5)


def test_6():
    string = "the length of (%s) is %d" % ('runoob', len('runoob'))
    print(string)


def test_7():
    age = int(input("Please input your age:"))
    name = input("Please input your name:")
    # Python3.6新增了一种f-字符串格式化
    print(f"Ok,your name is {age}, and your age is {name}.")


def test_8():
    f = float(input('请输入华氏温度: '))
    c = (f - 32) / 1.8
    # print('%.1f华氏度 = %.1f摄氏度' % (f, c))
    print(f'{f:.1f}华氏度 = {c:.1f}摄氏度')


def test_9():
    '''在屏幕上显示跑马灯文字'''
    import time, os
    content = '北京欢迎你为你开天辟地…………'
    while True:
        # 清理屏幕上的输出
        if os.name == "nt":
            os.system('cls')
        elif os.name == "posix":
            os.system('clear')
        print(content)
        # 休眠200毫秒
        time.sleep(0.2)
        content = content[1:] + content[0]


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
