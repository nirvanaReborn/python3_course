#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# delattr() 函数
# delattr 函数用于删除属性。
# delattr(x, 'foobar') 相等于 del x.foobar。


class Coordinate:
    x = 10
    y = -5
    z = 0


def main():
    point1 = Coordinate()

    print('x = ', point1.x)
    print('y = ', point1.y)
    print('z = ', point1.z)

    delattr(Coordinate, 'z')

    print('--删除 z 属性后--')
    print('x = ', point1.x)
    print('y = ', point1.y)

    # 触发错误
    print('z = ', point1.z)


if __name__ == "__main__":
    main()
