#!/usr/bin/env python
# -*- coding:utf-8 -*-

'''
# 绝对引入主要是针对python2.4及之前的版本的，
# 这些版本在引入某一个.py文件时，会首先从当前目录下查找是否有该文件。
# 如果有，则优先引用当前包内的文件。而如果我们想引用python自带的.py文件时，则需要使用
from __future__ import absolute_import


# 在开头加上这句之后，即使在python2.X，使用print就得像python3.X那样加括号使用。
from __future__ import print_function


# 对于str类型的字符串，调用len()和遍历时，其实都是以字节为单位的，这个太坑爹了，同一个字符使用不同的编码格式，长度往往是不同的。
# 对于unicode类型的字符串调用len()和遍历才是以字符为单位，这是我们所要的。
# 将模块中显式出现的所有字符串转为unicode类型，不过，对于必须使用str字符串的地方要加以注意。
from __future__ import unicode_literals


# https://blog.csdn.net/zzc15806/article/details/81133045
# 用with替代上述异常检测代码：
from __future__ import with_statement


# 导入python未来支持的语言特征division(精确除法)，
# 当我们没有在程序中导入该特征时，"/"操作符执行的是截断除法(Truncating Division),
# 当我们导入精确除法之后，"/"执行的是精确除法
from __future__ import division




'''


def main():
    pass


if __name__ == "__main__":
    main()
