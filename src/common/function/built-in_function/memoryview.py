#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# memoryview() 函数: 返回给定参数的内存查看对象(Momory view)。
# 所谓内存查看对象，是指对支持缓冲区协议的数据进行包装，在不需要复制对象基础上允许Python代码访问。


def main():
    v = memoryview(b'abcefg')
    print(v[1])  # 98
    print(v[-1])  # 103
    print(v[1:4])  # <memory at 0x00000193A548C588>
    print(v[1:4].tobytes())  # b'bce'


if __name__ == "__main__":
    main()
