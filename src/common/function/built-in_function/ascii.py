#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# ascii() 函数
# ascii() 函数类似 repr() 函数, 返回一个表示对象的字符串,
# 但是对于字符串中的非 ASCII 字符则返回通过 repr() 函数使用 \x, \u 或 \U 编码的字符。
# 生成字符串类似 Python2 版本中 repr() 函数的返回值。


def main():
    print(ascii('A'))
    print(ascii('runoob'))

    source_str = r"chrome_bookmarks.json"
    print(ascii(source_str[-4:]))
    print(repr(source_str[-4:]))
    print('''{0}'''.format(source_str[-4:]))


if __name__ == "__main__":
    main()
