#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# slice() 函数： 实现切片对象，主要用在切片操作函数里的参数传递。
'''
class slice(stop)
class slice(start, stop[, step])
参数说明：
    start -- 起始位置
    stop -- 结束位置
    step -- 间距
'''


def main():
    myslice = slice(5)  # 设置截取5个元素的切片
    print(myslice)
    # slice(None, 5, None)
    arr = list(range(10))
    print(arr)
    # [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    print(arr[myslice])  # 截取 5 个元素
    # [0, 1, 2, 3, 4]

    alist = ['123', 'abc', 'good', 'hello', 'nice']  # 定义一个列表
    alist_1 = alist[:]  # [:]分号左边表示从第0位开始，分号右边表示最后一位结束。
    print(alist_1)  # 成功将alist列表拷贝给了alist1


if __name__ == "__main__":
    main()
