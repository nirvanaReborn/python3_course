#!/usr/bin/env python
# -*- coding:utf-8 -*-


# http://www.runoob.com/python3/python3-built-in-functions.html
# isinstance() 函数: 判断一个对象是否是一个已知的类型，类似 type()。

# isinstance() 与 type() 区别：
# type() 不会认为子类是一种父类类型，不考虑继承关系。
# isinstance() 会认为子类是一种父类类型，考虑继承关系。
# 如果要判断两个类型是否相同推荐使用 isinstance()。


# 判断变量类型
def typeof(variate):
    if isinstance(variate, bool):
        var_type = "bool"
    elif isinstance(variate, int):
        var_type = "int"
    elif isinstance(variate, float):
        var_type = "float"
    elif isinstance(variate, complex):
        var_type = "complex"
    elif isinstance(variate, str):
        var_type = "str"
    elif isinstance(variate, list):
        var_type = "list"
    elif isinstance(variate, tuple):
        var_type = "tuple"
    elif isinstance(variate, dict):
        var_type = "dict"
    elif isinstance(variate, set):
        var_type = "set"
    else:
        var_type = None
    return var_type


# 返回变量类型
def getType(variate):
    dict_type = {
        "bool": "布尔型",
        "int": "整型",
        "float": "浮点型",
        "complex": "复数",
        "str": "字符串",
        "list": "列表",
        "tuple": "元组",
        "dict": "字典",
        "set": "集合"
    }
    vartype = typeof(variate)
    if not (vartype in dict_type):
        return "未知类型"
    return dict_type[vartype]


def main():
    a = 2
    print(isinstance(a, int))
    print(isinstance(a, str))
    print(isinstance(a, (str, int, list)))  # 是元组中的一个返回 True

    class A:
        pass

    class B(A):
        pass

    print(isinstance(A(), A))  # returns True
    print(type(A()) == A)  # returns True
    print(isinstance(B(), A))  # returns True
    print(type(B()) == A)  # returns False


if __name__ == "__main__":
    main()
