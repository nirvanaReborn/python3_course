#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# type() 函数
# type() 函数如果你只有第一个参数则返回对象的类型，三个参数返回新的类型对象。

'''
# class type(name, bases, dict)
# 参数
#     name -- 类的名称。
#     bases -- 基类的元组。
#     dict -- 字典，类内定义的命名空间变量。
    
# isinstance() 与 type() 区别：
# type() 不会认为子类是一种父类类型，不考虑继承关系。
# isinstance() 会认为子类是一种父类类型，考虑继承关系。
# 如果要判断两个类型是否相同推荐使用 isinstance()。
'''


# 判断变量类型的函数
def typeof(variate):
    if type(variate) == type(True):
        var_type = "bool"
    elif type(variate) == type(1):
        var_type = "int"
    elif type(variate) == type(12.3):
        var_type = "float"
    elif type(variate) == type(1 + 5j):
        var_type = "complex"
    elif type(variate) == type("string"):
        var_type = "str"
    elif type(variate) == type([1]):
        var_type = "list"
    elif type(variate) == type(()):
        var_type = "tuple"
    elif type(variate) == type({"key1": "123"}):
        var_type = "dict"
    elif type(variate) == type({"key1"}):
        var_type = "set"
    else:
        var_type = None
    return var_type


# 返回变量类型
def getType(variate):
    dict_type = {
        "bool": "布尔型",
        "int": "整型",
        "float": "浮点型",
        "complex": "复数",
        "str": "字符串",
        "list": "列表",
        "tuple": "元组",
        "dict": "字典",
        "set": "集合"
    }
    vartype = typeof(variate)
    if not (vartype in dict_type):
        return "未知类型"
    return dict_type[vartype]


def main():
    print(type(1))

    # 三个参数
    class X(object):
        a = 1

    X = type('X', (object,), dict(a=1))  # 产生一个新的类型 X
    print(X)


if __name__ == "__main__":
    main()
