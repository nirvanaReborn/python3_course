#!/usr/bin/env python
# -*- coding:utf-8 -*-


# http://www.runoob.com/python3/python3-built-in-functions.html
# int() 函数: 用于将一个字符串会数字转换为整型。
'''
class int(x, base=10)
参数
    x -- 字符串或数字。
    base -- 进制数，默认十进制。
'''


def main():
    print(int())
    print(int('0101', 2))
    print(int('0xa', 16))
    print(int('10', 8))


if __name__ == "__main__":
    main()
