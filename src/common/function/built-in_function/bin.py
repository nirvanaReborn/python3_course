#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# bin() 函数
# bin() 返回一个整数 int 或者长整数 long int 的二进制表示。


def main():
    print(bin(10))


if __name__ == "__main__":
    main()
