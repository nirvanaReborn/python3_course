#!/usr/bin/env python
# -*- coding:utf-8 -*-


# http://www.runoob.com/python3/python3-built-in-functions.html
# hasattr() 函数: 用于判断对象是否包含对应的属性。
'''
hasattr 语法：
    hasattr(object, name)
参数
    object -- 对象。
    name -- 字符串，属性名。
'''


def test_1(obj, attribute):
    print(hasattr(obj, attribute))
    return hasattr(obj, attribute)


# 等同于
def test_2(obj, attribute):
    list_obj = dir(obj)
    if attribute in list_obj:
        # do some thing you need
        print(True)
    else:
        # error: has not attribute
        print(False)


def test_3():
    class Coordinate:
        x = 10
        y = -5
        z = 0

    point1 = Coordinate()
    print(hasattr(point1, 'x'))
    print(hasattr(point1, 'y'))
    print(hasattr(point1, 'z'))
    print(hasattr(point1, 'no'))  # 没有该属性
    print(hasattr(point1, 'z') and getattr(point1, 'x') or getattr(point1, 'y'))
    print(hasattr(point1, 'no') and getattr(point1, 'x') or getattr(point1, 'y'))


def main():
    dict_choice = {
        "1": "test_1(list, 'append')",
        "2": "test_2(list, 'append')",
        "3": "test_3()",
    }

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
