#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# setattr() 函数： 用于设置属性值，该属性必须存在。
# setattr 函数对应函数 getatt()
'''
setattr(object, name, value)
参数
    object -- 对象。
    name -- 字符串，对象属性。
    value -- 属性值。
'''


class A(object):
    bar = 1


def main():
    a = A()
    print(getattr(a, 'bar'))  # 获取属性 bar 值
    print(setattr(a, 'bar', 5))  # 设置属性 bar 值


if __name__ == "__main__":
    main()
