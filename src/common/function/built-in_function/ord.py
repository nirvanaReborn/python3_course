#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# ord() 函数
# 它以一个字符（长度为1的字符串）作为参数，返回对应的 Unicode 数值，
# 如果所给的 Unicode 字符超出了你的 Python 定义范围，则会引发一个 TypeError 的异常。
# ord() 函数是 chr() 函数（对于8位的ASCII字符串）或 unichr() 函数（对于Unicode对象）的配对函数，


def main():
    print(ord('a'))  # 97
    print(ord('高'))


if __name__ == "__main__":
    main()
