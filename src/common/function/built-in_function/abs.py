#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# abs() 函数返回数字的绝对值。

def main():
    print("abs(-45) : ", abs(-45))
    print("abs(100.12) : ", abs(100.12))


if __name__ == "__main__":
    main()
