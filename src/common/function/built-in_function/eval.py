#!/usr/bin/env python
# -*- coding:utf-8 -*-


# http://www.runoob.com/python3/python3-built-in-functions.html
# eval() 函数
# eval() 函数用来执行一个字符串表达式，并返回表达式的值。
# eval(expression[, globals[, locals]])
# 参数
#     expression -- 表达式。
#     globals -- 变量作用域，全局命名空间，如果被提供，则必须是一个字典对象。
#     locals -- 变量作用域，局部命名空间，如果被提供，可以是任何映射对象。
# 返回值
#     返回表达式计算结果。


def test_1():
    x = 7
    print(eval('3 * x'))
    print(eval('pow(2,2)'))


# 强大之处: 字符串与list、tuple、dict的转化。
def test_2():
    a = "[[1,2], [3,4], [5,6], [7,8], [9,0]]"
    print(type(a))
    b = eval(a)
    print(type(b))
    print(b)

    a = "{1: 'a', 2: 'b'}"
    print(type(a))
    b = eval(a)
    print(type(b))
    print(b)

    a = "([1,2], [3,4], [5,6], [7,8], (9,0))"
    print(type(a))
    b = eval(a)
    print(type(b))
    print(b)


# 危险之处:eval虽然方便，但是要注意安全性，可以将字符串转成表达式并执行，就可以利用执行系统命令，删除文件等操作。
def test_3():
    eval("__import__('os').system('ls /Users/chunming.liu/Downloads/')")
    eval("__import__('os').system('rm /Users/chunming.liu/Downloads/*')")


# NameError: name 'func' is not defined
def test_4():
    def func():
        sum = 10
        return sum

    expr = "func()"
    result = eval(expr, {'x': 1, 'y': 2})
    print(result)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
