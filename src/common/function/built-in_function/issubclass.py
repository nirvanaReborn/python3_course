#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# issubclass(class, classinfo) 函数
# 用于判断参数 class 是否是类型参数 classinfo 的子类。

class A:
    pass


class B(A):
    pass


def main():
    print(issubclass(B, A))  # 返回 True


if __name__ == "__main__":
    main()
