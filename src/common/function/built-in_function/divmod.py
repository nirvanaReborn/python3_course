#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# divmod() 函数
# divmod() 函数把除数和余数运算结果结合起来，返回一个包含商和余数的元组(a // b, a % b)。
# 在 python 2.3 版本之前不允许处理复数。


def main():
    print(divmod(7, 2))  # (3, 1)
    print(divmod(8, 4))  # (2, 0)
    # print(divmod(1+2j, 1+0.5j))


if __name__ == "__main__":
    main()
