#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# list()方法: 用于将元组转换为列表。
# 注：元组与列表是非常类似的，区别在于元组的元素值不能修改，元组是放在括号中，列表是放于方括号中。

def main():
    aTuple = (123, 'Google', 'Runoob', 'Taobao')
    list1 = list(aTuple)
    print("列表元素 : ", list1)

    str = "Hello World"
    list2 = list(str)
    print("列表元素 : ", list2)


if __name__ == "__main__":
    main()
