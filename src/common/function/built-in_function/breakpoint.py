#!/usr/bin/env python3
# -*- coding:utf-8 -*-


def main():
    def calculate_sum(a, b):
        result = a + b
        breakpoint()  # Debugger breakpoint
        return result

    x = 5
    y = 10
    z = calculate_sum(x, y)
    print(z)


if __name__ == '__main__':
    main()
