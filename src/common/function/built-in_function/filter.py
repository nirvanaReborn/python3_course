#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# filter() 函数: 用于过滤序列
'''
filter() 函数用于过滤序列，过滤掉不符合条件的元素，返回由符合条件元素组成的新列表。
该函数接收两个参数，第一个为函数，第二个为序列，序列的每个元素作为参数传递给函数进行判，然后返回 True 或 False，最后将返回 True 的元素放到新列表中。
filter(function, iterable)
参数
    function -- 判断函数。
    iterable -- 可迭代对象。
返回值
    返回列表。

http://blog.csdn.net/damotiansheng/article/details/44139111
Python3.4中filter函数，map函数和reduce函数

filter()为已知的序列的每个元素调用给定的布尔函数，调用中，返回值为非零的元素将被添加至一个列表中
'''


def test_1():
    def function_1(x):
        if x > 20:
            return True
        else:
            return False

    list_1 = [1, 2, 3, 42, 67, 16]
    print(list(filter(function_1, list_1)))  # [42, 67]
    list_2 = filter(function_1, list_1)
    print(list(list_2))  # [42, 67]
    print(list_2.__next__)  # <method-wrapper '__next__' of filter object at 0x01286330>
    print(list_2.__next__)  # <method-wrapper '__next__' of filter object at 0x01286330>
    print(list_2.__next__)  # <method-wrapper '__next__' of filter object at 0x01286330>
    print(list_2.__next__)  # <method-wrapper '__next__' of filter object at 0x01286330>


def test_2():
    # 过滤出列表中的所有奇数
    def is_odd(n):
        return n % 2 == 1

    newlist = filter(is_odd, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    print(list(newlist))


def test_3():
    # 过滤出1~100中平方根是整数的数
    import math
    def is_sqr(x):
        return math.sqrt(x) % 1 == 0

    newlist = filter(is_sqr, range(1, 101))
    print(list(newlist))


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
