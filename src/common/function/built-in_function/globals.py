#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# globals() 函数: 会以字典类型返回当前位置的全部全局变量。

import pprint


def main():
    pprint.pprint(globals())
    '''
    {'__annotations__': {},
     '__builtins__': <module 'builtins' (built-in)>,
     '__cached__': None,
     '__doc__': None,
     '__file__': 'D:/share/git/gitee/nirvanaReborn/python3_course/src/common/function/built-in_function/globals.py',
     '__loader__': <_frozen_importlib_external.SourceFileLoader object at 0x0000019EC2D1C0F0>,
     '__name__': '__main__',
     '__package__': None,
     '__spec__': None,
     'main': <function main at 0x0000019EC405BD08>,
     'pprint': <module 'pprint' from 'D:\\ProgramFiles\\Python\\Anaconda3\\lib\\pprint.py'>}
    '''


if __name__ == "__main__":
    main()
