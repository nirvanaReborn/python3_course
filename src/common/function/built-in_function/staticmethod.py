#!/usr/bin/env python
# -*- coding:utf-8 -*-

# http://www.runoob.com/python3/python3-built-in-functions.html
# staticmethod() 函数：返回函数的静态方法。
'''
class C(object):
    @staticmethod
    def f(arg1, arg2, ...):
        ...
以上实例声明了静态方法 f，类可以不用实例化就可以调用该方法 C.f()，当然也可以实例化后调用 C().f()。
'''


class C(object):
    @staticmethod
    def f():
        print('runoob')


def main():
    C.f()  # 静态方法无需实例化
    cobj = C()
    cobj.f()  # 也可以实例化后调用


if __name__ == "__main__":
    main()
