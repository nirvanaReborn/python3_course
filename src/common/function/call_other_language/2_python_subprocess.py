#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://docs.python.org/2/library/subprocess.html?highlight=subprocess#frequently-used-arguments
'''
https://www.cnblogs.com/wupeiqi/articles/4963027.html
subprocess.Popen(...)用于执行复杂的系统命令
参数：
args：shell命令，可以是字符串或者序列类型（如：list，元组）
bufsize：指定缓冲。0 无缓冲,1 行缓冲,其他 缓冲区大小,负值 系统缓冲
stdin, stdout, stderr：分别表示程序的标准输入、输出、错误句柄
preexec_fn：只在Unix平台下有效，用于指定一个可执行对象（callable object），它将在子进程运行之前被调用
close_sfs：在windows平台下，如果close_fds被设置为True，则新创建的子进程将不会继承父进程的输入、输出、错误管道。
            所以不能将close_fds设置为True同时重定向子进程的标准输入、输出与错误(stdin, stdout, stderr)。
shell：同上
cwd：用于设置子进程的当前目录
env：用于指定子进程的环境变量。如果env = None，子进程的环境变量将从父进程中继承。
universal_newlines：不同系统的换行符不同，True -> 同意使用 \n
startupinfo与createionflags只在windows下有效,将被传递给底层的CreateProcess()函数，用于设置子进程的一些属性，如：主窗口的外观，进程的优先级等等


终端输入的命令分为两种：
1.输入即可得到输出，如：ifconfig
2.输入进行某环境，依赖再输入，如：python
'''
# http://www.jb51.net/article/55913.htm
# 实例调用其他python脚本

# http://www.jb51.net/article/78957.htm
# 实例调用其他python脚本中的函数

# http://www.cnblogs.com/arkenstone/p/5609063.html
# Python中在脚本中引用其他文件函数的方法

# subprocess意在替代其他几个老的模块或者函数，比如：os.system os.spawn* os.popen* popen2.* commands.*
# https://blog.csdn.net/wintersweetzeng/article/details/47319115

# https://blog.csdn.net/hpwzjz/article/details/82992176
# python中的subprocess.Popen()使用

import logging
# import shlex
import os
import subprocess


# 获取python解释器的路径
def get_python_exe_path():
    import platform
    sys_str = platform.system()
    if sys_str == 'Linux':
        interpreter = r"/usr/bin/python3"
    elif sys_str == 'Windows':
        interpreter = r"D:\ProgramFiles\Anaconda3\python.exe"
    else:
        print("未识别操作系统:", sys_str)
        interpreter = None

    return interpreter


def get_list_cmd():
    interpreter = get_python_exe_path()

    log = logging.getLogger("Core.Analysis.Processing")
    if not os.path.exists(interpreter):
        log.error("找不到python解释器：[%s]." % interpreter)

    # processor = os.path.join(os.path.dirname(os.path.realpath(__file__)), r"..\..\data_structure\1_list.py")
    processor = os.path.join(os.path.dirname(os.path.realpath(__file__)), r"../../data_structure/1_list.py")
    # processor = os.path.join(os.path.dirname(os.path.realpath(__file__)), r"../../data_structure/8_int.py") # 用于linux系统测试

    option_1 = "1"  # 直接带参执行
    # list_arg = shlex.split(processor) # 用于linux系统测试
    list_cmd = [interpreter, processor, option_1]
    print(list_cmd)
    return list_cmd


def test_1():
    list_cmd = get_list_cmd()
    child = subprocess.Popen(list_cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    child.wait()  # 等待子进程结束

    # stdout, stderr = child.communicate()
    # print(str(stdout, encoding="utf-8"))
    # print(str(stderr))

    if child.returncode == 0:
        # print(child .stdout.read().decode('gbk'))

        line = child.stdout.readline().decode('gbk').strip()
        while line:
            print('Subprogram output: {}'.format(line))
            line = child.stdout.readline().decode('gbk').strip()
    else:
        logging.error('Subprogram failed:%d' % child.returncode)


def test_2():
    list_result = []
    list_cmd = get_list_cmd()
    child = subprocess.Popen(list_cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1)

    for line in iter(child.stdout.readline, b''):
        # print(line)
        print(str(line, encoding='utf-8'))
        list_result.append(str(line, encoding='utf-8'))
    child.stdout.close()
    child.wait()  # 等待子进程结束
    return list_result


def test_3():
    list_cmd = get_list_cmd()
    child = subprocess.Popen(list_cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    for line in iter(child.stdout.readline, 'b'):
        line = str(line, encoding='utf-8')
        print(line)
        if not subprocess.Popen.poll(child) is None:
            if line == "":
                break
    child.stdout.close()


def test_4():
    data = {}
    list_cmd = get_list_cmd()
    child = subprocess.Popen(list_cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    try:
        while True:
            buff = child.stdout.readline()
            buff = str(buff, encoding='utf-8')
            print(buff)
            if buff == '' and child.poll() == None:
                break
            else:
                print(buff, child.poll())
    except Exception:
        data["status"] = -1
    finally:
        return data


def test_5():
    # call 执行命令，返回状态码
    # shell = True ，允许 shell 命令是字符串形式
    list_cmd = ["ls", "-l"]
    ret = subprocess.call(list_cmd, shell=False)
    ret = subprocess.call(" ".join(list_cmd), shell=True)


def test_6():
    # check_call 执行命令，如果执行状态码是 0 ，则返回0，否则抛异常
    subprocess.check_call(["ls", "-l"])
    subprocess.check_call("exit 1", shell=True)


def test_7():
    # check_output 执行命令，如果状态码是 0 ，则返回执行结果，否则抛异常
    subprocess.check_output(["echo", "Hello World!"])
    subprocess.check_output("exit 1", shell=True, cwd='/home/dev')


def test_8():
    ret1 = subprocess.Popen(["mkdir", "t1"])
    ret2 = subprocess.Popen("mkdir t2", shell=True)


def test_9():
    obj = subprocess.Popen(["python"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    obj.stdin.write('print 1 \n ')
    obj.stdin.write('print 2 \n ')
    obj.stdin.write('print 3 \n ')
    obj.stdin.write('print 4 \n ')
    obj.stdin.close()

    cmd_out = obj.stdout.read()
    obj.stdout.close()
    cmd_error = obj.stderr.read()
    obj.stderr.close()

    print(cmd_out)
    print(cmd_error)


def test_10():
    obj = subprocess.Popen(["python"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    obj.stdin.write('print 1 \n ')
    obj.stdin.write('print 2 \n ')
    obj.stdin.write('print 3 \n ')
    obj.stdin.write('print 4 \n ')

    out_error_list = obj.communicate()
    print(out_error_list)


def test_11():
    obj = subprocess.Popen(["python"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out_error_list = obj.communicate(b'print "hello"')
    print(out_error_list)


# -----------------------------------------------------------------------


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
