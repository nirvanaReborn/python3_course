#!/usr/bin/env python
# -*- coding:utf-8 -*-

# 将 .pyx/.py 编译为 .c 文件，再将 .c 文件编译为 .so(Unix)或 .pyd(Windows)

# http://blog.csdn.net/jerry_1126/article/details/46584179
# 执行系统命令

# 在Python中，可以方便地使用os模块来运行其他脚本或者程序，
# 这样就可以在脚本中直接使用其他脚本或程序提供的功能，而不必再次编写实现该功能的代码。
# 为了更好地控制运行的进程，可以使用win32process模块中的函数，
# 如果想进一步控制进程，则可以使用ctype模块，直接调用kernel32.dll中的函数.
#
# 可以执行shell命令的相关模块和函数有：
# os.system
# os.spawn*
# os.popen*       --废弃
# popen2.*        --废弃
# commands.*      --废弃，3.x中被移除


def test_0():
    import os
    # 直接调用可执行程序
    os.startfile(r"D:\ProgramFiles\1_FreeInstallation\SearchAndReplace_6.5\SR32.EXE")


def test_1():
    # os模块中的system()函数可以方便地运行其他程序或者脚本，模式如下:
    # os.system(command)
    # command: 要执行的命令，如果要向脚本传递参数，可以使用空格分割程序及多个参数。
    import os

    os.system('notepad')  # 打开记事本程序
    # os.system('notepad 1.txt')  # 打开1.txt文件,如果不存在，则创建


def test_2():
    '''
    os.popen() 方法用于从一个命令打开一个管道。在Unix，Windows中有效。
    os.popen() 功能强于os.system() , os.popen() 可以返回回显的内容，以文件描述符返回。
    popen()方法语法格式如下：
        os.popen(command[, mode[, bufsize]])
    参数
        command -- 使用的命令。
        mode    -- 模式权限可以是 'r'(默认) 或 'w'。
        bufsize -- 指明了文件需要的缓冲大小：0意味着无缓冲；1意味着行缓冲；
                   其它正值表示使用参数大小的缓冲（大概值，以字节为单位），负的bufsize意味着使用系统的默认值。
                   一般来说，对于tty设备，它是行缓冲；对于其它文件，它是全缓冲。如果没有改参数，使用系统的默认值。
    返回值
        返回一个文件描述符号为fd的打开的文件对象
    '''
    import os

    os.popen('notepad')
    # os.popen('notepad 1.txt')


def test_3():
    # Popen非常强大，支持多种参数和模式。
    # 但是Popen函数有一个缺陷，就是它是一个阻塞的方法。
    # 如果运行cmd时产生的内容非常多，函数非常容易阻塞住。
    # 解决办法是不使用wait()方法，但是也不能获得执行的返回值了。
    import subprocess

    cmd = "cmd.exe /c " + r"D:\share\git\gitee\zhangtao2016\course_bat\src\base\百度百科_for_01.bat"
    # cmd = shlex.split(cmd) # 用于linux系统测试
    print(cmd)
    child = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    child.wait()
    if child.returncode == 0:
        result = child.stdout.readline().decode('gbk').strip()
        while result:
            print(result)
            result = child.stdout.readline().decode('gbk').strip()

    # subprocess.Popen.wait(child, timeout=1)
    # print(child.returncode)


def test_4():
    '''
    除了使用os.system()函数外，还可以使用win32api模块中的ShellExecute()函数来运行其他程序,格式如下:
    # ShellExecute(hwnd, op, file, args, dir, show)
    #     hwnd:          父窗口的句柄，如果没有父窗口，则为0
    #     op  :          要运行的操作，为open,print或者为空
    #     file:          要运行的程序，或者打开的脚本
    #     args:          要向程序传递的参数，如果打开的是文件则为空
    #     dir :          程序初始化的目录
    #     show:          是否显示窗口
    使用ShellExecute函数，就相当于在资源管理器中双击文件图标，系统会打开相应程序运行。
    win32api安装:
    '''
    import win32api

    win32api.ShellExecute(0, 'open', 'notepad.exe', '', '', 0)  # 后台执行
    win32api.ShellExecute(0, 'open', 'notepad.exe', '', '', 1)  # 前台打开
    win32api.ShellExecute(0, 'open', 'notepad.exe', '1.txt', '', 1)  # 打开文件
    win32api.ShellExecute(0, 'open', 'http://www.sohu.com', '', '', 1)  # 打开网页
    win32api.ShellExecute(0, 'open', 'D:\\Opera.mp3', '', '', 1)  # 播放视频
    win32api.ShellExecute(0, 'open', 'D:\\hello.py', '', '', 1)  # 运行程序


def test_5():
    '''
    为了便于控制通过脚本运行的程序，可以使用win32process模块中的CreateProcess()函数创建一个运行相应程序的进程。其函数格式为:
    # CreateProcess(appName, cmdLine, proAttr, threadAttr, InheritHandle, CreationFlags, newEnv, currentDir, Attr)
    #     appName         可执行文件名
    #     cmdLine         命令行参数
    #     procAttr        进程安全属性
    #     threadAttr      线程安全属性
    #     InheritHandle  继承标志
    #     CreationFlags  创建标志
    #     currentDir      进程的当前目录
    #     Attr             创建程序的属性

    可以使用win32process.TerminateProcess函数来结束已创建的进程, 函数格式如下:
    # TerminateProcess(handle, exitCode)
    #     handle     要操作的进程句柄
    #     exitCode   进程退出代码

    或者使用win32event.WaitForSingleObject等待创建的线程结束，函数格式如下:
    # WaitForSingleObject(handle, milisecond)
    #     handle     : 要操作的进程句柄
    #     milisecond : 等待的时间,如果为-1,则一直等待.
    '''
    import win32process

    # 打开记事本，获得其句柄
    handle = win32process.CreateProcess('C:\\Windows\\notepad.exe', '',
                                        None, None, 0, win32process.CREATE_NO_WINDOW,
                                        None, None, win32process.STARTUPINFO())
    # (<PyHANDLE:892>, <PyHANDLE:644>, 21592, 18780) # 函数返回进程句柄、线程句柄、进程ID以及线程ID
    win32process.TerminateProcess(handle[0], 0)  # 终止进程

    # 或者
    # import win32event
    #
    # win32event.WaitForSingleObject(handle[0], -1)  # 等待进程结束
    # 0  # 进程结束返回值


def test_6():
    '''
    使用ctypes模块可以让Python调用位于动态链接库的函数。
    使用ctypes模块可以方便地调用由C语言编写的动态链接库，并向其传递参数。
    ctypes模块定义了C语言中的基本数据类型，并且可以实现C语言中的结构体和联合体。
    ctypes模块可以工作在Windows,Linux,Mac OS等多种操作系统，基本上实现了跨平台。
    ctype模块中含有的基本类型与C语言类似，下面是几个基本的数据类型的对照:
    ---------------------------------------
    Ctypes数据类型           C数据类型
    ---------------------------------------
    c_char                    char
    c_short                   short
    c_int                     int
    c_long                    long
    c_float                   float
    c_doule                   double
    c_void_p                  void *
    ---------------------------------------
    '''
    import ctypes
    import platform

    if platform.system() == 'Windows':
        # [WinError 193] %1 不是有效的 Win32 应用程序。
        # python是64位的python，而windll.LoadLibrary只能由32位的python使用
        # 如果有对应dll的源码，那么可以重新配置成64位方式编译，获得64位的dll，就可以直接调用了。
        libc = ctypes.cdll.LoadLibrary('msvcrt.dll')
        # libc = ctypes.cdll.msvcrt
        # libc = ctypes.pydll.LoadLibrary('msvcrt.dll')
        # libc = ctypes.windll.LoadLibrary('msvcrt.dll')  # Windows only
        # libc = ctypes.oledll.LoadLibrary('msvcrt.dll')  # Windows only

        # libc = ctypes.CDLL('msvcrt.dll')
        # libc = ctypes.PyDLL('msvcrt.dll')
        # libc = ctypes.WinDLL('msvcrt.dll')  # Windows only
        # libc = ctypes.OleDLL('msvcrt.dll')  # Windows only

        # Windows下调用user32.dll中的MessageBoxA函数。
        # user32 = ctypes.windll.LoadLibrary('user32.dll')
        # user32.MessageBoxA(0, str.encode('Ctypes is so smart!'), str.encode('Ctypes'), 0)
    elif platform.system() == 'Linux':
        libc = ctypes.cdll.LoadLibrary('libc.so.6')
        # libc = ctypes.pydll.LoadLibrary('libc.so.6')

        # libc = ctypes.CDLL('libc.so.6')
        # libc = ctypes.PyDLL('libc.so.6')
    else:
        print("未识别的操作系统", platform.system())
        libc = None
        exit(1)

    # libc.printf("Hello ctypes!\n")
    print(libc.rand())


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
