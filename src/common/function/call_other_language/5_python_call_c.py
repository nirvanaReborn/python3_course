#!/usr/bin/env python
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/apexchu/p/5015961.html
# python实例浅谈之三Python与C/C++相互调用
#
# 使用c语言调用python小结
# http://blog.csdn.net/feitianxuxue/article/details/41129677
import faulthandler

faulthandler.enable(file=open("1.log", "a+"), all_threads=True)

import ctypes
from ctypes import *
import platform
import os
import binascii
import struct


def to_bytes(bytes_or_str):
    if isinstance(bytes_or_str, str):
        value = bytes_or_str.encode('utf-8')
    else:
        value = bytes_or_str
    return value  # Instance of bytes


def get_lib(lib_path=None):
    if platform.architecture()[0] == '64bit':
        print("需要切换为64位版本的python运行")
    else:
        # （需要32位版本的python运行）
        print("需要切换为32位版本的python运行")

    if lib_path:
        (filepath, filename) = os.path.split(lib_path)
        (shotname, extension) = os.path.splitext(filename)
        if platform.system() == 'Windows':
            lib_name = os.path.join(filepath, shotname + '.dll')
            if os.path.exists(lib_name):
                libc = ctypes.cdll.LoadLibrary(lib_name)
            else:
                print('不存在:', lib_name)
                libc = None
        elif platform.system() == 'Linux':
            lib_name = os.path.join(filepath, shotname + '.so')
            if os.path.exists(lib_name):
                libc = ctypes.cdll.LoadLibrary(lib_name)
            else:
                print('不存在:', lib_name)
                libc = None
        else:
            print("未识别的操作系统", platform.system())
            libc = None
    else:
        if platform.system() == 'Windows':
            # （需要32位版本的python运行）
            libc = ctypes.cdll.LoadLibrary('msvcrt.dll')
        elif platform.system() == 'Linux':
            libc = ctypes.cdll.LoadLibrary('libc.so.6')
        else:
            print("未识别的操作系统", platform.system())
            libc = None

    if libc is None:
        exit(1)
    return libc


# -----------------------------------------------------------------------------------------------------
def test_1():
    import cppyy

    # variadic template example; the code can also live in a header which is loaded
    # with "cppyy.include()"
    cppyy.cppdef(r"""
        template <typename Arg, typename... Args>
        void log(int level, Arg&& arg, Args&&... args)
        {   
            std::stringstream out;
            out << std::forward<Arg>(arg);
            using expander = int[];
            (void)expander{0, (void(out << ' ' << std::forward<Args>(args)), 0)...};
            std::cerr << out.str() << '\n';
        }
    """)

    level = 1
    cppyy.gbl.log(level, 1, "string", 3.14)


def test_2():
    '''用来调用 C 代码的外来函数接口。'''
    import cffi
    pass


def test_3():
    '''Nvidia CUDA API 的封装'''
    import pycuda
    pass


# -----------------------------------------------------------------------------------------------------
# https://www.cnblogs.com/gaowengang/p/7919219.html
# 导入dll文件
def test_11():
    libc = get_lib()
    libc.printf('Hello ctypes!\n')
    print(libc.time(None))


def test_12():
    if platform.system() == 'Linux':
        lib_path = 'lib_zt_public.so'
        libc = get_lib(lib_path)
        ret = libc.lozenge(ctypes.c_int(7))  # 调用so库中的函数
        print(ret)


def test_13():
    '''
    extern "C"
    {
        int addBuf(char* data, int num, char* outData);
    }

    int addBuf(char* data, int num, char* outData)
    {
        for (int i = 0; i < num; ++i)
        {
            outData[i] = data[i] + 3;
        }
        return num;
    }
    '''
    num = 4
    data_in = (ctypes.c_char_p * num)()
    print(type(data_in))
    for i in range(num):
        data_in[i] = i
    data_out = (ctypes.c_char_p * num)()
    lib_path = 'libmathBuf.so'
    libc = get_lib(lib_path)
    ret = libc.addBuf(data_in, ctypes.c_int(num), data_out)  # 调用so库中的函数


def test_14():
    '''
    #include <stdio.h>

    void get_str_list(int n, char *b[2])
    {
        printf("in c start");
        for(int i=0;i<n;i++)
        {
            printf("%s", *(b+i));
            printf("\n");
        }
        printf("in c end");
    }

    void get_point_int(int n, int *b)
    {
        printf("in c start\n");
        for(int i=0;i<n;i++)
        {
            printf("%d", *(b+i));
            printf("\n");
        }
        printf("in c end\n");
    }

    void get_point_str(int n, char *b[])
    {
        printf("in c start\n");
        for(int i=0;i<n;i++)
        {
            printf("%s", *(b+i));
            printf("\n");
        }
        printf("in c end");
    }
    # gcc -o hello2.so -shared -fPIC c_p.c
    '''
    lib_path = "./12_23_ctype_list/hello2.so"
    lib = get_lib(lib_path)
    n = 3
    str1 = ctypes.c_char_p(bytes("nihao", 'utf-8'))
    str2 = ctypes.c_char_p(bytes("shijie", 'utf-8'))
    lists = [str1, str2]
    print(enumerate(lists))
    a = (ctypes.c_char_p * 2)(str1, str2)
    lib.get_str_list(2, a)

    b = [1, 23, 345]
    b_arr = (ctypes.c_int * 3)(*b)
    lib.get_point_int(3, b_arr)

    c = ["hello", "world", "ni", "hao"]
    str_c1 = ctypes.c_char_p(bytes("hello", "utf-8"))
    str_c2 = ctypes.c_char_p(bytes("world", "utf-8"))
    str_c3 = ctypes.c_char_p(bytes("ni", "utf-8"))
    str_c4 = ctypes.c_char_p(bytes("hao", "utf-8"))
    c = [str_c1, str_c2, str_c3, str_c4]
    c_arr = (ctypes.c_char_p * 4)(*c)  # 传入一个python的list的指针
    lib.get_point_str(4, c_arr)


# Load 自定义 DLL
def test_15():
    if platform.system() == 'Windows':
        mylib = cdll.LoadLibrary('./nfs.dll')
    elif platform.system() == 'Linux':
        mylib = cdll.LoadLibrary('./nfs.so')
    else:
        mylib = None
        exit()
    mylib.hello_world()

    mod_rtv = mylib.mod(ctypes.c_int(10), ctypes.c_int(4))
    print('mod_rtv(10 % 4) =', mod_rtv)

    # 1D array get elem #
    IntArray10Type = ctypes.c_int * 10
    intArray = IntArray10Type()
    for idx in range(10):
        intArray[idx] = idx ** 2

    for idx in range(10):
        print('intArray[%d] = %d' % (idx, mylib.get_array_elem(intArray, idx)))
    print()

    # 2D array get elem #
    IntArray3Col = ctypes.c_int * 3
    IntArray3Row3Col = IntArray3Col * 3
    arr2d = IntArray3Row3Col(IntArray3Col(1, 2, 3), IntArray3Col(8, 9, 4), IntArray3Col(7, 6, 5))

    print('arr2d is:')
    for r in range(3):
        for c in range(3):
            print('%d ' % mylib.get_array2D_elem(arr2d, r, c), end="\t")
        print()


# https://blog.csdn.net/weixin_39820177/article/details/110275036
def test_16():
    class SSHead(BigEndianStructure):
        # 1字节对齐
        _pack_ = 1
        _fields_ = [
            # (字段名, c类型 )
            ('nTotalSize', c_uint32),
            ('nSourceID', c_int32),
            ('sourceType', c_uint8),
            ('destType', c_uint8),
            ('transType', c_uint8),
            ('nDestID', c_int32),
            ('nFlag', c_uint8),
            ('nOptionalLength', c_uint16),
            ('arrOptional', c_char * 20),
        ]

        def encode(self):
            return string_at(addressof(self), sizeof(self))

        def decode(self, data):
            memmove(addressof(self), data, sizeof(self))
            return len(data)

        def pack(self):
            return struct.pack("!IIBBBIBH20s", self.nTotalSize, self.nSourceID,
                               self.sourceType, self.destType, self.transType, self.nDestID,
                               self.nFlag, self.nOptionalLength, self.arrOptional)

        def unpack(self, data):
            (self.nTotalSize, self.nSourceID, self.sourceType, self.destType,
             self.transType, self.nDestID, self.nFlag, self.nOptionalLength,
             self.arrOptional) = struct.unpack("!IIBBBIBH20s", data)


# -----------------------------------------------------------------------------------------------------


def test_31():
    int_p = pointer(ctypes.c_int(4))
    print(int_p, type(int_p))
    # <__main__.LP_c_long object at 0x000001FEFF3C6F48> <class '__main__.LP_c_long'>

    char_p_type = POINTER(c_char)
    print(char_p_type, type(char_p_type))
    # <class 'ctypes.LP_c_char'> <class '_ctypes.PyCPointerType'>

    # ctypes提供cast()方法将一个ctypes实例转换为指向另一个ctypes数据类型的指针，
    # cast()接受两个参数，一个是ctypes对象，它是或可以转换成某种类型的指针，另一个是ctypes指针类型。
    # 它返回第二个参数的一个实例，该实例引用与第一个参数相同的内存块。
    # cast()函数需要两个参数，第一个是转换前的指针实例，第二个是目标指针类型。
    cast_type = cast(int_p, char_p_type)
    print(cast_type)  # <ctypes.LP_c_char object at 0x0000022AB2434EC8>
    print(byref(cast_type))  # <cparam 'P' (0000022AB2434F10)>
    # 访问指针指向的内容
    print(cast_type.contents)  # c_char(b'\x04')
    print(cast_type.contents.value)  # b'\x04'
    print(cast_type.contents.value.decode())


# -----------------------------------------------------------------------------------------------------
# https://blog.51cto.com/u_15127561/4148397
def test_32():
    a = 12345678
    # cast()函数需要两个参数，第一个是转换前的指针实例，第二个是目标指针类型。
    p = cast(id(a), POINTER(c_char))
    print(p.contents)  # c_char(b'\x03')
    b = a
    print(p.contents)  # c_char(b'\x04')
    c = a
    print(p.contents)  # c_char(b'\x05')
    del b
    print(p.contents)  # c_char(b'\x04')
    del c
    print(p.contents)  # c_char(b'\x03')
    print(p.contents.value)  # b'\x03'
    print(p.contents.value.decode())
    # 可以看到，引用每增加一个，refcnt的第一个字节就加1；引用每减少一个，refcnt的第一个字节就减1。
    # 我用的是window系统，整数是小端存储，先存储低位字节，所以恰好引用数量的变化反映在了refcnt的第一个字节上。


def print_bytes(bs):
    for i, b in enumerate(bs):
        if i % 8 == 0 and i != 0:
            print(' ', end='')
        if i % 16 == 0 and i != 0:
            print()
        print('{:02X} '.format(b), end='')
    print('\n')


def dump_mem(address, size):
    p = ctypes.cast(address, ctypes.POINTER(ctypes.c_char * size))
    return p.contents.raw


def print_mem(address, size):
    mem = dump_mem(address, size)
    print_bytes(mem)


def print_obj(obj, size):
    print_mem(id(obj), size)


def test_33():
    a = 1234.5678
    p = cast(id(a), POINTER(c_char * 32))
    print(p.contents)  # <__main__.c_char_Array_32 object at 0x00000287F4286E48>
    print(p.contents.raw)  # b'\x03\x00\x00\x00\x00\x00\x00\x00\x00,\xa5\x7f\xfd\x7f\x00\x00\xad\xfa\\mEJ\x93@P\xdd\xd0\xf3\x87\x02\x00\x00'
    print(p.contents.value)  # b'\x03'
    print_obj(1, 32)
    print_obj(0xABCDEF, 32)


# https://www.codingdict.com/questions/165691
def test_34():
    '''
    extern "C" int* function(){
        int* information = new int[10];
        for(int k=0;k<10;k++){
            information[k] = k;
        }
        return information;
    }
    '''
    from numpy.ctypeslib import ndpointer

    lib = ctypes.CDLL('./library.so')
    lib.function.restype = ndpointer(dtype=ctypes.c_int, shape=(10,))

    res = lib.function()


# https://www.cnpython.com/qa/112612
def test_35():
    '''
    typedef struct td_Group
    {
        unsigned int group_id;
        char groupname[256];
        char date_created[32];
        char date_modified[32];
        unsigned int user_modified;
        unsigned int user_created;
    } Group;
    int getGroups(LIBmanager * handler, Group ** unallocatedPointer);
    '''

    class Group(Structure):
        _fields_ = [("group_id", c_uint),
                    ("groupname", c_char * 256),
                    ("date_created", c_char * 32),
                    ("date_modified", c_char * 32),
                    ("user_modified", c_uint),
                    ("user_created", c_uint)]

    myGroups = c_void_p()
    count = libnativetest.getGroups(nativePointer, byref(myGroups))
    # casted = cast(myGroups, POINTER(Group * count))
    # for x in range(0, count):
    #     theGroup = cast(casted[x], POINTER(Group))
    #     # this only works for the first entry in the array:
    #     print("~~~~~~~~~~" + theGroup.contents.groupname)
    GroupArray = POINTER(Group * count)
    group_array = GroupArray.from_address(addressof(myGroups))
    for x in range(0, count):
        print("~~~~~~~~~~" + group_array.contents[x].groupname)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
