#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# http://www.jb51.net/article/43502.htm
# python调用shell的方法
# https://blog.csdn.net/baidu_36943075/article/details/105681683

import sys
import os
import subprocess


def test_1(shell_cmd):
    # 只能判断是否执行成功
    def os_system(shell_cmd):
        result = os.system(shell_cmd)
        if result is not 0:
            print("命令执行失败")
        else:
            return result  # 结果为0则表示执行成功，为其他值则表示执行不成功

    result = os_system(shell_cmd)
    print(result)


def test_2(shell_cmd):
    # 只能获取执行结果
    def os_popen(shell_cmd, *parm):
        re = os.popen(shell_cmd).readlines()
        result = []
        for i in range(0, len(re) - 1):  # 由于原始结果需要转换编码，所以循环转为utf8编码并且去除\n换行
            res = re[i].strip('\n')
            result.append(res)

        if parm == ():
            return result  # 获取全部执行结果
        else:
            line = int(parm[0]) - 1
            return result[line]  # 获取执行结果的指定行

    result = os_popen(shell_cmd)
    print(result)


def test_3(shell_cmd):
    # 只能判断是否执行成功
    def subprocess_call(shell_cmd):
        result = subprocess.call(shell_cmd)
        # 执行失败不需要特殊处理，命令执行失败会直接报错
        return result  # 结果为0则表示执行成功，为其他值则表示执行不成功

    result = subprocess_call(shell_cmd)
    print(result)


def test_4(shell_cmd):
    # 只能判断是否执行成功
    def subprocess_check_call(shell_cmd):  # 基本与call一致
        result = subprocess.check_call(shell_cmd)
        # 执行失败不需要特殊处理，命令执行失败会直接报错
        return result  # 结果为0则表示执行成功，为其他值则表示执行不成功

    result = subprocess_check_call(shell_cmd)
    print(result)


def test_5(shell_cmd):
    def subprocess_getstatusoutput(shell_cmd):
        (status, output) = subprocess.getstatusoutput(shell_cmd)
        print(status, output)
        return output

    result = subprocess_getstatusoutput(shell_cmd)
    print(result)


def test_6(shell_cmd):
    # 只能获取执行结果
    def subprocess_getoutput(shell_cmd):
        result = subprocess.getoutput(shell_cmd)
        # 执行失败不需要特殊处理，因为该方法无法判断失败成功，只负责将结果进行返回
        return result  # 返回执行结果，但是结果返回的是一个str字符串（不论有多少行）

    result = subprocess_getoutput(shell_cmd)
    print(result)


def test_7(shell_cmd):
    # 只能获取执行结果
    def subprocess_check_output(shell_cmd):
        result = subprocess.check_output(shell_cmd).decode('utf-8')
        # 执行失败不需要特殊处理，命令执行失败会直接报错
        return result  # 返回执行结果，但是结果返回的是一个str字符串（不论有多少行），并且返回的结果需要转换编码

    result = subprocess_check_output(shell_cmd)
    print(result)


def test_8(shell_cmd):
    # 既可以判断执行是否成功，还可以获取执行结果
    def subprocess_popen(shell_cmd):
        p = subprocess.Popen(shell_cmd, shell=True, stdout=subprocess.PIPE)  # 执行shell语句并定义输出格式
        while p.poll() is None:  # 判断进程是否结束（Popen.poll()用于检查子进程（命令）是否已经执行结束，没结束返回None，结束后返回状态码）
            if p.wait() != 0:  # 判断是否执行成功（Popen.wait()等待子进程结束，并返回状态码；如果设置并且在timeout指定的秒数之后进程还没有结束，将会抛出一个TimeoutExpired异常。）
                print("命令执行失败，请检查设备连接状态")
                return False
            else:
                re = p.stdout.readlines()  # 获取原始执行结果
                result = []
                for i in range(len(re)):  # 由于原始结果需要转换编码，所以循环转为utf8编码并且去除\n换行
                    res = re[i].decode('utf-8').strip('\r\n')
                    result.append(res)
                return result

    result = subprocess_popen(shell_cmd)
    print(result)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_" + str(i) + "(shell_cmd)"

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == '__main__':
    # source_file = r"../../../../../../zhangtao2016/course_linux/shell/src/base/01_test.sh"
    shell_cmd = "python3-config --extension-suffix"
    if sys.platform.startswith('linux'):
        main()
    else:
        print("未识别操作系统:", sys.platform)
