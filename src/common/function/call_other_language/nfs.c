//----------------------------------------------------------------------------
// Purpose: this c module is used to speed up the Python program, should be 
//          compiled into dll, and then load into Python module with ctypes
//          method.
//
// Compile Methods:
//
//    ======================
//    Windows: use MSVC, x64
//    C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\bin\amd64> cl /LD needforspeed.c /o nfs.dll
//    ======================
//
//    ======
//    Linux:
//    $ gcc -fPIC -shared nfs.c -o nfs.so
//    ======
//
//----------------------------------------------------------------------------

#include <stdio.h>

// Windows need this compile direction for dll compilation, Linux no need 
#ifdef _MSC_VER
    #define DLL_EXPORT __declspec( dllexport ) 
#else
    #define DLL_EXPORT
#endif

DLL_EXPORT void hello_world(void) {
    printf("Hello world!\n");
}

DLL_EXPORT int mod(int m, int n) {
    return m % n;
}

DLL_EXPORT int get_array_elem(int arr[], int idx) {
    return arr[idx];
}

DLL_EXPORT int get_array2D_elem(int arr[][3], int row, int col) {
    return arr[row][col];
}