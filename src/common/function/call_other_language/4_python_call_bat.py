#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://blog.csdn.net/menglei8625/article/details/7494094

# http://blog.csdn.net/luhouxiang/article/details/31411273
# python启动cmd批处理文件
'''
cmd /c dir：是执行完dir命令后关闭命令窗口；
cmd /k dir：是执行完dir命令后不关闭命令窗口。
cmd /c start dir：会打开一个新窗口后执行dir指令，原窗口会关闭；
cmd /k start dir：会打开一个新窗口后执行dir指令，原窗口不会关闭。
'''

import sys


def test_1(source_file):
    import os

    result = os.system(source_file)
    print(type(result))  # <class 'int'>


def test_2(source_file):
    import os

    list_result = os.popen(source_file).readlines()
    for i, fileLine in enumerate(list_result):
        print(fileLine)


def test_3(source_file):
    import subprocess
    # Popen非常强大，支持多种参数和模式。
    # 但是Popen函数有一个缺陷，就是它是一个阻塞的方法。
    # 如果运行cmd时产生的内容非常多，函数非常容易阻塞住。
    # 解决办法是不使用wait()方法，但是也不能获得执行的返回值了。

    choice = r"cmd.exe /c" + source_file
    result = subprocess.Popen(choice, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    # print(result.stdout.read().decode('gbk'))
    curline = result.stdout.readline()
    while (curline != b''):
        print(curline.decode("gb2312"))
        curline = result.stdout.readline()

    result.wait()
    # subprocess.Popen.wait(result, timeout=1)
    print(result.returncode)


def test_4(source_file):
    import subprocess

    choice = r"cmd.exe /c" + source_file
    result = subprocess.Popen(choice, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    byte_data = result.stdout.read(1)
    word_data = b''
    while (byte_data != b''):
        word_data += byte_data
        try:
            showdata = word_data.decode('gb2312')
            print(showdata, end="", flush=True)
            word_data = b''
        except Exception as e:
            # print(e)
            pass
        byte_data = result.stdout.read(1)

    result.wait()
    print(result.returncode)


def test_5(source_file):
    # 使用commands.getstatusoutput方法
    # 这个方法唯一的优点是，它不是一个阻塞的方法。即没有Popen函数阻塞的问题。
    # commands模块在python3.x被subprocess取代
    import subprocess

    status, output = subprocess.getstatusoutput(source_file)
    # 还有只获得output和status的方法：
    # output = commands.getoutput("ls")
    # status = commands.getstatus("ls")
    print(status, output)


def test_6(source_file):
    import ctypes
    import platform

    if platform.system() == 'Windows':
        # Windows下调用user32.dll中的MessageBoxA函数。
        user32 = ctypes.windll.LoadLibrary('user32.dll')
        user32.MessageBoxA(0, str.encode('Ctypes is so smart!'), str.encode('Ctypes'), 0)


def main():
    if sys.platform.startswith('win'):
        source_file = r'D:\share\git\gitee\zhangtao2016\course_bat\src\test\python_test.bat'

        dict_choice = {
            "1": "test_1(source_file)",
            "2": "test_2(source_file)",
            "3": "test_3(source_file)",
            "4": "test_4(source_file)",
        }

        choice = str(input("Enter a positive integer to choice: "))
        if choice in dict_choice.keys():
            exec(dict_choice[choice])
        else:
            print("输入有误，请重输！", choice)
    else:
        print("未识别操作系统:", sys.platform)


if __name__ == '__main__':
    main()
