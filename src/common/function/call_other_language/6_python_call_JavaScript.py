#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# https://www.cnblogs.com/tjp40922/p/10816375.html
# 将 JavaScript 转换为 Python 代码。
# Js2Py 能够翻译和执行几乎任何 JavaScript 代码。
# Js2Py 是用纯 python 编写的，没有任何依赖关系。基本上是在纯 python 中实现 JavaScript 内核。
# https://github.com/PiotrDabkowski/Js2Py
# pip install js2py
import js2py


def test_1():
    js = js2py.EvalJs({})
    js.execute("""
    var i = 0;
    for(;;i++) {
        break;
    }
    console.log("i should be 0. i=" + i);
    """)


# https://blog.csdn.net/weixin_42812527/article/details/82918746
def test_2():
    LoginKey = js2py.eval_js('''
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    };
    function guid() {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }
    var a = guid()  // 在这里是需要调用的
    ''')
    print(LoginKey)


def main():
    dict_choice = {}
    for i in range(0, 100):
        dict_choice[str(i)] = "test_{}()".format(str(i))

    choice = str(input("Enter a positive integer to choice: "))
    if choice in dict_choice.keys():
        exec(dict_choice[choice])
    else:
        print("输入有误，请重输！", choice)


if __name__ == "__main__":
    main()
