#!/usr/bin/env python
# -*- coding:utf-8 -*-

# pycharm 自动补全代码提示前符号 是自动补全的变量的类别:
# p：parameter 参数
# f：function 函数
# c：class 类
# m：method 方法
# v：variable 变量

import os
import importlib

# print("当前文件名:", sys._getframe().f_code.co_filename)
# print("当前函数名:", sys._getframe().f_code.co_name)
# print("当前的行号:", sys._getframe().f_lineno)

PROJECT_ROOT = os.path.dirname(os.path.realpath(__file__))
# print(PROJECT_ROOT)


call_dir = os.path.join(PROJECT_ROOT, r"../../../python3_course/src/common")
GLOBAL_RELATIVE_PATH_OTHER_MODULE = os.path.join(call_dir, r"./module/other_module")
GLOBAL_RELATIVE_PATH_BUILTIN_MODULE = os.path.join(call_dir, r"./module/built-in_module")

# call_dir = os.path.join(PROJECT_ROOT, r"../../../python3_public_resource/src/public_resource")
# print(os.path.abspath(call_dir))
# sys.path.append(call_dir)
# from public_function_summary import *

# 全局常量
PYTHON_PATH = r"D:\ProgramFiles\Anaconda3\python.exe"
PYTHON_PIP_PATH = r"D:\ProgramFiles\Anaconda3\Scripts\pip.exe"
PYTHON_2TO3_PATH = r"D:\ProgramFiles\Anaconda3\Scripts\2to3.exe"
PYTHON_PYINSTALLER_PATH = r"D:\ProgramFiles\Anaconda3\Scripts\pyinstaller.exe"

GLOBAL_EMPTY_STR = ''
GLOBAL_SPACE_CHAR = ' '

GLOBAL_WORK_DIR = r"D:\share\test"
GLOBAL_FILE_NAME = r"%s" % importlib.import_module("time").strftime("%Y%m%d")
GLOBAL_TXT_FILE = importlib.import_module("os").path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".txt")
GLOBAL_LOG_FILE = importlib.import_module("os").path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".log")
GLOBAL_ERROR_FILE = importlib.import_module("os").path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".error")
GLOBAL_CSV_FILE = importlib.import_module("os").path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".csv")
GLOBAL_XLS_FILE = importlib.import_module("os").path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".xls")
GLOBAL_XLSX_FILE = importlib.import_module("os").path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".xlsx")
GLOBAL_DOCX_FILE = importlib.import_module("os").path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".docx")

GLOBAL_JPG_FILE = importlib.import_module("os").path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".jpg")
GLOBAL_PNG_FILE = importlib.import_module("os").path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".png")
GLOBAL_GIF_FILE = importlib.import_module("os").path.join(GLOBAL_WORK_DIR, GLOBAL_FILE_NAME + ".gif")


dict_choice = {}
for i in range(0, 100):
    dict_choice[str(i)] = "test_{}()".format(str(i))


# 函数装饰器: 它经常用于有切面需求的场景，比如：插入日志，性能测试，事务处理，缓存，权限的校验等场景。
# 有了装饰器就可以抽离出大量的与函数功能本身无关的雷同代码并发并继续使用。
def timeit(func):
    import time
    from functools import wraps

    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.clock()
        ret = func(*args, **kwargs)
        end = time.clock()
        print('used:',end-start)
        return ret
    return wrapper


def get_browser_chrome():
    from selenium import webdriver

    options = webdriver.ChromeOptions()
    # 在 Chrome 59 版本开始已经开始支持了 Headless 模式，也就是无界面模式，这样爬取的时候就不会弹出浏览器了。
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    # 让chrome浏览器不出现‘Chrome正在受到自动软件的控制’的提示语
    options.add_argument('disable-infobars')
    # options.add_argument('--window-size=1920,1080')  # 设置窗口界面大小
    browser = webdriver.Chrome(
        executable_path=r"D:\ProgramFiles\1_FreeInstallation\python_selenium_driver\chromedriver.exe",
        options=options,
    )
    # print(type(browser)) # <class 'selenium.webdriver.chrome.webdriver.WebDriver'>
    return browser


class FileInfo():
    # 初始化函数，类似于构造函数
    def __init__(self, file_name):
        super(FileInfo, self).__init__()
        self.file_name = os.path.abspath(file_name)

    # python读取文件编码格式
    # https://www.cnblogs.com/lizhengtan/p/5538231.html
    def get_file_encoding(self):
        '''
        {'confidence': 0.64465744, 'encoding': 'utf-8'}
        confidence：表示相似度
        encoding：表示编码格式
        '''
        # pip install chardet chardet2
        import chardet
        with open(self.file_name, "rb") as fd:
            return chardet.detect(fd.read(1024))["encoding"]

    def get_file_path_info(self):
        filepath, basename = os.path.split(self.file_name)
        shotname, extension = os.path.splitext(basename)
        return filepath, basename, shotname, extension

    def get_file_prefix(self):
        # return os.path.splitext(self.file_name)[0]

        # tmp = self.file_name.split('\\')
        # return tmp[len(tmp) - 1].split('.')[0]
        return self.get_file_path_info()[2]

    def get_file_suffix(self):
        # return os.path.splitext(self.file_name)[1]
        return self.get_file_path_info()[3]


class GetConfigInfo():
    # 初始化函数，类似于构造函数
    def __init__(self, file_name, filter_tag=r"登录信息"):
        self.source_file = os.path.abspath(file_name)
        self.filter_tag = filter_tag
        self.list_unique_index = []

    def analysis_xml(self) -> list:
        try:
            import xml.etree.cElementTree as ET
        except ImportError:
            import xml.etree.ElementTree as ET

        list_config = []
        tree = ET.parse(self.source_file)
        root = tree.getroot()
        for child in root:  # 第二层节点
            if child.tag == self.filter_tag:
                if "unique_index" in child.keys():
                    unique_index = child.get("unique_index", "")
                    if unique_index:
                        self.list_unique_index = str(unique_index).split(',')

                for subchild in child:  # 第三层节点
                    list_config.append(subchild.attrib)
        return list_config

    def get_list_config(self) -> list:
        filename, suffix = os.path.splitext(self.source_file)
        # print(suffix)
        if ".xml" == suffix:
            return self.analysis_xml()
        # elif suffix in (".xlsx", ".xls"):
        #     return self.analysis_excel()
        elif ".ini" == suffix:
            pass
        else:
            print("未知的文件格式:", self.source_file)
            return []

    def get_dict_config(self) -> dict:
        def get_list_unique_index(dict_info):
            if self.list_unique_index:
                if len(self.list_unique_index) == 1:
                    return str(dict_info[self.list_unique_index[0]])
                else:
                    list_unique_index = []
                    for item in self.list_unique_index:
                        list_unique_index.append(str(dict_info[item]))
                    return tuple(list_unique_index)
            else:
                print("没有指定唯一索引字段列表")
                return None

        dict_config = {}
        list_dict_info = self.get_list_config()
        for dict_info in list_dict_info:
            dict_config[get_list_unique_index(dict_info)] = dict_info

        return dict_config


if __name__ == "__main__":
    dict_file = {}
    try:
        for parent, dirnames, filenames in os.walk(os.path.join(PROJECT_ROOT, "module")):
            for filename in filenames:
                source_file = os.path.join(parent, filename).encode('utf-8').decode('utf-8')
                short_name = os.path.basename(source_file)
                if short_name in dict_file.keys():
                    print("已存在:", source_file)
                else:
                    dict_file[short_name] = source_file
        print(dict_file)
    except Exception as e:
        print(e)
