# python3_course

#### 项目介绍

python3基础教程

#### 软件架构

*  PyCharm + Anaconda3(V5.1.0)[python3.6.4]
*  [Anaconda版本与Python版本对应关系](https://blog.csdn.net/zyb228/article/details/103251761)
*  [Anaconda历史版本](https://repo.anaconda.com/archive/)

#### 教程来源

[Python3 教程](https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000)

#### 参考教程

[廖雪峰的官方网站](https://www.liaoxuefeng.com/)
