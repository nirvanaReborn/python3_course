class Foo(object):
    def __init__(self, x):
        self.x = x

    def bar(self, y):
        return self.x + y


foo = Foo(333)
print(foo.bar(3))

# result:336
