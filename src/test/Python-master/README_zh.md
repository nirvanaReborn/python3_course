[项目地址](https://github.com/geekcomputers/Python)

# My Python Examples.
	• batch_file_rename.py - 这将批量重命名给定目录中的一组文件，一旦你通过当前和新的扩展。
	• create_dir_if_not_there.py - 检查用户主目录中是否存在目录，如果不存在，则创建它。
    • youtube downloader fast - 使用aria2c以并行线程快速下载youtube视频
	• Google图片下载器 - 查询特定的术语，并从谷歌图像数据库检索图像。
	• dir_test.py - 测试目录是否 testdir 存在，如果不存在，它会为你创建目录。
	• env_check.py - 该脚本将检查是否设置了所有我需要的环境变量。
	• fileinfo.py - 显示给定文件的文件信息。
	• folder_size.py - 这将扫描当前目录和所有子目录并显示大小。
	• logs.py - 此脚本将搜索 *.log 给定目录中的所有文件，使用您指定的程序对它们进行压缩，然后对它们进行日期戳记。
	• move_files_over_x_days.py - 这会将所有文件从240天以上的源目录移动到目标目录。
	• nslookup_check.py - 这个非常简单的脚本打开文件server_list.txt，然后为每个脚本执行nslookup来检查DNS条目。
	• osinfo.py - 显示有关运行此脚本的操作系统的一些信息。
	• ping_servers.py - 根据提供的参数，该脚本将ping与该应用程序组关联的服务器。
	• ping_subnet.py - 提供前3个字节后，将扫描可用地址的最终范围。
	• powerdown_startup.py - 这是通过服务器列表，并ping机器，如果它将加载腻子会话，如果它不会通知你。
	• puttylogs.py - 压缩给定目录中的所有日志。
	• script_count.py - 这将扫描我的脚本目录，并给出不同类型的脚本的计数。
	• script_listing.py - 这将列出给定目录中的所有文件，它也将通过所有的子目录。
	• testlines.py - 这个非常简单的脚本打开一个文件，打印出100行，无论是行变量的设置。
	• tweeter.py - 这个脚本允许你从终端发送文本或图片。
	• serial_scanner.py包含一个名为ListAvailablePorts的方法，它返回一个列表，其中包含我们的计算机中使用的串行端口的名称，此方法仅适用于Linux和Windows（可以扩展到mac osx）。如果没有找到端口，则返回空列表。
	• get_youtube_view.py - 这是一个非常简单的python脚本，可以为youtube视频获取更多视图。有时候我会用这个脚本来重复我最喜欢的歌曲。
	• CountMillionCharacter.py和CountMillionCharacter2.0 .py - 该脚本用于1.计数字符脚本2.计算在任何基于文本的文件上存在多少字符。
	• xkcd_downloader.py - 下载最新的XKCD漫画，并把它们放在一个新的文件夹“漫画”。
	• timymodule.py - Pythons'timeit '模块的一个很好的选择，更易于使用。
	• calculator.py - 使用Python的eval（）函数来实现一个计算器。
	• Google_News.py - 使用BeautifulSoup提供最新的新闻标题以及新闻链接。
	• cricket_live_score - 使用BeautifulSoup提供活蟋蟀得分。
	• youtube.py - 输入歌曲名称并获取最佳匹配歌曲的YouTube网址并播放。
